package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;
import android.text.TextUtils;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.TeacherListBean;
import com.sdzn.pkt.student.phone.network.api.ApiInterface;

import java.util.List;

/**
 * 描述：
 * - 首页正在直播列表adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/6
 */
public class TeacherAdapter extends BaseRcvAdapter<TeacherListBean> {

    public TeacherAdapter(Context context, List<TeacherListBean> mList) {
        super(context, R.layout.item_teacher_cart, mList);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 1 : mList.size() + 1;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        convert(holder, position, mList == null || position == mList.size() ? null : mList.get(position));
    }

    @Override
    public void convert(BaseViewHolder holder, int position, TeacherListBean bean) {
        if (bean == null) {
            holder.setText(R.id.tv_name, "查看全部");
            holder.setVisible(R.id.iv_head, false);
            holder.setVisible(R.id.iv_more, true);
            holder.setInVisible(R.id.tv_info, false);
        } else {
            holder.setVisible(R.id.iv_head, true);
            holder.setVisible(R.id.iv_more, false);
            holder.setVisible(R.id.tv_info, true);
            if (TextUtils.isEmpty(bean.getPicPath())) {
                holder.setImageView(R.id.iv_head, R.mipmap.ic_avatar);
            } else {
                String url = bean.getPicPath();
                holder.setImageView(R.id.iv_head, "" + url, R.mipmap.ic_avatar);
            }
            holder.setText(R.id.tv_name, bean.getName() == null ? "" : bean.getName());
            holder.setText(R.id.tv_info, bean.getEducation() == null ? "" : bean.getEducation());
        }
    }
}
