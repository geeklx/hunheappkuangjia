package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.pkt.student.phone.R;

/**
 * zs
 */
public class SubmitAssignmentActivity extends BaseActivity {
    private String Titlenumber;
    private TextView submittitle;
    private RecyclerView rvaddpic;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_submit_assignment;
    }

    @Override
    protected void onInit(Bundle bundle) {
        rvaddpic = findViewById(R.id.rv_add_pic);
        submittitle = findViewById(R.id.submit_title);
        Titlenumber = getIntent().getStringExtra("titlnumber");
        submittitle.setText("填空题：" + Titlenumber);
        initView();
        initData();
    }

    private void initData() {
    }

    private void initView() {

    }

}
