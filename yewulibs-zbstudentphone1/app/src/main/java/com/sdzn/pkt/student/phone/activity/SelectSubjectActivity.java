package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.AppUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.SpellingTitleAdapter;
import com.sdzn.pkt.student.phone.bean.GradeJson;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.fragment.SelectFiveFragment;
import com.sdzn.pkt.student.phone.fragment.SelectSixFragment;
import com.sdzn.pkt.student.phone.listener.SendSelectData;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.SelectSubjectPresenter;
import com.sdzn.pkt.student.phone.mvp.view.SelectSubjectView;
import com.sdzn.pkt.student.phone.widget.TitleBar;
import com.sdzn.pkt.student.phone.widget.pager.PagerSlidingTabStrip;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SelectSubjectActivity extends BaseMVPActivity<SelectSubjectView, SelectSubjectPresenter> implements SelectSubjectView, SendSelectData {
    public static final String IS_MAIN_IN = "to_in_subject";   //
    public static final String IS_UPDATE = "isUpdate";   //
    public static final String IS_DETAILS = "isDetails";   // 是否从详情页进入
    private boolean isMainIn = false;
    private boolean isUpdate = false;
    private boolean isDetails = false;
    private SpellingTitleAdapter titleAdapter;
    private List<Fragment> list_fragment;//定义要装fragment的列表
    private ArrayList<String> listTitle;

    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.tabs)
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    @BindView(R.id.vp_select)
    ViewPager vp;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_select_subject;
    }

    @Override
    protected void onInit(Bundle bundle) {
        isMainIn = getIntent().getBooleanExtra(IS_MAIN_IN, false);
        isUpdate = getIntent().getBooleanExtra(IS_UPDATE, false);
        isDetails = getIntent().getBooleanExtra(IS_DETAILS, false);
        initView();
    }


    @Override
    protected SelectSubjectPresenter createPresenter() {
        return new SelectSubjectPresenter();
    }

    private void initView() {
        if (isMainIn) {
            titleBar.setVisibility(View.VISIBLE);
        } else {
            titleBar.setVisibility(View.INVISIBLE);
        }
        listTitle = new ArrayList<>();
        listTitle.add("六三制");
        listTitle.add("五四制");
        titleAdapter = new SpellingTitleAdapter(getSupportFragmentManager());

        list_fragment = new ArrayList<>();
        list_fragment.add(SelectSixFragment.newInstance());
        list_fragment.add(SelectFiveFragment.newInstance());
        titleAdapter.setmDatas(listTitle, list_fragment);

        vp.setAdapter(titleAdapter);
        mPagerSlidingTabStrip.setViewPager(vp);
        vp.setOffscreenPageLimit(2);
        if (SPManager.getUser().getEducationId() > 1) {
            vp.setCurrentItem(1);
        }

    }

    private void initData() {

    }


    @Override
    public void setSuccess() {
        if (isDetails) {//从课程页进来的
            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程
            this.finish();
        } else {
            IntentController.toMain(mContext, false);
        }
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onBackPressed() {
        if (isMainIn) {//主页
            IntentController.toMain(mContext, false);
        } else {
            AppUtils.exitApp(mContext);
        }
    }
/*
@Field("educationId") String educationId, @Field("educationName") String educationName,
                                                   @Field("gradeId") String gradeId, @Field("gradeName") String gradeName,
                                                   @Field("levelId") String levelId, @Field("levelName") String levelName
 */

    @Override
    public void send(String eduId, GradeJson.ChildListBean dataBean) {
        if (isUpdate && SPManager.autoLogin(mContext)) {//toc第一次登录且 学段没数据时 用
            String eduName = "";
            if ("0".equals(eduId)){
                eduName = "无学制";
            }else if ("1".equals(eduId)){
                eduName = "六三制";
            }else if ("2".equals(eduId)){
                eduName = "五四制";
            }
            mPresenter.setSubAndGrade(eduId,eduName,dataBean.getSectionId(),dataBean.getSectionName(),dataBean.getGradeId(),dataBean.getGradeName());
            //第一次数据存  基本信息取
            SPManager.getUser().setGrade(Integer.valueOf(dataBean.getGradeId()));
            SPManager.getUser().setGradeName(dataBean.getGradeName());
            SPManager.getUser().setSubjectId(Integer.valueOf(dataBean.getSectionId()));
            SPManager.getUser().setSubjectName(dataBean.getSectionName());
        } else {
            IntentController.toMain(mContext, false);
        }
    }
}
