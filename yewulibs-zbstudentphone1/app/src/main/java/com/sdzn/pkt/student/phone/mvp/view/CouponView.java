package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.CouponBean;

import java.util.List;

/**
 * 描述：我的优惠券
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/20
 */

public interface CouponView extends BaseView {
    void listOnSuccess(List<CouponBean> couponBeanList);

    void onError(String msg);
}
