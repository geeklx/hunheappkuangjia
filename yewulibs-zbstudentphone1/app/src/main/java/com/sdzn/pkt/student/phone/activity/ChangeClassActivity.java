package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.UpdateAccountPresenter;
import com.sdzn.pkt.student.phone.mvp.view.UpdateAccountView;
import com.sdzn.pkt.student.phone.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * zs
 */
public class ChangeClassActivity extends BaseMVPActivity<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView {

    @BindView(R.id.et_name)
    EditText etName;
    private String studentName;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_change_class;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        studentName = getIntent().getStringExtra("classes");
        if (!TextUtils.isEmpty(studentName)) {
            etName.setText(studentName);
            etName.setSelection(studentName.length());
        }

    }

    @OnClick(R.id.btn_certain)
    public void onViewClicked() {
        String className = etName.getText().toString().trim();
        if (TextUtils.isEmpty(className)) {
            ToastUtils.showShort("班级不能为空");
        } else {
                mPresenter.updateUserInfo(className,"","");
        }
    }

    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_CLASSES));
        ChangeClassActivity.this.finish();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
