package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.BannerInfoBean;
import com.sdzn.pkt.student.phone.bean.CourseList;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public interface HomeView extends BaseView {


    void getDataFailure(String msg);

    void getCourseEmpty();

    void getDataCourse(List<CourseList> recommendCourses);

    void getSubjectDataCourse(List<CourseList> subjectCourses);

    void getBannerData(List<BannerInfoBean> infoBeanList);

    void getBannerEmpty();

}
