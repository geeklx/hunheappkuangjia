package com.sdzn.pkt.student.phone.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.CollectAdapter;
import com.sdzn.pkt.student.phone.bean.CollectBean;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.mvp.presenter.CollectPresenter;
import com.sdzn.pkt.student.phone.mvp.view.CollectView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.SwipeItemLayout;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 描述：我的收藏
 */

public class CollectActivity extends BaseMVPActivity<CollectView, CollectPresenter> implements CollectView, OnRefreshLoadmoreListener {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.swipe_target)
    RecyclerView recyclerCollect;
    @BindView(R.id.swipeToLoadLayout)
    SmartRefreshLayout swipeToLoadLayout;
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;
    public static final int RESULT_COURSEDETAIL_CODE = 2;


    private List<CollectBean.FavoriteListBean> mData = new ArrayList<>();
    private CollectAdapter collectAdapter;
    private int pageIndex = 1;
    private int pageSize = 10;
    private int currentPosition = 0;
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_collect;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initView();
        initData();
    }

    private void initData() {
        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    @Override
    public void onResume() {
        super.onResume();
//        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    private void initView() {
        collectAdapter = new CollectAdapter(mContext, mData);
        recyclerCollect.addOnItemTouchListener(new SwipeItemLayout.OnSwipeItemTouchListener(mContext));
        recyclerCollect.setLayoutManager(new LinearLayoutManager(this));
        recyclerCollect.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 10));
        recyclerCollect.setAdapter(collectAdapter);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
            }
        });

        swipeToLoadLayout.setOnRefreshLoadmoreListener(this);

        collectAdapter.setOnCollectListener(new CollectAdapter.OnCollectListener() {
            @Override
            public void cancelListener(int position) {
                currentPosition = position;
                mPresenter.delCollection(String.valueOf(mData.get(position).getCourseId()));
            }

            @Override
            public void mainListener(int position) {
                IntentController.toCourseDetail(mContext, mData.get(position).getSellType(), mData.get(position).getCourseId(), true);
            }
        });
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    @Override
    protected CollectPresenter createPresenter() {
        return new CollectPresenter();
    }

    @Override
    public void upDataSuccess(CollectBean collectBean) {

        if (collectBean.getFavoriteList() != null && !collectBean.getFavoriteList().isEmpty()) {
            if (pageIndex == 1) {
                mData.clear();
            }
            mData.addAll(collectBean.getFavoriteList());
            collectAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            if (pageIndex == 1) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                ToastUtils.showShort("没有更多,到底了");
            }
        }
        goneSwipView();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        }
        goneSwipView();

    }

    @Override
    public void delSuccess() {
        mData.remove(currentPosition);
        collectAdapter.notifyDataSetChanged();
    }

    @Override
    public void delError(String msg) {
        ToastUtils.showShort(msg);

    }

    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (swipeToLoadLayout.isRefreshing()) {
            swipeToLoadLayout.finishRefresh();
        }
        if (swipeToLoadLayout.isLoading()) {
            swipeToLoadLayout.finishLoadmore();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_COURSEDETAIL_CODE) {
            mData.remove(currentPosition);
            collectAdapter.notifyDataSetChanged();
        }
    }

}
