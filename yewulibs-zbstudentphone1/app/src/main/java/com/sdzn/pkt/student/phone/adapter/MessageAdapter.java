package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.MessageBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class MessageAdapter extends BaseRcvAdapter<MessageBean.LetterListBean> {
    public MessageAdapter(Context context, List<MessageBean.LetterListBean> mList) {
        super(context, R.layout.item_message, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, MessageBean.LetterListBean bean) {
        ImageView imageView = holder.getView(R.id.img_message_check);
        imageView.setSelected(bean.isSelected());
        String updateTime = bean.getUpdateTime();
        holder.setText(R.id.tv_date, updateTime);
        holder.setText(R.id.tv_message_generalize, bean.getContent());
//        holder.setTag(R.id.tv_message_type,bean.getShowname());
        if (bean.getType() == 1) {
            holder.setImageView(R.id.img_message_type, R.mipmap.type_message_system);
        } else {
            holder.setImageView(R.id.img_message_type, R.mipmap.type_message_course);
        }
        String title;
        switch (bean.getType()) {
            case 1:
                title = "系统消息";
                break;
            case 2:
                title = "站内信";
                break;
            case 5:
                title = "课程消息";
                break;
            case 6:
                title = "优惠券过期";
                break;
            default:
                title = "系统消息";
                break;
        }
        holder.setText(R.id.tv_message_type,title);
        TextView textView = (TextView) holder.getView(R.id.tv_message_type);
        if ("1".equals(bean.getStatus())) {//已读消息

            textView.setCompoundDrawables(null, null, null, null);
        } else {//未读消息
            setImgviewtoText(textView, R.drawable.shape_yuan);
        }
        if (isEdit) {
            holder.setVisible(R.id.img_message_type, false);
            holder.setVisible(R.id.img_message_check, true);

        } else {
            holder.setVisible(R.id.img_message_type, true);
            holder.setVisible(R.id.img_message_check, false);
        }

    }

    private void setImgviewtoText(TextView textView, int imgId) {
        Drawable drawable = context.getResources().getDrawable(imgId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight()); //设置边界
        textView.setCompoundDrawables(null, null, drawable, null);//画在右边
    }
}
