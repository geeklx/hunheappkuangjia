package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.MineTaskBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/24
 */

public interface MineTaskView extends BaseView {

    void listCourseSuccess(List<MineTaskBean.RecordsBean> list);

    void listCourseEmpty();

    void listCourseError(String msg);

    void validatetime(Object url, String AnsweringState);
}
