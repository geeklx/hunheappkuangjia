package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.OrderDetail;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.mvp.view.OrderDetailView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.AccountService;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;


/**
 * 订单详情
 */
public class OrderDetailPresenter  extends BasePresenter<OrderDetailView> {

    public void getDetail(int orderId){
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("orderId", String.valueOf(orderId));
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(CourseService.class)
                .getOrderDetails(requestBody)
                .compose(TransformUtils.<ResultBean<OrderDetail>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<OrderDetail>(new SubscriberOnNextListener<OrderDetail>() {

                    @Override
                    public void onNext(OrderDetail o) {
                        getView().onOrderInfo(o);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onOrderError(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);
    }
    /**
     * 支付的接口
     */

    public void toBuyIndent(int orderId, String payType) {
        Map<String, String> map = new HashMap<>();
        map.put("payType", payType);
        map.put("orderId", String.valueOf(orderId));
        map.put("deviceType", "0");//0 安卓手机
        String jsonPay = new Gson().toJson(map);//要传递的json
        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
        RestApi.getInstance()
                .createNew(CourseService.class)
                .getOrderPayInfo(requestBodyPay)
                .compose(TransformUtils.<ResultBean<PayInfoBean>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<PayInfoBean>() {
                    @Override
                    public void onNext(PayInfoBean payInfoBean) {
                        getView().getPayInfoSuccess(payInfoBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onOrderError(msg);
                    }
                }, mActivity, false));
    }

    /**
     * 取消订单
     */

    public void toCancelIndent(int orderId) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("orderId", String.valueOf(orderId));
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .cancelIndent(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        getView().cancelSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onOrderError(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }


    /**
     * 取消退课
     */

    public void toCancelCourse(int orderId) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("orderId", String.valueOf(orderId));
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .cancelCourse(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        getView().cancelSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onOrderError(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }
}
