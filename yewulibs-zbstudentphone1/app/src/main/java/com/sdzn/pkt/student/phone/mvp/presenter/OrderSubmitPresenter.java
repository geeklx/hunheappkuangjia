package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.exception.RetryWhenNetworkException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.OrderInfoBean;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;
import com.sdzn.pkt.student.phone.mvp.view.OrderSubmitView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;

/**
 *
 */
public class OrderSubmitPresenter extends BasePresenter<OrderSubmitView> {

    /**
     * 查询购物车
     */
    public void queryShoppingCart(String shopCartId) {
        Map<String, String> map = new HashMap<>();
        map.put("id", shopCartId);
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .queryShoppingCart(requestBody)
                .compose(TransformUtils.<ResultBean<ShoppingCartBean>>defaultSchedulers())
                .map(new ResponseNewFunc<ShoppingCartBean>())
                .retryWhen(new RetryWhenNetworkException())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<ShoppingCartBean>() {


                    @Override
                    public void onNext(ShoppingCartBean shoppingCartBeen) {
                        if (shoppingCartBeen.getShopcartList() == null || shoppingCartBeen.getShopcartList().isEmpty()) {
//                            getView().queryCartEmpty();
                        } else {
                            getView().queryCartSuccess(shoppingCartBeen);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
//                        getView().queryCartFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    public void submitOrder(Map<String, String> params,String payType) {
        String json = new Gson().toJson(params);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        RestApi.getInstance()
                .createNew(CourseService.class)
                .submitOrder(requestBody)
                .compose(TransformUtils.<ResultBean<OrderInfoBean>>allIo())
                .map(new ResponseNewFunc<>())
                .concatMap((new Func1<OrderInfoBean, Observable<ResultBean<PayInfoBean>>>() {
                    @Override
                    public Observable<ResultBean<PayInfoBean>> call(OrderInfoBean orderInfoBean) {

                        getView().submitOrderSuccess();

                        Map<String, String> map = new HashMap<>();
                        map.put("payType", payType);
                        map.put("orderId", orderInfoBean.getOrderId());
                        map.put("deviceType", "0");//0 安卓手机
                        String jsonPay = new Gson().toJson(map);//要传递的json
                        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
                        return RestApi.getInstance()
                                .createNew(CourseService.class)
                                .getOrderPayInfo(requestBodyPay);
                    } // 嵌套请求

                }))
                .compose(TransformUtils.<ResultBean<PayInfoBean>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<PayInfoBean>() {
                    @Override
                    public void onNext(PayInfoBean payInfoBean) {
                        getView().getPayInfoSuccess(payInfoBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getPayInfoFailure(msg);
                    }
                }, mActivity, false));
    }

}
