package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.MessageDetailBean;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.event.MsgCountEvent;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.view.MessageDetailView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.AccountService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import org.greenrobot.eventbus.EventBus;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/21
 */

public class MessageDetailPresenter extends BasePresenter<MessageDetailView> {

    public void getMessageDetail(String id) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .setMessageDetail(id, "1")
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        getView().getMessageSuccess();
                    }
                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    public void getUnReadMessageCount() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryUnReadMsgCount("0", SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        Double obj = (Double) o;
                        EventBus.getDefault().post(new MsgCountEvent(obj.intValue()));
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
