package com.sdzn.pkt.student.phone.widget.bannerview.holder;


public interface MZHolderCreator<VH extends MZViewHolder> {
    /**
     * 创建ViewHolder
     *
     * @return
     */
    public VH createViewHolder();
}
