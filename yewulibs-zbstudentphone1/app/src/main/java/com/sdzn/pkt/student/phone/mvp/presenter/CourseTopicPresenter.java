package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.bean.TopicBean;
import com.sdzn.pkt.student.phone.mvp.view.CourseTopicView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.util.Map;

import rx.Subscription;

/**
 * 专题课程列表
 */

public class CourseTopicPresenter extends BasePresenter<CourseTopicView> {

    public void getTopicTitle() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTopicSpell(1)
                .compose(TransformUtils.<ResultBean<TopicBean>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<TopicBean>() {
                    @Override
                    public void onNext(TopicBean topicBean) {
                        if (topicBean!=null&&topicBean.getRecords().size()>0) {
                            getView().onTopicSuccess(topicBean.getRecords());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onTopicFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
