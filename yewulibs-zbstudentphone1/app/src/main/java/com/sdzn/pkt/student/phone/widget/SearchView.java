package com.sdzn.pkt.student.phone.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.res.ResourcesCompat;

import com.sdzn.pkt.student.phone.R;

/**
 * 描述：
 * - 搜索edittext
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SearchView extends AppCompatEditText implements View.OnFocusChangeListener {

    private Context mContext;
    private boolean hasFoucs;
    private onFocusChangedListener onFocusChangedListener;
    private float translationX;
    private float bodyWidth;
    private Drawable mClearDrawable;

    public SearchView(Context context) {
        this(context, null, 0);
    }

    public SearchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public SearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }

    private void init() {
        mClearDrawable = getCompoundDrawables()[2];
        if (mClearDrawable == null) {
            mClearDrawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_clear, null);
        }

        mClearDrawable.setBounds(0, 0, mClearDrawable.getIntrinsicWidth(), mClearDrawable.getIntrinsicHeight());
        calculateTrans();
        setClearIconVisible(false);
        setOnFocusChangeListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!hasFoucs) {
            if (bodyWidth > getWidth()) {
                bodyWidth = getWidth();
            }
            translationX = (getWidth() - bodyWidth) / 2 - getPaddingLeft();
            canvas.translate(translationX, 0);
        }
        super.onDraw(canvas);
    }

    private void calculateTrans() {
        Drawable[] drawables = getCompoundDrawables();
        Drawable drawableLeft = drawables[0];
        final float textWidth = getPaint().measureText(getHint().toString());
        final int drawablePadding = getCompoundDrawablePadding();
        if (drawableLeft != null) {
            int drawableWidth = drawableLeft.getIntrinsicWidth();
            bodyWidth = textWidth + drawableWidth + drawablePadding;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (getCompoundDrawables()[2] != null) {
                boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight())
                        && (event.getX() < ((getWidth() - getPaddingRight())));

                if (touchable) {
                    this.setText("");
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        this.hasFoucs = hasFocus;
        if (onFocusChangedListener != null) {
            onFocusChangedListener.onFocusChanged(v, hasFocus);
        }
        if (hasFocus) {
            setClearIconVisible(getText().length() > 0);
        } else {
            setClearIconVisible(false);
        }
    }

    /**
     * 设置清除图标的显示与隐藏，调用setCompoundDrawables为EditText绘制上去
     *
     * @param visible
     */
    public void setClearIconVisible(boolean visible) {
        if (hasFoucs) {
            Drawable right = visible ? mClearDrawable : null;
            setCompoundDrawables(getCompoundDrawables()[0],
                    getCompoundDrawables()[1], right, getCompoundDrawables()[3]);
        }
    }


    public void setOnFocusChangedListener(onFocusChangedListener onFocusChangedListener) {
        this.onFocusChangedListener = onFocusChangedListener;
    }

    public interface onFocusChangedListener {
        void onFocusChanged(View v, boolean hasFocus);
    }

}