package com.sdzn.pkt.student.phone.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.MineCourseFragmentPagerAdapter;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.event.UpdateAvatarEvent;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.widget.EmptySchoolLayout;
import com.sdzn.pkt.student.phone.widget.NoScrollViewPager;
import com.sdzn.pkt.student.phone.widget.RoundRectImageView;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 描述：
 * - 我的页面
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class MineFragment extends BaseFragment {

    @BindView(R.id.empty_layout_mine)
    EmptySchoolLayout emptySchoolLayout;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.tabl_course)
    TabLayout tabLayout;
    @BindView(R.id.vp_course)
    NoScrollViewPager vpCourse;
    @BindView(R.id.img_avatar)
    RoundRectImageView imgAvatar;
    @BindView(R.id.view)
    View views;

    private MineCourseFragmentPagerAdapter fragmentAdapter;
    private List<CoursePageFragment> listFragment;//定义要装fragment的列表
    private OnFragmentInteractionListener mListener;

    public MineFragment() {
        // Required empty public constructor
    }

    public static MineFragment newInstance() {
        return new MineFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        initView();
        initData();
    }

    private void initData() {
//        userBean = SPManager.getUser();
//        GlideImgManager.loadImage(mContext, "" + userBean.getPicImg(), R.mipmap.ic_avatar, R.mipmap.ic_avatar, imgAvatar);

    }

    @Override
    public void onResume() {
        super.onResume();
        setInitOnResume();
    }

    private void initView() {
        titleBar.getView(R.id.iv_left).setVisibility(View.VISIBLE);
        titleBar.setLeftClickListener(null);
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPManager.autoLogin(mContext)) {
                    IntentController.toLogin(mContext);
                    return;
                }
                IntentController.toShoppingCart(mContext);
            }
        });

        listFragment = new ArrayList<>();
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_TODY));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_RECENTLY));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_COURSE));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_COURSE_OVER));

        fragmentAdapter = new MineCourseFragmentPagerAdapter(getChildFragmentManager(), listFragment, mContext);
        vpCourse.setAdapter(fragmentAdapter);
        vpCourse.setNoScroll(false);
        vpCourse.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(vpCourse);

    }

//    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.onFragmentInteraction();
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setInitOnResume() {
        if (!SPManager.autoLogin(mContext)) {
            views.setVisibility(View.VISIBLE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
            emptySchoolLayout.setErrorMessage(getString(R.string.error_view_load_mine));
        } else {
            emptySchoolLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
            views.setVisibility(View.GONE);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            initData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            setInitOnResume();
        }
    }
}
