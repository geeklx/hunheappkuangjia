package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.PlayLogInfoBean;

/**
 * @author Reisen at 2018-05-17
 */

public interface VideoCoursePlayerView extends BaseView {
    void addLogSuccess(int id);

    void getPlayLogInfoSuccess(PlayLogInfoBean bean);
}
