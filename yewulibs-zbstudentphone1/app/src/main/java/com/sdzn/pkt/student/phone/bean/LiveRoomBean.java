package com.sdzn.pkt.student.phone.bean;

/**
 * 描述：直播间的实体类
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/4
 */

public class LiveRoomBean {

    /**
     * channel : {"cid":"558a83c75ae74915afa679776b42a2d7","ctime":0,"hlsPullUrl":"http://pullhls4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7/playlist.m3u8","httpPullUrl":"http://flv4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7.flv?netease=flv4623a6e2.live.126.net","id":9,"kpointId":662,"name":"拼课堂-高中一年级暑期数学辅导第一节频道","pushUrl":"rtmp://p4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7?wsSecret=8a3696879381a451ecc2fa0a6e022a5d&wsTime=1501738479","rtmpPullUrl":"rtmp://v4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7"}
     * chatroom : {"announcement":"拼课堂-高中一年级暑期数学辅导","broadcasturl":"","createTime":1501738486000,"ext":"","id":18,"kpointId":662,"name":"拼课堂-高中一年级暑期数学辅导第一节聊天室","onlineusercount":1,"roomId":10383476,"status":"0"}
     */

    private ChannelBean channel;
    private ChatroomBean chatroom;
    private String shareUrl;

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public ChannelBean getChannel() {
        return channel;
    }

    public void setChannel(ChannelBean channel) {
        this.channel = channel;
    }

    public ChatroomBean getChatroom() {
        return chatroom;
    }

    public void setChatroom(ChatroomBean chatroom) {
        this.chatroom = chatroom;
    }

    public static class ChannelBean {
        /**
         * cid : 558a83c75ae74915afa679776b42a2d7
         * ctime : 0
         * hlsPullUrl : http://pullhls4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7/playlist.m3u8
         * httpPullUrl : http://flv4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7.flv?netease=flv4623a6e2.live.126.net
         * id : 9
         * kpointId : 662
         * name : 拼课堂-高中一年级暑期数学辅导第一节频道
         * pushUrl : rtmp://p4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7?wsSecret=8a3696879381a451ecc2fa0a6e022a5d&wsTime=1501738479
         * rtmpPullUrl : rtmp://v4623a6e2.live.126.net/live/558a83c75ae74915afa679776b42a2d7
         */

        private String cid;
        private int ctime;
        private String hlsPullUrl;
        private String httpPullUrl;
        private int id;
        private int kpointId;
        private String name;
        private String pushUrl;
        private String rtmpPullUrl;

        public String getCid() {
            return cid;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        public int getCtime() {
            return ctime;
        }

        public void setCtime(int ctime) {
            this.ctime = ctime;
        }

        public String getHlsPullUrl() {
            return hlsPullUrl;
        }

        public void setHlsPullUrl(String hlsPullUrl) {
            this.hlsPullUrl = hlsPullUrl;
        }

        public String getHttpPullUrl() {
            return httpPullUrl;
        }

        public void setHttpPullUrl(String httpPullUrl) {
            this.httpPullUrl = httpPullUrl;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getKpointId() {
            return kpointId;
        }

        public void setKpointId(int kpointId) {
            this.kpointId = kpointId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPushUrl() {
            return pushUrl;
        }

        public void setPushUrl(String pushUrl) {
            this.pushUrl = pushUrl;
        }

        public String getRtmpPullUrl() {
            return rtmpPullUrl;
        }

        public void setRtmpPullUrl(String rtmpPullUrl) {
            this.rtmpPullUrl = rtmpPullUrl;
        }
    }

    public static class ChatroomBean {
        /**
         * announcement : 拼课堂-高中一年级暑期数学辅导
         * broadcasturl :
         * createTime : 1501738486000
         * ext :
         * id : 18
         * kpointId : 662
         * name : 拼课堂-高中一年级暑期数学辅导第一节聊天室
         * onlineusercount : 1
         * roomId : 10383476
         * status : 0
         */

        private String announcement;
        private String broadcasturl;
        private long createTime;
        private String ext;
        private int id;
        private int kpointId;
        private String name;
        private int onlineusercount;
        private int roomId;
        private String status;

        public String getAnnouncement() {
            return announcement;
        }

        public void setAnnouncement(String announcement) {
            this.announcement = announcement;
        }

        public String getBroadcasturl() {
            return broadcasturl;
        }

        public void setBroadcasturl(String broadcasturl) {
            this.broadcasturl = broadcasturl;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getExt() {
            return ext;
        }

        public void setExt(String ext) {
            this.ext = ext;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getKpointId() {
            return kpointId;
        }

        public void setKpointId(int kpointId) {
            this.kpointId = kpointId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getOnlineusercount() {
            return onlineusercount;
        }

        public void setOnlineusercount(int onlineusercount) {
            this.onlineusercount = onlineusercount;
        }

        public int getRoomId() {
            return roomId;
        }

        public void setRoomId(int roomId) {
            this.roomId = roomId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
