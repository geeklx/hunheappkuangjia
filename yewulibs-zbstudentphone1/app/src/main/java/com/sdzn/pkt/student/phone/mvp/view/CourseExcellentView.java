package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.CourseList;

import java.util.List;

/**
 * 精品课列表
 */
public interface CourseExcellentView extends BaseView {

    void getDataFailure(String msg);


    void getDataCourse(List<CourseList> recommendCourses);

}
