package com.sdzn.pkt.student.phone.network.subscriber;

import android.content.Context;

import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.exception.CodeException;
import com.sdzn.core.network.exception.HttpTimeException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.subscriber.ProgressSubscriber;
import com.sdzn.core.utils.AppManager;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;

/**
 * 登录处抛异常的处理  znxt
 */
public class LoginProgressSubscriber<T> extends ProgressSubscriber<T> {


    public LoginProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context) {
        super(mSubscriberOnNextListener, context);
    }

    public LoginProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress) {
        super(mSubscriberOnNextListener, context, isShowProgress);
    }

    public LoginProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress, String showMsg) {
        super(mSubscriberOnNextListener, context, isShowProgress, showMsg);
    }

    public LoginProgressSubscriber(SubscriberOnNextListener<T> subscriberOnNextListener) {
        super(subscriberOnNextListener);
    }

    @Override
    public void onError(Throwable e) {
        if (isShowProgress) {
            dismissProgressDialog();
        }
        if (e instanceof ApiException) {
//            ApiException exception = (ApiException) e;
//            if (NetExceptionCode.TOKEN_NONE == exception.getCode()
//                    || NetExceptionCode.TOKEN_NULL == exception.getCode()) {//token异常重新登陆
//                reLogin();
//                return;
//            }
            mSubscriberOnNextListener.onFail(e);
        } else if (e instanceof HttpTimeException) {
            HttpTimeException exception = (HttpTimeException) e;
            mSubscriberOnNextListener.onFail(new ApiException(exception, CodeException.RUNTIME_ERROR));
        } else {
//            mSubscriberOnNextListener.onFail(new ApiException(e, CodeException.UNKNOWN_ERROR));
        }

    }

    private void reLogin() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(context, false);
        IntentController.toMain(context, false);
    }

    @Override
    public void onNext(T t) {
        mSubscriberOnNextListener.onNext(t);
    }

}