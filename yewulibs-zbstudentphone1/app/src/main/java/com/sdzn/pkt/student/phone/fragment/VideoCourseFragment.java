package com.sdzn.pkt.student.phone.fragment;


import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.sdzn.core.base.BaseFragment;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import butterknife.BindView;

/**
 * 描述：
 * - 点播视频观看
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class VideoCourseFragment extends BaseFragment {


    @BindView(R.id.title_bar)
    TitleBar titleBar;

    public VideoCourseFragment() {
        // Required empty public constructor
    }

    public static VideoCourseFragment newInstance() {
        return new VideoCourseFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_video;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentController.toSearch(mContext, CourseCons.Type.VIDEO);
            }
        });
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentController.toShoppingCart(mContext);
            }
        });

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_live_container, CourseFragment.newInstance(CourseCons.Type.VIDEO, null)).commit();
    }


}
