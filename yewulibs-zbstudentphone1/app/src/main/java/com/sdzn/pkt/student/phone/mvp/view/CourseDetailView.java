package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.CourseDetailBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public interface CourseDetailView extends BaseView {

    void getCourseDetailSuccess(CourseDetailBean courseDetailBean);

    void getCourseDetailFailure(String msg);

}
