package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.LiveRoomBean;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;
import com.sdzn.pkt.student.phone.bean.VideoRoomBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public interface PurchasedCourseDetailView extends BaseView {

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void liveRoomInfoOnError(String msg);

    void addShoppingCartSuccess(boolean isExsits, int cartNum);

    void addShoppingCartFailure(String msg);

    void addFavoriteSuccess();

    void addFavoriteFailure(String msg);

    void delFavoriteSuccess();

    void delFavoriteFailure(String msg);

    void getCartNumSuccess(int cartNum);

    void toSettlement(boolean isExsits,String carIdStr, int cartNum);

    void getVideoRoomInfoSuccrss(NewVideoInfo videoRoomBean);

    void videoRoomInfoOnError(String msg);

    void getReplayInfoSuccess(NewVideoInfo info);

    void applySuccess();
}
