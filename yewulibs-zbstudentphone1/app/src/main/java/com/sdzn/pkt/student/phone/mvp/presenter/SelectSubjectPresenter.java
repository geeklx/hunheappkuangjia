package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.view.SelectSubjectView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import rx.Subscription;

public class SelectSubjectPresenter extends BasePresenter<SelectSubjectView> {
//    public void getGrade() {
//        Subscription subscribe = RestApi.getInstance()
//                .createNew(CourseService.class)
//                .getGradeJson()
//                .compose(TransformUtils.<ResultBean<List<GradeJson>>>defaultSchedulers())
//                .map(new ResponseNewFunc<>())
//                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<GradeJson>>() {
//                    @Override
//                    public void onNext(List<GradeJson> gradeJson) {
//                        if (gradeJson!=null){
//                            getView().onSuccess(gradeJson);
//                        }
//
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        String msg = mActivity.getString(R.string.request_failure_try_again);
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
//                        getView().onFailed(msg);
//                    }
//                }, mActivity, true));
//        addSubscribe(subscribe);
//
//    }


    public void setSubAndGrade(String eduId,String eduName,String levelId,String levelName,String gradeId,String gradeName) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .updateEducaLevel(eduId,eduName,levelId,levelName,gradeId,gradeName)
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        SPManager.saveUser(userBean);
                        SPManager.saveSection(userBean.getSubjectId(), userBean.getSubjectName(),userBean.getEducationId());
                        SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());
                        getView().setSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onFailed(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);

    }


}
