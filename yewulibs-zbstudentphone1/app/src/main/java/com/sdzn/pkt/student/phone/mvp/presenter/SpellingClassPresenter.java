package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.bean.SubjectBean;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.view.SpellingClassView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

public class SpellingClassPresenter extends BasePresenter<SpellingClassView> {

    public void getSubject(){
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSubjectSpell(SPManager.getSectionId())
                .compose(TransformUtils.<ResultBean<List<SubjectBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<SubjectBean>>() {
                    @Override
                    public void onNext(List<SubjectBean> subjectSpellBeanList) {
                        getView().onSubjectSuccess(subjectSpellBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onSubjectFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
