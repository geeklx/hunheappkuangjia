package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.ChangePwdPresenter;
import com.sdzn.pkt.student.phone.mvp.view.ChangePwdView;
import com.sdzn.pkt.student.phone.widget.PwdEditText;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 修改密码-》重置
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class ReSetPasswordActivity extends BaseMVPActivity<ChangePwdView, ChangePwdPresenter> implements ChangePwdView {


    @BindView(R.id.et_new_password)
    PwdEditText etNewPassword;
    @BindView(R.id.et_password_certain)
    PwdEditText etPasswordCertain;
    @BindView(R.id.iv_back)
    ImageView ivback;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reset_password;
    }

    @Override
    protected ChangePwdPresenter createPresenter() {
        return new ChangePwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {

    }


    @OnClick({R.id.btn_certain})
    public void onViewClicked(View view) {
        String newPwd = etNewPassword.getText().toString().trim();
        String confirmPwd = etPasswordCertain.getText().toString().trim();
        mPresenter.
                confirm(newPwd, confirmPwd);
    }

    @OnClick({R.id.iv_back})
    public void onBackClicked(View view) {
        onBackPressed();
    }

    @Override
    public void changeSuccess() {
        SPManager.saveToken("");
        IntentController.toLogin(mContext);
    }

    @Override
    public void changeFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
