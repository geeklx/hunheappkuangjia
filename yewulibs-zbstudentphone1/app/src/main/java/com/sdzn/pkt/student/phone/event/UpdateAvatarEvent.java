package com.sdzn.pkt.student.phone.event;

/**
 * 描述：更新头像
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/12
 */

public class UpdateAvatarEvent {

    private int ui;

    public UpdateAvatarEvent(int ui) {
        this.ui = ui;
    }

    public int getUi() {
        return ui;
    }
}
