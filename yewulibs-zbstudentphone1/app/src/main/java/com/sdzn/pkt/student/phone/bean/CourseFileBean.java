package com.sdzn.pkt.student.phone.bean;

import com.sdzn.pkt.student.phone.annotation.DownloadState;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Unique;

import java.io.Serializable;

/**
 * @author Reisen at 2018-05-28
 */
@Entity
public class CourseFileBean implements Serializable {
    private static final long serialVersionUID = -6480828656571242418L;

    private int courseId;//课程id
    @Transient
    private String courseName;//课程名称
    private String createTime;//创建时间
    private int downloadCount;//下载数
    private String filePath;//路径
    private int fileType;//类型
    private String fileTypeName;//类型
    @Id
    @Unique
    private long id;//
    private long kpointId;//章节id
    @Transient
    private String kpointName;//章节名
    private String srcFileName;//文件名

    private String updater;//上传人

    //下载需要进度和状态标记
    private float progress;
    @DownloadState
    private int downloadState;

    @Generated(hash = 1703838036)
    public CourseFileBean(int courseId, String createTime, int downloadCount, String filePath,
            int fileType, String fileTypeName, long id, long kpointId, String srcFileName,
            String updater, float progress, int downloadState) {
        this.courseId = courseId;
        this.createTime = createTime;
        this.downloadCount = downloadCount;
        this.filePath = filePath;
        this.fileType = fileType;
        this.fileTypeName = fileTypeName;
        this.id = id;
        this.kpointId = kpointId;
        this.srcFileName = srcFileName;
        this.updater = updater;
        this.progress = progress;
        this.downloadState = downloadState;
    }

    @Generated(hash = 197906114)
    public CourseFileBean() {
    }

    public int getCourseId() {
        return this.courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return this.courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getDownloadCount() {
        return this.downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getFileType() {
        return this.fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return this.fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getKpointId() {
        return this.kpointId;
    }

    public void setKpointId(long kpointId) {
        this.kpointId = kpointId;
    }

    public String getKpointName() {
        return this.kpointName;
    }

    public void setKpointName(String kpointName) {
        this.kpointName = kpointName;
    }

    public String getSrcFileName() {
        return this.srcFileName;
    }

    public void setSrcFileName(String srcFileName) {
        this.srcFileName = srcFileName;
    }

    public String getUpdater() {
        return this.updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public float getProgress() {
        return this.progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    @DownloadState
    public int getDownloadState() {
        return this.downloadState;
    }

    public void setDownloadState(@DownloadState int downloadState) {
        this.downloadState = downloadState;
    }
}
