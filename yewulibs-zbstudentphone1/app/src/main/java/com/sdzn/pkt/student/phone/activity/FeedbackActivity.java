package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;
import rx.Subscription;

/**
 *意见反馈
 */

public class FeedbackActivity extends BaseActivity {
    @BindView(R.id.et_content)
    EditText etContent;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.btn_certain)
    Button button;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_feed_back;
    }

    @Override
    protected void onInit(Bundle bundle) {
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.statusBarLightMode(this);
    }

    @OnClick({R.id.btn_certain})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_certain:
                setData();
                break;

                default:

                    break;
        }
    }
    private void setData(){
        Map<String, String> map = new HashMap<>();
        if (etPhone.getText().toString().trim().isEmpty()){
            map.put("phone", "0");
        }else {
            map.put("phone", etPhone.getText().toString());
        }
        map.put("opinionContent", etContent.getText().toString());
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getOpinion(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<Object>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object courses) {
                      ToastUtils.showShort("提交成功");
                      FeedbackActivity.this.finish();
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        ToastUtils.showShort("" + e.getMessage());

                    }
                }, mContext, false));
    }


}
