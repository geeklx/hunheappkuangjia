package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.NoteBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class NotesAdapter extends BaseRcvAdapter<NoteBean.CourseNoteAllListBean> {
    public NotesAdapter(Context context, List<NoteBean.CourseNoteAllListBean> mList) {
        super(context, R.layout.item_notes, mList);
    }


    @Override
    public void convert(BaseViewHolder holder, int position, NoteBean.CourseNoteAllListBean noteBean) {
        NoteBean.CourseNoteAllListBean bean = noteBean;
        ImageView imageView = holder.getView(R.id.checkbox_note);

        holder.setText(R.id.tv_course_name, bean.getCourseName());
        String updateTime = TimeUtils.millis2String(bean.getUpdateTime(), "yyyy.MM.dd HH:mm");
        holder.setText(R.id.tv_date, updateTime);
        holder.setText(R.id.tv_note_title, bean.getKpointName());
        holder.setText(R.id.tv_note_generalize, bean.getContent());
        if (isEdit) {
            imageView.setSelected(bean.isSelect());
        } else {
            imageView.setSelected(true);
        }

    }
}
