package com.sdzn.pkt.student.phone.bean;

/**
 * @author Reisen at 2018-01-26
 */

public class UnReadMsgCountBean {
    private double unreadMsgCount;

    public double getUnreadMsgCount() {
        return unreadMsgCount;
    }

    public void setUnreadMsgCount(double unreadMsgCount) {
        this.unreadMsgCount = unreadMsgCount;
    }
}
