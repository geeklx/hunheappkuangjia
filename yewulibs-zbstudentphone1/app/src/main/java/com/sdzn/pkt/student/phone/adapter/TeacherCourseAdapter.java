package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import androidx.core.content.ContextCompat;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.TeacherCourseBean;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.utils.PriceUtil;

import java.util.List;

/**
 * 描述：
 * - 直播点播课程列表adpater
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class TeacherCourseAdapter extends BaseRcvAdapter<TeacherCourseBean.TeacherCourseList> {
    private int courseType;

    public TeacherCourseAdapter(Context context, List<TeacherCourseBean.TeacherCourseList> mList,
                                @CourseCons.Type.CourseType int courseType) {
        super(context, R.layout.item_course, mList);
        this.courseType = courseType;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, TeacherCourseBean.TeacherCourseList courseListBean) {
        holder.setImageView(R.id.iv_cover, "" + courseListBean.getLogo());
        holder.setText(R.id.tv_course_title, courseListBean.getCourseName());
        boolean free = PriceUtil.isFree(courseListBean.getCurrentPrice());
        if (free) {
            holder.setText(R.id.tv_course_price, R.string.free);
            holder.setTextSize(R.id.tv_course_price, 14);
            holder.setTextColorRes(R.id.tv_course_price, R.color.free_green);
        } else {
            holder.setText(R.id.tv_course_price, "￥" + courseListBean.getCurrentPrice());
            holder.setTextSize(R.id.tv_course_price, 16);
            holder.setTextColorRes(R.id.tv_course_price, R.color.red);
        }
        if (courseType == CourseCons.Type.VIDEO) {
            holder.setText(R.id.tv_status, "点播");
            StringBuilder courseDesc = new StringBuilder();
            if ("PACKAGE".equals(courseListBean.getSellType())) {
                courseDesc.append("科目：").append(courseListBean.getSubjectNames());
            } else {
                courseDesc.append("讲师：");
                for (TeacherCourseBean.TeacherList teacherListBean : courseListBean.getTeacherList()) {
                    courseDesc.append(teacherListBean.getName()).append("、");
                }
                if (!courseListBean.getTeacherList().isEmpty()) {
                    courseDesc.deleteCharAt(courseDesc.lastIndexOf("、"));
                }
            }
            holder.setText(R.id.tv_start_time, courseDesc.toString());
        } else {
            holder.setText(R.id.tv_status, courseListBean.getState());
            if ("报名截止".equals(courseListBean.getState())) {
                holder.setTextColor(R.id.tv_status, ContextCompat.getColor(context, R.color.textMinor));
            } else {
                holder.setTextColor(R.id.tv_status, ContextCompat.getColor(context, R.color.colorPrimary));
            }
            String beginTime = TimeUtils.millis2String(courseListBean.getLiveBeginTime(), "yyyy.MM.dd");
            String endTime = TimeUtils.millis2String(courseListBean.getLiveEndTime(), "yyyy.MM.dd");
            holder.setText(R.id.tv_start_time, beginTime + " - " + endTime);
        }

        if ("PACKAGE".equals(courseListBean.getSellType())) {
            holder.setText(R.id.tv_type, "组合");
        } else {
            holder.setText(R.id.tv_type, "单科");
        }
    }
}
