package com.sdzn.pkt.student.phone.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Reisen at 2018-07-19
 */
public class TeacherCourseBean implements Serializable {
    private static final long serialVersionUID = -626008459680565186L;
    private int totalPageSize;
    private int totalResultSize;
    private ArrayList<TeacherCourseList> courseList;

    public int getTotalPageSize() {
        return totalPageSize;
    }

    public void setTotalPageSize(int totalPageSize) {
        this.totalPageSize = totalPageSize;
    }

    public int getTotalResultSize() {
        return totalResultSize;
    }

    public void setTotalResultSize(int totalResultSize) {
        this.totalResultSize = totalResultSize;
    }

    public ArrayList<TeacherCourseList> getCourseList() {
        return courseList;
    }

    public void setCourseList(ArrayList<TeacherCourseList> courseList) {
        this.courseList = courseList;
    }

    public static class TeacherCourseList implements Serializable {
        private static final long serialVersionUID = -3108073542889918640L;
        private long addTime;
        private int auditionLessionCount;
        private int classId;
        private int courseId;
        private String courseName;
        private int courseTerm;
        private double currentPrice;
        private int customerId;
        private int grade;
        private int isavaliable;
        private int lessionNum;
        private LevelSubject levelSubject;
        private long liveBeginTime;
        private int liveCourseId;
        private long liveEndTime;
        private int livingKpointCount;
        private String logo;
        private String loseTime;
        private int loseType;
        private int pageBuycount;
        private int pageViewcount;
        private int recommendId;
        private long refundEndTime;
        private String sellType;
        private long signEndTime;
        private String subjectNames;
        private String state;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getSubjectNames() {
            return subjectNames;
        }

        public void setSubjectNames(String subjectNames) {
            this.subjectNames = subjectNames;
        }

        private int sourcePrice;
        private Object sqlMap;
        private int subjectId;
        private String subjectName;
        private SysCustomer sysCustomer;
        private List<TeacherList> teacherList;
        private String title;
        private long updateTime;
        private int userId;
        private int userType;
        private int packageType;

        public int getPackageType() {
            return packageType;
        }

        public void setPackageType(int packageType) {
            this.packageType = packageType;
        }

        public void setAddTime(long addTime) {
            this.addTime = addTime;
        }

        public long getAddTime() {
            return addTime;
        }

        public void setAuditionLessionCount(int auditionLessionCount) {
            this.auditionLessionCount = auditionLessionCount;
        }

        public int getAuditionLessionCount() {
            return auditionLessionCount;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public int getClassId() {
            return classId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public int getCourseId() {
            return courseId;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseTerm(int courseTerm) {
            this.courseTerm = courseTerm;
        }

        public int getCourseTerm() {
            return courseTerm;
        }

        public void setCurrentPrice(double currentPrice) {
            this.currentPrice = currentPrice;
        }

        public double getCurrentPrice() {
            return currentPrice;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setGrade(int grade) {
            this.grade = grade;
        }

        public int getGrade() {
            return grade;
        }

        public void setIsavaliable(int isavaliable) {
            this.isavaliable = isavaliable;
        }

        public int getIsavaliable() {
            return isavaliable;
        }

        public void setLessionNum(int lessionNum) {
            this.lessionNum = lessionNum;
        }

        public int getLessionNum() {
            return lessionNum;
        }

        public void setLevelSubject(LevelSubject levelSubject) {
            this.levelSubject = levelSubject;
        }

        public LevelSubject getLevelSubject() {
            return levelSubject;
        }

        public void setLiveBeginTime(long liveBeginTime) {
            this.liveBeginTime = liveBeginTime;
        }

        public long getLiveBeginTime() {
            return liveBeginTime;
        }

        public void setLiveCourseId(int liveCourseId) {
            this.liveCourseId = liveCourseId;
        }

        public int getLiveCourseId() {
            return liveCourseId;
        }

        public void setLiveEndTime(long liveEndTime) {
            this.liveEndTime = liveEndTime;
        }

        public long getLiveEndTime() {
            return liveEndTime;
        }

        public void setLivingKpointCount(int livingKpointCount) {
            this.livingKpointCount = livingKpointCount;
        }

        public int getLivingKpointCount() {
            return livingKpointCount;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getLogo() {
            return logo;
        }

        public void setLoseTime(String loseTime) {
            this.loseTime = loseTime;
        }

        public String getLoseTime() {
            return loseTime;
        }

        public void setLoseType(int loseType) {
            this.loseType = loseType;
        }

        public int getLoseType() {
            return loseType;
        }

        public void setPageBuycount(int pageBuycount) {
            this.pageBuycount = pageBuycount;
        }

        public int getPageBuycount() {
            return pageBuycount;
        }

        public void setPageViewcount(int pageViewcount) {
            this.pageViewcount = pageViewcount;
        }

        public int getPageViewcount() {
            return pageViewcount;
        }

        public void setRecommendId(int recommendId) {
            this.recommendId = recommendId;
        }

        public int getRecommendId() {
            return recommendId;
        }

        public void setRefundEndTime(long refundEndTime) {
            this.refundEndTime = refundEndTime;
        }

        public long getRefundEndTime() {
            return refundEndTime;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSignEndTime(long signEndTime) {
            this.signEndTime = signEndTime;
        }

        public long getSignEndTime() {
            return signEndTime;
        }

        public void setSourcePrice(int sourcePrice) {
            this.sourcePrice = sourcePrice;
        }

        public int getSourcePrice() {
            return sourcePrice;
        }

        public void setSqlMap(Object sqlMap) {
            this.sqlMap = sqlMap;
        }

        public Object getSqlMap() {
            return sqlMap;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSysCustomer(SysCustomer sysCustomer) {
            this.sysCustomer = sysCustomer;
        }

        public SysCustomer getSysCustomer() {
            return sysCustomer;
        }

        public void setTeacherList(List<TeacherList> teacherList) {
            this.teacherList = teacherList;
        }

        public List<TeacherList> getTeacherList() {
            return teacherList;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public int getUserType() {
            return userType;
        }
    }

    public static class LevelSubject implements Serializable {

        private static final long serialVersionUID = -3126946602265973289L;
        private long createTime;
        private String logo;
        private int parentId;
        private int sort;
        private int status;
        private int subjectId;
        private String subjectName;
        private String type;

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getLogo() {
            return logo;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getParentId() {
            return parentId;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public int getSort() {
            return sort;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

    }

    public static class SysCustomer implements Serializable {

        private static final long serialVersionUID = 4339613392684460832L;
        private long cityId;
        private String cityName;
        private String contactWay;
        private String contacts;
        private long createTime;
        private int createUser;
        private String customerName;
        private String domainName;
        private int id;
        private boolean isInvalid;
        private int orgId;
        private long provienceId;
        private String provienceName;
        private Object sqlMap;
        private int staffId;
        private long townId;
        private String townName;

        public void setCityId(long cityId) {
            this.cityId = cityId;
        }

        public long getCityId() {
            return cityId;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getCityName() {
            return cityName;
        }

        public void setContactWay(String contactWay) {
            this.contactWay = contactWay;
        }

        public String getContactWay() {
            return contactWay;
        }

        public void setContacts(String contacts) {
            this.contacts = contacts;
        }

        public String getContacts() {
            return contacts;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public int getCreateUser() {
            return createUser;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setDomainName(String domainName) {
            this.domainName = domainName;
        }

        public String getDomainName() {
            return domainName;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setIsInvalid(boolean isInvalid) {
            this.isInvalid = isInvalid;
        }

        public boolean getIsInvalid() {
            return isInvalid;
        }

        public void setOrgId(int orgId) {
            this.orgId = orgId;
        }

        public int getOrgId() {
            return orgId;
        }

        public void setProvienceId(long provienceId) {
            this.provienceId = provienceId;
        }

        public long getProvienceId() {
            return provienceId;
        }

        public void setProvienceName(String provienceName) {
            this.provienceName = provienceName;
        }

        public String getProvienceName() {
            return provienceName;
        }

        public void setSqlMap(Object sqlMap) {
            this.sqlMap = sqlMap;
        }

        public Object getSqlMap() {
            return sqlMap;
        }

        public void setStaffId(int staffId) {
            this.staffId = staffId;
        }

        public int getStaffId() {
            return staffId;
        }

        public void setTownId(long townId) {
            this.townId = townId;
        }

        public long getTownId() {
            return townId;
        }

        public void setTownName(String townName) {
            this.townName = townName;
        }

        public String getTownName() {
            return townName;
        }

    }

    public static class TeacherList implements Serializable {

        private static final long serialVersionUID = -9177025838068857939L;
        private String picPath;
        private String education;
        private String name;
        private int id;

        public void setPicPath(String picPath) {
            this.picPath = picPath;
        }

        public String getPicPath() {
            return picPath;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getEducation() {
            return education;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

    }
}
