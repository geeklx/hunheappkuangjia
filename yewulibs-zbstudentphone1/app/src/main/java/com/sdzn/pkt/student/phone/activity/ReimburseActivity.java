package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.event.OrderPayEvent;
import com.sdzn.pkt.student.phone.mvp.presenter.ReimbursePresenter;
import com.sdzn.pkt.student.phone.mvp.view.ReimburseView;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：退课申请界面
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/7
 */

public class ReimburseActivity extends BaseMVPActivity<ReimburseView, ReimbursePresenter> implements ReimburseView {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.et_reason)
    EditText etReason;
    @BindView(R.id.bt_confirm)
    Button btConfirm;
    @BindView(R.id.ll_reason)
    LinearLayout llReason;
    private int orderId;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reimburse;
    }

    @Override
    protected void onInit(Bundle bundle) {
        orderId = getIntent().getIntExtra("orderId", -1);

    }


    @OnClick({R.id.bt_write, R.id.bt_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_write:
                llReason.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_confirm:
                String reason = etReason.getText().toString().trim();
                if (TextUtils.isEmpty(reason)) {

                    ToastUtils.showShort("请填写退款理由");
                    return;
                }
                mPresenter.dropOut(orderId, reason);
                break;
        }
    }

    @Override
    protected ReimbursePresenter createPresenter() {
        return new ReimbursePresenter();
    }

    @Override
    public void dropSuccess(Object o) {
            ToastUtils.showShort("已申请，请耐心等待");
            EventBus.getDefault().post(new OrderPayEvent(false));
            finish();
    }

    @Override
    public void dropError(String msg) {
        ToastUtils.showShort(msg);
    }

}
