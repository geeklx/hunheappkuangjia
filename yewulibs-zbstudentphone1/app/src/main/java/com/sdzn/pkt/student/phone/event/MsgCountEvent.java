package com.sdzn.pkt.student.phone.event;

public class MsgCountEvent {
    private int count;

    public MsgCountEvent(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
