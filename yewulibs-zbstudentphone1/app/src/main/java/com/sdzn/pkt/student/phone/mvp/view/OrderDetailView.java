package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.IndentBean;
import com.sdzn.pkt.student.phone.bean.OrderDetail;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;

public interface OrderDetailView extends BaseView {

    void onOrderInfo(OrderDetail orderDetail);

    void onOrderError(String msg);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void cancelSuccess();
}
