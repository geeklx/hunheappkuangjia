package com.sdzn.pkt.student.phone.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/28.
 */
public class Dp2Px {
    private Dp2Px() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static int convert(Context context, float dpVal) {
        return (int)TypedValue.applyDimension(1, dpVal, context.getResources().getDisplayMetrics());
    }
}
