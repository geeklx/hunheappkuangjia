package com.sdzn.pkt.student.phone.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.core.utils.NetworkUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.OrderInfoAdapter;
import com.sdzn.pkt.student.phone.bean.CouponBean;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;
import com.sdzn.pkt.student.phone.event.OrderPayEvent;
import com.sdzn.pkt.student.phone.manager.AlipayManager;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.WXPayManager;
import com.sdzn.pkt.student.phone.manager.constant.PayType;
import com.sdzn.pkt.student.phone.mvp.presenter.OrderSubmitPresenter;
import com.sdzn.pkt.student.phone.mvp.view.OrderSubmitView;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 订单提交
 */
public class OrderSubmitActivity extends BaseMVPActivity<OrderSubmitView, OrderSubmitPresenter>
        implements OrderSubmitView, RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.rcv_course)
    RecyclerView rcvCourse;
    @BindView(R.id.tv_order_amount)
    TextView tvOrderAmount;
    @BindView(R.id.tv_discount_amount)
    TextView tvDiscountAmount;
    @BindView(R.id.tv_actual_payment)
    TextView tvActualPayment;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.rg_payment)
    RadioGroup rgPayment;
    @BindView(R.id.rb_wxpay)
    RadioButton rbWxpay;
    @BindView(R.id.rb_alipay)
    RadioButton rbAlipay;
    @BindView(R.id.tv_coupon)
    TextView tvCoupon;
    @BindView(R.id.btn_certain_pay)
    Button btnCertainPay;
    @BindView(R.id.rl_settlement)
    RelativeLayout rlSettlement;

    private List<CouponBean> couponBeans;
    private List<ShoppingCartBean.ShopCartListBean> orderInfoBeans;
    private OrderInfoAdapter orderInfoAdapter;
    private double totalPrice;
    private double discountPrice;
    private String payType;
    private String couponCode;

    private String shopCarIdStr = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_submit;
    }

    @Override
    protected OrderSubmitPresenter createPresenter() {
        return new OrderSubmitPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initData();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        orderInfoBeans = new ArrayList<>();
        couponBeans = new ArrayList<>();
        payType = PayType.ALIPAY;
        //通过课程详情或购物车传过来的课程数据
        shopCarIdStr = getIntent().getStringExtra("goods");
    }

    private void initView() {
        rgPayment.setOnCheckedChangeListener(this);
        orderInfoAdapter = new OrderInfoAdapter(mContext, orderInfoBeans);
        mPresenter.queryShoppingCart(shopCarIdStr);//获取购物车数据
        rcvCourse.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 1));
        rcvCourse.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rcvCourse.setAdapter(orderInfoAdapter);
        Drawable wxDrawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_share_wx);
        wxDrawable.setBounds(0, 0, ConvertUtils.dp2px(mContext, 44), ConvertUtils.dp2px(mContext, 44));//必须设置图片大小，否则不显示
        rbWxpay.setCompoundDrawables(wxDrawable, null, null, null);

        Drawable aliDrawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_alipay);
        aliDrawable.setBounds(0, 0, ConvertUtils.dp2px(mContext, 44), ConvertUtils.dp2px(mContext, 44));//必须设置图片大小，否则不显示
        rbAlipay.setCompoundDrawables(aliDrawable, null, null, null);


        String discount = String.format(Locale.getDefault(), "￥%.2f", discountPrice);
        tvDiscountAmount.setText(splitString("优惠金额：", discount));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            CouponBean couponBean = data.getParcelableExtra("coupon");
            this.couponCode = couponBean.getCouponCode();
            String orderPrice = String.format(Locale.getDefault(), "￥%.2f", totalPrice);
            tvOrderAmount.setText(splitString("订单金额：", orderPrice));
            discountPrice = couponBean.getAmount();
            String couponPrice = String.format(Locale.getDefault(), "￥%.2f", discountPrice);
            tvDiscountAmount.setText(splitString("优惠金额：", couponPrice));
            tvCoupon.setText(String.format("-%s", couponPrice));
            double price = totalPrice - discountPrice;
            if (price < 0) {
                price = 0;
            }
            String actualPrice = String.format(Locale.getDefault(), "￥%.2f", price);
            tvActualPayment.setText(splitString("实付款：", actualPrice));
        }
    }


    @OnClick({R.id.btn_certain_pay, R.id.tv_coupon})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_certain_pay:
                if (TextUtils.isEmpty(payType)) {
                    ToastUtils.showShort("请选择支付方式后结算");
                    return;
                }
                if (totalPrice - discountPrice <= 0) {
                    ToastUtils.showShort("结算价格不能为负");
                    return;
                }
                if (TextUtils.isEmpty(shopCarIdStr)) {
                    ToastUtils.showShort("数据错误，请重新选择支付课程");
                    return;
                }
                mPresenter.submitOrder(getOrderParams(),payType);
                break;
            case R.id.tv_coupon:
                if (!couponBeans.isEmpty()) {
                    IntentController.toCoupon(mContext, "选择优惠券", couponBeans);
                }
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart(shopCarIdStr);
        if (orderPayEvent.isSuccess()) {
            new SweetAlertDialog.Builder(mContext).setMessage("支付成功")
                    .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                        @Override
                        public void onClick(Dialog dialog, int which) {
                            finish();
                        }
                    }).show();
        } else {
            finish();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_wxpay:
                payType = PayType.WXPAY;
                break;
            case R.id.rb_alipay:
                payType = PayType.ALIPAY;
                break;
            default:
                break;
        }
    }

    private Map<String, String> getOrderParams() {
        Map<String, String> params = new HashMap<>();
//        params.put("reqIp", NetworkUtils.getIPAddress(true));//请求ip地址
//        params.put("orderType", "COURSE");//订单类型（COURSE课程、MEMBER会员、ACCOUNT账户充值）
//        params.put("payType", payType);//支付方式，可选值： ALIPAY,支付宝；WEIXIN,微信
//        if (!TextUtils.isEmpty(couponCode)) {
//            params.put("couponCode", couponCode);//优惠券编码
//        }
        params.put("couponPrice", "0");
        params.put("couponCode", "");
        params.put("ids", shopCarIdStr);
        return params;
    }

    private SpannableString splitString(String firstStr, String secStr) {
        SpannableString spannableString = new SpannableString(firstStr + secStr);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.textPrimary)),
                0, firstStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.red)),
                firstStr.length(), spannableString.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    @Override
    public void queryCartSuccess(ShoppingCartBean shoppingCartBeen) {
        orderInfoBeans.addAll(shoppingCartBeen.getShopcartList());
        this.totalPrice = shoppingCartBeen.getTotalPrice();
        String orderPrice = String.format(Locale.getDefault(), "￥%.2f", totalPrice);
        tvOrderAmount.setText(splitString("订单金额：", orderPrice));
        double price = totalPrice - discountPrice;
        if (price < 0) {
            price = 0;
        }
        String actualPrice = String.format(Locale.getDefault(), "￥%.2f", price);
        tvActualPayment.setText(splitString("实付款：", actualPrice));
        orderInfoAdapter.notifyDataSetChanged();
        if (shoppingCartBeen.getCouponCodeList().isEmpty()) {
            tvCoupon.setText("暂无可用优惠券");
        } else {
            tvCoupon.setText("有可用优惠券");
            couponBeans.clear();
            couponBeans.addAll(shoppingCartBeen.getCouponCodeList());
        }
    }

    @Override
    public void getPayInfoSuccess(PayInfoBean payInfoBean) {
        if (PayType.WXPAY.equals(payInfoBean.getPayType())) {
            WXPayManager wxPayManager = new WXPayManager(mContext);
            wxPayManager.doStartWXPayPlugin(payInfoBean.getWxpayParams());
        } else {
            AlipayManager alipayManager = new AlipayManager(mContext);
            alipayManager.doStartALiPayPlugin(payInfoBean.getAlipayOrderStr());
        }
    }

    @Override
    public void getPayInfoFailure(String msg) {
        ToastUtils.showLong(mContext, msg);
    }

    @Override
    public void queryCartEmpty() {
        ToastUtils.showShort("课程获取失败，请返回重试");
    }

    @Override
    public void queryCartFailure(String msg) {
        ToastUtils.showShort("课程获取失败，请返回重试");
    }

    @Override
    public void submitOrderSuccess() {

    }

    @Override
    public void submitOrderFailure(String msg) {
        ToastUtils.showShort("课程购买失败，请稍后重试");
    }


}
