package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.GradeJson;
import com.sdzn.pkt.student.phone.bean.UserBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public interface AccountSettingView extends BaseView {

    void changePhotoSuccess(String photo);

    void changePhotoError(String msg);

    void perfectAccountSuccess(UserBean userBean);

    void perfectAccountFailure(String msg);

    void onGradeEmpty();
}
