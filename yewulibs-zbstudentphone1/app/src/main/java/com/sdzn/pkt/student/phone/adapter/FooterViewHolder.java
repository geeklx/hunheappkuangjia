package com.sdzn.pkt.student.phone.adapter;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sdzn.pkt.student.phone.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FooterViewHolder {
    @BindView(R.id.ll_footer)
    public RelativeLayout llFooter;
    @BindView(R.id.tv_hint)
    public TextView tvHint;
    @BindView(R.id.pb_progress)
    public ProgressBar pbProgress;

    public FooterViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}