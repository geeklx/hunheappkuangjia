package com.sdzn.pkt.student.phone.manager;

/**
 * 描述：
 * -全局配置
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class Config {
    public static final boolean is213 = false;//本地服务器为true, 正式上线为FALSE
    public static final String PHONE = "Android手机";
    public static final String TABLET = "Android平板";

    public static final String SERVER_HOST_OLD = is213 ? "http://114.115.128.225" : "http://admin.znclass.com";
    public static final String SERVER_PORT = is213 ? ":8030/" : ":80/";//端口号

    public static final String SERVER_HOST = is213 ? "http://114.115.212.162" : "http://doc.znclass.com";
    public static final String SERVER_PORT_NEW = is213 ? ":7799/" : "";//新接口  端口号

//    public static final String WX_APP_ID = "wx66fa858c7ed41794";
    public static final String WX_APP_ID = "wx267c9d9090a88c69";
    public static final String WX_APP_KEY = "b372ff2fffd8588966d5ce406c704374";
    public static final String QQ_ZOOM_APP_ID = "1106231039";
    public static final String QQ_APP_KEY = "AihJLfcUr4rM1PGa";
    public static final String PGY_APP_ID = "b3f225739110dbd155708d63e08837a5";
    public static final String PGY_APP_KEY = "b405c9a108daf31e08c81e5ccb36909d";


    public static final String ROOT_CACHE = "com.sdzn.pkt.student.phone";
    public static final String APP_CACHE = "cache";
    public static final String APP_DOWNLOAD = "download";
    public static final String IMAGE_CACHE = "image";
    public static final String CRASH_CACHE = "crash";
    public static final String AVATAR_CACHE = "avatar";
    public static final String SCREENSHOT_FILE_DIR = "screenshots"; //截屏文件夹
    public static final String NIM_FILE_DIR = "nim"; //nim文件夹
    public static final String APP_NAME = "拼课堂.apk";
    public static final String DB_ENCRYPT_KEY = "SDZN";

    public static final boolean OPEN_REGISTRATION = false;//是否开放注册


}
