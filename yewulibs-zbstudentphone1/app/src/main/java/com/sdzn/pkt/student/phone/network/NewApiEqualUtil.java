package com.sdzn.pkt.student.phone.network;


import com.sdzn.pkt.student.phone.application.App;
import com.sdzn.pkt.student.phone.manager.Config;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.network.api.ApiInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * 是否跑新接口
 *
 */

public class NewApiEqualUtil {


    private static String getUrl(String url) {
        return Config.SERVER_HOST + Config.SERVER_PORT_NEW + url;
    }
    /**
     *
     *有的新接口不需要token    false -->不加token
     */
    public static boolean isNoToken(String url) {
        String allNewApi[] = {
                getUrl(ApiInterface.SELECT_SUBJECT_LIST),
                getUrl(ApiInterface.SELECT_TOPIC_LIST),
                getUrl(ApiInterface.QUERY_APP_RECOMMENDED),
                getUrl(ApiInterface.QUERY_APP_TOPIC),
                getUrl(ApiInterface.HOME_RECOMMENDED),
                getUrl(ApiInterface.COURSE_SELECTION),
                getUrl(ApiInterface.QUERY_BANNER),
                getUrl(ApiInterface.QUERY_VERSION_INFO)

        };

        for (int a = 0; a < allNewApi.length; a++) {
            if (url.equalsIgnoreCase(allNewApi[a])) {
                return false;
            }
        }
        if (url.equalsIgnoreCase(getUrl(ApiInterface.COURSE_PACKAGE_DETAILS))||url.equalsIgnoreCase(getUrl(ApiInterface.COURSE_DETAILS_NORMAL))){
        if (!SPManager.autoLogin(App.mContext)){
            //未登录
            return false;
            }
        }
        return true;

    }
}
