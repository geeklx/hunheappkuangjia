package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.IndentBean;
import com.sdzn.pkt.student.phone.bean.IndentResultBean;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;

/**
 * 描述：订单
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public interface IndentView extends BaseView {
    void listindents(IndentResultBean bean);

    void onError(String msg);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void getPayInfoFailure();

    void cancelSuccess(IndentBean indentBean);

    void cancelError(String msg);

}
