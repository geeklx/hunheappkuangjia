package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.CourseList;

import java.util.List;

public interface SpellingContentView extends BaseView {
    void getDataCourse(List<CourseList> courseLists);

    void onFailed(String msg);
}
