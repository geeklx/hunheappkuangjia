package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;

public interface UserInfoView extends BaseView {
    void loginSuccess();

    void onFailed();
}
