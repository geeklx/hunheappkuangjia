package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.MovieBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/15
 */
public class TestAdapter extends BaseRcvAdapter {

    public TestAdapter(Context context, List mList) {
        super(context, R.layout.item_test, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, Object o) {
        if (o instanceof MovieBean) {
            MovieBean movieBean = (MovieBean) o;
            holder.setText(R.id.tvChapterName, movieBean.getTitle());
            holder.setText(R.id.tvCreateTime, movieBean.getYear());
            holder.setImageView(R.id.ivPic, movieBean.getImages().getMedium());
        }

    }
}
