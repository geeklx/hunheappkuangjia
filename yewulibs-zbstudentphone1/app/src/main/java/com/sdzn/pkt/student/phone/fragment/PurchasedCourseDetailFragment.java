package com.sdzn.pkt.student.phone.fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.baijiayun.livecore.LiveSDK;
import com.baijiayun.videoplayer.ui.playback.PBRoomUI;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.activity.CollectActivity;
import com.sdzn.pkt.student.phone.activity.CourseDetailActivity;
import com.sdzn.pkt.student.phone.adapter.PageAdapterWithIndicator;
import com.sdzn.pkt.student.phone.bean.CourseCatalogueBean;
import com.sdzn.pkt.student.phone.bean.CourseDetailBean;
import com.sdzn.pkt.student.phone.bean.CourseKpointListBean;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;
import com.sdzn.pkt.student.phone.event.OrderPayEvent;
import com.sdzn.pkt.student.phone.event.ToApplyStatusEvent;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.mvp.presenter.PurchasedCourseDetailPresenter;
import com.sdzn.pkt.student.phone.mvp.view.PurchasedCourseDetailView;
import com.sdzn.pkt.student.phone.utils.PriceUtil;
import com.sdzn.pkt.student.phone.widget.RoundRectImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 课程详情
 * 创建人：baoshengxiang
 * 创建时间：2017/9/4
 */
public class PurchasedCourseDetailFragment extends BaseMVPFragment<PurchasedCourseDetailView, PurchasedCourseDetailPresenter> implements PurchasedCourseDetailView {
    @BindView(R.id.iv_cover)
    RoundRectImageView ivCover;
    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.tab_navi_detail)
    TabLayout tabNaviDetail;
    @BindView(R.id.vp_detail)
    ViewPager vpDetail;
    @BindView(R.id.tv_recmd_time)
    TextView tvRecmdTime;
    @BindView(R.id.tv_recmd_title)
    TextView tvRecmdTitle;
    @BindView(R.id.tv_course_count)
    TextView tvCourseCount;
    @BindView(R.id.tv_course_info)
    TextView tvCourseInfo;
    @BindView(R.id.tv_course_type)
    TextView tvCourseType;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.bt_live)
    Button btLive;
    @BindView(R.id.rl_start_time)
    RelativeLayout rlStartTime;
    //    @BindView(R.id.tv_status)
//    TextView tvStatus;
    @BindView(R.id.tv_sign_end_time)
    TextView tvSignEndTime;
    @BindView(R.id.tv_validity_time)
    TextView tvValidityTime;
    @BindView(R.id.tv_recmd_price)
    TextView tvRecmdPrice;
    @BindView(R.id.tv_old_price)
    TextView tvOldPrice;
    @BindView(R.id.ll_recmd_price)
    LinearLayout llRecmdPrice;
    @BindView(R.id.iv_collect)
    ImageView ivCollect;
    @BindView(R.id.tv_cart_num)
    TextView tvCartNum;
    @BindView(R.id.btn_add_cart)
    Button btnAddCart;
    @BindView(R.id.btn_deadline)
    Button btnDeadline;
    @BindView(R.id.btn_buy)
    Button btnBuy;
    @BindView(R.id.btn_free)
    Button btnFree;
    @BindView(R.id.btn_apply)
    Button btnApply;
    @BindView(R.id.ll_settlement)
    LinearLayout bottomLayout;

    private String[] TITLES = {"介绍", "目录", "资料"};
    private List<Fragment> fragments;
    private PageAdapterWithIndicator fragmentAdapter;

    private CourseDetailBean courseDetailBean;
    private int courseType;//课程类型, 直播/点播
    private int courseId;//课程id
    private int kpointId;//课程章节的ID
    private String kpointName;////课程章节的Name
    private boolean isFavorite;
    private boolean isFree;//是否免费
    private boolean isPurchased;//是否已购买
    private boolean isShowLiveBtn;//区分调用入口, 是否显示进入直播按钮
    private CourseIntroduceFragment courseIntroduce;
    private MineCourseGroupCatalogueFragment mineCourseGroup;
    private CourseGroupCatalogueFragment courseGroup;
    private CourseDataFragment courseData;
    private CourseKpointListBean courseBean;

    public static final String CourseType = "courseType";
    public static final String CourseId = "courseId";
    public static final String CourseState = "courseState";
    public static final String ShowLiveBtn = "showLiveBtn";
    public static final String CourseStateFree = "courseStateFree";//是否免费

    private String kpointStatus = "直播状态";

    public static PurchasedCourseDetailFragment newInstance(int courseId, int courseState, int CourseFree, int type) {
        PurchasedCourseDetailFragment courseDetailFragment = new PurchasedCourseDetailFragment();
        Bundle args = new Bundle();
        args.putInt(CourseId, courseId);
        args.putInt(CourseState, courseState);
        args.putInt(CourseStateFree, CourseFree);
        args.putInt(CourseType, type);
//        args.putBoolean(ShowLiveBtn, showLiveBtn);
        courseDetailFragment.setArguments(args);
        return courseDetailFragment;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        courseDetailBean = ((CourseDetailActivity) context).getCourseDetails();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            courseId = getArguments().getInt(CourseId, -1);
            isShowLiveBtn = getArguments().getBoolean(ShowLiveBtn, false);
            int state = getArguments().getInt(CourseState, 0);
            courseType = getArguments().getInt(CourseType, 0);
            isFree = getArguments().getInt(CourseStateFree, 0) == 0 ? true : false;
            isPurchased = CourseCons.State.isPurchase(state);
        }
    }

    @Override
    protected PurchasedCourseDetailPresenter createPresenter() {
        return new PurchasedCourseDetailPresenter();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_purchased_course_detail;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        initData();
        initView();
        initCourseDetail();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.queryShoppingCart();
    }

    private void initData() {
        if (courseDetailBean != null) {
            if (courseType == CourseCons.Type.LIVING) {
                if (courseDetailBean.isRelationLiveCourse()) {
                    rlStartTime.setVisibility(View.GONE);
                } else {
                    rlStartTime.setVisibility(View.VISIBLE);
                }
            } else if (courseType == CourseCons.Type.VIDEO) {
                rlStartTime.setVisibility(View.GONE);
            }
        }
        fragments = new ArrayList<>();
        courseIntroduce = CourseIntroduceFragment.newInstance();
        fragments.add(courseIntroduce);
        if (showMineCourseGroupCatalogueFragment()) {
            mineCourseGroup = MineCourseGroupCatalogueFragment.newInstance(courseType, courseDetailBean.isPurchase());
            fragments.add(mineCourseGroup);
        } else {
            courseGroup = CourseGroupCatalogueFragment.newInstance(courseType);
            fragments.add(courseGroup);
        }
        courseData = CourseDataFragment.newInstance(courseType);
        fragments.add(courseData);
    }

    /**
     * 判断要加载可点击播放的课程目录 还是不可点击的目录
     */
    private boolean showMineCourseGroupCatalogueFragment() {
//        if (courseDetailBean.isPurchase()){
//            return true;
//        }
//        return isFree || isShowLiveBtn;
        if (!isFree) {
            btnBuy.setText("立即购买");
        }
        return true;
    }

    /**
     * 设置底部购买/加入购物车/开始观看/不可观看四个按钮是否显示
     *
     * @param state 0 : 显示加入购物车 & 报名 <br/>
     *              1 : 显示开始观看 <br/>
     *              2 : 显示不可观看
     *              <p>
     *              3:显示去报名
     */
    private void changeBottomView(int state) {
        if (state < 0 || state > 3) {
            return;
        }
        btnAddCart.setVisibility(state == 0 ? View.VISIBLE : View.GONE);
        btnBuy.setVisibility(state == 0 ? View.VISIBLE : View.GONE);
        btnFree.setVisibility(state == 1 ? View.VISIBLE : View.GONE);
        btnDeadline.setVisibility(state == 2 ? View.VISIBLE : View.GONE);
        btnApply.setVisibility(state == 3 ? View.VISIBLE : View.GONE);
//        if (state == 0) {
//            String time = TimeUtils.millis2String(TimeUtils.string2Millis(courseDetailBean.getSignEndTime()), "MM.dd HH:mm");
//            tvSignEndTime.setText("报名截止: " + time);
//            tvSignEndTime.setVisibility(View.VISIBLE);
//        } else {
//            tvSignEndTime.setVisibility(View.GONE);
//        }
//        tvSignEndTime.setText("报名截止: " + courseDetailBean.getSignEndTime());
//        tvValidityTime.setText("" + courseDetailBean.getDate());
    }

    private void initView() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            AppManager.getAppManager().appExit();
            return;
        }
        if (courseDetailBean == null) {
            courseDetailBean = ((CourseDetailActivity) activity).getCourseDetails();
            if (courseDetailBean == null) {
                AppManager.getAppManager().appExit();
                return;
            }
        }
        boolean isVideo;
        if (courseType == CourseCons.Type.VIDEO) {
            isVideo = true;
        } else if ("PACKAGE".equals(courseDetailBean.getSellType()) && "2".equals(courseDetailBean.getPackageType())) {
            isVideo = true;
        } else {
            isVideo = false;

        }


        tvValidityTime.setText("" + courseDetailBean.getDate());
        tvSignEndTime.setText("报名截止: " + courseDetailBean.getSignEndTime());

        if (isShowLiveBtn) {//从我的课程进入
            bottomLayout.setVisibility(View.GONE);
//            if (!isVideo && "直播中".equals(courseDetailBean.getState())) {
//                btLive.setText("进入直播");
////                btLive.setVisibility(View.VISIBLE);
//            }
        } else {
            btLive.setVisibility(View.GONE);
            llRecmdPrice.setVisibility(View.VISIBLE);
            bottomLayout.setVisibility(View.VISIBLE);
            if (isVideo) {//点播列表
                if (isFree) {
                    if (!courseDetailBean.isPurchase()) {//报名问题
                        changeBottomView(3);
                    } else {
                        changeBottomView(1);
                        btnFree.setText("开始观看");
                    }
                } else {
                    if (courseDetailBean.isPurchase()) {//已报名  购买
                        bottomLayout.setVisibility(View.GONE);
                    } else {
                        changeBottomView(0);
                    }
                }
            } else {//直播列表
                if (isFree) {//免费直播
                    if (courseDetailBean.isPurchase()) {//报名问题
                        bottomLayout.setVisibility(View.GONE);
                    } else {
                        changeBottomView(3);
                    }
                } else {
                    if (courseDetailBean.isPurchase()) {//已报名  购买
                        bottomLayout.setVisibility(View.GONE);
                    } else {
                        changeBottomView(0);
                    }
                }
            }
        }
        if (!isVideo) {
            rlStartTime.setVisibility(View.VISIBLE);
//            tvStatus.setVisibility(View.VISIBLE);
//            tvStatus.setText(courseDetailBean.getState());
            //直播才显示直播时间
            String beginTime = TimeUtils.millis2String(TimeUtils.string2Millis(courseDetailBean.getLiveBeginTime()), "MM.dd HH:mm");
            String endTime = TimeUtils.millis2String(TimeUtils.string2Millis(courseDetailBean.getLiveEndTime()), "MM.dd HH:mm");
            if (courseDetailBean.getLiveBeginTime()!=null) {
                tvRecmdTime.setText("开课时间：" + beginTime + " - " + endTime);
            }else {
                rlStartTime.setVisibility(View.GONE);
            }
        } else {
            rlStartTime.setVisibility(View.GONE);
        }
        btLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickLiveButton();
            }
        });

        fragmentAdapter = new PageAdapterWithIndicator(getChildFragmentManager(), fragments, TITLES);
        vpDetail.setAdapter(fragmentAdapter);
        vpDetail.setCurrentItem(1);
        tabNaviDetail.setupWithViewPager(vpDetail);
    }

    private void clickLiveButton() {
        if (!isFree && !isPurchased) {
            ToastUtils.showShort("该课程尚未购买");
            return;
        }
        boolean isVideo;
        if (courseType == CourseCons.Type.VIDEO) {
            isVideo = true;
        } else if ("PACKAGE".equals(courseDetailBean.getSellType()) && "2".equals(courseDetailBean.getPackageType())) {
            isVideo = true;
        } else {
            isVideo = false;
        }
        if (!isVideo) {
            if ("观看直播".equals(kpointStatus)) {
                mPresenter.getLivingInfo(kpointId);

            } else if ("查看回放".equals(kpointStatus)) {//暂时没有
                mPresenter.getReplayInfo(kpointId, courseId);


            }
            return;
        }
        if (!isFree && courseDetailBean.getIsavaliable() != 1) {
            ToastUtils.showShort("没有可播放的课程");
            return;
        }
        //点播
        //单科
        if (!"PACKAGE".equals(courseDetailBean.getSellType())) {
            List<CourseKpointListBean> kpointList = courseDetailBean.getCourseKpointList();
            if (checkList(kpointList)) {
                for (CourseKpointListBean courseKpointListBean : kpointList) {
                    if (courseKpointListBean.getKpointType() == 1 && !courseDetailBean.isRelationLiveCourse()) {
                        mPresenter.getVideoInfo(courseKpointListBean.getKpointId());
                        return;
                    } else if (courseKpointListBean.getKpointType() == 1 && courseDetailBean.isRelationLiveCourse()) {
                        mPresenter.getReplayInfo(courseKpointListBean.getKpointId(), courseId);
                        return;
                    }
                }
            } else {
                ToastUtils.showShort("没有可播放的课程");
            }
            return;
        }
        //组合
        List<CourseCatalogueBean> courseList = courseDetailBean.getCourseList();
        if (!checkList(courseList)) {
            ToastUtils.showShort("没有可播放的课程");
            return;
        }
        for (CourseCatalogueBean catalogueBean : courseList) {
            for (CourseKpointListBean bean : catalogueBean.getCourseKpointList()) {
                if (bean.getKpointType() == 1 && !courseDetailBean.isRelationLiveCourse()) {
                    courseBean = bean;
                    mPresenter.getVideoInfo(courseBean.getKpointId());
                    return;
                } else if (bean.getKpointType() == 1 && courseDetailBean.isRelationLiveCourse()) {
                    courseBean = bean;
                    mPresenter.getReplayInfo(courseBean.getKpointId(), courseId);
                    return;
                }
            }
        }
        ToastUtils.showShort("没有可播放的课程");
    }

    private boolean checkList(List list) {
        return !(list == null || list.isEmpty());
    }

    private void initCourseDetail() {
        if (courseDetailBean == null) {
            return;
        }
        //图片
        if (courseDetailBean.getLogo() != null) {
            GlideImgManager.loadImage(mContext, "" + courseDetailBean.getLogo(), ivCover);
        }
        //科目/讲师
        StringBuilder courseDesc = new StringBuilder();
        if ("PACKAGE".equals(courseDetailBean.getSellType())) {

            tvCourseType.setText("组合");
            courseDesc.append("" + courseDetailBean.getCourseDescString());

        } else {
            tvCourseType.setText("单科");
            courseDesc.append("讲师：");
            courseDesc.append("" + courseDetailBean.getCourseDescString());
        }
        tvCourseInfo.setText(courseDesc.toString());
        //标题
        tvRecmdTitle.setText(courseDetailBean.getCourseName());

        //课时数
//        tvCourseCount.setText(String.valueOf(courseDetailBean.getLessionNum()));
        //只有从我的课程进入, 才不显示费用
        if (!isShowLiveBtn) {
            if (PriceUtil.isFree(courseDetailBean.getCurrentPrice())) {
                tvRecmdPrice.setText(getString(R.string.free));
                tvRecmdPrice.setTextColor(ContextCompat.getColor(mContext, R.color.free_green));
                tvRecmdPrice.setTextSize(20);
            } else {
                tvRecmdPrice.setText("¥" + new BigDecimal(String.valueOf(courseDetailBean.getCurrentPrice())).stripTrailingZeros().toPlainString());
                tvRecmdPrice.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                tvRecmdPrice.setTextSize(24);
            }
            if (PriceUtil.isFree(courseDetailBean.getSourcePrice())) {
                tvOldPrice.setText("¥0");
            } else {
                tvOldPrice.setText("¥" + new BigDecimal(String.valueOf(courseDetailBean.getSourcePrice())).stripTrailingZeros().toPlainString());
            }
            tvOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        }
        isFavorite = courseDetailBean.isCollection();
        if (isFavorite) {
            ivCollect.setImageResource(R.mipmap.ic_collect);
        } else {
            ivCollect.setImageResource(R.mipmap.ic_uncollect);
        }
        courseIntroduce.setData(courseDetailBean);
        //给对应加载的bean设置课程目录数据
        if (showMineCourseGroupCatalogueFragment()) {
            mineCourseGroup.setData(courseDetailBean);
        } else {
            courseGroup.setData(courseDetailBean);
        }
        courseData.setData(courseDetailBean);
        if ((!isFree && !isPurchased) || courseType != CourseCons.Type.LIVING) {
            return;
        }
        //遍历查询正在直播的课程，展示正在直播
        if ("PACKAGE".equals(courseDetailBean.getSellType())) {
            for (int i = 0; i < courseDetailBean.getCourseList().size(); i++) {
                for (int j = 0; j < courseDetailBean.getCourseList().get(i).getCourseKpointList().size(); j++) {
                    CourseKpointListBean bean = courseDetailBean.getCourseList().get(i).getCourseKpointList().get(j);
                    if (CourseCons.LiveStatus.LIVE_LIVING_NEW.equalsIgnoreCase(bean.getLiveStates())) {
                        kpointId = bean.getKpointId();
                        kpointName = bean.getName();
                    }
                }
            }
        } else {
            for (int i = 0; i < courseDetailBean.getCourseKpointList().size(); i++) {
                CourseKpointListBean bean = courseDetailBean.getCourseKpointList().get(i);
                if (CourseCons.LiveStatus.LIVE_LIVING_NEW.equalsIgnoreCase(bean.getLiveStates())) {
                    kpointId = bean.getKpointId();
                    kpointName = bean.getName();
                }
            }
        }
    }

    /**
     * 直播 信息成功返回
     */
    @Override
    public void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean) {
        if (kpointName == null) {
            kpointName = "拼课堂";
        }
//        IntentController.toLiveCoursePlayer(mContext, liveRoomBean.getChannel().getKpointId(),
//                String.valueOf(liveRoomBean.getChatroom().getRoomId()),
//                liveRoomBean.getChannel().getRtmpPullUrl(), kpointName, liveRoomBean.getShareUrl());

    }

    @Override
    public void liveRoomInfoOnError(String msg) {
        ToastUtils.showShort(msg);
    }

    private long mLastClickTime;
    private long timeInterval = 1000L;

    @OnClick({R.id.tv_cart, R.id.iv_collect, R.id.btn_add_cart, R.id.btn_buy, R.id.btn_free, R.id.bt_live, R.id.btn_apply})
    public void onViewClicked(View view) {
        if (!SPManager.autoLogin(mContext)) {
            IntentController.toLogin(mContext, true);
            return;
        }
        long nowTime = System.currentTimeMillis();
        if (nowTime - mLastClickTime > timeInterval) {
            switch (view.getId()) {
                case R.id.tv_cart:
                    IntentController.toShoppingCart(mContext);
                    break;
                case R.id.iv_collect:
                    doFavorite();
                    break;
                case R.id.btn_add_cart:
                    addToCart();
                    break;
                case R.id.btn_buy:
                    toBuy();
                    break;
                case R.id.btn_free:
                case R.id.bt_live:
                    clickLiveButton();
                    break;
                case R.id.btn_apply:
                    toApply();
                    break;
                default:
                    break;
            }
            mLastClickTime = nowTime;
        }
    }

    /**
     * 报名 都是免费
     */
    private void toApply() {
        if (!isFree) {
            changeBottomView(1);
            return;
        }
        mPresenter.getIsPurchase(courseId);
    }

    /**
     * 收藏操作
     */
    private void doFavorite() {
        isFavorite = !isFavorite;
        if (isFavorite) {
            mPresenter.addFavorite(courseId);
            ivCollect.setImageResource(R.mipmap.ic_collect);
        } else {
            mPresenter.delFavorite(courseId);
            ivCollect.setImageResource(R.mipmap.ic_uncollect);
        }
    }

    /**
     * 购买
     */
    private void toBuy() {
        if (courseDetailBean == null) {
            return;
        }
        if (isFree) {
            ToastUtils.showShort("免费课程不需购买");
            return;
        }
        mPresenter.addShoppingCart(courseId, true);
    }

    /**
     * 添加到购物车
     */
    private void addToCart() {
        if (isFree) {
            ToastUtils.showShort("免费课程不需购买");
            return;
        }
        mPresenter.addShoppingCart(courseId, false);
    }

    @Override
    public void toSettlement(boolean isexsits, String toSettlement, int cartNum) {
        IntentController.toOrderSubmit(mContext, toSettlement);
        if (!isexsits) {
            cartN = cartN + cartNum;
            tvCartNum.setVisibility(cartN == 0 ? View.GONE : View.VISIBLE);
            tvCartNum.setText(String.valueOf(cartN));
        }
    }

    private int cartN = 0;

    @Override
    public void addShoppingCartSuccess(boolean isexsits, int cartNum) {
        if (isexsits) {
            ToastUtils.showShort("购物车已存在该课程");
        } else {
            cartN = cartN + cartNum;
            ToastUtils.showShort("加入购物车成功");
            tvCartNum.setVisibility(cartN == 0 ? View.GONE : View.VISIBLE);
            tvCartNum.setText(String.valueOf(cartN));
        }

    }

    @Override
    public void addShoppingCartFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void addFavoriteSuccess() {

    }

    @Override
    public void addFavoriteFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void delFavoriteSuccess() {
        ((Activity) mContext).setResult(CollectActivity.RESULT_COURSEDETAIL_CODE);
    }

    @Override
    public void delFavoriteFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void getCartNumSuccess(int cartNum) {
        cartN = cartNum;
        tvCartNum.setVisibility(cartNum == 0 ? View.GONE : View.VISIBLE);
        tvCartNum.setText(String.valueOf(cartNum));
    }

    /**
     * 点播信息成功返回
     */
    @Override
    public void getVideoRoomInfoSuccrss(NewVideoInfo info) {
        PBRoomUI.startPlayVideo(mContext, Long.valueOf(info.getVideoId()), info.getToken(), null);
    }

    @Override
    public void videoRoomInfoOnError(String msg) {
        ToastUtils.showShort(msg);
    }

    /**
     * 回放
     */
    @Override
    public void getReplayInfoSuccess(NewVideoInfo info) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        PBRoomUI.enterPBRoom(getActivity(), info.getRoomId(), info.getToken(), "0", new PBRoomUI.OnEnterPBRoomFailedListener() {

            @Override
            public void onEnterPBRoomFailed(String msg) {
                ToastUtils.showShort(msg);
            }
        });
    }

    @Override
    public void applySuccess() {
        ToastUtils.showShort("报名成功");
        EventBus.getDefault().post(new ToApplyStatusEvent(true));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setIsPurchase(ToApplyStatusEvent statusEvent) {
        //直播即隐藏    点播
        if (courseType == CourseCons.Type.VIDEO) {
            changeBottomView(1);
        } else {
            bottomLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
