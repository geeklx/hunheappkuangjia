package com.sdzn.pkt.student.phone.manager;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import com.sdzn.pkt.student.phone.activity.AboutActivity;
import com.sdzn.pkt.student.phone.activity.AccountRemoveActivity;
import com.sdzn.pkt.student.phone.activity.AccountSettingActivity;
import com.sdzn.pkt.student.phone.activity.BindingAccountActivity;
import com.sdzn.pkt.student.phone.activity.BindingMobileActivity;
import com.sdzn.pkt.student.phone.activity.ChangeClassActivity;
import com.sdzn.pkt.student.phone.activity.ChangeNameActivity;
import com.sdzn.pkt.student.phone.activity.ChangePwdActivity;
import com.sdzn.pkt.student.phone.activity.ChangeSchoolNewActivity;
import com.sdzn.pkt.student.phone.activity.CollectActivity;
import com.sdzn.pkt.student.phone.activity.CouponActivity;
import com.sdzn.pkt.student.phone.activity.CourseDetailActivity;
import com.sdzn.pkt.student.phone.activity.CourseExcellentActivity;
import com.sdzn.pkt.student.phone.activity.CourseTopicActivity;
import com.sdzn.pkt.student.phone.activity.FeedbackActivity;
import com.sdzn.pkt.student.phone.activity.IndentActivity;
import com.sdzn.pkt.student.phone.activity.LoginActivity;
import com.sdzn.pkt.student.phone.activity.MainActivity;
import com.sdzn.pkt.student.phone.activity.MessageActivity;
import com.sdzn.pkt.student.phone.activity.MessageDetailActivity;
import com.sdzn.pkt.student.phone.activity.OrderDetailActivity;
import com.sdzn.pkt.student.phone.activity.OrderSubmitActivity;
import com.sdzn.pkt.student.phone.activity.ReSetPasswordActivity;
import com.sdzn.pkt.student.phone.activity.RegisterActivity;
import com.sdzn.pkt.student.phone.activity.ReimburseActivity;
import com.sdzn.pkt.student.phone.activity.RetrievePasswordActivity;
import com.sdzn.pkt.student.phone.activity.SearchActivity;
import com.sdzn.pkt.student.phone.activity.SearchResultActivity;
import com.sdzn.pkt.student.phone.activity.SelectSubjectActivity;
import com.sdzn.pkt.student.phone.activity.ShoppingCartActivity;
import com.sdzn.pkt.student.phone.activity.SystemSettingActivity;
import com.sdzn.pkt.student.phone.activity.WebActivity;
import com.sdzn.pkt.student.phone.activity.WelcomeActivity;
import com.sdzn.pkt.student.phone.bean.CouponBean;
import com.sdzn.pkt.student.phone.bean.TeacherListBean;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.mvp.view.CourseExcellentView;

import java.util.ArrayList;
import java.util.List;

import static com.sdzn.pkt.student.phone.activity.CourseDetailActivity.COURSE_ID;
import static com.sdzn.pkt.student.phone.activity.CourseDetailActivity.COURSE_TYPE;
import static com.sdzn.pkt.student.phone.activity.CourseDetailActivity.PACKAGE;
import static com.sdzn.pkt.student.phone.activity.CourseDetailActivity.SHOW_LIVE_BTN;
import static com.sdzn.pkt.student.phone.activity.LoginActivity.LOGIN_DETAIL;
import static com.sdzn.pkt.student.phone.activity.MainActivity.AUTO_LOGIN;

public class IntentController {

    /**
     * @param autoLogin 传入true时, 在mainActivity会重新登录一次
     */
    public static void toMain(Context context, boolean autoLogin) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(AUTO_LOGIN, autoLogin);
        context.startActivity(intent);
    }

    public static void toWelcome(Context context) {
        Intent intent = new Intent(context, WelcomeActivity.class);
        context.startActivity(intent);
    }

    public static void toLogin(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    public static void toLogin(Context context, boolean isDetail) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(LOGIN_DETAIL, isDetail);
        context.startActivity(intent);
    }

    public static void toRegister(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        context.startActivity(intent);
    }

    public static void toWeb(Context context, String type) {
        Intent changeNameIntent = new Intent(context, WebActivity.class);
        changeNameIntent.putExtra(WebActivity.INTENT_WEB, type);
        context.startActivity(changeNameIntent);
    }

    public static void toCourseDetail(Context context, int courseType,
                                      int courseId, boolean showLiveBtn) {
        Intent intent = new Intent(context, CourseDetailActivity.class);
        intent.putExtra(COURSE_TYPE, courseType);
        intent.putExtra(COURSE_ID, courseId);
        intent.putExtra(SHOW_LIVE_BTN, showLiveBtn);
        context.startActivity(intent);
    }

    public static void toCourseDetail(Context context, String courseType,
                                      int courseId, boolean showLiveBtn) {
        Intent intent = new Intent(context, CourseDetailActivity.class);
        intent.putExtra(PACKAGE, courseType);
        intent.putExtra(COURSE_ID, courseId);
        intent.putExtra(SHOW_LIVE_BTN, showLiveBtn);
        context.startActivity(intent);
    }

    public static void toCourseDetailForResult(Context context, int courseType,
                                               int courseId, boolean showLiveBtn) {
        Intent intent = new Intent(context, CourseDetailActivity.class);
        intent.putExtra(COURSE_TYPE, courseType);
        intent.putExtra(COURSE_ID, courseId);
        intent.putExtra(SHOW_LIVE_BTN, showLiveBtn);
        ((Activity) context).startActivityForResult(intent, 0);
    }

    public static void toAccountSetting(Context context) {
        Intent startIntent = new Intent(context, AccountSettingActivity.class);
        context.startActivity(startIntent);
    }

    public static void toAccountSetting(Context context, UserBean userBean) {
        Intent accountSettingIntent = new Intent(context, AccountSettingActivity.class);
        accountSettingIntent.putExtra("userInfo", userBean);
        context.startActivity(accountSettingIntent);
    }


    public static void toChageName(Context context, String studentName) {
        Intent changeNameIntent = new Intent(context, ChangeNameActivity.class);
        changeNameIntent.putExtra("studentName", studentName);
        context.startActivity(changeNameIntent);
    }

    public static void toChageClass(Context context, String classes) {
        Intent changeNameIntent = new Intent(context, ChangeClassActivity.class);
        changeNameIntent.putExtra("classes", classes);
        context.startActivity(changeNameIntent);
    }

    public static void toChageSchool(Context context, String classes) {
        Intent changeNameIntent = new Intent(context, ChangeSchoolNewActivity.class);
        changeNameIntent.putExtra("schoolName", classes);
        context.startActivity(changeNameIntent);
    }

    public static void toBindAccount(Context context, String classes) {
        Intent changeNameIntent = new Intent(context, BindingAccountActivity.class);
        changeNameIntent.putExtra("bindAccount", classes);
        context.startActivity(changeNameIntent);
    }

    public static void toBindingMobile(Context context) {
        Intent bindMobileIntent = new Intent(context, BindingMobileActivity.class);
        context.startActivity(bindMobileIntent);
    }


    public static void toSearch(Context context, @CourseCons.Type.CourseType int courseType) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra("courseType", courseType);
        context.startActivity(intent);
    }

    public static void toSearch(Context context) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra(SearchActivity.IS_SEARCH_TEACHER, true);
        context.startActivity(intent);
    }

    public static void toAbout(Context context) {
        Intent intent = new Intent(context, AboutActivity.class);
        context.startActivity(intent);
    }

    public static void toAccountRemove(Context context) {
        Intent intent = new Intent(context, AccountRemoveActivity.class);
        context.startActivity(intent);
    }

    public static void toSystemSetting(Context context) {
        Intent intent = new Intent(context, SystemSettingActivity.class);
        context.startActivity(intent);
    }

    public static void toChangePwd(Context context) {
        Intent intent = new Intent(context, ChangePwdActivity.class);
        context.startActivity(intent);
    }


    public static void toSearchResult(Context context, @CourseCons.Type.CourseType int courseType,
                                      String searchStr) {
        Intent intent = new Intent(context, SearchResultActivity.class);
        intent.putExtra("searchStr", searchStr);
        intent.putExtra("courseType", courseType);
        context.startActivity(intent);
    }


    public static void toShoppingCart(Context context) {
        Intent intent = new Intent(context, ShoppingCartActivity.class);
        context.startActivity(intent);
    }

    public static void toOrderSubmit(Context context, String cartIdStr) {
        Intent intent = new Intent(context, OrderSubmitActivity.class);
        intent.putExtra("goods", cartIdStr);
        context.startActivity(intent);
    }

    public static void toMessage(Context context) {
        Intent intent = new Intent(context, MessageActivity.class);
        context.startActivity(intent);
    }

    public static void toMessageStatic(Context context, String messageId, String messageType, String content, String addTime) {
        Intent intent = new Intent(context, MessageDetailActivity.class);
        intent.putExtra(MessageDetailActivity.ID_MESSAGE, messageId);
        intent.putExtra(MessageDetailActivity.TYPE_MESSAGE, messageType);
        intent.putExtra(MessageDetailActivity.CONTENT, content);
        intent.putExtra(MessageDetailActivity.ADD_TIME, addTime);
        context.startActivity(intent);
    }


    public static void toIndent(Context context) {
        Intent intent = new Intent(context, IndentActivity.class);
        context.startActivity(intent);
    }

    public static void toCollect(Context context) {
        Intent intent = new Intent(context, CollectActivity.class);
        context.startActivity(intent);
    }

    public static void toReimburse(Context context, int orderId) {
        Intent intent = new Intent(context, ReimburseActivity.class);
        intent.putExtra("orderId", orderId);
        context.startActivity(intent);
    }

    public static void toRetrievePwd(Context context) {
        Intent intent = new Intent(context, RetrievePasswordActivity.class);
        context.startActivity(intent);
    }

    public static void toResetPassword(Context context) {
        Intent intent = new Intent(context, ReSetPasswordActivity.class);
        context.startActivity(intent);
    }

    public static void toCoupon(Context context, String titleName) {
        Intent intent = new Intent(context, CouponActivity.class);
        intent.putExtra("titleName", titleName);
        context.startActivity(intent);
    }

    public static void toCoupon(Context context, String titleName, List<CouponBean> coupons) {
        Intent intent = new Intent(context, CouponActivity.class);
        intent.putExtra("titleName", titleName);
        intent.putParcelableArrayListExtra("coupons", (ArrayList<? extends Parcelable>) coupons);
        ((Activity) context).startActivityForResult(intent, 1);
    }

    //暂时将LiveCoursePlayerActivity修改为im的AudienceActivity
    public static void toLiveCoursePlayer(Context context, int kpointId, String roomId, String pullUrl, String title, String shareHttp) {
//        AudienceActivity.start(context, kpointId, roomId, pullUrl, title, shareHttp);
    }


    public static void toSelectSubject(Context context, boolean isMainIn) {
        Intent startIntent = new Intent(context, SelectSubjectActivity.class);
        startIntent.putExtra(SelectSubjectActivity.IS_MAIN_IN, isMainIn);
        context.startActivity(startIntent);
    }

    public static void toSelectSubject(Context context, boolean isMainIn, boolean isUpdate, boolean isDetails) {
        Intent startIntent = new Intent(context, SelectSubjectActivity.class);
        startIntent.putExtra(SelectSubjectActivity.IS_MAIN_IN, isMainIn);
        startIntent.putExtra(SelectSubjectActivity.IS_UPDATE, isUpdate);
        startIntent.putExtra(SelectSubjectActivity.IS_DETAILS, isDetails);
        context.startActivity(startIntent);
    }

    public static void toFKActivity(Context context) {
        context.startActivity(new Intent(context, FeedbackActivity.class));
    }

    public static void toRecActivity(Context context) {
        context.startActivity(new Intent(context, CourseExcellentActivity.class));
    }

    public static void toTopicActivity(Context context) {
        context.startActivity(new Intent(context, CourseTopicActivity.class));
    }
    public static void toOrderDetailActivity(Context context,int orderId) {
        Intent startIntent = new Intent(context, OrderDetailActivity.class);
        startIntent.putExtra(OrderDetailActivity.ORDER_ID, orderId);
        context.startActivity(startIntent);
    }
}