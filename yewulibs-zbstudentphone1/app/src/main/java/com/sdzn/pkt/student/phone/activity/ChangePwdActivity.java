package com.sdzn.pkt.student.phone.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.ChangePwdPresenter;
import com.sdzn.pkt.student.phone.mvp.view.ChangePwdView;
import com.sdzn.pkt.student.phone.widget.DialogUtil;
import com.sdzn.pkt.student.phone.widget.PwdEditText;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 修改密码
 *  zs
 */

public class ChangePwdActivity extends BaseMVPActivity<ChangePwdView, ChangePwdPresenter> implements ChangePwdView {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.et_oldpwd)
    PwdEditText etOldpwd;
    @BindView(R.id.et_newpwd)
    PwdEditText etNewpwd;
    @BindView(R.id.et_againpwd)
    PwdEditText etAgainpwd;
    @BindView(R.id.bt_affirm)
    Button btAffirm;
    @BindView(R.id.rl_old)
    RelativeLayout rlOld;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_changepwd;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initView();
        initData();

    }

    private void initData() {
    }

    private void initView() {
        if (SPManager.getUser().getPassword()==null||SPManager.getUser().getPassword().isEmpty()){//toc没设置密码
            rlOld.setVisibility(View.GONE);
        }
    }

    @Override
    protected ChangePwdPresenter createPresenter() {
        return new ChangePwdPresenter();
    }

    @OnClick(R.id.bt_affirm)
    public void onViewClicked() {
        String old = etOldpwd.getText().toString().trim();
        String newP = etNewpwd.getText().toString().trim();
        String again = etAgainpwd.getText().toString().trim();
        if (SPManager.getUser().getPassword()==null||SPManager.getUser().getPassword().isEmpty()){
            mPresenter.confirm(newP, again);
        }else {
            mPresenter.confirmPwd(old, newP, again);
        }
    }

    @Override
    public void changeSuccess() {
        SPManager.getUser().setPassword(etNewpwd.getText().toString());
                DialogUtil.showReLoginDialog(this, "密码修改成功", false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //密码修改成功之后应该关掉所有界面并跳转到登录界面，
//                logout();
                ChangePwdActivity.this.finish();
            }
        });

    }

    @Override
    public void changeFailure(String msg) {
        ToastUtils.showShort(msg);

    }

    private void logout() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
        IntentController.toMain(mContext,false);
    }
}
