package com.sdzn.pkt.student.phone.fragment;


import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.RecommendAdapter;
import com.sdzn.pkt.student.phone.adapter.WrapAdapter;
import com.sdzn.pkt.student.phone.bean.CourseList;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.SpellingContentPresenter;
import com.sdzn.pkt.student.phone.mvp.view.SpellingContentView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * A simple {@link } subclass.
 */
public class SpellingContentFragment extends BaseMVPFragment<SpellingContentView, SpellingContentPresenter> implements SpellingContentView, OnRefreshLoadmoreListener {
    public static final String SUBJECT_TYPE = "subject_page";
    private static final String TYPE_SEARCH = "SEARCH";
    public static final String IS_SEARCH_IN = "to_in_search";   //
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;
    @BindView(R.id.swipe_target)
    RecyclerView rvCourseRec;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    private int pageIndex = 1;//当前页
    private int pageSize = 10;//

    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private String subjectType = "";//学科
    private String courseName = "";
    private String isSearchIn = "";

    public SpellingContentFragment() {
        // Required empty public constructor
    }

    public static SpellingContentFragment newInstance(String id, String courseName, String typeIn) {
        Bundle args = new Bundle();
        args.putString(SUBJECT_TYPE, id);
        args.putString(TYPE_SEARCH, courseName);
        args.putString(IS_SEARCH_IN, typeIn);
        SpellingContentFragment fragment = new SpellingContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_spelling_content;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        if (getArguments() != null) {
            subjectType = getArguments().getString(SUBJECT_TYPE);
            courseName = getArguments().getString(TYPE_SEARCH);
            isSearchIn = getArguments().getString(IS_SEARCH_IN);
        }
        pageIndex = 1;
        recommendCourses = new ArrayList<>();
        initView();
        loadData();
    }


    @Override
    protected SpellingContentPresenter createPresenter() {
        return new SpellingContentPresenter();
    }

    private void initView() {
        refreshLayout.setOnRefreshLoadmoreListener(this);
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 2));
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    IntentController.toCourseDetail(mContext, recommendCourses.get(position).getSellType(), recommendCourses.get(position).getCourseId(), false);
                }

            }
        });


        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                loadData();
            }
        });
    }

    private void loadData() {
        Map<String, String> map = new HashMap<>();
        if (!IS_SEARCH_IN.equals(isSearchIn)) {
            map.put("classId", String.valueOf(SPManager.getSectionId()));
            map.put("subjectId", subjectType);
            map.put("gradeType", String.valueOf(SPManager.getEduId()));
        }
        map.put("courseName", courseName);
        map.put("size", String.valueOf(pageSize));
        map.put("index", String.valueOf(pageIndex));
        mPresenter.getCourse(map);

    }

    @Override
    public void getDataCourse(List<CourseList> courses) {
        clearLoingState();
        if (pageIndex == 1) {
            this.recommendCourses.clear();
            if (courses.size() == 0) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                return;
            }
        }
        refreshLayout.setLoadmoreFinished(courses.size() < pageSize);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);

    }

    @Override
    public void onFailed(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        loadData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        loadData();

    }

    private void clearLoingState() {
        if (refreshLayout != null) {
            if (refreshLayout.isRefreshing()) {
                refreshLayout.finishRefresh();
            }
            if (refreshLayout.isLoading()) {
                refreshLayout.finishLoadmore();
            }
        }
    }
}
