package com.sdzn.pkt.student.phone.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class NoteBean implements Serializable {
    /**
     * courseNoteAllList : [{"content":"很实用很好","courseId":202,"courseName":"7.10-1","id":9,"kpointId":155,"kpointName":"第一讲：人力资源规划","updateTime":"2017-05-22 10:33:08","userId":1464}]
     * page : {"currentPage":1,"first":true,"last":true,"pageSize":10,"totalPageSize":1,"totalResultSize":1}
     */

    private PageBean page;
    private List<CourseNoteAllListBean> courseNoteAllList;

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public List<CourseNoteAllListBean> getCourseNoteAllList() {
        return courseNoteAllList;
    }

    public void setCourseNoteAllList(List<CourseNoteAllListBean> courseNoteAllList) {
        this.courseNoteAllList = courseNoteAllList;
    }

    public static class PageBean {
        /**
         * currentPage : 1
         * first : true
         * last : true
         * pageSize : 10
         * totalPageSize : 1
         * totalResultSize : 1
         */

        private int currentPage;
        private boolean first;
        private boolean last;
        private int pageSize;
        private int totalPageSize;
        private int totalResultSize;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalPageSize() {
            return totalPageSize;
        }

        public void setTotalPageSize(int totalPageSize) {
            this.totalPageSize = totalPageSize;
        }

        public int getTotalResultSize() {
            return totalResultSize;
        }

        public void setTotalResultSize(int totalResultSize) {
            this.totalResultSize = totalResultSize;
        }
    }

    public static class CourseNoteAllListBean implements Serializable {
        /**
         * content : 很实用很好
         * courseId : 202
         * courseName : 7.10-1
         * id : 9
         * kpointId : 155
         * kpointName : 第一讲：人力资源规划
         * updateTime : 2017-05-22 10:33:08
         * userId : 1464
         */

        private String content;
        private int courseId;
        private String courseName;
        private int id;
        private int kpointId;
        private String kpointName;
        private long updateTime;
        private int userId;
        private boolean isSelect;

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getKpointId() {
            return kpointId;
        }

        public void setKpointId(int kpointId) {
            this.kpointId = kpointId;
        }

        public String getKpointName() {
            return kpointName;
        }

        public void setKpointName(String kpointName) {
            this.kpointName = kpointName;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }


}
