package com.sdzn.pkt.student.phone.widget.ActionSheet;

public interface OnSheetItemClickListener {
    void onClick(int which);
}