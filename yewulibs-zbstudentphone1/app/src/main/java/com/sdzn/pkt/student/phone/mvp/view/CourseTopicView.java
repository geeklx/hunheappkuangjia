package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.CourseList;
import com.sdzn.pkt.student.phone.bean.SubjectBean;

import java.util.List;

/**
 * 专题课
 */
public interface CourseTopicView extends BaseView {
    void onTopicSuccess(List<SubjectBean> subjectList);

    void onTopicFailed(String msg);

}
