package com.sdzn.pkt.student.phone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdzn.core.base.BaseFragment;
import com.sdzn.pkt.student.phone.R;

import butterknife.BindView;
import butterknife.OnClick;

public class WebFragment extends BaseFragment {
    @BindView(R.id.web)
    WebView mWebView;
    @BindView(R.id.iv_close)
    ImageView mImageView;
    @BindView(R.id.tv_title)
    TextView mTextView;

    private AnswerFragment.OnFragmentExitListener mListener;
    private String url;


    public static WebFragment newInstance(String url) {
        WebFragment fragment = new WebFragment();
        fragment.url = url;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AnswerFragment.OnFragmentExitListener) {
            mListener = (AnswerFragment.OnFragmentExitListener) context;
        } else {
            throw new UnsupportedOperationException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_web;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        if (url == null || url.isEmpty()) {
            return;
        }
        mTextView.setText(url);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                String title = view.getTitle();
                if (title == null || title.isEmpty()) {
                    title = url;
                }
                mTextView.setText(title);
            }
        });
        mWebView.loadUrl(url);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
    }


    @OnClick(R.id.iv_close)
    public void submitAnswer() {
        mListener.onAnswerCardClose();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
