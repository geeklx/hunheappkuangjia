package com.sdzn.pkt.student.phone.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：关于我们
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class AboutActivity extends BaseActivity {
    @BindView(R.id.tv_qq)
    TextView tvQQ;
    @BindView(R.id.tv_phone)
    TextView tvPhone;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about;
    }

    @Override
    protected void onInit(Bundle bundle) {
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.statusBarLightMode(this);
    }

    @OnClick({R.id.tv_qq, R.id.tv_phone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_qq:
                startQQ();
                break;
            case R.id.tv_phone:
                callPhone();
                break;
        }
    }

    private void startQQ() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mqqwpa://im/chat?chat_type=wpa&uin=2495065899")));
        } catch (Exception e) {
            ToastUtils.showShort("请检查是否安装QQ");
            e.printStackTrace();
        }
    }

    private void callPhone() {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:053189922971"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            ToastUtils.showShort("无法拨号");
            e.printStackTrace();
        }
    }
}
