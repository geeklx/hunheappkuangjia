package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import butterknife.BindView;

public class WebActivity extends BaseActivity {
    public static final String INTENT_WEB = "userType";
    @BindView(R.id.web)
    WebView mWebView;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    private String nameType = "";//1 拼课堂用户协议  2拼课堂用户隐私协议
    private String url = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_web;
    }

    @Override
    protected void onInit(Bundle bundle) {
        nameType = getIntent().getStringExtra("userType");
        if (!TextUtils.isEmpty(nameType)) {
            switch (nameType) {
                case "1":
//                    url = "http://www.znclass.com/video/Useragreement.html";
                    url = "https://m.znclass.com/Useragreement1.html";
                    nameType = "用户协议";
                    break;
                case "2":
//                    url = "http://www.znclass.com/video/agreement.html";
                    url = "https://m.znclass.com/agreement1.html";
                    nameType = "隐私协议";
                    break;
                default:
                    nameType = "暂无";
                    break;
            }
            titleBar.setTitleText(nameType);

            mWebView.loadUrl(url);
//            WebSettings settings = mWebView.getSettings();
//            settings.setJavaScriptEnabled(true);
//            settings.setUseWideViewPort(true);
//            settings.setLoadWithOverviewMode(true);
//            settings.setLoadsImagesAutomatically(true);
//            settings.setJavaScriptCanOpenWindowsAutomatically(true);
        }
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.this.finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        WebActivity.this.finish();
    }
}
