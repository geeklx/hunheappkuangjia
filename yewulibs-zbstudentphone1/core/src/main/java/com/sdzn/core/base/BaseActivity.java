package com.sdzn.core.base;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.StatusBarUtil;

import butterknife.ButterKnife;

/**
 * 描述：
 * - activity基类
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Context mContext = null;//context

    protected abstract int getLayoutResource();

    protected abstract void onInit(Bundle bundle);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        StatusBarCompat.compat(this);
        AppManager.getAppManager().addActivity(this);
        int layoutResource = getLayoutResource();
        if (layoutResource != 0) {
            setContentView(layoutResource);
        }
        ButterKnife.bind(this);
        mContext = this;
        onInit(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setStatusBar();
    }

    protected void setStatusBar() {
        StatusBarUtil.setColor(this, 0xFFFFB039, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
//        MobclickAgent.onResume(this);       //统计时长
    }

    @Override
    public void onPause() {
        super.onPause();
//        MobclickAgent.onPause(this);
    }

//    /**
//     *点击空白处隐藏软键盘
//     */
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        if (null != this.getCurrentFocus()) {
//            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//            return mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
//        }
//        return super.onTouchEvent(event);
//    }

    @Override
    protected void onDestroy() {
        AppManager.getAppManager().finishActivity(this);
        super.onDestroy();
    }

}
