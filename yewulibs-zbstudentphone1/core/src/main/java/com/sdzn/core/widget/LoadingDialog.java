package com.sdzn.core.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.sdzn.core.R;


/**
 * 描述：
 * -
 * 创建人：鲍圣祥
 * 创建时间：2016/12/15
 */
public class LoadingDialog extends Dialog {
    private CircularView circularView;

    public LoadingDialog(Context context) {
        super(context, R.style.CustomDialog);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading);
        circularView = (CircularView) findViewById(R.id.cv_loading);
        circularView.startAnim();
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void dismiss() {
        circularView.startAnim();
        super.dismiss();
    }
}
