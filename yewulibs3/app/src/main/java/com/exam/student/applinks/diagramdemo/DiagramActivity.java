package com.exam.student.applinks.diagramdemo;

import android.os.Bundle;
import android.util.Log;

import com.blankj.utilcode.util.LogUtils;
import com.exam.student.applinks.CorrectVo;
import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.util.DateUtil;
import com.exam.student.applinks.widgits.GraphView;
import com.exam.student.applinks.widgits.MyLineChartView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author：luck
 * @data：2019/12/20 晚上 23:12
 * @描述: Demo
 */

public class DiagramActivity extends BaseActivity {
    private GraphView graphView;
    private MyLineChartView chartView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagram);
        graphView = findViewById(R.id.graphView);
        chartView=findViewById(R.id.mylineChartView);
        CorrectVo grades = getGrades();
        getCorrectSuccess(grades);
    }

    public void getCorrectSuccess(CorrectVo correctVo) {


        // 填充图表数据
        if (correctVo != null && correctVo.getData() != null) {
            List<GraphView.LineViewBean> mDatas = new ArrayList<>();
            if (correctVo.getData().getCorrectList() != null) {
                GraphView.LineViewBean lineViewBean = new GraphView.LineViewBean();
                ArrayList<GraphView.ItemInfoVo> list = new ArrayList<>();
                for (CorrectVo.DataBean.CorrectListBean correctListBean : correctVo.getData().getCorrectList()) {
                    GraphView.ItemInfoVo vo = new GraphView.ItemInfoVo();
                    vo.setValue(correctListBean.getRate() / 100);
//                    vo.setValue((float) Math.random());
                    if (DateUtil.differentDays(correctListBean.getDay(), System.currentTimeMillis()) == 0) {
                        vo.setName("今日");
                    } else if (DateUtil.getMinMonthDate(correctListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "yyyy-M-d"))
                            || DateUtil.getMaxMonthDate(correctListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "yyyy-M-d"))) {
                        vo.setName(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "d"));
                        vo.setUnit(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "M") + "月");
                    } else {
                        vo.setName(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "d"));
                    }
                    vo.setTime(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "yyyy-M-d"));
                    list.add(vo);

                }
                lineViewBean.setItemVo(list);
                lineViewBean.setColor(getResources().getColor(R.color.graphview_correct));
                lineViewBean.setShadowColor(new int[]{getResources().getColor(R.color.graphview_correct_bg), 0x00FFFFFF});
                mDatas.add(lineViewBean);
                chartView.setXValues(list);
            }
            if (correctVo.getData().getCompletionList() != null) {
                GraphView.LineViewBean lineViewBean = new GraphView.LineViewBean();
                ArrayList<GraphView.ItemInfoVo> list = new ArrayList<>();
                for (CorrectVo.DataBean.CompletionListBean completionListBean : correctVo.getData().getCompletionList()) {
                    GraphView.ItemInfoVo vo = new GraphView.ItemInfoVo();
                    vo.setValue(completionListBean.getRate() / 100);
//                    vo.setValue((float) Math.random());
                    if (DateUtil.differentDays(completionListBean.getDay(), System.currentTimeMillis()) == 0) {
                        vo.setName("今日");
                    } else if (DateUtil.getMinMonthDate(completionListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "yyyy-M-d"))
                            || DateUtil.getMaxMonthDate(completionListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "yyyy-M-d"))) {
                        vo.setName(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "d"));
                        vo.setUnit(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "M") + "月");
                    } else {
                        vo.setName(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "d"));
                    }
                    vo.setTime(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "yyyy-M-d"));
                    list.add(vo);
                }
                lineViewBean.setItemVo(list);
                lineViewBean.setColor(getResources().getColor(R.color.graphview_completion));
                lineViewBean.setShadowColor(new int[]{getResources().getColor(R.color.graphview_completion_bg), 0x00FFFFFF});
                mDatas.add(lineViewBean);
                chartView.setYValues(list);
            }
            graphView.setDatas(mDatas, false);
        }
    }


    public CorrectVo getGrades() {
        try {
            InputStream open = this.getResources().getAssets().open("correct.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                CorrectVo lists = new Gson().fromJson(json, new TypeToken<CorrectVo>() {
                }.getType());
                Log.e("aaatest", String.valueOf(lists));
                return new Gson().fromJson(json, new TypeToken<CorrectVo>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new CorrectVo();
    }

}