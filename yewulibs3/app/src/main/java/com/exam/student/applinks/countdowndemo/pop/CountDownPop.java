package com.exam.student.applinks.countdowndemo.pop;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exam.student.applinks.App;
import com.exam.student.applinks.util.DateUtil;
import com.exam.student.applinks.R;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountDownPop extends PopupWindow {

    private Activity activity;
    private TextView tvCountDown;
    private TextView SmallTvCountDown;
    private RelativeLayout rlTime;
    private LinearLayout llTime;
    private Button btnClose;

    View contentView;
    final ImageView imageView;
    ImageView smallImageView;

    RotateAnimation animation;
    boolean flag = true;
    private View view;

    public CountDownPop(final Activity activity) {
        this.activity = activity;
        contentView = LayoutInflater.from(activity).inflate(R.layout.popup_tools_countdown, null);
        tvCountDown = contentView.findViewById(R.id.tvCountDown);
        SmallTvCountDown = contentView.findViewById(R.id.Small_tvCountDown);
        rlTime = contentView.findViewById(R.id.rl_time);
        llTime = contentView.findViewById(R.id.ll_time);
        btnClose = contentView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 关闭倒计时
                dismissPop();
                App.showCountDown = false;
            }
        });

        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.setPopupWindowTouchModal(this, false);
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        imageView = (ImageView) contentView.findViewById(R.id.daojishi);
        smallImageView = (ImageView) contentView.findViewById(R.id.Small_daojishi);
        Button close = (Button) contentView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 关闭倒计时
                dismissPop();
                App.showCountDown = false;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.GONE);
                llTime.setVisibility(View.GONE);
                rlTime.setVisibility(View.VISIBLE);
                App.showSmallCountDown = false;
                dismissPop();
                showPopupWindow();
            }
        });

        rlTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.VISIBLE);
                llTime.setVisibility(View.VISIBLE);
                rlTime.setVisibility(View.GONE);
                dismissPop();
                showCenterPopupWindow();
                App.showSmallCountDown = true;
            }
        });
    }

    public void setAnimation() {
        animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(3000);//设置动画持续时间
        animation.setRepeatCount(Animation.INFINITE);//设置重复次数
        animation.setFillAfter(false);//动画执行完后是否停留在执行完的状态
        animation.setStartOffset(0);//执行前的等待时间
        LinearInterpolator lir = new LinearInterpolator();
        animation.setInterpolator(lir);
        imageView.setAnimation(animation);
        animation.startNow();


    }

    public void setSmallAnimation() {

        animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(3000);//设置动画持续时间
        animation.setRepeatCount(Animation.INFINITE);//设置重复次数
        animation.setFillAfter(false);//动画执行完后是否停留在执行完的状态
        animation.setStartOffset(0);//执行前的等待时间
        LinearInterpolator lir = new LinearInterpolator();
        animation.setInterpolator(lir);
        smallImageView.setAnimation(animation);
        animation.startNow();


    }

    public void setTime(int time) {
        String str = DateUtil.millsecondsToStr(time * 1000);
        richText(tvCountDown, str, ":");
        richText(SmallTvCountDown, str, ":");
    }

    /**
     * 设置富文本，改变textView部分文字颜色
     *
     * @param tv
     * @param str
     * @param regExp
     */
    public static void richText(TextView tv, String str, String regExp) {
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        //  Pattern p = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE);


        String pattern = "-?[0-9]\\d*";
        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);


        //  Matcher m = p.matcher(str);
        while (m.find()) {
            int start = m.start(0);
            int end = m.end(0);
            //文字字体
            TypefaceSpan ab = new TypefaceSpan("DISPLAYFREETFB");
            style.setSpan(ab, 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            style.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //指定位置文本的字体颜色
        }
        tv.setText(style);
    }

    void showPopupWindow() {
        setSmallAnimation();
        this.showAtLocation(view, Gravity.TOP | Gravity.END, 10, 20);
    }

    void showCenterPopupWindow() {
        setAnimation();
        this.showAtLocation(view, Gravity.CENTER, 0, 0);

    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(final View parent) {
        view = parent;
        if (!this.isShowing()) {
            if (App.showSmallCountDown) {
                setAnimation();
                imageView.setVisibility(View.VISIBLE);
                llTime.setVisibility(View.VISIBLE);
                rlTime.setVisibility(View.GONE);
                this.showAtLocation(parent, Gravity.CENTER, 0, 0);
            } else {
                setSmallAnimation();
                imageView.setVisibility(View.GONE);
                llTime.setVisibility(View.GONE);
                rlTime.setVisibility(View.VISIBLE);
                this.showAtLocation(parent, Gravity.TOP | Gravity.END, 10, 20);
            }
        } else {
            this.dismiss();
        }
    }

    public void dismissPop() {
        if (this.isShowing()) {
            this.dismiss();
        }
    }

    private void setPopupWindowTouchModal(PopupWindow popupWindow, boolean touchModal) {
        if (null == popupWindow) {
            return;
        }
        try {
            Method method = PopupWindow.class.getDeclaredMethod("setTouchModal",
                    boolean.class);
            method.setAccessible(true);
            method.invoke(popupWindow, touchModal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
