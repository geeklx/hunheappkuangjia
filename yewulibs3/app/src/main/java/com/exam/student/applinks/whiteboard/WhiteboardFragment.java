package com.exam.student.applinks.whiteboard;


import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseFragment;
import com.exam.student.applinks.whiteboard.even.BaiBanScreenEvent;
import com.exam.student.applinks.whiteboard.listener.CustomClicklistener;
import com.exam.student.applinks.whiteboard.views.GraffitiView;
import com.exam.student.applinks.whiteboard.views.Pen;
import com.exam.student.applinks.whiteboard.views.PopView;
import com.exam.student.applinks.whiteboard.views.PopViewhuabi;
import com.exam.student.applinks.whiteboard.views.Shape;

import org.greenrobot.eventbus.EventBus;

/**
 * 电子白板
 * 张超
 */
public class WhiteboardFragment extends BaseFragment implements View.OnClickListener {
    GraffitiView graffitiview;
    RadioButton rbExit;
    RadioButton rbqingping;
    RadioButton rbchexiao;
    RadioButton rbhuifu;
    RadioButton rbyanse;
    RadioButton rbhuabi;
    RadioButton rbtuxing;
    RadioButton rbxiangpi;
    RadioButton rbwenzi;
    RadioButton rbbeijing;
    RadioButton rbtupian;
    RadioButton rbfasong;
    RadioGroup radioGroup;

    private ObjectAnimator rlToolsHideAnimator;
    private ObjectAnimator rlToolsShowAnimator;

    private final static int NONE = -1;
    private final static int SHAPE = 0;
    private final static int ERASER = 1;
    private final static int PEN = 2;
    private int tool = NONE;

    private Shape shape;
    private int shapeColor;
    private int penColor;
    private int shapeSize;
    private int eraserSize;
    private int penSize;
    private boolean flag = true;
    PopupWindow popupWindow;

    public WhiteboardFragment() {
        // Required empty public constructor
    }

    public static WhiteboardFragment newInstance(Bundle bundle) {
        WhiteboardFragment whiteboardFragment = new WhiteboardFragment();
        whiteboardFragment.setArguments(bundle);
        return whiteboardFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_whiteboard, container, false);
        graffitiview = view.findViewById(R.id.graffitiview);
        rbExit = view.findViewById(R.id.rbExit);
        rbqingping = view.findViewById(R.id.rbqingping);
        rbchexiao = view.findViewById(R.id.rbchexiao);
        rbhuifu = view.findViewById(R.id.rbhuifu);
        rbyanse = view.findViewById(R.id.rbyanse);
        rbhuabi = view.findViewById(R.id.rbhuabi);
        rbtuxing = view.findViewById(R.id.rbtuxing);
        rbxiangpi = view.findViewById(R.id.rbxiangpi);
        rbwenzi = view.findViewById(R.id.rbwenzi);
        rbbeijing = view.findViewById(R.id.rbbeijing);
        rbtupian = view.findViewById(R.id.rbtupian);
        rbfasong = view.findViewById(R.id.rbfasong);
        radioGroup = view.findViewById(R.id.radioGroup);
        rbExit.setOnClickListener(this);
        rbqingping.setOnClickListener(this);
        rbchexiao.setOnClickListener(this);
        rbhuifu.setOnClickListener(this);
        rbyanse.setOnClickListener(this);
        rbhuabi.setOnClickListener(this);
        rbtuxing.setOnClickListener(this);
        rbxiangpi.setOnClickListener(this);
        rbwenzi.setOnClickListener(this);
        rbbeijing.setOnClickListener(this);
        rbtupian.setOnClickListener(this);
        initView();
        return view;
    }

    private void initView() {
        rbfasong.setOnClickListener(new CustomClicklistener() {
            @Override
            protected void onSingleClick() {
                getScreen();
            }

            @Override
            protected void onFastClick() {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbExit:
                activity.finish();
                break;
            case R.id.rbqingping:
                graffitiview.clear();
                break;
            case R.id.rbchexiao:
                graffitiview.undo();
                break;
            case R.id.rbhuifu:
                graffitiview.redo();
                break;
            case R.id.rbyanse:
                popupWindow = PopView.showColorPopupWindow(rbyanse, activity);
                popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
                PopView.setOnColorClick(new PopView.onColorClick() {
                    @Override
                    public void onClickColor(int color) {
                        switch (color) {
                            case 1:
                                graffitiview.setColor(getResources().getColor(R.color.white));
                                break;
                            case 2:
                                graffitiview.setColor(getResources().getColor(R.color.palette_red));
                                break;
                            case 3:
                                graffitiview.setColor(getResources().getColor(R.color.black));
                                break;
                            case 4:
                                graffitiview.setColor(getResources().getColor(R.color.palette_yellow));
                                break;
                            case 5:
                                graffitiview.setColor(getResources().getColor(R.color.palette_green));
                                break;
                        }
                    }
                });
                break;
            case R.id.rbhuabi:
                PopViewhuabi.showPenPopupWindow(rbyanse, activity).showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
                PopViewhuabi.setOnPenClick(new PopViewhuabi.onPenClick() {
                    @Override
                    public void onClickPen(int size) {
                        graffitiview.setPen(Pen.HAND);
                        graffitiview.setPaintSize(size);
                    }
                });
                break;
            case R.id.rbxiangpi:
                PopView.showXinagpiPopupWindow(rbyanse, activity).showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
                PopView.setonXinagPiClick(new PopView.onXiangPiClick() {
                    @Override
                    public void onClickTuXing(int size) {
                        graffitiview.setPen(Pen.ERASER);
                        graffitiview.setShape(Shape.HAND_WRITE);
                        graffitiview.setEraserSize(size);
                    }
                });
                break;
            case R.id.rbtuxing:
                PopView.showtuxingPopupWindow(rbyanse, activity).showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
                PopView.setonTuXingClick(new PopView.onTuXingClick() {
                    @Override
                    public void onClickTuXing(int type) {
                        switch (type) {
                            case 1:
                                graffitiview.setShape(Shape.LINE);
                                break;
                            case 2:
                                graffitiview.setShape(Shape.HOLLOW_RECT);
                                break;
                            case 3:
                                graffitiview.setShape(Shape.HOLLOW_CIRCLE);
                                break;
                            case 4:
                                graffitiview.setShape(Shape.TRIANGLE);
                                break;
                        }
                    }
                });
                break;
            case R.id.rbwenzi:
                break;
            case R.id.rbbeijing:
                try {
                    if (flag) {
                        graffitiview.setColorBg(Color.parseColor("#24604C"));
                        flag = false;
                    } else {
                        graffitiview.setColorBg(Color.parseColor("#ffffff"));
                        flag = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rbtupian:
                break;
        }
    }

    /**
     * 截屏
     */
    public void getScreen() {
        EventBus.getDefault().post(new BaiBanScreenEvent());
    }


}
