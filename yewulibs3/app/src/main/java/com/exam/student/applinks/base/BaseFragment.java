package com.exam.student.applinks.base;

import android.app.Application;
import android.os.Bundle;

import androidx.fragment.app.Fragment;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BaseFragment extends Fragment {

    public BaseActivity activity;
    protected Application appContext;
    private long mCurrentMs = System.currentTimeMillis();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
        appContext = activity.getApplication();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

    @Override
    public void onDestroy() {
        activity = null;
        appContext = null;
        super.onDestroy();
    }
}