package com.exam.student.applinks.teachingcoueses.classrecord;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.teachingcoueses.CoursesDataBindingBean;
import com.exam.student.applinks.teachingcoueses.TeachingCoursesAdapter;
import com.exam.student.applinks.teachingcoueses.TeachingCoursesBean;
import com.exam.student.applinks.teachingcoueses.TeachingCoursesPresenter;
import com.exam.student.applinks.teachingcoueses.TeachingCoursesViews;

import java.util.ArrayList;
import java.util.List;

public class ClassRecordActivity extends BaseActivity implements ClassRecordViews {
    ClassRecordPresenter classRecordPresenter;
    private RecyclerView rvClassRecord;
    private ClassRecordAdapter classRecordAdapter;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token = "Bearer 26459844-34ba-424a-ad94-e6aa2ccf2046";
        setContentView(R.layout.activity_class_record);
        classRecordPresenter = new ClassRecordPresenter();
        classRecordPresenter.onCreate(this);
        classRecordPresenter.queryClassRecord(token);
        rvClassRecord = findViewById(R.id.rv_class_record);
        onclick();

    }

    private List<ClassRecordDataBindingBean> mData;

    private void donetwork(List<ClassRecordBean.RowsBean> treeBeans) {
        mData = new ArrayList<>();
        if (treeBeans.size() == 0 || treeBeans == null) {
            return;
        } else {
            for (int i = 0; i < treeBeans.size(); i++) {
                mData.add(new ClassRecordDataBindingBean(treeBeans.get(i).getName(),  treeBeans.get(i).getClassName(),treeBeans.get(i).getCreateTime()));
            }
            classRecordAdapter.setNewData(mData);
        }
    }

    private void onclick() {
        rvClassRecord.setLayoutManager(new LinearLayoutManager(this));
        classRecordAdapter = new ClassRecordAdapter();
        rvClassRecord.setAdapter(classRecordAdapter);
        classRecordAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ToastUtils.showLong(position);
            }
        });
    }

    @Override
    public void onCehuaSuccess(ClassRecordBean classRecordBean) {
        donetwork(classRecordBean.getRows());
    }


    @Override
    public void onCehuaNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onCehuaFail(String msg) {
        ToastUtils.showLong(msg);
    }


}
