package com.exam.student.applinks.recyclerDemo;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.recyclerDemo.adapter.MyAdapter;
import com.exam.student.applinks.recyclerDemo.modle.TextModel;
import com.exam.student.applinks.recyclerDemo.modle.UpdateAvatarEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity2 extends BaseActivity {
    private List<TextModel> mTextModels;
    private RecyclerView rvWithdrawal;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal);
        EventBus.getDefault().post(new UpdateAvatarEvent(1));
        rvWithdrawal = findViewById(R.id.rl_withdrawabl);
        initData();
        initRv();
    }

    private void initRv() {
        rvWithdrawal.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(RecyclerActivity2.this, mTextModels);
        rvWithdrawal.setAdapter(myAdapter);
    }

    private void initData() {
        mTextModels = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            TextModel textModel = new TextModel("1405" + i);
            mTextModels.add(textModel);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
