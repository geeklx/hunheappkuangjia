package com.exam.student.applinks;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.base.BaseActivity;

public class SpActivity extends BaseActivity {
    private EditText et_inputs;
    private Button btn_sp;
    private TextView tv_ceshi;

    String aaaa = SPUtils.getInstance().getString("url");
    String bbbb = aaaa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp);
        et_inputs = findViewById(R.id.et_inputs);
        btn_sp = findViewById(R.id.btn_sp);
        tv_ceshi = findViewById(R.id.tv_ceshi);
        tv_ceshi.setText(bbbb);
        btn_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = et_inputs.getText().toString().trim();
                if (input != null) {
                    SPUtils.getInstance().put("url", input);
                    ToastUtils.showLong("设置成功");
                    tv_ceshi.setText(bbbb);
                } else {
                    ToastUtils.showLong("请设置内容");
                }
            }
        });
    }
}
