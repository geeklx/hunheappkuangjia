package com.exam.student.applinks.basedemo;

public interface BaseOnClickListener {
    //个人中心
    void Titlegrzx();

    //时间
    void Titleshijian();

    //展开时间
    void Titlezankaishijian();

    /*搜索*/
    void Titlesousuo();

    /*提交*/
    void Titletijiao();

    /*下拉加载*/
    void TitleDropdown();

    /*返回*/
    void TitleBack();
}
