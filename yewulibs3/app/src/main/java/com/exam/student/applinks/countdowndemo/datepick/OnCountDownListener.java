package com.exam.student.applinks.countdowndemo.datepick;


public interface OnCountDownListener {
    void onClick(CountDownPickWheelDialog dialog);
}
