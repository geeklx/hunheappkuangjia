package com.exam.student.applinks.teachingcoueses;

import com.alibaba.fastjson.JSONObject;
import com.blankj.utilcode.util.LogUtils;
import com.exam.student.applinks.newwork.api.Api;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class TeachingCoursesPresenter extends Presenter<TeachingCoursesViews> {

    public void queryTree(String token) {
        JSONObject requestData = new JSONObject();
        requestData.put("lessonId", "534");
//        requestData.put("chapterId", 2002623);
//        requestData.put("page", 1);
//        requestData.put("limit", 10);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());



        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryTree(token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<TeachingCoursesBean1>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<TeachingCoursesBean1>> call, Response<ResponseSlbBean1<TeachingCoursesBean1>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCehuaNodata(response.body().getMessage());
                            return;
                        }
                        getView().onCehuaSuccess(response.body().getResult());
                        LogUtils.e(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<TeachingCoursesBean1>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCehuaFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });

//        RetrofitNetNew.build(Api.class, getIdentifier())
//                .queryTree(token, requestBody)
//                .enqueue(new Callback<ResponseSlbBean1<List<TreeBean>>>() {
//                    @Override
//                    public void onResponse(Call<ResponseSlbBean1<List<TreeBean>>> call, Response<ResponseSlbBean1<List<TreeBean>>> response) {
//                        if (!hasView()) {
//                            return;
//                        }
//                        if (response.body() == null) {
//                            return;
//                        }
//                        if (response.body().getCode() != 0) {
//                            getView().onCehuaNodata(response.body().getMessage());
//                            return;
//                        }
//                        getView().onCehuaSuccess(response.body().getResult());
//                        call.cancel();
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseSlbBean1<List<TreeBean>>> call, Throwable t) {
//                        if (!hasView()) {
//                            return;
//                        }
//                        String string = BanbenUtils.getInstance().error_tips;
//                        getView().onCehuaFail(string);
//                        t.printStackTrace();
//                        call.cancel();
//                    }
//                });
    }
}
