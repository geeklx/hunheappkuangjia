package com.exam.student.applinks.timeselector;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.util.StringUtils;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;

import java.util.Calendar;

public class TimeSelectorActivity extends BaseActivity {

    private String startTime;
    private String endTime;
    private Button btnTimer;
    private TextView tvDate;

    private CalenderDialog calendarDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_selector);
        btnTimer = (Button) findViewById(R.id.btn_timer);
        tvDate = (TextView) findViewById(R.id.tvDate);
        btnTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (TextUtils.isEmpty(startTime) || TextUtils.isEmpty(endTime)) {
//                }
                showCalendarDialog();
            }
        });
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarDialog();
            }
        });
    }

    private void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(activity, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    tvDate.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  至  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    startTime = String.valueOf(startCalendar.getTimeInMillis());
                    endTime = String.valueOf(endCalendar.getTimeInMillis());
                }
            });
        }
        calendarDialog.show();
    }
}
