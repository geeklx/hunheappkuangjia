package com.exam.student.applinks.basedemo;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.basedemo.bean.VersionInfoBean;
import com.exam.student.applinks.basedemo.presenter.CheckverionPresenter;
import com.exam.student.applinks.basedemo.view.CheckverionView;
import com.example.baselibrary.emptyview.EmptyView;
import com.just.agentweb.WebViewClient;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;

public class BaseActDemoYewu extends BaseActDemo implements BaseOnClickListener, CheckverionView {
    CheckverionPresenter checkverionPresenter;
    protected SmartRefreshLayout refreshLayout1;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        emptyview1.loading();
        findview();
        onclick();
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("3", "0");
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                checkverionPresenter.checkVerion("3", "0");
            }
        });
        //使上拉加载具有弹性效果
        refreshLayout1.setEnableAutoLoadmore(false);
        //禁止越界拖动（1.0.4以上版本）
        refreshLayout1.setEnableOverScrollDrag(false);
        //关闭越界回弹功能
        refreshLayout1.setEnableOverScrollBounce(false);
        refreshLayout1.setScrollBoundaryDecider(new ScrollBoundaryDecider() {
            @Override
            public boolean canRefresh(View content) {
                //webview滚动到顶部才可以下拉刷新
                MyLogUtil.e("ssssss", "" + mAgentWeb.getWebCreator().getWebView().getScrollY());
                return mAgentWeb.getWebCreator().getWebView().getScrollY() > 0;
            }

            @Override
            public boolean canLoadmore(View content) {
                return false;
            }
        });

        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                checkverionPresenter.checkVerion("3", "0");
            }
        });
        ShowAgentweb();
    }

    private void ShowAgentweb() {
        mAgentWeb.getWebCreator().getWebView().setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView contentWebView, String url) {
                super.onPageFinished(contentWebView, url);
                writeLocalStorage(contentWebView);
            }
        });
    }

    /**
     * 写入LocalStorage
     */
    private void writeLocalStorage(WebView contentWebView) {
        String token = "dasdasd123123123123123123";
        String phone = "13717870853";
        if (TextUtils.isEmpty(token)) {
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (contentWebView != null) {
                contentWebView.evaluateJavascript("window.localStorage.setItem('" + "token" + "','" + token + "');", null);
//                contentWebView.evaluateJavascript("window.localStorage.setItem('" + "phone" + "','" + phone + "');", null);
            }
        } else {
            if (contentWebView != null) {
                contentWebView.loadUrl("javascript:localStorage.setItem('" + "token" + "','" + token + "');");
//                contentWebView.loadUrl("javascript:localStorage.setItem('" + "phone" + "','" + phone + "');");
            }
        }
    }


    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        TitleShowHideState(1);
        setBaseOnClickListener(this);
    }

    private TextView localStorage1;
    private TextView localStorage2;

    private void findview() {
        refreshLayout1 = findViewById(R.id.refreshLayout1_order);
        localStorage1 = findViewById(R.id.localStorage1);
        localStorage2 = findViewById(R.id.localStorage2);
        localStorage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.BaseActDemoYewu1");
                intent.putExtra(URL_KEY, "http://192.168.6.77:8081/1.html");
                startActivity(intent);
            }
        });
        localStorage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.BaseActDemoYewu2");
                intent.putExtra(URL_KEY, "http://192.168.6.77:8081/2.html");
                startActivity(intent);
            }
        });

    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.ll_base_container);
    }

    private void onclick() {
    }


    //个人中心
    @Override
    public void Titlegrzx() {
        ToastUtils.showLong("点击了个人中心");
    }

    @Override
    public void Titleshijian() {
        showCalendarDialog();
        BaseActDemo.setOnDisplayRefreshListener(new refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });
    }

    /*展开时间*/
    @Override
    public void Titlezankaishijian() {
        showCalendarDialog();
        BaseActDemo.setOnDisplayRefreshListener(new refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });
    }

    //搜索
    @Override
    public void Titlesousuo() {
        ToastUtils.showLong("点击了搜索带刷新效果");
        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    public void Titletijiao() {
        ToastUtils.showLong("点击了提交");
    }

    @Override
    public void TitleDropdown() {
        ToastUtils.showLong("点击了下拉列表");
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
//        ToastUtils.showLong("OnUpdateVersionSuccess" + versionInfoBean.getProgramName());
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
//        AgentwebRefresh("https://www.baidu.com/");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        ToastUtils.showLong("OnUpdateVersionNodata" + bean);
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showLong("OnUpdateVersionFail" + msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
