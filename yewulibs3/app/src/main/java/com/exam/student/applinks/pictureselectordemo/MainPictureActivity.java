package com.exam.student.applinks.pictureselectordemo;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.AppUtils;
import com.exam.student.applinks.R;


/**
 * @author：luck
 * @data：2019/12/20 晚上 23:12
 * @描述: Demo
 */

public class MainPictureActivity extends AppCompatActivity {
    TextView singleImageUpload,multiImageUpload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_picture);
        singleImageUpload=(TextView) findViewById(R.id.single_image_upload);
        multiImageUpload=(TextView) findViewById(R.id.multi_image_upload);
        singleImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SinglePictureActivity"));
            }
        });
        multiImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.MultiPictureActivity"));
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}

