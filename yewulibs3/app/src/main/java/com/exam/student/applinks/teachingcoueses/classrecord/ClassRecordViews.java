package com.exam.student.applinks.teachingcoueses.classrecord;

import com.haier.cellarette.libmvp.mvp.IView;

/**
 * 首页 view
 */
public interface ClassRecordViews extends IView {
    void onCehuaSuccess(ClassRecordBean teachingCoursesBean);

    void onCehuaNodata(String msg);

    void onCehuaFail(String msg);
}
