package com.exam.student.applinks.whiteboard.listener;

/**
 * Created by wangc on 2018/6/20 0020.
 */

public interface PathStackWatcher {
    void pathStackChange(int size);
}
