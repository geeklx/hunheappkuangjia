package com.exam.student.applinks.teachingcoueses;

import android.text.Html;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.exam.student.applinks.R;

public class TeachingCoursesAdapter extends BaseQuickAdapter<CoursesDataBindingBean, BaseViewHolder> {

    public TeachingCoursesAdapter() {
        super(R.layout.recycleview_teaching_courses_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, CoursesDataBindingBean item) {
        TextView tvCreateTime = helper.itemView.findViewById(R.id.tv_createTime);//创建时间
        TextView tvCourseName = helper.itemView.findViewById(R.id.tv_courseName);//课程名称
        TextView tvChapterName = helper.itemView.findViewById(R.id.tv_chapterName);//章节名称
        TextView tvTestingNum = helper.itemView.findViewById(R.id.tv_testingNum);//检测
        TextView tvSourseNum = helper.itemView.findViewById(R.id.tv_sourseNum);//资料
        tvCreateTime.setText("创建时间：" + item.getCreateTime());
        tvCourseName.setText(item.getCourseName());
        tvChapterName.setText(item.getChapterName());
        String strMsg = "检测  <font color=\"#FA541C\">" + item.getTestingNum() + "</font>";
        tvTestingNum.setText(Html.fromHtml(strMsg));
        String strMsg1 = "资料  <font color=\"#FA541C\">" + item.getSourseNum() + "</font>";
        tvSourseNum.setText(Html.fromHtml(strMsg1));
    }
}
