package com.exam.student.applinks.androidtreeview;

public class DataInfoBean {
    /**
     * id : 2007096
     * name : 文言虚词的意义和用法
     * parentId : 2007091
     * parentName : 文言文阅读
     * seq : 4
     * bookVolumeId : 699
     * bookVolumeName : 必修一
     * bookVersionId : 10257
     * bookVersionName : 语文版
     * subjectId : 3
     * subjectName : 语文
     * levelId : 3
     * levelName : 高中
     * nodeIdPath :
     * nodeNamePath :
     * isDelete : 0
     */

    private int id;
    private String name;
    private int parentId;
    private String parentName;
    private int seq;
    private int bookVolumeId;
    private String bookVolumeName;
    private int bookVersionId;
    private String bookVersionName;
    private int subjectId;
    private String subjectName;
    private int levelId;
    private String levelName;
    private String nodeIdPath;
    private String nodeNamePath;
    private int isDelete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getBookVolumeId() {
        return bookVolumeId;
    }

    public void setBookVolumeId(int bookVolumeId) {
        this.bookVolumeId = bookVolumeId;
    }

    public String getBookVolumeName() {
        return bookVolumeName;
    }

    public void setBookVolumeName(String bookVolumeName) {
        this.bookVolumeName = bookVolumeName;
    }

    public int getBookVersionId() {
        return bookVersionId;
    }

    public void setBookVersionId(int bookVersionId) {
        this.bookVersionId = bookVersionId;
    }

    public String getBookVersionName() {
        return bookVersionName;
    }

    public void setBookVersionName(String bookVersionName) {
        this.bookVersionName = bookVersionName;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getNodeIdPath() {
        return nodeIdPath;
    }

    public void setNodeIdPath(String nodeIdPath) {
        this.nodeIdPath = nodeIdPath;
    }

    public String getNodeNamePath() {
        return nodeNamePath;
    }

    public void setNodeNamePath(String nodeNamePath) {
        this.nodeNamePath = nodeNamePath;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }
}
