package com.exam.student.applinks.teachingcoueses;

import java.util.List;

public class TeachingCoursesBean {

    /**
     * rows : [{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-08 14:24:58","lessonId":456,"sourseNum":3,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-08 10:41:07","lessonId":452,"sourseNum":0,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-07 11:11:37","lessonId":448,"sourseNum":1,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-04 17:23:57","lessonId":441,"sourseNum":1,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-04 16:15:28","lessonId":440,"sourseNum":0,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"阿巴阿巴","createTime":"2020-12-04 15:34:50","lessonId":439,"sourseNum":0,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-02 14:13:12","lessonId":430,"sourseNum":0,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-02 14:07:37","lessonId":429,"sourseNum":0,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-02 13:54:28","lessonId":427,"sourseNum":0,"testingNum":1},{"chapterName":"第一单元 > 春","classes":[{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}],"courseName":"春","createTime":"2020-12-02 11:57:15","lessonId":425,"sourseNum":1,"testingNum":1}]
     * total : 11
     */

    private int total;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * chapterName : 第一单元 > 春
         * classes : [{"classesId":112,"classesName":"初一一班","gradeId":7,"gradeName":"初一","schoolClassesGroupsRes":[]}]
         * courseName : 春
         * createTime : 2020-12-08 14:24:58
         * lessonId : 456
         * sourseNum : 3
         * testingNum : 1
         */

        private String chapterName;
        private String courseName;
        private String createTime;
        private int lessonId;
        private String sourseNum;
        private String testingNum;
        private List<ClassesBean> classes;

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getSourseNum() {
            return sourseNum;
        }

        public void setSourseNum(String sourseNum) {
            this.sourseNum = sourseNum;
        }

        public String getTestingNum() {
            return testingNum;
        }

        public void setTestingNum(String testingNum) {
            this.testingNum = testingNum;
        }

        public List<ClassesBean> getClasses() {
            return classes;
        }

        public void setClasses(List<ClassesBean> classes) {
            this.classes = classes;
        }

        public static class ClassesBean {
            /**
             * classesId : 112
             * classesName : 初一一班
             * gradeId : 7
             * gradeName : 初一
             * schoolClassesGroupsRes : []
             */

            private int classesId;
            private String classesName;
            private int gradeId;
            private String gradeName;
            private List<?> schoolClassesGroupsRes;

            public int getClassesId() {
                return classesId;
            }

            public void setClassesId(int classesId) {
                this.classesId = classesId;
            }

            public String getClassesName() {
                return classesName;
            }

            public void setClassesName(String classesName) {
                this.classesName = classesName;
            }

            public int getGradeId() {
                return gradeId;
            }

            public void setGradeId(int gradeId) {
                this.gradeId = gradeId;
            }

            public String getGradeName() {
                return gradeName;
            }

            public void setGradeName(String gradeName) {
                this.gradeName = gradeName;
            }

            public List<?> getSchoolClassesGroupsRes() {
                return schoolClassesGroupsRes;
            }

            public void setSchoolClassesGroupsRes(List<?> schoolClassesGroupsRes) {
                this.schoolClassesGroupsRes = schoolClassesGroupsRes;
            }
        }
    }
}
