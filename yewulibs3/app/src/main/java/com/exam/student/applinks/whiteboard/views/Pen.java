package com.exam.student.applinks.whiteboard.views;

/**
 * 画笔
 */
public enum Pen {
    HAND, // 手绘
    HIGHLIGHTER,//荧光笔
    COPY, // 仿制
    ERASER // 橡皮擦
}

