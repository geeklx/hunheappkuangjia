package com.exam.student.applinks.videoplaydemo.utlis.floatUtil;

/**
 * Created by yhao on 2017/11/14.
 * https://github.com/yhaolpz
 */
interface PermissionListener {
    void onSuccess();

    void onFail();
}
