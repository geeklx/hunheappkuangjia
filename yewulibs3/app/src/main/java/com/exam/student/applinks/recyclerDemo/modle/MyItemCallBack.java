package com.exam.student.applinks.recyclerDemo.modle;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

public class MyItemCallBack extends DiffUtil.ItemCallback<TextModel> {
    @Override
    public boolean areItemsTheSame(@NonNull TextModel oldItem, @NonNull TextModel newItem) {
        return TextUtils.equals(oldItem.getTextTitle(), newItem.getTextTitle());
    }

    @Override
    public boolean areContentsTheSame(@NonNull TextModel oldItem, @NonNull TextModel newItem) {
        return TextUtils.equals(oldItem.getTextContent(), newItem.getTextContent());
    }
}
