package com.exam.student.applinks.teachingcoueses;

import com.haier.cellarette.libmvp.mvp.IView;

/**
 * 首页 view
 */
public interface TeachingCoursesViews extends IView {
    void onCehuaSuccess(TeachingCoursesBean1 teachingCoursesBean);

    void onCehuaNodata(String msg);

    void onCehuaFail(String msg);
}
