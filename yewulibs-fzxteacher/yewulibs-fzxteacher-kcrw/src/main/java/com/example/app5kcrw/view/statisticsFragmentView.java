package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.GroupStudent;
import com.sdzn.fzx.teacher.vo.GroupStudentDetils;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StudentList;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public interface statisticsFragmentView extends BaseView {
    void StudentList(StudentList studentList);

    void StudentStatics(StatisticsVo statisticsVo);
    void GroupStudentStatics(GroupStudent groupStudent);
    void GroupDetilsStatics(GroupStudentDetils groupStudentDetils);
}
