package com.example.app5kcrw.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5kcrw.adapter.CorrectingWayAdapter;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5kcrw.presenter.CorrectingWayPresenter;
import com.example.app5kcrw.view.CorrectingWayView;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.CorrectingWayVo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 批改方式
 *
 * @author zs
 */

public class CorrectingWayActivity extends MBaseActivity<CorrectingWayPresenter> implements CorrectingWayView, View.OnClickListener {

    //批改方式RadioGroup
    RadioGroup rgRandom;
    //指定学生批改
    RadioButton rbonly;
    //学生随机批改
    RadioButton rbrandom;
    //批改范围
    RadioGroup rgRange;
    //全班
    RadioButton wholeClass;
    //组内
    RadioButton rbGroup;
    //控制现在组内信息LinearLayout
    LinearLayout includeOnly;
    //取消
    LinearLayout cancel;
    //确定
    LinearLayout confirm;
    //分组列表
    RecyclerView rv_correcting;
    private TextView btSeve;

    private boolean isClass = true;
    private String classId;//当前年级id
    private String libId;//当前任务页id
    private CorrectingWayAdapter correctingWayAdapter;
    private List<CorrectingWayVo> correctingBeanslist;
    private int correctType = 1;//批改方式
    private int correctRange = 1;//批改范围

    ArrayList<StudentId> StudentIdList = null;//提交批改用的json保存的集合
    List<StudentLocation> selectStudent;//新封装获取选中资料用的
    private int GroupId;//分组id
    private int StudentId;//学生id
    private String studentjson;//转换的json

    private final int RANGE_CLASS = 0;//全班
    private final int RANGE_GROUP = 1;//组内

    @Override
    public void initPresenter() {
        mPresenter = new CorrectingWayPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correcting_way);
        rgRandom = (RadioGroup) findViewById(R.id.rg_random);
        rbonly = (RadioButton) findViewById(R.id.rb_only);
        rbrandom = (RadioButton) findViewById(R.id.rb_random);
        includeOnly = (LinearLayout) findViewById(R.id.include_only);
        rgRange = (RadioGroup) findViewById(R.id.rg_range);
        wholeClass = (RadioButton) findViewById(R.id.rb_class);
        rbGroup = (RadioButton) findViewById(R.id.rb_group);
        rv_correcting = (RecyclerView) findViewById(R.id.correcting_reclycleview);
        cancel = (LinearLayout) findViewById(R.id.cancel);
        confirm = (LinearLayout) findViewById(R.id.confirm);
        btSeve = (TextView) findViewById(R.id.bt_seve);
        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入CorrectingWayActivity成功");
                    classId = appLinkIntent.getStringExtra("classId");//获取到的当前班级id
                    libId = appLinkIntent.getStringExtra("libId");//当前界面的任务id
                }
            }
        }
        initView();
        initData();
    }

    @Override
    protected void initView() {

        //批改方式
        rgRandom.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_only) {//指定学生批改
                    includeOnly.setVisibility(View.VISIBLE);
                    correctType = 1;
                } else if (checkedId == R.id.rb_random) {//随机批改
                    includeOnly.setVisibility(View.GONE);
                    correctType = 2;
                }
            }
        });
        //批改范围
        rgRange.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_class) {//全班
                    correctRange = 1;
                } else if (checkedId == R.id.rb_group) {//组内
                    correctRange = 2;
                }
            }
        });
    }


    private void getCorrectingList() {
        // mPresenter.getCorrectingList("393");
    }

    public boolean saveChecking(int range, List<CorrectingWayVo> correctingList) {
        boolean ok = false;
        for (int i = 0; i < correctingList.size(); i++) {
            for (CorrectingWayVo.StudentsBean studentsBean : correctingList.get(i).getStudents()) {
                if (studentsBean.isStatus()) {//拿到选中状态判断
                    ok = true;
                    break;
                }
            }
            if (range == RANGE_CLASS) {
                if (ok) {
                    return true;
                }
            } else {
                Log.e("aaatest", "组内保存");
                if (ok) {
                    //判断是否为最后一个组
                    //如果不为租后一个组
                    //  重置状态
                    if (i != correctingList.size() - 1) {
                        ok = false;
                    }
                } else {
                    return false;
                }
            }
        }
        if (ok) {
            return true;
        } else {
            return false;
        }
    }

    //把位置和选中的人的信息添加到StudentLocation集合中
    public List<StudentLocation> getSelectStudent(List<CorrectingWayVo> CorrectingWayVoList) {
        ArrayList<StudentLocation> studentLocationList = null;
        for (int i = 0; i < CorrectingWayVoList.size(); i++) {
            CorrectingWayVo correctingWayVo = CorrectingWayVoList.get(i);
            for (int y = 0; y < correctingWayVo.getStudents().size(); y++) {
                CorrectingWayVo.StudentsBean studentsBean = correctingWayVo.getStudents().get(y);
                if (studentsBean.isStatus()) {
                    if (studentLocationList == null)
                        studentLocationList = new ArrayList<>();
                    studentLocationList.add(new StudentLocation(i, y, correctingWayVo, studentsBean));
                }
            }
        }
        return studentLocationList;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.cancel) {
            CorrectingWayActivity.this.finish();
        } else if (id == R.id.confirm) {
            if (correctType == 2) {
                saveData(studentjson);
            } else {
                int i = wholeClass.isChecked() ? RANGE_CLASS : RANGE_GROUP;
                if (saveChecking(i, correctingBeanslist)) {
                    selectStudent = getSelectStudent(correctingBeanslist);
                    if (selectStudent != null) {
                        for (StudentLocation StudentLocation : selectStudent) {
                            int groupIndex = StudentLocation.getGroupIndex(); //所在分组
                            int studentsIndex = StudentLocation.getStudentsIndex();//所在位置
                            CorrectingWayVo correctingWayVo = StudentLocation.getCorrectingWayVo();//分组
                            String groupName = correctingWayVo.getGroupName();//分组名称
                            CorrectingWayVo.StudentsBean students = StudentLocation.getStudent();//学生
                            String studentName = students.getStudentName();//学生名称
                            Log.e("aaatest", String.format("分组名称:%s\t分组位置:%d\t学生名称:%s\t学生位置:%d", groupName, groupIndex, studentName, studentsIndex));
                            GroupId = correctingWayVo.getGroupId();//分组id
                            StudentId = students.getStudentId();//学生id
                            if (StudentIdList == null)
                                StudentIdList = new ArrayList<>();
                            StudentIdList.add(new StudentId(GroupId, StudentId));
                        }
                        Gson gson = new Gson();
                        studentjson = gson.toJson(StudentIdList);
                        Log.e("aaatest", "转化的json" + studentjson);
                    } else {
                        //沒有已选中的学生，系统可能发生了一些问题
                        ToastUtil.showShortlToast("您没有选择批改人");
                    }
                    //允許保存
                    saveData(studentjson);
                    Log.e("aaatest", "保存成功");
                } else {
                    if (i == RANGE_CLASS) {
                        //至少选择一名学生
                        ToastUtil.showShortlToast("全班至少选择一名批改人");
                    } else {
                        //每个小组 至少选择一名学生
                        ToastUtil.showShortlToast("每个小组至少选择一名批改人");
                    }
                    return;
                }
            }
        }

    }

    private void saveData(String json) {
        Map<String, Object> param = new HashMap<>();
        param.put("classId", classId);//班级id
        param.put("lessonTaskId", libId);//前小模块的id
        param.put("correctType", correctType);
        param.put("correctRange", correctRange);
        param.put("students", String.valueOf(json));
        Log.e("aaatest", "進入确定" + param);
        Network.createTokenService(NetWorkService.CorrectingWayService.class)
                .SaveCorrectionMethod(param)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("aaatest", "error" + e.toString());
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {
                            }
                        } else {
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        Log.e("aaatest", "aaatest" + o.toString());
                        ToastUtil.showShortlToast("保存成功");
                        CorrectingWayActivity.this.finish();
                    }
                }, this, true, false));
    }

    protected void initData() {
        Network.createTokenService(NetWorkService.CorrectingWayService.class)
                .getCorrectingList(classId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("infos--->error", e.toString());
                    }

                    @Override
                    public void onNext(Object object) {
                        Gson gson = new Gson();
                        String jsonOrg = gson.toJson(object);
                        Log.i("infos--->success", jsonOrg);
                        try {
                            JSONObject dataJson = new JSONObject(jsonOrg).getJSONObject("result").getJSONObject("data");
                            Iterator<String> keys = dataJson.keys();
                            correctingBeanslist = new ArrayList<>();
                            while (keys.hasNext()) {
                                String key = keys.next();
                                CorrectingWayVo correctingWayVo = gson.fromJson(dataJson.getJSONObject(key).toString(), CorrectingWayVo.class);
                                CorrectingWayVo correctingWayVo1bean = new CorrectingWayVo(correctingWayVo.getGroupName());//分组标题
                                Log.i("infos--->correctingWayVo1bean", correctingWayVo1bean.toString());
                                correctingBeanslist.add(correctingWayVo);
                                if (correctingWayVo.getStudents() == null) {
                                    continue;
                                }
                                for (CorrectingWayVo.StudentsBean studentsBean : correctingWayVo.getStudents()) {
                                    correctingWayVo1bean.addstudents(new CorrectingWayVo.StudentsBean(studentsBean.getStudentName()));//组员名称
                                }
                            }
                            Log.i("infos--->correctingBeanslist", correctingBeanslist.toString());
                            correctingWayAdapter = new CorrectingWayAdapter(correctingBeanslist);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
                            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rv_correcting.setLayoutManager(layoutManager);
                            rv_correcting.setFocusableInTouchMode(false);
                            rv_correcting.setAdapter(correctingWayAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void getCorrectSuccess(CorrectingWayVo correctingWayVo) {

    }

    private class StudentLocation {
        private int groupIndex;//分组位置
        private int studentsIndex;//学生位置
        private CorrectingWayVo CorrectingWayVo;//分组
        private CorrectingWayVo.StudentsBean student;//学生

        public StudentLocation(int groupIndex, int studentsIndex, com.sdzn.fzx.teacher.vo.CorrectingWayVo correctingWayVo, com.sdzn.fzx.teacher.vo.CorrectingWayVo.StudentsBean student) {
            this.groupIndex = groupIndex;
            this.studentsIndex = studentsIndex;
            CorrectingWayVo = correctingWayVo;
            this.student = student;
        }

        public int getGroupIndex() {
            return groupIndex;
        }

        public int getStudentsIndex() {
            return studentsIndex;
        }

        public CorrectingWayVo getCorrectingWayVo() {
            return CorrectingWayVo;
        }

        public CorrectingWayVo.StudentsBean getStudent() {
            return student;
        }
    }

    private class StudentId {
        private int groupId;//分组id
        private int studentId;//学生id

        public StudentId(int groupId, int studentId) {
            this.groupId = groupId;
            this.studentId = studentId;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public int getStudentId() {
            return studentId;
        }

        public void setStudentId(int studentId) {
            this.studentId = studentId;
        }
    }
}
