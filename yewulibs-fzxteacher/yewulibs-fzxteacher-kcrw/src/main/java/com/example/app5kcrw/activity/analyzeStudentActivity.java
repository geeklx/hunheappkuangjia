package com.example.app5kcrw.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5kcrw.adapter.analyzeStudentDetailAdapter;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.StudentDetailExam;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class analyzeStudentActivity extends Activity {
    private TextView tvBack;
    private TextView tvTitle;
    private TextView xx;
    private View line;
    private RecyclerView mRecyclerView;

    private List<StudentDetailExam.DataBean> mList = new ArrayList<>();

    private String lessonTaskStudentId;
    private analyzeStudentDetailAdapter analyzeStudentDetailAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analyze_student);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        xx = (TextView) findViewById(R.id.xx);
        line = (View) findViewById(R.id.line);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        xx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        analyzeStudentDetailAdapter = new analyzeStudentDetailAdapter(this, mList, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(analyzeStudentDetailAdapter);
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入analyzeStudentActivity成功");
                }
            }
        }
    }


    protected void initData() {
        lessonTaskStudentId = getIntent().getIntExtra("id", 0) + "";
        tvBack.setText(getIntent().getStringExtra("name"));
        getStudentDetailList(lessonTaskStudentId);
    }


    public void setStudentDetailExam(StudentDetailExam studentDetailExam) {

        mList.clear();
        StudentDetailExam.DataBean data = studentDetailExam.getData();
        mList.add(data);
        refreshAdapter();

    }

    private void refreshAdapter() {
        analyzeStudentDetailAdapter.notifyDataSetChanged();
    }


    public void getStudentDetailList(String id) {

        Network.createTokenService(NetWorkService.GetExamInfoService.class)
                .GetStudentList(id)
                .map(new StatusFunc<StudentDetailExam>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentDetailExam>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StudentDetailExam o) {
                        initJsonData(o);
                        setStudentDetailExam(o);
                    }
                });
    }


    private void initJsonData(StudentDetailExam beans) {
        StudentDetailExam.DataBean data = beans.getData();

        Gson gson = new Gson();


        String resourceText = data.getExamText();
        ExamText examText = gson.fromJson(resourceText, ExamText.class);
        data.setExamTextVo(examText);


        //   List<StudentDetails.DataBean.ExamListBean> examList = bean.getExamList();
        for (StudentDetailExam.DataBean dataBean : data.getExamList()) {
            ExamText json = gson.fromJson(dataBean.getExamText(), ExamText.class);
            dataBean.setExamTextVo(json);
        }
    }
}
