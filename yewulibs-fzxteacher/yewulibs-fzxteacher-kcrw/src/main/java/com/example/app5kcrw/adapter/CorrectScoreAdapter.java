package com.example.app5kcrw.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.Log;
import com.example.app5libbase.util.MySeekBarChangeListener;
import com.sdzn.fzx.teacher.vo.CorrectParam;
import com.sdzn.fzx.teacher.vo.StudentDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/15
 * 修改单号：
 * 修改内容:
 */
public class CorrectScoreAdapter extends BaseAdapter {

    private List<StudentDetails.DataBean> mList;
    private List<StudentDetails.DataBean.AnswerBean> mExamList;
    private Context mContext;

    private List<CorrectParam> correctParams = new ArrayList<>();

    public CorrectScoreAdapter(List<StudentDetails.DataBean> mList, List<StudentDetails.DataBean.AnswerBean> mExamList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
        this.mExamList = mExamList;
        setParam(mList);
        setParam1(mExamList);
    }

    void setParam1(List<StudentDetails.DataBean.AnswerBean> mExamList) {
        for (StudentDetails.DataBean.AnswerBean dataBean : mExamList) {
            CorrectParam correctParam = new CorrectParam();
            correctParam.setId(dataBean.getId());
            correctParam.setParentId(dataBean.getParentId());
            correctParam.setIsRight(dataBean.getIsRight());
            correctParam.setFullScore(dataBean.getFullScore());
            correctParam.setIsCorrent(dataBean.getIsCorrect());
            correctParam.setScore(dataBean.getScore());
            correctParam.setExamTypeId(dataBean.getTemplateId());
            List<CorrectParam> correctParamList = new ArrayList<>();
            if (dataBean.getTemplateId() == 6) {// && dataBean.getAnswer().getTemplateId() == 16
                for (StudentDetails.DataBean.AnswerBean.ExamOptionListBean examOptionBean : dataBean.getExamOptionList()) {
                    CorrectParam examOptionBeanParam = new CorrectParam();
                    examOptionBeanParam.setId(examOptionBean.getId());
                    examOptionBeanParam.setParentId(dataBean.getParentId());
                    examOptionBeanParam.setFullScore(dataBean.getFullScore());
                    examOptionBeanParam.setIsRight(examOptionBean.getIsRight());
                    examOptionBeanParam.setScore(examOptionBean.getScore());
                    correctParamList.add(examOptionBeanParam);
                    correctParam.setChildren(correctParamList);
                }
                correctParams.add(correctParam);
            }
        }
    }

    void setParam(List<StudentDetails.DataBean> mList) {
        for (StudentDetails.DataBean dataBean : mList) {
            CorrectParam correctParam = new CorrectParam();
            correctParam.setId(dataBean.getAnswer().getId());
            correctParam.setParentId(dataBean.getAnswer().getParentId());
            correctParam.setIsRight(dataBean.getAnswer().getIsRight());
            correctParam.setFullScore(dataBean.getAnswer().getFullScore());
            correctParam.setIsCorrent(dataBean.getAnswer().getIsCorrect());
            correctParam.setScore(dataBean.getAnswer().getScore());
            correctParam.setExamTypeId(dataBean.getAnswer().getTemplateId());
            if (dataBean.getAnswer().getTemplateId() == 6) {// && dataBean.getAnswer().getTemplateId() == 16
                List<CorrectParam> correctParamList = new ArrayList<>();
                for (StudentDetails.DataBean.AnswerBean.ExamOptionListBean examOptionBean : dataBean.getAnswer().getExamOptionList()) {
                    CorrectParam examOptionBeanParam = new CorrectParam();
                    examOptionBeanParam.setId(examOptionBean.getId());
                    examOptionBeanParam.setParentId(dataBean.getAnswer().getParentId());
                    examOptionBeanParam.setFullScore(dataBean.getAnswer().getFullScore());
                    examOptionBeanParam.setIsRight(examOptionBean.getIsRight());
                    examOptionBeanParam.setScore(examOptionBean.getScore());
                    correctParamList.add(examOptionBeanParam);
                }
                correctParam.setChildren(correctParamList);
            }
            correctParams.add(correctParam);
        }
    }

    public List<CorrectParam> getCorrectParams() {
        return correctParams;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder fholder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_fill_exam_socre, null);

            fholder = new ViewHolder(convertView);
            convertView.setTag(fholder);
        } else {
            fholder = (ViewHolder) convertView.getTag();
        }
        final ViewHolder holder = fholder;
        /*填空题*/

        if (mList.get(position).getAnswer().getTemplateId() == 6) {
            holder.examName.setText(mList.get(position).getAnswer().getExamSeq() + "." + mList.get(position).getAnswer().getTemplateStyleName() + "");
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.examScore.setVisibility(View.GONE);
            holder.seekbar.setVisibility(View.GONE);
            holder.linearLayout.removeAllViews();

            for (int i = 0; i < mList.get(position).getAnswer().getExamOptionList().size(); i++) {
                View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_fill_exam_pigai, null);
                TextView examName = inflate.findViewById(R.id.exam_name);

                final ImageView dui = inflate.findViewById(R.id.dui);
                final ImageView cuo = inflate.findViewById(R.id.cuo);
                final int finalI = i;

                examName.setText("(" + (i + 1) + ")");
                holder.linearLayout.addView(inflate);
                if (mList.get(position).getAnswer().getExamOptionList().get(i).getIsRight() == 0) {
                    cuo.setImageResource(R.mipmap.cuo_pigai);
                }
                if (mList.get(position).getAnswer().getExamOptionList().get(i).getIsRight() == 1) {
                    dui.setImageResource(R.mipmap.dui_pigai);
                }

                dui.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {//1 对
                        mList.get(position).getAnswer().getExamOptionList().get(finalI).getId();
                        for (CorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                for (CorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                    if (childrenCorrectParam.getId() == mList.get(position).getAnswer().getExamOptionList().get(finalI).getId()) {
                                        if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 0) {
                                            dui.setImageResource(R.mipmap.dui_pigai);
                                            childrenCorrectParam.setIsRight(1);
                                            childrenCorrectParam.setScore(childrenCorrectParam.getFullScore());
                                            cuo.setImageResource(R.mipmap.cuo);
                                        } else if (childrenCorrectParam.getIsRight() == 1) {
                                            dui.setImageResource(R.mipmap.dui);
                                            childrenCorrectParam.setIsRight(-1);

                                            cuo.setImageResource(R.mipmap.cuo);
                                        }

                                    }
                                }
                            }
                        }
                    }
                });
                cuo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mList.get(position).getAnswer().getExamOptionList().get(finalI).getId();
                        for (CorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                for (CorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                    if (childrenCorrectParam.getId() == mList.get(position).getAnswer().getExamOptionList().get(finalI).getId()) {
                                        if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 1) {
                                            cuo.setImageResource(R.mipmap.cuo_pigai);
                                            childrenCorrectParam.setIsRight(0);
                                            childrenCorrectParam.setScore(0);
                                            dui.setImageResource(R.mipmap.dui);
                                        } else if (childrenCorrectParam.getIsRight() == 0) {
                                            cuo.setImageResource(R.mipmap.cuo);
                                            childrenCorrectParam.setIsRight(-1);
                                            dui.setImageResource(R.mipmap.dui);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        } else if (mList.get(position).getAnswer().getTemplateId() == 16) {   //综合题
            for (final StudentDetails.DataBean.AnswerBean dataBean : mList.get(position).getAnswer().getExamList()) {
                /*综合填空题*/
                if (dataBean.getTemplateId() == 6) {
                    holder.examName.setText(dataBean.getExamSeq() + "." + dataBean.getTemplateStyleName() + "");
                    holder.linearLayout.setVisibility(View.VISIBLE);
                    holder.examScore.setVisibility(View.GONE);
                    holder.seekbar.setVisibility(View.GONE);
                    holder.linearLayout.removeAllViews();

                    for (int i = 0; i < dataBean.getExamOptionList().size(); i++) {
                        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_fill_exam_pigai, null);
                        TextView examName = inflate.findViewById(R.id.exam_name);

                        final ImageView dui = inflate.findViewById(R.id.dui);
                        final ImageView cuo = inflate.findViewById(R.id.cuo);
                        final int finalI = i;

                        examName.setText("(" + (i + 1) + ")");
                        holder.linearLayout.addView(inflate);
                        if (dataBean.getExamOptionList().get(i).getIsRight() == 0) {
                            cuo.setImageResource(R.mipmap.cuo_pigai);
                        }
                        if (dataBean.getExamOptionList().get(i).getIsRight() == 1) {
                            dui.setImageResource(R.mipmap.dui_pigai);
                        }

                        dui.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {//1 对
                                dataBean.getExamOptionList().get(finalI).getId();
                                for (CorrectParam correctParam : correctParams) {
                                    if (dataBean.getId() == correctParam.getId()) {
                                        Log.e("aaatext", "correctParam: " + correctParam.getChildren());
                                        for (CorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                            if (childrenCorrectParam.getId() == dataBean.getExamOptionList().get(finalI).getId()) {
                                                if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 0) {
                                                    dui.setImageResource(R.mipmap.dui_pigai);
                                                    childrenCorrectParam.setIsRight(1);
                                                    childrenCorrectParam.setScore(childrenCorrectParam.getFullScore());
                                                    cuo.setImageResource(R.mipmap.cuo);
                                                } else if (childrenCorrectParam.getIsRight() == 1) {
                                                    dui.setImageResource(R.mipmap.dui);
                                                    childrenCorrectParam.setIsRight(-1);
                                                    cuo.setImageResource(R.mipmap.cuo);
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        });
                        cuo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dataBean.getExamOptionList().get(finalI).getId();
                                for (CorrectParam correctParam : correctParams) {
                                    if (dataBean.getId() == correctParam.getId()) {
                                        for (CorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                            if (childrenCorrectParam.getId() == dataBean.getExamOptionList().get(finalI).getId()) {
                                                if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 1) {
                                                    cuo.setImageResource(R.mipmap.cuo_pigai);
                                                    childrenCorrectParam.setIsRight(0);
                                                    childrenCorrectParam.setScore(0);
                                                    dui.setImageResource(R.mipmap.dui);
                                                } else if (childrenCorrectParam.getIsRight() == 0) {
                                                    cuo.setImageResource(R.mipmap.cuo);
                                                    childrenCorrectParam.setIsRight(-1);
                                                    dui.setImageResource(R.mipmap.dui);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });


                    }
                } else {

                    if (Double.valueOf(dataBean.getScore()) > -1) {
                        // holder.linearLayout.setVisibility(View.GONE);
                        holder.examScore.setVisibility(View.VISIBLE);
                        holder.seekbar.setVisibility(View.VISIBLE);
                        holder.examScore.setText(dataBean.getScore() + "");
                        holder.seekbar.setMax((int) dataBean.getFullScore());
                        holder.seekbar.setProgress(Double.valueOf(mList.get(position).getAnswer().getScore()).intValue());
                        //holder.examName.setText(mList.get(position).getAnswer().getUserStudentName());
                        holder.examName.setText(dataBean.getExamSeq() + "." + dataBean.getTemplateStyleName());

                    } else {
                        holder.examScore.setVisibility(View.GONE);
                        holder.seekbar.setVisibility(View.GONE);
                        holder.examName.setVisibility(View.GONE);
                    }
                }
            }
        } else if (mList.get(position).getAnswer().getScore() > -1) {//除了填空和綜合
            holder.linearLayout.setVisibility(View.GONE);
            holder.examScore.setVisibility(View.VISIBLE);
            holder.seekbar.setVisibility(View.VISIBLE);


            holder.examScore.setText(mList.get(position).getAnswer().getScore() + "");
            holder.seekbar.setMax((int) mList.get(position).getAnswer().getFullScore());
            holder.seekbar.setProgress(Double.valueOf(mList.get(position).getAnswer().getScore()).intValue());
            holder.examName.setText(mList.get(position).getAnswer().getExamSeq() + "." + mList.get(position).getAnswer().getTemplateStyleName());
            //holder.examName.setText(mList.get(position).getAnswer().getUserStudentName());
        } else {
            holder.examScore.setVisibility(View.GONE);
            holder.seekbar.setVisibility(View.GONE);
            holder.examName.setVisibility(View.GONE);
        }

        holder.textWatcher = new CorrectScoreAdapter.MyTextWatcher(holder) {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    holder.seekbar.setProgress(Double.valueOf(s.toString()).intValue());
                }

            }
        };
        holder.examScore.addTextChangedListener(holder.textWatcher);
        holder.seekbar.setOnSeekBarChangeListener(new MySeekBarChangeListener(position) {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (mList.get(position).getAnswer().getTemplateId() == 4 || mList.get(position).getAnswer().getTemplateId() == 16) {
                    if (progress >= ((int) mList.get(position).getAnswer().getFullScore())) {
                        holder.examScore.setText(mList.get(position).getAnswer().getFullScore() + "");
                        for (CorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                correctParam.setScore(mList.get(position).getAnswer().getFullScore());
                            }
                        }
                    } else {
                        holder.examScore.setText(progress + "");
                        for (CorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                correctParam.setScore(progress);
                            }
                        }
                    }
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return convertView;
    }

    private SeekBar seekbar, seekbar1;
    private TextView examName, examName1;
    private EditText exam_score, exam_score1;

    static abstract class MyTextWatcher implements TextWatcher {
        public ViewHolder holder;

        public MyTextWatcher(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    static class ViewHolder {
        TextView examName;
        EditText examScore;
        SeekBar seekbar;
        LinearLayout linearLayout;

        MyTextWatcher textWatcher;
        //  MyTextWatcher textWatcher1;
        //  @BindView(R.id.filling_seekbar)
        // LinearLayout seekbarlinearLayout;

        ViewHolder(View view) {
            examName = view.findViewById(R.id.exam_name);
            examScore = view.findViewById(R.id.exam_score);
            seekbar = view.findViewById(R.id.seekbar);
            linearLayout = view.findViewById(R.id.filling);
        }
    }
}
