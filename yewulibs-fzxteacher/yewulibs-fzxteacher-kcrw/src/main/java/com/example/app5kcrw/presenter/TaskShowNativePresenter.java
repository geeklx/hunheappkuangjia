package com.example.app5kcrw.presenter;

import com.example.app5kcrw.activity.TaskShowNativeActivity;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.TaskShowNativeView;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.ExamVo;
import com.sdzn.fzx.teacher.vo.ResourceVo;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/22
 * 修改单号：
 * 修改内容:
 */
public class TaskShowNativePresenter extends BasePresenter<TaskShowNativeView, TaskShowNativeActivity> {


    /**
     * 加载试题
     */
    public void getExamList(Map<String, String> params) {
        Network.createTokenService(NetWorkService.GetExamListService.class)
                .GetExamList(params)
                .map(new StatusFunc<ExamVo>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<ExamVo>(new SubscriberListener<ExamVo>() {
                    @Override
                    public void onNext(ExamVo examVo) {
                        initData(examVo);
                        mView.setExamList(examVo);
                    }

                    @Override
                    public void onError(Throwable e) {


                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    private void initData(ExamVo beans) {
        List<ExamVo.DataBean> list = beans.getData();
        Gson gson = new Gson();
        if (list == null) {
            return;
        }
        for (ExamVo.DataBean bean : list) {
            if (bean.getType() == 3) {//纯文本
                continue;
            }
            if (bean.getType() == 2) {//资源
                String resourceText = bean.getResourceText();
                ResourceVo.ResourceTextVo resourceBean = gson.fromJson(resourceText, ResourceVo.ResourceTextVo.class);
                bean.setResourceVoBean(resourceBean);
                continue;
            }
            String resourceText = bean.getExamText();
            ExamText examText = gson.fromJson(resourceText, ExamText.class);
            bean.setExamTextVo(examText);

                        /*    //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);*/
            //综合题额外进行一轮解析
            if (bean.getExamTemplateId() != 16) {
                continue;
            }
            for (ExamVo.DataBean dataBean : bean.getChildLessonLibDetail()) {
                ExamText json = gson.fromJson(dataBean.getExamText(), ExamText.class);
                dataBean.setExamTextVo(json);
            }
        }
    }
}
