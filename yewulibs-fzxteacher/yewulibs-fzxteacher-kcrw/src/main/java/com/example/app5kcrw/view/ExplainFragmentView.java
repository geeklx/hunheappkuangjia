package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.ExamCorrectVo;
import com.sdzn.fzx.teacher.vo.ExplainVo;
import com.sdzn.fzx.teacher.vo.UserTeacherInfo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public interface ExplainFragmentView extends BaseView {
    void setExplainVo(ExplainVo explainVo);
    void setExamCorrectVo(ExamCorrectVo examCorrectVo);
    void setdel();
    void setUserTeacherInfo(UserTeacherInfo userTeacherInfo);
}

