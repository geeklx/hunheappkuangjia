package com.example.app5kcrw.presenter;

import com.google.gson.Gson;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.ExplainFragmentView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.ExamCorrectVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.ExplainVo;
import com.sdzn.fzx.teacher.vo.UserTeacherInfo;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class ExplainFragmentPresenter extends BasePresenter<ExplainFragmentView, BaseActivity> {

    public void getexamList(String lessonTaskId, String type) {

        Network.createTokenService(NetWorkService.GetExplainListService.class)
                .AnalyzeStatisc(lessonTaskId, type)
                .map(new StatusFunc<ExplainVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ExplainVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(ExplainVo o) {
                        mView.setExplainVo(o);

                    }
                });
    }


    public void getexamCorrectList(String lessonTaskDetailsId) {

        Network.createTokenService(NetWorkService.GetExplainExamListService.class)
                .AnalyzeStatisc(lessonTaskDetailsId)
                .map(new StatusFunc<ExamCorrectVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ExamCorrectVo>(new SubscriberListener<ExamCorrectVo>() {
                    @Override
                    public void onNext(ExamCorrectVo o) {
                        initData(o);
                        mView.setExamCorrectVo(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity,true, false, false, ""));
    }

    private void initData(ExamCorrectVo beans) {
        List<ExamCorrectVo.DataBean> list = beans.getData();
        Gson gson = new Gson();
        if (list == null) {
            return;
        }
        for (ExamCorrectVo.DataBean bean : list) {

            String resourceText = bean.getExamText();
            ExamText examText = gson.fromJson(resourceText, ExamText.class);
            bean.setExamTextVo(examText);

                        /*    //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);*/
            //综合题额外进行一轮解析
            if (bean.getTemplateId() != 16) {
                continue;
            }
            //   List<StudentDetails.DataBean.ExamListBean> examList = bean.getExamList();
//            for (ExamCorrectVo.DataBean dataBean : bean.getExamList()) {
//                ExamText json = gson.fromJson(dataBean.getExamText(), ExamText.class);
//                dataBean.setExamTextVo(json);
//            }
        }
    }

    public void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateStudentExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("删除成功");
                        mView.setdel();

                    }
                });
    }


    public void teacherInfo() {
        Network.createTokenService(NetWorkService.GetUserTeacherInfoService.class)
                .getUserInfo()
                .map(new StatusFunc<UserTeacherInfo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserTeacherInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserTeacherInfo o) {

                        mView.setUserTeacherInfo(o);
                    }
                });
    }


}
