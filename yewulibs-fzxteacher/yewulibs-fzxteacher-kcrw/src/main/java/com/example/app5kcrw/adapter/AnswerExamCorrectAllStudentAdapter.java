package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.exam.FillBlankCheckTextView;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.ClozeAnswerBean;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.student.libutils.annotations.CheckState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class AnswerExamCorrectAllStudentAdapter extends BaseRcvAdapter<AnswerExamCorrectAllStudentVo.DataBean> {

    private Activity mActivity = null;
    private int vid = 1;
    private List<AnswerExamCorrectAllStudentVo.DataBean> mList;


    public AnswerExamCorrectAllStudentAdapter(Context context, List<AnswerExamCorrectAllStudentVo.DataBean> mList, Activity activity, int v) {
        super(context, mList);
        this.mList = mList;
        mActivity = activity;
        this.vid = v;
    }

    @Override
    public int getItemViewType(int position) {
        AnswerExamCorrectAllStudentVo.DataBean bean = mList.get(position);
        int templateId = bean.getAnswer().getTemplateId();
        return bean.getAnswer().getTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_pigaijianda_exam);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_pigaierror_exam_text);
            case 14://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 16://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnswerExamCorrectAllStudentVo.DataBean bean) {
        switch (bean.getAnswer().getTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }


    private AnswerExamStudentDetailAllAdapter answerExamStudentDetailAllAdapter;//获取按批改adapter
    private CopyAnswerExamStudentDetailAllAdapter copyAnswerExamStudentDetailAllAdapter;//获取按批改adapter

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */
    //这个是底部显示的数据
    private void bindAllAnswer(final BaseViewHolder holder, final int position, final AnswerExamCorrectAllStudentVo.DataBean bean) {
        bindAllExamTitle(holder, position, bean);
        HtmlTextView htmlTextView = holder.getView(R.id.tv_exam);//题干
        RecyclerView examListView = holder.getView(R.id.lv);//显示内容
        RelativeLayout rl_comment = holder.getView(R.id.rl_comment);//评语包裹
        TextView etcomment = holder.getView(R.id.et_comment);//评语
        RelativeLayout rlexamtitle = holder.getView(R.id.rl_exam_title);//头部
        LinearLayout llgood = holder.getView(R.id.ll);//优秀解答包裹
        RecyclerView copylv = holder.getView(R.id.copylv);
        TextView tvstudentAnswer = holder.getView(R.id.tv_student_Answer);//学生作答标签
        RelativeLayout rlmarkingexamtitle = holder.getView(R.id.rl_marking_exam_title);//显示学生头部
        RelativeLayout Oneshowlv = holder.getView(R.id.Oneshowlv);
        TextView marking_show = holder.getView(R.id.marking_show);
        final TextView answermarkingname = holder.getView(R.id.answer_marking_name);//名称
        final TextView answermarkingscore = holder.getView(R.id.answer_marking_score);//评分
        final ImageView answermarkingphoto = holder.getView(R.id.answer_marking_photo);//头像

        rlmarkingexamtitle.setVisibility(View.VISIBLE);//学生做题信息
        llgood.setVisibility(View.GONE);//头部优秀作答隐藏
        marking_show.setVisibility(View.GONE);//学生作答显示答案


        if (position == 0) {
            rlexamtitle.setVisibility(View.VISIBLE);//标题头
            htmlTextView.setVisibility(View.VISIBLE);//主题干
            Oneshowlv.setVisibility(View.VISIBLE);//题干内容
            tvstudentAnswer.setVisibility(View.VISIBLE);//学生作答
            rl_comment.setVisibility(View.VISIBLE);//评语
        } else {
            Oneshowlv.setVisibility(View.GONE);
            htmlTextView.setVisibility(View.GONE);//主题干
            rlexamtitle.setVisibility(View.GONE);
            rl_comment.setVisibility(View.VISIBLE);
            tvstudentAnswer.setVisibility(View.GONE);
        }
        answermarkingname.setText(bean.getAnswer().getUserStudentName());
        if (Double.valueOf(bean.getAnswer().getScore()) > 0) {
            answermarkingscore.setVisibility(View.VISIBLE);
            answermarkingscore.setText(bean.getAnswer().getScore() + "分");
        } else {
            answermarkingscore.setVisibility(View.GONE);
        }
        //RequestOptions error = new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(context)).placeholder(R.mipmap.tx_img);
        // Glide.with(context).load(bean.getAnswer()).apply(error).into(answermarkingphoto);

        final List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> list = new ArrayList();
        htmlTextView.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());
        list.addAll(bean.getAnswer().getExamList());
        /*头部内容*/
        copylv.setLayoutManager(new LinearLayoutManager(context));
        copyAnswerExamStudentDetailAllAdapter = new CopyAnswerExamStudentDetailAllAdapter(context, list, bean);
        copylv.setAdapter(copyAnswerExamStudentDetailAllAdapter);

        /*底部内容*/
        examListView.setLayoutManager(new LinearLayoutManager(context));
        answerExamStudentDetailAllAdapter = new AnswerExamStudentDetailAllAdapter(context, list, bean);
        examListView.setAdapter(answerExamStudentDetailAllAdapter);

        if (bean.getComment().equals("")) {
            etcomment.setHint("老师评语");
        } else {
            etcomment.setHint(bean.getComment());
        }

        /*发起评论*/
        etcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AnswerExamActivity");
                intent.putExtra("lessonTaskId", bean.getAnswer().getLessonTaskId());
                intent.putExtra("lessonTaskStudentId", bean.getAnswer().getLessonTaskStudentId());
                intent.putExtra("lessonAnswerExamId", bean.getAnswer().getId());
                mActivity.startActivity(intent);
            }
        });
    }


    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position,
                               final AnswerExamCorrectAllStudentVo.DataBean bean) {
        Log.e("aaatest", "bindFillBlank进入解答题");
        bindExamTitle(holder, position, bean);
        final LinearLayout error_ly = holder.getView(R.id.error_ly);//包裹答案
        final LinearLayout llgooderror = holder.getView(R.id.ll);//优秀解答包裹
        final TextView etcomment = holder.getView(R.id.et_comment);
        final TextView show = holder.getView(R.id.show);
        final ImageView answer_marking_photo = holder.getView(R.id.answer_marking_photo);//头像
        final TextView answer_marking_name = holder.getView(R.id.answer_marking_name);//名称
        final TextView answer_marking_score = holder.getView(R.id.answer_marking_score);//分值
        final TextView marking_show = holder.getView(R.id.marking_show);//显示答案
        final FillBlankCheckTextView titlehtml = holder.getView(R.id.title_html_tv);
        final RelativeLayout rlexamtitle = holder.getView(R.id.rl_exam_title);//标题头
        final TextView studentanswer = holder.getView(R.id.tv_student_Answer);//学生作答
        final RelativeLayout rl_marking_exam_title = holder.getView(R.id.rl_marking_exam_title);//用户信息


        if (position == 0) {
            rl_marking_exam_title.setVisibility(View.VISIBLE);
            marking_show.setVisibility(View.GONE);
            show.setVisibility(View.VISIBLE);
            titlehtml.setVisibility(View.VISIBLE);
            rlexamtitle.setVisibility(View.VISIBLE);
            studentanswer.setVisibility(View.VISIBLE);
        } else {
            rl_marking_exam_title.setVisibility(View.VISIBLE);
            marking_show.setVisibility(View.GONE);
            show.setVisibility(View.GONE);
            titlehtml.setVisibility(View.GONE);
            rlexamtitle.setVisibility(View.GONE);
            studentanswer.setVisibility(View.GONE);
        }
        titlehtml.setHtmlBody(bean.getAnswer().getExamTextVo().getExamStem());//显示题干
        answer_marking_name.setText(bean.getAnswer().getUserStudentName());

        if (Double.valueOf(bean.getAnswer().getScore()) > 0) {
            answer_marking_score.setVisibility(View.VISIBLE);
            answer_marking_score.setText(bean.getAnswer().getScore() + "分");
        } else {
            answer_marking_score.setVisibility(View.GONE);
        }
        //RequestOptions error = new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(context)).placeholder(R.mipmap.tx_img);
        // Glide.with(context).load(bean.getAnswer()).apply(error).into(answer_marking_photo);
        if (bean.getComment().equals("")) {
            etcomment.setHint("老师评语");
        } else {
            etcomment.setHint(bean.getComment());
        }

        error_ly.setVisibility(View.VISIBLE);
        llgooderror.setVisibility(View.GONE);

        final FillBlankCheckTextView tv = holder.getView(R.id.FillBlankCheckTextView);
        tv.setVisibility(View.VISIBLE);

        List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean> examOptionList = bean.getAnswer().getExamOptionList();
        SparseArray<ClozeAnswerBean> array = new SparseArray<>();
        array.clear();
        for (int i = 0; i < examOptionList.size(); i++) {
            AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean optionBean = examOptionList.get(i);
            int checkState;
            if (bean.getAnswer().getIsCorrect() != 0) {
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                } else {
                    checkState = CheckState.FALSE;
                }
            } else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(),
                    optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(), checkState));
        }
        if (bean.getAnswer().getExamTextVo() != null && bean.getAnswer().getExamTextVo().getExamStem() != null) {
            tv.setHtmlBody(bean.getAnswer().getExamTextVo().getExamStem(), array);
        }
        tv.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                if (!TextUtils.isEmpty(imageSrc)) {
                    CorrectDataVo correctData = new CorrectDataVo();
                    correctData.setSrc(imageSrc);
                    correctData.setSeq("");
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectActivity");
                    intent.putExtra("correctData", correctData);
                    intent.putExtra("vid", 0);
                    mActivity.startActivity(intent);
                } else {
                    ToastUtil.showLonglToast("图片地址为空");
                }
            }
        });
        /*发起评论*/
        etcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AnswerExamActivity");
                intent.putExtra("lessonTaskId", bean.getAnswer().getLessonTaskId());
                intent.putExtra("lessonTaskStudentId", bean.getAnswer().getLessonTaskStudentId());
                intent.putExtra("lessonAnswerExamId", bean.getAnswer().getId());
                mActivity.startActivity(intent);
            }
        });
    }

    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position,
                                 final AnswerExamCorrectAllStudentVo.DataBean bean) {
        Log.e("aaatest", "bindShortAnswer进入解答题");
        bindExamTitle(holder, position, bean);
        final HtmlTextView tvhtml = holder.getView(R.id.tv_exam);//题干
        final TextView textView = holder.getView(R.id.score_tv_pigai);//作答分数
        final LinearLayout llgooderror = holder.getView(R.id.ll);//包裹优秀答案-错误答案外层
        final TextView show = holder.getView(R.id.show);//显示答案
        final TextView tvtype = holder.getView(R.id.tv_type);//题目标题
        final TextView tvcount = holder.getView(R.id.tv_count);//题号
        final LinearLayout line = holder.getView(R.id.line);//作答得分外层
        final TextView tvunanswer = holder.getView(R.id.tv_un_answer);//未提交该题答案
        final RelativeLayout rlexamtitle = holder.getView(R.id.rl_exam_title);//包裹title
        final RelativeLayout marking_examtitle = holder.getView(R.id.rl_marking_exam_title);//获取学员信息
        final TextView etcomment = holder.getView(R.id.et_comment);//评语
        final TextView answermarkingname = holder.getView(R.id.answer_marking_name);//名称
        final TextView answermarkingscore = holder.getView(R.id.answer_marking_score);//评分
        final ImageView answermarkingphoto = holder.getView(R.id.answer_marking_photo);//头像
        final TextView marking_show = holder.getView(R.id.marking_show);//显示答案

        //判断只有第一个参数显示数据
        if (position == 0) {
            tvhtml.setVisibility(View.VISIBLE);
            llgooderror.setVisibility(View.GONE);
            tvhtml.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());
            show.setVisibility(View.VISIBLE);
            marking_show.setVisibility(View.GONE);
            tvtype.setVisibility(View.VISIBLE);
            tvcount.setVisibility(View.VISIBLE);
            line.setVisibility(View.GONE);
            tvunanswer.setVisibility(View.GONE);
            rlexamtitle.setVisibility(View.VISIBLE);
            marking_examtitle.setVisibility(View.VISIBLE);
            etcomment.setVisibility(View.VISIBLE);
        } else {
            etcomment.setVisibility(View.VISIBLE);
            marking_examtitle.setVisibility(View.VISIBLE);
            rlexamtitle.setVisibility(View.GONE);
            tvunanswer.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
            tvhtml.setVisibility(View.GONE);
            llgooderror.setVisibility(View.GONE);
            show.setVisibility(View.GONE);
            marking_show.setVisibility(View.GONE);
            tvtype.setVisibility(View.GONE);
            tvcount.setVisibility(View.GONE);
        }

        answermarkingname.setText(bean.getAnswer().getUserStudentName());

        if (Double.valueOf(bean.getAnswer().getScore()) > 0) {
            answermarkingscore.setVisibility(View.VISIBLE);
            answermarkingscore.setText(bean.getAnswer().getScore() + "分");
        } else {
            answermarkingscore.setVisibility(View.GONE);
        }

        //RequestOptions error = new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(context)).placeholder(R.mipmap.tx_img);
        // Glide.with(context).load(bean.getAnswer()).apply(error).into(answermarkingphoto);
        if (bean.getComment().equals("")) {
            etcomment.setHint("老师评语");
        } else {
            etcomment.setHint(bean.getComment());
        }

        if (bean.getAnswer().getExamOptionList().size() == 0) {
            holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.VISIBLE);
        } else {
            holder.getView(R.id.rv_add_pic).setVisibility(View.VISIBLE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.GONE);
        }
        RecyclerView rv = holder.getView(R.id.rv_add_pic);//item图片列表数据
        rv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        MarkingAnswerResultAdapter adapter = (MarkingAnswerResultAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new MarkingAnswerResultAdapter(context);
            rv.setAdapter(adapter);
        }
        /*发起评论*/
        etcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AnswerExamActivity");
                intent.putExtra("lessonTaskId", bean.getAnswer().getLessonTaskId());
                intent.putExtra("lessonTaskStudentId", bean.getAnswer().getLessonTaskStudentId());
                intent.putExtra("lessonAnswerExamId", bean.getAnswer().getId());
                mActivity.startActivity(intent);
            }
        });
        adapter.setListener(new MarkingAnswerResultAdapter.OnClickListener() {
            @Override
            public void clickImage(int index, String url) {
                CorrectDataVo correctData = new CorrectDataVo();
                correctData.setSrc(bean.getAnswer().getExamOptionList().get(0).getId() + "");
                correctData.setSeq(index + "");
                correctData.setList(url);
                correctData.setEid(bean.getAnswer().getId() + "");
                correctData.setId(bean.getAnswer().getLessonTaskStudentId());
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MarkingNativeActivity");
                // Intent intent = new Intent(mActivity, CorrectActivity.class);
                intent.putExtra("correctData", correctData);
                intent.putExtra("comment", bean.getComment());
                intent.putExtra("fullscore", bean.getAnswer().getFullScore());
                intent.putExtra("score", bean.getAnswer().getScore());
                intent.putExtra("vid", vid);
                intent.putExtra("lessonTaskDetailsId", bean.getAnswer().getLessonTaskDetailsId());
                mActivity.startActivity(intent);
            }
        });
        List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean> examList = bean.getAnswer().getExamOptionList();
        for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position,
                              final AnswerExamCorrectAllStudentVo.DataBean bean) {
        bindExamTitle(holder, position, bean);

        ExamText examTextVo = bean.getAnswer().getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(examTextVo.getExamStem());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvText.setHtmlText(options.get(i).getContent());
            String str = String.valueOf((char) (65 + i));
            tvNumber.setText(str);


            tvText.setHtmlText(options.get(i).getContent());

            final List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean> examList = bean.getAnswer().getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                tvNumber.setBackground(context.getResources().getDrawable(
                        bean.getAnswer().getExamTextVo().getExamOptions().get(i).isRight() ?
                                R.drawable.bg_select_selector_2 :
                                R.drawable.bg_result_select_selector_n));
                tvNumber.setSelected(TextUtils.equals(str, examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean optionBean : examList) {
                    if (TextUtils.equals(str, optionBean.getMyAnswer())) {
                        tvNumber.setBackground(context.getResources().getDrawable(
                                bean.getAnswer().getExamTextVo().getExamOptions().get(i).isRight() ?
                                        R.drawable.bg_select_selector_2 :
                                        R.drawable.bg_result_select_selector_n));
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }


    }

    /**
     * 完型填空
     */
    private void bindFillExam(final BaseViewHolder holder, final int position,
                              final AnswerExamCorrectAllStudentVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());


    }

    private boolean goodFlag = true;
    private boolean errorFlag = true;
    private TextView textCount;
    private TextView textType;

    private void bindExamTitle(final BaseViewHolder holder, int position,
                               final AnswerExamCorrectAllStudentVo.DataBean bean) {
        textCount = holder.getView(R.id.tv_count);
        textType = holder.getView(R.id.tv_type);

        if (bean.getAnswer().getFullScore() > -1) {
            if (bean.getAnswer().getTemplateId() == 6) {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + (bean.getAnswer().getFullScore() * bean.getAnswer().getEmptyCount()) + "分)");
            } else {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + bean.getAnswer().getFullScore() + "分)");
            }
        } else {

            textCount.setText(bean.getAnswer().getExamSeq() + ".");
        }
        if (bean.getAnswer().getExamTextVo() != null && bean.getAnswer().getExamTextVo().getTemplateStyleName() != null) {
            textType.setText(bean.getAnswer().getExamTextVo().getTemplateStyleName() + "");
        } else {
            textType.setText("");
        }
        final ImageView good = holder.getView(R.id.marking_good);
        final ImageView error = holder.getView(R.id.marking_error);
        if (bean.getAnswer().getCorrectType() == 1) {
            good.setImageResource(R.mipmap.dx_sel);
            goodFlag = false;
        } else {
            good.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }

        if (bean.getAnswer().getCorrectType() == 2) {
            error.setImageResource(R.mipmap.dx_sel);
            errorFlag = false;
        } else {
            error.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }

        final TextView markingshow = holder.getView(R.id.marking_show);
        holder.getView(R.id.marking_show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    markingshow.setText("隐藏答案");
                    markingshow.setTextColor(Color.parseColor("#5BA2EA"));
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    markingshow.setText("显示答案");
                    markingshow.setTextColor(Color.parseColor("#8A8A8A"));
                }
            }
        });

        final TextView show = holder.getView(R.id.show);
        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    show.setText("隐藏答案");
                    show.setTextColor(Color.parseColor("#5BA2EA"));
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    show.setText("显示答案");
                    show.setTextColor(Color.parseColor("#8A8A8A"));
                }
            }
        });
        holder.getView(R.id.marking_good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goodFlag) {
                    good.setImageResource(R.mipmap.dx_sel);
                    error.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = false;
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "1");
                } else {
                    good.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

        holder.getView(R.id.marking_error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (errorFlag) {
                    error.setImageResource(R.mipmap.dx_sel);
                    good.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = false;
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "2");
                } else {
                    error.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        ExamText examTextVo = bean.getAnswer().getExamTextVo();

        if (examTextVo == null) {
            return;
        }
        if (TextUtils.isEmpty(examTextVo.getExamAnswer())) {
            result.setHtmlText("略...");
        } else {
            result.setHtmlText(examTextVo.getExamAnswer());
        }
        if (TextUtils.isEmpty(examTextVo.getExamAnalysis())) {
            analysis.setHtmlText("略...");
        } else {
            analysis.setHtmlText(examTextVo.getExamAnalysis());
        }
    }

    private void bindAllExamTitle(final BaseViewHolder holder, int position,
                                  final AnswerExamCorrectAllStudentVo.DataBean bean) {
        textCount = holder.getView(R.id.tv_count);
        textType = holder.getView(R.id.tv_type);
        if (bean.getAnswer().getFullScore() > -1) {
            if (bean.getAnswer().getTemplateId() == 6) {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + (bean.getAnswer().getFullScore() * bean.getAnswer().getEmptyCount()) + "分)");
            } else {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + bean.getAnswer().getFullScore() + "分)");
            }
        } else {
            textCount.setText(bean.getAnswer().getExamSeq() + ".");
        }
        if (bean.getAnswer().getExamTextVo() != null && bean.getAnswer().getExamTextVo().getTemplateStyleName() != null) {
            textType.setText(bean.getAnswer().getExamTextVo().getTemplateStyleName() + "");
        } else {
            textType.setText("");
        }

        final TextView markingshow = holder.getView(R.id.show);
        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    markingshow.setText("隐藏答案");
                    markingshow.setTextColor(Color.parseColor("#5BA2EA"));
                    copyAnswerExamStudentDetailAllAdapter.setShowAnswer(true);
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    markingshow.setText("显示答案");
                    markingshow.setTextColor(Color.parseColor("#8A8A8A"));
                    copyAnswerExamStudentDetailAllAdapter.setShowAnswer(false);
                }
            }
        });
        final ImageView good = holder.getView(R.id.marking_good);
        final ImageView error = holder.getView(R.id.marking_error);
        if (bean.getAnswer().getCorrectType() == 1) {
            good.setImageResource(R.mipmap.dx_sel);
            goodFlag = false;
        } else {
            good.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }

        if (bean.getAnswer().getCorrectType() == 2) {
            error.setImageResource(R.mipmap.dx_sel);
            errorFlag = false;
        } else {
            error.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }
        holder.getView(R.id.marking_good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goodFlag) {
                    good.setImageResource(R.mipmap.dx_sel);
                    error.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = false;
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "1");
                } else {
                    good.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

        holder.getView(R.id.marking_error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (errorFlag) {
                    error.setImageResource(R.mipmap.dx_sel);
                    good.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = false;
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "2");
                } else {
                    error.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });
    }

    void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showShortlToast("操作成功");

                    }
                });
    }
}
