package com.example.app5kcrw.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import com.example.app5libbase.R;
import com.example.app5libpublic.cons.ListStatus;
import com.example.app5libbase.baseui.adapter.ActivityGvAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.listener.OnCalenderSelectListener;
import com.example.app5libbase.listener.OnSearchClickListener;
import com.example.app5libbase.pop.TaskSearchPop;
import com.example.app5kcrw.presenter.SyncPresenter;
import com.example.app5kcrw.view.SyncView;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CalenderClearEditText;
import com.example.app5libbase.views.CalenderDialog;
import com.example.app5libbase.views.ClearableEditText;
import com.example.app5libbase.views.ImageHintEditText;
import com.example.app5libbase.views.LoadStatusView;
import com.example.app5libbase.views.SpinerPopWindow;
import com.example.app5libbase.views.SpinnerView;
import com.example.app5libbase.views.VersionSpinner;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SyncFragment extends MBaseFragment<SyncPresenter> implements SyncView, View.OnClickListener {


    public static final String SAVE_CATEGORY = "save_category";
    private static final int subjectId = 0;
    private static final String[] TASK_STATUS = {"0", "1", "2", "3", "4"};

    private TextView versionText;
    private View line;
    private TextView nodeText;
    private ImageHintEditText btnSearch;
    private LinearLayout statusLy;
    private RadioGroup statusRg;
    private RadioButton status1;
    private RadioButton status2;
    private RadioButton status3;
    private RadioButton status4;
    private RadioButton status5;
    private TextView classSpinnerTxt;
    private CalenderClearEditText dateChooseTxt;
    private SmartRefreshLayout refreshLayout;
    private GridView syncGridview;
    private LoadStatusView contentError;
    private SpinnerView spinnerView;
    private VersionSpinner versionSpinner;

    private SpinerPopWindow classChoosePop;

    private ActivityGvAdapter mAdapter;
    private CalenderDialog calendarDialog;


    public SyncFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sync, container, false);
        versionText = (TextView) view.findViewById(R.id.version_text);
        line = (View) view.findViewById(R.id.line);
        nodeText = (TextView) view.findViewById(R.id.node_text);
        btnSearch = (ImageHintEditText) view.findViewById(R.id.btnSearch);
        statusLy = (LinearLayout) view.findViewById(R.id.status_ly);
        statusRg = (RadioGroup) view.findViewById(R.id.status_rg);
        status1 = (RadioButton) view.findViewById(R.id.status_1);
        status2 = (RadioButton) view.findViewById(R.id.status_2);
        status3 = (RadioButton) view.findViewById(R.id.status_3);
        status4 = (RadioButton) view.findViewById(R.id.status_4);
        status5 = (RadioButton) view.findViewById(R.id.status_5);
        classSpinnerTxt = (TextView) view.findViewById(R.id.class_spinner_txt);
        dateChooseTxt = (CalenderClearEditText) view.findViewById(R.id.date_choose_txt);
        refreshLayout = (SmartRefreshLayout) view.findViewById(R.id.refreshLayout);
        syncGridview = (GridView) view.findViewById(R.id.sync_gridview);
        contentError = (LoadStatusView) view.findViewById(R.id.content_error);
        spinnerView = (SpinnerView) view.findViewById(R.id.spinner_view);
        versionSpinner = (VersionSpinner) view.findViewById(R.id.version_spinner_view);

        versionText.setOnClickListener(this);
        nodeText.setOnClickListener(this);
        classSpinnerTxt.setOnClickListener(this);
        dateChooseTxt.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        initView();
        initData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // mPresenter.getTaskList();
    }

    private void initView() {
        mAdapter = new ActivityGvAdapter(getActivity());
        syncGridview.setAdapter(mAdapter);
       /* mAdapter.setItemClickRefreshlListener(new ActivityGvAdapter.ItemClickRefreshlListener() {
            @Override
            public void onRefreshl(SyncTaskVo.DataBean dataBean) {
                mPresenter.getTaskList();
            }
        });*/

        mAdapter.setItemDisposalListener(new ActivityGvAdapter.ItemDisposalListener() {
            @Override
            public void onDelet(SyncTaskVo.DataBean dataBean) {
                mPresenter.delTaskItem(String.valueOf(dataBean.getId()));
            }

            @Override
            public void onPublishAnswer(int id) {
                mPresenter.sendAnswer(id + "");
            }
        });

        versionSpinner.setOnItemChoosedListener(new VersionSpinner.ItemChoosedListener() {
            @Override
            public void onItemChoosed(VersionBean.DataBean.VolumeListBean bean) {
                onVersionChoose(bean);
            }
        });

        spinnerView.setItemChoosedListener(new SpinnerView.ItemChooseListener() {
            @Override
            public void onItemChoosed(NodeBean.DataBeanXX.DataBean data) {
                onNodeChoose(data);
            }
        });

        statusRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (id == R.id.status_1) {
                    mPresenter.setStatus(null);
                } else if (id == R.id.status_2) {
                    mPresenter.setStatus(TASK_STATUS[1]);
                } else if (id == R.id.status_3) {
                    mPresenter.setStatus(TASK_STATUS[2]);
                } else if (id == R.id.status_4) {
                    mPresenter.setStatus(TASK_STATUS[3]);
                } else if (id == R.id.status_5) {
                    mPresenter.setStatus(TASK_STATUS[4]);
                }
                mPresenter.getTaskList();
            }
        });

        classChoosePop = new SpinerPopWindow(getActivity());
        classChoosePop.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                if (classVo == null)
                    return;
                onChooseClass(classVo.getData().get(pos));
            }
        });


        contentError.setReloadListener(new LoadStatusView.ReloadListener() {
            @Override
            public void onReloadClicked() {
                mPresenter.getTaskList();
            }
        });


        refreshLayout.setEnableLoadMore(true);
        refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                refreshLayout.setEnableLoadMore(true);
                mPresenter.getTaskList();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshLayout) {
                mPresenter.getTaskList(ListStatus.Loadmore);
            }
        });
    }

    private void initData() {
        mPresenter.setStatus(null);
        mPresenter.getVersionList();
        mPresenter.getClassList();
        mPresenter.getTaskList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.version_text) {
            spinnerView.setVisibility(View.GONE);

            if (versionSpinner.getVisibility() == View.GONE) {
                mPresenter.getVersionList();
                versionSpinner.setList(versionList);
            } else {
                versionSpinner.setVisibility(View.GONE);
            }
        } else if (id == R.id.node_text) {
            versionSpinner.setVisibility(View.GONE);

            if (spinnerView.getVisibility() == View.GONE) {
                spinnerView.setData(nodeList);
            } else {
                spinnerView.setVisibility(View.GONE);
            }
        } else if (id == R.id.class_spinner_txt) {
            classChoosePop.showAsDropDown(classSpinnerTxt);
        } else if (id == R.id.btnSearch) {
            versionSpinner.setVisibility(View.GONE);
            spinnerView.setVisibility(View.GONE);
            showPop();
        } else if (id == R.id.date_choose_txt) {
            showCalendarDialog();
        }
    }

    private TaskSearchPop taskSearchPop;

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(getActivity(), new OnSearchClickListener() {
                @Override
                public void onSearch(String searchStr) {

                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    mPresenter.setKeyword(searchStr);
                    mPresenter.getTaskList();
                }
            });
        }

        taskSearchPop.showPopupWindow(SAVE_CATEGORY, btnSearch, subjectId);
    }

    @Override
    public void initPresenter() {
        mPresenter = new SyncPresenter();
        mPresenter.attachView(this, activity);
    }

    private ArrayList<VersionBean.DataBean.VolumeListBean> versionList;

    @Override
    public void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> retList) {
        this.versionList = retList;
        if (retList == null || retList.size() == 0) {
            return;
        }
        if (versionSpinner.getVisibility() == View.VISIBLE) {
            versionSpinner.setList(versionList);
        }
    }

    private void onVersionChoose(VersionBean.DataBean.VolumeListBean bean) {
        versionText.setText(bean.getBaseVersionName() + bean.getBaseGradeName() + bean.getBaseVolumeName());
        mPresenter.setBaseEducationId(bean.getBaseEducationId());
        mPresenter.setBaseGradeId(bean.getBaseGradeId());
        mPresenter.setBaseVolumeId(bean.getBaseVolumeId());

        mPresenter.getNodeList(bean.getBaseVersionId());
    }

    private List<NodeBean.DataBeanXX> nodeList;

    @Override
    public void onNodeSuccessed(List<NodeBean.DataBeanXX> list) {

        if (list == null) {
            list = new ArrayList<>();
        }

        NodeBean.DataBeanXX dataBeanXX = new NodeBean.DataBeanXX();
        NodeBean.DataBeanXX.DataBean dataBean = new NodeBean.DataBeanXX.DataBean();
        dataBean.setName("全部章节");
        dataBean.setNodeNamePath("全部章节");
        dataBean.setLeaf(true);
        dataBeanXX.setData(dataBean);
        list.add(0, dataBeanXX);

        onNodeChoose(dataBean);
        nodeList = list;

    }

    @Override
    public void onTitleFailed() {
    }

    private void onNodeChoose(NodeBean.DataBeanXX.DataBean data) {
        nodeText.setText(data.getNodeNamePath());
        mPresenter.setChapterNodeIdPath(data.getNodeIdPath());
        mPresenter.getTaskList();
    }

    private SyncClassVo classVo;

    @Override
    public void onClassSuccessed(SyncClassVo classVo) {

        if (classVo.getData() == null || classVo.getData().size() == 0) {
            List<SyncClassVo.DataBean> list = new ArrayList<>();
            classVo.setData(list);
        }
        SyncClassVo.DataBean all = new SyncClassVo.DataBean();
        all.setBaseGradeName("");
        all.setClassName("全部班级");
        classVo.getData().add(0, all);
        this.classVo = classVo;

        List<String> list = new ArrayList<>();
        for (int i = 0; i < classVo.getData().size(); i++) {
            list.add(classVo.getData().get(i).getBaseGradeName() + classVo.getData().get(i).getClassName());
        }
        classChoosePop.setPopDatas(list);
    }

    @Override
    public void onTaskListSuccessed(SyncTaskVo taskVo, ListStatus listStatus, boolean isAll) {

        if (listStatus == ListStatus.Refresh) {
            refreshLayout.finishRefresh(true);
            if (taskVo != null && taskVo.getData() != null && taskVo.getData().size() != 0) {
                contentError.onSuccess();
                //重新设置数据
                mAdapter.setData(taskVo.getData());
            } else {
                contentError.onEmpty();
            }
        } else if (listStatus == ListStatus.Loadmore) {
            refreshLayout.finishLoadMore(true);
            if (taskVo != null && taskVo.getData() != null && taskVo.getData().size() != 0) {
                mAdapter.addData(taskVo.getData());
            }
        }
        refreshLayout.setEnableLoadMore(!isAll);
    }

    @Override
    public void onTaskListFailed(ListStatus listStatus) {
        refreshLayout.finishRefresh(false);
        refreshLayout.finishLoadMore(false);
        if (listStatus == ListStatus.Refresh) {
            contentError.onError();
        }
    }

    @Override
    public void onDelSuccessed() {
        ToastUtil.showShortlToast("删除成功");
    }

    @Override
    public void onDelFailed() {
        mPresenter.getTaskList();
    }

    @Override
    public void onSendAnswerSuccess() {

    }

    private void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(activity, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    dateChooseTxt.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  至  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String startTime = sdf.format(startCalendar.getTimeInMillis());
                    String endTime = sdf.format(endCalendar.getTimeInMillis());

                    mPresenter.setStartTime(startTime);
                    mPresenter.setEndTime(endTime);
                    mPresenter.getTaskList();
                }
            });

            dateChooseTxt.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    mPresenter.setStartTime("");
                    mPresenter.setEndTime("");
                    mPresenter.getTaskList();
                    calendarDialog = null;
                }
            });
        }
        calendarDialog.show();
    }

    public void onChooseClass(SyncClassVo.DataBean classBean) {

        classSpinnerTxt.setText(classBean.getBaseGradeName() + classBean.getClassName());
        mPresenter.setBaseClassId(classBean.getClassId());
        mPresenter.getTaskList();
    }


}
