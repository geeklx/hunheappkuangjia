package com.example.app5kcrw.fragment.natives;


import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5kcrw.adapter.OverallDetailAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.OverallFragmentPresenter;
import com.example.app5kcrw.view.OverallFragmentView;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.OverallVo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.List;

/**
 * 卷面
 */
public class OverallFragment extends MBaseFragment<OverallFragmentPresenter> implements OverallFragmentView, View.OnClickListener {
    private RadioGroup radioGroup;
    private RadioButton leftRb;
    private RadioButton rightRb;
    private TextView numTv;
    private LinearLayout radioGroupRight;
    private Button leftRbRight;
    private Button rightRbRight;
    private RelativeLayout rll;
    private TextView examState;
    private TextView studentName;
    private TextView scoreTv;
    private ImageView goodImg;
    private ImageView delImg;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private RecyclerView recyclerView;

    private Bundle bundle;
    private String classId;
    private String id;
    private OverallDetailAdapter studentDetailAdapter;
    private List<OverallVo.DataBean.AnswerExamListBean> mList = new ArrayList<>();
    private int position = 0;
    private OverallVo positionOverallVo;

    public OverallFragment() {
    }

    public static OverallFragment newInstance(Bundle bundle) {
        OverallFragment fragment = new OverallFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_overall, container, false);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        leftRb = (RadioButton) view.findViewById(R.id.left_rb);
        rightRb = (RadioButton) view.findViewById(R.id.right_rb);
        numTv = (TextView) view.findViewById(R.id.num_tv);
        radioGroupRight = (LinearLayout) view.findViewById(R.id.radioGroup_right);
        leftRbRight = (Button) view.findViewById(R.id.left_rb_right);
        rightRbRight = (Button) view.findViewById(R.id.right_rb_right);
        rll = (RelativeLayout) view.findViewById(R.id.rll);
        examState = (TextView) view.findViewById(R.id.exam_state);
        studentName = (TextView) view.findViewById(R.id.student_name);
        scoreTv = (TextView) view.findViewById(R.id.score_tv);
        goodImg = (ImageView) view.findViewById(R.id.good_img);
        delImg = (ImageView) view.findViewById(R.id.del_img);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        leftRbRight.setOnClickListener(this);
        rightRbRight.setOnClickListener(this);
        goodImg.setOnClickListener(this);
        delImg.setOnClickListener(this);

        initData();
        studentDetailAdapter = new OverallDetailAdapter(App2.get(), mList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        recyclerView.setAdapter(studentDetailAdapter);
        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new OverallFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    private void initData() {
        bundle = getArguments();
        classId = bundle.getString("classId");
        id = bundle.getString("id");
        mPresenter.getOverallList(id, "1");

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == R.id.left_rb) {
                    mPresenter.getOverallList(id, "1");
                } else if (i == R.id.right_rb) {
                    mPresenter.getOverallList(id, "2");
                }
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void setOverallVo(OverallVo overallVo) {
        rll.setVisibility(View.VISIBLE);
        llTaskEmpty.setVisibility(View.GONE);
        positionOverallVo = overallVo;
        mList.clear();
        mList.addAll(overallVo.getData().get(position).getAnswerExamList());
        if (overallVo.getTotal() > 1) {
            leftRbRight.setEnabled(true);
            rightRbRight.setEnabled(true);
            rightRbRight.setTextColor(Color.parseColor("#323C47"));
            leftRbRight.setTextColor(Color.parseColor("#323C47"));

        }
        numTv.setText("第1份/共" + overallVo.getTotal() + "份");
        studentName.setText(overallVo.getData().get(position).getUserStudentName());

        if (overallVo.getData().get(position).getScoreTotal() >= 0) {
            scoreTv.setText((int) overallVo.getData().get(position).getScoreTotal() + "分");
        } else {
            scoreTv.setVisibility(View.GONE);
        }

        refreshAdapter();
    }


    @Override
    public void setNullOverallVo(OverallVo overallVo) {
        rll.setVisibility(View.GONE);
        llTaskEmpty.setVisibility(View.VISIBLE);
        mList.clear();
        refreshAdapter();
    }

    @Override
    public void setdel() {
        position = 0;
        mPresenter.getOverallList(id, "1");

    }

    private void refreshAdapter() {
        studentDetailAdapter.notifyDataSetChanged();

    }

    private boolean flagGoodImg = true;

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.left_rb_right) {
            leftRbRight.setBackgroundColor(getResources().getColor(R.color.fragment_main_chart_line_text));
            leftRbRight.setTextColor(getResources().getColor(R.color.white));

            rightRbRight.setBackgroundColor(getResources().getColor(R.color.white));
            rightRbRight.setTextColor(getResources().getColor(R.color.black));
            if (position <= 0) {
                ToastUtil.showLonglToast("第一份");
                return;
            } else {
                position--;
            }
            Log.e("上一页位置", "上一页位置" + position);
            if (position < 0) {
                ToastUtil.showLonglToast("第一份");
            } else {
                if (mList != null && positionOverallVo != null) {
                    mList.clear();
                    mList.addAll(positionOverallVo.getData().get(position).getAnswerExamList());
                    numTv.setText("第" + (position + 1) + "份/共" + positionOverallVo.getTotal() + "份");


                    refreshAdapter();
                    studentName.setText(positionOverallVo.getData().get(position).getUserStudentName());
                    scoreTv.setText((int) positionOverallVo.getData().get(position).getScoreTotal() + "分");
                }

            }
        } else if (viewId == R.id.right_rb_right) {//fragment_main_chart_line_text
            rightRbRight.setBackgroundColor(getResources().getColor(R.color.fragment_main_chart_line_text));
            rightRbRight.setTextColor(getResources().getColor(R.color.white));

            leftRbRight.setBackgroundColor(getResources().getColor(R.color.white));
            leftRbRight.setTextColor(getResources().getColor(R.color.black));
            if (positionOverallVo == null) {
                return;
            }
            if (position >= positionOverallVo.getData().size()) {
                ToastUtil.showLonglToast("最后一份");
                return;
            }

            Log.e("下一页位置", "下一页位置" + position);
            if (position >= positionOverallVo.getData().size()) {
                ToastUtil.showLonglToast("最后一份");
            } else {
                if (mList != null && positionOverallVo != null) {
                    position++;
                    if (position >= positionOverallVo.getData().size()) {
                        ToastUtil.showLonglToast("最后一份");
                    } else {
                        mList.clear();
                        mList.addAll(positionOverallVo.getData().get(position).getAnswerExamList());
                        numTv.setText("第" + (position + 1) + "份/共" + positionOverallVo.getTotal() + "份");
                        studentName.setText(positionOverallVo.getData().get(position).getUserStudentName());
                        scoreTv.setText((int) positionOverallVo.getData().get(position).getScoreTotal() + "分");
                        refreshAdapter();
                    }


                }
            }
        } else if (viewId == R.id.good_img) {
            if (flagGoodImg) {
                goodImg.setImageResource(R.mipmap.yinc_icon);
                flagGoodImg = false;
                studentName.setText("***");
            } else {
                goodImg.setImageResource(R.mipmap.xs_icon);
                flagGoodImg = true;
                studentName.setText(positionOverallVo.getData().get(position).getUserStudentName());
            }
        } else if (viewId == R.id.del_img) {
            Log.e("dddddddd", "dddddddd" + positionOverallVo.getData().get(position).getId());
            mPresenter.updateState(positionOverallVo.getData().get(position).getId() + "", "3");
        }
    }
}
