package com.example.app5kcrw.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5kcrw.adapter.TabPageAdapter;
import com.google.android.material.tabs.TabLayout;
import com.example.app5libbase.R;
import com.example.app5kcrw.fragment.natives.ExplainFragment;
import com.example.app5kcrw.fragment.natives.PointFragment;
import com.example.app5kcrw.fragment.natives.RganalyzeFragment;
import com.example.app5kcrw.fragment.natives.analyzeFragment;
import com.example.app5kcrw.fragment.natives.correctFragment;
import com.example.app5kcrw.fragment.natives.statisticsFragment;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5kcrw.presenter.CorrectNativePresenter;
import com.example.app5kcrw.view.CorrectNativeView;
import com.example.app5libbase.views.NoSrcollViewPage;

import java.util.ArrayList;
import java.util.List;

public class CorrectNativeActivity extends MBaseActivity<CorrectNativePresenter> implements CorrectNativeView {


    private TextView tvBack;
    private TextView tvTitle;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private TabLayout mTabLayout;
    private ImageView shuaxin;
    private RadioGroup answerGroup;
    private RadioButton leftRb;
    private RadioButton rightRb;
    private RadioGroup corrgroup;
    private RadioButton corrLeftRb;
    private RadioButton corrRightRb;
    private NoSrcollViewPage mViewPager;


    private String type;
    private String name;
    private String classId;
    private String id;
    private List<Fragment> fragments;
    //private String[] titles = {"作答统计", "批改", "题目分析", "资料分析", "讲解典型", "卷面"};
    private String[] titles = {"作答统计", "批改", "题目分析", "知识点分析", "资料分析", "讲解典型"};
    private Bundle bundle;
    private statisticsFragment statisticsFragment;
    private correctFragment correctFragment;
    private String teacherExamId;

    @Override
    public void initPresenter() {
        mPresenter = new CorrectNativePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correct_native);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        llTaskEmpty = (LinearLayout) findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) findViewById(R.id.tvTaskEmpty);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        shuaxin = (ImageView) findViewById(R.id.shuaxin);
        answerGroup = (RadioGroup) findViewById(R.id.answerGroup);
        leftRb = (RadioButton) findViewById(R.id.left_rb);
        rightRb = (RadioButton) findViewById(R.id.right_rb);
        corrgroup = (RadioGroup) findViewById(R.id.corrgroup);
        corrLeftRb = (RadioButton) findViewById(R.id.corr_left_rb);
        corrRightRb = (RadioButton) findViewById(R.id.corr_right_rb);
        mViewPager = (NoSrcollViewPage) findViewById(R.id.viewPager);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bundle = new Bundle();
        initData();
        initView();
        initViewPage();
        mTabLayout.setupWithViewPager(mViewPager);
        if (type.equals("correct")) {
            mTabLayout.getTabAt(1).select();
        }
        if (type.equals("representative")) {
            mTabLayout.getTabAt(4).select();
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入CorrectNativeActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        tvTitle.setText(name);
    }

    @Override
    protected void initData() {
        name = getIntent().getStringExtra("name");
        classId = getIntent().getStringExtra("classId");
        id = getIntent().getStringExtra("id");
        type = getIntent().getStringExtra("type");
        teacherExamId = getIntent().getStringExtra("teacherExamId");

        bundle.putString("classId", classId);
        bundle.putString("id", id);
        bundle.putString("name", name);
        bundle.putString("teacherExamId", teacherExamId);
    }

    private void initViewPage() {
        fragments = new ArrayList<>();
        //将提前写好三个Fragment添加到集合中
        statisticsFragment = statisticsFragment.newInstance(bundle);//作答统计
        //fragments.add(correctFragment.newInstance(bundle));
        fragments.add(statisticsFragment);
        correctFragment = correctFragment.newInstance(bundle);
        fragments.add(correctFragment);///批改
        fragments.add(analyzeFragment.newInstance(bundle));//题目分析
        fragments.add(PointFragment.newInstance(bundle));// 知识点分析
        fragments.add(RganalyzeFragment.newInstance(bundle));//资料分析
        fragments.add(ExplainFragment.newInstance(bundle));//讲解典型
        //fragments.add(OverallFragment.newInstance(bundle));

        //创建适配器
        TabPageAdapter pageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments, titles);
        //设置ViewPager的适配器
        mViewPager.setAdapter(pageAdapter);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equals("作答统计")) {
                    answerGroup.setVisibility(View.VISIBLE);
                    corrgroup.setVisibility(View.GONE);
                    shuaxin.setVisibility(View.VISIBLE);
                } else if (tab.getText().toString().equals("批改")) {
                    answerGroup.setVisibility(View.GONE);
                    corrgroup.setVisibility(View.VISIBLE);
                    shuaxin.setVisibility(View.GONE);
                } else {
                    corrgroup.setVisibility(View.GONE);
                    answerGroup.setVisibility(View.GONE);
                    shuaxin.setVisibility(View.GONE);
                }
                Log.e("", "");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.e("", "");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e("", "");
                if (tab.getText().toString().equals("作答统计")) {
                    statisticsFragment.allSee();
                    answerGroup.setVisibility(View.VISIBLE);
                    corrgroup.setVisibility(View.GONE);
                    shuaxin.setVisibility(View.VISIBLE);
                } else if (tab.getText().toString().equals("批改")) {
                    correctFragment.allSee();
                    answerGroup.setVisibility(View.GONE);
                    corrgroup.setVisibility(View.VISIBLE);
                    shuaxin.setVisibility(View.GONE);
                }
            }
        });
        /*作答统计*/
        answerGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.left_rb) {
                    statisticsFragment.allSee();
                } else if (i == R.id.right_rb) {
                    statisticsFragment.groupSee();
                }
            }
        });
        /*批改*/
        corrgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.corr_left_rb) {
                    correctFragment.allSee();
                } else if (i == R.id.corr_right_rb) {
                    correctFragment.groupSee();
                }
            }
        });

        shuaxin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statisticsFragment.updateData();
            }
        });
    }

}
