package com.example.app5kcrw.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.util.MySeekBarChangeListener;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.MarkingCorrectParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/15
 * 修改单号：
 * 修改内容:
 */
public class MarkingcorrectScoreAdapter extends BaseAdapter {

    private List<AnswerExamCorrectAllStudentVo.DataBean> mList;
    private List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> mExamList;
    private Context mContext;
    private List<MarkingCorrectParam> correctParams = new ArrayList<>();

    public MarkingcorrectScoreAdapter(List<AnswerExamCorrectAllStudentVo.DataBean> mList, List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> mExamList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
        this.mExamList = mExamList;
        setParam(mList);
        setParam1(mExamList);
    }

    void setParam1(List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> mExamList) {
        for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean dataBean : mExamList) {
            MarkingCorrectParam correctParam = new MarkingCorrectParam();
            correctParam.setId(dataBean.getId());
            correctParam.setParentId(dataBean.getParentId());
            correctParam.setIsRight(dataBean.getIsRight());
            correctParam.setFullScore(dataBean.getFullScore());
            correctParam.setIsCorrent(dataBean.getIsCorrect());
            correctParam.setLessonTaskId(dataBean.getLessonTaskId());
            correctParam.setLessonTaskStudentId(dataBean.getLessonTaskStudentId());
            correctParam.setScore(dataBean.getScore());
            correctParam.setExamTypeId(dataBean.getTemplateId());
            List<MarkingCorrectParam> correctParamList = new ArrayList<>();
            for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean examOptionBean : dataBean.getExamOptionList()) {
                MarkingCorrectParam examOptionBeanParam = new MarkingCorrectParam();
                examOptionBeanParam.setId(examOptionBean.getId());
                examOptionBeanParam.setParentId(dataBean.getParentId());
                examOptionBeanParam.setFullScore(dataBean.getFullScore());
                examOptionBeanParam.setIsRight(examOptionBean.getIsRight());
                examOptionBeanParam.setScore(examOptionBean.getScore());
                examOptionBeanParam.setLessonTaskId(examOptionBean.getLessonAnswerExamId());
                examOptionBeanParam.setLessonTaskStudentId(examOptionBean.getLessonTaskStudentId());
                correctParamList.add(examOptionBeanParam);
                correctParam.setChildren(correctParamList);
            }
            correctParams.add(correctParam);
        }
    }


    void setParam(List<AnswerExamCorrectAllStudentVo.DataBean> mList) {
        for (AnswerExamCorrectAllStudentVo.DataBean dataBean : mList) {
            MarkingCorrectParam correctParam = new MarkingCorrectParam();
            correctParam.setId(dataBean.getAnswer().getId());
            correctParam.setParentId(dataBean.getAnswer().getParentId());
            correctParam.setIsRight(dataBean.getAnswer().getIsRight());
            correctParam.setFullScore(dataBean.getAnswer().getFullScore());
            correctParam.setIsCorrent(dataBean.getAnswer().getIsCorrect());
            correctParam.setScore(dataBean.getAnswer().getScore());
            correctParam.setExamTypeId(dataBean.getAnswer().getTemplateId());
            correctParam.setLessonTaskStudentId(dataBean.getAnswer().getLessonTaskStudentId());
            correctParam.setLessonTaskId(dataBean.getAnswer().getLessonTaskId());
            if (dataBean.getAnswer().getTemplateId() == 6) {
                List<MarkingCorrectParam> correctParamList = new ArrayList<>();
                for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean examOptionBean : dataBean.getAnswer().getExamOptionList()) {
                    MarkingCorrectParam examOptionBeanParam = new MarkingCorrectParam();
                    examOptionBeanParam.setId(examOptionBean.getId());
                    examOptionBeanParam.setParentId(dataBean.getAnswer().getParentId());
                    examOptionBeanParam.setFullScore(dataBean.getAnswer().getFullScore());
                    examOptionBeanParam.setIsRight(examOptionBean.getIsRight());
                    examOptionBeanParam.setScore(examOptionBean.getScore());
                    examOptionBeanParam.setLessonTaskId(examOptionBean.getLessonAnswerExamId());
                    examOptionBeanParam.setLessonTaskStudentId(examOptionBean.getLessonTaskStudentId());
                    correctParamList.add(examOptionBeanParam);
                }
                correctParam.setChildren(correctParamList);
            }
            correctParams.add(correctParam);
        }
    }

    public List<MarkingCorrectParam> getCorrectParams() {
        return correctParams;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder fholder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_marking_fill_exam_socre, null);

            fholder = new ViewHolder(convertView);
            convertView.setTag(fholder);
        } else {
            fholder = (ViewHolder) convertView.getTag();
        }
        final ViewHolder holder = fholder;
        if (mList.get(position).getAnswer().getTemplateId() == 6) {
            holder.markingName.setText(mList.get(position).getAnswer().getUserStudentName());//mList.get(position).getAnswer().getExamSeq() + "." + mList.get(position).getAnswer().getTemplateStyleName() + ""
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.examScore.setVisibility(View.GONE);
            holder.seekbar.setVisibility(View.GONE);
            holder.linearLayout.removeAllViews();

            for (int i = 0; i < mList.get(position).getAnswer().getExamOptionList().size(); i++) {
                View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_fill_exam_pigai, null);
                TextView examName = inflate.findViewById(R.id.exam_name);

                final ImageView dui = inflate.findViewById(R.id.dui);
                final ImageView cuo = inflate.findViewById(R.id.cuo);
                final int finalI = i;

                examName.setText("(" + (i + 1) + ")");
                holder.linearLayout.addView(inflate);
                if (mList.get(position).getAnswer().getExamOptionList().get(i).getIsRight() == 0) {
                    cuo.setImageResource(R.mipmap.cuo_pigai);
                }
                if (mList.get(position).getAnswer().getExamOptionList().get(i).getIsRight() == 1) {
                    dui.setImageResource(R.mipmap.dui_pigai);
                }

                dui.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {//1 对
                        mList.get(position).getAnswer().getExamOptionList().get(finalI).getId();
                        for (MarkingCorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                for (MarkingCorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                    if (childrenCorrectParam.getId() == mList.get(position).getAnswer().getExamOptionList().get(finalI).getId()) {
                                        if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 0) {
                                            dui.setImageResource(R.mipmap.dui_pigai);
                                            childrenCorrectParam.setIsRight(1);
                                            childrenCorrectParam.setScore(childrenCorrectParam.getFullScore());
                                            cuo.setImageResource(R.mipmap.cuo);
                                        } else if (childrenCorrectParam.getIsRight() == 1) {
                                            dui.setImageResource(R.mipmap.dui);
                                            childrenCorrectParam.setIsRight(-1);

                                            cuo.setImageResource(R.mipmap.cuo);
                                        }

                                    }
                                }
                            }
                        }
                    }
                });
                cuo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mList.get(position).getAnswer().getExamOptionList().get(finalI).getId();
                        for (MarkingCorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                for (MarkingCorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                    if (childrenCorrectParam.getId() == mList.get(position).getAnswer().getExamOptionList().get(finalI).getId()) {
                                        if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 1) {
                                            cuo.setImageResource(R.mipmap.cuo_pigai);
                                            childrenCorrectParam.setIsRight(0);
                                            childrenCorrectParam.setScore(0);
                                            dui.setImageResource(R.mipmap.dui);
                                        } else if (childrenCorrectParam.getIsRight() == 0) {
                                            cuo.setImageResource(R.mipmap.cuo);
                                            childrenCorrectParam.setIsRight(-1);
                                            dui.setImageResource(R.mipmap.dui);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        } else if (mList.get(position).getAnswer().getTemplateId() == 16) {   //综合题
            for (final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean dataBean : mList.get(position).getAnswer().getExamList()) {
                if (dataBean.getTemplateId() == 6) {
                    //holder.examName.setText(dataBean.getAnswer().getExamSeq() + "." + dataBean.getAnswer().getTemplateStyleName() + "");
                    holder.markingName.setText(mList.get(position).getAnswer().getUserStudentName());
                    holder.linearLayout.setVisibility(View.VISIBLE);
                    holder.examScore.setVisibility(View.GONE);
                    holder.seekbar.setVisibility(View.GONE);
                    holder.linearLayout.removeAllViews();

                    for (int i = 0; i < dataBean.getExamOptionList().size(); i++) {
                        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_fill_exam_pigai, null);
                        TextView examName = inflate.findViewById(R.id.exam_name);

                        final ImageView dui = inflate.findViewById(R.id.dui);
                        final ImageView cuo = inflate.findViewById(R.id.cuo);
                        final int finalI = i;

                        examName.setText("(" + (i + 1) + ")");
                        holder.linearLayout.addView(inflate);
                        if (dataBean.getExamOptionList().get(i).getIsRight() == 0) {
                            cuo.setImageResource(R.mipmap.cuo_pigai);
                        }
                        if (dataBean.getExamOptionList().get(i).getIsRight() == 1) {
                            dui.setImageResource(R.mipmap.dui_pigai);
                        }

                        dui.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {//1 对
                                dataBean.getExamOptionList().get(finalI).getId();
                                for (MarkingCorrectParam correctParam : correctParams) {
                                    for (int i = 0; i < mExamList.size(); i++) {
                                        if (mExamList.get(i).getId() == correctParam.getId()) {
                                            for (MarkingCorrectParam childrenCorrectParam : correctParam.getChildren()) {
                                                if (childrenCorrectParam.getId() == dataBean.getExamOptionList().get(finalI).getId()) {
                                                    if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 0) {
                                                        dui.setImageResource(R.mipmap.dui_pigai);
                                                        childrenCorrectParam.setIsRight(1);
                                                        childrenCorrectParam.setScore(childrenCorrectParam.getFullScore());
                                                        cuo.setImageResource(R.mipmap.cuo);
                                                    } else if (childrenCorrectParam.getIsRight() == 1) {
                                                        dui.setImageResource(R.mipmap.dui);
                                                        childrenCorrectParam.setIsRight(-1);
                                                        cuo.setImageResource(R.mipmap.cuo);
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        cuo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dataBean.getExamOptionList().get(finalI).getId();
                                for (MarkingCorrectParam correctParam : correctParams) {
                                    for (int i = 0; i < mExamList.size(); i++) {
                                        if (mExamList.get(i).getId() == correctParam.getId()) {
                                            for (MarkingCorrectParam childrenCorrectParam : correctParam.getChildren()) {

                                                if (childrenCorrectParam.getId() == dataBean.getExamOptionList().get(finalI).getId()) {
                                                    if (childrenCorrectParam.getIsRight() == -1 || childrenCorrectParam.getIsRight() == 1) {
                                                        cuo.setImageResource(R.mipmap.cuo_pigai);
                                                        childrenCorrectParam.setIsRight(0);
                                                        childrenCorrectParam.setScore(0);
                                                        dui.setImageResource(R.mipmap.dui);
                                                    } else if (childrenCorrectParam.getIsRight() == 0) {
                                                        cuo.setImageResource(R.mipmap.cuo);
                                                        childrenCorrectParam.setIsRight(-1);
                                                        dui.setImageResource(R.mipmap.dui);
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                } else {

                    if (Double.valueOf(dataBean.getScore()) > -1) {
                        // holder.linearLayout.setVisibility(View.GONE);
                        holder.examScore.setVisibility(View.VISIBLE);
                        holder.seekbar.setVisibility(View.VISIBLE);
                        holder.examScore.setText(dataBean.getScore() + "");
                        holder.seekbar.setMax((int) dataBean.getFullScore());
                        holder.seekbar.setProgress(Double.valueOf(mList.get(position).getAnswer().getScore()).intValue());
                        holder.markingName.setText(mList.get(position).getAnswer().getUserStudentName());
                        // holder.examName.setText(dataBean.getAnswer().getExamSeq() + "." + dataBean.getAnswer().getTemplateStyleName());

                    } else {
                        holder.examScore.setVisibility(View.GONE);
                        holder.seekbar.setVisibility(View.GONE);
                        holder.markingName.setVisibility(View.GONE);
                    }

                }
            }


        } else if (Double.valueOf(mList.get(position).getAnswer().getScore()) > -1) {//除了填空和綜合
            holder.linearLayout.setVisibility(View.GONE);
            holder.examScore.setVisibility(View.VISIBLE);
            holder.seekbar.setVisibility(View.VISIBLE);


            holder.examScore.setText(mList.get(position).getAnswer().getScore() + "");
            holder.seekbar.setMax((int) mList.get(position).getAnswer().getFullScore());
            holder.seekbar.setProgress(Double.valueOf(mList.get(position).getAnswer().getScore()).intValue());
            // holder.examName.setText(mList.get(position).getAnswer().getExamSeq() + "." + mList.get(position).getAnswer().getTemplateStyleName());
            holder.markingName.setText(mList.get(position).getAnswer().getUserStudentName());
        } else {
            holder.examScore.setVisibility(View.GONE);
            holder.seekbar.setVisibility(View.GONE);
            holder.markingName.setVisibility(View.GONE);
        }

        holder.textWatcher = new MyTextWatcher(holder) {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    holder.seekbar.setProgress(Double.valueOf(s.toString()).intValue());
                }

            }
        };
        holder.examScore.addTextChangedListener(holder.textWatcher);
        holder.seekbar.setOnSeekBarChangeListener(new MySeekBarChangeListener(position) {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (mList.get(position).getAnswer().getTemplateId() == 4 || mList.get(position).getAnswer().getTemplateId() == 16) {
                    if (progress >= ((int) mList.get(position).getAnswer().getFullScore())) {
                        holder.examScore.setText(mList.get(position).getAnswer().getFullScore() + "");
                        for (MarkingCorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                correctParam.setScore(mList.get(position).getAnswer().getFullScore());
                            }
                        }
                    } else {
                        holder.examScore.setText(progress + "");
                        for (MarkingCorrectParam correctParam : correctParams) {
                            if (mList.get(position).getAnswer().getId() == correctParam.getId()) {
                                correctParam.setScore(progress);
                            }
                        }
                    }
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return convertView;
    }

    static abstract class MyTextWatcher implements TextWatcher {
        public ViewHolder holder;

        public MyTextWatcher(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    static class ViewHolder {
        TextView markingName;
        ImageView markingPhone;
        EditText examScore;
        SeekBar seekbar;
        LinearLayout linearLayout;
        MyTextWatcher textWatcher;

        ViewHolder(View view) {
            markingName = view.findViewById(R.id.tv_marking_name);
            markingPhone = view.findViewById(R.id.iv_marking_phone);
            examScore = view.findViewById(R.id.exam_score);
            seekbar = view.findViewById(R.id.seekbar);
            linearLayout = view.findViewById(R.id.filling);
        }
    }
}
