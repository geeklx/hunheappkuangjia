package com.example.app5kcrw.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/14
 * 修改单号：
 * 修改内容:
 */
public class TabPageAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;
    private String[] tabNames;//tab选项名字

    public TabPageAdapter(FragmentManager fm, List<Fragment> fragments, String[] tabNames) {
        super(fm);
        this.fragments = fragments;
        this.tabNames = tabNames;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    //重写这个方法，将设置每个Tab的标题
    @Override
    public CharSequence getPageTitle(int position) {
        return tabNames[position];
    }

}
