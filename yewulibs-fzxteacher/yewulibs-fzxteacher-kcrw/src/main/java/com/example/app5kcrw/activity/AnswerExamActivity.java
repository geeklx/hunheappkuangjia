package com.example.app5kcrw.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5kcrw.adapter.SloganAdapter;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;

import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.EmojiKeyboard;
import com.sdzn.fzx.teacher.vo.SloganBane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 评论
 *
 * @author zs
 */

public class AnswerExamActivity extends AppCompatActivity {
    private final String TAG = "AnswerExamActivity";
    List<SloganBane> sloganBeanList = new ArrayList<>();

    private RecyclerView rl_slogan;
    private TextView tv_save;//发送
    private ImageView iv_exittext;//弹起软键盘
    private EditText et_inputMessage;//输入框
    private ImageView iv_more;//弹起评语
    private LinearLayout ll_rootEmojiPanel;//外层包裹评语列表
    private SloganAdapter sloganAdapter;
    private EmojiKeyboard emojiKeyboard;
    private View rv_messageList;
    private String messagge;
    private int lessonTaskId;
    private int lessonTaskStudentId;
    private int lessonAnswerExamId;
    private InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_answerexam_item);
        this.inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入AnswerExamActivity成功");

                    lessonTaskId = appLinkIntent.getIntExtra("lessonTaskId", 0);
                    lessonTaskStudentId = appLinkIntent.getIntExtra("lessonTaskStudentId", 0);
                    lessonAnswerExamId = appLinkIntent.getIntExtra("lessonAnswerExamId", 0);
                }
            }
        }

        rl_slogan = findViewById(R.id.rl_slogan);
        iv_exittext = findViewById(R.id.iv_exittext);//弹起软键盘
        et_inputMessage = findViewById(R.id.et_inputMessage);
        iv_more = findViewById(R.id.iv_more);
        ll_rootEmojiPanel = findViewById(R.id.ll_rootEmojiPanel);
        tv_save = findViewById(R.id.tv_save);
        rv_messageList = findViewById(R.id.rv_messageList);
        iv_exittext.setImageResource(R.mipmap.yes_keyboard);
        openKeyBoard(et_inputMessage);
        showpingyushuju();
        rv_messageList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                et_inputMessage.setText("");
                inputMethodManager.hideSoftInputFromWindow(et_inputMessage.getWindowToken(), 0);
                AnswerExamActivity.this.finish();
                return false;
            }
        });
        initView();
        initData();

    }

    protected void initView() {
        emojiKeyboard = new EmojiKeyboard(AnswerExamActivity.this, et_inputMessage, ll_rootEmojiPanel, iv_more, iv_exittext, rv_messageList);
        emojiKeyboard.setEmoticonPanelVisibilityChangeListener(new EmojiKeyboard.OnEmojiPanelVisibilityChangeListener() {
            @Override
            public void onShowEmojiPanel() {
                android.util.Log.e(TAG, "onShowEmojiPanel");
            }

            @Override
            public void onHideEmojiPanel() {
                android.util.Log.e(TAG, "onHideEmojiPanel");
            }
        });
        showpingyushuju();
        sloganAdapter.setOnItemClickLitener(new SloganAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                et_inputMessage.setText(String.valueOf(sloganBeanList.get(position).getName()));
                Toast.makeText(AnswerExamActivity.this, "" + "这是条目" + sloganBeanList.get(position).getName(), Toast.LENGTH_SHORT);
            }
        });
        ll_rootEmojiPanel.setVisibility(View.GONE);
        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messagge = et_inputMessage.getText().toString().trim();
                android.util.Log.e(TAG, "获取到的数据" + messagge);
                correctSavecomment(messagge);
            }
        });
    }

    protected void initData() {
    }

    private void showpingyushuju() {
        if (!sloganBeanList.isEmpty()) {
            sloganBeanList.clear();
        }
        /*评语假数据*/
        for (int i = 0; i < 1; i++) {
            SloganBane groupBean = new SloganBane("作答很棒！继续加油！");
            sloganBeanList.add(groupBean);
            SloganBane groupBean1 = new SloganBane("作答很棒！书写很棒！很赞！");
            sloganBeanList.add(groupBean1);
            SloganBane groupBean2 = new SloganBane("作答的很棒，请注意卷面书写，继续加油！");
            sloganBeanList.add(groupBean2);
            SloganBane groupBean3 = new SloganBane("如果作答更仔细一点，会更好哦！加油！");
            sloganBeanList.add(groupBean3);
            SloganBane groupBean4 = new SloganBane("有进步！请继续努力！");
            sloganBeanList.add(groupBean4);
        }
        sloganAdapter = new SloganAdapter(this, sloganBeanList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rl_slogan.setLayoutManager(layoutManager);
        rl_slogan.setAdapter(sloganAdapter);
        ll_rootEmojiPanel.setVisibility(View.GONE);
    }

    public void openKeyBoard(final EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                InputMethodManager imm =
                        (InputMethodManager)
                                getApplication()
                                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, 0);
                editText.setSelection(editText.getText().length());
            }
        }, 200);
    }

    //提交评语
    public void correctSavecomment(String comment) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("lessonTaskId", lessonTaskId);
        param.put("lessonTaskStudentId", lessonTaskStudentId);
        param.put("lessonAnswerExamId", lessonAnswerExamId);
        param.put("comment", comment);
        Network.createTokenService(NetWorkService.CorrectingWayService.class)
                .SaveComment(param)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        //ToastUtil.showLonglToast("操作成功");
                        inputMethodManager.hideSoftInputFromWindow(et_inputMessage.getWindowToken(), 0);
                        AnswerExamActivity.this.finish();
                    }
                });
    }
}
