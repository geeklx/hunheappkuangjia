package com.example.app5kcrw.presenter;

import com.example.app5kcrw.activity.StudentDetailsActivity;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.StudentDetailsView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.google.gson.Gson;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class StudentDetailsPresenter extends BasePresenter<StudentDetailsView, StudentDetailsActivity> {
    public void getStudentDetailList(String id) {
        Network.createTokenService(NetWorkService.GetStudentDetailService.class)
                .StudentStatisc(id)
                .map(new StatusFunc<StudentDetails>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentDetails>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StudentDetails o) {
                        initData(o);
                        mView.setStudentDetail(o);
                    }
                });
    }

    private void initData(StudentDetails beans) {
        List<StudentDetails.DataBean> list = beans.getData();
        Gson gson = new Gson();
        if (list == null) {
            return;
        }
        for (StudentDetails.DataBean bean : list) {

            String resourceText = bean.getAnswer().getExamText();
            ExamText examText = gson.fromJson(resourceText, ExamText.class);
            bean.getAnswer().setExamTextVo(examText);

                        /*    //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);*/
            //综合题额外进行一轮解析
            if (bean.getAnswer().getTemplateId() != 16) {
                continue;
            }
            //   List<StudentDetails.DataBean.ExamListBean> examList = bean.getExamList();
            for (StudentDetails.DataBean.AnswerBean dataBean : bean.getAnswer().getExamList()) {
                ExamText json = gson.fromJson(dataBean.getExamText(), ExamText.class);
                dataBean.setExamTextVo(json);
            }
        }
    }

    public void getStudentStatistics(String id) {

        Network.createTokenService(NetWorkService.GetStudentDetailsService.class)
                .StudentStatisc(id)
                .map(new StatusFunc<StatisticsVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StatisticsVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StatisticsVo o) {
                        mView.StudentStatics(o);

                    }
                });
    }


    public void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateStudentExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                                mView.updateStatics();
                            }
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("操作成功");

                    }
                });
    }
}
