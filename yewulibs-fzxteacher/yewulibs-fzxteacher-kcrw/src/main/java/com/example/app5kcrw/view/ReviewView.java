package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;

/**
 * ReviewView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface ReviewView extends BaseView {

    public void onVersionSuccessed(ReviewVersionBean reviewVersionBean);

    public void onVersionFailed();

    public void onClassSuccessed(SyncClassVo classVo);

    public void onTaskListSuccessed(SyncTaskVo taskVo);

    public void onTaskListFailed();

    public void onDelSuccessed();

    public void onDelFailed();
}
