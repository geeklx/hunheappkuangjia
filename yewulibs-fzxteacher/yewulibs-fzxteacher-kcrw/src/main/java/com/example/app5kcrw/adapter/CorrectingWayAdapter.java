package com.example.app5kcrw.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.CorrectingWayVo;

import java.util.List;

public class CorrectingWayAdapter extends RecyclerView.Adapter<CorrectingWayAdapter.ViewHolder> {
    private List<CorrectingWayVo> groupBeanList;

    public CorrectingWayAdapter(List<CorrectingWayVo> groupBeanList) {
        this.groupBeanList = groupBeanList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_correctingwaylist, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CorrectingWayVo groupBean = groupBeanList.get(position);
        String name = String.valueOf(groupBean.getGroupName());
        List<CorrectingWayVo.StudentsBean> studentBeanList = groupBean.getStudents();
        holder.nameTextView.setText(name);
        CorrectingWayItemAdapter studentAdapter = new CorrectingWayItemAdapter(studentBeanList);
        holder.StudentRecyclerView.setAdapter(studentAdapter);
        holder.StudentRecyclerView.setLayoutManager(new GridLayoutManager(holder.StudentRecyclerView.getContext(), 5));
    }

    @Override
    public int getItemCount() {
        return groupBeanList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private RecyclerView StudentRecyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.tv_title);
            StudentRecyclerView = itemView.findViewById(R.id.rv_item);
        }
    }
}