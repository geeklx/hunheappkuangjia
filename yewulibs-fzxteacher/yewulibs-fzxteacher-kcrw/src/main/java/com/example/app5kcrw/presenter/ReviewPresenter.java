package com.example.app5kcrw.presenter;

import android.text.TextUtils;

import com.example.app5kcrw.view.ReviewView;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * ReviewPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ReviewPresenter extends BasePresenter<ReviewView, BaseActivity> {
    private String baseEducationId;
    private String baseGradeId;
    private String baseVolumeId;
    private String status;
    private String baseClassId;
    private String typeReview;
    private String startTime;
    private String endTime;
    private String chapterId;
    private String chapterNodeIdPath;
    private String type = "2";
    private String page;
    private String rows;
    private String keyword;

    public String getBaseEducationId() {
        return dealNullStr(baseEducationId);
    }

    public void setBaseEducationId(String baseEducationId) {
        this.baseEducationId = baseEducationId;
    }

    public String getBaseGradeId() {
        return dealNullStr(baseGradeId);
    }

    public void setBaseGradeId(String baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseVolumeId() {
        return dealNullStr(baseVolumeId);
    }

    public void setBaseVolumeId(String baseVolumeId) {
        this.baseVolumeId = baseVolumeId;
    }

    public String getStatus() {
        return dealNullStr(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBaseClassId() {
        return dealNullStr(baseClassId);
    }

    public void setBaseClassId(String baseClassId) {
        this.baseClassId = baseClassId;
    }

    public String getTypeReview() {
        return dealNullStr(typeReview);
    }

    public void setTypeReview(String typeReview) {
        this.typeReview = typeReview;
    }

    public String getStartTime() {
        return dealNullStr(startTime);
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return dealNullStr(endTime);
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChapterId() {
        return dealNullStr(chapterId);
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterNodeIdPath() {
        return dealNullStr(chapterNodeIdPath);
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPage() {
        return dealNullStr(page);

    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getRows() {
        return dealNullStr(rows);
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getKeyword() {
        return dealNullStr(keyword);
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void getClassList() {
        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getClassList(subject)
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        mView.onClassSuccessed(o);

                    }
                });
    }

    public void getTaskList() {

        String baseSubjectId = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";


        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getTaskList(getBaseEducationId(), getBaseGradeId(), getBaseVolumeId(),
                        baseSubjectId, getStatus(), getStatus(), getBaseClassId(), getTypeReview(),
                        getStartTime(), getEndTime(), getChapterId(), getChapterNodeIdPath(),
                        getType(), getPage(), getRows(), getKeyword())
                .map(new StatusFunc<SyncTaskVo>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<SyncTaskVo>(new SubscriberListener<SyncTaskVo>() {
                    @Override
                    public void onNext(SyncTaskVo o) {

                        mView.onTaskListSuccessed(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.onTaskListFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    public void getVersionList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getReviewList()
                .map(new StatusFunc<ReviewVersionBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ReviewVersionBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onVersionFailed();
                    }

                    @Override
                    public void onNext(ReviewVersionBean versionBean) {
                        dealVersionBean(versionBean);
                    }
                });

    }

    public void delTaskItem(String id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .delTaskItem(id)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        mView.onDelSuccessed();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onDelFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }


    void dealVersionBean(ReviewVersionBean versionBean) {

        if (versionBean == null) {
            versionBean = new ReviewVersionBean();
        }
        if (versionBean.getData() == null) {
            versionBean.setData(new ArrayList<ReviewVersionBean.DataBean>());
        }
        ReviewVersionBean.DataBean dataBean = new ReviewVersionBean.DataBean();
        dataBean.setReviewTypeName("全部复习");
        versionBean.getData().add(0, dataBean);

        mView.onVersionSuccessed(versionBean);
    }

    public void sendAnswer(String id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .sendAnswer(id)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }


    private String dealNullStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;

    }

}
