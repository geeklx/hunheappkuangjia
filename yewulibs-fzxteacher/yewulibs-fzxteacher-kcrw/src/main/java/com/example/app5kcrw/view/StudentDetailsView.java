package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StudentDetails;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public interface StudentDetailsView extends BaseView {

    void setStudentDetail(StudentDetails examVo);

    void StudentStatics(StatisticsVo statisticsVo);

    void updateStatics();
}
