package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.student.libutils.util.Log;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.AnalyzeVo;
import com.sdzn.fzx.teacher.vo.ExamText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/31
 * 修改单号：
 * 修改内容:
 */
public class AnalyzeExamAdapter extends BaseRcvAdapter<AnalyzeVo.DataBean> {

    public int selectPostion = 0;
    private Activity mActivity = null;
    private boolean showAnswer = true;//答案

    public AnalyzeExamAdapter(Context context, List<AnalyzeVo.DataBean> mList, Activity activity) {
        super(context, mList);
        mActivity = activity;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        AnalyzeVo.DataBean bean = mList.get(position);
        return bean.getExamTemplateId();
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_jianda_exam);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_text);
            case 14://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 16://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnalyzeVo.DataBean bean) {

        switch (bean.getExamTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }


    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final AnalyzeVo.DataBean bean) {
        bindExamTitle(holder, position, bean);

        ExamText examTextVo = bean.getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        tv.setTextSize(20);
        tv.setHtmlText(examTextVo.getExamStem());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setTextSize(20);
            tvText.setTextSize(20);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            // final List<AnswerListBean.ExamOptionBean> examList = bean.getEx;
            List<ExamText.ExamOptionsBean> examOptions = bean.getExamTextVo().getExamOptions();
            if (examOptions == null || examOptions.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examOptions);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examOptions.get(0).getContent()));
            } else {//多选
                Collections.sort(examOptions);
                for (ExamText.ExamOptionsBean optionBean : examOptions) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getContent())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }


    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final AnalyzeVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        tv.setTextSize(20);

    }


    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final AnalyzeVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        tv.setTextSize(20);
        holder.getView(R.id.line).setVisibility(View.GONE);
        holder.getView(R.id.tv_un_answer).setVisibility(View.GONE);
        holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
    }


    /**
     * 完型填空
     */
    private void bindFillExam(final BaseViewHolder holder, final int position, final AnalyzeVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());

        tv.setTextSize(20);
    }


    private void bindExamTitle(final BaseViewHolder holder, final int position, final AnalyzeVo.DataBean bean) {
        final String id = String.valueOf(bean.getId());
        TextView textCount = holder.getView(R.id.tv_count);
        TextView textType = holder.getView(R.id.tv_type);


        if (bean.getScore() > -1) {
            if (bean.getExamTemplateId() == 6) {
            } else {
            }
            textCount.setText(position + 1 + ". (本题" + bean.getScore() + "分)");
        } else {
            textCount.setText(position + 1 + ".");
        }

        textType.setText(bean.getExamTemplateStyleName() + "");
        holder.getView(R.id.ll).setVisibility(View.GONE);
        //holder.getView(R.id.line).setVisibility(View.GONE);
        // holder.getView(R.id.tv_un_answer).setVisibility(View.GONE);
        //holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
        final TextView show = holder.getView(R.id.show);
        show.setTextColor(Color.parseColor("#4291FF"));
        show.setTextSize(20);
        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    show.setText("隐藏答案");//


                    show.setTextColor(Color.parseColor("#4291FF"));
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    show.setText("显示答案");
                    show.setTextColor(Color.parseColor("#4291FF"));
                }
            }
        });
        //答案部分

        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        ExamText examTextVo = bean.getExamTextVo();
        holder.getView(R.id.select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("2222222222222", "22222222");
                selectPostion = position;
                notifyDataSetChanged();

                onItemAddClick.onItemClick(bean.getId());

            }
        });
        if (selectPostion == position) {
            holder.getView(R.id.select).setBackgroundResource(R.drawable.normal_bg);//设置边框
        } else {
            holder.getView(R.id.select).setBackgroundResource(R.drawable.normal_bg_null);//设置边框
        }

        if (examTextVo == null) {
            return;
        }
        result.setHtmlText(examTextVo.getExamAnswer());
        analysis.setHtmlText(examTextVo.getExamAnalysis());

        textCount.setTextSize(20);
        textType.setTextSize(20);
        analysis.setTextSize(20);
        result.setTextSize(20);
        show.setTextSize(20);

    }

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindAllAnswer(final BaseViewHolder holder, final int position, final AnalyzeVo.DataBean bean) {
        TextView textCount = holder.getView(R.id.tv_count);
        holder.getView(R.id.ll).setVisibility(View.GONE);
        HtmlTextView htmlTextView = holder.getView(R.id.tv_exam);
        TextView textType = holder.getView(R.id.tv_type);
        RecyclerView examListView = holder.getView(R.id.lv);
        if (bean.getChildList().size() == 0) {
            textCount.setVisibility(View.GONE);
            htmlTextView.setVisibility(View.GONE);
            textType.setVisibility(View.GONE);
            examListView.setVisibility(View.GONE);
            return;
        }
        final List<AnalyzeVo.DataBean> list = new ArrayList();
        final String id = String.valueOf(bean.getId());


        textCount.setText(position + 1 + ". (本题" + bean.getScore() + "分)");
        textType.setText(bean.getExamTextVo().getTemplateStyleName());
        htmlTextView.setHtmlText(bean.getExamTextVo().getExamStem());

       /* list.addAll(bean.getChildList());
        final AnalyzeAllAdapter allAnserAdapter = new AnalyzeAllAdapter(list, context);
        examListView.setAdapter(allAnserAdapter);
        setListHeight(examListView);*/

        holder.getView(R.id.select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("2222222222222", "22222222");
                selectPostion = position;
                notifyDataSetChanged();

                onItemAddClick.onItemClick(bean.getId());

            }
        });

        if (selectPostion == position) {
            holder.getView(R.id.select).setBackgroundResource(R.drawable.normal_bg);//设置边框
        } else {
            holder.getView(R.id.select).setBackgroundResource(R.drawable.normal_bg_null);//设置边框
        }

        textCount.setTextSize(20);
        textType.setTextSize(20);
    }

    public static interface OnAddClickListener {
        // true add; false cancel
        public void onItemClick(int id); //传递boolean类型数据给activity
    }

    // add click callback
    OnAddClickListener onItemAddClick;

    public void setOnAddClickListener(OnAddClickListener onItemAddClick) {
        this.onItemAddClick = onItemAddClick;
    }

    //设置listview高度
    private void setListHeight(ListView lv) {
        ListAdapter la = lv.getAdapter();
        if (null == la) {
            return;
        }
        // calculate height of all items.
        int h = 0;
        final int cnt = la.getCount();
        for (int i = 0; i < cnt; i++) {
            View item = la.getView(i, null, lv);
            item.measure(0, 0);
            h += item.getMeasuredHeight();
        }
        // reset ListView height
        ViewGroup.LayoutParams lp = lv.getLayoutParams();
        lp.height = h + (lv.getDividerHeight() * (cnt - 1));
        lv.setLayoutParams(lp);
    }
}
