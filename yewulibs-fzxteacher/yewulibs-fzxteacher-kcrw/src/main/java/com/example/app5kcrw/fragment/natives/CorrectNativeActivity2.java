package com.example.app5kcrw.fragment.natives;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.app5libbase.R;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5kcrw.presenter.CorrectNativeActivityPresenter;
import com.example.app5kcrw.view.CorrectNativeActivityView;
import com.sdzn.fzx.student.libutils.util.Log;
import com.example.app5libbase.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CustomDialog;
import com.example.app5libbase.views.DelActivityDialog;
import com.example.app5libbase.views.graffiti.GraffitiOnTouchGestureListener;
import com.example.app5libbase.views.graffiti.GraffitiParams;
import com.example.app5libbase.views.graffiti.GraffitiTouchDetector;
import com.example.app5libbase.views.graffiti.GraffitiView;
import com.example.app5libbase.views.graffiti.IGraffitiListener;
import com.example.app5libbase.views.graffiti.core.IGraffiti;
import com.example.app5libbase.views.graffiti.core.IGraffitiTouchDetector;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ImgList;
import com.sdzn.fzx.teacher.vo.UndoCorrectDataVo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;


public class CorrectNativeActivity2 extends MBaseActivity<CorrectNativeActivityPresenter> implements CorrectNativeActivityView, View.OnClickListener {
    private FrameLayout flGraffiti;
    private TextView tvTitle;
    private ImageView ivClose;
    private LinearLayout ll;
    private TextView tvXuanZhuan;
    private TextView tvCancel;
    private TextView tvUndo;
    private TextView tvSave;
    private TextView tvNext;

    private IGraffiti mGraffiti;
    private GraffitiView mGraffitiView;

    private GraffitiParams mGraffitiParams;

    private GraffitiOnTouchGestureListener mTouchGestureListener;

    private CorrectDataVo correctData;
    private int vid;
    private String oldPath;
    private String restoreOldPath;
    private ProgressDialogManager mManager;

    private boolean undoBitmap;
    private ImgList imgList;
    private String id;
    private String seq;
    private int postion = 0;


    private String imgPath = "";
    private String LessonAnswerExamId = "";
    private String CorrectId;


    private String correct_Id = "";
    private String correct_seq = "";

    @Override
    public void initPresenter() {
        mPresenter = new CorrectNativeActivityPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //这个是跳转进来的界面
        setContentView(R.layout.activity_correct_native2);
        flGraffiti = (FrameLayout) findViewById(R.id.fl_graffiti);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        ivClose = (ImageView) findViewById(R.id.ivClose);
        ll = (LinearLayout) findViewById(R.id.ll);
        tvXuanZhuan = (TextView) findViewById(R.id.tvXuanZhuan);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvUndo = (TextView) findViewById(R.id.tvUndo);
        tvSave = (TextView) findViewById(R.id.tvSave);
        tvNext = (TextView) findViewById(R.id.tvNext);
        ivClose.setOnClickListener(this);
        tvXuanZhuan.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvUndo.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        tvNext.setOnClickListener(this);

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入CorrectNativeActivity2成功");
                    correctData = appLinkIntent.getParcelableExtra("correctData");
                    initData();
                }
            }
        }
    }


    @Override
    protected void initData() {

        id = correctData.getSrc();
        seq = correctData.getSeq();
        mPresenter.getImgList(correctData.getId() + "");
        vid = getIntent().getIntExtra("vid", 0);
        // oldPath = correctData.getSrc();
        tvTitle.setText(correctData.getSeq());
        // 涂鸦参数
        mGraffitiParams = new GraffitiParams();
        mGraffitiParams.mIsFullScreen = true;
        mGraffitiParams.mImagePath = correctData.getSrc();
        mGraffitiParams.mPaintUnitSize = GraffitiView.DEFAULT_SIZE;
        mGraffitiParams.mAmplifierScale = 0;
        flGraffiti.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                flGraffiti.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                // setGraffitiBg();
            }
        });
    }


    @Override
    protected void initView() {
        if (imgList.getData().size() == 0) {
            return;
        }
        for (int i = 0; i < imgList.getData().size(); i++) {
            if (String.valueOf(imgList.getData().get(i).getId()).equals(id) && String.valueOf(imgList.getData().get(i).getSeq()).equals(seq)) {
                postion = i;
                break;
            }
        }

        oldPath = imgList.getData().get(postion).getImgPath();
        restoreOldPath = imgList.getData().get(postion).getOldImgPath();

        imgPath = imgList.getData().get(postion).getImgPath();
        CorrectId = imgList.getData().get(postion).getId() + "";
        LessonAnswerExamId = imgList.getData().get(postion).getLessonAnswerExamId() + "";
        tvTitle.setText(imgList.getData().get(postion).getExamSeq() + "、" + imgList.getData().get(postion).getTemplateName());
        ll.setVisibility(vid == 1 ? View.VISIBLE : View.GONE);
        mManager = new ProgressDialogManager(this);
        mManager.showWaiteDialog();
        nextImg();
    }

    private void setGraffitiBg() {
        Glide.with(App2.get()).asBitmap().load(imgList.getData().get(postion).getImgPath()).listener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                mManager.cancelWaiteDialog();
                String msg = e == null ? "未知异常" : e.getMessage();
                ToastUtil.showShortlToast(msg);
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                initGraffiti(resource);
                mManager.cancelWaiteDialog();
                return false;
            }
        }).submit();
    }

    private void initGraffiti(Bitmap resource) {
        mGraffiti = mGraffitiView = new GraffitiView(this, resource, new IGraffitiListener() {
            @Override
            public void onReady() {
                float size = mGraffitiParams.mPaintUnitSize > 0 ? mGraffitiParams.mPaintUnitSize * mGraffiti.getSizeUnit() : 0;
                if (size <= 0) {
                    size = mGraffitiParams.mPaintPixelSize > 0 ? mGraffitiParams.mPaintPixelSize : mGraffiti.getSize();
                }
                // 设置初始值
                mGraffiti.setSize(size);
            }

            @Override
            public void isUndo(boolean isUndo) {
                tvCancel.setEnabled(isUndo);
            }
        });
        mTouchGestureListener = new GraffitiOnTouchGestureListener(mGraffitiView);
        IGraffitiTouchDetector detector = new GraffitiTouchDetector(this, mTouchGestureListener);
        mGraffitiView.setDefaultTouchDetector(detector);
        mGraffitiView.setIsJustDrawOriginal(vid == 0);
        mGraffiti.setIsDrawableOutside(mGraffitiParams.mIsDrawableOutside);
        flGraffiti.addView(mGraffitiView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mGraffiti.setGraffitiMinScale(mGraffitiParams.mMinScale);
        mGraffiti.setGraffitiMaxScale(mGraffitiParams.mMaxScale);
    }


    private void saveAndClose() {
        if (vid == 0) {
            finish();
            return;
        }
        if (undoBitmap || (mGraffiti != null && mGraffiti.isUndo())) {
            new DelActivityDialog()
                    .creatDialog(this)
                    .buildText("关闭前是否保存当前批改结果？")
                    .buildConfirmText("保存")
                    .buildCancelText("不保存")
                    .buildListener(new DelActivityDialog.OptionListener() {
                        @Override
                        public void onConfirmed() {
                            buttonEnable = false;
                            imgPath = imgList.getData().get(postion).getImgPath();
                            CorrectId = imgList.getData().get(postion).getId() + "";

                            LessonAnswerExamId = imgList.getData().get(postion).getLessonAnswerExamId() + "";
                            mPresenter.saveBitmap(mGraffitiView, false);
                        }

                        @Override
                        public void onCancel() {
                            finish();
                        }
                    }).show();
        } else {
            finish();
        }
    }

    private boolean buttonEnable = true;

    @Override
    public void saveBitmapSuccess(String b64Bitmap, boolean b) {
        //mPresenter.saveCorrectPic(oldPath, b64Bitmap, String.valueOf(correctData.getId()), correctData.getEid(), b);
        mPresenter.saveCorrectPic(imgPath, b64Bitmap, CorrectId, LessonAnswerExamId, b);
    }

    @Override
    public void saveFailed() {
        buttonEnable = true;
        ToastUtil.showShortlToast("保存失败");
    }

    @Override
    public void getImgList(ImgList img) {
        imgList = img;
        initView();
    }

    @Override
    public void getMarkingImgList(ImgList img) {
        /*imgList = img;
        initView();*/
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            saveAndClose();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void saveSuccess(String path, boolean cloce) {
        Log.e("保存后的照片", "保存后的照片" + path);
        ArrayList datas = new ArrayList<String>();
        for (int i = 0; i < imgList.getData().size(); i++) {
            if (String.valueOf(imgList.getData().get(i).getId()).equals(correct_Id) && String.valueOf(imgList.getData().get(i).getSeq()).equals(correct_seq)) {
                imgList.getData().get(i).setImgPath(path);
                break;
            }
        }
        imgList.getData().get(postion).setImgPath(path);
        if (cloce) {
            nextImg();
        } else {
            //改这里，筛选出您想要的图片
          /*  for (ImgList.DataBean dataBean : imgList.getData()) {
                datas.add(dataBean.getImgPath());
            }
            Intent intent = new Intent();
            intent.putExtra("imgs", datas);
            intent.putExtra("postion",postion);
            setResult(Activity.RESULT_OK, intent);*/
            finish();
        }

    }

    @Override
    public void networkError(String msg, boolean close) {
        buttonEnable = true;
        ToastUtil.showShortlToast(msg);
        if (close) {
            finish();
        }
    }

    @Override
    public void undoSuccess(UndoCorrectDataVo undoCorrectDataVo) {
        undoBitmap = true;
        mGraffiti.clear();
        correctData.setSrc(undoCorrectDataVo.getData());
        mManager = new ProgressDialogManager(this);
        mManager.showWaiteDialog();
        Glide.with(App2.get()).asBitmap().load(undoCorrectDataVo.getData()).listener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                mManager.cancelWaiteDialog();
                String msg = e == null ? "未知异常" : e.getMessage();
                ToastUtil.showShortlToast(msg);
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                if (mGraffitiView != null) {
                    flGraffiti.removeView(mGraffitiView);
                }
                initGraffiti(resource);
                mManager.cancelWaiteDialog();
                return false;
            }
        }).submit();
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.ivClose) {// 关闭
            saveAndClose();
        } else if (viewId == R.id.tvCancel) {// 撤销
            mGraffiti.undo();
        } else if (viewId == R.id.tvUndo) {// 还原，访问接口，拿到原始图片，重新加载
            new CustomDialog.Builder(this).setMessage("确定把批改结果还原成最初状态？")
                    .setPositive("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Log.e("oldPath", "oldPath" + oldPath);
                            Log.e("oldPath", "id" + imgList.getData().get(postion).getId());
                            Log.e("oldPath", "seq" + imgList.getData().get(postion).getSeq());
                            mPresenter.undoCorrectPic(oldPath, String.valueOf(imgList.getData().get(postion).getId()), String.valueOf(imgList.getData().get(postion).getSeq()));
                        }
                    }).setNegative("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        } else if (viewId == R.id.tvSave) {
            buttonEnable = false;
            Log.e("第一张", "第一张" + postion);
            if (postion <= 0) {
                ToastUtil.showLonglToast("第一张");
                return;
            } else {

                /*    imgPath = imgList.getData().get(postion).getImgPath();
                    CorrectId = imgList.getData().get(postion).getId() + "";
                    LessonAnswerExamId = imgList.getData().get(postion).getLessonAnswerExamId() + "";*/
                //nextImg();


                imgPath = imgList.getData().get(postion).getImgPath();
                CorrectId = imgList.getData().get(postion).getId() + "";
                LessonAnswerExamId = imgList.getData().get(postion).getLessonAnswerExamId() + "";
                correct_Id = imgList.getData().get(postion).getId() + "";
                correct_seq = imgList.getData().get(postion).getSeq() + "";


                mPresenter.saveBitmap(mGraffitiView, true);
            }
            postion--;
//                }
        } else if (viewId == R.id.tvXuanZhuan) {
            mGraffiti.setGraffitiRotation(mGraffiti.getGraffitiRotation() + 90);
        } else if (viewId == R.id.tvNext) {
            Log.e("最后一张了", "最后一张了" + postion);

            if (postion >= imgList.getData().size() - 1) {
                ToastUtil.showLonglToast("最后一张了，请点击关闭按钮");
                imgPath = imgList.getData().get(postion).getImgPath();
                return;
            } else {
                imgPath = imgList.getData().get(postion).getImgPath();
                CorrectId = imgList.getData().get(postion).getId() + "";
                LessonAnswerExamId = imgList.getData().get(postion).getLessonAnswerExamId() + "";
                correct_Id = imgList.getData().get(postion).getId() + "";
                correct_seq = imgList.getData().get(postion).getSeq() + "";
                mPresenter.saveBitmap(mGraffitiView, true);
                // nextImg();
            }
            postion++;
        }
    }

    void nextImg() {
        flGraffiti.removeAllViews();
        setGraffitiBg();
        tvTitle.setText(imgList.getData().get(postion).getExamSeq() + "、" + imgList.getData().get(postion).getTemplateName());
    }


}
