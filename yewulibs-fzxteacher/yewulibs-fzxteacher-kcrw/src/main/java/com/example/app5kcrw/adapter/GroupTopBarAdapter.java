package com.example.app5kcrw.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.teacher.vo.GroupStudent;

import java.util.ArrayList;
import java.util.List;

/**
 * 分组。。。
 */
public class GroupTopBarAdapter extends BaseRcvAdapter<GroupStudent.DataBean> {
    public int current_index = 2;
    private Context mContext;

    public GroupTopBarAdapter(Context context) {
        super(context, new ArrayList<GroupStudent.DataBean>());
        mContext = context;
    }

    public void clear() {
        mList.clear();
    }

    public void add(List<GroupStudent.DataBean> list) {
        mList.addAll(list);
    }

    public GroupStudent.DataBean get(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = BaseViewHolder.get(context, null, parent, R.layout.group_student_details_item);
        setListener(parent, holder, viewType);
        return holder;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, GroupStudent.DataBean bean) {
        if (current_index == position) {
            holder.getView(R.id.exam_name).setBackground(mContext.getResources().getDrawable(R.drawable.shape_progressbar_group));
        } else {
            holder.getView(R.id.exam_name).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        TextView tv = holder.getView(R.id.exam_name);
        if (position > 1) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(bean.getGroupName() + "");
        } else {
            tv.setVisibility(View.GONE);
        }

    }
}
