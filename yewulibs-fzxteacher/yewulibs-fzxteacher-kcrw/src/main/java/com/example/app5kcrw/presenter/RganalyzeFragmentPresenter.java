package com.example.app5kcrw.presenter;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.RganalyzeFragmentView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.RgAnalyze;
import com.sdzn.fzx.teacher.vo.RgDetailsVo;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class RganalyzeFragmentPresenter extends BasePresenter<RganalyzeFragmentView, BaseActivity> {

    public void getVideoList(String lessonTaskId, String parentId, String type) {
        Network.createTokenService(NetWorkService.GetRgAnalyzeListService.class)
                .AnalyzeStatisc(lessonTaskId, parentId, type)
                .map(new StatusFunc<RgAnalyze>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RgAnalyze>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(RgAnalyze o) {
                        mView.setRgAnalyze(o);

                    }
                });
    }


    public void getRgDetail(String lessonTaskDetailsId) {
        Network.createTokenService(NetWorkService.GetRgDetailAnalyzeListService.class)
                .AnalyzeStatisc(lessonTaskDetailsId)
                .map(new StatusFunc<RgDetailsVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<RgDetailsVo>(new SubscriberListener<RgDetailsVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(RgDetailsVo o) {
                        // mView.setRgAnalyze(o);
                        mView.setRgDetailAnalyze(o);

                    }
                }, mActivity,true, false, false, ""));
    }
}
