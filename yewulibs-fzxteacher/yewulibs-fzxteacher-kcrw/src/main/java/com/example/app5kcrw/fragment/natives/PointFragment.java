package com.example.app5kcrw.fragment.natives;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.app5libbase.R;
import com.example.app5libbase.holder.IconTreeItemHolder;
import com.example.app5libbase.holder.SelectableHeaderHolder;
import com.example.app5libbase.baseui.adapter.PointAbilityAdapter;
import com.example.app5libbase.baseui.adapter.PointAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.PointFragmentPresenter;
import com.example.app5kcrw.view.PointFragmentView;
import com.example.app5libbase.views.exam.ExamListView;
import com.sdzn.fzx.teacher.vo.PointVo;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 知识点分析
 */
public class PointFragment extends MBaseFragment<PointFragmentPresenter> implements PointFragmentView {
    private AndroidTreeView tView;
    private RelativeLayout rlPoint;
    private RelativeLayout containerView;
    private RelativeLayout rlNengli;
    private ExamListView listNengli;
    private RelativeLayout rlFangfa;
    private ExamListView listFangfa;


    private Bundle InstanceState = null;
    private String teacherExamId;

    private List<PointVo.PointBean> newPointList = new ArrayList<>();
    private String amount, errors, accuracy;

    public PointFragment() {
        // Required empty public constructor
    }

    public static PointFragment newInstance(Bundle bundle) {
        PointFragment fragment = new PointFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_point, container, false);
        rlPoint = (RelativeLayout) view.findViewById(R.id.rl_point);
        containerView = (RelativeLayout) view.findViewById(R.id.container);
        rlNengli = (RelativeLayout) view.findViewById(R.id.rl_nengli);
        listNengli = (ExamListView) view.findViewById(R.id.list_nengli);
        rlFangfa = (RelativeLayout) view.findViewById(R.id.rl_fangfa);
        listFangfa = (ExamListView) view.findViewById(R.id.list_fangfa);


        InstanceState = savedInstanceState;
        teacherExamId = getArguments().getString("teacherExamId");
        mPresenter.getPointList(teacherExamId);
        return view;
    }


    @Override
    public void initPresenter() {
        mPresenter = new PointFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void setPointVo(PointVo bean) {
        accuracy = bean.getErrorRate() == 0 ? "0.0%" : bean.getErrorRate() + "%";
        if (bean.getAbility() != null && bean.getAbility().size() > 0) {
            listNengli.setAdapter(new PointAbilityAdapter(bean.getAbility(), getActivity(), accuracy));
        } else {
            rlNengli.setVisibility(View.GONE);
        }
        if (bean.getMethod() != null && bean.getMethod().size() > 0) {
            listFangfa.setAdapter(new PointAdapter(bean.getMethod(), getActivity(), accuracy));
        } else {
            rlFangfa.setVisibility(View.GONE);
        }
        if (bean.getPoint() == null || bean.getPoint().size() < 1) {
            rlPoint.setVisibility(View.GONE);
        }
        newPointList.clear();
        newPointList = builTree(bean.getPoint());

        Collections.sort(newPointList);
        TreeNode node = TreeNode.root();
        createTreeNodes(node, newPointList);
        AndroidTreeView tView = new AndroidTreeView(getActivity(), node);
        tView.setDefaultAnimation(true);
        //tView.setUse2dScroll(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom, false);

        containerView.addView(tView.getView(R.style.TreeNodeStyle));
        tView.expandAll();

    }

    @Override
    public void onError() {
        rlPoint.setVisibility(View.GONE);
        rlNengli.setVisibility(View.GONE);
        rlFangfa.setVisibility(View.GONE);
    }

    /**
     * 获取根节点
     */
    private List<PointVo.PointBean> getList(List<PointVo.PointBean> pointList) {
        List<Long> utilsPoint = new ArrayList<>();
        List<Long> parentPoint = new ArrayList<>();
        List<PointVo.PointBean> parentPointList = new ArrayList<>();

        for (int i = 0; i < pointList.size() - 1; i++) {
            //内存循环，是每一轮中进行的两两比较
            for (int j = 0; j < pointList.size() - 1; j++) {
                if (pointList.get(i).getPointParentId() == pointList.get(j + 1).getPointId()) {
                    utilsPoint.add(pointList.get(i).getPointParentId());
                }
            }
        }
        for (PointVo.PointBean point : pointList) {
            if (!utilsPoint.contains(point.getPointParentId())) {
                parentPoint.add(point.getPointParentId());
                point.setLast(false);
                parentPointList.add(point);
            }
        }

        return parentPointList;
    }

    /**
     * 建立树形结构
     */

    private List<PointVo.PointBean> builTree(List<PointVo.PointBean> pointList) {
        List<PointVo.PointBean> tree = new ArrayList<>();
        for (PointVo.PointBean point : getList(pointList)) {
            point.setLast(true);
            point = buildChilTree(point, pointList);
            tree.add(point);
        }
        return tree;
    }

    /**
     * 建立子树型结构
     */
    private PointVo.PointBean buildChilTree(PointVo.PointBean point, List<PointVo.PointBean> pointList) {
        List<PointVo.PointBean> child = new ArrayList<>();
        for (PointVo.PointBean myPoint : pointList) {
            if (myPoint.getPointParentId() == point.getPointId()) {
                point.setLast(false);
                myPoint.setLast(true);
                child.add(buildChilTree(myPoint, pointList));
            }
        }
        point.setChildList(child);

        return point;
    }


    private void createTreeNodes(TreeNode treeNode, List<PointVo.PointBean> points) {
        for (PointVo.PointBean point : points) {
            TreeNode node = initTreeNode(point.getPointName(), point.getPointAccuracy(), point.isLast());
            treeNode.addChild(node);
            if (point != null && !point.getChildList().isEmpty()) {
                createTreeNodes(node, point.getChildList(), point.getPointId());
            }
        }
    }

    private void createTreeNodes(TreeNode treeNode, List<PointVo.PointBean> points, long parentId) {
        for (PointVo.PointBean point : points) {
            if (point.getPointParentId() == parentId) {
                TreeNode node = initTreeNode(point.getPointName(), point.getPointAccuracy(), point.isLast());
                treeNode.addChild(node);
                createTreeNodes(node, points, point.getPointId());
            }
        }
    }


    private TreeNode initTreeNode(String name, double score, boolean isLast) {
        //分数为0时不显示
        return new TreeNode(new IconTreeItemHolder.IconTreeItem(name, score == 0 ? "0.0%" : score + "%", isLast, amount, errors, accuracy))
                .setViewHolder(new SelectableHeaderHolder(getActivity()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
