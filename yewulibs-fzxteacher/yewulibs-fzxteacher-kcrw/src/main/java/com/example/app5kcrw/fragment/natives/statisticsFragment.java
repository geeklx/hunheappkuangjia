package com.example.app5kcrw.fragment.natives;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5kcrw.adapter.GroupTopBarAdapter;
import com.example.app5libbase.baseui.adapter.statisticeAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.statisticsFragmentPresenter;
import com.example.app5kcrw.view.statisticsFragmentView;
import com.sdzn.fzx.student.libutils.util.Log;
import com.example.app5libbase.views.ChartLegendView;
import com.sdzn.fzx.teacher.vo.GroupStudent;
import com.sdzn.fzx.teacher.vo.GroupStudentDetils;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StudentList;
import com.sdzn.fzx.student.libutils.app.App2;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 作答统计
 */
public class statisticsFragment extends MBaseFragment<statisticsFragmentPresenter> implements statisticsFragmentView {


    private RelativeLayout rlTop;
    private ImageView ivLeft;
    private ImageView ivRight;
    private RecyclerView rvAnswers;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private LinearLayout statisticsCount;
    private TextView num;
    private ImageView photo;
    private TextView state;
    private TextView correctState;
    private TextView score1;
    private TextView time;
    private ListView listview;
    private PieChart mPieChart;
    private TextView tvWCount;
    private LinearLayout ll;
    private ChartLegendView clv1;
    private ChartLegendView clv3;
    private ChartLegendView clv2;
    private TextView score;


    private Bundle bundle;

    private String classId;
    private String id;
    private String name;
    private GroupTopBarAdapter mTopAdapter;
    private boolean groupFlag = false;
    private statisticeAdapter statisticeAdapter;

    public statisticsFragment() {
        // Required empty public constructor
    }

    private List<StudentList.DataBean> mlist = new ArrayList<>();

    public static statisticsFragment newInstance(Bundle bundle) {
        statisticsFragment fragment = new statisticsFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_statistics2, container, false);
        rlTop = (RelativeLayout) view.findViewById(R.id.rl_top);
        ivLeft = (ImageView) view.findViewById(R.id.iv_left);
        ivRight = (ImageView) view.findViewById(R.id.iv_right);
        rvAnswers = (RecyclerView) view.findViewById(R.id.rv_answers);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        statisticsCount = (LinearLayout) view.findViewById(R.id.statistics_count);
        num = (TextView) view.findViewById(R.id.num);
        photo = (ImageView) view.findViewById(R.id.photo);
        state = (TextView) view.findViewById(R.id.state);
        correctState = (TextView) view.findViewById(R.id.correct_state);
        score1 = (TextView) view.findViewById(R.id.score_1);
        time = (TextView) view.findViewById(R.id.time);
        listview = (ListView) view.findViewById(R.id.listview);
        mPieChart = (PieChart) view.findViewById(R.id.piechart);
        tvWCount = (TextView) view.findViewById(R.id.tvWCount);
        ll = (LinearLayout) view.findViewById(R.id.ll);
        clv1 = (ChartLegendView) view.findViewById(R.id.clv1);
        clv3 = (ChartLegendView) view.findViewById(R.id.clv3);
        clv2 = (ChartLegendView) view.findViewById(R.id.clv2);
        score = (TextView) view.findViewById(R.id.score);


        mTopAdapter = new GroupTopBarAdapter(App2.get());

        rvAnswers.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.HORIZONTAL, false));
        rvAnswers.setAdapter(mTopAdapter);

        bundle = getArguments();
        classId = bundle.getString("classId");
        id = bundle.getString("id");
        name = bundle.getString("name");
        initData();
        return view;
    }

   /* @Override
    public void onResume() {
        super.onResume();
        initData();
    }*/

    private void initData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("scoreOrder", "0");
        param.put("lessonTaskId", id);
        param.put("classId", classId);
        if (groupFlag == true) {
            mPresenter.getGroupStudentList(classId);
            rlTop.setVisibility(View.VISIBLE);
        } else {
            mPresenter.getStudentList(param);
            mPresenter.getStudentStatistics(id, "");
            rlTop.setVisibility(View.GONE);
        }
    }

    @Override
    public void initPresenter() {
        mPresenter = new statisticsFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    private statisticeAdapter adapter;

    @Override
    public void StudentList(StudentList studentList) {
        if (studentList.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
            statisticsCount.setVisibility(View.GONE);
        } else {
            llTaskEmpty.setVisibility(View.GONE);
            statisticsCount.setVisibility(View.VISIBLE);
            mlist.clear();
            mlist.addAll(studentList.getData());
            adapter = new statisticeAdapter(mlist, getActivity());
            listview.setAdapter(adapter);
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long idl) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.StudentDetailsActivity");
                intent.putExtra("id", mlist.get(position).getId());
                intent.putExtra("lessonTaskId", id + "");
                intent.putExtra("userStudentName", mlist.get(position).getUserStudentName());
                intent.putExtra("useTime", mlist.get(position).getUseTime());
                intent.putExtra("scoreTotal", mlist.get(position).getScoreTotal());
                intent.putExtra("name", name);
                startActivity(intent);
            }
        });
    }

    /*查看整体作答列表*/
    @Override
    public void StudentStatics(StatisticsVo statisticsVo) {
        mPieChart.clearChart();
        StatisticsVo.DataBean dataBean = statisticsVo.getData();

        if (dataBean.getStatus() == 0) {
            clv3.setSubject("作答中");
            clv2.setSubject("未开始");
            clv3.setCount((int) dataBean.getReplenishPre());
            clv2.setCount(dataBean.getUnAnswer());

            mPieChart.addPieSlice(new PieModel("", (int) dataBean.getReplenishPre(), getResources().getColor(R.color.fragment_main_chart_yellow)));


            mPieChart.addPieSlice(new PieModel("", dataBean.getUnAnswer(), getResources().getColor(R.color.fragment_main_chart_violet)));

        } else {
            clv3.setSubject("未完成");
            clv2.setSubject("补交");
            clv3.setCount(dataBean.getUnAnswer());
            clv2.setCount((int) dataBean.getReplenish());


            mPieChart.addPieSlice(new PieModel("", dataBean.getUnAnswer(), getResources().getColor(R.color.fragment_main_chart_yellow)));


            mPieChart.addPieSlice(new PieModel("", dataBean.getReplenishPre(), getResources().getColor(R.color.fragment_main_chart_violet)));


        }
        mPieChart.addPieSlice(new PieModel("", dataBean.getComplete(), getResources().getColor(R.color.fragment_main_chart_green)));


        clv1.setCount(dataBean.getComplete());

        //  mPieChart.addPieSlice(new PieModel("", dataBean.getUnAnswer(), getResources().getColor(R.color.fragment_main_chart_yellow)));


        //clv3.setCount(dataBean.getUnAnswer());

        // mPieChart.addPieSlice(new PieModel("", dataBean.getReplenishPre(), getResources().getColor(R.color.fragment_main_chart_violet)));


        //clv2.setCount((int) dataBean.getReplenishPre());


        tvWCount.setText("共" + dataBean.getUnAnswer() + "人");


        if (groupFlag) {
            if (dataBean.getScoreAvg() < 0 || dataBean.getScope() < 0) {
                score.setText("小组平均分：" + "----" + "        总分：" + "----");
            } else {
                score.setText("小组平均分：" + dataBean.getScoreAvg() + "        总分：" + dataBean.getScope());
            }
        } else {
            if (dataBean.getScoreAvg() < 0 || dataBean.getScope() < 0) {
                score.setText("班级平均分：" + "----" + "        总分：" + "----");
            } else {
                score.setText("班级平均分：" + dataBean.getScoreAvg() + "        总分：" + dataBean.getScope());
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public void allSee() {
        Log.e("整体查看", "整体查看");
        groupFlag = false;
        initData();
    }

    public void groupSee() {
        Log.e("分组查看", "分组查看");
        groupFlag = true;
        initData();
    }

    public void updateData() {
        Log.e("刷新数据", "刷新数据");
        if (rlTop.getVisibility() == View.VISIBLE) {
            mPresenter.getGroupStudentList(classId);
        } else {
            initData();
        }


    }

    private int grouppostion = 2;

    /**
     * 分组查看
     *
     * @param groupStudent
     */
    @Override
    public void GroupStudentStatics(final GroupStudent groupStudent) {
        mTopAdapter.clear();
        if (groupStudent.getData().size() > 2) {
            mTopAdapter.add(groupStudent.getData());
        }
        mTopAdapter.notifyDataSetChanged();
        final Map<String, Object> param = new HashMap<>();
        param.put("lessonTaskId", id);
        param.put("scoreOrder", 0);
        param.put("classId", classId);
        //param.put("classGroupId", id);
        param.put("classGroupId", groupStudent.getData().get(grouppostion).getId());
        mPresenter.getStudentStatistics(id, groupStudent.getData().get(grouppostion).getId() + "");
        mPresenter.getGroupDetilsListService(param);
        mTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                grouppostion = position;
                mTopAdapter.current_index = position;
                param.put("classGroupId", groupStudent.getData().get(position).getId());
                mPresenter.getGroupDetilsListService(param);
                mTopAdapter.notifyDataSetChanged();
            }
        });
    }

    /*分组列表*/
    @Override
    public void GroupDetilsStatics(GroupStudentDetils groupStudentDetils) {
        if (groupStudentDetils.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
            statisticsCount.setVisibility(View.GONE);
        } else {
            llTaskEmpty.setVisibility(View.GONE);
            statisticsCount.setVisibility(View.VISIBLE);
            mlist.clear();
            if (groupStudentDetils.getData().size() < 1 || groupStudentDetils.getData().get(0).getStudents().size() < 1) {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            } else {
                for (int i = 0; i < groupStudentDetils.getData().size(); i++) {
                    mlist.addAll(groupStudentDetils.getData().get(i).getStudents());
                }
                listview.setAdapter(new statisticeAdapter(mlist, getActivity()));
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                       /* if (position == 0) {
                            return;
                        }*/
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.StudentDetailsActivity");
                        intent.putExtra("id", mlist.get(position).getId());
                        intent.putExtra("lessonTaskId", id + "");
                        intent.putExtra("userStudentName", mlist.get(position).getUserStudentName());
                        intent.putExtra("useTime", mlist.get(position).getUseTime());
                        intent.putExtra("scoreTotal", mlist.get(position).getScoreTotal());
                        intent.putExtra("name", name);
                        startActivity(intent);
                    }
                });
            }
        }
    }
}
