package com.example.app5kcrw.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.TeachActivityAdapter;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.base.BaseView;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.baseui.presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程任务
 * * @author wangchunxiao
 *
 * @date 2018/1/31
 */
public class TaskFragment extends MBaseFragment<BasePresenter> implements BaseView {

    public static final int SYNC_TASK = 1;
    public static final int SYNC_REVIEW = 2;
    public static final int[] TASK_TYPES = {SYNC_TASK, SYNC_REVIEW};
    private RadioGroup radioGroup;
    private RadioButton leftRb;
    private RadioButton rightRb;
    private ViewPager viewpager;
    private TeachActivityAdapter mAdapter;

    public static TaskFragment newInstance(Bundle bundle) {
        TaskFragment fragment = new TaskFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new MainPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task, container, false);
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);
        leftRb = (RadioButton) rootView.findViewById(R.id.left_rb);
        rightRb = (RadioButton) rootView.findViewById(R.id.right_rb);
        viewpager = (ViewPager) rootView.findViewById(R.id.viewpager);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new TeachActivityAdapter(getChildFragmentManager());
        viewpager.setAdapter(mAdapter);
        List<Fragment> list = new ArrayList<>();
        SyncFragment syncFragment = new SyncFragment();
        ReviewFragment reviewFragment = new ReviewFragment();

        list.add(syncFragment);
        //list.add(reviewFragment);
        mAdapter.setDatas(list);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0)
                    leftRb.setChecked(true);
                else if (position == 1)
                    rightRb.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.left_rb) {
                    viewpager.setCurrentItem(0);
                } else if (i == R.id.right_rb) {
                    viewpager.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
