package com.example.app5kcrw.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.teacher.vo.StudentTaskExamListVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 按题批改试题页面顶部序号adapter
 *
 * @author Reisen at 2018-08-29
 */
public class MarkingTopBarAdapter extends BaseRcvAdapter<StudentTaskExamListVo.DataBean> {
    public int current_index = 0;

    public MarkingTopBarAdapter(Context context) {
        super(context, new ArrayList<StudentTaskExamListVo.DataBean>());
    }

    public void clear() {
        mList.clear();
    }

    public void add(List<StudentTaskExamListVo.DataBean> list) {
        mList.addAll(list);
    }

    public StudentTaskExamListVo.DataBean get(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getExamSeq();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return BaseViewHolder.get(context, null, parent, R.layout.item_gone);
            default:
                BaseViewHolder holder = BaseViewHolder.get(context, null, parent, R.layout.item_answer_top);
                setListener(parent, holder, viewType);
                return holder;
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, StudentTaskExamListVo.DataBean bean) {
        if (bean.getExamSeq() == 0) {
            return;
        }
        if (current_index == position) {
            holder.getView(R.id.tv_index).setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_10));
        } else {
            holder.getView(R.id.tv_index).setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
            //holder.getView(R.id.tv_index).setBackgroundColor(context.getResources().getColor(R.color.color_509bff));
        }
        TextView tv = holder.getView(R.id.tv_index);
        tv.setText(bean.getExamSeq() + "");
    }
}
