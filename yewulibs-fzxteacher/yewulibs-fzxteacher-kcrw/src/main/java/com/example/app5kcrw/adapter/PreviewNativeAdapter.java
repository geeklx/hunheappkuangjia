package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.google.gson.Gson;
import com.example.app5libbase.R;
import com.example.app5libbase.app.SchoolBoxWatcher;
import com.example.app5libbase.baseui.activity.preview.DocViewActivity;
import com.example.app5libbase.baseui.activity.preview.ImageDisplayActivity;
import com.example.app5libbase.baseui.activity.preview.PDFActivity;
import com.example.app5libbase.baseui.activity.preview.PlayerActivity;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.ExamVo;
import com.sdzn.fzx.teacher.vo.ResourceVo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/13
 * 修改单号：
 * 修改内容:
 * on 2018/12/13
 */

public class PreviewNativeAdapter extends BaseRcvAdapter<ExamVo.DataBean> {

    private Activity mActivity = null;

    public PreviewNativeAdapter(Context context, List<ExamVo.DataBean> mList, Activity activity) {
        super(context, mList);
        mActivity = activity;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        ExamVo.DataBean bean = mList.get(position);
        return bean.getType() * 10000 + bean.getExamTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 20000://资源
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_resource);
            case 30000://文本
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_text);
            case 10001://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 10002://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 10003://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 10004://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_jianda_exam);
            case 10006://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_text);
            case 10014://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 10016://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer_used);//item_error_exam_short_answer
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, ExamVo.DataBean bean) {
        if (bean.getType() == 2) {//资源
            bindResource(holder, position, bean);
            return;
        }
        if (bean.getType() == 3) {//文字
            bindText(holder, position, bean);
            return;
        }
        switch (bean.getExamTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }

    /**
     * 文本
     */
    private void bindText(BaseViewHolder holder, int position, ExamVo.DataBean bean) {
        HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getResourceText());
    }

    /**
     * 资源
     */
    private void bindResource(BaseViewHolder holder, int position, final ExamVo.DataBean bean) {
        ImageView iv_collect = holder.getView(R.id.iv_collect);
        iv_collect.setVisibility(View.INVISIBLE);
//        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        holder.setText(R.id.tv_name, bean.getResourceName());
        ImageView iv_type = holder.getView(R.id.iv_type);
//        final ResourceVo.ResourceTextVo resourceBean = bean.getResourceVoBean();
        //1文档类2演示稿3视频类4图片类5音频类
        switch (bean.getResourceType()) {
            case 1:
                if (".txt".equalsIgnoreCase(bean.getResourceVoBean().getResourceSuffix())) {
                    iv_type.setImageResource(R.drawable.txt);
                } else {
                    iv_type.setImageResource(R.drawable.doc);
                }
                break;
            case 2:
                iv_type.setImageResource(R.drawable.ppt);
                break;
            case 3:
                iv_type.setImageResource(R.drawable.video);
                break;
            case 4:
                iv_type.setImageResource(R.drawable.image);
                break;
            case 5:
                iv_type.setImageResource(R.drawable.music);
                break;
            case 6:
                iv_type.setImageResource(R.drawable.pdf);
                break;
            default:
                iv_type.setImageResource(R.drawable.ppt);
                break;
        }
        holder.getView(R.id.ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (mListener != null) {
                    mListener.showResource(bean);
                }*/

                final ResourceVo.ResourceTextVo resourceTextVo = new Gson().fromJson(bean.getResourceText(), ResourceVo.ResourceTextVo.class);

                switch (resourceTextVo.getResourceType()) {
                    case 1:
                    case 2:
                        showSelectOpenDialog(new OpenListener() {
                            @Override
                            public void openByApp() {
                                Intent intentDoc = new Intent(mActivity, PDFActivity.class);
                                intentDoc.putExtra("path", resourceTextVo.getConvertPath());
                                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                                mActivity.startActivity(intentDoc);
                            }

                            @Override
                            public void openByOther() {
                                Intent intentDoc = new Intent(mActivity, DocViewActivity.class);
                                intentDoc.putExtra("path", resourceTextVo.getResourcePath());
                                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                                switch (resourceTextVo.getResourceType()) {
                                    case 1:
                                        if ("txt".equals(resourceTextVo.getResourceSuffix())) {
                                            intentDoc.putExtra("type", "txt");
                                        } else {
                                            intentDoc.putExtra("type", "doc");
                                        }
                                        break;
                                    case 2:
                                        intentDoc.putExtra("type", "ppt");
                                        break;
                                }
                                mActivity.startActivity(intentDoc);
                            }
                        });
                        break;
                    case 3:
                        SchoolBoxWatcher.getFileUrlAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), new SchoolBoxWatcher.SchoolBosUrl() {
                            @Override
                            public void bosUrl(String url) {
                                Intent intentVideo = new Intent(mActivity, PlayerActivity.class);
                                intentVideo.putExtra("videoUrl", url);
                                intentVideo.putExtra("title", resourceTextVo.getResourceName());
                                mActivity.startActivity(intentVideo);
                            }
                        });

                        break;
                    case 4:
                        SchoolBoxWatcher.getFileUrlAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), new SchoolBoxWatcher.SchoolBosUrl() {
                            @Override
                            public void bosUrl(String url) {
                                Intent intentImage = new Intent(mActivity, ImageDisplayActivity.class);
                                intentImage.putExtra("photoUrl", url);
                                mActivity.startActivity(intentImage);
                            }
                        });


                        break;
                    case 5:

                        SchoolBoxWatcher.getFileUrlAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), new SchoolBoxWatcher.SchoolBosUrl() {
                            @Override
                            public void bosUrl(String url) {
                                Intent intentRadio = new Intent(mActivity, PlayerActivity.class);
                                intentRadio.putExtra("radioUrl", url);
                                intentRadio.putExtra("title", resourceTextVo.getResourceName());
                                mActivity.startActivity(intentRadio);
                            }
                        });

                        break;
                    case 6:
                        Intent intentDoc = new Intent(mActivity, PDFActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getConvertPath());
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        mActivity.startActivity(intentDoc);
                        break;
                }
            }
        });
//        iv_collect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mListener != null) {
//                    mListener.changeCollect(bean);
//                }
//            }
//        });
    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final ExamVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        ExamText examTextVo = bean.getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(examTextVo.getExamStem());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            // final List<AnswerListBean.ExamOptionBean> examList = bean.getEx;
            List<ExamText.ExamOptionsBean> examOptions = bean.getExamTextVo().getExamOptions();
            if (examOptions == null || examOptions.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examOptions);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examOptions.get(0).getContent()));
            } else {//多选
                Collections.sort(examOptions);
                for (ExamText.ExamOptionsBean optionBean : examOptions) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getContent())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }


    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final ExamVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());

    }


    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final ExamVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        holder.getView(R.id.line).setVisibility(View.GONE);
        holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
    }


    /**
     * 完型填空
     */
    private void bindFillExam(final BaseViewHolder holder, final int position, final ExamVo.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());


    }


    private void bindExamTitle(BaseViewHolder holder, int position, ExamVo.DataBean bean) {
        final String id = String.valueOf(bean.getId());
        TextView textCount = holder.getView(R.id.tv_count);
        TextView textType = holder.getView(R.id.tv_type);

        if (bean.getScore() > -1) {
            if (bean.getExamTemplateId() == 6) {
                textCount.setText(bean.getExamSeq() + ". (本题" + (bean.getScore() * bean.getExamEmptyCount()) + "分)");
            } else {
                textCount.setText(bean.getExamSeq() + ". (本题" + bean.getScore() + "分)");
            }
        } else {
            textCount.setText(bean.getExamSeq() + ".");
        }
        textType.setText(bean.getExamTemplateStyleName() + "");
        holder.getView(R.id.error_ly).setVisibility(View.GONE);
        holder.getView(R.id.rl_result).setVisibility(View.GONE);


    }

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindAllAnswer(final BaseViewHolder holder, final int position, final ExamVo.DataBean bean) {

        holder.getView(R.id.error_ly).setVisibility(View.GONE);
        // holder.getView(R.id.rl_result).setVisibility(View.GONE);


        TextView textCount = holder.getView(R.id.tv_count);
        HtmlTextView htmlTextView = holder.getView(R.id.tv_exam);
        TextView textType = holder.getView(R.id.tv_type);
        ListView examListView = holder.getView(R.id.lv);
        if (bean.getChildLessonLibDetail().size() == 0) {
            textCount.setVisibility(View.GONE);
            htmlTextView.setVisibility(View.GONE);
            textType.setVisibility(View.GONE);
            examListView.setVisibility(View.GONE);
            return;
        }
        final List<ExamVo.DataBean> list = new ArrayList();
        final String id = String.valueOf(bean.getId());
        if (bean.getScore() > -1) {
            textCount.setText(position + ". (本题" + bean.getScore() + "分)");
        } else {
            textCount.setText(position + ".");
        }


        textType.setText(bean.getExamTextVo().getTemplateStyleName());
        htmlTextView.setHtmlText(bean.getExamTextVo().getExamStem());

        list.addAll(bean.getChildLessonLibDetail());
        final AllAnserAdapter allAnserAdapter = new AllAnserAdapter(list, context);
        examListView.setAdapter(allAnserAdapter);
        setListHeight(examListView);
    }

    //设置listview高度
    private void setListHeight(ListView lv) {
        ListAdapter la = lv.getAdapter();
        if (null == la) {
            return;
        }
        // calculate height of all items.
        int h = 0;
        final int cnt = la.getCount();
        for (int i = 0; i < cnt; i++) {
            View item = la.getView(i, null, lv);
            item.measure(0, 0);
            h += item.getMeasuredHeight();
        }
        // reset ListView height
        ViewGroup.LayoutParams lp = lv.getLayoutParams();
        lp.height = h + (lv.getDividerHeight() * (cnt - 1));
        lv.setLayoutParams(lp);
    }


    private Dialog showSelectOpenDialog(final OpenListener listener) {
        final Dialog dialog = new Dialog(mActivity, R.style.MyDialog);
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_album_or_camera, null);
        view.findViewById(R.id.tv_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByApp();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByOther();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
        return dialog;
    }

    interface OpenListener {
        void openByApp();

        void openByOther();
    }
}
