package com.example.app5kcrw.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.ExamVo;

import java.util.Collections;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：综合题小题的适配器
 * 修改内容：
 * 修改时间：on 2018/12/14
 * 修改单号：
 * 修改内容:
 */
public class AllAnserAdapter extends BaseAdapter {
    private List<ExamVo.DataBean> mList = null;
    private Context mContext;
    private boolean isOpen = true;

    public AllAnserAdapter(List<ExamVo.DataBean> List, Context mContext) {
        this.mList = List;
        this.mContext = mContext;
    }

    void setGoneParse(boolean isChecked) {

        isOpen = isChecked;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getExamTemplateId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = createItemView(getItemViewType(position));
        }
        if (convertView == null) {
            return null;
        }
        Object tag = convertView.getTag();
        if (tag instanceof SynSelectorHolder) {
            bindSelectorData(position, convertView, (SynSelectorHolder) tag);
        } else if (tag instanceof SynShortAnswerHolder) {
            bindShortAnswerData(position, convertView, (SynShortAnswerHolder) tag);
        } else if (tag instanceof SynFillBlankHolder) {
            bindFillBlank(position, convertView, (SynFillBlankHolder) tag);
        }
        return convertView;
    }

    private View createItemView(int itemViewType) {
        View view;
        SynHolder holder;
        switch (itemViewType) {
            case 1://单选
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;

            case 2://多选
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 3://判断
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 4://简答
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_text, null);
                holder = new SynShortAnswerHolder(view);
                view.setTag(holder);
                return view;
            case 6://填空
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_text, null);
                holder = new SynFillBlankHolder(view);
                view.setTag(holder);
                return view;
            default://综合题不嵌套综合题
                return null;
        }
    }

    private static class SynHolder {
        TextView tvCount;
        TextView tvType;

        SynHolder(View view) {
            tvCount = view.findViewById(R.id.tv_count);
            tvType = view.findViewById(R.id.tv_type);

        }
    }

    //单选多选判断
    private static class SynSelectorHolder extends SynHolder {
        HtmlTextView tvExam;//题干
        RadioGroup rg;//选项


        SynSelectorHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            rg = view.findViewById(R.id.rg_answer);
        }
    }

    //简答
    private static class SynShortAnswerHolder extends SynHolder {
        FillHtmlTextView tvExam;

        SynShortAnswerHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv);

        }
    }

    //填空
    private static class SynFillBlankHolder extends SynHolder {
        FillHtmlTextView tvExam;

        SynFillBlankHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv);
        }
    }

    //统一设置标题
    private void bindItemTitle(int position, SynHolder holder) {
        // mList.get(position).getExamSeq()
        //holder.tvCount.setText(position + 1 + "");
        if (mList.get(position).getScore() > -1) {
            holder.tvCount.setText((position+1) + ". (本题" + mList.get(position).getScore() + "分)");
        }
        holder.tvType.setText(getTemplateStyleName(mList.get(position).getExamTextVo().getExamTypeId()));
    }

    //选择判断
    private void bindSelectorData(int position, View convertView, SynSelectorHolder holder) {
        bindItemTitle(position, holder);
        ExamVo.DataBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlText(examTextVo.getExamStem());

        //无选项情况
        List<ExamText.ExamOptionsBean> examOptions = examTextVo.getExamOptions();
        if (examOptions == null || examOptions.isEmpty()) {
            holder.rg.removeAllViews();
            return;
        }
        //有选项
        Collections.sort(examOptions);
        int size = examOptions.size();
        int childCount = holder.rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            holder.rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(mContext).inflate(R.layout.item_child_select, holder.rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = holder.rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(examOptions.get(i).getContent());
            List<ExamText.ExamOptionsBean> examOptionss = dataBean.getExamTextVo().getExamOptions();
            if (examOptionss == null || examOptionss.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examOptionss);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examOptionss.get(0).getContent()));
            } else {//多选
                Collections.sort(examOptionss);
                for (ExamText.ExamOptionsBean optionBean : examOptionss) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getContent())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }

        }
    }

    /**
     * 填空
     */
    private void bindFillBlank(int position, View convertView, SynFillBlankHolder holder) {
        bindItemTitle(position, holder);
        ExamVo.DataBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlText(examTextVo.getExamStem());
    }

    /**
     * 简单题
     */
    private void bindShortAnswerData(int position, View convertView, SynShortAnswerHolder holder) {
        bindItemTitle(position, holder);
        ExamVo.DataBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlText(examTextVo.getExamStem());

    }

    String getTemplateStyleName(int num) {
        switch (num) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知";
        }
    }
}
