package com.example.app5kcrw.fragment.natives;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5kcrw.adapter.RgAnalyzeAdapter;
import com.example.app5kcrw.adapter.RgDetailStudentAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.RganalyzeFragmentPresenter;
import com.example.app5kcrw.view.RganalyzeFragmentView;
import com.example.app5libbase.views.ChartLegendView;
import com.sdzn.fzx.teacher.vo.RgAnalyze;
import com.sdzn.fzx.teacher.vo.RgDetailsVo;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 资料分析
 */
public class RganalyzeFragment extends MBaseFragment<RganalyzeFragmentPresenter> implements RganalyzeFragmentView {
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private ListView leftLv;
    private LinearLayout right;
    private RelativeLayout ly;
    private PieChart piechart;
    private TextView tvWCount;
    private TextView tvFinish;
    private TextView unfinsh;
    private TextView time;
    private LinearLayout ll;
    private ChartLegendView clv1;
    private ChartLegendView clv2;
    private ImageView photo;
    private TextView name;
    private TextView correctState;
    private ListView listview;


    private Bundle bundle;
    private String classId;
    private String id;
    private List<RgAnalyze.DataBean> mlist = new ArrayList<>();
    private List<RgDetailsVo.DataBean.StudentListBean> rgStudnet = new ArrayList<>();
    private RgAnalyzeAdapter rgAnalyzeAdapter;
    private int selPosition = 0;

    public RganalyzeFragment() {
        // Required empty public constructor
    }

    public static RganalyzeFragment newInstance(Bundle bundle) {

        RganalyzeFragment fragment = new RganalyzeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rganalyze2, container, false);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        leftLv = (ListView) view.findViewById(R.id.left_lv);
        right = (LinearLayout) view.findViewById(R.id.right);
        ly = (RelativeLayout) view.findViewById(R.id.ly);
        piechart = (PieChart) view.findViewById(R.id.piechart);
        tvWCount = (TextView) view.findViewById(R.id.tvWCount);
        tvFinish = (TextView) view.findViewById(R.id.tvFinish);
        unfinsh = (TextView) view.findViewById(R.id.unfinsh);
        time = (TextView) view.findViewById(R.id.time);
        ll = (LinearLayout) view.findViewById(R.id.ll);
        clv1 = (ChartLegendView) view.findViewById(R.id.clv1);
        clv2 = (ChartLegendView) view.findViewById(R.id.clv2);
        photo = (ImageView) view.findViewById(R.id.photo);
        name = (TextView) view.findViewById(R.id.name);
        correctState = (TextView) view.findViewById(R.id.correct_state);
        listview = (ListView) view.findViewById(R.id.listview);

        initData();
        leftLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                rgAnalyzeAdapter.selectPosition = position;
                rgAnalyzeAdapter.notifyDataSetChanged();
                mPresenter.getRgDetail(mlist.get(position).getId() + "");

            }
        });
        return view;
    }

    private void initData() {
        rgStudnet.add(null);

        bundle = getArguments();
        classId = bundle.getString("classId");
        id = bundle.getString("id");

        mPresenter.getVideoList(id, "0", "2");
    }

    @Override
    public void initPresenter() {
        mPresenter = new RganalyzeFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void setRgAnalyze(RgAnalyze analyze) {
        mlist = analyze.getData();
        if (analyze.getData() == null || analyze.getData().size() == 0) {
            right.setVisibility(View.GONE);
            llTaskEmpty.setVisibility(View.VISIBLE);
        } else {
            rgAnalyzeAdapter = new RgAnalyzeAdapter(analyze.getData(), getActivity());
            leftLv.setAdapter(rgAnalyzeAdapter);
            mPresenter.getRgDetail(analyze.getData().get(0).getId() + "");
        }

    }

    @Override
    public void setRgDetailAnalyze(RgDetailsVo rgDetailAnalyze) {
        tvFinish.setText(rgDetailAnalyze.getData().getCountStudentSubmit() + "人");
        unfinsh.setText(rgDetailAnalyze.getData().getCountStudentTotal() + "人");
        tvWCount.setText(rgDetailAnalyze.getData().getCountStudentSubmit() + "/" + rgDetailAnalyze.getData().getCountStudentTotal());
        time.setText("查看平均时长：" + ((int) rgDetailAnalyze.getData().getUseTimeAvg()) % (24 * 60 * 60) % (60 * 60) / 60 + "分" + ((int) rgDetailAnalyze.getData().getUseTimeAvg()) % (24 * 60 * 60) % (60 * 60) % 60 + "秒");
        rgStudnet.clear();
        rgStudnet.addAll(rgDetailAnalyze.getData().getStudentList());
        listview.setAdapter(new RgDetailStudentAdapter(rgStudnet, getActivity()));
        piechart.clearChart();
        piechart.addPieSlice(new PieModel("", rgDetailAnalyze.getData().getCountStudentSubmit(), getResources().getColor(R.color.fragment_main_chart_green)));
        piechart.addPieSlice(new PieModel("", rgDetailAnalyze.getData().getCountStudentTotal(), getResources().getColor(R.color.fragment_main_chart_violet)));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
