package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.ImgList;
import com.sdzn.fzx.teacher.vo.UndoCorrectDataVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/15
 * 修改单号：
 * 修改内容:
 */
public interface CorrectNativeActivityView extends BaseView {

    void saveSuccess(String path, boolean close);

    void networkError(String msg, boolean close);

    void undoSuccess(UndoCorrectDataVo undoCorrectDataVo);

    void saveBitmapSuccess(String b64Bitmap, boolean b);

    void saveFailed();

    void getImgList(ImgList imgList);

    void getMarkingImgList(ImgList imgList);
}
