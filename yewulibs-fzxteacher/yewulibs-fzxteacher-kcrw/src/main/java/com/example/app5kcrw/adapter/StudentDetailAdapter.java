package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.exam.FillBlankCheckTextView;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.ClozeAnswerBean;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.student.libutils.annotations.CheckState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class StudentDetailAdapter extends BaseRcvAdapter<StudentDetails.DataBean> {

    private Activity mActivity = null;
    private boolean showAnswer = true;//答案
    private int vid = 1;
    public ShortAnswerResultAdapter mClickStudentDetailAdapter;
    public boolean isFragmentFrist = false;

    public StudentDetailAdapter(Context context, List<StudentDetails.DataBean> mList, Activity activity, int v) {
        super(context, mList);
        mActivity = activity;
        this.vid = v;
    }

    public StudentDetailAdapter(Context context, List<StudentDetails.DataBean> mList, Fragment fragment, int v) {
        this(context, mList, fragment.getActivity(), v);
        this.fragment = fragment;
        isFragmentFrist = true;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        StudentDetails.DataBean bean = mList.get(position);
        return bean.getAnswer().getTemplateId();
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_jianda_exam);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_text);
            case 14://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 16://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, StudentDetails.DataBean bean) {
        switch (bean.getAnswer().getTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }

    private StudentDetailAllAdapter studentDetailAllAdapter;

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindAllAnswer(final BaseViewHolder holder, final int position, final StudentDetails.DataBean bean) {
        bindAllExamTitle(holder, position, bean);
        TextView etcomment = holder.getView(R.id.et_comment);
        RecyclerView examListView = holder.getView(R.id.lv);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        RelativeLayout rl_comment = holder.getView(R.id.rl_comment);

        rl_comment.setVisibility(View.VISIBLE);
        tv.setVisibility(View.VISIBLE);
        tv.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());
        final List<StudentDetails.DataBean.AnswerBean> list = new ArrayList();
        list.addAll(bean.getAnswer().getExamList());


        examListView.setLayoutManager(new LinearLayoutManager(context));
        studentDetailAllAdapter = new StudentDetailAllAdapter(context, list, mActivity);
        examListView.setAdapter(studentDetailAllAdapter);

        if (bean.getComment().equals("")) {
            etcomment.setHint("老师评语");
        } else {
            etcomment.setHint(bean.getComment());
        }

        /*发起评论*/
        etcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AnswerExamActivity");
                intent.putExtra("lessonTaskId", bean.getAnswer().getLessonTaskId());
                intent.putExtra("lessonTaskStudentId", bean.getAnswer().getLessonTaskStudentId());
                intent.putExtra("lessonAnswerExamId", bean.getAnswer().getId());
                mActivity.startActivity(intent);
            }
        });
    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final StudentDetails.DataBean bean) {
        bindExamTitle(holder, position, bean);

        ExamText examTextVo = bean.getAnswer().getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(examTextVo.getExamStem());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvText.setHtmlText(options.get(i).getContent());
            String str = String.valueOf((char) (65 + i));
            tvNumber.setText(str);


            tvText.setHtmlText(options.get(i).getContent());

            final List<StudentDetails.DataBean.AnswerBean.ExamOptionListBean> examList = bean.getAnswer().getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                tvNumber.setBackground(context.getResources().getDrawable(
                        bean.getAnswer().getExamTextVo().getExamOptions().get(i).isRight() ?
                                R.drawable.bg_select_selector_2 :
                                R.drawable.bg_result_select_selector_n));
                tvNumber.setSelected(TextUtils.equals(str, examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (StudentDetails.DataBean.AnswerBean.ExamOptionListBean optionBean : examList) {
                    if (TextUtils.equals(str, optionBean.getMyAnswer())) {
                        tvNumber.setBackground(context.getResources().getDrawable(
                                bean.getAnswer().getExamTextVo().getExamOptions().get(i).isRight() ?
                                        R.drawable.bg_select_selector_2 :
                                        R.drawable.bg_result_select_selector_n));
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }
    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final StudentDetails.DataBean bean) {
        bindExamTitle(holder, position, bean);
        FillHtmlTextView view = holder.getView(R.id.tv);
        view.setVisibility(View.GONE);
        final FillBlankCheckTextView tv = holder.getView(R.id.FillBlankCheckTextView);
        tv.setVisibility(View.VISIBLE);
        SparseArray<ClozeAnswerBean> array = new SparseArray<>();//作答结果
        List<StudentDetails.DataBean.AnswerBean.ExamOptionListBean> examOptionList = bean.getAnswer().getExamOptionList();
        final TextView etcomment = holder.getView(R.id.et_comment);

        if (bean.getComment().equals("")) {
            etcomment.setHint("老师评语");
        } else {
            etcomment.setHint(bean.getComment());
        }

        /*发起评论*/
        etcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AnswerExamActivity");
                intent.putExtra("lessonTaskId", bean.getAnswer().getLessonTaskId());
                intent.putExtra("lessonTaskStudentId", bean.getAnswer().getLessonTaskStudentId());
                intent.putExtra("lessonAnswerExamId", bean.getAnswer().getId());
                mActivity.startActivity(intent);
            }
        });
        for (int i = 0; i < examOptionList.size(); i++) {
            StudentDetails.DataBean.AnswerBean.ExamOptionListBean optionBean = examOptionList.get(i);

            int checkState;

            if (bean.getAnswer().getIsCorrect() != 0) {//todo,2019年2月27日15:39:323
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                } else {
                    checkState = CheckState.FALSE;
                }
            } else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(),

                    optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(), checkState));
        }
        if (bean.getAnswer().getExamTextVo() != null && bean.getAnswer().getExamTextVo().getExamStem() != null) {
            tv.setHtmlBody(bean.getAnswer().getExamTextVo().getExamStem(), array);
        }

        tv.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                if (!TextUtils.isEmpty(imageSrc)) {
                    CorrectDataVo correctData = new CorrectDataVo();
                    correctData.setSrc(imageSrc);
                    correctData.setSeq("");
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectActivity");
                    intent.putExtra("correctData", correctData);
                    intent.putExtra("vid", 0);
                    mActivity.startActivity(intent);
                } else {
                    ToastUtil.showLonglToast("图片地址为空");
                }
            }
        });

    }


    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final StudentDetails.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        final TextView textView = holder.getView(R.id.score_tv_pigai);
        final TextView etcomment = holder.getView(R.id.et_comment);

        if (Double.valueOf(bean.getAnswer().getScore()) > 0) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(bean.getAnswer().getScore() + "分");
        } else {
            textView.setVisibility(View.GONE);
        }

        tv.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());
        if (bean.getComment().equals("")) {
            etcomment.setHint("老师评语");
        } else {
            etcomment.setHint(bean.getComment());
        }

        /*发起评论*/
        etcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AnswerExamActivity");
                intent.putExtra("lessonTaskId", bean.getAnswer().getLessonTaskId());
                intent.putExtra("lessonTaskStudentId", bean.getAnswer().getLessonTaskStudentId());
                intent.putExtra("lessonAnswerExamId", bean.getAnswer().getId());
                mActivity.startActivity(intent);
            }
        });
        if (bean.getAnswer().getExamOptionList().size() == 0) {
            holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.VISIBLE);
        } else {
            holder.getView(R.id.rv_add_pic).setVisibility(View.VISIBLE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.GONE);
        }

        RecyclerView rv = holder.getView(R.id.rv_add_pic);
        rv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(context);
            rv.setAdapter(adapter);
        }
        //final ShortAnswerResultAdapter finalAdapter = adapter;
        adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
            @Override
            public void clickImage(int index, String url) {
                CorrectDataVo correctData = new CorrectDataVo();
                correctData.setSrc(bean.getAnswer().getExamOptionList().get(0).getId() + "");
                correctData.setSeq(index + "");
                correctData.setList(url);
                correctData.setEid(bean.getAnswer().getId() + "");
                correctData.setId(bean.getAnswer().getLessonTaskStudentId());
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectNativeActivity2");
                // Intent intent = new Intent(mActivity, CorrectActivity.class);
                intent.putExtra("correctData", correctData);
                intent.putExtra("vid", vid);
                intent.putExtra("position", position);
                mActivity.startActivity(intent);
                //mClickStudentDetailAdapter = finalAdapter;
               /* if (mActivity != null && !isFragmentFrist) {
                    mActivity.startActivity(intent);
                } else if (fragment != null) {
                    fragment.startActivityForResult(intent, 1234);
                }*/

            }
        });
        // List<StudentDetails.ExamOptionBean> examOptionList = bean.getExamOptionList();
        List<StudentDetails.DataBean.AnswerBean.ExamOptionListBean> examList = bean.getAnswer().getExamOptionList();
        for (StudentDetails.DataBean.AnswerBean.ExamOptionListBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
    }

    /**
     * 完型填空
     */
    private void bindFillExam(final BaseViewHolder holder, final int position, final StudentDetails.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());


    }

    private boolean goodFlag = true;
    private boolean errorFlag = true;
    private TextView textCount;
    private TextView textType;

    private void bindExamTitle(final BaseViewHolder holder, int position, final StudentDetails.DataBean bean) {
        final String id = String.valueOf(bean.getAnswer().getId());
        textCount = holder.getView(R.id.tv_count);
        textType = holder.getView(R.id.tv_type);


        if (bean.getAnswer().getFullScore() > -1) {
            if (bean.getAnswer().getTemplateId() == 6) {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + (bean.getAnswer().getFullScore() * bean.getAnswer().getEmptyCount()) + "分)");
            } else {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + bean.getAnswer().getFullScore() + "分)");
            }

        } else {

            textCount.setText(bean.getAnswer().getExamSeq() + ".");
        }
        if (bean.getAnswer().getExamTextVo() != null && bean.getAnswer().getExamTextVo().getTemplateStyleName() != null) {
            textType.setText(bean.getAnswer().getExamTextVo().getTemplateStyleName() + "");
        } else {
            textType.setText("");
        }

        final ImageView good = holder.getView(R.id.good);
        final ImageView error = holder.getView(R.id.error);
        if (bean.getAnswer().getCorrectType() == 1) {
            good.setImageResource(R.mipmap.dx_sel);
            goodFlag = false;
        } else {
            good.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }

        if (bean.getAnswer().getCorrectType() == 2) {
            error.setImageResource(R.mipmap.dx_sel);
            errorFlag = false;
        } else {
            error.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }
        final TextView show = holder.getView(R.id.show);
        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    show.setText("隐藏答案");
                    show.setTextColor(Color.parseColor("#5BA2EA"));
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    show.setText("显示答案");
                    show.setTextColor(Color.parseColor("#8A8A8A"));
                }
            }
        });

        holder.getView(R.id.good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goodFlag) {
                    good.setImageResource(R.mipmap.dx_sel);
                    error.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = false;
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "1");
                } else {
                    good.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

        holder.getView(R.id.error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (errorFlag) {
                    error.setImageResource(R.mipmap.dx_sel);
                    good.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = false;
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "2");
                } else {
                    error.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        ExamText examTextVo = bean.getAnswer().getExamTextVo();

        if (examTextVo == null) {
            return;
        }
        if (TextUtils.isEmpty(examTextVo.getExamAnswer())) {
            result.setHtmlText("略...");
        } else {
            result.setHtmlText(examTextVo.getExamAnswer());
        }
        if (TextUtils.isEmpty(examTextVo.getExamAnalysis())) {
            analysis.setHtmlText("略...");
        } else {
            analysis.setHtmlText(examTextVo.getExamAnalysis());
        }
    }

    private void bindAllExamTitle(final BaseViewHolder holder, int position, final StudentDetails.DataBean bean) {
        textCount = holder.getView(R.id.tv_count);
        textType = holder.getView(R.id.tv_type);

        if (bean.getAnswer().getFullScore() > -1) {
            if (bean.getAnswer().getTemplateId() == 6) {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + (bean.getAnswer().getFullScore() * bean.getAnswer().getEmptyCount()) + "分)");
            } else {
                textCount.setText(bean.getAnswer().getExamSeq() + ". (本题" + bean.getAnswer().getFullScore() + "分)");
            }
        } else {
            textCount.setText(bean.getAnswer().getExamSeq() + ".");
        }
        if (bean.getAnswer().getExamTextVo() != null && bean.getAnswer().getExamTextVo().getTemplateStyleName() != null) {
            textType.setText(bean.getAnswer().getExamTextVo().getTemplateStyleName() + "");
        } else {
            textType.setText("");
        }

        final TextView markingshow = holder.getView(R.id.show);
        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    markingshow.setText("隐藏答案");
                    markingshow.setTextColor(Color.parseColor("#5BA2EA"));
                    studentDetailAllAdapter.setShowAnswer(true);
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    markingshow.setText("显示答案");
                    markingshow.setTextColor(Color.parseColor("#8A8A8A"));
                    studentDetailAllAdapter.setShowAnswer(false);
                }
            }
        });
        final ImageView good = holder.getView(R.id.good);
        final ImageView error = holder.getView(R.id.error);
        if (bean.getAnswer().getCorrectType() == 1) {
            good.setImageResource(R.mipmap.dx_sel);
            goodFlag = false;
        } else {
            good.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }

        if (bean.getAnswer().getCorrectType() == 2) {
            error.setImageResource(R.mipmap.dx_sel);
            errorFlag = false;
        } else {
            error.setImageResource(R.mipmap.dxs_nor);
            goodFlag = true;
        }
        holder.getView(R.id.good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goodFlag) {
                    good.setImageResource(R.mipmap.dx_sel);
                    error.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = false;
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "1");
                } else {
                    good.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

        holder.getView(R.id.error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (errorFlag) {
                    error.setImageResource(R.mipmap.dx_sel);
                    good.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = false;
                    goodFlag = true;
                    updateState(bean.getAnswer().getId() + "", "2");
                } else {
                    error.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = true;
                    updateState(bean.getAnswer().getId() + "", "3");
                }
            }
        });

    }

    void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showShortlToast("操作成功");

                    }
                });
    }
}
