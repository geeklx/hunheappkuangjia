package com.example.app5home.ktjx1.skjl;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.sdzn.fzx.teacher.bean.ClassRecordBean;

public class ClassRecordAdapter extends BaseQuickAdapter<ClassRecordBean.RowsBean, BaseViewHolder> {

    public ClassRecordAdapter() {
        super(R.layout.recycleview_class_record_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassRecordBean.RowsBean item) {
        TextView tvClassTime = helper.itemView.findViewById(R.id.tv_class_time);//上课时间
        TextView tvClassRecordName = helper.itemView.findViewById(R.id.tv_class_record_name);//课程名称
        TextView tvClassName = helper.itemView.findViewById(R.id.tv_class_name);//上课班级名称
        tvClassTime.setText(item.getCreateTime());
        tvClassRecordName.setText(item.getName());
        tvClassName.setText(item.getClassName());
    }
}
