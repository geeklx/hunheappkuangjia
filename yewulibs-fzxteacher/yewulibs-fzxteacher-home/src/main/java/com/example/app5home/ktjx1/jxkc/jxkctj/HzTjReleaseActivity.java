package com.example.app5home.ktjx1.jxkc.jxkctj;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.ktjx1.jxkc.GroupClassAdapter;
import com.example.app5home.ktjx1.jxkc.presenter.EstablishCooperationPresenter;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.EventReleaseObject;
import com.example.app5libbase.ai.pop.ReleaseObjectPopupView;
import com.example.app5libbase.ai.pop.adapter.OnItemClickLitener;
import com.example.app5libbase.ai.pop.bean.TextList;
import com.example.app5libbase.listener.chatroom.AlbumOrCameraListener;
import com.example.app5libbase.listener.chatroom.DiscussionDeleteImage;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.util.GlideImageLoader;
import com.example.app5libbase.views.NoScrollGridView;
import com.example.app5libpublic.event.FullImageEvBus;
import com.example.app5xztl.adapter.NewImageGridViewAdapter;
import com.lxj.xpopup.XPopup;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.SchoolClassGroupBean;
import com.sdzn.fzx.teacher.presenter.ExploreReleasePresenter;
import com.sdzn.fzx.teacher.view.ExploreReleaseViews;
import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;
import rain.coder.photopicker.utils.UCropUtils;

/*
 * 合作探究发布
 * */
public class HzTjReleaseActivity extends BaseActWebActivity1 implements BaseOnClickListener, View.OnClickListener, EstablishCooperationView, ExploreReleaseViews
        , DiscussionDeleteImage, EasyPermissions.PermissionCallbacks {
    private TextView tvChapterTitle;//章节名称
    private EditText etDiscussionTitle;//讨论标题
    private EditText etDiscussionContent;//讨论内容
    private LinearLayout llPublishingObjects;//发布对象
    private RecyclerView rvSchoolClass;//发布对象
    private TextView endtime;//截止时间
    private TextView tvConfirm;//发布
    private TextView tvCancel;//取消
    private NoScrollGridView gridView;//图片选择
    private CalenderDialog calendarDialog;//日历dialog
    //    SchoolClassGroupPresenter schoolClassGroupPresenter;
    EstablishCooperationPresenter establishCooperationPresenter;
    ExploreReleasePresenter exploreReleasePresenter;
    private int checkedPos = 0;//默认学科
    private String classesId;
    private String chapterName;
    private String baseVolumeId;
    private String chapterId;
    private String lessonLibId;
    private String subjectId;

    NewImageGridViewAdapter gridViewAdapter;

    private boolean isEdit = false;
    private int deletePos;
    private int numSelect = 6;

    private ArrayList<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> pics;
    public static final int REQ_CODE_CAMERA = 1003;
    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Dialog dialog;

    private String publishGroupJson;
    private GroupClassAdapter groupClassAdapter;
    ArrayList<TextList> userList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hztj_release;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        classesId = getIntent().getStringExtra("classesId");
        chapterName = getIntent().getStringExtra("chapterName");
        baseVolumeId = getIntent().getStringExtra("baseVolumeId");
        chapterId = getIntent().getStringExtra("chapterId");
        lessonLibId = getIntent().getStringExtra("lessonLibId");
        subjectId = getIntent().getStringExtra("subjectId");

        dialog = new Dialog(HzTjReleaseActivity.this, R.style.notice_dialog);
        InitView();
        InieData();
        InitClick();
    }

    private void InitView() {
        EventBus.getDefault().register(this);
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        tvTitleName.setText("创建合作探究");
        if (isEdit) {
            for (int i = 0; i < pics.size(); i++) {
                mDatas.add(pics.get(i).getPicUrl());
            }
            if (gridViewAdapter != null) gridViewAdapter.notifyDataSetChanged();
        }

//        schoolClassGroupPresenter = new SchoolClassGroupPresenter();
//        schoolClassGroupPresenter.onCreate(this);
        exploreReleasePresenter = new ExploreReleasePresenter();
        exploreReleasePresenter.onCreate(this);

        establishCooperationPresenter = new EstablishCooperationPresenter();
        establishCooperationPresenter.onCreate(this);
//        schoolClassGroupPresenter.getClassGroup("Bearer " + (String) SPUtils.getInstance().getString("token", ""), classesId);
        tvChapterTitle = findViewById(R.id.tv_chapter_title);
        etDiscussionTitle = findViewById(R.id.et_discussion_title);
        etDiscussionContent = findViewById(R.id.et_discussion_content);
        llPublishingObjects = findViewById(R.id.ll_publishing_objects);
        rvSchoolClass = findViewById(R.id.rv_schoolclass);
        gridView = (NoScrollGridView) findViewById(R.id.gridView);
        endtime = findViewById(R.id.endtime);
        tvConfirm = findViewById(R.id.tv_confirm);
        tvCancel = findViewById(R.id.tv_cancel);
        tvChapterTitle.setText(chapterName);//章节名称
    }

    final List<String> mDatas = new ArrayList<>();

    private void InieData() {
        rvSchoolClass.setLayoutManager(new GridLayoutManager(this, 4));
//        rvSchoolClass.setLayoutManager(new LinearLayoutManager(mActivity));
        // 通过下面的方法，开启LinearLayout 的点击事件，使LinearLayout 可以调用OnClick()
        rvSchoolClass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llPublishingObjects.performClick();  //模拟父控件的点击
                }
                return false;
            }
        });

        gridViewAdapter = new NewImageGridViewAdapter(this, mDatas);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == adapterView.getChildCount() - 1) {//添加
                    showSelectImgDialog(new AlbumOrCameraListener() {
                        @Override
                        public void selectAlbum() {
                            if (numSelect == 0) {
                                ToastUtil.showShortlToast("图片最多选6张");
                            } else {
                                new PhotoPickConfig.Builder(HzTjReleaseActivity.this)
                                        .imageLoader(new GlideImageLoader())
                                        .showCamera(false)
                                        .maxPickSize(numSelect)
                                        .spanCount(8)
                                        .clipPhoto(false)
                                        .build();
                            }
                        }

                        @Override
                        public void selectCamera() {
                            requestPermissiontest();
                        }
                    });
                } else {
                    deletePos = i;
                    Intent intentImage = new Intent(AppUtils.getAppPackageName() + ".hs.act.FullImageActivity");
                    intentImage.putExtra("photoUrl", mDatas.get(i));
                    intentImage.putExtra("showDel", true);
                    startActivity(intentImage);
                }

            }
        });
    }

    private void InitClick() {
        llPublishingObjects.setOnClickListener(this);
        endtime.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    }

    List<SchoolClassGroupBean.GroupsBean> dataList;//学科集合
    String ChapterTitle;//章节名称
    String DiscussionTitle;//讨论标题
    String DiscussionContent;//讨论内容
    String PublishingObjects;//发布对象
    String jxendtime;//截止时间
    ReleaseObjectPopupView releaseObjectPopupView;//发布对象

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ll_publishing_objects) {
            releaseObjectPopupView = new ReleaseObjectPopupView(HzTjReleaseActivity.this, classesId);
            new XPopup.Builder(HzTjReleaseActivity.this)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .asCustom(releaseObjectPopupView)
                    .show();
        } else if (id == R.id.endtime) {
            showCalendarDialog();
        } else if (id == R.id.tv_confirm) {
            ChapterTitle = tvChapterTitle.getText().toString().trim();//章节名称
            DiscussionTitle = etDiscussionTitle.getText().toString().trim();//讨论标题
            DiscussionContent = etDiscussionContent.getText().toString().trim();//讨论内容
//            PublishingObjects = tvPublishingObjects.getText().toString().trim();
            jxendtime = endtime.getText().toString().trim();
            if (TextUtils.isEmpty(ChapterTitle)) {
                ToastUtils.showShort("章节名称不能为空");
                return;
            } else if (TextUtils.isEmpty(DiscussionTitle)) {
                ToastUtils.showShort("讨论标题不能为空");
                return;
            } else if (TextUtils.isEmpty(DiscussionContent)) {
                ToastUtils.showShort("讨论内容不能为空");
                return;
            } else if (TextUtils.isEmpty(publishGroupJson)) {
                ToastUtils.showShort("发布对象不能为空");
                return;
            } else if (TextUtils.isEmpty(jxendtime)) {
                ToastUtils.showShort("截止日期不能为空");
                return;
            } else {
                exploreReleasePresenter.addExploreRelease("Bearer " + (String) SPUtils.getInstance().getString("token", ""), baseVolumeId, chapterId, ChapterTitle,
                        DiscussionTitle, DiscussionContent, getPicJson(), lessonLibId, getpublishClassJson(), publishGroupJson, subjectId, jxendtime);
            }
        } else if (id == R.id.tv_cancel) {
            finish();
        }
    }

    /*发布班级分装*/
    public String getpublishClassJson() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("classId", classesId);
        jsonArray.add(jsonObject);
        String jsonOutput = jsonArray.toJSONString();
        return jsonOutput;
    }

//    private String groupId;
//    private String groupName;
    /*发布对象分装*/
//    public String getpublishGroupJson() {
//        JSONArray jsonArray = new JSONArray();
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("classId", classesId);
//        jsonObject.put("groupId", groupId);
//        jsonObject.put("groupName", groupName);
//        jsonArray.add(jsonObject);
//        String jsonOutput = jsonArray.toJSONString();
//        return jsonOutput;
//    }

    /*图片分装*/
    public String getPicJson() {
        JSONArray jsonArray = new JSONArray();
        if (mDatas.size() != 0) {
            for (int i = 0; i < mDatas.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("pic", mDatas.get(i));
                jsonArray.add(jsonObject);
            }
        }
        String jsonOutput = jsonArray.toJSONString();
        return jsonOutput;
    }

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(this, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    endtime.setText(StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    endTime = StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd");
                }
            });
        }
        calendarDialog.show();
    }

    /*发布合作探究成功*/
    @Override
    public void onSExploreReleaseSuccess(String s) {
        ToastUtils.showShort("发布成功");
        finish();
    }

    @Override
    public void onExploreReleaseFail(String msg) {
        ToastUtils.showShort(msg);
    }

//    @Override
//    public void getSchoolClassGroupSuccess(SchoolClassGroupBean schoolClassGroupBean) {
//        dataList = new ArrayList<>();
//        if (schoolClassGroupBean != null) {
//            dataList.addAll(schoolClassGroupBean.getGroups());
//        }
//    }
//
//    @Override
//    public void onSchoolClassGroupFailed(String msg) {
//        ToastUtil.showShortlToast(msg);
//    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void onUploadPicSuccess(UploadPicVo uploadVos) {
        for (int a = 0; a < uploadVos.getData().size(); a++) {
            mDatas.add(uploadVos.getData().get(a).getOriginalPath());
            if (mDatas.size() > 6) {
                ToastUtil.showShortlToast("图片最多选6张");
                for (int i = mDatas.size() - 1; i > 5; i--) {
                    mDatas.remove(i);
                }
            }
        }
        numSelect = 6 - mDatas.size();
        if (gridViewAdapter != null) {
            gridViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void success(String str) {
        HzTjReleaseActivity.this.finish();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void rventRelease(EventReleaseObject event) {
        if (event.getJson() != null) {
            publishGroupJson = event.getJson();
            userList = JSON.parseObject(publishGroupJson, new TypeReference<ArrayList<TextList>>() {
            });
            groupClassAdapter = new GroupClassAdapter(this, userList);
            rvSchoolClass.setAdapter(groupClassAdapter);
            groupClassAdapter.setOnItemClickLitener(new OnItemClickLitener() {
                @Override
                public void onItemClick(View view, int position) {
                    releaseObjectPopupView = new ReleaseObjectPopupView(HzTjReleaseActivity.this, classesId);
                    new XPopup.Builder(HzTjReleaseActivity.this)
                            .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                            .asCustom(releaseObjectPopupView)
                            .show();
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(FullImageEvBus event) {
        if (event.isDel()) {
            deleteImage(deletePos);
        }
    }

    @Override
    public void deleteImage(int position) {
        if (mDatas.size() > 0) {
            mDatas.remove(position);
            numSelect = 6 - mDatas.size();
        }
        gridViewAdapter.notifyDataSetChanged();
    }

    private void resultSend(List<Photo> pathList) {
        establishCooperationPresenter.uploadSubjectivePhoto(pathList, HzTjReleaseActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {
            //相册返回图片  图片选择器
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(path);
                    paths.add(photo);
                    resultSend(paths);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        resultSend(photoLists);
                    }
                }

            }
        } else if (requestCode == REQ_CODE_CAMERA) {
            //拍照返回
            if (resultCode == Activity.RESULT_OK) {
                startClipPic(false);
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {

                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(UCrop.getOutput(data));
                paths.add(photo);
                resultSend(paths);
            }
        } else if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            //从设置页面返回，判断权限是否申请。
            if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
//                Toast.makeText(this, "权限申请成功!", Toast.LENGTH_SHORT).show();
                toSystemCamera();
                dialog.dismiss();
            } else {
                requestPermissiontest();
//                Toast.makeText(this, "权限申请失败!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private File takeImageFile;

    /**
     * 调用相机
     */
    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            if (FileUtil.existsSdcard()) {
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            } else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            Uri uri;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                uri = Uri.fromFile(takeImageFile);
            } else {
                uri = FileProvider.getUriForFile(this, BuildConfig3.APPLICATION_ID + ".fileProvider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = this.getPackageManager().queryIntentActivities
                        (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    this.grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        this.startActivityForResult(takePictureIntent, REQ_CODE_CAMERA);
    }

    /**
     * 裁剪图片
     */
    public void startClipPic(boolean isFillBlank) {
        if (isFillBlank) {

        } else {
            String imagePath = rain.coder.photopicker.utils.ImageUtils.getImagePath(this, "/Crop/");
            File corpFile = new File(imagePath + rain.coder.photopicker.utils.ImageUtils.createFile());
            UCropUtils.start(this, new File(takeImageFile.getAbsolutePath()), corpFile, false);
        }
    }

    /**
     * 发送 图片
     */
    public void showSelectImgDialog(final AlbumOrCameraListener listener) {
        final Dialog dialog = new Dialog(this, R.style.Dialog);
        @SuppressWarnings("inflateParams")
        View view = this.getLayoutInflater().inflate(R.layout.dialog_album_or_camera_open, null);
        view.findViewById(R.id.tv_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlbum();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectCamera();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void requestPermissiontest() {
        if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            toSystemCamera();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 16061);
                }
            });
        }
    }

    public static class ChatPicBean {
        /**
         * pic : http://file.znclass.com/161095329844720190118_113003.png
         */

        private String pic;

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }
    }

}