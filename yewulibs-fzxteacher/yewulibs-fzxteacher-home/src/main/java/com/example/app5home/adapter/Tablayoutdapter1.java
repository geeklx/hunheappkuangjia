package com.example.app5home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.sdzn.fzx.teacher.bean.OneBean2;

public class Tablayoutdapter1 extends BaseQuickAdapter<OneBean2, BaseViewHolder> {

    public Tablayoutdapter1() {
        super(R.layout.recycleview_hxkt_tablayout_item1);
    }
    ViewCallBack viewCallBack;
    public void seetTablayoutdapter1Listener(ViewCallBack viewCallBack){
        this.viewCallBack=viewCallBack;
    }

    @Override
    protected void convert(final BaseViewHolder helper, OneBean2 item) {
        final TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        View view1 = helper.itemView.findViewById(R.id.view1);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        ImageView iv_jiantou=helper.itemView.findViewById(R.id.iv_jiantou);
        tv1.setText(item.getTab_name());

        if (item.getList().size()!=0){
            iv_jiantou.setVisibility(View.VISIBLE);
        }
        iv_jiantou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCallBack.viewOnClick(v,tv1,helper.getAdapterPosition());
            }
        });
        if (item.isEnable()) {
            //选中
            view1.setVisibility(View.VISIBLE);
            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color_FFFFFFFF));
            iv1.setImageResource(item.getTab_icon());
        } else {
            //未选中
            view1.setVisibility(View.INVISIBLE);
            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            iv1.setImageResource(item.getTab_icon());
        }
    }

    public interface ViewCallBack{
        void viewOnClick(View view,TextView tv,int position);
    }
}
