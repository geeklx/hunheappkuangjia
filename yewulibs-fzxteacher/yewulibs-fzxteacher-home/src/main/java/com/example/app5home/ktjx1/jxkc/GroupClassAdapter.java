package com.example.app5home.ktjx1.jxkc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.app5home.R;
import com.example.app5libbase.ai.pop.adapter.OnItemClickLitener;
import com.example.app5libbase.ai.pop.bean.TextList;

import java.util.List;

public class GroupClassAdapter extends RecyclerView.Adapter<GroupClassAdapter.MyViewHolder> {
    private Context context;
    private List<TextList> list;

    public GroupClassAdapter(Context context, List<TextList> list) {
        this.context = context;
        this.list = list;
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                context).inflate(R.layout.recycleview_group_class_item, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tv_groupclass.setText(list.get(position).getGroupName());
        if (mOnItemClickLitener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(holder.tv_groupclass, holder.getAdapterPosition());
                }
            });
        }
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //删除自带默认动画
                removeData(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    //  删除数据
    public void removeData(int position) {
        list.remove(position);
        //删除动画
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    /**
     * ViewHolder的类，用于缓存控件
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_groupclass;
        ImageView iv_delete;

        //因为删除有可能会删除中间条目，然后会造成角标越界，所以必须整体刷新一下！
        public MyViewHolder(View view) {
            super(view);
            tv_groupclass = (TextView) view.findViewById(R.id.tv_groupclass_name);
            iv_delete = (ImageView) view.findViewById(R.id.iv_delete);
        }
    }
}