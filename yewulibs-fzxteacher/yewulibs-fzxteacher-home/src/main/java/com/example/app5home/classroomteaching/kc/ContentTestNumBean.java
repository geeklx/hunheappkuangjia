package com.example.app5home.classroomteaching.kc;

import java.io.Serializable;

public class ContentTestNumBean implements Serializable {
    String testType;
    String testTypeNum;

    public ContentTestNumBean(String testType, String testTypeNum) {
        this.testType = testType;
        this.testTypeNum = testTypeNum;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public String getTestTypeNum() {
        return testTypeNum;
    }

    public void setTestTypeNum(String testTypeNum) {
        this.testTypeNum = testTypeNum;
    }
}
