package com.example.app5home.bdbk;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5libbase.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;


public class WebBaiduEncyclopediaActivity extends BaseActWebActivity1 implements BaseOnClickListener {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_webbaidubaike;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        TitleShowHideState(1);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            String target = getIntent().getStringExtra(URL_KEY);
            loadWebSite(target); // 刷新
        }
    }

    //屏幕方向发生改变的回调方法
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LogUtils.e("当前屏幕为横屏");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {//this.getResources().getConfiguration().orientation
            LogUtils.e("当前屏幕为竖屏");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "百度百科");
    }
}
