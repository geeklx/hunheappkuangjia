package com.example.app5home.ktjx1.bean;

import java.io.Serializable;
import java.util.List;

public class KtjxTabCountBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String url;
    private List<KtjxTabDataBean1> list;

    public KtjxTabCountBean(String id, String name,String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<KtjxTabDataBean1> getList() {
        return list;
    }

    public void setList(List<KtjxTabDataBean1> list) {
        this.list = list;
    }
}
