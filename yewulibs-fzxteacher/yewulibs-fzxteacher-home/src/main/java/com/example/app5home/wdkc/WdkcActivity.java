package com.example.app5home.wdkc;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5libbase.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;

/**
 * 我的课程
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class WdkcActivity extends BaseActWebActivity1 implements BaseOnClickListener {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        TitleShowHideState(1);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            String target = getIntent().getStringExtra(URL_KEY);
            loadWebSite(target); // 刷新
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.MeActivity?query1=" + "3"));
//                    startActivity(intent);
//                }
//            }, 8000);
        }
    }

    /*设置WebView---Title*/
    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "我的课程");
    }
}