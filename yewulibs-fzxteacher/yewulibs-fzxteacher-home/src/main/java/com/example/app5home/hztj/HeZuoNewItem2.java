package com.example.app5home.hztj;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;


public class HeZuoNewItem2 extends BaseActFragment1 {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_newitem2;
    }


    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
            Bundle bundle = this.getArguments();
            String target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
        }
        super.getJsInterface();
    }

    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(com.example.app5libbase.R.id.ll_base_container2);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }
}
