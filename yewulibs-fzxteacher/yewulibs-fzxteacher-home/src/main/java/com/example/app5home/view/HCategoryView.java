package com.example.app5home.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.HCategoryBean;

public interface HCategoryView extends IView {

    void OnCategorySuccess(HCategoryBean bean);

    void OnCategoryNodata(String bean);

    void OnCategoryFail(String msg);
}
