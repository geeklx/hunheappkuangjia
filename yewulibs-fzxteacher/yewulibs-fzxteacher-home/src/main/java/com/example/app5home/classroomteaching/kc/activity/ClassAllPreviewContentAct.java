package com.example.app5home.classroomteaching.kc.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.R;
import com.example.app5home.classroomteaching.kc.ClassSourseAdapter;
import com.example.app5home.classroomteaching.kc.CourseSourseImageFragment;
import com.example.app5home.classroomteaching.kc.CourseSourseOw365Fragment;
import com.example.app5home.classroomteaching.kc.CourseSourseRadioFragment;
import com.example.app5home.classroomteaching.kc.CourseSourseVideoFragment;
import com.example.app5home.classroomteaching.kc.adapter.ClassPreviewContentAdapter;
import com.example.app5home.classroomteaching.kc.fragment.CoursePreviewContentFragment;
import com.example.app5home.classroomteaching.kc.libDetailListBean;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.bean.ClassObjectivesBean;
import com.sdzn.fzx.teacher.presenter.TeachingCoursesContentPresenter;
import com.sdzn.fzx.teacher.view.TeachingCoursesContentViews;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ClassAllPreviewContentAct extends BaseActWebActivity1 implements BaseOnClickListener, TeachingCoursesContentViews, View.OnClickListener {
    TeachingCoursesContentPresenter contentPresenter;
    ClassPreviewContentAdapter classContentAdapter;
    GridLayoutManager rvCountentManager;
    private RecyclerView rvContent;
    ClassSourseAdapter classSourseAdapter;
    private RecyclerView rvSourse;
    public static final String URL_KEY = "url_key";
    public static final String URL_IMG = "img_url";
    public static final String URL_PLAYER = "player_url";
    private String lessonId; //= "692";//456  452   692  698
    private String titlename;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_all_content_preview;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        lessonId = getIntent().getStringExtra("lessonId");
        titlename = getIntent().getStringExtra("titlename");
        TitleShowHideState(1);
        tvTitleName.setText(titlename + "");
        setBaseOnClickListener(this);
        rvSourse = findViewById(R.id.rv_sourse);
        rvContent = findViewById(R.id.rv_content);


        rvCountentManager = new GridLayoutManager(this, 3);
        rvContent.setLayoutManager(rvCountentManager);
        rvSourse.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        contentPresenter = new TeachingCoursesContentPresenter();
        contentPresenter.onCreate(this);
        contentPresenter.queryCoursesContent("Bearer " + (String) SPUtils.getInstance().getString("token", ""), lessonId);


    }

    /**
     * 右侧数据返回处理
     */

    @Override
    public void onCoursesContentSuccess(ClassContentBean classContentBean) {

        setSourseData(classContentBean);
        setContentData(classContentBean);

        transactionContent = getSupportFragmentManager().beginTransaction();
        if (sourseListFragment.size() > 0) {
            transactionContent.replace(R.id.fl_fragment, sourseListFragment.get(0));
        } else if (contentListFragment.size() > 0) {
            transactionContent.replace(R.id.fl_fragment, contentListFragment.get(0));
        }
        transactionContent.commit();

    }

    List<CoursePreviewContentFragment> contentListFragment = new ArrayList<>();
    FragmentTransaction transactionContent;

    /**
     * 右侧课堂检测数据处理
     */

    private void setContentData(final ClassContentBean data) {
        contentListFragment.clear();

        if (data.getTestIngList() == null || data.getTestIngList().size() < 0) {
            return;
        }


        final List<libDetailListBean> dList = new ArrayList<>();

        for (int i = 0; i < data.getTestIngList().size(); i++) {
            final List<libDetailListBean> dList1 = new ArrayList<>();
            for (int a = 0; a < data.getTestIngList().get(i).getLibCourseDetailList().size(); a++) {
                dList1.add(new libDetailListBean(String.valueOf(i), "1",
                        "0", data.getTestIngList().get(i).getLibCourseDetailList().get(a).getSeq(),
                        data.getTestIngList().get(i).getLibCourseDetailList().get(a).getExamTemplateStyleName()));//此处id 用来判断是同一个课堂检测
            }

            Map<String, List<libDetailListBean>> listMap = detaiList(dList1);
            dList1.clear();
            dList1.add(new libDetailListBean(String.valueOf(i), data.getTestIngList().get(i).getName(), "3"));
            for (Map.Entry<String, List<libDetailListBean>> entry : listMap.entrySet()) {
                dList1.add(new libDetailListBean("-1", entry.getValue().get(0).getExamTemplateStyleName(), "2", entry.getValue().size(), ""));
                dList1.addAll(entry.getValue());
            }

            dList.addAll(dList1);

        }


        rvCountentManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if ("3".equals(dList.get(position).getType()) || "2".equals(dList.get(position).getType())) {
                    return 3;
                }
                return 1;
            }
        });

        classContentAdapter = new ClassPreviewContentAdapter(this, dList);
        rvContent.setAdapter(classContentAdapter);


        for (int y = 0; y < data.getTestIngList().size(); y++) {
            int count=0;//总分值
            int num = 0;//题目数量
            for (ClassContentBean.TestIngListBean.LibCourseDetailListBean beanData : data.getTestIngList().get(y).getLibCourseDetailList()) {
                num++;
                count=count+beanData.getScore();
            }

            Bundle bundle = new Bundle();
            bundle.putString("courseName",data.getCourseName()+"");
            bundle.putString("objectiveName",data.getObjectiveName()+"");
            bundle.putString("testCount", count + "");
            bundle.putString("testNum", num + "");
            bundle.putString("lessonId", lessonId);
            bundle.putSerializable("courseDetailTest", data.getTestIngList().get(y));
            contentListFragment.add(CoursePreviewContentFragment.newInstance(bundle));
        }

        classContentAdapter.addRecycleItemListener(listener);

    }


    /**
     * @param list
     * @return
     *
     * map 进行分组
     */
    private LinkedHashMap<String, List<libDetailListBean>> detaiList(List<libDetailListBean> list) {
        LinkedHashMap<String, List<libDetailListBean>> map = new LinkedHashMap<String, List<libDetailListBean>>();
        for (libDetailListBean bean : list) {
            if (map.containsKey(bean.getExamTemplateStyleName())) {
                List<libDetailListBean> subList = map.get(bean.getExamTemplateStyleName());
                subList.add(bean);
            } else {
                List<libDetailListBean> subList = new ArrayList<libDetailListBean>();
                subList.add(bean);
                map.put(bean.getExamTemplateStyleName(), subList);
            }
        }
        return map;
    }


    /**
     * 右侧课堂检测监听
     */
    private int posContent;//展示第几个contentListFragment
    ClassPreviewContentAdapter.OnRecycleItemListener<libDetailListBean> listener = new ClassPreviewContentAdapter.OnRecycleItemListener<libDetailListBean>() {
        @Override
        public void OnRecycleItemClick(View v, int position, libDetailListBean o) {
            int vId = v.getId();
            if (R.id.tv_content_title == vId) {
                if (Integer.parseInt(o.getId()) < contentListFragment.size() && posContent != Integer.parseInt(o.getId())) {
                    posContent = Integer.parseInt(o.getId());
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.replace(R.id.fl_fragment, contentListFragment.get(posContent));
                    trans.commit();
                }else if (contentListFragment.size()==1){
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.replace(R.id.fl_fragment, contentListFragment.get(posContent));
                    trans.commit();
                }
            } else if (R.id.tv_content_num == vId) {
//                ToastUtils.showShort(o.getId() + "-------" + o.getSeq());
                if (posContent == Integer.parseInt(o.getId())) {
                    contentListFragment.get(posContent).scrollToPos(o.getSeq());
                }
            }
        }
    };


    List<Fragment> sourseListFragment = new ArrayList<>();

    /**
     * 右侧资源数据处理
     */

    private void setSourseData(final ClassContentBean data) {
        sourseListFragment.clear();
        classSourseAdapter = new ClassSourseAdapter(data.getSourseList(),false);
        rvSourse.setAdapter(classSourseAdapter);

        for (int i = 0; i < data.getSourseList().size(); i++) {
            Bundle bundle = new Bundle();
            if ("7".equals(data.getSourseList().get(i).getResourceType()) || data.getSourseList().get(i).getResourceUrl().endsWith(".png")
                    || data.getSourseList().get(i).getResourceUrl().endsWith(".jpeg") || data.getSourseList().get(i).getResourceUrl().endsWith(".jpg")
                    || data.getSourseList().get(i).getResourceUrl().endsWith(".gif") || data.getSourseList().get(i).getResourceUrl().endsWith(".GIF")) {//图像
                bundle.putString(URL_IMG, data.getSourseList().get(i).getResourceUrl());
                sourseListFragment.add(CourseSourseImageFragment.newInstance(bundle));
            }
//            else if ("5".equals(data.getSourseList().get(i).getResourceType()) || data.getSourseList().get(i).getResourceUrl().endsWith(".mp4")) {//视频
//                bundle.putString(URL_PLAYER, data.getSourseList().get(i).getResourceUrl());
//                sourseListFragment.add(CourseSourseVideoFragment.newInstance(bundle));
//            }
            else if(data.getSourseList().get(i).getResourceUrl().endsWith(".mp4")||data.getSourseList().get(i).getResourceUrl().endsWith(".mpg")||data.getSourseList().get(i).getResourceUrl().endsWith(".3gp")||data.getSourseList().get(i).getResourceUrl().endsWith(".wmv")){
                bundle.putString(URL_PLAYER, data.getSourseList().get(i).getConvertResourceUrl());
                sourseListFragment.add(CourseSourseVideoFragment.newInstance(bundle));
            }else if ("8".equals(data.getSourseList().get(i).getResourceType()) || data.getSourseList().get(i).getResourceUrl().endsWith(".mp3")) {//音频
                bundle.putString("courseName",data.getCourseName()+"");
                bundle.putString("objectiveName",data.getObjectiveName()+"");
                bundle.putString(URL_PLAYER, data.getSourseList().get(i).getResourceUrl());
                sourseListFragment.add(CourseSourseRadioFragment.newInstance(bundle));
            } else {

                bundle.putString(URL_KEY, "https://ow365.cn/?i=" + BuildConfig3.OFFICE_ID + "&n=5&furl=" + data.getSourseList().get(i).getResourceUrl());
                sourseListFragment.add(CourseSourseOw365Fragment.newInstance(bundle));
            }
        }


        classSourseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position < sourseListFragment.size()) {
                    Log.e("eeee", "资源文件");
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.replace(R.id.fl_fragment, sourseListFragment.get(position));
                    trans.commit();
                }
            }
        });

//        classSourseAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                if (R.id.tv_sourse_push == view.getId()) {
//
//                    ToastUtils.showShort("资源文件发布--" + data.getSourseList().get(position).getId());
//                }
//            }
//        });


    }

    @Override
    public void onCoursesContentNodata(String msg) {

    }

    @Override
    public void onCoursesContentFail(String msg) {

    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();

    }
}
