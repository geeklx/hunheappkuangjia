package com.example.app5home;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

/**
 * 我的课程
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class SecondLevelWebViewActivity extends BaseActWebActivity1 implements BaseOnClickListener {
    //    private String titlename;
    private String target;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
//        titlename = getIntent().getStringExtra("titlename");
        TitleShowHideState(1);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            target = getIntent().getStringExtra(URL_KEY);
            loadWebSite(target); // 刷新
        }
    }

    //屏幕方向发生改变的回调方法
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LogUtils.e("当前屏幕为横屏");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {//this.getResources().getConfiguration().orientation
            LogUtils.e("当前屏幕为竖屏");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
    }
}