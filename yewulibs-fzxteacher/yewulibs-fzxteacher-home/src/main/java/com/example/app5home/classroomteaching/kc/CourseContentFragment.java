package com.example.app5home.classroomteaching.kc;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5home.R;
import com.example.app5home.classroomteaching.kc.test.ContentExamAdapter;
import com.example.app5kcrw.adapter.AnalyzeExamAdapter;
import com.example.app5kcrw.adapter.AnalyzeTopAdapter;
import com.example.app5kcrw.adapter.ExamAnalyzeAdapter;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.vo.AnalyzeNumberVo;
import com.sdzn.fzx.teacher.vo.AnalyzeVo;

import java.util.ArrayList;
import java.util.List;


public class CourseContentFragment extends BaseActFragment1 {

    private RecyclerView mRecyclerView;
    private ContentExamAdapter contentExamAdapter;
    private ClassContentBean.TestIngListBean testBean;
    private List<ContentTestNumBean> listTestNum;

    public static CourseContentFragment newInstance(Bundle bundle) {
        CourseContentFragment fragment = new CourseContentFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_course_content;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        initData();

        mRecyclerView = rootView.findViewById(R.id.recyclerView);
        contentExamAdapter = new ContentExamAdapter(App2.get(), testBean.getLibCourseDetailList(), getActivity(),listTestNum);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(contentExamAdapter);


    }

    private void initData() {
        testBean = (ClassContentBean.TestIngListBean) getArguments().getSerializable("courseDetailTest");
        listTestNum = (ArrayList<ContentTestNumBean>) getArguments().getSerializable("testNumList");
        Log.e("eeeNum", "" + listTestNum.size());
    }


    private void refreshAdapter() {
        contentExamAdapter.notifyDataSetChanged();
    }


    public void onDestroyView() {
        super.onDestroyView();

    }


    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }


}
