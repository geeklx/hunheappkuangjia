package com.example.app5home.classroomteaching.kc.radio;


/**
 * @author Reisen at 2018-12-06
 */
public class RadioServiceEvent {
    private RadioService mService;

    public RadioServiceEvent(RadioService service) {
        mService = service;
    }

    public RadioService getService() {
        return mService;
    }

    public void setService(RadioService service) {
        mService = service;
    }
}
