package com.example.app5home.classroomteaching.kc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;

import com.example.app5home.R;
import com.example.app5home.classroomteaching.kc.libDetailListBean;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;

import java.util.List;

/**
 * 试题 查看页面
 */
public class ClassPreviewContentAdapter extends BaseRcvAdapter<libDetailListBean> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */

    private OnRecycleItemListener listener;
    private int itemName;
    public int selectPos = -1;

    public ClassPreviewContentAdapter(Context context, List<libDetailListBean> data) {
        super(context, data);
    }

    public void addRecycleItemListener(OnRecycleItemListener listener) {
        this.listener = listener;
    }

    public void setSingleChoose(int pos) {

    }

    @Override
    public int getItemViewType(int position) {

        return mList.get(position).getItemType();
    }


    public static final int TYPE1 = 3;//课堂检测
    public static final int TYPE2 = 2;//子内容 试题类型
    public static final int TYPE3 = 0;//子内容 每个item

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE1) {
            return BaseViewHolder.get(context, null, parent, R.layout.item_class_content1);
        } else if (viewType == TYPE2) {
            return BaseViewHolder.get(context, null, parent, R.layout.item_class_content2);
        } else if (viewType == TYPE3) {
            return BaseViewHolder.get(context, null, parent, R.layout.item_class_content3);
        } else {
            return BaseViewHolder.get(context, null, parent, R.layout.item_class_content1);
        }

    }

    @Override
    public void convert(BaseViewHolder holder, final int position, final libDetailListBean libDetailListBean) {
        if (libDetailListBean.getItemType() == TYPE1) {
            holder.setText(R.id.tv_content_title, libDetailListBean.getName());
//            helper.setTextColor(R.id.tv_content, Color.parseColor("#ff0000"));
            holder.getView(R.id.tv_content_title).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnRecycleItemClick(v, position, libDetailListBean);
                }
            });

        } else if (libDetailListBean.getItemType() == TYPE2) {
            itemName = 0;
            holder.setText(R.id.tv_content_type, libDetailListBean.getName() + ":" + libDetailListBean.getSeq());
        } else if (libDetailListBean.getItemType() == TYPE3) {
            itemName++;
            holder.setText(R.id.tv_content_num, "" + itemName / 2);
            holder.getView(R.id.tv_content_num).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnRecycleItemClick(v, position, libDetailListBean);
                    selectPos = position;
                    notifyDataSetChanged();
                }
            });
            if (position == selectPos) {
                holder.setTextColor(R.id.tv_content_num, Color.parseColor("#ffffff"));
                holder.getView(R.id.tv_content_num).setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_22));

            } else {
                holder.setTextColor(R.id.tv_content_num, Color.parseColor("#73000000"));
                holder.getView(R.id.tv_content_num).setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_022));

            }
        }

    }

    public interface OnRecycleItemListener<T> {
        void OnRecycleItemClick(View v, int position, T o);
    }


}
