package com.example.app5home.xqfx;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.R;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter;
import com.example.app5home.adapter.Tablayoutdapter2;
import com.example.app5home.ktjx1.bean.KtjxTabBean;
import com.example.app5home.ktjx1.bean.KtjxTabCountBean;
import com.example.app5home.ktjx1.bean.KtjxTabDataBean1;
import com.example.app5home.xqfx.xq.XqfxNewItem1;
import com.example.app5home.xqfx.xq.XqfxNewItem2;
import com.example.app5home.xqfx.xq.XqfxNewItem3;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;

import java.util.ArrayList;
import java.util.List;

public class XqfxActivity extends BaseActWebActivity1 implements BaseOnClickListener {
    private ViewPagerSlide viewpagerBaseactTablayout;

    private String current_id;
    private Tablayoutdapter2 mAdapter11;
    private List<KtjxTabDataBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private int defCurrentItem = 0;//设置默认见面
    public static final String URL_KEY = "url_key";


    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_tablayout;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        donetwork();
    }

    /* 重载业务部分*/
    public void donetwork() {
        if ("1".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 0;
        } else if ("2".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 1;
        }
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        tvTitleName.setText("学情分析");
        onclick();
        setData();
    }

    private void setData() {
        KtjxTabBean ktjxTabBean = new KtjxTabBean();
        List<KtjxTabCountBean> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new KtjxTabCountBean("1", "学情首页", "/analysisLearning/home?step=0"));
        mDataTablayout1.add(new KtjxTabCountBean("2", "整体教学提升", "/analysisLearning/promotion?step=1"));
        mDataTablayout1.add(new KtjxTabCountBean("3", "阶段性教学", "/analysisLearning/studentDataAnalysis?step=2"));
        ktjxTabBean.setList(mDataTablayout1);

        mDataTablayout = new ArrayList<>();

        for (int i = 0; i < ktjxTabBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new KtjxTabDataBean1(ktjxTabBean.getList().get(i).getId(), ktjxTabBean.getList().get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + ktjxTabBean.getList().get(i).getUrl(), true));
            } else {
                mDataTablayout.add(new KtjxTabDataBean1(ktjxTabBean.getList().get(i).getId(), ktjxTabBean.getList().get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + ktjxTabBean.getList().get(i).getUrl(), false));
            }
        }

        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
//        Log.e("---geekyun----", current_id);
    }

    private void onclick() {
        //
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter2();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                KtjxTabDataBean1 bean1 = (KtjxTabDataBean1) adapter.getData().get(position);
                current_id = bean1.getTab_id();
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }


    private void init_viewp(List<KtjxTabDataBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                XqfxNewItem1 fragment1 = FragmentHelper.newFragment(XqfxNewItem1.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                XqfxNewItem2 fragment2 = FragmentHelper.newFragment(XqfxNewItem2.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 2) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                XqfxNewItem3 fragment3 = FragmentHelper.newFragment(XqfxNewItem3.class, bundle);
                mFragmentList.add(fragment3);
            }
            /*else if (i == 3) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                XqfxNewItem4 fragment4 = FragmentHelper.newFragment(XqfxNewItem4.class, bundle);
                mFragmentList.add(fragment4);
            } else {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                XqfxNewItem5 fragment5 = FragmentHelper.newFragment(XqfxNewItem5.class, bundle);
                mFragmentList.add(fragment5);
            }*/
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(4);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        KtjxTabDataBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            KtjxTabDataBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(KtjxTabDataBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

