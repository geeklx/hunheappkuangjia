package com.example.app5home.ktjx1.bean;

import java.io.Serializable;

public class CompetitionClassDataBindingBean implements Serializable {
    private String classesId;//年级id
    private String classesName;//年级名称
    private String gradeName;//年级
    private boolean enable;//选中状态

    public CompetitionClassDataBindingBean(String classesId, String classesName, String gradeName, boolean enable) {
        this.classesId = classesId;
        this.classesName = classesName;
        this.gradeName = gradeName;
        this.enable = enable;
    }

    public String getClassesId() {
        return classesId;
    }

    public void setClassesId(String classesId) {
        this.classesId = classesId;
    }

    public String getClassesName() {
        return classesName;
    }

    public void setClassesName(String classesName) {
        this.classesName = classesName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
