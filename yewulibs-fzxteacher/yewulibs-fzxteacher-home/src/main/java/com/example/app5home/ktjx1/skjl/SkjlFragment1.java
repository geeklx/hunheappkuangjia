package com.example.app5home.ktjx1.skjl;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.R;
import com.example.app5home.ktjx1.Interface.RiliChangeListener2;
import com.example.app5home.ktjx1.bean.CompetitionClassDataBindingBean;
import com.example.app5home.ktjx1.bean.KtjxTabDataBean1;
import com.example.app5home.ktjx1.holder.ContentHolder;
import com.example.app5home.ktjx1.holder.IconTreeItem;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.app5libbase.views.search.ClearEditText;
import com.example.app5libbase.views.search.KeyboardUtils;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.teacher.bean.ClassRecordBean;
import com.sdzn.fzx.teacher.bean.CompetitionClassBean;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;
import com.sdzn.fzx.teacher.bean.TreeBean;
import com.sdzn.fzx.teacher.presenter.ClassRecordPresenter;
import com.sdzn.fzx.teacher.presenter.CompetitionClassPresenter;
import com.sdzn.fzx.teacher.presenter.TeacherVolumePresenter;
import com.sdzn.fzx.teacher.presenter.TreePresenter;
import com.sdzn.fzx.teacher.utils.SPUtilsTracher;
import com.sdzn.fzx.teacher.view.ClassRecordViews;
import com.sdzn.fzx.teacher.view.CompetitionClassViews;
import com.sdzn.fzx.teacher.view.TeacherVolumeViews;
import com.sdzn.fzx.teacher.view.TreeViews;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/*
 * 上课记录
 * */
public class SkjlFragment1 extends BaseActFragment1 implements ClassRecordViews, CompetitionClassViews, TreeViews, NetconListener2, TeacherVolumeViews, RiliChangeListener2 {
    private ClassRecordPresenter classRecordPresenter;//上课记录数据层
    private CompetitionClassPresenter competitionClassPresenter;//上课记录选择班级
    private TeacherVolumePresenter teacherVolumePresenter;//册别数据层
    private TreePresenter treePresenter;//树结构数据层

    private RecyclerView rvClassRecord, rlclass;//上课列表班级列表
    private ClassRecordAdapter classRecordAdapter;//上课记录列表适配器
    private CompetitionClassAdapter competitionClassAdapter;//选择班级适配器
    private AndroidTreeView tView;//树结构
    private LinearLayout ClassrecordContainer;//包裹tree控件
    private TextView tvVolumeName;//册别内容
    private LinearLayout ClassrecordShijian;//时间搜索
    private TextView Classrecirdtime;
    private ImageView clearClasstime;

    private List<ClassRecordBean.RowsBean> ClassRecordData = new ArrayList<>();//上课记录集合
    private List<CompetitionClassDataBindingBean> CompetitionClassData = new ArrayList<>();//选择班级集合
    private SmartRefreshLayout refreshLayout1;//分页
    private LinearLayout ClassrecordNodata;//暂无数据
    private ContentHolder myHolder = null;//treeView Holder
    private CalenderDialog calendarDialog;//日历dialog
    private ClearEditText etSearch;//搜索
    private TextView tvSearch;//搜索
    private LinearLayout ShouyeOuterLayer;
    protected EmptyViewNew1 emptyview1;//网络监听
    protected NetState netState;

    private String chapterId = "";
    private String searchStr = "";//搜索框搜索条件
    private String teacherId="";
    private String classid = "";//班级id
    private String startTime = "";//开始时间
    private String endTime = "";//结束时间
    private int page = 1;//页数
    private static final int PAGE_SIZE = 10;//条数

    @Override
    public void OnSuccess(Object msg) {
        startTime = ((KtjxTabDataBean1) msg).getTab_id();
        endTime = ((KtjxTabDataBean1) msg).getTab_name();
        ToastUtils.showLong(startTime + "----" + endTime);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_class_record;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        /*绑定控件*/
        findview(rootView);

        /*点击事件*/
        onclick();

        /*初始化数据*/
        donetwork();
    }

    private void findview(View rootView) {
        rvClassRecord = rootView.findViewById(R.id.rv_class_record);
        rlclass = rootView.findViewById(R.id.rlclass);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);
        ClassrecordNodata = rootView.findViewById(R.id.ll_classrecord_nodata);
        ClassrecordContainer = rootView.findViewById(R.id.classrecord_container);//包裹tree控件
        tvVolumeName = rootView.findViewById(R.id.tv_volume_name);//册别按钮
        ClassrecordShijian = rootView.findViewById(R.id.ll_classrecirdtime);//时间选择器
        Classrecirdtime = rootView.findViewById(R.id.tv_classrecirdtime);//显示时间
        clearClasstime = rootView.findViewById(R.id.clear_classtime);//删除时间搜索
        etSearch = rootView.findViewById(R.id.et_search);//搜索框
        tvSearch = rootView.findViewById(R.id.tv_search);//搜索按钮
        /*动态隐藏软键盘*/
        KeyboardUtils.hideSoftInput(getActivity());

        emptyview1=rootView.findViewById(R.id.emptyview_order);//网络监听
        ShouyeOuterLayer=rootView.findViewById(R.id.ll_shouye_outer_layer);

        netState = new NetState();
        netState.setNetStateListener(this, getActivity());
        if (emptyview1 != null) {
            /*绑定empty方法*/
            emptyview1.bind(ShouyeOuterLayer).setRetryListener(new EmptyView.RetryListener() {
                @Override
                public void retry() {
                    // 分布局
                    emptyview1.loading();
                    //获取教程教材信息
                    teacherVolumePresenter.queryTeacherVolume(Token, teacherId);
                }
            });
            emptyview1.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        }

        rlclass.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        competitionClassAdapter = new CompetitionClassAdapter();
        rlclass.setAdapter(competitionClassAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rvClassRecord.setHasFixedSize(true);
        rvClassRecord.setNestedScrollingEnabled(false);
        rvClassRecord.setLayoutManager(linearLayoutManager);
        classRecordAdapter = new ClassRecordAdapter();
        rvClassRecord.setAdapter(classRecordAdapter);
    }

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(getActivity(), new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    Classrecirdtime.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  ~  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));

                    if (!Classrecirdtime.getText().equals("")) {
                        clearClasstime.setVisibility(View.VISIBLE);
                    }
                    startTime = StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd 00:00:00");// String.valueOf(startCalendar.getTimeInMillis());
                    endTime = StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd 00:00:00");//String.valueOf(endCalendar.getTimeInMillis());
                    setDatas();
                }
            });
        }
        calendarDialog.show();
    }

    private void onclick() {
        clearClasstime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearClasstime.setVisibility(View.GONE);
                Classrecirdtime.setText("请选择日期");
                startTime = "";
                endTime = "";
                setDatas();
            }
        });
        ClassrecordShijian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarDialog();
            }
        });
        tvVolumeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volumeshow(v);
            }
        });

        competitionClassAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CompetitionClassDataBindingBean bean1 = competitionClassAdapter.getData().get(position);
                set_footer_change(bean1);
                classid = bean1.getClassesId();
                setDatas();
            }
        });
        classRecordAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                ToastUtils.showLong(position);//item点击事件
            }
        });
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                setDatas();
            }
        });
        classRecordAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadMore();
            }
        }, rvClassRecord);

        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStr = etSearch.getText().toString().trim();
                KeyboardUtils.hideSoftInputs(getActivity());
                setDatas();
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        searchStr = etSearch.getText().toString().trim();
                        setDatas();
                        KeyboardUtils.hideSoftInputs(getActivity());
                    }
                    return true;
                }
                return false;
            }
        });
    }


    private void donetwork() {

        teacherVolumePresenter = new TeacherVolumePresenter();
        teacherVolumePresenter.onCreate(this);

        treePresenter = new TreePresenter();
        treePresenter.onCreate(this);


        competitionClassPresenter = new CompetitionClassPresenter();
        classRecordPresenter = new ClassRecordPresenter();

        competitionClassPresenter.onCreate(this);
        classRecordPresenter.onCreate(this);

        /*获取选择班级*/
        LoginBean loginBean = SPUtilsTracher.getLoginBean();
        teacherId = loginBean.getData().getOpenid();

        //获取教程教材信息
        teacherVolumePresenter.queryTeacherVolume(Token, teacherId);
    }

    private void RefreshLoad() {
        classRecordPresenter.queryClassRecord(Token, chapterId, classid, searchStr, startTime, endTime, page, PAGE_SIZE);
    }

    /**
     * 册别数据开始
     */
    private int checkedPos = 0;//默认学科
    List<TeacherVolumeBean.DataBean.BookVolumeResBean> dataList;//学科集合

    private void volumeshow(View view) {
        final List<String> stringList = new ArrayList<>();
        if (dataList==null){
            return;
        }
        if (dataList.size() != 0) {
            for (int i = 0; i < dataList.size(); i++) {
                stringList.add(dataList.get(i).getName());
            }
        }
        String[] title1 = stringList.toArray(new String[stringList.size()]);
        new XPopup.Builder(getActivity())
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .offsetY(20)
                .offsetX(40)
                .atView(view)  //依附于所点击的View，内部会自动判断在上方或者下方显示 new String[]{"语文", "数学", "物理", "化学"}
                .asAttachList1(title1,
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
                                tvVolumeName.setText(stringList.get(position));
                                ClassrecordContainer.removeAllViews();
                                treePresenter.queryTree(Token, dataList.get(position).getBookVolumeId());
                            }
                        }, R.layout.popup_subject_adapter_rv, R.layout.popup_subject_adapter_item, checkedPos)
                .show();
    }

    @Override
    public void onTeacherVolumeSuccess(TeacherVolumeBean teacherVolumeBean) {
        emptyview1.success();
        if (teacherVolumeBean == null) {
            return;
        }
        dataList = new ArrayList<>();
        for (int i = 0; i < teacherVolumeBean.getData().size(); i++) {
            if (teacherVolumeBean.getData().get(i).getSubjectId().equals(SPUtils.getInstance().getString("shouye_class_id"))) {
                dataList.addAll(teacherVolumeBean.getData().get(i).getBookVolumeRes());
            }
        }
        if (dataList != null && dataList.size() > 0) {
            tvVolumeName.setText(dataList.get(0).getName());
            treePresenter.queryTree(Token, dataList.get(0).getBookVolumeId());
        }
    }


    @Override
    public void onTeacherVolumeNodata(String msg) {
        emptyview1.errorNet();
        ToastUtils.showLong(msg);
    }

    @Override
    public void onTeacherVolumeFail(String msg) {
        emptyview1.errorNet();
        ToastUtils.showLong(msg);
    }


    /**
     * 树结构信息开始
     * tree学科信息
     */

    @Override
    public void onTreeSuccess(final List<TreeBean> treeBeanList) {
        if (treeBeanList == null) {
            return;
        }
        TreeNode nodeRoot = TreeNode.root();
        createTreeNodes(nodeRoot, treeBeanList);
        tView = new AndroidTreeView(getActivity(), nodeRoot);
        tView.setDefaultAnimation(true);
        //tView.setUse2dScroll(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleNo, false);
        ClassrecordContainer.addView(tView.getView(R.style.TreeNodeStyleNo));
        TreeNode nodeFirst = nodeRoot;
        for (int i = 0; i < 3; i++) {
            if (nodeFirst.getChildren().size() > 0) {
                nodeFirst = nodeFirst.getChildren().get(0);
                if (nodeFirst.getChildren().size() < 1) {
                    tView.expandNode(nodeFirst.getParent());
                    tView.expandLevel(0);
                    chapterId = ((IconTreeItem) nodeFirst.getValue()).id;
                    myHolder = (ContentHolder) nodeFirst.getViewHolder();
                    myHolder.setSelected();
                }
            }
        }
        tView.setDefaultNodeClickListener(nodeClickListener);
        competitionClassPresenter.queryCompetitionClass(Token, teacherId);
        setDatas();
    }

    private void createTreeNodes(TreeNode treeNode, List<TreeBean> dataParentBeans) {
        for (TreeBean dataParentBean : dataParentBeans) {
            TreeNode node = initTreeNode(dataParentBean.getData().getName(), dataParentBean.getData().getId());
            treeNode.addChild(node);
            if (dataParentBean != null && !dataParentBean.getChildren().isEmpty()) {
                createTreeNodes(node, dataParentBean.getChildren(), dataParentBean.getData().getId());
            }
        }
    }

    private void createTreeNodes(TreeNode treeNode, List<TreeBean.ChildrenBean> dataChildBeans, String parentId) {
        for (TreeBean.ChildrenBean dataChildBean : dataChildBeans) {
            if (dataChildBean.getData().getParentId().equals(parentId)) {
                TreeNode node = initTreeNode(dataChildBean.getData().getName(), dataChildBean.getData().getId());
                treeNode.addChild(node);
                createTreeNodes(node, dataChildBean.getChildren(), dataChildBean.getData().getId());
            }
        }
    }

    //    每一条数据
    private TreeNode initTreeNode(String name, String id) {
        return new TreeNode(new IconTreeItem(R.drawable.ic_tree_wei, name, id))
                .setViewHolder(new ContentHolder(getActivity()));  //p 图标样式   p内容展示
    }


    private TreeNode.TreeNodeClickListener nodeClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            if (node.getChildren().size() > 0) {
                return;
            }

            if (myHolder != null) {
                myHolder.setUnselected();
            }
            myHolder = (ContentHolder) node.getViewHolder();
            myHolder.setSelected();

            chapterId = ((IconTreeItem) value).id;
            setDatas();
        }
    };

    @Override
    public void onTreeNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onTreeFail(String msg) {
        ToastUtils.showLong(msg);
    }


    /*上课记录列表*/
    @Override
    public void onClassRecordSuccess(ClassRecordBean classRecordBean) {
        if (classRecordBean == null) {
            return;
        }
        refreshLayout1.finishRefresh(true);
        ClassRecordData = classRecordBean.getRows();
        if (page == 1) {
            if (ClassRecordData == null || ClassRecordData.size() == 0) {
                ClassrecordNodata.setVisibility(View.VISIBLE);
            } else {
                ClassrecordNodata.setVisibility(View.GONE);
            }
            setData(true, ClassRecordData);
        } else {
            setData(false, ClassRecordData);
        }
    }


    @Override
    public void onClassRecordNodata(String msg) {
        refreshLayout1.finishRefresh(false);
        ToastUtils.showLong(msg);
        if (page == 1) {
            classRecordAdapter.setEnableLoadMore(true);
        } else {
            classRecordAdapter.loadMoreFail();
        }
    }

    @Override
    public void onClassRecordFail(String msg) {
        refreshLayout1.finishRefresh(false);
        ToastUtils.showLong(msg);
        if (page == 1) {
            classRecordAdapter.setEnableLoadMore(true);
            // 根据需求修改bufen
            classRecordAdapter.setNewData(null);
        } else {
            classRecordAdapter.loadMoreFail();
        }
    }


    @Override
    public void onCompetitionClassSuccess(List<CompetitionClassBean> competitionClassBeans) {
        if (competitionClassBeans == null) {
            return;
        }
        CompetitionClassData.clear();
        CompetitionClassData.add(0, new CompetitionClassDataBindingBean("", "全部", "", true));
        for (int i = 0; i < competitionClassBeans.size(); i++) {
            CompetitionClassData.add(new CompetitionClassDataBindingBean(competitionClassBeans.get(i).getClassesId(), competitionClassBeans.get(i).getClassesName(), competitionClassBeans.get(i).getGradeName(), false));
        }
        competitionClassAdapter.setNewData(CompetitionClassData);
    }

    @Override
    public void onCompetitionClassNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onCompetitionClassFail(String msg) {
        ToastUtils.showLong(msg);
    }


    @Override
    public void onDestroy() {
        if (classRecordPresenter != null) {
            classRecordPresenter.onDestory();
            competitionClassPresenter.onDestory();
        }
        super.onDestroy();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }

    private void setData(boolean isRefresh, List data) {
        page++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            classRecordAdapter.setNewData(data);
        } else {
            if (size > 0) {
                classRecordAdapter.addData(data);
            }
        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            classRecordAdapter.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了");
        } else {
            classRecordAdapter.loadMoreComplete();
        }
    }


    /**
     * 加载第一页数据bufen
     */
    private void setDatas() {
        page = 1;
        classRecordAdapter.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        RefreshLoad();
    }

    /**
     * 加载更多bufen
     */
    private void loadMore() {
        RefreshLoad();
    }

    /*刷新选中Item更新*/
    private void set_footer_change(CompetitionClassDataBindingBean model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            competitionClassAdapter.notifyDataSetChanged();
        }
    }

    private void initList() {
        for (int i = 0; i < competitionClassAdapter.getData().size(); i++) {
            CompetitionClassDataBindingBean item = competitionClassAdapter.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }
}
