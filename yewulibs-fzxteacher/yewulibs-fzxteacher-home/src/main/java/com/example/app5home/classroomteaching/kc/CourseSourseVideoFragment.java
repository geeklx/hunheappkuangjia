package com.example.app5home.classroomteaching.kc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.Nullable;

import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.app5libbase.views.VerticalSeekBar;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import java.util.Timer;
import java.util.TimerTask;


public class CourseSourseVideoFragment extends BaseActFragment1 implements View.OnClickListener {
    private RelativeLayout rlVideoArea;
    private VideoView mVideoView;
    private ProgressBar loadingPb;
    private RelativeLayout rlController;
    private ImageView ivVolume;
    private LinearLayout llVolumeSeekBar;
    private VerticalSeekBar volumeSeekBar;
    private LinearLayout llProgress;
    private ImageView ivPlay;
    private TextView tvPlayTime;
    private SeekBar seekbar;
    private TextView tvTotalTime;


    private String path;

    private Timer timer;
    private TimerTask timerTask;

    private boolean isDisplay = false;

    private static final int MSG_CONTROLLER = 0;
    private static final int HIDE_TIME = 3000;

    private int width = 500;
    private int height = 300;
    private String title;

    private AudioManager mAudioManager;

    private MyVolumeReceiver mVolumeReceiver;

    private boolean isTracking = false;
    private boolean isPlaying = false;
    private long currentPos;
    private MediaPlayer mMediaPlayer;

    public static CourseSourseVideoFragment newInstance(Bundle bundle) {
        CourseSourseVideoFragment fragment = new CourseSourseVideoFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_course_sourse_video;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        path = getArguments().getString("player_url");

    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);




        rlVideoArea = (RelativeLayout) rootView.findViewById(R.id.rlVideoArea);
        mVideoView = (VideoView) rootView.findViewById(R.id.surface_view);
        loadingPb = (ProgressBar) rootView.findViewById(R.id.pb_loading);
        rlController = (RelativeLayout) rootView.findViewById(R.id.rlController);
        ivVolume = (ImageView) rootView.findViewById(R.id.ivVolume);
        llVolumeSeekBar = (LinearLayout) rootView.findViewById(R.id.llVolumeSeekBar);
        volumeSeekBar = (VerticalSeekBar) rootView.findViewById(R.id.volumeSeekBar);
        llProgress = (LinearLayout) rootView.findViewById(R.id.llProgress);
        ivPlay = (ImageView) rootView.findViewById(R.id.ivPlay);
        tvPlayTime = (TextView) rootView.findViewById(R.id.tvPlayTime);
        seekbar = (SeekBar) rootView.findViewById(R.id.seekbar);
        tvTotalTime = (TextView) rootView.findViewById(R.id.tvTotalTime);

        ivPlay.setOnClickListener(this);
        ivVolume.setOnClickListener(this);
        llProgress.setOnClickListener(this);
        llVolumeSeekBar.setOnClickListener(this);

//        Vitamio.isInitialized(getActivity());
        initView();
        playFunction(rootView);

    }

    private void initView() {
        timer = new Timer();
        playerHandler = new Handler() {
            public void handleMessage(Message msg) {
                // 更新播放进度
                if (isPlaying) {
                    updateProgress();
                }
            }
        };
        controllerHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_CONTROLLER:
                        setLayoutVisibility(View.GONE, false);
                        break;
                }
            }
        };


        // 解决thumb透明问题
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekbar.setSplitTrack(false);
            volumeSeekBar.setSplitTrack(false);
        }

        rlVideoArea.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                Log.e("eeeee", "--rlVideoArea.setOnTouchListene--" + event.getAction());
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    controllerHandler.removeMessages(MSG_CONTROLLER);
                    if (isDisplay) {
                        setLayoutVisibility(View.GONE, false);
                    } else {
                        setLayoutVisibility(View.VISIBLE, true);
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    hideControllerDelayed();
                }
                return true;
            }
        });
        SurfaceHolder holder = mVideoView.getHolder();
        holder.setFormat(PixelFormat.RGBX_8888);
        rlController.setVisibility(View.VISIBLE);
//        if (setScreenSize) {
//            ivFullScreen.setVisibility(View.VISIBLE);
//        } else {
//            ivFullScreen.setVisibility(View.GONE);
//        }
        seekbar.setMax(100);

        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeSeekBar.setMax(max);
        volumeSeekBar.setProgress(current);

        setVolumeImage(max, current);

        mVolumeReceiver = new MyVolumeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.VOLUME_CHANGED_ACTION");
        getActivity().registerReceiver(mVolumeReceiver, filter);

        setListener();
    }

    private void setListener() {
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        volumeSeekBar.setOnSeekBarChangeListener(onVolumeSeekBarChangeListener);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivPlay) {
            hideControllerDelayed();
            if (mVideoView.isPlaying()) {
                mVideoView.pause();
                ivPlay.setImageResource(R.mipmap.bofang_icon);
                isPlaying = false;
                updateProgress();
            } else {
                mVideoView.start();
                ivPlay.setImageResource(R.mipmap.zanting_icon);
                isPlaying = true;
                updateProgress();
            }
        } else if (id == R.id.ivVolume) {
            int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            volumeSeekBar.setProgress(current);
            llVolumeSeekBar.setVisibility(View.VISIBLE);
            // 设置全屏
//                if (mVideoView.isPlaying()) {
//                    hideControllerDelayed();
//                }
//                if (!isFullScreen) {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.FULL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.play_normal);
//                    isFullScreen = true;
//                } else {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.NORMAL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.fullscreen);
//                    isFullScreen = false;
//                }
        }
    }

    /**
     * 处理音量变化时的界面显示
     *
     * @author long
     */
    private class MyVolumeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //如果音量发生变化则更改seekbar的位置
            int currVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);// 当前的媒体音量
            volumeSeekBar.setProgress(currVolume);
        }
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    private void playFunction(View view) {
        if (TextUtils.isEmpty(path)) {
            ToastUtil.showShortlToast(getString(R.string.fragment_task_content_text12));
        } else {
            mVideoView.setVideoPath(path);
//            mVideoView.setVideoURI(Uri.parse(path));
            mVideoView.requestFocus();

//            mVideoView.setOnBufferingUpdateListener(onBufferingUpdateListener);
            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer = mp;
                    mMediaPlayer.setOnBufferingUpdateListener(onBufferingUpdateListener);
                    mMediaPlayer.start();
//                    setScreenSizeParams(width, height);

//                    mediaPlayer.setPlaybackSpeed(1.0f);
                    long duration = mVideoView.getDuration();
                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    tvTotalTime.setText(DateUtil.millsecondsToStr(duration));
                    isPlaying = true;
                    hideControllerDelayed();
                    loadingPb.setVisibility(View.GONE);
//                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            playerHandler.sendEmptyMessage(0);
                        }
                    };
                    timer.schedule(timerTask, 0, 1000);

//                    controllerHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            mVideoView.setBackgroundColor(Color.TRANSPARENT);
//                        }
//                    }, 100);
//                    isPlaying = true;
                }
            });
//            mVideoView.setOnStartListener(new VideoView.OnStartListener() {
//                @Override
//                public void onStart(MediaPlayer mediaPlayer) {
//
//                }
//            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (mVideoView != null) {
                        mVideoView.seekTo((int) 0);
                    }
                    ivPlay.setImageResource(R.mipmap.bofang_icon);
                    isPlaying = false;

                    seekbar.setSecondaryProgress(0);
                    seekbar.setProgress(0);
                    tvPlayTime.setText("00:00");


                }
            });
        }
    }

    MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            seekbar.setSecondaryProgress((int) (((float) percent / (float) mp.getDuration()) * seekbar.getMax()));
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            long time = seekBar.getProgress() * mVideoView.getDuration() / seekBar.getMax();
            if (time == 0) {
                time += 1000;
            }
            mVideoView.seekTo((int) time);
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
            isTracking = false;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
            isTracking = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            tvPlayTime.setText(DateUtil.millsecondsToStr(progress * mVideoView.getDuration() / seekBar.getMax()));
        }
    };

    SeekBar.OnSeekBarChangeListener onVolumeSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, AudioManager.FLAG_VIBRATE);
            setVolumeImage(seekBar.getMax(), progress);
        }
    };

    public void setVolumeImage(int max, int progress) {
        if (progress > max / 2) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangda_icon));
        } else if (progress == 0) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangwu_icon));
        } else {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangxiao_icon));
        }
    }


    Handler playerHandler;

    private void updateProgress() {
        long position = mVideoView.getCurrentPosition();
        long duration = mVideoView.getDuration();
        //  Log.d("position:::" + position);
        if (duration > 0) {
            long pos = seekbar.getMax() * position / duration;
            tvPlayTime.setText(DateUtil.millsecondsToStr(position));
            if (!isTracking) {
                seekbar.setProgress((int) pos);
            }
        }
    }

    /**
     * 控制播放器面板显示
     */
    private View.OnTouchListener touchListener = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                controllerHandler.removeMessages(MSG_CONTROLLER);
                if (isDisplay) {
                    setLayoutVisibility(View.GONE, false);
                } else {
                    setLayoutVisibility(View.VISIBLE, true);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                hideControllerDelayed();
            }
            return true;
        }
    };

    private void hideControllerDelayed() {
        if (controllerHandler != null) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
        }


    }

    private void setLayoutVisibility(int visibility, boolean isDisplay) {
        this.isDisplay = isDisplay;
        rlController.setVisibility(visibility);
        llVolumeSeekBar.setVisibility(View.GONE);
    }

    Handler controllerHandler;


    public void setScreenSizeParams(int width, int height) {
        int vWidth;
        int vHeight;
        if (mMediaPlayer == null) {
            vWidth = 0;
            vHeight = 0;
        } else {
            vWidth = mMediaPlayer.getVideoWidth();
            vHeight = mMediaPlayer.getVideoHeight();
        }

        if (vWidth > width || vHeight > height) {
            float wRatio = (float) vWidth / (float) width;
            float hRatio = (float) vHeight / (float) height;
            float ratio = Math.max(wRatio, hRatio);

            width = (int) Math.ceil((float) vWidth / ratio);
            height = (int) Math.ceil((float) vHeight / ratio);
        } else {
            float wRatio = (float) width / (float) vWidth;
            float hRatio = (float) height / (float) vHeight;
            float ratio = Math.min(wRatio, hRatio);

            width = (int) Math.ceil((float) vWidth * ratio);
            height = (int) Math.ceil((float) vHeight * ratio);
        }

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlVideoArea.getLayoutParams();
        params.width = width;
        params.height = height;
        rlVideoArea.setLayoutParams(params);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mVideoView.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        mVideoView.setLayoutParams(layoutParams);
        // 调用这个方法可以让视频大小适应父控件大小
//        mVideoView.setVideoLayout(VideoView.VIDEO_LAYOUT_SCALE, 0);

    }

    @Override
    public void onResume() {
        if (mVideoView != null) {
            mVideoView.seekTo((int) currentPos);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mVideoView != null) {
            currentPos = mVideoView.getCurrentPosition();
        }
        super.onPause();

    }

    @Override
    public void onDestroyView() {
        if (mVideoView != null) {
            mVideoView.stopPlayback();
        }
        super.onDestroyView();
        if (playerHandler != null) {
            playerHandler.removeCallbacksAndMessages(null);
            playerHandler = null;
        }
        if (controllerHandler != null) {
            controllerHandler.removeCallbacksAndMessages(null);
            controllerHandler = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (mVolumeReceiver != null) {
            getActivity().unregisterReceiver(mVolumeReceiver);
        }
    }


    @Override
    protected ViewGroup getAgentWebParent() {

        return null;
    }

}
