package com.example.app5home.ktjx1.jxkc.jxkctj;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter2;
import com.example.app5home.ktjx1.bean.KtjxTabBean;
import com.example.app5home.ktjx1.bean.KtjxTabCountBean;
import com.example.app5home.ktjx1.bean.KtjxTabDataBean1;
import com.example.app5home.zzxx.fbjltwo.DxjjFragment;
import com.example.app5home.zzxx.fbjltwo.TmfxFragment;
import com.example.app5home.zzxx.fbjltwo.WtjlFragment;
import com.example.app5home.zzxx.fbjltwo.ZdtjFragment;
import com.example.app5home.zzxx.fbjltwo.ZlfxFragment;
import com.example.app5libbase.R;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;

import java.util.ArrayList;
import java.util.List;

/*
 * 教学课程统计
 * */
public class JxkcTjActivity extends BaseActWebActivity1 implements BaseOnClickListener {
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter2 mAdapter11;
    private List<KtjxTabDataBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private int defCurrentItem = 0;//设置默认见面
    public static final String URL_KEY = "url_key";
    //    TabPresenter tabPresenter;
//    private String type = "8";//查询
    private String lessonId;
    private String courseName;
    private String lessonLibId;
    private String lessonTaskId;
    private String classesId;
    private String baseVolumeId;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_zzxxshouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        lessonId = getIntent().getStringExtra("lessonId");
        courseName = getIntent().getStringExtra("courseName");
        lessonLibId = getIntent().getStringExtra("lessonLibId");
        lessonTaskId = getIntent().getStringExtra("lessonTaskId");
        classesId = getIntent().getStringExtra("classesId");
        baseVolumeId = getIntent().getStringExtra("baseVolumeId");
        donetwork();
    }

    /* 重载业务部分*/
    public void donetwork() {
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        tvTitleName.setText(courseName);
        onclick();
//        tabPresenter = new TabPresenter();
//        tabPresenter.onCreate(this);
//        tabPresenter.querytab(type);
        setData();
    }

    private void setData() {
        KtjxTabBean ktjxTabBean = new KtjxTabBean();
        List<KtjxTabCountBean> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new KtjxTabCountBean("1", "自主学习统计", "/courseTeach/workbench/studyStatistics"));
        mDataTablayout1.add(new KtjxTabCountBean("2", "典型案例", "/courseTeach/workbench/typicalCase"));
        mDataTablayout1.add(new KtjxTabCountBean("3", "问题单", "/courseTeach/workbench/questionNaire"));
        mDataTablayout1.add(new KtjxTabCountBean("4", "合作探究结果", "/courseTeach/workbench/cooperationDetails"));
        mDataTablayout1.add(new KtjxTabCountBean("5", "课堂检测统计", "/courseTeach/workbench/detectionStatistics"));
        ktjxTabBean.setList(mDataTablayout1);

        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < ktjxTabBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new KtjxTabDataBean1(ktjxTabBean.getList().get(i).getId(), ktjxTabBean.getList().get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + ktjxTabBean.getList().get(i).getUrl(), true));
            } else {
                mDataTablayout.add(new KtjxTabDataBean1(ktjxTabBean.getList().get(i).getId(), ktjxTabBean.getList().get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + ktjxTabBean.getList().get(i).getUrl(), false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
    }


    private void onclick() {
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter2();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                KtjxTabDataBean1 bean1 = (KtjxTabDataBean1) adapter.getData().get(position);
                current_id = bean1.getTab_id();
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }


    private void init_viewp(final List<KtjxTabDataBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
//                bundle.putString(URL_KEY, mlist.get(i).getUrl()+"?" + "lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                bundle.putString("lessonId", lessonId);
                bundle.putString("lessonLibId", lessonLibId);
                bundle.putString("lessonTaskId",  lessonTaskId);
                bundle.putString("classesId",  classesId);
                bundle.putString("baseVolumeId", baseVolumeId);
                ZdtjFragment fragment1 = FragmentHelper.newFragment(ZdtjFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                TmfxFragment fragment2 = FragmentHelper.newFragment(TmfxFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 2) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                ZlfxFragment fragment2 = FragmentHelper.newFragment(ZlfxFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 3) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                DxjjFragment fragment2 = FragmentHelper.newFragment(DxjjFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                WtjlFragment fragment2 = FragmentHelper.newFragment(WtjlFragment.class, bundle);
                mFragmentList.add(fragment2);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(4);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        KtjxTabDataBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            KtjxTabDataBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(KtjxTabDataBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
//        tabPresenter.onDestory();
        super.onDestroy();
    }


//    @Override
//    public void onTabSuccess(RecyclerTabBean recyclerTabBean) {
//        setData(recyclerTabBean.getList());
//    }
//
//    @Override
//    public void onTabNodata(String msg) {
//        ToastUtils.showLong(msg);
//    }
//
//    @Override
//    public void onTabFail(String msg) {
//        ToastUtils.showLong(msg);
//    }

}