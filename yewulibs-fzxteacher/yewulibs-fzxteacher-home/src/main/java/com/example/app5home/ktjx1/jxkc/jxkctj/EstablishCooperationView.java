package com.example.app5home.ktjx1.jxkc.jxkctj;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;

public interface EstablishCooperationView extends IView {

    void networkError(String msg);

    void onUploadPicSuccess(UploadPicVo uploadVos);

    void success(String str);

}
