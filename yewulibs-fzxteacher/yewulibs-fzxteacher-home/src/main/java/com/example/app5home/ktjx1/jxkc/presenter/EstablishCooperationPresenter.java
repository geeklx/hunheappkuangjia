package com.example.app5home.ktjx1.jxkc.presenter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.TextUtils;
import android.util.Log;

import com.example.app5home.ktjx1.jxkc.jxkctj.EstablishCooperationView;
import com.example.app5libbase.controller.UserController;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.vo.QiniuUptoken;
import com.sdzn.fzx.teacher.vo.chatroom.ImageUploadInfoBean;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import rain.coder.photopicker.bean.Photo;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 *
 */
public class EstablishCooperationPresenter extends Presenter<EstablishCooperationView> {

    private Subscription subscribe1;
    private Subscription subscribe2;
    private String HostUrl = "";

//    public void saveGroupDiscussion(Map<String, Object> param) {
//        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
//                .SaveGroupDiscussion(param)
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            ApiException apiException = (ApiException) e;
//                            StatusVo status = apiException.getStatus();
//                            if (status != null && status.getMsg() != null) {
//                                ToastUtil.showLonglToast(status.getMsg());
//                            } else {
//
//                            }
//                        } else {
//
//                        }
//                    }
//
//                    @Override
//                    public void onNext(Object o) {
////                        mView.setAnalyzeVo(o);
//                        mView.success(o.toString());
//                    }
//                },mActivity,true,false,false,""));
//
//    }


    public void uploadSubjectivePhoto(final List<Photo> photoLists, Activity activity) {
        final com.sdzn.fzx.student.libutils.util.ProgressDialogManager pdm = new ProgressDialogManager(activity);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
//                RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);
                RequestParams params = new RequestParams(BuildConfig3.SERVER_ISERVICE_NEW2 + "/knowledgecenter/resource/getUploadToken");
                String key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
//                    QiniuUptoken uptoken = GsonUtil.fromJson(result, QiniuUptoken.class);
//                    if (uptoken == null || uptoken.getResult() == null || TextUtils.isEmpty(uptoken.getResult().getUpToken())) {
//                        throw new RuntimeException("上传失败: 获取upToken失败");
//                    }
//                    QiniuUptoken.ResultBean bean = uptoken.getResult();
//                    return Observable.just(new ImageUploadInfoBean(key, bean.getUpToken(), bean.getDomain(), bean.getImageStyle(), bitmap));

                    ResponseSlbBean1 responseSlbBean1 = GsonUtil.fromJson(result, ResponseSlbBean1.class);
                    if (responseSlbBean1 == null || responseSlbBean1.getResult() == null) {
                        throw new RuntimeException("");//上传失败: 获取upToken失败
                    }
                    return Observable.just(new ImageUploadInfoBean(key, responseSlbBean1.getResult().toString(), bitmap));
                   } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        getView().networkError(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                            .create("http://file.znclass.com", key);

                                    uploadPicVoList.add(dataBean);

                                    if (uploadPicVoList.size() == photoLists.size()) {
                                        pdm.cancelWaiteDialog();
                                        UploadPicVo mUploadPicVo = new UploadPicVo();
                                        mUploadPicVo.setData(uploadPicVoList);
                                        getView().onUploadPicSuccess(mUploadPicVo);
                                    }
                                } else {
                                    pdm.cancelWaiteDialog();
                                    getView().networkError(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }

    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }


}