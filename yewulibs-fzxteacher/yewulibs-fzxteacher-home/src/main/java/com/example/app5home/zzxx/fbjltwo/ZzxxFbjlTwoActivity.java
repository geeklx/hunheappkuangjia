package com.example.app5home.zzxx.fbjltwo;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter;
import com.example.app5libbase.R;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.OneBean1;
import com.sdzn.fzx.teacher.bean.RecyclerTabBean;
import com.sdzn.fzx.teacher.presenter.TabPresenter;
import com.sdzn.fzx.teacher.view.TabViews;

import java.util.ArrayList;
import java.util.List;

/*
 * 自主学习主页
 * */
public class ZzxxFbjlTwoActivity extends BaseActWebActivity1 implements BaseOnClickListener, TabViews {
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private int defCurrentItem = 0;//设置默认见面
    public static final String URL_KEY = "url_key";
    TabPresenter tabPresenter;
    private String type = "8";//查询
    private String id;
    private String titlename;
    private String url;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_zzxxshouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        id = getIntent().getStringExtra("id");
        titlename = getIntent().getStringExtra("titlename");
        url = getIntent().getStringExtra("url");
        donetwork();
    }

    /* 重载业务部分*/
    public void donetwork() {
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        tvTitleName.setText(titlename);
        onclick();
        tabPresenter = new TabPresenter();
        tabPresenter.onCreate(this);
        tabPresenter.querytab(type);
    }

    private void setData(List<RecyclerTabBean.ListBean> recyclerTabBeans) {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < recyclerTabBeans.size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + url, true));
            } else {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + url, false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
    }


    private void onclick() {
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
                current_id = bean1.getTab_id();
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }


    private void init_viewp(final List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "&step=0");// + "?id=" + id +
                ZdtjFragment1 fragment1 = FragmentHelper.newFragment(ZdtjFragment1.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "&step=1");// + "?id=" + id
                TmfxFragment fragment2 = FragmentHelper.newFragment(TmfxFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 2) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "&step=2");
                ZlfxFragment fragment2 = FragmentHelper.newFragment(ZlfxFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 3) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "&step=3");
                DxjjFragment fragment2 = FragmentHelper.newFragment(DxjjFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else {
                bundle.putString(URL_KEY, mlist.get(i).getUrl() + "&step=4");
                WtjlFragment fragment2 = FragmentHelper.newFragment(WtjlFragment.class, bundle);
                mFragmentList.add(fragment2);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(5);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        OneBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        tabPresenter.onDestory();
        super.onDestroy();
    }


    @Override
    public void onTabSuccess(RecyclerTabBean recyclerTabBean) {
        setData(recyclerTabBean.getList());
    }

    @Override
    public void onTabNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onTabFail(String msg) {
        ToastUtils.showLong(msg);
    }

}