package com.example.app5home.ktjx1.ktcj;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.sdzn.fzx.student.libutils.util.HorizontalProgress;
import com.sdzn.fzx.teacher.bean.ClassroomTestingBean;

public class ClassroomTestingAdapter extends BaseQuickAdapter<ClassroomTestingBean.RowsBean, BaseViewHolder> {

    public ClassroomTestingAdapter() {
        super(R.layout.recycleview_classroom_testing_item);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final ClassroomTestingBean.RowsBean item) {

        TextView ClassRoomstatus = helper.itemView.findViewById(R.id.tv_classRoom_status);//状态
        TextView ClassRoomName = helper.itemView.findViewById(R.id.tv_classRoom_name);//课程名称
        TextView ClassRoomClassName = helper.itemView.findViewById(R.id.tv_classRoom_className);//年级名称
        TextView ClassRoomCountExam = helper.itemView.findViewById(R.id.tv_classRoom_countExam);//试题数量
        TextView ClassRoomTime = helper.itemView.findViewById(R.id.tv_classRoom_time);//开始结束时间
        TextView ClassRoomAlreadyNum = helper.itemView.findViewById(R.id.tv_classRoom_alreadyNum);//提交数量
        TextView ClassRoomAllNum = helper.itemView.findViewById(R.id.tv_classRoom_allNum);//总数
        HorizontalProgress horizontalProgress = helper.itemView.findViewById(R.id.progress_horizontal);//提交数量进度条

        TextView JcDelete = helper.itemView.findViewById(R.id.tv_jcdelete);//删除
        TextView JcWithdraw = helper.itemView.findViewById(R.id.tv_jcwithdraw);//撤回
        TextView JcAnswer = helper.itemView.findViewById(R.id.tv_jcanswer);//答案
        TextView JcClosingQuestions = helper.itemView.findViewById(R.id.tv_jcclosingquestions);//收题
        TextView JcSo = helper.itemView.findViewById(R.id.tv_jcso);//可见
        TextView JcPreview = helper.itemView.findViewById(R.id.tv_jcpreview);//预览

        if (onHelperItemClickLitener != null) {
            helper.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onHelperItemClickLitener.OnHelperItemClick(helper, item);
                }
            });
        }

        if (onPreviewItemClickLitener != null) {
            JcPreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onPreviewItemClickLitener.OnPreviewItemClick(helper, item);
                }
            });
        }
        if (onSoItemClickLitener != null) {
            JcSo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSoItemClickLitener.OnSoItemClick(helper, item);
                }
            });
        }
        if (onClosingQuestionsItemCLickLitener != null) {
            JcClosingQuestions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClosingQuestionsItemCLickLitener.OnClosingQuestionsItemCLickLitener(helper, item);
                }
            });
        }
        if (onAnswerItemClickLitener != null) {
            JcAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAnswerItemClickLitener.OnAnswerItemClick(helper, item);
                }
            });
        }
        if (onWithdrawItemClickLitener != null) {
            JcWithdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onWithdrawItemClickLitener.OnWithdrawItemCLick(helper, item);
                }
            });
        }
        ClassRoomName.setText(item.getName());
        ClassRoomClassName.setText(item.getClassName());
        String countexam = "试题:  <font color=\"#FA541C\">" + item.getCountExam() + "</font>";
        ClassRoomCountExam.setText(Html.fromHtml(countexam));
        ClassRoomTime.setText(item.getStartTime() + " 至 " + item.getEndTime());
        String AlreadyNum = "已交:  <font color=\"#000000\">" + item.getAlreadyNum() + "</font>";
        ClassRoomAlreadyNum.setText(Html.fromHtml(AlreadyNum));
        String AllNum = "总数:  <font color=\"#000000\">" + item.getAllNum() + "</font>";
        ClassRoomAllNum.setText(Html.fromHtml(AllNum));
        if (item.getIsHide() == 1) {
            JcSo.setText("可见");
        } else {
            JcSo.setText("隐藏");
        }
        horizontalProgress.setMax(Integer.parseInt(item.getAllNum()));
        horizontalProgress.setProgress(Integer.parseInt(item.getAlreadyNum()));

        if (item.getStatus().equals("1")) {//未作答
            ClassRoomstatus.setText("待发布");
        } else if (item.getStatus().equals("2")) {//作答中
            ClassRoomstatus.setText("作答中");
        } else if (item.getStatus().equals("3")) {//已截止
            ClassRoomstatus.setText("已截止");
        } else if (item.getStatus().equals("4")) {//已完成
            ClassRoomstatus.setText("可批改");
        } else if (item.getStatus().equals("5")) {//已完成
            ClassRoomstatus.setText("已完成");
        }
        JcSo.setVisibility(View.GONE);//可见
        JcPreview.setVisibility(View.GONE);//预览
        JcWithdraw.setVisibility(View.GONE);//撤回
        JcAnswer.setVisibility(View.GONE);//答案
        JcClosingQuestions.setVisibility(View.GONE);//收题

        if (item.getStatus().equals("2") && item.getAnswerViewType().equals("3")) {
            JcSo.setVisibility(View.VISIBLE);//可见
            JcPreview.setVisibility(View.VISIBLE);//预览
            JcWithdraw.setVisibility(View.VISIBLE);//撤回
            JcAnswer.setVisibility(View.VISIBLE);//答案
            JcClosingQuestions.setVisibility(View.VISIBLE);//收题
        } else if (item.getAnswerViewType().equals("3")) {
            JcSo.setVisibility(View.VISIBLE);//可见
            JcPreview.setVisibility(View.VISIBLE);//预览
            JcWithdraw.setVisibility(View.VISIBLE);//撤回
            JcAnswer.setVisibility(View.VISIBLE);//答案
        } else if (item.getStatus().equals("2") || item.getStatus().equals("4")) {
            JcSo.setVisibility(View.VISIBLE);//可见
            JcPreview.setVisibility(View.VISIBLE);//预览
            JcWithdraw.setVisibility(View.VISIBLE);//撤回
            JcClosingQuestions.setVisibility(View.VISIBLE);//收题
        } else {
            JcSo.setVisibility(View.VISIBLE);//可见
            JcPreview.setVisibility(View.VISIBLE);//预览
            JcWithdraw.setVisibility(View.VISIBLE);//撤回
        }
//        }else if (item.getAnswerViewType().equals("2")){
//            JcClosingQuestions.setVisibility(View.GONE);//收题
//            JcAnswer.setVisibility(View.GONE);//答案
//        }else if (item.getAnswerViewType().equals("3")){
//            JcClosingQuestions.setVisibility(View.GONE);//收题
//        }else if (item.getAnswerViewType().equals("4")){
//            JcClosingQuestions.setVisibility(View.GONE);//收题
//            JcAnswer.setVisibility(View.GONE);//答案
//        }

    }

    public interface onHelperItemClickLitener {
        void OnHelperItemClick(BaseViewHolder helper, ClassroomTestingBean.RowsBean item);
    }

    public interface OnPreviewItemClickLitener {
        void OnPreviewItemClick(BaseViewHolder helper, ClassroomTestingBean.RowsBean item);
    }

    public interface OnSoItemClickLitener {
        void OnSoItemClick(BaseViewHolder holder, ClassroomTestingBean.RowsBean item);
    }

    public interface OnClosingQuestionsItemCLickLitener {
        void OnClosingQuestionsItemCLickLitener(BaseViewHolder holder, ClassroomTestingBean.RowsBean item);
    }

    public interface OnAnswerItemClickLitener {
        void OnAnswerItemClick(BaseViewHolder Holder, ClassroomTestingBean.RowsBean item);
    }

    public interface OnWithdrawItemClickLitener {
        void OnWithdrawItemCLick(BaseViewHolder Holder, ClassroomTestingBean.RowsBean item);
    }


    private onHelperItemClickLitener onHelperItemClickLitener;//item
    private OnPreviewItemClickLitener onPreviewItemClickLitener;//预览
    private OnSoItemClickLitener onSoItemClickLitener;//可见
    private OnClosingQuestionsItemCLickLitener onClosingQuestionsItemCLickLitener;//收题
    private OnAnswerItemClickLitener onAnswerItemClickLitener;//答案
    private OnWithdrawItemClickLitener onWithdrawItemClickLitener;//撤回


    public void setOnItemClickLitener(onHelperItemClickLitener onHelperItemClickLitener) {
        this.onHelperItemClickLitener = onHelperItemClickLitener;
    }

    public void setOnItemClickLitener(OnWithdrawItemClickLitener onWithdrawItemClickLitener) {
        this.onWithdrawItemClickLitener = onWithdrawItemClickLitener;
    }

    public void setOnItemClickLitener(OnAnswerItemClickLitener onAnswerItemClickLitener) {
        this.onAnswerItemClickLitener = onAnswerItemClickLitener;
    }

    public void setOnItemClickLitener(OnClosingQuestionsItemCLickLitener onClosingQuestionsItemCLickLitener) {
        this.onClosingQuestionsItemCLickLitener = onClosingQuestionsItemCLickLitener;
    }

    public void setOnItemClickLitener(OnSoItemClickLitener onSoItemClickLitener) {
        this.onSoItemClickLitener = onSoItemClickLitener;
    }

    public void setOnItemClickLitener(OnPreviewItemClickLitener onPreviewItemClickLitener) {
        this.onPreviewItemClickLitener = onPreviewItemClickLitener;
    }
}
