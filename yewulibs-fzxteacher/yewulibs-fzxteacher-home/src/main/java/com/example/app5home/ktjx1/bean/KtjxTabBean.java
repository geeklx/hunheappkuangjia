package com.example.app5home.ktjx1.bean;

import java.io.Serializable;
import java.util.List;

public class KtjxTabBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<KtjxTabCountBean> list;//

    public KtjxTabBean() {
    }

    public KtjxTabBean(List<KtjxTabCountBean> list) {
        this.list = list;
    }

    public List<KtjxTabCountBean> getList() {
        return list;
    }

    public void setList(List<KtjxTabCountBean> list) {
        this.list = list;
    }
}
