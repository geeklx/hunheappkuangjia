package com.example.app5home.ktjx1.ktcj;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.example.app5home.ktjx1.Interface.RiliChangeListener3;
import com.example.app5home.ktjx1.bean.CompetitionClassDataBindingBean;
import com.example.app5home.ktjx1.bean.KtjxTabDataBean1;
import com.example.app5home.ktjx1.holder.ContentHolder;
import com.example.app5home.ktjx1.holder.IconTreeItem;
import com.example.app5home.ktjx1.skjl.CompetitionClassAdapter;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.app5libbase.views.search.ClearEditText;
import com.example.app5libbase.views.search.KeyboardUtils;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.util.SingleButtonUtils;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.teacher.bean.ClassroomTestingBean;
import com.sdzn.fzx.teacher.bean.CompetitionClassBean;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;
import com.sdzn.fzx.teacher.bean.TreeBean;
import com.sdzn.fzx.teacher.presenter.ClassroomTestingPresenter;
import com.sdzn.fzx.teacher.presenter.CompetitionClassPresenter;
import com.sdzn.fzx.teacher.presenter.NowOutPresenter;
import com.sdzn.fzx.teacher.presenter.SendAnswerOpenPresenter;
import com.sdzn.fzx.teacher.presenter.SendIsHidePresenter;
import com.sdzn.fzx.teacher.presenter.TeacherQuestionPresenter;
import com.sdzn.fzx.teacher.presenter.TeacherVolumePresenter;
import com.sdzn.fzx.teacher.presenter.TreePresenter;
import com.sdzn.fzx.teacher.utils.SPUtilsTracher;
import com.sdzn.fzx.teacher.view.ClassroomTestingViews;
import com.sdzn.fzx.teacher.view.CompetitionClassViews;
import com.sdzn.fzx.teacher.view.NowOutViews;
import com.sdzn.fzx.teacher.view.SendAnswerOpenViews;
import com.sdzn.fzx.teacher.view.SendIsHideViews;
import com.sdzn.fzx.teacher.view.TeacherQuestionViews;
import com.sdzn.fzx.teacher.view.TeacherVolumeViews;
import com.sdzn.fzx.teacher.view.TreeViews;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/*
 * 课堂检测
 * */
public class KtjcFragment1 extends BaseActFragment1 implements ClassroomTestingViews, TreeViews,
        NetconListener2, TeacherVolumeViews, TeacherQuestionViews, CompetitionClassViews,
        View.OnClickListener, RiliChangeListener3, NowOutViews, SendAnswerOpenViews, SendIsHideViews {
    private RecyclerView rvClassroomTesting, rlClassroom;
    private RadioButton radioButton1, radioButton2, radioButton3, radioButton4, radioButton5;

    private String chapterId = "";
    private String classid = "";
    private String searchStr = "";//搜索框搜索条件
    private String teacherId = "";
    private String status = "";
    private String startTime = "";
    private String endTime = "";
    private int page = 1;//页数
    private static final int PAGE_SIZE = 10;//条数

    private ClassroomTestingAdapter classroomTestingAdapter;//列表适配器
    private CompetitionClassAdapter competitionClassAdapter;//班级适配器
    private ClassroomTestingPresenter classroomTestingPresenter;//列表数据层
    private CompetitionClassPresenter competitionClassPresenter;//班级数据层
    private TeacherQuestionPresenter teacherQuestionPresenter;//收题
    private NowOutPresenter nowOutPresenter;//撤回
    private SendAnswerOpenPresenter sendAnswerOpenPresenter;//答案
    private SendIsHidePresenter sendIsHidePresenter;//可见隐藏
    private List<ClassroomTestingBean.RowsBean> classroomTestingDataBindingBeans = new ArrayList<>();//列表集合
    private List<CompetitionClassDataBindingBean> CompetitionClassData = new ArrayList<>();//班级集合
    private SmartRefreshLayout refreshLayout1;//刷新
    private LinearLayout ktjcNodata;//无数据
    private LinearLayout containerView;//包裹tree控件
    private TextView tvVolumeName;//册别内容
    private AndroidTreeView tView;//树结构
    private TreePresenter treePresenter;//树结构数据层
    private TeacherVolumePresenter teacherVolumePresenter;//册别数据层
    private ContentHolder myHolder = null;//treeView Holder
    private LinearLayout ClassrecordShijian;//时间搜索
    private TextView Classrecirdtime;
    private ImageView clearClasstime;
    private CalenderDialog calendarDialog;//日历dialog
    private ClearEditText etSearch;//搜索
    private TextView tvSearch;//搜索
    private LinearLayout ShouyeOuterLayer;
    protected EmptyViewNew1 emptyview1;//网络监听
    protected NetState netState;

    @Override
    public void OnSuccess(Object msg) {
        startTime = ((KtjxTabDataBean1) msg).getTab_id();
        endTime = ((KtjxTabDataBean1) msg).getTab_name();
        ToastUtils.showShort(startTime + "课堂检测" + endTime);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_classroom_testing;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        /*绑定控件*/
        findview(rootView);

        /*点击事件*/
        onclick();

        /*初始化数据*/
        donetwork();
    }

    private void donetwork() {
        /*课本教材*/
        teacherVolumePresenter = new TeacherVolumePresenter();
        teacherVolumePresenter.onCreate(this);


        /*树结构课程数据*/
        treePresenter = new TreePresenter();
        treePresenter.onCreate(this);

        /*课堂检测列表*/
        classroomTestingPresenter = new ClassroomTestingPresenter();
        classroomTestingPresenter.onCreate(this);

        /*上课记录选择班级*/
        competitionClassPresenter = new CompetitionClassPresenter();
        competitionClassPresenter.onCreate(this);

        /*收题*/
        teacherQuestionPresenter = new TeacherQuestionPresenter();
        teacherQuestionPresenter.onCreate(this);

        /*撤回*/
        nowOutPresenter = new NowOutPresenter();
        nowOutPresenter.onCreate(this);

        /*答案*/
        sendAnswerOpenPresenter = new SendAnswerOpenPresenter();
        sendAnswerOpenPresenter.onCreate(this);
        /*可见隐藏*/
        sendIsHidePresenter = new SendIsHidePresenter();
        sendIsHidePresenter.onCreate(this);


        LoginBean loginBean = SPUtilsTracher.getLoginBean();
        teacherId = loginBean.getData().getOpenid();
        //刷新教材
        teacherVolumePresenter.queryTeacherVolume(Token, teacherId);
    }

    private void findview(View rootView) {
        rvClassroomTesting = rootView.findViewById(R.id.rv_Classroom_testing);
        rlClassroom = rootView.findViewById(R.id.rl_classroom);
        containerView = rootView.findViewById(R.id.classroom_container);//包裹tree控件
        tvVolumeName = rootView.findViewById(R.id.tv_volume_name);//册别按钮
        etSearch = rootView.findViewById(R.id.et_search);//搜索框
        tvSearch = rootView.findViewById(R.id.tv_search);//搜索按钮
        /*动态隐藏软键盘*/
        KeyboardUtils.hideSoftInput(getActivity());
        emptyview1 = rootView.findViewById(R.id.emptyview_order);//网络监听
        ShouyeOuterLayer = rootView.findViewById(R.id.ll_shouye_outer_layer);

        netState = new NetState();
        netState.setNetStateListener(this, getActivity());
        if (emptyview1 != null) {
            /*绑定empty方法*/
            emptyview1.bind(ShouyeOuterLayer).setRetryListener(new EmptyView.RetryListener() {
                @Override
                public void retry() {
                    // 分布局
                    emptyview1.loading();
                    teacherVolumePresenter.queryTeacherVolume(Token, teacherId);
                }
            });
            emptyview1.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        }

        radioButton1 = rootView.findViewById(R.id.radioButton1);
        radioButton2 = rootView.findViewById(R.id.radioButton2);
        radioButton3 = rootView.findViewById(R.id.radioButton3);
        radioButton4 = rootView.findViewById(R.id.radioButton4);
        radioButton5 = rootView.findViewById(R.id.radioButton5);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);//刷新控件
        ktjcNodata = rootView.findViewById(R.id.ll_ktjc_nodata);//暂无数据

        ClassrecordShijian = rootView.findViewById(R.id.ll_classrecirdtime);//时间选择器
        Classrecirdtime = rootView.findViewById(R.id.tv_classrecirdtime);
        clearClasstime = rootView.findViewById(R.id.clear_classtime);

        radioButton1.setOnClickListener(this);
        radioButton2.setOnClickListener(this);
        radioButton3.setOnClickListener(this);
        radioButton4.setOnClickListener(this);
        radioButton5.setOnClickListener(this);

        rlClassroom.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        competitionClassAdapter = new CompetitionClassAdapter();
        rlClassroom.setAdapter(competitionClassAdapter);

        rvClassroomTesting.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));
        classroomTestingAdapter = new ClassroomTestingAdapter();
        rvClassroomTesting.setAdapter(classroomTestingAdapter);
    }

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(getActivity(), new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    Classrecirdtime.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  ~  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));

                    if (!Classrecirdtime.getText().equals("")) {
                        clearClasstime.setVisibility(View.VISIBLE);
                    }
                    startTime = StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd 00:00:00");// String.valueOf(startCalendar.getTimeInMillis());
                    endTime = StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd 00:00:00");//String.valueOf(endCalendar.getTimeInMillis());
                    setDatas();
                }
            });
        }
        calendarDialog.show();
    }

    /*点击事件*/
    private void onclick() {
        /*搜索按钮关键字搜索*/
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStr = etSearch.getText().toString().trim();
                KeyboardUtils.hideSoftInputs(getActivity());
                setDatas();
            }
        });
        /*软键盘关键字搜索*/
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        searchStr = etSearch.getText().toString().trim();
                        setDatas();
                        KeyboardUtils.hideSoftInputs(getActivity());
                    }
                    return true;
                }
                return false;
            }
        });
        /*清空日期搜索*/
        clearClasstime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearClasstime.setVisibility(View.GONE);
                Classrecirdtime.setText("请选择日期");
                startTime = "";
                endTime = "";
                setDatas();
            }
        });
        /*展开时间搜索*/
        ClassrecordShijian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarDialog();
            }
        });

        /**
         *选择册别*/
        tvVolumeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volumeshow(v);
            }
        });
        /*班级选择搜索*/
        competitionClassAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CompetitionClassDataBindingBean bean1 = competitionClassAdapter.getData().get(position);
                set_footer_change(bean1);
                classid = bean1.getClassesId();
                setDatas();
            }
        });
        /*预览功能*/
        classroomTestingAdapter.setOnItemClickLitener(new ClassroomTestingAdapter.OnPreviewItemClickLitener() {
            @Override
            public void OnPreviewItemClick(BaseViewHolder helper, ClassroomTestingBean.RowsBean item) {
                if (SingleButtonUtils.isFastClick()) {
                    return;
                }
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ClassAllPreviewContentAct");
                intent.putExtra("lessonId", item.getLessonId());
                intent.putExtra("titlename", item.getName());
                startActivity(intent);
            }
        });
        /*可见*/
        classroomTestingAdapter.setOnItemClickLitener(new ClassroomTestingAdapter.OnSoItemClickLitener() {
            @Override
            public void OnSoItemClick(BaseViewHolder holder, ClassroomTestingBean.RowsBean item) {
                if (SingleButtonUtils.isFastClick()) {
                    return;
                }
                if (item.getIsHide() == 1) {
                    sendIsHidePresenter.addSendIsHide(Token, item.getId(), false);
                } else {
                    sendIsHidePresenter.addSendIsHide(Token, item.getId(), true);
                }

            }
        });
        /*收题*/
        classroomTestingAdapter.setOnItemClickLitener(new ClassroomTestingAdapter.OnClosingQuestionsItemCLickLitener() {
            @Override
            public void OnClosingQuestionsItemCLickLitener(BaseViewHolder holder, ClassroomTestingBean.RowsBean item) {
                if (SingleButtonUtils.isFastClick()) {
                    return;
                }
                teacherQuestionPresenter.queryTeacherQuestion(Token, item.getId());
            }
        });
        /*答案*/
        classroomTestingAdapter.setOnItemClickLitener(new ClassroomTestingAdapter.OnAnswerItemClickLitener() {
            @Override
            public void OnAnswerItemClick(BaseViewHolder Holder, ClassroomTestingBean.RowsBean item) {
                if (SingleButtonUtils.isFastClick()) {
                    return;
                }
                sendAnswerOpenPresenter.addSendAnswerOpen(Token, item.getId());
            }
        });
        /*撤回*/
        classroomTestingAdapter.setOnItemClickLitener(new ClassroomTestingAdapter.OnWithdrawItemClickLitener() {
            @Override
            public void OnWithdrawItemCLick(BaseViewHolder Holder, ClassroomTestingBean.RowsBean item) {
                if (SingleButtonUtils.isFastClick()) {
                    return;
                }
                nowOutPresenter.addNowOut(Token, item.getId());
            }
        });
        /*列表点击事件*/
        classroomTestingAdapter.setOnItemClickLitener(new ClassroomTestingAdapter.onHelperItemClickLitener() {
            @Override
            public void OnHelperItemClick(BaseViewHolder helper, ClassroomTestingBean.RowsBean item) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.KtjxKtjcTjActivity"));
                intent.putExtra("lessonId", item.getLessonId());
                intent.putExtra("name", item.getName());
                intent.putExtra("lessonLibId", item.getLessonLibId());
                intent.putExtra("lessonTaskId", item.getLessonTaskId());
                intent.putExtra("classesId", item.getClassId());
                startActivity(intent);
            }
        });

        /*下拉刷新*/
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                setDatas();
            }
        });
        /*上拉加载更多*/
        classroomTestingAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadMore();
            }
        }, rvClassroomTesting);
    }

    /* 上课记录选择班级 */
    @Override
    public void onCompetitionClassSuccess(List<CompetitionClassBean> competitionClassBeans) {
        if (competitionClassBeans == null) {
            return;
        }
        CompetitionClassData.clear();
        CompetitionClassData.add(0, new CompetitionClassDataBindingBean("", "全部", "", true));
        for (int i = 0; i < competitionClassBeans.size(); i++) {
            CompetitionClassData.add(new CompetitionClassDataBindingBean(competitionClassBeans.get(i).getClassesId(), competitionClassBeans.get(i).getClassesName(), competitionClassBeans.get(i).getGradeName(), false));
        }
        competitionClassAdapter.setNewData(CompetitionClassData);
    }

    @Override
    public void onCompetitionClassNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onCompetitionClassFail(String msg) {
        ToastUtils.showShort(msg);
    }


    private void RefreshLoad() {
        classroomTestingPresenter.queryClassroomTesting(Token, chapterId, classid, searchStr, status, startTime, endTime, page, PAGE_SIZE);
    }

    /*课堂检测列表*/
    @Override
    public void onClassroomTestingSuccess(ClassroomTestingBean classroomTestingBean) {
        if (classroomTestingBean == null) {
            return;
        }
        refreshLayout1.finishRefresh(true);
        classroomTestingDataBindingBeans = classroomTestingBean.getRows();
        if (page == 1) {
            if (classroomTestingDataBindingBeans == null || classroomTestingDataBindingBeans.size() == 0) {
                ktjcNodata.setVisibility(View.VISIBLE);
            } else {
                ktjcNodata.setVisibility(View.GONE);
            }
            setData(true, classroomTestingDataBindingBeans);
        } else {
            setData(false, classroomTestingDataBindingBeans);
        }
    }

    @Override
    public void onClassroomTestingNodata(String msg) {
        ToastUtils.showShort(msg);
        refreshLayout1.finishRefresh(false);
        if (page == 1) {
            classroomTestingAdapter.setEnableLoadMore(true);
        } else {
            classroomTestingAdapter.loadMoreFail();
        }

    }

    @Override
    public void onClassroomTestingFail(String msg) {
        ToastUtils.showShort(msg);
        refreshLayout1.finishRefresh(false);
        if (page == 1) {
            classroomTestingAdapter.setEnableLoadMore(true);
            // 根据需求修改bufen
            classroomTestingAdapter.setNewData(null);
        } else {
            classroomTestingAdapter.loadMoreFail();
        }
    }


    /*刷新选中Item更新*/
    private void set_footer_change(CompetitionClassDataBindingBean model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            competitionClassAdapter.notifyDataSetChanged();
        }
    }

    private void initList() {
        for (int i = 0; i < competitionClassAdapter.getData().size(); i++) {
            CompetitionClassDataBindingBean item = competitionClassAdapter.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }

    private void setData(boolean isRefresh, List data) {
        page++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            classroomTestingAdapter.setNewData(data);
        } else {
            if (size > 0) {
                classroomTestingAdapter.addData(data);
            }
        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            classroomTestingAdapter.loadMoreEnd(isRefresh);
//            ToastUtils.showShort("已经到底了");
        } else {
            classroomTestingAdapter.loadMoreComplete();
        }
    }


    /**
     * 加载第一页数据bufen
     */
    private void setDatas() {
        page = 1;
        classroomTestingAdapter.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        RefreshLoad();
    }


    /**
     * 加载更多bufen
     */
    private void loadMore() {
        RefreshLoad();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showShort(ids);
    }

    @Override
    public void onDestroy() {
        if (classroomTestingPresenter != null) {
            classroomTestingPresenter.onDestory();
            competitionClassPresenter.onDestory();
        }
        super.onDestroy();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.radioButton1) {//全部
            status = "";
        } else if (id == R.id.radioButton2) {//未作答
            status = "1";
        } else if (id == R.id.radioButton3) {//作答中
            status = "2";
        } else if (id == R.id.radioButton4) {//已截止
            status = "3";
        } else if (id == R.id.radioButton5) {//已完成
            status = "4";
        }
        setDatas();
    }

    private int checkedPos = 0;//默认学科
    List<TeacherVolumeBean.DataBean.BookVolumeResBean> dataList;//学科集合

    private void volumeshow(View view) {
        final List<String> stringList = new ArrayList<>();
        if (dataList==null){
            return;
        }
        if (dataList.size() != 0) {
            for (int i = 0; i < dataList.size(); i++) {
                stringList.add(dataList.get(i).getName());
            }
        }
        String[] title1 = stringList.toArray(new String[stringList.size()]);
        new XPopup.Builder(getActivity())
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .offsetY(20)
                .offsetX(40)
                .atView(view)  //依附于所点击的View，内部会自动判断在上方或者下方显示 new String[]{"语文", "数学", "物理", "化学"}
                .asAttachList1(title1,
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
                                tvVolumeName.setText(stringList.get(position));
                                containerView.removeAllViews();
                                treePresenter.queryTree(Token, dataList.get(position).getBookVolumeId());
                            }
                        }, R.layout.popup_subject_adapter_rv, R.layout.popup_subject_adapter_item, checkedPos)
                .show();
    }

    /*册别接口成功*/
    @Override
    public void onTeacherVolumeSuccess(TeacherVolumeBean teacherVolumeBean) {
        emptyview1.success();
        if (teacherVolumeBean == null) {
            return;
        }
        dataList = new ArrayList<>();
        for (int i = 0; i < teacherVolumeBean.getData().size(); i++) {
            if (teacherVolumeBean.getData().get(i).getSubjectId().equals(SPUtils.getInstance().getString("shouye_class_id"))) {
                dataList.addAll(teacherVolumeBean.getData().get(i).getBookVolumeRes());
            }
        }
        if (dataList != null && dataList.size() > 0) {
            tvVolumeName.setText(dataList.get(0).getName());
            treePresenter.queryTree(Token, dataList.get(0).getBookVolumeId());
        }
    }

    @Override
    public void onTeacherVolumeNodata(String msg) {
        emptyview1.errorNet();
        ToastUtils.showShort(msg);
    }

    @Override
    public void onTeacherVolumeFail(String msg) {
        emptyview1.errorNet();
        ToastUtils.showShort(msg);
    }


    /**
     * 树结构信息开始
     * tree学科信息
     */

    @Override
    public void onTreeSuccess(final List<TreeBean> treeBeanList) {
        if (treeBeanList == null) {
            return;
        }
        TreeNode nodeRoot = TreeNode.root();
        createTreeNodes(nodeRoot, treeBeanList);
        tView = new AndroidTreeView(getActivity(), nodeRoot);
        tView.setDefaultAnimation(true);
        //tView.setUse2dScroll(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleNo, false);
        containerView.addView(tView.getView(R.style.TreeNodeStyleNo));
        TreeNode nodeFirst = nodeRoot;
        for (int i = 0; i < 3; i++) {
            if (nodeFirst.getChildren().size() > 0) {
                nodeFirst = nodeFirst.getChildren().get(0);
                if (nodeFirst.getChildren().size() < 1) {
                    tView.expandNode(nodeFirst.getParent());
                    tView.expandLevel(0);
                    chapterId = ((IconTreeItem) nodeFirst.getValue()).id;
                    myHolder = (ContentHolder) nodeFirst.getViewHolder();
                    myHolder.setSelected();
                }
            }
        }
        tView.setDefaultNodeClickListener(nodeClickListener);
        /*班级接口*/
        competitionClassPresenter.queryCompetitionClass(Token, teacherId);
        setDatas();
    }

    private void createTreeNodes(TreeNode treeNode, List<TreeBean> dataParentBeans) {
        for (TreeBean dataParentBean : dataParentBeans) {
            TreeNode node = initTreeNode(dataParentBean.getData().getName(), dataParentBean.getData().getId());
            treeNode.addChild(node);
            if (dataParentBean != null && !dataParentBean.getChildren().isEmpty()) {
                createTreeNodes(node, dataParentBean.getChildren(), dataParentBean.getData().getId());
            }
        }
    }

    private void createTreeNodes(TreeNode treeNode, List<TreeBean.ChildrenBean> dataChildBeans, String parentId) {
        for (TreeBean.ChildrenBean dataChildBean : dataChildBeans) {
            if (dataChildBean.getData().getParentId().equals(parentId)) {
                TreeNode node = initTreeNode(dataChildBean.getData().getName(), dataChildBean.getData().getId());
                treeNode.addChild(node);
                createTreeNodes(node, dataChildBean.getChildren(), dataChildBean.getData().getId());
            }
        }
    }

    //    每一条数据
    private TreeNode initTreeNode(String name, String id) {
        return new TreeNode(new IconTreeItem(R.drawable.ic_tree_wei, name, id))
                .setViewHolder(new ContentHolder(getActivity()));  //p 图标样式   p内容展示
    }


    private TreeNode.TreeNodeClickListener nodeClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            if (node.getChildren().size() > 0) {
                return;
            }

            if (myHolder != null) {
                myHolder.setUnselected();
            }
            myHolder = (ContentHolder) node.getViewHolder();
            myHolder.setSelected();

            chapterId = ((IconTreeItem) value).id;
            setDatas();
        }
    };

    @Override
    public void onTreeNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onTreeFail(String msg) {
        ToastUtils.showShort(msg);
    }

    /**
     * 收题
     */
    @Override
    public void onTeacherQuestionSuccess(Object object) {
        setDatas();
        ToastUtils.showShort(object.toString());
    }

    @Override
    public void onTeacherQuestionNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onTeacherQuestionFail(String msg) {
        ToastUtils.showShort(msg);
    }

    /**
     * 撤回
     */
    @Override
    public void onNowOutSuccess(Object object) {
        setDatas();
        ToastUtils.showShort(object.toString());
    }

    @Override
    public void onNowOutNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onNowOutFail(String msg) {
        ToastUtils.showShort(msg);
    }

    /**
     * 答案
     */
    @Override
    public void onSendAnswerOpenSuccess(Object object) {
        setDatas();
        ToastUtils.showShort(object.toString());
    }

    @Override
    public void onSendAnswerOpenNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onSendAnswerOpenFail(String msg) {
        ToastUtils.showShort(msg);
    }

    /**
     * 可见隐藏
     */

    @Override
    public void onSendIsHideSuccess(Object object) {
        setDatas();
    }

    @Override
    public void onSendIsHideNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onSendIsHideFail(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

}
