package com.example.app5home.classroomteaching.kc;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.sdzn.fzx.teacher.bean.ClassContentBean;

import java.util.List;

public class ClassSourseAdapter extends BaseQuickAdapter<ClassContentBean.SourseListBean, BaseViewHolder> {
    private boolean isPush = true;//是否显示发布按钮

    /**
     * 预览
     */
    public ClassSourseAdapter(List<ClassContentBean.SourseListBean> data, boolean isPush) {
        super(R.layout.item_class_sourse, data);
        this.isPush = isPush;
    }

    /**
     * 开始教学
     */
    public ClassSourseAdapter(List<ClassContentBean.SourseListBean> data) {
        super(R.layout.item_class_sourse, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassContentBean.SourseListBean item) {
        //1word2ppt3excel4pdf5视频6文本7图像8音频9试卷10备注

        if ("1".equals(item.getResourceType())) {//word
            helper.setImageResource(R.id.iv_icon, R.drawable.source_word);

        } else if ("2".equals(item.getResourceType())) {//ppt
            helper.setImageResource(R.id.iv_icon, R.drawable.source_ppt);
        } else if ("3".equals(item.getResourceType())) {//excel
            helper.setImageResource(R.id.iv_icon, R.drawable.source_excel);
        } else if ("4".equals(item.getResourceType())) {//pdf
            helper.setImageResource(R.id.iv_icon, R.drawable.source_pdf);
        } else if ("5".equals(item.getResourceType())) {//视频
            helper.setImageResource(R.id.iv_icon, R.drawable.source_video);
        } else if ("6".equals(item.getResourceType())) {//文本
            helper.setImageResource(R.id.iv_icon, R.drawable.source_txt);
        } else if ("7".equals(item.getResourceType())) {//图像
            helper.setImageResource(R.id.iv_icon, R.drawable.source_img);
        } else if ("8".equals(item.getResourceType())) {//音频
            helper.setImageResource(R.id.iv_icon, R.drawable.source_audio);
        } else if ("9".equals(item.getResourceType())) {//试卷
            helper.setImageResource(R.id.iv_icon, R.drawable.source_shijuan);
        } else if ("10".equals(item.getResourceType())) {//备注
            helper.setImageResource(R.id.iv_icon, R.drawable.source_beizhu);
        } else {

        }
        helper.setText(R.id.tv_content, item.getName());

        helper.setVisible(R.id.tv_sourse_push, isPush);
        if (isPush) {
            helper.addOnClickListener(R.id.tv_sourse_push);
        }


    }
}
