package com.example.app5home.ktjx;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;

/*
 * 备课记录首页
 * */
public class KtjcFragment extends BaseActFragment1 {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_ktjc;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
            Bundle bundle = this.getArguments();
            String target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(com.example.app5libbase.R.id.ll_base_container_ktjc);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }
}
