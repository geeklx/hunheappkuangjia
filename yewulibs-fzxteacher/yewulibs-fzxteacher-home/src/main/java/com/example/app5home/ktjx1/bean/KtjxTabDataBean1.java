package com.example.app5home.ktjx1.bean;

public class KtjxTabDataBean1 {
    private String tab_id;
    private String tab_name;
    private String url;
    private boolean enable;

    public KtjxTabDataBean1() {
    }

    public KtjxTabDataBean1(String tab_id, String tab_name, String url, boolean enable) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
        this.url = url;
    }


    public String getTab_id() {
        return tab_id;
    }

    public void setTab_id(String tab_id) {
        this.tab_id = tab_id;
    }

    public String getTab_name() {
        return tab_name;
    }

    public void setTab_name(String tab_name) {
        this.tab_name = tab_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

}
