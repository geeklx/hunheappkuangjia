package com.example.app5home.ktjx1.holder;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.app5home.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Bogdan Melnychuk on 2/13/15.
 */
public class ContentHolder extends TreeNode.BaseNodeViewHolder<IconTreeItem> {
    ImageView ivicon;
    LinearLayout llBg;
    TextView tvValue;


    public ContentHolder(Context context) {
        super(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View createNodeView(TreeNode node, IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_tree_content, null, false);
        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value.text);
        ivicon = (ImageView) view.findViewById(R.id.ivicon);
        ivicon.setImageResource(value.icon);//未展开

        if (node.isLeaf()) {
            ivicon.setVisibility(View.INVISIBLE);
        }

        int treeviewPadding = 25;
        int treeviewleft;
        treeviewleft = node.getLevel() * treeviewPadding;
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) llBg.getLayoutParams();
        llBg.setPadding(treeviewleft, 0, 0, 0);
        lp.weight = 500 - treeviewleft;

        view.setLayoutParams(lp);


        return view;
    }

    public TextView setSelected() {
        tvValue.setTextColor(Color.parseColor("#FA541C"));
        llBg.setBackgroundResource(R.drawable.bg_tree_select);
        return tvValue;
    }

    public TextView setUnselected() {
        tvValue.setTextColor(Color.parseColor("#A6000000"));
        tvValue.setBackgroundColor(Color.TRANSPARENT);
        llBg.setBackgroundColor(Color.TRANSPARENT);
        return tvValue;
    }





    @Override
    public void toggle(boolean active) {
        ivicon.setImageResource(active?R.drawable.ic_tree_xuan:R.drawable.ic_tree_wei);
    }

}
