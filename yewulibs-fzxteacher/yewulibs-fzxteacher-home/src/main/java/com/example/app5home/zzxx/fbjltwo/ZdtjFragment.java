package com.example.app5home.zzxx.fbjltwo;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.teacher.BuildConfig3;

/*
 * 发布记录作答统计
 * */
public class ZdtjFragment extends BaseActFragment1 {
    private RadioButton rbtongji, rbtimufengx, rbziliaofengx;
    private RadioGroup rgTongji;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_zdtj;
    }

    private String url, lessonId, lessonLibId, lessonTaskId, classesId, baseVolumeId;

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        rbtongji = rootView.findViewById(R.id.rbtongji);
        rbtimufengx = rootView.findViewById(R.id.rbtimufengx);
        rgTongji = rootView.findViewById(R.id.rg_tongji);
        rbziliaofengx = rootView.findViewById(R.id.rbziliaofengx);
        rbtongji.setChecked(true);
        Bundle bundle = this.getArguments();
        url = bundle.getString("url_key");
        lessonId = bundle.getString("lessonId");
        lessonLibId = bundle.getString("lessonLibId");
        lessonTaskId = bundle.getString("lessonTaskId");
        classesId = bundle.getString("classesId");
        baseVolumeId = bundle.getString("baseVolumeId");

        rgTongji.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbtongji) {
                    loadWebSite(url + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId); // 刷新
                } else if (checkedId == R.id.rbtimufengx) {
                    loadWebSite(BuildConfig3.SERVER_ISERVICE_NEW1 + "/courseTeach/workbench/topicAnalysis" + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                } else if (checkedId == R.id.rbziliaofengx) {
                    loadWebSite(BuildConfig3.SERVER_ISERVICE_NEW1 + "/courseTeach/workbench/materialAnalysis" + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId);
                }
            }
        });
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
            loadWebSite(url + "?lessonId=" + lessonId + "&lessonLibId=" + lessonLibId + "&lessonTaskId=" + lessonTaskId + "&classesId=" + classesId + "&baseVolumeId=" + baseVolumeId); // 刷新
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(com.example.app5libbase.R.id.ll_base_containe_zdtj);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }

}
