package com.example.app5home.ktjx1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter2;
import com.example.app5home.ktjx1.Interface.RiliChangeListener;
import com.example.app5home.ktjx1.Interface.RiliChangeListener2;
import com.example.app5home.ktjx1.Interface.RiliChangeListener3;
import com.example.app5home.ktjx1.bean.KtjxTabBean;
import com.example.app5home.ktjx1.bean.KtjxTabCountBean;
import com.example.app5home.ktjx1.bean.KtjxTabDataBean1;
import com.example.app5home.ktjx1.skjl.SkjlFragment1;
import com.example.app5home.ktjx1.ktcj.KtjcFragment1;
import com.example.app5home.ktjx1.jxkc.JxkcFragment1;
import com.example.app5libbase.R;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;

import java.util.ArrayList;
import java.util.List;

/*
 * 课堂教学主页
 * */
public class KtjxShouyeActivity1 extends BaseActWebActivity1 implements BaseOnClickListener {//, TabViews
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter2 mAdapter11;
    private List<KtjxTabDataBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private int defCurrentItem = 0;//设置默认见面
    private int tabItemid = 0;
    public static final String URL_KEY = "url_key";
    //    TabPresenter tabPresenter;
    private String type = "4";//查询


    @Override
    protected int getLayoutId() {
        return R.layout.activity_ktjxshouye;
    }


    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);

        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        donetwork();
    }

    /* 重载业务部分*/
    public void donetwork() {
        if ("1".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 0;
        } else if ("2".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 1;
        }
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        tvTitleName.setText("课堂教学");
//        tvSousuoTitle.setVisibility(View.GONE);
//        tvWeizhi.setVisibility(View.GONE);
        onclick();
        setData();
//        tabPresenter = new TabPresenter();
//        tabPresenter.onCreate(this);
//        tabPresenter.querytab(type);
    }

    //    List<RecyclerTabBean.ListBean> recyclerTabBeans
    private void setData() {
        KtjxTabBean ktjxTabBean = new KtjxTabBean();
        List<KtjxTabCountBean> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new KtjxTabCountBean("1", "教学课程", ""));//courseTeach/courseTeachList
        mDataTablayout1.add(new KtjxTabCountBean("2", "上课记录", ""));//courseTeach/classRecord
        mDataTablayout1.add(new KtjxTabCountBean("3", "课堂检测", ""));//courseTeach/workbenchRecord
        ktjxTabBean.setList(mDataTablayout1);

        mDataTablayout = new ArrayList<>();

        for (int i = 0; i < ktjxTabBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new KtjxTabDataBean1(ktjxTabBean.getList().get(i).getId(), ktjxTabBean.getList().get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + ktjxTabBean.getList().get(i).getUrl(), true));
            } else {
                mDataTablayout.add(new KtjxTabDataBean1(ktjxTabBean.getList().get(i).getId(), ktjxTabBean.getList().get(i).getName(), BuildConfig3.SERVER_ISERVICE_NEW1 + ktjxTabBean.getList().get(i).getUrl(), false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
    }

    private void onclick() {
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter2();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                KtjxTabDataBean1 bean1 = (KtjxTabDataBean1) adapter.getData().get(position);
                current_id = bean1.getTab_id();
                if (null == current_id) {
                    return;
                }
                tabItemid = position;
              /*  if (position == 0) {
                    tvSousuoTitle.setVisibility(View.GONE);
                } else if(position==1){
                    tvSousuoTitle.setVisibility(View.VISIBLE);
                }else {
                    tvSousuoTitle.setVisibility(View.VISIBLE);
                }*/
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });

        BaseActWebActivity1.setOnDisplayRefreshListener(new BaseActWebActivity1.refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
//                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                KtjxTabDataBean1 ktjxTabDataBean1 = new KtjxTabDataBean1();
                ktjxTabDataBean1.setTab_id("开始时间-" + startTime);
                ktjxTabDataBean1.setTab_name("结束时间-" + endTime);
                SendToFragment(ktjxTabDataBean1);
            }
        });
    }


    private void init_viewp(List<KtjxTabDataBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                JxkcFragment1 fragment1 = FragmentHelper.newFragment(JxkcFragment1.class, bundle);
                riliChangeListener = (RiliChangeListener) fragment1;
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                SkjlFragment1 fragment2 = FragmentHelper.newFragment(SkjlFragment1.class, bundle);
                riliChangeListener2 = (RiliChangeListener2) fragment2;
                mFragmentList.add(fragment2);
            } else {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                KtjcFragment1 fragment3 = FragmentHelper.newFragment(KtjcFragment1.class, bundle);
                riliChangeListener3 = (RiliChangeListener3) fragment3;
                mFragmentList.add(fragment3);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(2);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabItemid = position;
               /* if (position == 0) {
                    tvSousuoTitle.setVisibility(View.GONE);
                } else if(position==1){
                    tvSousuoTitle.setVisibility(View.VISIBLE);
                }else {
                    tvSousuoTitle.setVisibility(View.VISIBLE);
                }*/
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        KtjxTabDataBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            KtjxTabDataBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(KtjxTabDataBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
//        tabPresenter.onDestory();
        super.onDestroy();
    }

    @Override
    public void Titleshijian() {
        showCalendarDialog();
    }

    /*展开时间*/
    @Override
    public void Titlezankaishijian() {
        showCalendarDialog();
    }

    private RiliChangeListener riliChangeListener;
    private RiliChangeListener2 riliChangeListener2;
    private RiliChangeListener3 riliChangeListener3;

    /**
     * 页面传值操作部分
     */
    private void SendToFragment(Object msg) {
        //举例
        if (this != null && this instanceof KtjxShouyeActivity1) {
//            this.callFragment(msg, SkjlFragment1.class.getName());
            if (riliChangeListener != null && TextUtils.equals("1", current_id)) {
                riliChangeListener.OnSuccess(msg);
            }
            if (riliChangeListener2 != null && TextUtils.equals("2", current_id)) {
                riliChangeListener2.OnSuccess(msg);
            }
            if (riliChangeListener3 != null && TextUtils.equals("3", current_id)) {
                riliChangeListener3.OnSuccess(msg);
            }


        }
    }

    public void Titlesousuo() {
        if (tabItemid == 1) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.SearchActivity"));
            startActivity(intent);
            ToastUtils.showLong("上课记录搜索");
        } else if (tabItemid == 2) {
            ToastUtils.showLong("课堂检测搜索");
        }
    }

//    /**
//     * fragment间通讯bufen
//     *
//     * @param value 要传递的值
//     * @param tag   要通知的fragment的tag
//     */
//    public void callFragment(Object value, String... tag) {
//        FragmentManager fm = getSupportFragmentManager();
//        Fragment fragment;
//        for (String item : tag) {
//            if (TextUtils.isEmpty(item)) {
//                continue;
//            }
//            fragment = fm.findFragmentByTag(item);
//            if (fragment instanceof BaseActFragment1) {
//                ((BaseActFragment1) fragment).call(value);
//            }
//        }
//    }

//    @Override
//    public void onTabSuccess(RecyclerTabBean recyclerTabBean) {
//        tvSousuoTitle.setVisibility(View.GONE);
//        tvWeizhi.setVisibility(View.GONE);
//        setData(recyclerTabBean.getList());
//    }
//
//    @Override
//    public void onTabNodata(String msg) {
//        ToastUtils.showLong(msg);
//    }
//
//    @Override
//    public void onTabFail(String msg) {
//        ToastUtils.showLong(msg);
//    }
}