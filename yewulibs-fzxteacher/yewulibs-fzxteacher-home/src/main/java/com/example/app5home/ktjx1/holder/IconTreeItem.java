package com.example.app5home.ktjx1.holder;

public class IconTreeItem {
    public int icon;
    public String text;
    public String id;

    public IconTreeItem(int icon, String text,String id) {
        this.icon = icon;
        this.text = text;
        this.id = id;
    }
}
