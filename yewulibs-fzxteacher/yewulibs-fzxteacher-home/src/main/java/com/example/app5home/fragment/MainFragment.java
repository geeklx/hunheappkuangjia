package com.example.app5home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libpublic.event.ToLessonEvent;
import com.example.app5libpublic.event.ToTaskEvent;
import com.example.app5libbase.listener.OnClassClickListener;
import com.example.app5libbase.listener.OnItemTouchListener;
import com.example.app5libbase.pop.ClassPop;
import com.example.app5libbase.baseui.presenter.MainPresenter;
import com.example.app5libbase.baseui.view.MainView;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.example.app5libbase.views.EmptyRecyclerView;
import com.example.app5libbase.views.GraphView;
import com.example.app5libbase.views.MainFragmentTitleView;
import com.example.app5libbase.views.RankView;
import com.sdzn.fzx.teacher.vo.CorrectVo;
import com.sdzn.fzx.teacher.vo.LessonListVo;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.TaskListVo;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class MainFragment extends MBaseFragment<MainPresenter> implements MainView, OnRefreshListener, View.OnClickListener {

    private SmartRefreshLayout refreshLayout;
    private LinearLayout swipeTarget;
    private MainFragmentTitleView mftvTask;
    private EmptyRecyclerView rvTask;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private MainFragmentTitleView mftvLesson;
    private EmptyRecyclerView rvLesson;
    private LinearLayout llLessonEmpty;
    private ImageView ivLessonEmpty;
    private TextView tvLessonEmpty;
    private TextView mftvRank;
    private TextView tvClass1;
    private RankView rankView;
    private TextView tvClass2;
    private GraphView graphView;


    private Y_ItemEntityList itemEntityListTask = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter taskAdapter;

    private Y_ItemEntityList itemEntityListLesson = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter lessonAdapter;

    private ClassPop classPop;

    private SyncClassVo syncClassVo;

    public static MainFragment newInstance(Bundle bundle) {
        MainFragment fragment = new MainFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new MainPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        swipeTarget = (LinearLayout) rootView.findViewById(R.id.swipe_target);
        mftvTask = (MainFragmentTitleView) rootView.findViewById(R.id.mftvTask);
        rvTask = (EmptyRecyclerView) rootView.findViewById(R.id.rvTask);
        llTaskEmpty = (LinearLayout) rootView.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) rootView.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) rootView.findViewById(R.id.tvTaskEmpty);
        mftvLesson = (MainFragmentTitleView) rootView.findViewById(R.id.mftvLesson);
        rvLesson = (EmptyRecyclerView) rootView.findViewById(R.id.rvLesson);
        llLessonEmpty = (LinearLayout) rootView.findViewById(R.id.llLessonEmpty);
        ivLessonEmpty = (ImageView) rootView.findViewById(R.id.ivLessonEmpty);
        tvLessonEmpty = (TextView) rootView.findViewById(R.id.tvLessonEmpty);
        mftvRank = (TextView) rootView.findViewById(R.id.mftvRank);
        tvClass1 = (TextView) rootView.findViewById(R.id.tvClass1);
        rankView = (RankView) rootView.findViewById(R.id.rankView);
        tvClass2 = (TextView) rootView.findViewById(R.id.tvClass2);
        graphView = (GraphView) rootView.findViewById(R.id.graphView);
        tvClass1.setOnClickListener(this);
        tvClass2.setOnClickListener(this);
        mftvTask.setOnClickListener(this);
        mftvLesson.setOnClickListener(this);
        initView();
        return rootView;
    }

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));

        ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.unf_empty));
        tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text2));
        rvTask.setEmptyView(llTaskEmpty);
        LinearLayoutManager linearLayoutManagerTask = new LinearLayoutManager(App2.get());
        rvTask.setLayoutManager(linearLayoutManagerTask);

        taskAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityListTask);
        rvTask.setAdapter(taskAdapter);

        rvTask.addOnItemTouchListener(new OnItemTouchListener(rvTask) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                if (vh.getAdapterPosition() < 0 || vh.getAdapterPosition() > itemEntityListTask.getItemCount()) {
                    return;
                }
                TaskListVo.DataBean itemData = (TaskListVo.DataBean) itemEntityListTask.getItemData(vh.getAdapterPosition());
                Intent answerIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectNativeActivity");//20190129 todo
                Bundle bundle = new Bundle();
                bundle.putString("name", itemData.getName());
                bundle.putString("classId", String.valueOf(itemData.getClassId()));
                bundle.putString("id", String.valueOf(itemData.getId()));
                bundle.putString("type", "transcript");
                bundle.putString("teacherExamId", itemData.getTeacherExamId() == null ? "" : itemData.getTeacherExamId());
                answerIntent.putExtras(bundle);
                startActivity(answerIntent);
            }
        });

        ivLessonEmpty.setBackground(getResources().getDrawable(R.mipmap.f_empty));
        tvLessonEmpty.setText(getResources().getString(R.string.fragment_main_text3));
        rvLesson.setEmptyView(llLessonEmpty);
        LinearLayoutManager linearLayoutManagerLesson = new LinearLayoutManager(App2.get());
        rvLesson.setLayoutManager(linearLayoutManagerLesson);

        lessonAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityListLesson);
        rvLesson.setAdapter(lessonAdapter);

        rvLesson.addOnItemTouchListener(new OnItemTouchListener(rvLesson) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                if (vh.getAdapterPosition() < 0 || vh.getAdapterPosition() > itemEntityListLesson.getItemCount()) {
                    return;
                }
                LessonListVo.DataBean dataBean = (LessonListVo.DataBean) itemEntityListLesson.getItemData(vh.getAdapterPosition());
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LessonDetailsFragment");
                intent.putExtra("lessonId", dataBean.getId() + "");
                intent.putExtra("lessonName", dataBean.getName() + "");
                intent.putExtra("titleName", dataBean.getName() + "");
                startActivity(intent);
            }
        });

        getTaskList();
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        getTaskList();
    }

    private void getTaskList() {
        mPresenter.getTaskList();
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvClass1) {
            showPop(tvClass1, new OnClassClickListener() {
                @Override
                public void onClassClick(SyncClassVo.DataBean dataBean) {
                    tvClass1.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
                    mPresenter.getRank(dataBean.getClassId());
                }
            });
        } else if (id == R.id.tvClass2) {
            showPop(tvClass2, new OnClassClickListener() {
                @Override
                public void onClassClick(SyncClassVo.DataBean dataBean) {
                    tvClass2.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
                    mPresenter.getCorrect(dataBean.getClassId());
                }
            });
        } else if (id == R.id.mftvTask) {
            EventBus.getDefault().post(new ToTaskEvent());
        } else if (id == R.id.mftvLesson) {
            EventBus.getDefault().post(new ToLessonEvent());
        }
    }

    private void showPop(View view, OnClassClickListener onClassClickListener) {
        if (classPop == null) {
            classPop = new ClassPop(activity, onClassClickListener);
        } else {
            classPop.setOnClassClickListener(onClassClickListener);
        }
        if (syncClassVo != null) {
            classPop.setClassVos(syncClassVo.getData());
        }
        classPop.showPopupWindow(view);
    }

    @Override
    public void getTaskListSuccess(TaskListVo taskListVo) {
        itemEntityListTask.clear();
        if (taskListVo != null && taskListVo.getData() != null && taskListVo.getData().size() > 0) {
            itemEntityListTask.addItems(R.layout.item_fragment_main_task, taskListVo.getData())
                    .addOnBind(R.layout.item_fragment_main_task, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindTaskHolder(rvTask.getHeight(), holder, (TaskListVo.DataBean) itemData);
                        }
                    });
            taskAdapter.notifyDataSetChanged();
            mftvTask.setNum(taskListVo.getTotal());
        } else {
            ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.unf_empty));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text2));
            rvTask.setEmptyView(llTaskEmpty);
        }
    }

    @Override
    public void getLessonListSuccess(LessonListVo lessonListVo) {
        itemEntityListLesson.clear();
        if (lessonListVo != null && lessonListVo.getData() != null && lessonListVo.getData().size() > 0) {
            itemEntityListLesson.addItems(R.layout.item_fragment_main_lesson, lessonListVo.getData())
                    .addOnBind(R.layout.item_fragment_main_lesson, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindLessonHolder(rvLesson.getHeight(), holder, (LessonListVo.DataBean) itemData);
                        }
                    });
            lessonAdapter.notifyDataSetChanged();
            mftvLesson.setNum(lessonListVo.getTotal());
        } else {
            ivLessonEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.unf_empty));
            tvLessonEmpty.setText(getResources().getString(R.string.fragment_main_text3));
            rvLesson.setEmptyView(llLessonEmpty);
        }
    }

    @Override
    public void getClassVoSuccess(SyncClassVo syncClassVo) {
        if (syncClassVo != null && syncClassVo.getData() != null && syncClassVo.getData().size() > 0) {
            this.syncClassVo = syncClassVo;
            tvClass1.setText(syncClassVo.getData().get(0).getBaseGradeName() + syncClassVo.getData().get(0).getClassName());
            tvClass2.setText(syncClassVo.getData().get(0).getBaseGradeName() + syncClassVo.getData().get(0).getClassName());
        }
    }

    @Override
    public void getRankSuccess(RankVo rankVo) {
        for (int i = 0; i < 3; i++) {
            String groupName = null;
            if (rankVo != null && rankVo.getData() != null && rankVo.getData().size() > i) {
                RankVo.DataBean dataBean = rankVo.getData().get(i);
                groupName = dataBean.getClassGroupName();
            }
            if (i == 0) {
                rankView.setGroup1(groupName);
            } else if (i == 1) {
                rankView.setGroup2(groupName);
            } else {
                rankView.setGroup3(groupName);
            }
        }
    }

    @Override
    public void getCorrectSuccess(CorrectVo correctVo) {
        // 填充图表数据
        if (correctVo != null && correctVo.getData() != null) {
            List<GraphView.LineViewBean> mDatas = new ArrayList<>();
            if (correctVo.getData().getCorrectList() != null) {
                GraphView.LineViewBean lineViewBean = new GraphView.LineViewBean();
                ArrayList<GraphView.ItemInfoVo> list = new ArrayList<>();
                for (CorrectVo.DataBean.CorrectListBean correctListBean : correctVo.getData().getCorrectList()) {
                    GraphView.ItemInfoVo vo = new GraphView.ItemInfoVo();
                    vo.setValue(correctListBean.getRate() / 100);
//                    vo.setValue((float) Math.random());
                    if (DateUtil.differentDays(correctListBean.getDay(), System.currentTimeMillis()) == 0) {
                        vo.setName("今日");
                    } else if (DateUtil.getMinMonthDate(correctListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "yyyy-M-d"))
                            || DateUtil.getMaxMonthDate(correctListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "yyyy-M-d"))) {
                        vo.setName(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "d"));
                        vo.setUnit(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "M") + "月");
                    } else {
                        vo.setName(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "d"));
                    }
                    vo.setTime(DateUtil.getTimeStrByTimemillis(correctListBean.getDay(), "yyyy-M-d"));
                    list.add(vo);
                }
                lineViewBean.setItemVo(list);
                lineViewBean.setColor(getResources().getColor(R.color.graphview_correct));
                lineViewBean.setShadowColor(new int[]{getResources().getColor(R.color.graphview_correct_bg), 0x00FFFFFF});
                mDatas.add(lineViewBean);
            }
            if (correctVo.getData().getCompletionList() != null) {
                GraphView.LineViewBean lineViewBean = new GraphView.LineViewBean();
                ArrayList<GraphView.ItemInfoVo> list = new ArrayList<>();
                for (CorrectVo.DataBean.CompletionListBean completionListBean : correctVo.getData().getCompletionList()) {
                    GraphView.ItemInfoVo vo = new GraphView.ItemInfoVo();
                    vo.setValue(completionListBean.getRate() / 100);
//                    vo.setValue((float) Math.random());
                    if (DateUtil.differentDays(completionListBean.getDay(), System.currentTimeMillis()) == 0) {
                        vo.setName("今日");
                    } else if (DateUtil.getMinMonthDate(completionListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "yyyy-M-d"))
                            || DateUtil.getMaxMonthDate(completionListBean.getDay(), "yyyy-M-d")
                            .equals(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "yyyy-M-d"))) {
                        vo.setName(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "d"));
                        vo.setUnit(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "M") + "月");
                    } else {
                        vo.setName(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "d"));
                    }
                    vo.setTime(DateUtil.getTimeStrByTimemillis(completionListBean.getDay(), "yyyy-M-d"));
                    list.add(vo);
                }
                lineViewBean.setItemVo(list);
                lineViewBean.setColor(getResources().getColor(R.color.graphview_completion));
                lineViewBean.setShadowColor(new int[]{getResources().getColor(R.color.graphview_completion_bg), 0x00FFFFFF});
                mDatas.add(lineViewBean);
            }
            graphView.setDatas(mDatas, false);
        }
    }

    @Override
    public void getTaskListFailure() {
        ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.no_data));
        tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text8));
        rvTask.setEmptyView(llTaskEmpty);
    }

    @Override
    public void getLessonListFailure() {
        ivLessonEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.no_data));
        tvLessonEmpty.setText(getResources().getString(R.string.fragment_main_text8));
        rvLesson.setEmptyView(llLessonEmpty);
    }

    @Override
    public void networkError(String msg) {
        cancilLoadState();
    }

    @Override
    public void onCompleted() {
        cancilLoadState();
    }
}
