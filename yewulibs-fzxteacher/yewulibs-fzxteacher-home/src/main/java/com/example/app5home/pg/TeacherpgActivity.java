package com.example.app5home.pg;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.example.app5home.AndroidInterface;
import com.example.app5libbase.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

/*
 * 教师端批改首页
 * */
public class TeacherpgActivity extends BaseActWebActivity1 implements BaseOnClickListener {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_teacher_pg;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        TitleShowHideState(1);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            String target = getIntent().getStringExtra(URL_KEY);
            loadWebSite(target); // 刷新
        }
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "批改");
    }
}