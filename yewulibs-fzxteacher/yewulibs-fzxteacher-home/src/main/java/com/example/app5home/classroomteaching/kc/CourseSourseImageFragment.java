package com.example.app5home.classroomteaching.kc;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.app5libbase.util.ImageLoaderOptions;
import com.example.app5libbase.util.chatroom.GlideHelper;

import uk.co.senab.photoview.PhotoView;

public class CourseSourseImageFragment extends BaseActFragment1 {
    private PhotoView ivDisplay;
    public static CourseSourseImageFragment newInstance(Bundle bundle) {
        CourseSourseImageFragment fragment = new CourseSourseImageFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_course_sourse_img;
    }


    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        ivDisplay = rootView.findViewById(R.id.iv_display);
        Bundle bundle = this.getArguments();
        String photoUrl = bundle.getString("img_url");


        RequestOptions options = new RequestOptions()
                .error(com.example.app5libbase.R.mipmap.error_image)
                .placeholder(com.example.app5libbase.R.mipmap.img_placeholder);
        Glide.with(getActivity())
                .load(photoUrl)
                .apply(options)
                .into(ivDisplay);

    }


    @Override
    protected ViewGroup getAgentWebParent() {

        return null;

    }


}
