package com.example.app5home.classroomteaching.kc;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;

public class CourseSourseOw365Fragment extends BaseActFragment1 {

    public static CourseSourseOw365Fragment newInstance(Bundle bundle) {
        CourseSourseOw365Fragment fragment = new CourseSourseOw365Fragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_course_sourse;
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
//            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
            Bundle bundle = this.getArguments();
            String target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
        }
        super.getJsInterface();
    }
    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);


    }


    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(R.id.ll_container_sourse);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (KeyEvent.KEYCODE_BACK == keyCode) {
//            if (mAgentWeb.getWebCreator().getWebView().canGoBack()) {
//                mAgentWeb.getWebCreator().getWebView().goBack();
//                return false;
//            }
//            back();
//            return false;
//        }
//        return superonKeyDown(keyCode, event);
//    }

    private void back() {
//        Intent intent = new Intent();
//        intent.putExtra("time", (System.currentTimeMillis() - startTime) / 1000);
//        setResult(Activity.RESULT_OK, intent);
//        finish();
//        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }
}
