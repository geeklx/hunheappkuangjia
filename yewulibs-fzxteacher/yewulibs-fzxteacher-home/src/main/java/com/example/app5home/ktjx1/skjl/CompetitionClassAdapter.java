package com.example.app5home.ktjx1.skjl;

import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.example.app5home.ktjx1.bean.CompetitionClassDataBindingBean;

public class CompetitionClassAdapter extends BaseQuickAdapter<CompetitionClassDataBindingBean, BaseViewHolder> {

    public CompetitionClassAdapter() {
        super(R.layout.recycleview_competition_class_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, CompetitionClassDataBindingBean item) {
        TextView tvClassName = helper.itemView.findViewById(R.id.tv_class_name);//班级名称
        tvClassName.setText(item.getClassesName());
        if (item.isEnable()){//选中
            tvClassName.setTextColor(ContextCompat.getColor(mContext, R.color.colorFA541C));
        }else{
            tvClassName.setTextColor(ContextCompat.getColor(mContext, R.color.colorff000000));
        }
    }
}
