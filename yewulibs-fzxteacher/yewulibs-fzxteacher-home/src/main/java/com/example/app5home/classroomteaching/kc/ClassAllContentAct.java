package com.example.app5home.classroomteaching.kc;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.R;
import com.example.app5libbase.ai.pop.QuestionsReleasePopupView;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.lxj.xpopup.XPopup;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.bean.StartStudyBean;
import com.sdzn.fzx.teacher.presenter.ResourcesPresenter;
import com.sdzn.fzx.teacher.presenter.StartStudyPresenter;
import com.sdzn.fzx.teacher.presenter.TeachingCoursesContentPresenter;
import com.sdzn.fzx.teacher.view.ResourcesViews;
import com.sdzn.fzx.teacher.view.StartStudyViews;
import com.sdzn.fzx.teacher.view.TeachingCoursesContentViews;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ClassAllContentAct extends BaseActWebActivity1 implements BaseOnClickListener, TeachingCoursesContentViews, ResourcesViews, StartStudyViews, View.OnClickListener {
    TeachingCoursesContentPresenter contentPresenter;
    ResourcesPresenter resourcesPresenter;
    private StartStudyPresenter startStudyPresenter;//开始教学
    ClassContentAdapter classContentAdapter;
    private RecyclerView rvContent;
    ClassSourseAdapter classSourseAdapter;
    private RecyclerView rvSourse;
    private TextView tvChat;//合作探究
    public static final String URL_KEY = "url_key";
    public static final String URL_IMG = "img_url";
    public static final String URL_PLAYER = "player_url";
    private String lessonId;//456  452   692
    private String courseName;
    private String classesId;
    private String lessonTaskId;
    QuestionsReleasePopupView questionsReleasePopupView;

    private String baseVolumeId;
    private String chapterId;
    private String lessonLibId;
    private String subjectId;
    private String chapterName;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_all_content;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        lessonId = getIntent().getStringExtra("lessonId");
        courseName = getIntent().getStringExtra("courseName");
        classesId = getIntent().getStringExtra("classesId");

        rvSourse = findViewById(R.id.rv_sourse);
        rvContent = findViewById(R.id.rv_content);
        tvChat = findViewById(R.id.tv_chat);
        TitleShowHideState(5);
        setBaseOnClickListener(this);
        tvTitleName.setText(courseName + "");
        rvContent.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvSourse.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        tvChat.setOnClickListener(this);

        contentPresenter = new TeachingCoursesContentPresenter();
        contentPresenter.onCreate(this);
        resourcesPresenter = new ResourcesPresenter();
        resourcesPresenter.onCreate(this);
        startStudyPresenter = new StartStudyPresenter();
        startStudyPresenter.onCreate(this);

        contentPresenter.queryCoursesContent("Bearer " + (String) SPUtils.getInstance().getString("token", ""), lessonId);

        startStudyPresenter.addStartStudy("Bearer " + (String) SPUtils.getInstance().getString("token", ""), classesId, lessonId, SPUtils.getInstance().getString("shouye_class_id"));
        tvViewStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.JxkcTjActivity"));
                intent.putExtra("lessonLibId", lessonLibId);
                intent.putExtra("lessonId", lessonId);
                intent.putExtra("lessonTaskId", lessonTaskId);
                intent.putExtra("courseName", courseName);
                intent.putExtra("classesId", classesId);
                intent.putExtra("baseVolumeId", baseVolumeId);
                startActivity(intent);
            }
        });
    }

    /**
     * 右侧数据返回处理
     */

    @Override
    public void onCoursesContentSuccess(ClassContentBean classContentBean) {

        setSourseData(classContentBean);
        setContentData(classContentBean);

        transactionContent = getSupportFragmentManager().beginTransaction();
        if (sourseListFragment.size() > 0) {
            transactionContent.replace(R.id.fl_fragment, sourseListFragment.get(0));
        } else if (contentListFragment.size() > 0) {
            transactionContent.replace(R.id.fl_fragment, contentListFragment.get(0));
        }
        transactionContent.commit();

    }

    List<Fragment> contentListFragment = new ArrayList<>();
    FragmentTransaction transactionContent;
    List<ClassContentBean> classContentBeans;

    /**
     * 右侧课堂检测数据处理
     */
    private void setContentData(ClassContentBean data) {
        contentListFragment.clear();
        classContentBeans = new ArrayList<>();
        classContentBeans.add(data);
        if (data.getTestIngList() == null || data.getTestIngList().size() < 0) {
            return;
        }

        //数据
        final List<libDetailListBean> dList = new ArrayList<>();
        for (int i = 0; i < data.getTestIngList().size(); i++) {
            dList.add(new libDetailListBean(data.getTestIngList().get(i).getId(), data.getTestIngList().get(i).getName(),
                    data.getTestIngList().get(i).getType()));
        }

        //题型及对应数量
        Map<String, List<ContentTestNumBean>> testNumMap = new HashMap<>();
        for (int i = 0; i < data.getTestIngList().size(); i++) {
            final List<libDetailListBean> dList1 = new ArrayList<>();

            for (int a = 0; a < data.getTestIngList().get(i).getLibCourseDetailList().size(); a++) {
                dList1.add(new libDetailListBean(String.valueOf(i), "1",
                        "0", data.getTestIngList().get(i).getLibCourseDetailList().get(a).getSeq(),
                        data.getTestIngList().get(i).getLibCourseDetailList().get(a).getExamTemplateStyleName()));//此处id 用来判断是同一个课堂检测
            }

            Map<String, List<libDetailListBean>> listMap = detaiList(dList1);
            dList1.clear();
            List<ContentTestNumBean> numBeanList = new ArrayList<>();
            for (Map.Entry<String, List<libDetailListBean>> entry : listMap.entrySet()) {
                numBeanList.add(new ContentTestNumBean(entry.getValue().get(0).getExamTemplateStyleName(), entry.getValue().size() + ""));
            }
            testNumMap.put(i + "", numBeanList);
        }


        classContentAdapter = new ClassContentAdapter(this, dList);
        rvContent.setAdapter(classContentAdapter);

        for (int i = 0; i < data.getTestIngList().size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("courseDetailTest", data.getTestIngList().get(i));
//            contentListFragment.add(CourseContentFragment.newInstance(bundle));
            bundle.putSerializable("testNumList", (Serializable) testNumMap.get(i + ""));
            contentListFragment.add(CourseContentFragment.newInstance(bundle));
        }
        classContentAdapter.addRecycleItemListener(listener);
    }

    private LinkedHashMap<String, List<libDetailListBean>> detaiList(List<libDetailListBean> list) {
        LinkedHashMap<String, List<libDetailListBean>> map = new LinkedHashMap<String, List<libDetailListBean>>();
        for (libDetailListBean bean : list) {
            if (map.containsKey(bean.getExamTemplateStyleName())) {
                List<libDetailListBean> subList = map.get(bean.getExamTemplateStyleName());
                subList.add(bean);
            } else {
                List<libDetailListBean> subList = new ArrayList<libDetailListBean>();
                subList.add(bean);
                map.put(bean.getExamTemplateStyleName(), subList);
            }
        }
        return map;
    }

    /**
     * 右侧课堂检测监听
     */
    private int posContent;//展示第几个contentListFragment
    ClassContentAdapter.OnRecycleItemListener<libDetailListBean> listener = new ClassContentAdapter.OnRecycleItemListener<libDetailListBean>() {
        @Override
        public void OnRecycleItemClick(View v, int position, libDetailListBean o) {
            int vId = v.getId();
            if (R.id.tv_content == vId) {
                if (position < contentListFragment.size() && posContent != position) {
                    Log.e("eeee", "课堂检测");
                    posContent = position;
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.replace(R.id.fl_fragment, contentListFragment.get(position));
                    trans.commit();
                } else if (contentListFragment.size() == 1) {
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.replace(R.id.fl_fragment, contentListFragment.get(posContent));
                    trans.commit();
                }
            } else if (R.id.tv_content_push == vId) {
                for (int i = 0; i < classContentBeans.size(); i++) {
                    if (classContentBeans.get(i).getTestIngList().get(position).getLibCourseDetailList().size() <= 0) {
                        ToastUtils.showShort("请在该课堂检测模块下添加试题");
                    } else {
                        String lessonLibCourseId = o.getId();
                        questionsReleasePopupView = new QuestionsReleasePopupView(ClassAllContentAct.this, getClassesId(), lessonLibCourseId, lessonTaskId);
                        new XPopup.Builder(ClassAllContentAct.this)
                                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                                .asCustom(questionsReleasePopupView)
                                .show();
                    }
                }
//                ToastUtils.showShort("课堂检测发布--" + o.getId());
            }
        }
    };


    List<Fragment> sourseListFragment = new ArrayList<>();

    /**
     * 右侧资源数据处理
     */

    private void setSourseData(final ClassContentBean data) {
        sourseListFragment.clear();
        classSourseAdapter = new ClassSourseAdapter(data.getSourseList());
        rvSourse.setAdapter(classSourseAdapter);

        for (int i = 0; i < data.getSourseList().size(); i++) {
            Bundle bundle = new Bundle();
            if ("7".equals(data.getSourseList().get(i).getResourceType()) || data.getSourseList().get(i).getResourceUrl().endsWith(".png")
                    || data.getSourseList().get(i).getResourceUrl().endsWith(".jpeg") || data.getSourseList().get(i).getResourceUrl().endsWith(".jpg")
                    || data.getSourseList().get(i).getResourceUrl().endsWith(".gif") || data.getSourseList().get(i).getResourceUrl().endsWith(".GIF")) {//图像
                bundle.putString(URL_IMG, data.getSourseList().get(i).getResourceUrl());
                sourseListFragment.add(CourseSourseImageFragment.newInstance(bundle));
            }
//            else if ("5".equals(data.getSourseList().get(i).getResourceType()) || data.getSourseList().get(i).getResourceUrl().endsWith(".mp4")) {//视频
//                bundle.putString(URL_PLAYER, data.getSourseList().get(i).getResourceUrl());
//                sourseListFragment.add(CourseSourseVideoFragment.newInstance(bundle));
//            }
            else if(data.getSourseList().get(i).getResourceUrl().endsWith(".mp4")||data.getSourseList().get(i).getResourceUrl().endsWith(".mpg")||data.getSourseList().get(i).getResourceUrl().endsWith(".3gp")||data.getSourseList().get(i).getResourceUrl().endsWith(".wmv")){
                bundle.putString(URL_PLAYER, data.getSourseList().get(i).getConvertResourceUrl());
                sourseListFragment.add(CourseSourseVideoFragment.newInstance(bundle));
            } else if ("8".equals(data.getSourseList().get(i).getResourceType()) || data.getSourseList().get(i).getResourceUrl().endsWith(".mp3")) {//音频
                bundle.putString("courseName",data.getCourseName()+"");
                bundle.putString("objectiveName",data.getObjectiveName()+"");
                bundle.putString(URL_PLAYER, data.getSourseList().get(i).getResourceUrl());
                sourseListFragment.add(CourseSourseRadioFragment.newInstance(bundle));
            } else{
                bundle.putString(URL_KEY, "https://ow365.cn/?i=" + BuildConfig3.OFFICE_ID + "&n=5&furl=" + data.getSourseList().get(i).getResourceUrl());
                sourseListFragment.add(CourseSourseOw365Fragment.newInstance(bundle));
            }
        }


        classSourseAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position < sourseListFragment.size()) {
                    Log.e("eeee", "资源文件");
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.replace(R.id.fl_fragment, sourseListFragment.get(position));
                    trans.commit();
                }
            }
        });

        classSourseAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (R.id.tv_sourse_push == view.getId()) {
                    String lessonLibCourseId = data.getSourseList().get(position).getId();
                    resourcesPresenter.AddResources("Bearer " + (String) SPUtils.getInstance().getString("token", ""), getClassesId(), lessonLibCourseId, lessonTaskId, "", "", "");
//                    ToastUtils.showShort("资源文件发布--" + data.getSourseList().get(position).getId() + "------" + student1.toString() + "------" + jsonOutput.toString());
                }
            }
        });
    }

    public String getClassesId() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("classId", classesId);
        jsonArray.add(jsonObject);
        String jsonOutput = jsonArray.toJSONString();
        return jsonOutput;
    }

    @Override
    public void onCoursesContentNodata(String msg) {

    }

    @Override
    public void onCoursesContentFail(String msg) {

    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (R.id.tv_chat == viewId) {//创建合作探究
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.HzTjReleaseActivity");
            intent.putExtra("classesId", classesId);
            intent.putExtra("baseVolumeId", baseVolumeId);
            intent.putExtra("chapterId", chapterId);
            intent.putExtra("lessonLibId", lessonLibId);
            intent.putExtra("subjectId", subjectId);
            intent.putExtra("chapterName", chapterName);
            startActivity(intent);
        }
    }

    /*资源发布成功*/
    @Override
    public void onResourcesSuccess(String str) {
        ToastUtils.showShort("发布资源成功");
    }

    /*资源发布失败*/
    @Override
    public void onResourcesFail(String msg) {
        ToastUtils.showShort(msg);
    }


    /*获取开始教学*/
    @Override
    public void onStartStudySuccess(StartStudyBean startStudyBean) {
        lessonTaskId = startStudyBean.getId();
        baseVolumeId = startStudyBean.getVolumeId();
        chapterId = startStudyBean.getChapterId();
        lessonLibId = startStudyBean.getLessonLibId();
        subjectId = startStudyBean.getSubjectId();
        chapterName = startStudyBean.getName();
    }

    @Override
    public void onStartStudyFail(String msg) {
        ToastUtils.showShort(msg);
    }
}
