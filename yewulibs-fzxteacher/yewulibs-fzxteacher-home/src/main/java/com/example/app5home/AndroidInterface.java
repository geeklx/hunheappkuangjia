package com.example.app5home;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.LocalBroadcastManagers;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.student.libutils.util.UrlEncodeUtils;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.JstokenUserinfoBean;
import com.sdzn.fzx.teacher.bean.JumpNewActBean;

import java.io.UnsupportedEncodingException;

/**
 * Created by cenxiaozhong on 2017/5/14.
 * source code  https://github.com/Justson/AgentWeb
 */

public class AndroidInterface {
    private Handler deliver = new Handler(Looper.getMainLooper());
    private AgentWeb agent;
    private Activity context;

    public AndroidInterface(AgentWeb agent, Activity context) {
        this.agent = agent;
        this.context = context;
    }

    /*-------------New开始----------------------*/
    JumpNewActBean jumpNewActBean;

    /**
     * 统一跳转二级界面方法
     */
    @JavascriptInterface
    public void JumpNewAndroidAct(final String url) {
        if (url != null) {
            jumpNewActBean = new Gson().fromJson(url, JumpNewActBean.class);
            if(jumpNewActBean.getPath().equals("/indexPad")){
                Activity activity = ActivityUtils.getActivityByContext(context);
                ActivityUtils.finishActivity(activity);
                Intent msgIntent = new Intent();
                msgIntent.setAction("activityRefresh");
                LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
            }else if (jumpNewActBean.getPath().substring(0, jumpNewActBean.getPath().indexOf("?")).equals("/autonomicLearningParent/learningModule")) {
                String id = jumpNewActBean.getPath().substring(jumpNewActBean.getPath().lastIndexOf("=") + 1);//获取id
                Intent intent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.ZzxxFbjlTwoActivity");
                intent1.putExtra("id", id);
                intent1.putExtra("url", jumpNewActBean.getPath());
                intent1.putExtra("titlename", jumpNewActBean.getName());
                context.startActivity(intent1);
            } else {
                Intent intent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.SecondLevelWebViewActivity");
                String replaceurl1 = BuildConfig3.SERVER_ISERVICE_NEW1 + jumpNewActBean.getPath();
                try {
                    intent1.putExtra("url_key", UrlEncodeUtils.encodeUrl(replaceurl1));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                context.startActivity(intent1);
            }
        }
    }

    /**
     * 二级返回一级刷新
     */
    @JavascriptInterface
    public void activityRefresh() {
        //windows.android.activityRefresh() js
        Intent msgIntent = new Intent();
        msgIntent.setAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
//        ToastUtils.showLong("调用activityRefresh方法");
    }

    /**
     * token失效跳转登录页
     * str取到的参数无用
     */
    @JavascriptInterface
    public void JumpLogin() {
//        ToastUtils.showLong("token失效重新登录");
//        if (BaseAppManager.getInstance().top() instanceof LoginActivity) {
//            finishClass();
//        }
//        if (ActivityUtils.getTopActivity() instanceof LoginActivity) {
//            finishClass();
//        }
        MyLogUtil.e("ssssssssssssssssssssss", ActivityUtils.getTopActivity().getComponentName().getClassName().toString());
        if (!TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.example.app5libbase.login.activity.LoginActivity")) {
            ActivityUtils.finishAllActivities();
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    /**
     * web端拿token和Userinfo方法
     */
    @JavascriptInterface
    public String activityTokenUserInfo() {
        final String jsuserInfo = SPUtils.getInstance().getString("userInfo", "");
        final String jstoken = SPUtils.getInstance().getString("token", "");
//        final String jstoken ="c188e010-1f5d-4962-bcbe-e1d5cf086123";
        LogUtils.e("-------jsuserInfo--------"+jsuserInfo);
        LogUtils.e("------token---------"+jstoken);
        JstokenUserinfoBean jstokenUserinfoBean = new JstokenUserinfoBean();
        jstokenUserinfoBean.setToken(jstoken);
        jstokenUserinfoBean.setUserinfo(jsuserInfo);
        final String loginJsonStr = new Gson().toJson(jstokenUserinfoBean);
        LogUtils.e(loginJsonStr);
        return loginJsonStr;
    }


    /**
     * 统一方法
     */
//    @JavascriptInterface
//    public String activitySetting() {
//        String ClassIndexId = SPUtils.getInstance().getString("shouye_class_index_id", "0");
////        ToastUtils.showLong(ClassIndexId);
//        return ClassIndexId;
//    }

    /**
     * web端拿token方法
     */
//    @JavascriptInterface
//    public String activityToken() {
//        String Token = SPUtils.getInstance().getString("token", "");
////        ToastUtils.showLong(Token);
//        return Token;
//    }

    /**
     * web端拿用户实体方法
     */
//    @JavascriptInterface
//    public String activityUserBean() {
//        LoginBean data = com.sdzn.fzx.teacher.utils.SPUtils.getLoginBean();
//        final String loginJsonStr = new Gson().toJson(data);
////        ToastUtils.showLong(loginJsonStr);
//        return loginJsonStr;
//    }
    /*-------------New结束----------------------*/

}
