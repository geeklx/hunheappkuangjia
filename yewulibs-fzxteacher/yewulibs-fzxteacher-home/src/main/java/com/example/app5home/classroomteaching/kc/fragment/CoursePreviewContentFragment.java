package com.example.app5home.classroomteaching.kc.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SPUtils;
import com.example.app5home.R;
import com.example.app5home.classroomteaching.kc.adapter.ContentExamPreviewAdapter;
import com.example.app5home.classroomteaching.kc.test.ContentExamAdapter;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.bean.ClassObjectivesBean;
import com.sdzn.fzx.teacher.presenter.TeachingPreviewContentPresenter;
import com.sdzn.fzx.teacher.view.TeachingPreviewContentViews;

import java.util.ArrayList;
import java.util.List;


public class CoursePreviewContentFragment extends BaseActFragment1 implements TeachingPreviewContentViews {

    TeachingPreviewContentPresenter contentPresenter;
    private RecyclerView mRecyclerView;
    private ContentExamPreviewAdapter contentExamAdapter;
    private ClassContentBean.TestIngListBean testBean;
    private String lessonId;
    private int headNum;//添加头部数
    private List<ClassContentBean.TestIngListBean.LibCourseDetailListBean> mList;

    public static CoursePreviewContentFragment newInstance(Bundle bundle) {
        CoursePreviewContentFragment fragment = new CoursePreviewContentFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_course_content;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        initData();
        contentPresenter = new TeachingPreviewContentPresenter();
        contentPresenter.onCreate(this);
        mRecyclerView = rootView.findViewById(R.id.recyclerView);
        contentExamAdapter = new ContentExamPreviewAdapter(App2.get(), mList, getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(contentExamAdapter);
        contentPresenter.queryOjectivesName("Bearer " + (String) SPUtils.getInstance().getString("token", ""), lessonId);


    }

    /**
     * @param pos 滑动到某位置
     */
    public void scrollToPos(int pos) {
        if (mRecyclerView != null) {
            mRecyclerView.scrollToPosition(pos + headNum);
        }
    }


    private void initData() {
        mList = new ArrayList<>();
        testBean = (ClassContentBean.TestIngListBean) getArguments().getSerializable("courseDetailTest");
        lessonId = getArguments().getString("lessonId");

        //题数 总分值
        mList.add(new ClassContentBean.TestIngListBean.LibCourseDetailListBean(1001, getArguments().getString("courseName"),
                getArguments().getString("objectiveName"), getArguments().getString("testNum"), getArguments().getString("testCount")));
        headNum = 1;//header 数量为1个
        mList.addAll(testBean.getLibCourseDetailList());




    }



    public void onDestroyView() {
        super.onDestroyView();

    }


    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }


    @Override
    public void onCoursesOjectivesSuccess(ClassObjectivesBean objectivesBean) {
        objectivesBean.getName();
        objectivesBean.getLearningObjectives();
//        setHeader(mRecyclerView);
    }

//    private void setHeader(RecyclerView view) {
//        View header = LayoutInflater.from(getContext()).inflate(R.layout.item_class_content1, view, false);
//        ( (TextView)header.findViewById(R.id.tv_content_title)).setText("111111111");
//
//    }
}
