package com.example.app5home.ktjx1.jxkc;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.sdzn.fzx.teacher.bean.TeachingCoursesBean;

import java.util.List;

public class TeachingCoursesAdapter extends BaseQuickAdapter<TeachingCoursesBean.RowsBean, BaseViewHolder> {

    public TeachingCoursesAdapter() {
        super(R.layout.recycleview_teaching_courses_item);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final TeachingCoursesBean.RowsBean item) {
        TextView tvCreateTime = helper.itemView.findViewById(R.id.tv_createTime);//创建时间
        TextView tvCourseName = helper.itemView.findViewById(R.id.tv_courseName);//课程名称
        TextView tvChapterName = helper.itemView.findViewById(R.id.tv_chapterName);//章节名称
        TextView tvTestingNum = helper.itemView.findViewById(R.id.tv_testingNum);//检测
        TextView tvSourseNum = helper.itemView.findViewById(R.id.tv_sourseNum);//资料
        TextView tvYulan = helper.itemView.findViewById(R.id.tv_yulan);//预览
        TextView tvStartTeaching = helper.itemView.findViewById(R.id.tv_start_teaching);//开始教学

        if (mOnYulanItemClickLitener != null&&onStartTeachingItemClickLitener!=null) {
            helper.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnHelperItemClickLitener.onHelperItemClick(helper, item);
                }
            });
            tvYulan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnYulanItemClickLitener.onYulanItemClick(helper, item);
                }
            });
            tvStartTeaching.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStartTeachingItemClickLitener.onStartTeachingItemClick(helper, item);
                }
            });
        }
        tvCreateTime.setText("创建时间：" + item.getCreateTime());
        tvCourseName.setText(item.getCourseName());
        tvChapterName.setText(item.getChapterName());
        String strMsg = "检测  <font color=\"#FA541C\">" + item.getTestingNum() + "</font>";
        tvTestingNum.setText(Html.fromHtml(strMsg));
        String strMsg1 = "资料  <font color=\"#FA541C\">" + item.getSourseNum() + "</font>";
        tvSourseNum.setText(Html.fromHtml(strMsg1));
    }

    /**
     * ItemClick的回调接口
     *
     * @author geek
     */
    public interface OnHelperItemClickLitener {
        void onHelperItemClick(BaseViewHolder view,TeachingCoursesBean.RowsBean rowsBean);
    }
   public interface OnYulanItemClickLitener {
        void onYulanItemClick(BaseViewHolder view,TeachingCoursesBean.RowsBean rowsBean);
    }
    public interface OnStartTeachingItemClickLitener {
        void onStartTeachingItemClick(BaseViewHolder view,TeachingCoursesBean.RowsBean rowsBean);
    }
    private OnHelperItemClickLitener mOnHelperItemClickLitener;
    private OnYulanItemClickLitener mOnYulanItemClickLitener;
    private OnStartTeachingItemClickLitener onStartTeachingItemClickLitener;

    public void setOnItemClickLitener(OnHelperItemClickLitener mOnHelperItemClickLitener) {
        this.mOnHelperItemClickLitener = mOnHelperItemClickLitener;
    }
    public void setOnItemClickLitener(OnYulanItemClickLitener mOnYulanItemClickLitener) {
        this.mOnYulanItemClickLitener = mOnYulanItemClickLitener;
    }
    public void setOnItemClickLitener(OnStartTeachingItemClickLitener onStartTeachingItemClickLitener) {
        this.onStartTeachingItemClickLitener = onStartTeachingItemClickLitener;
    }
}
