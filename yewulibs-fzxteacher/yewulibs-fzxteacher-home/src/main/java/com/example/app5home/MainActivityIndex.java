package com.example.app5home;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.alibaba.fastjson.JSON;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.service.UpdataCommonservices;
import com.example.app5libbase.util.CircleTransform;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libpublic.event.BaiBanScreenEvent;
import com.example.app5libpublic.event.Event;
import com.example.app5libpublic.event.OnScreenCaptureEvent;
import com.example.baselibrary.base.BaseAppManager;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.google.gson.Gson;
import com.just.agentweb.WebViewClient;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.MmkvUtils;
import com.sdzn.fzx.student.libutils.util.StartHiddenManager;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.bean.SubjectData;
import com.sdzn.fzx.teacher.bean.SubjectData1;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CehuaPresenter;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.presenter.MainIndexPresenter;
import com.sdzn.fzx.teacher.presenter.ShouyePresenter;
import com.sdzn.fzx.teacher.utils.SPUtilsTracher;
import com.sdzn.fzx.teacher.view.CehuaViews;
import com.sdzn.fzx.teacher.view.CheckverionViews;
import com.sdzn.fzx.teacher.view.MainIndexViews;
import com.sdzn.fzx.teacher.view.ShouyeViews;
import com.sdzn.fzx.teacher.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import pub.devrel.easypermissions.EasyPermissions;
import update.UpdateAppUtils;


public class MainActivityIndex extends BaseActWebActivity1 implements MainIndexViews, CheckverionViews, ShouyeViews, View.OnClickListener, NetconListener2, CehuaViews {
    private LinearLayout llUserInformation;//个人信息
    private ImageView ivSetup;//设置
    private LinearLayout llSubject;//选择学科
    private TextView tvSubject;
    private int checkedPos = 0;//默认学科
    private ImageView ivMeHead;
    private TextView tvMeName;
    MainIndexPresenter mainIndexPresenter;
    private LoginBean data;
    private String userInfo;//添加学科的用户信息
    List<SubjectData> dataList;//学科集合

    private TextView tvDynamicTitle;//广告标题
    private ImageView ivMyIconCurriculum;//我的课程
    private LinearLayout llAutonomyStudy;//自主学习
    private LinearLayout llClassroomTeaching;//课堂教学
    private LinearLayout llTask;//作业
    private RelativeLayout rlExamination;//考试
    private RelativeLayout rlScienceAnalysis;//学情分析
    private RelativeLayout rlMyClass;//我的班级
    private RelativeLayout rlCorrecting;//批改
    private RelativeLayout rlCexplore;//合作探究
    private LinearLayout llThinkTankSchool;//智囊学堂跳转项目
    private LinearLayout llBaiduRncyclopedia;//百度百科

    private TextView tvShouyeZzxxTask;
    private TextView tvKtjxTask;
    private TextView tvJrkcTask;
    private TextView tvZuoyeTask;
    private TextView tvHztjTask;
    private TextView tvPigiaTask;
    private TextView tvXqfxTask;
    private Button btnRefresh;

    ShouyePresenter shouyePresenter;
    private Handler mHandler;
    private ScheduledExecutorService scheduledExecutorService;
    private ShouyeBean mmkvshouyeBean;
    private StartHiddenManager startHiddenManager;

    protected EmptyViewNew1 emptyview1;//网络监听
    protected NetState netState;
    protected SmartRefreshLayout refreshLayout1;//刷新
    LinearLayout llShouyebg;//首页bind内容

    CehuaPresenter cehuaPresenter;//侧滑
    private CheckverionFzxPresenter checkverionFzxPresenter;

    private Handler.Callback mCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == 10) {
                mmkvshouyeBean = MmkvUtils.getInstance().get_common_json("ShouyeBean", ShouyeBean.class);
                if (mmkvshouyeBean == null) {
                    return true;
                }
//                android.util.Log.e("onIndexSuccess", "mCallback: " + mmkvshouyeBean.getTvDynamicTitle());
                if (!TextUtils.isEmpty(mmkvshouyeBean.getTvDynamicTitle())) {
                    tvDynamicTitle.setText(Html.fromHtml(mmkvshouyeBean.getTvDynamicTitle()));//顶部标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlMyCurriculum())) {
                    tvJrkcTask.setText(Html.fromHtml(mmkvshouyeBean.getLlMyCurriculum()));//今日课程
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlAutonomyStudy())) {
                    tvShouyeZzxxTask.setText(Html.fromHtml(mmkvshouyeBean.getLlAutonomyStudy()));//自主学习标题
                }
//                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlClassroomTeaching())) {
//                    tvKtjxTask.setText(Html.fromHtml(mmkvshouyeBean.getLlClassroomTeaching()));//课堂教学标题
//                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlTask())) {
                    tvZuoyeTask.setText(Html.fromHtml(mmkvshouyeBean.getLlTask()));//作业标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getRlCexplore())) {
                    tvHztjTask.setText(Html.fromHtml(mmkvshouyeBean.getRlCexplore()));//合作探究标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getRlCorrecting())) {
                    tvPigiaTask.setText(Html.fromHtml(mmkvshouyeBean.getRlCorrecting()));//批改标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getRlScienceAnalysis())) {
                    tvXqfxTask.setText(Html.fromHtml(mmkvshouyeBean.getRlScienceAnalysis()));//学期分析标题
                }
            }
            return true;
        }
    };


    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_shouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.color_FB6215), 0);
        data = SPUtilsTracher.getLoginBean();
        /*学科请求*/
        mainIndexPresenter = new MainIndexPresenter();
        mainIndexPresenter.onCreate(this);

        /*首页内容*/
        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);

        findview();

        //
        startHiddenManager = new StartHiddenManager(findViewById(R.id.iv_left1), findViewById(R.id.tv_right1), null, new StartHiddenManager.OnClickFinish() {
            @Override
            public void onFinish() {
                new XPopup.Builder(MainActivityIndex.this)
                        //.dismissOnBackPressed(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .autoOpenSoftInput(true)
//                        .autoFocusEditText(false) //是否让弹窗内的EditText自动获取焦点，默认是true
                        .isRequestFocus(false)
                        //.moveUpToKeyboard(false)   //是否移动到软键盘上面，默认为true
                        .asInputConfirm("修改地址", SPUtils.getInstance().getString("版本地址2", "https://www.baidu.com"), null, "",
                                new OnInputConfirmListener() {
                                    @Override
                                    public void onConfirm(String text) {
//                                        toast("input text: " + text);
                                        if (text.contains("http")) {
                                            String[] content = text.split(",");
                                            SPUtils.getInstance().put("版本地址1", content[0]);
                                            SPUtils.getInstance().put("版本地址2", content[1]);
                                            BuildConfig3.SERVER_ISERVICE_NEW1 = content[0];
                                            BuildConfig3.SERVER_ISERVICE_NEW2 = content[1];
                                        }
//                                new XPopup.Builder(getContext()).asLoading().show();
                                    }
                                })
                        .show();
            }
        });
        EventBus.getDefault().register(this);
//        TimeBaseUtil.getInstance().init(this);

        /*侧滑数据存储*/
        cehuaPresenter = new CehuaPresenter();
        cehuaPresenter.onCreate(this);
        cehuaPresenter.queryCehua();
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        checkverionFzxPresenter.checkVerion("1", "7");

    }

    private void checkLatestMedal() {
        Intent intent = new Intent(this, UpdataCommonservices.class);
        intent.setAction(UpdataCommonservices.HUIBEN_READINGTIME_ACTION);
        startService(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(Event event) {
        if (event instanceof OnScreenCaptureEvent) {
            requestPermissiontest();
        } else if (event instanceof BaiBanScreenEvent) {
            requestPermissiontest1();
        }
    }

    private static final int REQUECT_CODE_SDCARD = 1000;

    public void requestPermissiontest() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事
            if (BaseAppManager.getInstance().top() != null) {
                if (TimeBaseUtil.getInstance().mMediaProjection == null) {
                    BaseAppManager.getInstance().top().startActivityForResult(
                            TimeBaseUtil.getInstance().mMediaProjectionManager.createScreenCaptureIntent(),
                            TimeBaseUtil.REQUEST_MEDIA_PROJECTION_CAPTURE);
                } else {
                    TimeBaseUtil.getInstance().getScreen();
                }
            }
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_SDCARD, perms);
        }
    }

    public void requestPermissiontest1() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事
            if (BaseAppManager.getInstance().top() != null) {
                if (TimeBaseUtil.getInstance().mMediaProjection == null) {
                    BaseAppManager.getInstance().top().startActivityForResult(
                            TimeBaseUtil.getInstance().mMediaProjectionManager.createScreenCaptureIntent(),
                            TimeBaseUtil.REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN);
                } else {
                    TimeBaseUtil.getInstance().getBaiBANScreen();
                }
            }
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置访问SD卡权限", REQUECT_CODE_SDCARD, perms);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void findview() {
        netState = new NetState();
        netState.setNetStateListener(this, this);
        mainIndexPresenter.getSubjectInfo(data.getData().getUser().getId());
//        RefreshLoad();
//        checkLatestMedal();//启动服务
        emptyview1 = findViewById(R.id.emptyview2_order);
        refreshLayout1 = findViewById(R.id.refreshLayout1_order);//刷新控件
        llShouyebg = findViewById(R.id.ll_shouye_outer_layer);
        if (refreshLayout1 != null && emptyview1 != null) {
            refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(final RefreshLayout refreshLayout) {
                    //刷新内容
                    RefreshLoad();
                }
            });

            /*绑定empty方法*/
            emptyview1.bind(llShouyebg).setRetryListener(new EmptyView.RetryListener() {
                @Override
                public void retry() {
                    // 分布局
                    emptyview1.loading();
                    RefreshLoad();
                }
            });
            emptyview1.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        }
        tvSubject = (TextView) findViewById(R.id.tv_subject);
        llUserInformation = (LinearLayout) findViewById(R.id.ll_user_information);
        ivSetup = (ImageView) findViewById(R.id.iv_setup);
        llSubject = (LinearLayout) findViewById(R.id.ll_subject);
        ivMeHead = findViewById(R.id.iv_meHead);
        tvMeName = findViewById(R.id.tv_meName);
        tvDynamicTitle = findViewById(com.example.app5libbase.R.id.tv_dynamic_title);
        ivMyIconCurriculum = findViewById(com.example.app5libbase.R.id.iv_my_icon_curriculum);
        llAutonomyStudy = findViewById(com.example.app5libbase.R.id.ll_autonomy_study);
        llClassroomTeaching = findViewById(com.example.app5libbase.R.id.ll_Classroom_teaching);
        llTask = findViewById(com.example.app5libbase.R.id.ll_task);
        rlExamination = findViewById(com.example.app5libbase.R.id.rl_examination);
        rlScienceAnalysis = findViewById(com.example.app5libbase.R.id.rl_scienceAnalysis);
        rlMyClass = findViewById(com.example.app5libbase.R.id.rl_myClass);
        rlCorrecting = findViewById(com.example.app5libbase.R.id.rl_correcting);
        rlCexplore = findViewById(com.example.app5libbase.R.id.rl_cexplore);
        llThinkTankSchool = findViewById(com.example.app5libbase.R.id.ll_think_tank_school);
        llBaiduRncyclopedia = findViewById(com.example.app5libbase.R.id.ll_baidu_rncyclopedia);
        btnRefresh = findViewById(R.id.btn_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ShowAgentweb();
            }
        });

        /*首页更新的textView*/
        tvShouyeZzxxTask = findViewById(com.example.app5libbase.R.id.tv_shouye_zzxx_task);
//        tvKtjxTask = findViewById(com.example.app5libbase.R.id.tv_ktjx_task);
        tvJrkcTask = findViewById(com.example.app5libbase.R.id.tv_jrkc_task);
        tvZuoyeTask = findViewById(com.example.app5libbase.R.id.tv_zuoye_task);
        tvHztjTask = findViewById(com.example.app5libbase.R.id.tv_hztj_task);
        tvPigiaTask = findViewById(com.example.app5libbase.R.id.tv_pigia_task);
        tvXqfxTask = findViewById(com.example.app5libbase.R.id.tv_xqfx_task);

        ivMyIconCurriculum.setOnClickListener(this);
        llAutonomyStudy.setOnClickListener(this);
        llClassroomTeaching.setOnClickListener(this);
        llTask.setOnClickListener(this);
        rlExamination.setOnClickListener(this);
        rlScienceAnalysis.setOnClickListener(this);
        rlMyClass.setOnClickListener(this);
        rlCorrecting.setOnClickListener(this);
        rlCexplore.setOnClickListener(this);
        llThinkTankSchool.setOnClickListener(this);
        llBaiduRncyclopedia.setOnClickListener(this);
        set_update_progress_ui();
        if (data == null) {
            return;
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getRealName())) {
            tvMeName.setText(data.getData().getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getPhoto())) {
            RequestOptions error = new RequestOptions().error(com.example.app5grzx.R.mipmap.tx_img).transform(new CircleTransform(this)).placeholder(com.example.app5grzx.R.mipmap.tx_img);
            Glide.with(this).load(data.getData().getUser().getPhoto()).apply(error).into(ivMeHead);
        }
        llUserInformation.setOnClickListener(this);
        ivSetup.setOnClickListener(this);
        llSubject.setOnClickListener(this);
        ShowAgentweb();
    }

    private void set_update_progress_ui() {
        if (mHandler != null) {
            return;
        }
        if (scheduledExecutorService != null) {
            return;
        }
        mHandler = new WeakRefHandler(mCallback);
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    Message msg1 = Message.obtain();
                    msg1.what = 10;
                    mHandler.sendMessage(msg1);
                } catch (Exception e) {
                    Log.e("houjie", "scheduleWithFixedDelay:" + e.getMessage());
                }
            }
        }, 1, 20, TimeUnit.SECONDS);//SECONDS--秒);//MILLISECONDS--毫秒
    }

    /*第一次刷新*/
    public void RefreshLoad() {
        LogUtils.e("RefreshLoad----------" + SPUtils.getInstance().getString("shouye_class_id"));
        shouyePresenter.queryShouye("Bearer " + (String) SPUtils.getInstance().getString("token", ""), SPUtils.getInstance().getString("shouye_class_id"));
    }

    private void ShowAgentweb() {
        LogUtils.e("-----首页刷新WebView");
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
//            ToastUtils.showLong(BuildConfig3.SERVER_ISERVICE_NEW1 + "/setToken");
            loadWebSite(BuildConfig3.SERVER_ISERVICE_NEW1 + "/setToken"); // 刷新
        }
        mAgentWeb.getWebCreator().getWebView().setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView contentWebView, String url) {
//                SPUtils.getInstance().put("token", "c188e010-1f5d-4962-bcbe-e1d5cf086123");
                final String jstoken = SPUtils.getInstance().getString("token", "");
                final String jsuserInfo = SPUtils.getInstance().getString("userInfo", "");
//                LogUtils.e(jstoken + "-----" + jsuserInfo);
                super.onPageFinished(contentWebView, url);
//                if (TextUtils.isEmpty(jstoken) && jsuserInfo.isEmpty()) {
//                    return;
//                }
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//                    if (contentWebView != null) {
//                        contentWebView.evaluateJavascript("window.localStorage.setItem('" + "sdzn-token" + "','" + jstoken + "');", null);
//                        contentWebView.evaluateJavascript("window.localStorage.setItem('" + "sdzn-userInfo" + "','" + jsuserInfo + "');", null);
//                        ToastUtils.showLong(jstoken + "--------" + jsuserInfo);
//                    }
//                } else {
//                    if (contentWebView != null) {
//                        contentWebView.loadUrl("javascript:localStorage.setItem('" + "sdzn-token" + "','" + jstoken + "');");
//                        contentWebView.loadUrl("javascript:localStorage.setItem('" + "sdzn-userInfo" + "','" + jsuserInfo + "');");
//                        ToastUtils.showLong(jstoken + "--------" + jsuserInfo);
//                    }
//                }
                contentWebView.loadUrl("javascript:getTokenAndUserInfo('" + jstoken + "','" + jsuserInfo + "')");
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private long exitTime;

    /**
     * 连按两次退出程序
     */
    private void exit() {
        if ((System.currentTimeMillis() - exitTime) < 1500) {
            ActivityUtils.finishAllActivities();
            Intent intent3 = new Intent(MainActivityIndex.this, UpdataCommonservices.class);
            MainActivityIndex.this.stopService(intent3);// 关闭服务
        } else {
            ToastUtils.showShort("再按一次退出程序 ~");
            exitTime = System.currentTimeMillis();
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == com.example.app5libbase.R.id.iv_my_icon_curriculum) {//我的课程
            IntentShow("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdkcActivity", BuildConfig3.SERVER_ISERVICE_NEW1 + "/indexPad");//"file:///android_asset/js_interaction/wdkc.html");
//            IntentShow("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdkcActivity", "https://www.pgyer.com/");//"file:///android_asset/js_interaction/wdkc.html");
        } else if (id == com.example.app5libbase.R.id.ll_autonomy_study) {//自主学习
            IntentShowExtra("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.ZzxxShouyeActivity");
        } else if (id == com.example.app5libbase.R.id.ll_Classroom_teaching) {//课堂教学
            IntentShowExtra("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.KtjxShouyeActivity1");
        } else if (id == com.example.app5libbase.R.id.ll_task) {//作业
            IntentShowExtra("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.ZyJiLuActivity");
        } else if (id == com.example.app5libbase.R.id.rl_examination) {//考试
            IntentShow("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.KaoShiActivity", BuildConfig3.SERVER_ISERVICE_NEW1 + "/examination/index/sectionTopics");
        } else if (id == com.example.app5libbase.R.id.rl_scienceAnalysis) {//学情分析
            IntentShowExtra("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.XqfxActivity?show=" + "1");
//            IntentShowExtra("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.MeActivity?show=" + "3");
        } else if (id == com.example.app5libbase.R.id.rl_myClass) {//我的班级
            IntentShow("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdbjActivity", BuildConfig3.SERVER_ISERVICE_NEW1 + "/myClass/index");
        } else if (id == com.example.app5libbase.R.id.rl_correcting) {//批改
            IntentShow("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.TeacherpgActivity", BuildConfig3.SERVER_ISERVICE_NEW1 + "/taskCorrecting/index");
        } else if (id == com.example.app5libbase.R.id.rl_cexplore) {//合作探究
            IntentShowExtra("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.HeZuoActivity?show=" + "1");
        } else if (id == com.example.app5libbase.R.id.ll_think_tank_school) {//智囊学堂
            if (AppUtils.isAppInstalled("com.sdzn.pkt.teacher.hd")) {
                String name = String.valueOf(SPUtilsTracher.get(App2.get(), SPUtilsTracher.LOGIN_USER_NUM, ""));
                String pwd = String.valueOf(SPUtilsTracher.get(App2.get(), SPUtilsTracher.LOGIN_PAS_CHECK, ""));
                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.teacher.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd));
                startActivity(intent);
            } else {
//                ToastUtils.showLong("跳转异常，请检查跳转配置、包名及Activity访问权限");
                ToastUtils.showLong("请先安装智囊学堂老师端APP");
            }
        } else if (id == com.example.app5libbase.R.id.ll_baidu_rncyclopedia) {//百度百科
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WebBaiduEncyclopediaActivity"));
            intent.putExtra("url_key", "https://baike.baidu.com/vbaike#");//https://www.baidu.com/
//            intent.putExtra("url_key", "https://www.pgyer.com/");
            startActivity(intent);
        } else if (id == R.id.ll_user_information) {//个人中心
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.MeActivity?query1=" + "0"));
            startActivity(intent);
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
//            intent.putExtra("query1", 0);
//            startActivity(intent);
        } else if (id == R.id.iv_setup) {//系统设置
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.MeActivity?query1=" + "3"));
            startActivity(intent);
        } else if (id == R.id.ll_subject) {//设置学科
            List<String> stringList = new ArrayList<>();
            if (dataList.size() != 0) {
                for (int i = 0; i < dataList.size(); i++) {
                    stringList.add(dataList.get(i).getSubjectName());
                    if (dataList.get(i).getSubjectName().equals(SPUtils.getInstance().getString("shouye_class_name"))) {
                        checkedPos = i;
                    }
                }
            }
            String[] title1 = stringList.toArray(new String[stringList.size()]);
            new XPopup.Builder(MainActivityIndex.this)
                    .hasShadowBg(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .offsetY(20)
                    .offsetX(40)
                    .atView(view)  //依附于所点击的View，内部会自动判断在上方或者下方显示 new String[]{"语文", "数学", "物理", "化学"}
                    .asAttachList1(title1,
                            new int[]{},
                            new OnSelectListener() {
                                @Override
                                public void onSelect(int position, String text) {
                                    checkedPos = position;
                                    tvSubject.setText(dataList.get(position).getSubjectName());
                                    SPUtils.getInstance().put("shouye_class_id", dataList.get(position).getSubjectId());
                                    SPUtils.getInstance().put("shouye_class_name", dataList.get(position).getSubjectName());
                                    SPUtils.getInstance().put("shouye_class_index_id", String.valueOf(checkedPos));
                                    mAgentWeb.getJsAccessEntrace().quickCallJs("activitySetting", SPUtils.getInstance().getString("shouye_class_index_id", "0"));
                                    shouyePresenter.queryShouye("Bearer " + (String) SPUtils.getInstance().getString("token", ""), dataList.get(position).getSubjectId());
                                }
                            }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                    .show();

        }
    }


    public static final String URL_KEY = "url_key";

    /*带参跳转*/
    private void IntentShow(String act, String url) {
        if (act != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(act));
            intent.putExtra(URL_KEY, url);
            startActivity(intent);
        }
    }

    /*无参跳转*/
    private void IntentShowExtra(String act) {
        if (act != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(act));
            startActivity(intent);
        }
    }

    @Override
    public void getSubjectInfo(SubjectData1 subjectData) {
        dataList = new ArrayList<>();
        if (subjectData != null) {
            dataList.addAll(subjectData.getData());
            for (int i = 0; i < dataList.size(); i++) {
//                SPUtils.getInstance().getString("shouye_class_name").equals("")
//                SPUtils.getInstance().getString("shouye_class_id").equals("") &&
                if (!SPUtils.getInstance().getString("shouye_class_id").equals(dataList.get(i).getSubjectId())) {
                    tvSubject.setText(dataList.get(0).getSubjectName());
                    SPUtils.getInstance().put("shouye_class_id", dataList.get(0).getSubjectId());
                    SPUtils.getInstance().put("shouye_class_name", dataList.get(0).getSubjectName());
                    SPUtils.getInstance().put("shouye_class_index_id", "0");
                    mAgentWeb.getJsAccessEntrace().quickCallJs("activitySetting", SPUtils.getInstance().getString("shouye_class_index_id", "0"));
                    userInfo = jsUserBean(subjectData);
                    SPUtils.getInstance().put("userInfo", userInfo);
                    LogUtils.e(userInfo);
                } else {
                    tvSubject.setText(SPUtils.getInstance().getString("shouye_class_name"));
                    mAgentWeb.getJsAccessEntrace().quickCallJs("activitySetting", SPUtils.getInstance().getString("shouye_class_index_id", "0"));
                    userInfo = jsUserBean(subjectData);
                    SPUtils.getInstance().put("userInfo", userInfo);
                }
            }
            RefreshLoad();
//            checkLatestMedal();//启动服务
        }
    }

    @Override
    public void OnSubjectNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onSubjectFailed(String msg) {
        ToastUtils.showLong(msg);
    }

    public String jsUserBean(SubjectData1 subjectData) {
        if (SPUtils.getInstance().getString("shouye_class_id").equals("") && SPUtils.getInstance().getString("shouye_class_name").equals("")) {
            LoginBean data = SPUtilsTracher.getLoginBean();
            data.getData().getUser().setSubjectList(subjectData.getData());
            data.getData().getUser().setSubjectType("0");
//        long timecurrentTimeMillis = System.currentTimeMillis();
//        JsShouyeBean jsShouyeBean = new JsShouyeBean();
//        jsShouyeBean.setContent(data.getData().getUser());
//        jsShouyeBean.setDatatype("object");
//        jsShouyeBean.setDatetime(String.valueOf(timecurrentTimeMillis));
            final String loginJsonStr1 = new Gson().toJson(data.getData().getUser());
            return loginJsonStr1;
        } else {
            LoginBean data = SPUtilsTracher.getLoginBean();
            data.getData().getUser().setSubjectList(subjectData.getData());
            data.getData().getUser().setSubjectType(SPUtils.getInstance().getString("shouye_class_index_id", "0"));
            final String loginJsonStr2 = new Gson().toJson(data.getData().getUser());
            return loginJsonStr2;
        }
    }


    @Override
    public void onIndexSuccess(ShouyeBean shouyeBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
        MmkvUtils.getInstance().set_common_json("ShouyeBean", JSON.toJSONString(shouyeBean), ShouyeBean.class);
        if (shouyeBean == null) {
            return;
        }
        android.util.Log.e("onIndexSuccess", "ShouyeBean: " + shouyeBean.getTvDynamicTitle());
        if (!TextUtils.isEmpty(shouyeBean.getTvDynamicTitle())) {
            tvDynamicTitle.setText(Html.fromHtml(shouyeBean.getTvDynamicTitle()));//顶部标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getLlMyCurriculum())) {
            tvJrkcTask.setText(Html.fromHtml(shouyeBean.getLlMyCurriculum()));//今日课程
        }
        if (!TextUtils.isEmpty(shouyeBean.getLlAutonomyStudy())) {
            tvShouyeZzxxTask.setText(Html.fromHtml(shouyeBean.getLlAutonomyStudy()));//自主学习标题
        }
//        if (!TextUtils.isEmpty(shouyeBean.getLlClassroomTeaching())) {
//            tvKtjxTask.setText(Html.fromHtml(shouyeBean.getLlClassroomTeaching()));//课堂教学标题
//        }
        if (!TextUtils.isEmpty(shouyeBean.getLlTask())) {
            tvZuoyeTask.setText(Html.fromHtml(shouyeBean.getLlTask()));//作业标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getRlCexplore())) {
            tvHztjTask.setText(Html.fromHtml(shouyeBean.getRlCexplore()));//合作探究标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getRlCorrecting())) {
            tvPigiaTask.setText(Html.fromHtml(shouyeBean.getRlCorrecting()));//批改标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getRlScienceAnalysis())) {
            tvXqfxTask.setText(Html.fromHtml(shouyeBean.getRlScienceAnalysis()));//学期分析标题
        }
    }

    @Override
    public void onIndexNodata(int code, String msg) {
        ToastUtils.showLong(msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void onIndexFail(String msg) {
        ToastUtils.showLong(msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(0);
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    protected void onDestroy() {
        shouyePresenter.onDestory();
        mainIndexPresenter.onDestory();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

    /**
     * 侧滑数据处理
     */

    @Override
    public void onCehuaSuccess(GrzxRecActBean grzxRecActBean) {
        MmkvUtils.getInstance().set_common_json("GrzxRecActBean", JSON.toJSONString(grzxRecActBean), GrzxRecActBean.class);
    }

    @Override
    public void onCehuaNodata(String msg) {

    }

    @Override
    public void onCehuaFail(String msg) {

    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = AppUtils.getAppVersionCode();//App2Utils.getAppVersionCode(App2.get());//获取版本号
            if (!versionInfoBean.getVersionNum().equals("")){
                int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
                if (updateVersion > currVersion) {
                    if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                        Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl(),versionInfoBean.getVersionInfo());
                    }
                }
            }
        }
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showShort(msg);
    }

    private void Updatemethod(String description, final String targetUrl,String versionInfo) {
        String updateTitle = "AI智囊学堂老师端" + versionInfo;
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(com.example.app5libbase.R.layout.view_update_dialog_custom_teacher);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(com.example.app5libbase.R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/fzxteacherhd");//apk下载位置
        forceconfig.setApkSaveName("AI智囊学堂老师端");//app名称
        String count = description.replace("|", "\n");
        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        android.util.Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        android.util.Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        android.util.Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        android.util.Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }
}
