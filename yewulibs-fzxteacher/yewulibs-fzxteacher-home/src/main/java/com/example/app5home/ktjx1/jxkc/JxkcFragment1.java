package com.example.app5home.ktjx1.jxkc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5home.R;
import com.example.app5home.ktjx1.Interface.RiliChangeListener;
import com.example.app5home.ktjx1.holder.ContentHolder;
import com.example.app5home.ktjx1.holder.IconTreeItem;
import com.example.app5libbase.ai.pop.ChoiceClassPopupView;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;
import com.sdzn.fzx.teacher.bean.TeachingCoursesBean;
import com.sdzn.fzx.teacher.bean.TreeBean;
import com.sdzn.fzx.teacher.presenter.TeacherVolumePresenter;
import com.sdzn.fzx.teacher.presenter.TeachingCoursesPresenter;
import com.sdzn.fzx.teacher.presenter.TreePresenter;
import com.sdzn.fzx.teacher.utils.SPUtilsTracher;
import com.sdzn.fzx.teacher.view.TeacherVolumeViews;
import com.sdzn.fzx.teacher.view.TeachingCoursesViews;
import com.sdzn.fzx.teacher.view.TreeViews;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.List;

/**
 * 教学课程
 */
public class JxkcFragment1 extends BaseActFragment1 implements TeachingCoursesViews, TreeViews, TeacherVolumeViews, NetconListener2, RiliChangeListener {
    private LinearLayout containerView;//包裹tree控件
    private LinearLayout TeachingNodata;//无数据
    private TextView tvVolumeName;//册别内容
    private RecyclerView rvjxkc;//数据列表
    private SmartRefreshLayout refreshLayout1;//刷新
    private AndroidTreeView tView;//树结构
    private TeachingCoursesAdapter teachingCoursesAdapter;//列表适配器
    private TeachingCoursesPresenter teachingCoursesPresenter;//课堂教学列表数据层
    private TreePresenter treePresenter;//树结构数据层
    private TeacherVolumePresenter teacherVolumePresenter;//册别数据层
    private ContentHolder myHolder = null;//treeView Holder
    private LinearLayout ShouyeOuterLayer;
    protected EmptyViewNew1 emptyview1;//网络监听
    protected NetState netState;

    /*课堂教学列表集合*/
    private List<TeachingCoursesBean.RowsBean> rowsBeanList = new ArrayList<>();
    private String chapterId = "";
    private int page = 1;//页数
    private String openid = "";
    private static final int PAGE_SIZE = 10;//条数
    ChoiceClassPopupView choiceClassPopupView;//分组

    /*绑定布局*/
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_teaching_courses;
    }

    /*初始化控件*/
    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        /*绑定控件*/
        findview(rootView);

        /*点击事件*/
        onclick();

        /*初始化数据*/
        donetwork();
    }

    private void donetwork() {
        /*课本教材*/
        teacherVolumePresenter = new TeacherVolumePresenter();
        teacherVolumePresenter.onCreate(this);

        /*树结构课程数据*/
        treePresenter = new TreePresenter();
        treePresenter.onCreate(this);

        //列表数据
        teachingCoursesPresenter = new TeachingCoursesPresenter();
        teachingCoursesPresenter.onCreate(this);

        //获取老师ID
        LoginBean loginBean = SPUtilsTracher.getLoginBean();
        openid = loginBean.getData().getOpenid();

        //刷新教材
        teacherVolumePresenter.queryTeacherVolume(Token, openid);
    }

    private void findview(View rootView) {
        rvjxkc = rootView.findViewById(R.id.rv_jxkc);//教学课程列表
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);//刷新控件
        TeachingNodata = rootView.findViewById(R.id.ll_teaching_nodata);//暂无数据
        containerView = rootView.findViewById(R.id.container);//包裹tree控件
        tvVolumeName = rootView.findViewById(R.id.tv_volume_name);//册别按钮
        emptyview1 = rootView.findViewById(R.id.emptyview_order);//网络监听
        ShouyeOuterLayer = rootView.findViewById(R.id.ll_shouye_outer_layer);

        netState = new NetState();
        netState.setNetStateListener(this, getActivity());
        if (emptyview1 != null) {
            /*绑定empty方法*/
            emptyview1.bind(ShouyeOuterLayer).setRetryListener(new EmptyView.RetryListener() {
                @Override
                public void retry() {
                    // 分布局
                    emptyview1.loading();
                    teacherVolumePresenter.queryTeacherVolume(Token, openid);
                }
            });
            emptyview1.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        }

        /*初始化列表控件*/
        rvjxkc.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));
        teachingCoursesAdapter = new TeachingCoursesAdapter();
        rvjxkc.setAdapter(teachingCoursesAdapter);
    }

    /*点击事件*/
    private void onclick() {
        teachingCoursesAdapter.setOnItemClickLitener(new TeachingCoursesAdapter.OnHelperItemClickLitener() {
            @Override
            public void onHelperItemClick(BaseViewHolder view, TeachingCoursesBean.RowsBean rowsBean) {
                if (rowsBean == null && rowsBean.getClasses() == null) {
                    return;
                }
                StartPreview(rowsBean);
            }
        });
        teachingCoursesAdapter.setOnItemClickLitener(new TeachingCoursesAdapter.OnYulanItemClickLitener() {

            @Override
            public void onYulanItemClick(BaseViewHolder view, TeachingCoursesBean.RowsBean rowsBean) {
                if (rowsBean == null && rowsBean.getClasses() == null) {
                    return;
                }
                StartPreview(rowsBean);
            }
        });

        teachingCoursesAdapter.setOnItemClickLitener(new TeachingCoursesAdapter.OnStartTeachingItemClickLitener() {
            @Override
            public void onStartTeachingItemClick(BaseViewHolder view, TeachingCoursesBean.RowsBean rowsBean) {
                if (rowsBean == null && rowsBean.getClasses() == null) {
                    return;
                }
                StartTeaching(rowsBean);
            }
        });
        tvVolumeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volumeshow(v);
            }
        });
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                setDatas();
            }
        });

        teachingCoursesAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadMore();
            }
        }, rvjxkc);
    }

    public void StartPreview(TeachingCoursesBean.RowsBean rowsBean) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ClassAllPreviewContentAct");
        intent.putExtra("lessonId", rowsBean.getLessonId());
        intent.putExtra("titlename", rowsBean.getCourseName());
        startActivity(intent);
    }

    public void StartTeaching(TeachingCoursesBean.RowsBean rowsBean) {
        choiceClassPopupView = new ChoiceClassPopupView(getActivity(), rowsBean);
        new XPopup.Builder(getActivity())
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(choiceClassPopupView)
                .show();
    }

    /**
     * 册别数据开始
     */
    private int checkedPos = 0;//默认学科
    List<TeacherVolumeBean.DataBean.BookVolumeResBean> dataList;//学科集合

    private void volumeshow(View view) {
        final List<String> stringList = new ArrayList<>();
        if (dataList==null){
            return;
        }
        if (dataList.size() != 0) {
            for (int i = 0; i < dataList.size(); i++) {
                stringList.add(dataList.get(i).getName());
            }
        }
        String[] title1 = stringList.toArray(new String[stringList.size()]);
        new XPopup.Builder(getActivity())
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .offsetY(20)
                .offsetX(40)
                .atView(view)  //依附于所点击的View，内部会自动判断在上方或者下方显示 new String[]{"语文", "数学", "物理", "化学"}
                .asAttachList1(title1,
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
                                tvVolumeName.setText(stringList.get(position));
                                containerView.removeAllViews();
                                treePresenter.queryTree(Token, dataList.get(position).getBookVolumeId());
                            }
                        }, R.layout.popup_subject_adapter_rv, R.layout.popup_subject_adapter_item, checkedPos)
                .show();
    }

    /*册别接口成功*/
    @Override
    public void onTeacherVolumeSuccess(TeacherVolumeBean teacherVolumeBean) {
        emptyview1.success();
        if (teacherVolumeBean == null) {
            return;
        }
        dataList = new ArrayList<>();
        for (int i = 0; i < teacherVolumeBean.getData().size(); i++) {
            if (teacherVolumeBean.getData().get(i).getSubjectId().equals(SPUtils.getInstance().getString("shouye_class_id"))) {
                dataList.addAll(teacherVolumeBean.getData().get(i).getBookVolumeRes());
            }
        }
        if (dataList != null && dataList.size() > 0) {
            tvVolumeName.setText(dataList.get(0).getName());
            treePresenter.queryTree(Token, dataList.get(0).getBookVolumeId());
        }
    }

    @Override
    public void onTeacherVolumeNodata(String msg) {
        emptyview1.errorNet();
        ToastUtils.showLong(msg);
    }

    @Override
    public void onTeacherVolumeFail(String msg) {
        emptyview1.errorNet();
        ToastUtils.showLong(msg);
    }
    /*册别数据结束*/


    /**
     * 树结构信息开始
     * tree学科信息
     */

    @Override
    public void onTreeSuccess(final List<TreeBean> treeBeanList) {
        if (treeBeanList == null) {
            return;
        }
        TreeNode nodeRoot = TreeNode.root();
        createTreeNodes(nodeRoot, treeBeanList);
        tView = new AndroidTreeView(getActivity(), nodeRoot);
        tView.setDefaultAnimation(true);
        //tView.setUse2dScroll(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleNo, false);
        containerView.addView(tView.getView(R.style.TreeNodeStyleNo));

//        if (treeBeanList.size() > 0 && treeBeanList.get(0).getChildren().size() > 0 && nodeRoot.getChildren().size() > 0) {
//            tView.expandNode(nodeRoot.getChildren().get(0));
//            tView.expandLevel(0);
//            chapterId = treeBeanList.get(0).getChildren().get(0).getData().getId();
//            myHolder = (ContentHolder) nodeRoot.getChildren().get(0).getChildren().get(0).getViewHolder();
//            myHolder.setSelected();
//        }

        TreeNode nodeFirst = nodeRoot;
        for (int i = 0; i < 3; i++) {
            if (nodeFirst.getChildren().size() > 0) {
                nodeFirst = nodeFirst.getChildren().get(0);
                if (nodeFirst.getChildren().size() < 1) {
                    tView.expandNode(nodeFirst.getParent());
                    tView.expandLevel(0);
                    chapterId = ((IconTreeItem) nodeFirst.getValue()).id;
                    myHolder = (ContentHolder) nodeFirst.getViewHolder();
                    myHolder.setSelected();
                }
            }
        }
        tView.setDefaultNodeClickListener(nodeClickListener);
        setDatas();
    }

    private void createTreeNodes(TreeNode treeNode, List<TreeBean> dataParentBeans) {
        for (TreeBean dataParentBean : dataParentBeans) {
            TreeNode node = initTreeNode(dataParentBean.getData().getName(), dataParentBean.getData().getId());
            treeNode.addChild(node);
            if (dataParentBean != null && !dataParentBean.getChildren().isEmpty()) {
                createTreeNodes(node, dataParentBean.getChildren(), dataParentBean.getData().getId());
            }
        }
    }

    private void createTreeNodes(TreeNode treeNode, List<TreeBean.ChildrenBean> dataChildBeans, String parentId) {
        for (TreeBean.ChildrenBean dataChildBean : dataChildBeans) {
            if (dataChildBean.getData().getParentId().equals(parentId)) {
                TreeNode node = initTreeNode(dataChildBean.getData().getName(), dataChildBean.getData().getId());
                treeNode.addChild(node);
                createTreeNodes(node, dataChildBean.getChildren(), dataChildBean.getData().getId());
            }
        }
    }

    //    每一条数据
    private TreeNode initTreeNode(String name, String id) {
        return new TreeNode(new IconTreeItem(R.drawable.ic_tree_wei, name, id))
                .setViewHolder(new ContentHolder(getActivity()));  //p 图标样式   p内容展示
    }


    private TreeNode.TreeNodeClickListener nodeClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            if (node.getChildren().size() > 0) {
                return;
            }

            if (myHolder != null) {
                myHolder.setUnselected();
            }
            myHolder = (ContentHolder) node.getViewHolder();
            myHolder.setSelected();

            chapterId = ((IconTreeItem) value).id;
            setDatas();
        }
    };

    @Override
    public void onTreeNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onTreeFail(String msg) {
        ToastUtils.showLong(msg);
    }
    /*树结构信息结束*/


    /**
     * 界面列表开始
     */
    /*刷新列表*/
    private void RefreshLoad() {
        teachingCoursesPresenter.queryTeachingCourses(Token, chapterId, page, PAGE_SIZE);
    }

    /*列表内容*/
    @Override
    public void onTeachingCoursesSuccess(TeachingCoursesBean teachingCoursesBean) {
        if (teachingCoursesBean == null) {
            return;
        }
        refreshLayout1.finishRefresh(true);
        rowsBeanList = teachingCoursesBean.getRows();
        if (page == 1) {
            if (rowsBeanList == null) {
                TeachingNodata.setVisibility(View.VISIBLE);
            } else {
                TeachingNodata.setVisibility(View.GONE);
            }
            setData(true, rowsBeanList);
        } else {
            setData(false, rowsBeanList);
        }
    }

    @Override
    public void onTeachingCoursesNodata(String msg) {
        ToastUtils.showLong(msg);
        refreshLayout1.finishRefresh(false);
        if (page == 1) {
            teachingCoursesAdapter.setEnableLoadMore(true);
        } else {
            teachingCoursesAdapter.loadMoreFail();
        }
    }

    @Override
    public void onTeachingCoursesFail(String msg) {
        ToastUtils.showLong(msg);
        refreshLayout1.finishRefresh(false);
        if (page == 1) {
            teachingCoursesAdapter.setEnableLoadMore(true);
            // 根据需求修改bufen
            teachingCoursesAdapter.setNewData(null);
        } else {
            teachingCoursesAdapter.loadMoreFail();
        }
    }
    /* 界面列表结束*/


    private void setData(boolean isRefresh, List data) {
        page++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            teachingCoursesAdapter.setNewData(data);
        } else {
            if (size > 0) {
                teachingCoursesAdapter.addData(data);
            }
        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            teachingCoursesAdapter.loadMoreEnd(isRefresh);
//            ToastUtils.showLong("已经到底了");
        } else {
            teachingCoursesAdapter.loadMoreComplete();
        }
    }

    /**
     * 加载第一页数据bufen
     */
    private void setDatas() {
        page = 1;
        teachingCoursesAdapter.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        RefreshLoad();
    }

    /**
     * 加载更多bufen
     */
    private void loadMore() {
        RefreshLoad();
    }

    /*WebView绑定控件*/
    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }


    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }

    @Override
    public void onDestroy() {
        if (teachingCoursesPresenter != null) {
            teachingCoursesPresenter.onDestory();
            treePresenter.onDestory();
            teacherVolumePresenter.onDestory();
        }
        super.onDestroy();
    }

    @Override
    public void OnSuccess(Object msg) {

    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

}
