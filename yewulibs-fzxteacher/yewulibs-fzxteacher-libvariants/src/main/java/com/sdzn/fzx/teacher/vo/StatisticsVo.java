package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class StatisticsVo {
    /**
     * data : {"unAnswer":6,"replenishPre":0,"answer":0,"answerPre":100,"completePre":0,"scope":-1,"endTime":1548684000000,"id":2041,"complete":0,"replenish":0,"scoreAvg":-1,"status":1}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * unAnswer : 6
         * replenishPre : 0
         * answer : 0
         * answerPre : 100
         * completePre : 0
         * scope : -1
         * endTime : 1548684000000
         * id : 2041
         * complete : 0
         * replenish : 0
         * scoreAvg : -1
         * status : 1
         */

        private int unAnswer;
        private float replenishPre;
        private int answer;
        private Double answerPre;
        private Double completePre;
        private int scope;
        private long endTime;
        private int id;
        private int complete;
        private int replenish;
        private double scoreAvg;
        private int status;

        public int getUnAnswer() {
            return unAnswer;
        }

        public void setUnAnswer(int unAnswer) {
            this.unAnswer = unAnswer;
        }

        public float getReplenishPre() {
            return replenishPre;
        }

        public void setReplenishPre(float replenishPre) {
            this.replenishPre = replenishPre;
        }

        public int getAnswer() {
            return answer;
        }

        public void setAnswer(int answer) {
            this.answer = answer;
        }

        public Double getAnswerPre() {
            return answerPre;
        }

        public void setAnswerPre(Double answerPre) {
            this.answerPre = answerPre;
        }

        public Double getCompletePre() {
            return completePre;
        }

        public void setCompletePre(Double completePre) {
            this.completePre = completePre;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getComplete() {
            return complete;
        }

        public void setComplete(int complete) {
            this.complete = complete;
        }

        public int getReplenish() {
            return replenish;
        }

        public void setReplenish(int replenish) {
            this.replenish = replenish;
        }

        public double getScoreAvg() {
            return scoreAvg;
        }

        public void setScoreAvg(double scoreAvg) {
            this.scoreAvg = scoreAvg;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
