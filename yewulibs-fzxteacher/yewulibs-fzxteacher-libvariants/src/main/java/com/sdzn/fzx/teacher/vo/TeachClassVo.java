package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/11
 * 修改单号：
 * 修改内容:
 */
public class TeachClassVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * classId : 610
         * baseGradeName : 七年级
         * isClassManager : true
         * className : .四班
         * classManagerName : 邵坤
         * baseSubjectName : 语文
         * classManagerId : 198
         * baseGradeId : 7
         * baseSubjectId : 1
         */

        private int classId;
        private String baseGradeName;
        private boolean isClassManager;
        private String className;
        private String classManagerName;
        private String baseSubjectName;
        private int classManagerId;
        private int baseGradeId;
        private int baseSubjectId;

        private boolean selected;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public boolean isIsClassManager() {
            return isClassManager;
        }

        public void setIsClassManager(boolean isClassManager) {
            this.isClassManager = isClassManager;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassManagerName() {
            return classManagerName;
        }

        public void setClassManagerName(String classManagerName) {
            this.classManagerName = classManagerName;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getClassManagerId() {
            return classManagerId;
        }

        public void setClassManagerId(int classManagerId) {
            this.classManagerId = classManagerId;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }
    }
}
