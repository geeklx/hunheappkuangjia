package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.ClassRecordBean;
import com.sdzn.fzx.teacher.view.ClassRecordViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -上课记录列表
 */
public class ClassRecordPresenter extends Presenter<ClassRecordViews> {

    public void queryClassRecord(String token,String chapterId,String classId,String keyWord,String startTime,String endTime,int page,int limit) {
        JSONObject requestData = new JSONObject();
        requestData.put("chapterId", chapterId);
        requestData.put("classId", classId);
        requestData.put("keyWord", keyWord);
        requestData.put("startTime", startTime);
        requestData.put("endTime", endTime);
        requestData.put("page", page);
        requestData.put("limit", limit);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());


        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryclassrecord(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/ClassRoomTesting/classRecordList", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<ClassRecordBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ClassRecordBean>> call, Response<ResponseSlbBean1<ClassRecordBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onClassRecordNodata(response.body().getMessage());
                            return;
                        }
                        getView().onClassRecordSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ClassRecordBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onClassRecordFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
