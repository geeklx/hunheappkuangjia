package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ClassroomTestingBean;

/**
 * 首页 view
 */
public interface ClassroomTestingViews extends IView {
    void onClassroomTestingSuccess(ClassroomTestingBean classroomTestingBean);

    void onClassroomTestingNodata(String msg);

    void onClassroomTestingFail(String msg);
}
