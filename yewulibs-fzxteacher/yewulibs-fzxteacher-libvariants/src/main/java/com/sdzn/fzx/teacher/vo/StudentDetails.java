package com.sdzn.fzx.teacher.vo;


import androidx.annotation.NonNull;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class StudentDetails {

    private String extend;
    private int total;
    private List<DataBean> data;
    private String num;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private AnswerBean answer;
        private String comment;

        public AnswerBean getAnswer() {
            return answer;
        }

        public void setAnswer(AnswerBean answer) {
            this.answer = answer;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public static class AnswerBean {
            private int id;
            private int isAnswer;
            private int isCorrect;
            private int isRight;
            private Object timeAnswer;
            private long timeSubmit;
            private int score;
            private int fullScore;
            private int lessonTaskId;
            private int lessonTaskStudentId;
            private int lessonTaskDetailsId;
            private int correctType;
            private int useTime;
            private int templateId;
            private int templateStyleId;
            private String templateStyleName;
            private int examSeq;
            private String examText;
            private int parentId;
            private int emptyCount;
            private int userStudentId;
            private String userStudentName;
            private Object examType;
            private int lessonLibId;
            private String taskExamId;
            private Object answerType;
            private List<DataBean.AnswerBean> examList;
            private List<ExamOptionListBean> examOptionList;
            private ExamText ExamTextVo;

            public ExamText getExamTextVo() {
                return ExamTextVo;
            }

            public void setExamTextVo(ExamText examTextVo) {
                ExamTextVo = examTextVo;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getIsAnswer() {
                return isAnswer;
            }

            public void setIsAnswer(int isAnswer) {
                this.isAnswer = isAnswer;
            }

            public int getIsCorrect() {
                return isCorrect;
            }

            public void setIsCorrect(int isCorrect) {
                this.isCorrect = isCorrect;
            }

            public int getIsRight() {
                return isRight;
            }

            public void setIsRight(int isRight) {
                this.isRight = isRight;
            }

            public Object getTimeAnswer() {
                return timeAnswer;
            }

            public void setTimeAnswer(Object timeAnswer) {
                this.timeAnswer = timeAnswer;
            }

            public long getTimeSubmit() {
                return timeSubmit;
            }

            public void setTimeSubmit(long timeSubmit) {
                this.timeSubmit = timeSubmit;
            }

            public int getScore() {
                return score;
            }

            public void setScore(int score) {
                this.score = score;
            }

            public int getFullScore() {
                return fullScore;
            }

            public void setFullScore(int fullScore) {
                this.fullScore = fullScore;
            }

            public int getLessonTaskId() {
                return lessonTaskId;
            }

            public void setLessonTaskId(int lessonTaskId) {
                this.lessonTaskId = lessonTaskId;
            }

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public int getLessonTaskDetailsId() {
                return lessonTaskDetailsId;
            }

            public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
                this.lessonTaskDetailsId = lessonTaskDetailsId;
            }

            public int getCorrectType() {
                return correctType;
            }

            public void setCorrectType(int correctType) {
                this.correctType = correctType;
            }

            public int getUseTime() {
                return useTime;
            }

            public void setUseTime(int useTime) {
                this.useTime = useTime;
            }

            public int getTemplateId() {
                return templateId;
            }

            public void setTemplateId(int templateId) {
                this.templateId = templateId;
            }

            public int getTemplateStyleId() {
                return templateStyleId;
            }

            public void setTemplateStyleId(int templateStyleId) {
                this.templateStyleId = templateStyleId;
            }

            public String getTemplateStyleName() {
                return templateStyleName;
            }

            public void setTemplateStyleName(String templateStyleName) {
                this.templateStyleName = templateStyleName;
            }

            public int getExamSeq() {
                return examSeq;
            }

            public void setExamSeq(int examSeq) {
                this.examSeq = examSeq;
            }

            public String getExamText() {
                return examText;
            }

            public void setExamText(String examText) {
                this.examText = examText;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getEmptyCount() {
                return emptyCount;
            }

            public void setEmptyCount(int emptyCount) {
                this.emptyCount = emptyCount;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public Object getExamType() {
                return examType;
            }

            public void setExamType(Object examType) {
                this.examType = examType;
            }

            public int getLessonLibId() {
                return lessonLibId;
            }

            public void setLessonLibId(int lessonLibId) {
                this.lessonLibId = lessonLibId;
            }

            public String getTaskExamId() {
                return taskExamId;
            }

            public void setTaskExamId(String taskExamId) {
                this.taskExamId = taskExamId;
            }

            public Object getAnswerType() {
                return answerType;
            }

            public void setAnswerType(Object answerType) {
                this.answerType = answerType;
            }

            public List<DataBean.AnswerBean> getExamList() {
                return examList;
            }

            public void setExamList(List<DataBean.AnswerBean> examList) {
                this.examList = examList;
            }

            public List<ExamOptionListBean> getExamOptionList() {
                return examOptionList;
            }

            public void setExamOptionList(List<ExamOptionListBean> examOptionList) {
                this.examOptionList = examOptionList;
            }

            public static class ExamOptionListBean implements Comparable<ExamOptionListBean> {
                /**
                 * id : 43169
                 * lessonAnswerExamId : 77507
                 * isRight : -1
                 * score : -1
                 * myAnswer : http://csfile.fuzhuxian.com/a638de35-b0b4-42e1-8ad0-8bc4471b34ec.jpg
                 * myAnswerHtml : null
                 * seq : 1
                 * lessonTaskStudentId : 18321
                 * myOldAnswer : http://csfile.fuzhuxian.com/a638de35-b0b4-42e1-8ad0-8bc4471b34ec.jpg
                 * clozeSeq : null
                 * templateStyleName : null
                 * examSeq : null
                 * answerType : 3
                 */

                private int id;
                private int lessonAnswerExamId;
                private int isRight;
                private int score;
                private String myAnswer;
                private Object myAnswerHtml;
                private int seq;
                private int lessonTaskStudentId;
                private String myOldAnswer;
                private Object clozeSeq;
                private Object templateStyleName;
                private Object examSeq;
                private int answerType;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getLessonAnswerExamId() {
                    return lessonAnswerExamId;
                }

                public void setLessonAnswerExamId(int lessonAnswerExamId) {
                    this.lessonAnswerExamId = lessonAnswerExamId;
                }

                public int getIsRight() {
                    return isRight;
                }

                public void setIsRight(int isRight) {
                    this.isRight = isRight;
                }

                public int getScore() {
                    return score;
                }

                public void setScore(int score) {
                    this.score = score;
                }

                public String getMyAnswer() {
                    return myAnswer;
                }

                public void setMyAnswer(String myAnswer) {
                    this.myAnswer = myAnswer;
                }

                public Object getMyAnswerHtml() {
                    return myAnswerHtml;
                }

                public void setMyAnswerHtml(Object myAnswerHtml) {
                    this.myAnswerHtml = myAnswerHtml;
                }

                public int getSeq() {
                    return seq;
                }

                public void setSeq(int seq) {
                    this.seq = seq;
                }

                public int getLessonTaskStudentId() {
                    return lessonTaskStudentId;
                }

                public void setLessonTaskStudentId(int lessonTaskStudentId) {
                    this.lessonTaskStudentId = lessonTaskStudentId;
                }

                public String getMyOldAnswer() {
                    return myOldAnswer;
                }

                public void setMyOldAnswer(String myOldAnswer) {
                    this.myOldAnswer = myOldAnswer;
                }

                public Object getClozeSeq() {
                    return clozeSeq;
                }

                public void setClozeSeq(Object clozeSeq) {
                    this.clozeSeq = clozeSeq;
                }

                public Object getTemplateStyleName() {
                    return templateStyleName;
                }

                public void setTemplateStyleName(Object templateStyleName) {
                    this.templateStyleName = templateStyleName;
                }

                public Object getExamSeq() {
                    return examSeq;
                }

                public void setExamSeq(Object examSeq) {
                    this.examSeq = examSeq;
                }

                public int getAnswerType() {
                    return answerType;
                }

                public void setAnswerType(int answerType) {
                    this.answerType = answerType;
                }

                @Override
                public int compareTo(@NonNull ExamOptionListBean o) {
                    return Integer.compare(seq, o.seq);
                }
            }
        }

        public static class ExamOptions implements Comparable<ExamOptions> {
            private String content;
            private String analysis;
            private boolean right;
            private int seq;//选项序号
            private List<ExamOptions> list;

            public List<ExamOptions> getList() {
                return list;
            }

            public void setList(List<ExamOptions> list) {
                this.list = list;
            }

            public String getAnalysis() {
                return analysis;
            }

            public void setAnalysis(String analysis) {
                this.analysis = analysis;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public boolean isRight() {
                return right;
            }

            public void setRight(boolean right) {
                this.right = right;
            }

            public int getSeq() {
                return seq;
            }

            public void setSeq(int seq) {
                this.seq = seq;
            }

            @Override
            public int compareTo(@NonNull ExamOptions o) {
                return Integer.compare(seq, o.seq);
            }
        }
    }
}
/*
    private String extend;
    private int total;
    private List<DataBean> data;
    private String num;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        *//**
 * id : 505265
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19970
 * correctType : 3
 * useTime : 0
 * templateId : 16
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnswer":"","examBases":[{"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202},{"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","difficulty":1,"examAnalysis":"","examAnswer":"1111111111111111","examExplain":"","examOptions":[{"analysis":"","content":"1111111111111111","right":true,"seq":1}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊<input index='1' class='cus-com' readonly='readonly' type='text' value='(1)'/></p>","examTypeId":6,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"optionNumber":1,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202},{"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","difficulty":1,"examAnalysis":"","examAnswer":"<p>11111111111111111111111111111</p>","examExplain":"","examOptions":[],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":4,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"optionNumber":0,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}],"examExplain":"","examId":"5c4eaaca1cb0766c620842a0","examStem":"<p>我是一道综合题</p>","examTypeId":16,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a8","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":0,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202,"zoneIdPath":"0.15.223.5025","zoneName":"高新区"}
 * parentId : 0
 * emptyCount : 0
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : [{"id":505266,"isAnswer":0,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":null,"score":-1,"fullScore":-1,"lessonTaskId":2047,"lessonTaskStudentId":71107,"lessonTaskDetailsId":19971,"correctType":3,"useTime":0,"templateId":1,"templateStyleId":338,"templateStyleName":"综合题","examSeq":1,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":3,\"baseSubjectName\":\"英语\",\"baseVersionId\":3,\"baseVersionName\":\"人教新目标版\",\"baseVolumeId\":10,\"baseVolumeName\":\"上册\",\"chapterId\":0,\"chapterName\":\"全部章节\",\"chapterNodeIdPath\":\"0\",\"chapterNodeNamePath\":\"全部章节\",\"createTime\":1548659392202,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"examAnalysis\":\"\",\"examAnswer\":\"D\",\"examExplain\":\"\",\"examOptions\":[{\"analysis\":\"\",\"content\":\"<p>11111111111<\/p>\",\"right\":false,\"seq\":1},{\"analysis\":\"\",\"content\":\"<p>11111111111111<\/p>\",\"right\":false,\"seq\":2},{\"analysis\":\"\",\"content\":\"<p>111111111111111<\/p>\",\"right\":false,\"seq\":3},{\"analysis\":\"\",\"content\":\"<p>11111111111111111<\/p>\",\"right\":true,\"seq\":4}],\"examStem\":\"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊<\/p>\",\"examTypeId\":1,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5c4eaad21cb0766c620842a9\",\"lessonLibId\":2450,\"lessonTaskId\":2047,\"optionNumber\":4,\"points\":[],\"templateStyleId\":338,\"templateStyleName\":\"综合题\",\"type\":1,\"updateTime\":1548659392202}","parentId":505265,"emptyCount":4,"userStudentId":5166,"userStudentName":"学生62","examType":null,"lessonLibId":2450,"examList":[],"examOptionList":[],"taskExamId":"5c4eaad21cb0766c620842a9"},{"id":505267,"isAnswer":0,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":null,"score":-1,"fullScore":-1,"lessonTaskId":2047,"lessonTaskStudentId":71107,"lessonTaskDetailsId":19972,"correctType":3,"useTime":0,"templateId":6,"templateStyleId":338,"templateStyleName":"综合题","examSeq":2,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":3,\"baseSubjectName\":\"英语\",\"baseVersionId\":3,\"baseVersionName\":\"人教新目标版\",\"baseVolumeId\":10,\"baseVolumeName\":\"上册\",\"chapterId\":0,\"chapterName\":\"全部章节\",\"chapterNodeIdPath\":\"0\",\"chapterNodeNamePath\":\"全部章节\",\"createTime\":1548659392202,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"examAnalysis\":\"\",\"examAnswer\":\"1111111111111111\",\"examExplain\":\"\",\"examOptions\":[{\"analysis\":\"\",\"content\":\"1111111111111111\",\"right\":true,\"seq\":1}],\"examStem\":\"<p>啊啊啊啊啊啊啊啊啊啊啊啊<input index='1' class='cus-com' readonly='readonly' type='text' value='(1)'/><\/p>\",\"examTypeId\":6,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5c4eaad21cb0766c620842aa\",\"lessonLibId\":2450,\"lessonTaskId\":2047,\"optionNumber\":1,\"points\":[],\"templateStyleId\":338,\"templateStyleName\":\"综合题\",\"type\":1,\"updateTime\":1548659392202}","parentId":505265,"emptyCount":1,"userStudentId":5166,"userStudentName":"学生62","examType":null,"lessonLibId":2450,"examList":[],"examOptionList":[],"taskExamId":"5c4eaad21cb0766c620842aa"},{"id":505268,"isAnswer":0,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":null,"score":-1,"fullScore":-1,"lessonTaskId":2047,"lessonTaskStudentId":71107,"lessonTaskDetailsId":19973,"correctType":3,"useTime":0,"templateId":4,"templateStyleId":338,"templateStyleName":"综合题","examSeq":3,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":3,\"baseSubjectName\":\"英语\",\"baseVersionId\":3,\"baseVersionName\":\"人教新目标版\",\"baseVolumeId\":10,\"baseVolumeName\":\"上册\",\"chapterId\":0,\"chapterName\":\"全部章节\",\"chapterNodeIdPath\":\"0\",\"chapterNodeNamePath\":\"全部章节\",\"createTime\":1548659392202,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"examAnalysis\":\"\",\"examAnswer\":\"<p>11111111111111111111111111111<\/p>\",\"examExplain\":\"\",\"examOptions\":[],\"examStem\":\"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊<\/p>\",\"examTypeId\":4,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5c4eaad21cb0766c620842ab\",\"lessonLibId\":2450,\"lessonTaskId\":2047,\"optionNumber\":0,\"points\":[],\"templateStyleId\":338,\"templateStyleName\":\"综合题\",\"type\":1,\"updateTime\":1548659392202}","parentId":505265,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examType":null,"lessonLibId":2450,"examList":[],"examOptionList":[],"taskExamId":"5c4eaad21cb0766c620842ab"}]
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a8
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 * <p>
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 * <p>
 * 答题内容bean
 * <p>
 * 先按;切, 再按,切
 *//*
        private int id;
        private int isAnswer;
        private int isCorrect;
        private int isRight;
        private Object timeAnswer;
        private Object timeSubmit;
        private String score;
        private int fullScore;
        private int lessonTaskId;
        private int lessonTaskStudentId;
        private int lessonTaskDetailsId;
        private int correctType;
        private int useTime;
        private int templateId;
        private int templateStyleId;
        private String templateStyleName;
        private int examSeq;
        private String examText;
        private int parentId;
        private int emptyCount;
        private int userStudentId;
        private String userStudentName;
        private Object examType;
        private int lessonLibId;
        private String taskExamId;
        private List<DataBean> examList;
        private List<ExamOptionBean> examOptionList;
        private ExamText ExamTextVo;

        public ExamText getExamTextVo() {
            return ExamTextVo;
        }

        public void setExamTextVo(ExamText examTextVo) {
            ExamTextVo = examTextVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsAnswer() {
            return isAnswer;
        }

        public void setIsAnswer(int isAnswer) {
            this.isAnswer = isAnswer;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public Object getTimeAnswer() {
            return timeAnswer;
        }

        public void setTimeAnswer(Object timeAnswer) {
            this.timeAnswer = timeAnswer;
        }

        public Object getTimeSubmit() {
            return timeSubmit;
        }

        public void setTimeSubmit(Object timeSubmit) {
            this.timeSubmit = timeSubmit;
        }

        public String getScore() {
            return new BigDecimal(score).stripTrailingZeros().toPlainString();
        }

        public void setScore(String score) {
            this.score = score;
        }

        public int getFullScore() {
            return fullScore;
        }

        public void setFullScore(int fullScore) {
            this.fullScore = fullScore;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public int getLessonTaskDetailsId() {
            return lessonTaskDetailsId;
        }

        public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
            this.lessonTaskDetailsId = lessonTaskDetailsId;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public int getTemplateId() {
            return templateId;
        }

        public void setTemplateId(int templateId) {
            this.templateId = templateId;
        }

        public int getTemplateStyleId() {
            return templateStyleId;
        }

        public void setTemplateStyleId(int templateStyleId) {
            this.templateStyleId = templateStyleId;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getEmptyCount() {
            return emptyCount;
        }

        public void setEmptyCount(int emptyCount) {
            this.emptyCount = emptyCount;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public Object getExamType() {
            return examType;
        }

        public void setExamType(Object examType) {
            this.examType = examType;
        }

        public int getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(int lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public String getTaskExamId() {
            return taskExamId;
        }

        public void setTaskExamId(String taskExamId) {
            this.taskExamId = taskExamId;
        }

        public List<DataBean> getExamList() {
            return examList;
        }

        public void setExamList(List<DataBean> examList) {
            this.examList = examList;
        }

        public List<ExamOptionBean> getExamOptionList() {
            return examOptionList;
        }

        public void setExamOptionList(List<ExamOptionBean> examOptionList) {
            this.examOptionList = examOptionList;
        }

        public static class ExamListBean {
            *//**
 * id : 505266
 * isAnswer : 0
 * isCorrect : 0
 * isRight : 0
 * timeAnswer : null
 * timeSubmit : null
 * score : -1.0
 * fullScore : -1.0
 * lessonTaskId : 2047
 * lessonTaskStudentId : 71107
 * lessonTaskDetailsId : 19971
 * correctType : 3
 * useTime : 0
 * templateId : 1
 * templateStyleId : 338
 * templateStyleName : 综合题
 * examSeq : 1
 * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","baseVolumeId":10,"baseVolumeName":"上册","chapterId":0,"chapterName":"全部章节","chapterNodeIdPath":"0","chapterNodeNamePath":"全部章节","createTime":1548659392202,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"examAnalysis":"","examAnswer":"D","examExplain":"","examOptions":[{"analysis":"","content":"<p>11111111111</p>","right":false,"seq":1},{"analysis":"","content":"<p>11111111111111</p>","right":false,"seq":2},{"analysis":"","content":"<p>111111111111111</p>","right":false,"seq":3},{"analysis":"","content":"<p>11111111111111111</p>","right":true,"seq":4}],"examStem":"<p>啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊</p>","examTypeId":1,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5c4eaad21cb0766c620842a9","lessonLibId":2450,"lessonTaskId":2047,"optionNumber":4,"points":[],"templateStyleId":338,"templateStyleName":"综合题","type":1,"updateTime":1548659392202}
 * parentId : 505265
 * emptyCount : 4
 * userStudentId : 5166
 * userStudentName : 学生62
 * examType : null
 * lessonLibId : 2450
 * examList : []
 * examOptionList : []
 * taskExamId : 5c4eaad21cb0766c620842a9
 *//*

            private int id;
            private int isAnswer;
            private int isCorrect;
            private int isRight;
            private Object timeAnswer;
            private Object timeSubmit;
            private double score;
            private double fullScore;
            private int lessonTaskId;
            private int lessonTaskStudentId;
            private int lessonTaskDetailsId;
            private int correctType;
            private int useTime;
            private int templateId;
            private int templateStyleId;
            private String templateStyleName;
            private int examSeq;
            private String examText;
            private int parentId;
            private int emptyCount;
            private int userStudentId;
            private String userStudentName;
            private Object examType;
            private int lessonLibId;
            private String taskExamId;
            private List<?> examList;
            private List<?> examOptionList;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getIsAnswer() {
                return isAnswer;
            }

            public void setIsAnswer(int isAnswer) {
                this.isAnswer = isAnswer;
            }

            public int getIsCorrect() {
                return isCorrect;
            }

            public void setIsCorrect(int isCorrect) {
                this.isCorrect = isCorrect;
            }

            public int getIsRight() {
                return isRight;
            }

            public void setIsRight(int isRight) {
                this.isRight = isRight;
            }

            public Object getTimeAnswer() {
                return timeAnswer;
            }

            public void setTimeAnswer(Object timeAnswer) {
                this.timeAnswer = timeAnswer;
            }

            public Object getTimeSubmit() {
                return timeSubmit;
            }

            public void setTimeSubmit(Object timeSubmit) {
                this.timeSubmit = timeSubmit;
            }

            public double getScore() {
                return score;
            }

            public void setScore(double score) {
                this.score = score;
            }

            public double getFullScore() {
                return fullScore;
            }

            public void setFullScore(double fullScore) {
                this.fullScore = fullScore;
            }

            public int getLessonTaskId() {
                return lessonTaskId;
            }

            public void setLessonTaskId(int lessonTaskId) {
                this.lessonTaskId = lessonTaskId;
            }

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public int getLessonTaskDetailsId() {
                return lessonTaskDetailsId;
            }

            public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
                this.lessonTaskDetailsId = lessonTaskDetailsId;
            }

            public int getCorrectType() {
                return correctType;
            }

            public void setCorrectType(int correctType) {
                this.correctType = correctType;
            }

            public int getUseTime() {
                return useTime;
            }

            public void setUseTime(int useTime) {
                this.useTime = useTime;
            }

            public int getTemplateId() {
                return templateId;
            }

            public void setTemplateId(int templateId) {
                this.templateId = templateId;
            }

            public int getTemplateStyleId() {
                return templateStyleId;
            }

            public void setTemplateStyleId(int templateStyleId) {
                this.templateStyleId = templateStyleId;
            }

            public String getTemplateStyleName() {
                return templateStyleName;
            }

            public void setTemplateStyleName(String templateStyleName) {
                this.templateStyleName = templateStyleName;
            }

            public int getExamSeq() {
                return examSeq;
            }

            public void setExamSeq(int examSeq) {
                this.examSeq = examSeq;
            }

            public String getExamText() {
                return examText;
            }

            public void setExamText(String examText) {
                this.examText = examText;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getEmptyCount() {
                return emptyCount;
            }

            public void setEmptyCount(int emptyCount) {
                this.emptyCount = emptyCount;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public Object getExamType() {
                return examType;
            }

            public void setExamType(Object examType) {
                this.examType = examType;
            }

            public int getLessonLibId() {
                return lessonLibId;
            }

            public void setLessonLibId(int lessonLibId) {
                this.lessonLibId = lessonLibId;
            }

            public String getTaskExamId() {
                return taskExamId;
            }

            public void setTaskExamId(String taskExamId) {
                this.taskExamId = taskExamId;
            }

            public List<?> getExamList() {
                return examList;
            }

            public void setExamList(List<?> examList) {
                this.examList = examList;
            }

            public List<?> getExamOptionList() {
                return examOptionList;
            }

            public void setExamOptionList(List<?> examOptionList) {
                this.examOptionList = examOptionList;
            }
        }
    }


    *//**
 * 答题内容bean
 *//*
    public static class ExamOptionBean implements Comparable<ExamOptionBean> {

        private int id;//试题答案选项id
        private int isRight;//是否正确答案
        private int lessonAnswerExamId;//试题答案id
        private int lessonTaskStudentId;//学生对应试卷的id
        private double score;//学生对应试卷的id

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        *//**
 * 先按;切, 再按,切
 *//*
        private String myAnswer;//答题内容, 填空题
        private String myAnswerHtml;
        private String myOldAnswer;//最开始提交的作答结果集
        private int seq;//选择:第几个选项;填空:第几个空;完形填空:第几个小题;简答:2=拍照1=手写;
        private int answerType;//填空：0 = 未作答，1 = 键盘作答， 2 = 手写作答， 3 = 拍照作答

        public int getAnswerType() {
            return answerType;
        }

        public void setAnswerType(int answerType) {
            this.answerType = answerType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public int getLessonAnswerExamId() {
            return lessonAnswerExamId;
        }

        public void setLessonAnswerExamId(int lessonAnswerExamId) {
            this.lessonAnswerExamId = lessonAnswerExamId;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public String getMyAnswer() {
            return myAnswer;
        }

        public void setMyAnswer(String myAnswer) {
            this.myAnswer = myAnswer;
        }

        public String getMyAnswerHtml() {
            return myAnswerHtml;
        }

        public void setMyAnswerHtml(String myAnswerHtml) {
            this.myAnswerHtml = myAnswerHtml;
        }

        public String getMyOldAnswer() {
            return myOldAnswer;
        }

        public void setMyOldAnswer(String myOldAnswer) {
            this.myOldAnswer = myOldAnswer;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptionBean o) {
            return Integer.compare(seq, o.seq);
        }
    }

    public static class ExamOptions implements Comparable<ExamOptions> {
        private String content;
        private String analysis;
        private boolean right;
        private int seq;//选项序号
        private List<ExamOptions> list;

        public List<ExamOptions> getList() {
            return list;
        }

        public void setList(List<ExamOptions> list) {
            this.list = list;
        }

        public String getAnalysis() {
            return analysis;
        }

        public void setAnalysis(String analysis) {
            this.analysis = analysis;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptions o) {
            return Integer.compare(seq, o.seq);
        }
    }
}*/
