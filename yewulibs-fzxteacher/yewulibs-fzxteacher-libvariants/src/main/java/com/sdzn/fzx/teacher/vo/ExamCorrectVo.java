package com.sdzn.fzx.teacher.vo;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/13
 * 修改单号：
 * 修改内容:
 */
public class ExamCorrectVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 505408
         * isAnswer : 1
         * isCorrect : 0
         * isRight : 0
         * timeAnswer : null
         * timeSubmit : 1548850665000
         * score : -1.0
         * fullScore : -1.0
         * lessonTaskId : 2053
         * lessonTaskStudentId : 71149
         * lessonTaskDetailsId : 20000
         * correctType : 2
         * useTime : 0
         * templateId : 4
         * templateStyleId : 359
         * templateStyleName : 默写
         * examSeq : 1
         * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","baseVolumeId":1,"baseVolumeName":"上册","chapterId":2888,"chapterName":"第一单元","chapterNodeIdPath":"0.2888","chapterNodeNamePath":"第一单元","createTime":1544690350661,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"downCount":3,"examAnalysis":"答案：<br />日月之行&nbsp; 若出其中&nbsp; 星汉灿烂&nbsp; 若出其里（重点字：星汉）","examAnswer":"1. 日月之行 <br>2. 若出其中 <br>3. 星汉灿烂 <br>4. 若出其里 <br>","examId":"5c121aaeb2c0131b25983330","examOptions":[],"examStem":"《观沧海》中表现作者伟大胸襟的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>。","examTypeId":4,"favTime":0,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":1,"id":"5c5195b21cb07676c580b8bb","label":"（2016秋•宁河县月考）","lessonLibId":2455,"lessonTaskId":2053,"optionNumber":0,"paperCount":72,"partnerExamId":"87116caa-c9a7-4694-9f89-d34d702f2387","points":[{"eName":"chinese","id":3762,"levelId":2,"name":"名篇名句默写","no":"EF","nodeIdPath":"0.3729.3747.3762","parentId":"3747","subjectId":1}],"score":0,"sourceId":3,"sourceName":"菁优网","templateStyleId":359,"templateStyleName":"默写","type":1,"updateTime":1544690381746,"viewCount":34,"zoneIdPath":"0.15.223.5025","zoneName":"高新区"}
         * parentId : 0
         * emptyCount : 0
         * userStudentId : 5166
         * userStudentName : 学生62
         * examType : null
         * lessonLibId : null
         * examList : []
         * examOptionList : [{"id":185693,"lessonAnswerExamId":505408,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null}]
         * taskExamId : 5c5195b21cb07676c580b8bb
         */

        private int id;
        private int isAnswer;
        private int isCorrect;
        private int isRight;
        private Object timeAnswer;
        private long timeSubmit;
        private double score;
        private double fullScore;
        private int lessonTaskId;
        private int lessonTaskStudentId;
        private int lessonTaskDetailsId;
        private int correctType;
        private int useTime;
        private int templateId;
        private int templateStyleId;
        private String templateStyleName;
        private int examSeq;
        private String examText;
        private int parentId;
        private int emptyCount;
        private int userStudentId;
        private String userStudentName;
        private Object examType;
        private Object lessonLibId;
        private String taskExamId;
        private List<?> examList;
        private List<ExamOptionListBean> examOptionList;
        private ExamText ExamTextVo;

        public ExamText getExamTextVo() {
            return ExamTextVo;
        }

        public void setExamTextVo(ExamText examTextVo) {
            ExamTextVo = examTextVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsAnswer() {
            return isAnswer;
        }

        public void setIsAnswer(int isAnswer) {
            this.isAnswer = isAnswer;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public Object getTimeAnswer() {
            return timeAnswer;
        }

        public void setTimeAnswer(Object timeAnswer) {
            this.timeAnswer = timeAnswer;
        }

        public long getTimeSubmit() {
            return timeSubmit;
        }

        public void setTimeSubmit(long timeSubmit) {
            this.timeSubmit = timeSubmit;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public double getFullScore() {
            return fullScore;
        }

        public void setFullScore(double fullScore) {
            this.fullScore = fullScore;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public int getLessonTaskDetailsId() {
            return lessonTaskDetailsId;
        }

        public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
            this.lessonTaskDetailsId = lessonTaskDetailsId;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public int getTemplateId() {
            return templateId;
        }

        public void setTemplateId(int templateId) {
            this.templateId = templateId;
        }

        public int getTemplateStyleId() {
            return templateStyleId;
        }

        public void setTemplateStyleId(int templateStyleId) {
            this.templateStyleId = templateStyleId;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getEmptyCount() {
            return emptyCount;
        }

        public void setEmptyCount(int emptyCount) {
            this.emptyCount = emptyCount;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public Object getExamType() {
            return examType;
        }

        public void setExamType(Object examType) {
            this.examType = examType;
        }

        public Object getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(Object lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public String getTaskExamId() {
            return taskExamId;
        }

        public void setTaskExamId(String taskExamId) {
            this.taskExamId = taskExamId;
        }

        public List<?> getExamList() {
            return examList;
        }

        public void setExamList(List<?> examList) {
            this.examList = examList;
        }

        public List<ExamOptionListBean> getExamOptionList() {
            return examOptionList;
        }

        public void setExamOptionList(List<ExamOptionListBean> examOptionList) {
            this.examOptionList = examOptionList;
        }

        public static class ExamOptionListBean implements Comparable<ExamOptionListBean> {
            /**
             * id : 185693
             * lessonAnswerExamId : 505408
             * isRight : 1
             * score : -1.0
             * myAnswer : http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg
             * myAnswerHtml :
             * seq : 2
             * lessonTaskStudentId : 71149
             * myOldAnswer : http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg
             * clozeSeq : null
             * templateStyleName : null
             * examSeq : null
             */

            private int id;
            private int lessonAnswerExamId;
            private int isRight;
            private double score;
            private String myAnswer;
            private String myAnswerHtml;
            private int seq;
            private int lessonTaskStudentId;
            private String myOldAnswer;
            private Object clozeSeq;
            private Object templateStyleName;
            private Object examSeq;
            private int answerType;//填空：0 = 未作答，1 = 键盘作答， 2 = 手写作答， 3 = 拍照作答

            public int getAnswerType() {
                return answerType;
            }

            public void setAnswerType(int answerType) {
                this.answerType = answerType;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getLessonAnswerExamId() {
                return lessonAnswerExamId;
            }

            public void setLessonAnswerExamId(int lessonAnswerExamId) {
                this.lessonAnswerExamId = lessonAnswerExamId;
            }

            public int getIsRight() {
                return isRight;
            }

            public void setIsRight(int isRight) {
                this.isRight = isRight;
            }

            public double getScore() {
                return score;
            }

            public void setScore(double score) {
                this.score = score;
            }

            public String getMyAnswer() {
                return myAnswer;
            }

            public void setMyAnswer(String myAnswer) {
                this.myAnswer = myAnswer;
            }

            public String getMyAnswerHtml() {
                return myAnswerHtml;
            }

            public void setMyAnswerHtml(String myAnswerHtml) {
                this.myAnswerHtml = myAnswerHtml;
            }

            public int getSeq() {
                return seq;
            }

            public void setSeq(int seq) {
                this.seq = seq;
            }

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public String getMyOldAnswer() {
                return myOldAnswer;
            }

            public void setMyOldAnswer(String myOldAnswer) {
                this.myOldAnswer = myOldAnswer;
            }

            public Object getClozeSeq() {
                return clozeSeq;
            }

            public void setClozeSeq(Object clozeSeq) {
                this.clozeSeq = clozeSeq;
            }

            public Object getTemplateStyleName() {
                return templateStyleName;
            }

            public void setTemplateStyleName(Object templateStyleName) {
                this.templateStyleName = templateStyleName;
            }

            public Object getExamSeq() {
                return examSeq;
            }

            public void setExamSeq(Object examSeq) {
                this.examSeq = examSeq;
            }

            @Override
            public int compareTo(@NonNull ExamOptionListBean o) {
                return Integer.compare(seq, o.seq);
            }
        }
    }
}
