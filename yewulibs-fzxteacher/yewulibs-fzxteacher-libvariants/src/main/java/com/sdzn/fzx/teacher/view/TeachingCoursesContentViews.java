package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ClassContentBean;

public interface TeachingCoursesContentViews extends IView {
    void onCoursesContentSuccess(ClassContentBean str);

    void onCoursesContentNodata(String msg);

    void onCoursesContentFail(String msg);
}
