package com.sdzn.fzx.teacher.vo.me;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/24
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class VolumeVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * gradeId : 1
         * gradeName : 一年级
         * volumes : [{"volumeId":1,"volumeName":"上册"},{"volumeId":2,"volumeName":"下册"},{"volumeId":3,"volumeName":"全一册"}]
         */

        private int gradeId;
        private String gradeName;
        private List<VolumesBean> volumes;

        public int getGradeId() {
            return gradeId;
        }

        public void setGradeId(int gradeId) {
            this.gradeId = gradeId;
        }

        public String getGradeName() {
            return gradeName;
        }

        public void setGradeName(String gradeName) {
            this.gradeName = gradeName;
        }

        public List<VolumesBean> getVolumes() {
            return volumes;
        }

        public void setVolumes(List<VolumesBean> volumes) {
            this.volumes = volumes;
        }

        public static class VolumesBean {
            /**
             * volumeId : 1
             * volumeName : 上册
             */

            private int volumeId;
            private String volumeName;

            public int getVolumeId() {
                return volumeId;
            }

            public void setVolumeId(int volumeId) {
                this.volumeId = volumeId;
            }

            public String getVolumeName() {
                return volumeName;
            }

            public void setVolumeName(String volumeName) {
                this.volumeName = volumeName;
            }
        }
    }
}
