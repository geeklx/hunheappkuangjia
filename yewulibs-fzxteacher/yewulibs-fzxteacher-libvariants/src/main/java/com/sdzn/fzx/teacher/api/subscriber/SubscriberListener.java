package com.sdzn.fzx.teacher.api.subscriber;

/**
 * 描述：自定义Subscriber
 * -
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public interface SubscriberListener<T> {
    void onNext(T t);

    void onError(Throwable e);

    void onCompleted();
}
