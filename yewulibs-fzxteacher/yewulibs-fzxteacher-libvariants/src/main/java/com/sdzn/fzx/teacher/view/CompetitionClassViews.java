package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ClassRecordBean;
import com.sdzn.fzx.teacher.bean.CompetitionClassBean;

import java.util.List;

/**
 * 首页 view
 */
public interface CompetitionClassViews extends IView {
    void onCompetitionClassSuccess(List<CompetitionClassBean> c);

    void onCompetitionClassNodata(String msg);

    void onCompetitionClassFail(String msg);
}
