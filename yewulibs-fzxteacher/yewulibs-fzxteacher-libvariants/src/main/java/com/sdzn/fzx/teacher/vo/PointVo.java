package com.sdzn.fzx.teacher.vo;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/7
 * 修改单号：
 * 修改内容:
 */
public class PointVo {
    /**
     * ability : [{"abilityAccuracy":100,"abilityId":5090,"abilityName":"应用意识"}]
     * avgScore : -1.0
     * avgUseTime : 0.0
     * maxScore : -1.0
     * maxUseTime : 308
     * method : [{"methodAccuracy":100,"methodId":5083,"methodName":"客观题解题方法"}]
     * myScore : -1.0
     * myUseTime : 0
     * point : [{"pointAccuracy":100,"pointId":15618,"pointName":"有理数","pointParentId":15617,"pointParentName":"数与式","pointQuestionCount":1,"pointScore":94.24,"pointWrongCount":0},{"pointId":15617,"pointName":"数与式","pointParentId":0},{"pointAccuracy":50,"pointId":11289,"pointName":"正数和负数","pointParentId":15618,"pointParentName":"有理数","pointQuestionCount":10,"pointScore":50,"pointWrongCount":5},{"pointAccuracy":60,"pointId":11290,"pointName":"数轴","pointParentId":15618,"pointParentName":"有理数","pointQuestionCount":5,"pointScore":60,"pointWrongCount":2}]
     */

    private double avgScore;
    private double avgUseTime;
    private double maxScore;
    private int maxUseTime;
    private double myScore;
    private int myUseTime;
    private List<AbilityBean> ability;
    private List<MethodBean> method;
    private List<PointBean> point;
    private double errorRate;

    public double getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(double errorRate) {
        this.errorRate = errorRate;
    }

    public double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(double avgScore) {
        this.avgScore = avgScore;
    }

    public double getAvgUseTime() {
        return avgUseTime;
    }

    public void setAvgUseTime(double avgUseTime) {
        this.avgUseTime = avgUseTime;
    }

    public double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(double maxScore) {
        this.maxScore = maxScore;
    }

    public int getMaxUseTime() {
        return maxUseTime;
    }

    public void setMaxUseTime(int maxUseTime) {
        this.maxUseTime = maxUseTime;
    }

    public double getMyScore() {
        return myScore;
    }

    public void setMyScore(double myScore) {
        this.myScore = myScore;
    }

    public int getMyUseTime() {
        return myUseTime;
    }

    public void setMyUseTime(int myUseTime) {
        this.myUseTime = myUseTime;
    }

    public List<AbilityBean> getAbility() {
        return ability;
    }

    public void setAbility(List<AbilityBean> ability) {
        this.ability = ability;
    }

    public List<MethodBean> getMethod() {
        return method;
    }

    public void setMethod(List<MethodBean> method) {
        this.method = method;
    }

    public List<PointBean> getPoint() {
        return point;
    }

    public void setPoint(List<PointBean> point) {
        this.point = point;
    }

    public static class AbilityBean {
        /**
         * abilityAccuracy : 100.0
         * abilityId : 5090
         * abilityName : 应用意识
         */

        private double abilityAccuracy;
        private int abilityId;
        private String abilityName;

        public double getAbilityAccuracy() {
            return abilityAccuracy;
        }

        public void setAbilityAccuracy(double abilityAccuracy) {
            this.abilityAccuracy = abilityAccuracy;
        }

        public int getAbilityId() {
            return abilityId;
        }

        public void setAbilityId(int abilityId) {
            this.abilityId = abilityId;
        }

        public String getAbilityName() {
            return abilityName;
        }

        public void setAbilityName(String abilityName) {
            this.abilityName = abilityName;
        }
    }

    public static class MethodBean {
        /**
         * methodAccuracy : 100.0
         * methodId : 5083
         * methodName : 客观题解题方法
         */

        private double methodAccuracy;
        private int methodId;
        private String methodName;

        public double getMethodAccuracy() {
            return methodAccuracy;
        }

        public void setMethodAccuracy(double methodAccuracy) {
            this.methodAccuracy = methodAccuracy;
        }

        public int getMethodId() {
            return methodId;
        }

        public void setMethodId(int methodId) {
            this.methodId = methodId;
        }

        public String getMethodName() {
            return methodName;
        }

        public void setMethodName(String methodName) {
            this.methodName = methodName;
        }
    }

    public static class PointBean implements Comparable<PointBean> {
        /**
         * pointAccuracy : 100.0
         * pointId : 15618
         * pointName : 有理数
         * pointParentId : 15617
         * pointParentName : 数与式
         * pointQuestionCount : 1
         * pointScore : 94.24
         * pointWrongCount : 0
         */

        private double pointAccuracy;
        private long pointId;
        private String pointName;
        private long pointParentId;
        private String pointParentName;
        private int pointQuestionCount;
        private double pointScore;
        private int pointWrongCount;
        private List<PointBean> childList;

        private boolean last;

        public List<PointBean> getChildList() {
            return childList;
        }

        public void setChildList(List<PointBean> childList) {
            this.childList = childList;
        }

        public double getPointAccuracy() {
            return pointAccuracy;
        }

        public void setPointAccuracy(double pointAccuracy) {
            this.pointAccuracy = pointAccuracy;
        }


        public long getPointId() {
            return pointId;
        }

        public void setPointId(long pointId) {
            this.pointId = pointId;
        }

        public long getPointParentId() {
            return pointParentId;
        }

        public void setPointParentId(long pointParentId) {
            this.pointParentId = pointParentId;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public String getPointName() {
            return pointName;
        }

        public void setPointName(String pointName) {
            this.pointName = pointName;
        }


        public String getPointParentName() {
            return pointParentName;
        }

        public void setPointParentName(String pointParentName) {
            this.pointParentName = pointParentName;
        }

        public int getPointQuestionCount() {
            return pointQuestionCount;
        }

        public void setPointQuestionCount(int pointQuestionCount) {
            this.pointQuestionCount = pointQuestionCount;
        }

        public double getPointScore() {
            return pointScore;
        }

        public void setPointScore(double pointScore) {
            this.pointScore = pointScore;
        }

        public int getPointWrongCount() {
            return pointWrongCount;
        }

        public void setPointWrongCount(int pointWrongCount) {
            this.pointWrongCount = pointWrongCount;
        }

        @Override
        public int compareTo(@NonNull PointBean point) {
            return Long.compare(pointParentId, point.pointParentId);
        }


    }
}
