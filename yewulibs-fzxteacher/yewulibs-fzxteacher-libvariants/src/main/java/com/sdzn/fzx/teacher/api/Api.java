package com.sdzn.fzx.teacher.api;

import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.bean.ClassObjectivesBean;
import com.sdzn.fzx.teacher.bean.ClassRecordBean;
import com.sdzn.fzx.teacher.bean.ClassroomTestingBean;
import com.sdzn.fzx.teacher.bean.CompetitionClassBean;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean;
import com.sdzn.fzx.teacher.bean.RecyclerTabBean;
import com.sdzn.fzx.teacher.bean.RefreshTokenRecActBean;
import com.sdzn.fzx.teacher.bean.SchoolClassGroupBean;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.bean.StartStudyBean;
import com.sdzn.fzx.teacher.bean.SubjectData1;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;
import com.sdzn.fzx.teacher.bean.TeachingCoursesBean;
import com.sdzn.fzx.teacher.bean.TreeBean;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.QiNiuUrlVo;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;
import com.sdzn.fzx.teacher.vo.UserBean;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {

    public String path = BuildConfig3.SERVER_ISERVICE_NEW2;

    /*资源发布*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<Object>> AddResources(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*查询发布对象*/
    @GET
    Call<ResponseSlbBean1<SchoolClassGroupBean>> getClassGroup(@Url String path, @Header("Authorization") String token, @Query("classId") String classId);

    /*发布合作探究*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<Object>> getGroupChatLib(@Url String path, @Header("Authorization") String token,@Body RequestBody body);


    /*教学预览内容*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<ClassObjectivesBean>> queryOjectivesContent(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*教学内容列表*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<ClassContentBean>> queryCoursesContent(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*课堂教学列表*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<TeachingCoursesBean>> queryTeachingCourses(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*上课记录列表*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<ClassRecordBean>> queryclassrecord(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*老师册别*/
    @GET
    Call<ResponseSlbBean1<TeacherVolumeBean>> queryTeacherVolume(@Url String path, @Header("Authorization") String token, @Query("teacherId") String teacherId);

    /*Tree树结构*/
    @GET
    Call<ResponseSlbBean1<List<TreeBean>>> queryTree(@Url String path, @Header("Authorization") String token, @Query("volumeId") String volumeId);

    /*上课记录选择班级*/
    @GET
    Call<ResponseSlbBean1<List<CompetitionClassBean>>> queryCompetitionClass(@Url String path, @Header("Authorization") String token, @Query("teacherId") String teacherId);

    /*课堂检测列表 */
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<ClassroomTestingBean>> queryClassroomTesting(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*老师收题*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<Object>> AddTeacherQuestion(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*课堂检测撤回*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<Object>> addNowOut(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*课堂检测答案*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<Object>> addSendAnswerOpen(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*课堂检测可见隐藏*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<Object>> addSendIsHide(@Url String path, @Header("Authorization") String token, @Body RequestBody body);

    /*教学课程开始教学*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
    Call<ResponseSlbBean1<StartStudyBean>> addStartStudy(@Url String path, @Header("Authorization") String token, @Body RequestBody body);


    // 检查更新
    @FormUrlEncoded
    @POST
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Url String path, @Field("programId") String programId, @Field("type") String type);//@Body RequestBody body@Header("Authorization") String token,

    // 首页
    @Headers({"Content-Type: application/json", "Accept: application/json"})
//    @POST("/course/home/data")
    @POST
    Call<ResponseSlbBean1<ShouyeBean>> queryShouye(@Url String path, @Header("Authorization") String token, @Body RequestBody body);//@Body RequestBody body@Header("Authorization") String token, "Authorization", "Bearer "+

    // 登录
    @FormUrlEncoded
    @POST
//    @POST("/teacher/login/token")
    Call<ResponseSlbBean1<LoginBean.DataBean>> login(@Url String path, @Field("username") String username, @Field("password") String password);//@Body RequestBody body@Header("Authorization") String token,

    // 侧滑pop
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST("/course/home/broadside/list")
    Call<ResponseSlbBean1<GrzxRecActBean>> queryCehua(@Url String path);

    // 动态获取tab数据
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST("/course/home/tabJump/list")
    Call<ResponseSlbBean1<RecyclerTabBean>> queryTab(@Url String path, @Body RequestBody body);


    // 个人中心获取资料
    @GET
//    @GET("/usercenter/teacher/teacherInfo")
    Call<ResponseSlbBean1<UserBean>> teacherInfo(@Url String path, @Header("Authorization") String token, @Query("userId") String userId);

    // 更新头像
    @FormUrlEncoded
    @POST
//    @POST("/usercenter/teacher/update/teacherInfo")
    Call<ResponseSlbBean1<Object>> updateAvatar(@Url String path, @Header("Authorization") String token, @Field("teacherId") String teacherId, @Field("avatar") String avatar);

    // 发送验证码
    @FormUrlEncoded
    @POST
//    @POST("/student/login/sendVerifyCode")
    Call<ResponseSlbBean1<Object>> sendYZM(@Url String path, @Field("telephone") String telephone, @Field("imgCode") String imgCode, @Field("imgToken") String imgToken);

    // 获取图形token
    @POST
//    @POST("/student/login/sendVerifyCode")
    Call<ResponseSlbBean1<Object>> QueryImgToken(@Url String path);

    // 获取Img图片
    @FormUrlEncoded
    @POST
//    @POST("/student/login/sendVerifyCode")
    Call<ResponseBody> QueryImg(@Url String path, @Field("imgToken") String imgToken);

    // 验证验证码
    @FormUrlEncoded
    @POST
//    @POST("/teacher/check/smsToken")
    Call<ResponseSlbBean1<Object>> checkYZM(@Url String path, @Field("mobile") String mobile, @Field("code") String code);

    // 教师更新手机号
    @FormUrlEncoded
    @POST
//    @POST("/usercenter/teacher/update/mobile")
    Call<ResponseSlbBean1<Object>> updatePhone(@Url String path, @Header("Authorization") String token, @Field("newTel") String mobile, @Field("code") String code);

    // 教师修改密码
    @FormUrlEncoded
    @POST
//    @POST("/usercenter/teacher/update/password")
    Call<ResponseSlbBean1<Object>> updatePassword(@Url String path, @Header("Authorization") String token, @Field("id") String id,
                                                  @Field("oldPassword") String oldPassword, @Field("password") String password);

    // 七牛key获取地址
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST("/knowledgecenter/resource/getUrlByFileName")
    Call<ResponseSlbBean1<QiNiuUrlVo>> getQiNiuUrl(@Url String path, @Header("Authorization") String token, @Body RequestBody body);  // 七牛key获取地址

    @GET
//    @GET("/usercenter/teacher/teach/classes")
    Call<ResponseSlbBean1<TakeOfficeVo>> getTeacherClasses(@Url String path, @Header("Authorization") String token, @Query("teacherId") String teacherId, @Query("schoolId") String schoolId);

    // 忘记密码--获取手机号
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST("/usercenter/teacher/find/teacherMobile")VerifyUserMobile
    Call<ResponseSlbBean1<Object>> teacherFindMobile(@Url String path, @Body RequestBody body);

    // 忘记密码--更新密码
    @FormUrlEncoded
    @POST
//    @POST("/usercenter/teacher/teacher/forget/password")
    Call<ResponseSlbBean1<Object>> updateForgetPas(@Url String path, @Field("mobile") String mobile, @Field("password") String password);//, @Field("code") String code)

    //根据老师获取老师教学科目
    @GET
//    @GET("/usercenter/teacherBookVersion/list")
    Call<ResponseSlbBean1<SubjectData1>> getSubjectData(@Url String path, @Query("teacherId") String teacherId);

    //刷新token
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.QUERY_TOKEN)
    Call<ResponseSlbBean1<RefreshTokenRecActBean>> queryRefreshToken(@Url String path, @Field("refreshToken") String refreshToken);
}
