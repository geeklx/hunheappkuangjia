package com.sdzn.fzx.teacher.vo;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/26
 * 修改单号：
 * 修改内容:
 */
public class StudentDetailExam {
    /**
     * data : {"id":12263,"isAnswer":1,"isCorrect":1,"isRight":2,"timeAnswer":null,"timeSubmit":1551066545000,"score":-1,"fullScore":-1,"lessonTaskId":64,"lessonTaskStudentId":2246,"lessonTaskDetailsId":450,"correctType":2,"useTime":0,"templateId":1,"templateStyleId":15,"templateStyleName":"选择题","examSeq":3,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":2,\"baseSubjectName\":\"数学\",\"baseVersionId\":5,\"baseVersionName\":\"北师大版\",\"baseVolumeId\":28,\"baseVolumeName\":\"上册\",\"chapterId\":11508,\"chapterName\":\"第1章 丰富的图形世界\",\"chapterNodeIdPath\":\"0.11508\",\"chapterNodeNamePath\":\"第1章 丰富的图形世界\",\"createTime\":1551060621199,\"createUserId\":216,\"createUserName\":\"黄东月\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":0,\"examAnalysis\":\"解：上、下边的直角三角形绕直角边旋转一周后可得到两个圆锥，中间的矩形绕一边旋转一周后可得到一个圆柱，那么组合体应是圆锥和圆柱的组合体．<br />故选：D．\",\"examAnswer\":\"D\",\"examId\":\"5c734e991cb0766796fb5c6d\",\"examOptions\":[{\"content\":\"<img alt=\\\"菁优网\\\" src=\\\"http://img.jyeoo.net/quiz/images/201201/81/3cbdfed8.png\\\" style=\\\"vertical-align:middle\\\" />\",\"right\":false,\"seq\":1},{\"content\":\"<img alt=\\\"菁优网\\\" src=\\\"http://img.jyeoo.net/quiz/images/201201/81/967646ce.png\\\" style=\\\"vertical-align:middle\\\" />\",\"right\":false,\"seq\":2},{\"content\":\"<img alt=\\\"菁优网\\\" src=\\\"http://img.jyeoo.net/quiz/images/201201/81/78293077.png\\\" style=\\\"vertical-align:middle\\\" />\",\"right\":false,\"seq\":3},{\"content\":\"<img alt=\\\"菁优网\\\" src=\\\"http://img.jyeoo.net/quiz/images/201201/81/aaed45f1.png\\\" style=\\\"vertical-align:middle\\\" />\",\"right\":true,\"seq\":4}],\"examStem\":\"<img alt=\\\"菁优网\\\" src=\\\"http://img.jyeoo.net/quiz/images/201202/17/61e77c89.png\\\" style=\\\"vertical-align:middle;FLOAT:right\\\" />用如图的图形，旋转一周所形成的图形是右边的（　　）\",\"examTypeId\":1,\"favTime\":0,\"flagShare\":0,\"holdUserId\":216,\"holdUserName\":\"黄东月\",\"hots\":0,\"id\":\"5c73542a1cb0766796fb5ced\",\"label\":\"（2018秋\u2022椒江区期末）\",\"lessonLibId\":76,\"lessonTaskId\":64,\"optionNumber\":4,\"paperCount\":0,\"partnerExamId\":\"079893c2-7058-4b3c-84f4-50a433afbd74\",\"points\":[{\"eName\":\"math\",\"id\":1375,\"levelId\":2,\"name\":\"点、线、面、体\",\"no\":\"I2\",\"nodeIdPath\":\"0.1372.1373.1375\",\"parentId\":\"1373\",\"subjectId\":2}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":15,\"templateStyleName\":\"选择题\",\"type\":1,\"updateTime\":1551060621199,\"viewCount\":0,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":4,"userStudentId":4596,"userStudentName":"测试15","examType":null,"lessonLibId":null,"examList":[],"examOptionList":[{"id":1086,"lessonAnswerExamId":12263,"isRight":1,"score":-1,"myAnswer":"C","myAnswerHtml":null,"seq":3,"lessonTaskStudentId":2246,"myOldAnswer":"C","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":0}],"taskExamId":"5c73542a1cb0766796fb5ced","answerType":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 12263
         * isAnswer : 1
         * isCorrect : 1
         * isRight : 2
         * timeAnswer : null
         * timeSubmit : 1551066545000
         * score : -1.0
         * fullScore : -1.0
         * lessonTaskId : 64
         * lessonTaskStudentId : 2246
         * lessonTaskDetailsId : 450
         * correctType : 2
         * useTime : 0
         * templateId : 1
         * templateStyleId : 15
         * templateStyleName : 选择题
         * examSeq : 3
         * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","baseVolumeId":28,"baseVolumeName":"上册","chapterId":11508,"chapterName":"第1章 丰富的图形世界","chapterNodeIdPath":"0.11508","chapterNodeNamePath":"第1章 丰富的图形世界","createTime":1551060621199,"createUserId":216,"createUserName":"黄东月","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"downCount":0,"examAnalysis":"解：上、下边的直角三角形绕直角边旋转一周后可得到两个圆锥，中间的矩形绕一边旋转一周后可得到一个圆柱，那么组合体应是圆锥和圆柱的组合体．<br />故选：D．","examAnswer":"D","examId":"5c734e991cb0766796fb5c6d","examOptions":[{"content":"<img alt=\"菁优网\" src=\"http://img.jyeoo.net/quiz/images/201201/81/3cbdfed8.png\" style=\"vertical-align:middle\" />","right":false,"seq":1},{"content":"<img alt=\"菁优网\" src=\"http://img.jyeoo.net/quiz/images/201201/81/967646ce.png\" style=\"vertical-align:middle\" />","right":false,"seq":2},{"content":"<img alt=\"菁优网\" src=\"http://img.jyeoo.net/quiz/images/201201/81/78293077.png\" style=\"vertical-align:middle\" />","right":false,"seq":3},{"content":"<img alt=\"菁优网\" src=\"http://img.jyeoo.net/quiz/images/201201/81/aaed45f1.png\" style=\"vertical-align:middle\" />","right":true,"seq":4}],"examStem":"<img alt=\"菁优网\" src=\"http://img.jyeoo.net/quiz/images/201202/17/61e77c89.png\" style=\"vertical-align:middle;FLOAT:right\" />用如图的图形，旋转一周所形成的图形是右边的（　　）","examTypeId":1,"favTime":0,"flagShare":0,"holdUserId":216,"holdUserName":"黄东月","hots":0,"id":"5c73542a1cb0766796fb5ced","label":"（2018秋•椒江区期末）","lessonLibId":76,"lessonTaskId":64,"optionNumber":4,"paperCount":0,"partnerExamId":"079893c2-7058-4b3c-84f4-50a433afbd74","points":[{"eName":"math","id":1375,"levelId":2,"name":"点、线、面、体","no":"I2","nodeIdPath":"0.1372.1373.1375","parentId":"1373","subjectId":2}],"score":0,"sourceId":3,"sourceName":"菁优网","templateStyleId":15,"templateStyleName":"选择题","type":1,"updateTime":1551060621199,"viewCount":0,"zoneIdPath":"0.15.223.5025","zoneName":"高新区"}
         * parentId : 0
         * emptyCount : 4
         * userStudentId : 4596
         * userStudentName : 测试15
         * examType : null
         * lessonLibId : null
         * examList : []
         * examOptionList : [{"id":1086,"lessonAnswerExamId":12263,"isRight":1,"score":-1,"myAnswer":"C","myAnswerHtml":null,"seq":3,"lessonTaskStudentId":2246,"myOldAnswer":"C","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":0}]
         * taskExamId : 5c73542a1cb0766796fb5ced
         * answerType : null
         */

        private int id;
        private int isAnswer;
        private int isCorrect;
        private int isRight;
        private Object timeAnswer;
        private long timeSubmit;
        private double score;
        private double fullScore;
        private int lessonTaskId;
        private int lessonTaskStudentId;
        private int lessonTaskDetailsId;
        private int correctType;
        private int useTime;
        private int templateId;
        private int templateStyleId;
        private String templateStyleName;
        private int examSeq;
        private String examText;
        private int parentId;
        private int emptyCount;
        private int userStudentId;
        private String userStudentName;
        private Object examType;
        private Object lessonLibId;
        private String taskExamId;
        private Object answerType;
        private List<DataBean> examList;
        private List<ExamOptionListBean> examOptionList;
        private ExamText ExamTextVo;

        public ExamText getExamTextVo() {
            return ExamTextVo;
        }

        public void setExamTextVo(ExamText examTextVo) {
            ExamTextVo = examTextVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsAnswer() {
            return isAnswer;
        }

        public void setIsAnswer(int isAnswer) {
            this.isAnswer = isAnswer;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public Object getTimeAnswer() {
            return timeAnswer;
        }

        public void setTimeAnswer(Object timeAnswer) {
            this.timeAnswer = timeAnswer;
        }

        public long getTimeSubmit() {
            return timeSubmit;
        }

        public void setTimeSubmit(long timeSubmit) {
            this.timeSubmit = timeSubmit;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public double getFullScore() {
            return fullScore;
        }

        public void setFullScore(double fullScore) {
            this.fullScore = fullScore;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public int getLessonTaskDetailsId() {
            return lessonTaskDetailsId;
        }

        public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
            this.lessonTaskDetailsId = lessonTaskDetailsId;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public int getTemplateId() {
            return templateId;
        }

        public void setTemplateId(int templateId) {
            this.templateId = templateId;
        }

        public int getTemplateStyleId() {
            return templateStyleId;
        }

        public void setTemplateStyleId(int templateStyleId) {
            this.templateStyleId = templateStyleId;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getEmptyCount() {
            return emptyCount;
        }

        public void setEmptyCount(int emptyCount) {
            this.emptyCount = emptyCount;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public Object getExamType() {
            return examType;
        }

        public void setExamType(Object examType) {
            this.examType = examType;
        }

        public Object getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(Object lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public String getTaskExamId() {
            return taskExamId;
        }

        public void setTaskExamId(String taskExamId) {
            this.taskExamId = taskExamId;
        }

        public Object getAnswerType() {
            return answerType;
        }

        public void setAnswerType(Object answerType) {
            this.answerType = answerType;
        }

        public List<DataBean> getExamList() {
            return examList;
        }

        public void setExamList(List<DataBean> examList) {
            this.examList = examList;
        }

        public List<ExamOptionListBean> getExamOptionList() {
            return examOptionList;
        }

        public void setExamOptionList(List<ExamOptionListBean> examOptionList) {
            this.examOptionList = examOptionList;
        }


        public static class ExamOptionListBean implements Comparable<ExamOptionListBean> {
            /**
             * id : 1086
             * lessonAnswerExamId : 12263
             * isRight : 1
             * score : -1.0
             * myAnswer : C
             * myAnswerHtml : null
             * seq : 3
             * lessonTaskStudentId : 2246
             * myOldAnswer : C
             * clozeSeq : null
             * templateStyleName : null
             * examSeq : null
             * answerType : 0
             */

            private int id;
            private int lessonAnswerExamId;
            private int isRight;
            private double score;
            private String myAnswer;
            private Object myAnswerHtml;
            private int seq;
            private int lessonTaskStudentId;
            private String myOldAnswer;
            private Object clozeSeq;
            private Object templateStyleName;
            private Object examSeq;
            private int answerType;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getLessonAnswerExamId() {
                return lessonAnswerExamId;
            }

            public void setLessonAnswerExamId(int lessonAnswerExamId) {
                this.lessonAnswerExamId = lessonAnswerExamId;
            }

            public int getIsRight() {
                return isRight;
            }

            public void setIsRight(int isRight) {
                this.isRight = isRight;
            }

            public double getScore() {
                return score;
            }

            public void setScore(double score) {
                this.score = score;
            }

            public String getMyAnswer() {
                return myAnswer;
            }

            public void setMyAnswer(String myAnswer) {
                this.myAnswer = myAnswer;
            }

            public Object getMyAnswerHtml() {
                return myAnswerHtml;
            }

            public void setMyAnswerHtml(Object myAnswerHtml) {
                this.myAnswerHtml = myAnswerHtml;
            }

            public int getSeq() {
                return seq;
            }

            public void setSeq(int seq) {
                this.seq = seq;
            }

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public String getMyOldAnswer() {
                return myOldAnswer;
            }

            public void setMyOldAnswer(String myOldAnswer) {
                this.myOldAnswer = myOldAnswer;
            }

            public Object getClozeSeq() {
                return clozeSeq;
            }

            public void setClozeSeq(Object clozeSeq) {
                this.clozeSeq = clozeSeq;
            }

            public Object getTemplateStyleName() {
                return templateStyleName;
            }

            public void setTemplateStyleName(Object templateStyleName) {
                this.templateStyleName = templateStyleName;
            }

            public Object getExamSeq() {
                return examSeq;
            }

            public void setExamSeq(Object examSeq) {
                this.examSeq = examSeq;
            }

            public int getAnswerType() {
                return answerType;
            }

            public void setAnswerType(int answerType) {
                this.answerType = answerType;
            }

            @Override
            public int compareTo(@NonNull ExamOptionListBean o) {
                return Integer.compare(seq, o.seq);
            }
        }
    }
}
