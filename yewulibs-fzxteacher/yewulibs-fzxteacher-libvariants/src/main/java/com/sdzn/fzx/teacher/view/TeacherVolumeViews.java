package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;
import com.sdzn.fzx.teacher.bean.TreeBean;

import java.util.List;

/**
 * 首页 view
 */
public interface TeacherVolumeViews extends IView {
    void onTeacherVolumeSuccess(TeacherVolumeBean teacherVolumeBean);

    void onTeacherVolumeNodata(String msg);

    void onTeacherVolumeFail(String msg);
}
