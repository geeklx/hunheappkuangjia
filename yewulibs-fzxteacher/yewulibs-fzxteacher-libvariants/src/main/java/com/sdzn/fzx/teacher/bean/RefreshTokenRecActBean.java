package com.sdzn.fzx.teacher.bean;

import java.io.Serializable;

public class RefreshTokenRecActBean implements Serializable {

    /**
     * access_token : bea49796-9a87-4982-bef2-bcc4a6cbce00
     * domain : @student.com
     * expires_in : 899
     * openid : 202107
     * refresh_token : 01968a61-68be-401e-858b-588c8bf6dd98
     * scope : userProfile
     * token_type : bearer
     */

    private String access_token;
    private String domain;
    private int expires_in;
    private String openid;
    private String refresh_token;
    private String scope;
    private String token_type;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }
}
