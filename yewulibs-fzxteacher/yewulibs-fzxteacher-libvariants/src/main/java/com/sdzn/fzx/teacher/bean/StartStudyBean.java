package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class StartStudyBean {

    /**
     * id : 2103
     * name : 春
     * chapterId : 2002623
     * volumeId : 556
     * versiond : 10228
     * subjectId : 2
     * gradeId : 0
     * createUserId : 2399
     * createTime : 2020-12-29 16:09:03
     * classId : 112
     * startTime : 2020-12-29 16:09:03
     * endTime : 2020-12-29 16:09:03
     * countExam : 0
     * countRescoure : 0
     * score : 0
     * scoreAvg : 0
     * countStudentTotal : 10
     * countStudentSub : 0
     * countTeacherCorrect : 0
     * useTimeAvg : 0
     * answerViewType : 1
     * answerViewOpen : 1
     * lessonLibId : 1067
     * lessonId : 535
     * status : 1
     * examId :
     * lessonName : 123412341234
     * sceneId : 1
     * isHide : 0
     * countStudentReplenish : 0
     * lessonExamAnswerPos : []
     * lessonQuestionnaireDetailPos : []
     * map : {}
     * lessonTaskResourcePos : []
     * taskTopicAnalysisPoList : []
     * lessonQuestionnaireTotalPos : []
     */

    private String id;
    private String name;
    private String chapterId;
    private String volumeId;
    private int versiond;
    private String subjectId;
    private int gradeId;
    private int createUserId;
    private String createTime;
    private String classId;
    private String startTime;
    private String endTime;
    private int countExam;
    private int countRescoure;
    private int score;
    private int scoreAvg;
    private int countStudentTotal;
    private int countStudentSub;
    private int countTeacherCorrect;
    private int useTimeAvg;
    private int answerViewType;
    private int answerViewOpen;
    private String lessonLibId;
    private String lessonId;
    private int status;
    private String examId;
    private String lessonName;
    private int sceneId;
    private int isHide;
    private String countStudentReplenish;
    private MapBean map;
    private List<?> lessonExamAnswerPos;
    private List<?> lessonQuestionnaireDetailPos;
    private List<?> lessonTaskResourcePos;
    private List<?> taskTopicAnalysisPoList;
    private List<?> lessonQuestionnaireTotalPos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(String volumeId) {
        this.volumeId = volumeId;
    }

    public int getVersiond() {
        return versiond;
    }

    public void setVersiond(int versiond) {
        this.versiond = versiond;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getCountExam() {
        return countExam;
    }

    public void setCountExam(int countExam) {
        this.countExam = countExam;
    }

    public int getCountRescoure() {
        return countRescoure;
    }

    public void setCountRescoure(int countRescoure) {
        this.countRescoure = countRescoure;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScoreAvg() {
        return scoreAvg;
    }

    public void setScoreAvg(int scoreAvg) {
        this.scoreAvg = scoreAvg;
    }

    public int getCountStudentTotal() {
        return countStudentTotal;
    }

    public void setCountStudentTotal(int countStudentTotal) {
        this.countStudentTotal = countStudentTotal;
    }

    public int getCountStudentSub() {
        return countStudentSub;
    }

    public void setCountStudentSub(int countStudentSub) {
        this.countStudentSub = countStudentSub;
    }

    public int getCountTeacherCorrect() {
        return countTeacherCorrect;
    }

    public void setCountTeacherCorrect(int countTeacherCorrect) {
        this.countTeacherCorrect = countTeacherCorrect;
    }

    public int getUseTimeAvg() {
        return useTimeAvg;
    }

    public void setUseTimeAvg(int useTimeAvg) {
        this.useTimeAvg = useTimeAvg;
    }

    public int getAnswerViewType() {
        return answerViewType;
    }

    public void setAnswerViewType(int answerViewType) {
        this.answerViewType = answerViewType;
    }

    public int getAnswerViewOpen() {
        return answerViewOpen;
    }

    public void setAnswerViewOpen(int answerViewOpen) {
        this.answerViewOpen = answerViewOpen;
    }

    public String getLessonLibId() {
        return lessonLibId;
    }

    public void setLessonLibId(String lessonLibId) {
        this.lessonLibId = lessonLibId;
    }

    public String getLessonId() {
        return lessonId;
    }

    public void setLessonId(String lessonId) {
        this.lessonId = lessonId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public int getSceneId() {
        return sceneId;
    }

    public void setSceneId(int sceneId) {
        this.sceneId = sceneId;
    }

    public int getIsHide() {
        return isHide;
    }

    public void setIsHide(int isHide) {
        this.isHide = isHide;
    }

    public String getCountStudentReplenish() {
        return countStudentReplenish;
    }

    public void setCountStudentReplenish(String countStudentReplenish) {
        this.countStudentReplenish = countStudentReplenish;
    }

    public MapBean getMap() {
        return map;
    }

    public void setMap(MapBean map) {
        this.map = map;
    }

    public List<?> getLessonExamAnswerPos() {
        return lessonExamAnswerPos;
    }

    public void setLessonExamAnswerPos(List<?> lessonExamAnswerPos) {
        this.lessonExamAnswerPos = lessonExamAnswerPos;
    }

    public List<?> getLessonQuestionnaireDetailPos() {
        return lessonQuestionnaireDetailPos;
    }

    public void setLessonQuestionnaireDetailPos(List<?> lessonQuestionnaireDetailPos) {
        this.lessonQuestionnaireDetailPos = lessonQuestionnaireDetailPos;
    }

    public List<?> getLessonTaskResourcePos() {
        return lessonTaskResourcePos;
    }

    public void setLessonTaskResourcePos(List<?> lessonTaskResourcePos) {
        this.lessonTaskResourcePos = lessonTaskResourcePos;
    }

    public List<?> getTaskTopicAnalysisPoList() {
        return taskTopicAnalysisPoList;
    }

    public void setTaskTopicAnalysisPoList(List<?> taskTopicAnalysisPoList) {
        this.taskTopicAnalysisPoList = taskTopicAnalysisPoList;
    }

    public List<?> getLessonQuestionnaireTotalPos() {
        return lessonQuestionnaireTotalPos;
    }

    public void setLessonQuestionnaireTotalPos(List<?> lessonQuestionnaireTotalPos) {
        this.lessonQuestionnaireTotalPos = lessonQuestionnaireTotalPos;
    }

    public static class MapBean {
    }
}