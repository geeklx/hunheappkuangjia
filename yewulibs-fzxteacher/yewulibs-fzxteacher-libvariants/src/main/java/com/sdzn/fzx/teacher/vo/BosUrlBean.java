package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/6/3
 * 修改单号：
 * 修改内容:
 */
public class BosUrlBean {
    /**
     * code : 0
     * msg : success
     * timestamp : 1559553936563
     * result : {"url":"http://192.168.0.185/7f962f99-c051-414c-9c41-3e16e514c567/%E7%BC%96%E7%A0%81%E6%B5%8B%E8%AF%95.mp4"}
     */

    private int code;
    private String msg;
    private String timestamp;
    private ResultBean result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * url : http://192.168.0.185/7f962f99-c051-414c-9c41-3e16e514c567/%E7%BC%96%E7%A0%81%E6%B5%8B%E8%AF%95.mp4
         */

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
