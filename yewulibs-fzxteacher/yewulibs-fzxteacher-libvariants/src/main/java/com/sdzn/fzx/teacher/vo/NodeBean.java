package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * NodeBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class NodeBean {


    private List<DataBeanXX> data;

    public List<DataBeanXX> getData() {
        return data;
    }

    public void setData(List<DataBeanXX> data) {
        this.data = data;
    }

    public static class DataBeanXX {
        /**
         * data : {"id":1,"name":"第一组","nodeIdPath":"0.1","nodeNamePath":"第一组","status":null,"parentId":0,"leaf":null,"seq":1,"baseLevelId":null,"baseLevelName":null,"baseEducationId":null,"baseEducationName":null,"baseSubjectId":null,"baseSubjectName":null,"baseGradeId":null,"baseGradeName":null,"startGradeId":null,"startGradeName":null,"baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null}
         * children : [{"data":{"id":2,"name":"我们的民族小学","nodeIdPath":"0.1.2","nodeNamePath":"第一组>我们的民族小学","status":null,"parentId":1,"leaf":null,"seq":1,"baseLevelId":null,"baseLevelName":null,"baseEducationId":null,"baseEducationName":null,"baseSubjectId":null,"baseSubjectName":null,"baseGradeId":null,"baseGradeName":null,"startGradeId":null,"startGradeName":null,"baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null},"children":[],"parent":null,"leaf":true},{"data":{"id":3,"name":"金色的草地","nodeIdPath":"0.1.3","nodeNamePath":"第一组>金色的草地","status":null,"parentId":1,"leaf":null,"seq":2,"baseLevelId":null,"baseLevelName":null,"baseEducationId":null,"baseEducationName":null,"baseSubjectId":null,"baseSubjectName":null,"baseGradeId":null,"baseGradeName":null,"startGradeId":null,"startGradeName":null,"baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null},"children":[],"parent":null,"leaf":true},{"data":{"id":4,"name":"爬天都峰","nodeIdPath":"0.1.4","nodeNamePath":"第一组>爬天都峰","status":null,"parentId":1,"leaf":null,"seq":3,"baseLevelId":null,"baseLevelName":null,"baseEducationId":null,"baseEducationName":null,"baseSubjectId":null,"baseSubjectName":null,"baseGradeId":null,"baseGradeName":null,"startGradeId":null,"startGradeName":null,"baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null},"children":[],"parent":null,"leaf":true},{"data":{"id":5,"name":"槐乡的孩子","nodeIdPath":"0.1.5","nodeNamePath":"第一组>槐乡的孩子 ","status":null,"parentId":1,"leaf":null,"seq":4,"baseLevelId":null,"baseLevelName":null,"baseEducationId":null,"baseEducationName":null,"baseSubjectId":null,"baseSubjectName":null,"baseGradeId":null,"baseGradeName":null,"startGradeId":null,"startGradeName":null,"baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null},"children":[],"parent":null,"leaf":true}]
         * parent : null
         * leaf : false
         */

        private DataBean data;
        private Object parent;
        private boolean leaf;
        private transient int pageIndex;

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        private List<DataBeanXX> children;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public Object getParent() {
            return parent;
        }

        public void setParent(Object parent) {
            this.parent = parent;
        }

        public boolean isLeaf() {
            return leaf;
        }

        public void setLeaf(boolean leaf) {
            this.leaf = leaf;
        }

        public List<DataBeanXX> getChildren() {
            return children;
        }

        public void setChildren(List<DataBeanXX> children) {
            this.children = children;
        }

        public static class DataBean {
            /**
             * id : 1
             * name : 第一组
             * nodeIdPath : 0.1
             * nodeNamePath : 第一组
             * status : null
             * parentId : 0
             * leaf : null
             * seq : 1
             * baseLevelId : null
             * baseLevelName : null
             * baseEducationId : null
             * baseEducationName : null
             * baseSubjectId : null
             * baseSubjectName : null
             * baseGradeId : null
             * baseGradeName : null
             * startGradeId : null
             * startGradeName : null
             * baseVersionId : null
             * baseVersionName : null
             * baseVolumeId : null
             * baseVolumeName : null
             */

            private int id;
            private String name;
            private String nodeIdPath;
            private String nodeNamePath;
            private Object status;
            private int parentId;
            private boolean leaf;
            private int seq;
            private Object baseLevelId;
            private Object baseLevelName;
            private Object baseEducationId;
            private Object baseEducationName;
            private Object baseSubjectId;
            private Object baseSubjectName;
            private Object baseGradeId;
            private Object baseGradeName;
            private Object startGradeId;
            private Object startGradeName;
            private Object baseVersionId;
            private Object baseVersionName;
            private Object baseVolumeId;
            private Object baseVolumeName;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getNodeIdPath() {
                return nodeIdPath;
            }

            public void setNodeIdPath(String nodeIdPath) {
                this.nodeIdPath = nodeIdPath;
            }

            public String getNodeNamePath() {
                return nodeNamePath;
            }

            public void setNodeNamePath(String nodeNamePath) {
                this.nodeNamePath = nodeNamePath;
            }

            public Object getStatus() {
                return status;
            }

            public void setStatus(Object status) {
                this.status = status;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public boolean isLeaf() {
                return leaf;
            }

            public void setLeaf(boolean leaf) {
                this.leaf = leaf;
            }

            public int getSeq() {
                return seq;
            }

            public void setSeq(int seq) {
                this.seq = seq;
            }

            public Object getBaseLevelId() {
                return baseLevelId;
            }

            public void setBaseLevelId(Object baseLevelId) {
                this.baseLevelId = baseLevelId;
            }

            public Object getBaseLevelName() {
                return baseLevelName;
            }

            public void setBaseLevelName(Object baseLevelName) {
                this.baseLevelName = baseLevelName;
            }

            public Object getBaseEducationId() {
                return baseEducationId;
            }

            public void setBaseEducationId(Object baseEducationId) {
                this.baseEducationId = baseEducationId;
            }

            public Object getBaseEducationName() {
                return baseEducationName;
            }

            public void setBaseEducationName(Object baseEducationName) {
                this.baseEducationName = baseEducationName;
            }

            public Object getBaseSubjectId() {
                return baseSubjectId;
            }

            public void setBaseSubjectId(Object baseSubjectId) {
                this.baseSubjectId = baseSubjectId;
            }

            public Object getBaseSubjectName() {
                return baseSubjectName;
            }

            public void setBaseSubjectName(Object baseSubjectName) {
                this.baseSubjectName = baseSubjectName;
            }

            public Object getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(Object baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public Object getBaseGradeName() {
                return baseGradeName;
            }

            public void setBaseGradeName(Object baseGradeName) {
                this.baseGradeName = baseGradeName;
            }

            public Object getStartGradeId() {
                return startGradeId;
            }

            public void setStartGradeId(Object startGradeId) {
                this.startGradeId = startGradeId;
            }

            public Object getStartGradeName() {
                return startGradeName;
            }

            public void setStartGradeName(Object startGradeName) {
                this.startGradeName = startGradeName;
            }

            public Object getBaseVersionId() {
                return baseVersionId;
            }

            public void setBaseVersionId(Object baseVersionId) {
                this.baseVersionId = baseVersionId;
            }

            public Object getBaseVersionName() {
                return baseVersionName;
            }

            public void setBaseVersionName(Object baseVersionName) {
                this.baseVersionName = baseVersionName;
            }

            public Object getBaseVolumeId() {
                return baseVolumeId;
            }

            public void setBaseVolumeId(Object baseVolumeId) {
                this.baseVolumeId = baseVolumeId;
            }

            public Object getBaseVolumeName() {
                return baseVolumeName;
            }

            public void setBaseVolumeName(Object baseVolumeName) {
                this.baseVolumeName = baseVolumeName;
            }
        }

    }
}
