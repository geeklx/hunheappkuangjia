package com.sdzn.fzx.teacher.vo;


import androidx.annotation.NonNull;

import java.util.List;

public class ExamText extends AnswerExamCorrectAllStudentVo.DataBean {

    /**
     * abilityStructureRy : 5093,5085
     * analyticMethodRy :
     * baseEducationId : 1
     * baseEducationName : 六三制
     * baseGradeId : 8
     * baseGradeName : 八年级
     * <p>
     * baseLevelId : 2
     * baseLevelName : 初中
     * baseSubjectId : 2
     * baseSubjectName : 数学
     * baseVersionId : 10
     * baseVersionName : 人教版
     * baseVolumeId : 50
     * baseVolumeName : 上册
     * chapterId : 4496
     * chapterName : 第十一章 三角形
     * chapterNodeIdPath : 0.4496
     * chapterNodeNamePath : 第十一章 三角形
     * createTime : 1583898487878
     * createUserId : 2169
     * createUserName : 王宁ceshi
     * customerSchoolId : 46
     * customerSchoolName : 时代智囊中学
     * delflag : 1
     * difficulty : 2
     * downCount : 0
     * examAnalysis : 如图：<p><img src="http://csfile.fuzhuxian.com/2020/2/c06fb494168c409c965e42ce40eca07b/hd48kCvBHYzdlg7N9lGuIZzJJ1Geeiba.jpg"></p>∵△ABC是含有45°角的直角三角板，<br>∴∠A=∠C=45°，<br>∵∠1=23°，<br>∴∠AGB=∠C+∠1=68°，<br>∵EF∥BD，<br>∴∠2=∠AGB=68°．<br>故答案为：68．
     * examAnswer : 68
     * examId : 5e698daf032f71432fcc91e1
     * examOptions : [{"analysis":"","content":"68","right":true,"seq":0}]
     * examStem : 把一块含有45°角的直角三角板与两条长边平行的直尺如图放置(直角顶点在直尺的一条长边上)．若∠1=23°，则∠2=<span data-ph="1"><input index='1' class='cus-com' readonly='readonly' type='text' value='(1)'></span>°．<br><img src="http://csfile.fuzhuxian.com/2020/2/d294e9846772450da8a82907c560aeb4/ugWzDgJLqOUSwMOUxnjemVjijml43ZQA.jpg">
     * examTypeId : 6
     * favTime : 0
     * flagOpen : 0
     * flagShare : 0
     * holdUserId : 2169
     * holdUserName : 王宁ceshi
     * id : 5e698dd3032f71432fcc91f7
     * knowledgePointIdRy : [13707]
     * lableStatus : 1
     * lessonLibId : 944
     * lessonTaskId : 1107
     * optionNumber : 1
     * paperCount : 0
     * partnerExamId : 77277921
     * points : [{"id":383,"levelId":2,"name":"平行线的性质","no":"15817","nodeIdPath":"0.347.373.383","parentId":"373","subjectId":2},{"id":394,"levelId":2,"name":"三角形的外角性质","no":"13707","nodeIdPath":"0.347.386.394","parentId":"386","subjectId":2}]
     * questionLevelRy : 2
     * score : 0
     * sourceId : 2
     * sourceName : 软云
     * templateStyleId : 27
     * templateStyleName : 填空题
     * updateTime : 1583898487878
     * viewCount : 0
     * zoneIdPath : 0.15.223.5025
     * zoneName : 高新区
     */

    private String abilityStructureRy;
    private String analyticMethodRy;
    private int baseEducationId;
    private String baseEducationName;
    private int baseGradeId;
    private String baseGradeName;
    private int baseLevelId;
    private String baseLevelName;
    private int baseSubjectId;
    private String baseSubjectName;
    private int baseVersionId;
    private String baseVersionName;
    private int baseVolumeId;
    private String baseVolumeName;
    private int chapterId;
    private String chapterName;
    private String chapterNodeIdPath;
    private String chapterNodeNamePath;
    private long createTime;
    private int createUserId;
    private String createUserName;
    private int customerSchoolId;
    private String customerSchoolName;
    private int delflag;
    private int difficulty;
    private int downCount;
    private String examAnalysis;
    private String examAnswer;
    private String examId;
    private String examStem;
    private int examTypeId;
    private int favTime;
    private int flagOpen;
    private int flagShare;
    private int holdUserId;
    private String holdUserName;
    private String id;
    private int lableStatus;
    private int lessonLibId;
    private int lessonTaskId;
    private int optionNumber;
    private int paperCount;
    private String partnerExamId;
    private int questionLevelRy;
    private int score;
    private int sourceId;
    private String sourceName;
    private int templateStyleId;
    private String templateStyleName;
    private long updateTime;
    private int viewCount;
    private String zoneIdPath;
    private String zoneName;
    private List<ExamOptionsBean> examOptions;
    private List<ExamBasesBean> examBases;
    private List<Integer> knowledgePointIdRy;
    private List<PointsBean> points;

    public String getAbilityStructureRy() {
        return abilityStructureRy;
    }

    public void setAbilityStructureRy(String abilityStructureRy) {
        this.abilityStructureRy = abilityStructureRy;
    }

    public String getAnalyticMethodRy() {
        return analyticMethodRy;
    }

    public void setAnalyticMethodRy(String analyticMethodRy) {
        this.analyticMethodRy = analyticMethodRy;
    }

    public int getBaseEducationId() {
        return baseEducationId;
    }

    public void setBaseEducationId(int baseEducationId) {
        this.baseEducationId = baseEducationId;
    }

    public String getBaseEducationName() {
        return baseEducationName;
    }

    public void setBaseEducationName(String baseEducationName) {
        this.baseEducationName = baseEducationName;
    }

    public int getBaseGradeId() {
        return baseGradeId;
    }

    public void setBaseGradeId(int baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseGradeName() {
        return baseGradeName;
    }

    public void setBaseGradeName(String baseGradeName) {
        this.baseGradeName = baseGradeName;
    }

    public int getBaseLevelId() {
        return baseLevelId;
    }

    public void setBaseLevelId(int baseLevelId) {
        this.baseLevelId = baseLevelId;
    }

    public String getBaseLevelName() {
        return baseLevelName;
    }

    public void setBaseLevelName(String baseLevelName) {
        this.baseLevelName = baseLevelName;
    }

    public int getBaseSubjectId() {
        return baseSubjectId;
    }

    public void setBaseSubjectId(int baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public String getBaseSubjectName() {
        return baseSubjectName;
    }

    public void setBaseSubjectName(String baseSubjectName) {
        this.baseSubjectName = baseSubjectName;
    }

    public int getBaseVersionId() {
        return baseVersionId;
    }

    public void setBaseVersionId(int baseVersionId) {
        this.baseVersionId = baseVersionId;
    }

    public String getBaseVersionName() {
        return baseVersionName;
    }

    public void setBaseVersionName(String baseVersionName) {
        this.baseVersionName = baseVersionName;
    }

    public int getBaseVolumeId() {
        return baseVolumeId;
    }

    public void setBaseVolumeId(int baseVolumeId) {
        this.baseVolumeId = baseVolumeId;
    }

    public String getBaseVolumeName() {
        return baseVolumeName;
    }

    public void setBaseVolumeName(String baseVolumeName) {
        this.baseVolumeName = baseVolumeName;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterNodeIdPath() {
        return chapterNodeIdPath;
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getChapterNodeNamePath() {
        return chapterNodeNamePath;
    }

    public void setChapterNodeNamePath(String chapterNodeNamePath) {
        this.chapterNodeNamePath = chapterNodeNamePath;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public int getCustomerSchoolId() {
        return customerSchoolId;
    }

    public void setCustomerSchoolId(int customerSchoolId) {
        this.customerSchoolId = customerSchoolId;
    }

    public String getCustomerSchoolName() {
        return customerSchoolName;
    }

    public void setCustomerSchoolName(String customerSchoolName) {
        this.customerSchoolName = customerSchoolName;
    }

    public int getDelflag() {
        return delflag;
    }

    public void setDelflag(int delflag) {
        this.delflag = delflag;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getDownCount() {
        return downCount;
    }

    public void setDownCount(int downCount) {
        this.downCount = downCount;
    }

    public String getExamAnalysis() {
        return examAnalysis;
    }

    public void setExamAnalysis(String examAnalysis) {
        this.examAnalysis = examAnalysis;
    }

    public String getExamAnswer() {
        return examAnswer;
    }

    public void setExamAnswer(String examAnswer) {
        this.examAnswer = examAnswer;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getExamStem() {
        return examStem;
    }

    public void setExamStem(String examStem) {
        this.examStem = examStem;
    }

    public int getExamTypeId() {
        return examTypeId;
    }

    public void setExamTypeId(int examTypeId) {
        this.examTypeId = examTypeId;
    }

    public int getFavTime() {
        return favTime;
    }

    public void setFavTime(int favTime) {
        this.favTime = favTime;
    }

    public int getFlagOpen() {
        return flagOpen;
    }

    public void setFlagOpen(int flagOpen) {
        this.flagOpen = flagOpen;
    }

    public int getFlagShare() {
        return flagShare;
    }

    public void setFlagShare(int flagShare) {
        this.flagShare = flagShare;
    }

    public int getHoldUserId() {
        return holdUserId;
    }

    public void setHoldUserId(int holdUserId) {
        this.holdUserId = holdUserId;
    }

    public String getHoldUserName() {
        return holdUserName;
    }

    public void setHoldUserName(String holdUserName) {
        this.holdUserName = holdUserName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLableStatus() {
        return lableStatus;
    }

    public void setLableStatus(int lableStatus) {
        this.lableStatus = lableStatus;
    }

    public int getLessonLibId() {
        return lessonLibId;
    }

    public void setLessonLibId(int lessonLibId) {
        this.lessonLibId = lessonLibId;
    }

    public int getLessonTaskId() {
        return lessonTaskId;
    }

    public void setLessonTaskId(int lessonTaskId) {
        this.lessonTaskId = lessonTaskId;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }

    public int getPaperCount() {
        return paperCount;
    }

    public void setPaperCount(int paperCount) {
        this.paperCount = paperCount;
    }

    public String getPartnerExamId() {
        return partnerExamId;
    }

    public void setPartnerExamId(String partnerExamId) {
        this.partnerExamId = partnerExamId;
    }

    public int getQuestionLevelRy() {
        return questionLevelRy;
    }

    public void setQuestionLevelRy(int questionLevelRy) {
        this.questionLevelRy = questionLevelRy;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public int getTemplateStyleId() {
        return templateStyleId;
    }

    public void setTemplateStyleId(int templateStyleId) {
        this.templateStyleId = templateStyleId;
    }

    public String getTemplateStyleName() {
        return templateStyleName;
    }

    public void setTemplateStyleName(String templateStyleName) {
        this.templateStyleName = templateStyleName;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getZoneIdPath() {
        return zoneIdPath;
    }

    public void setZoneIdPath(String zoneIdPath) {
        this.zoneIdPath = zoneIdPath;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public List<ExamOptionsBean> getExamOptions() {
        return examOptions;
    }

    public void setExamOptions(List<ExamOptionsBean> examOptions) {
        this.examOptions = examOptions;
    }

    public List<Integer> getKnowledgePointIdRy() {
        return knowledgePointIdRy;
    }

    public void setKnowledgePointIdRy(List<Integer> knowledgePointIdRy) {
        this.knowledgePointIdRy = knowledgePointIdRy;
    }

    public List<PointsBean> getPoints() {
        return points;
    }

    public void setPoints(List<PointsBean> points) {
        this.points = points;
    }

    public static class ExamBasesBean {
        /**
         * baseEducationId : 1
         * baseEducationName : 六三制
         * baseGradeId : 8
         * baseGradeName : 八年级\r\n
         * baseLevelId : 2
         * baseLevelName : 初中
         * baseSubjectId : 2
         * baseSubjectName : 数学
         * baseVersionId : 10
         * baseVersionName : 人教版
         * baseVolumeId : 50
         * baseVolumeName : 上册
         * chapterId : 0
         * chapterName : 全部章节
         * chapterNodeIdPath : 0
         * chapterNodeNamePath : 全部章节
         * createTime : 1583975746247
         * createUserId : 2169
         * createUserName : 王宁ceshi
         * customerSchoolId : 46
         * customerSchoolName : 时代智囊中学
         * difficulty : 1
         * examAnalysis : <p>综合题单选题解析</p>
         * examAnswer : A
         * examExplain :
         * examOptions : [{"analysis":"","content":"<p>1<\/p>","right":true,"seq":1},{"analysis":"","content":"<p>2<\/p>","right":false,"seq":2},{"analysis":"","content":"<p>3<\/p>","right":false,"seq":3},{"analysis":"","content":"<p>4<\/p>","right":false,"seq":4}]
         * examStem : <p>综合题单选题题干</p>
         * examTypeId : 1
         * flagShare : 0
         * holdUserId : 2169
         * holdUserName : 王宁ceshi
         * hots : 0
         * id : 5e698cbc032f71432fcc91bc
         * optionNumber : 4
         * score : 2
         * templateStyleId : 335
         * templateStyleName : 综合题
         * type : 1
         * updateTime : 1583975746247
         */

        private int baseEducationId;
        private String baseEducationName;
        private int baseGradeId;
        private String baseGradeName;
        private int baseLevelId;
        private String baseLevelName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseVersionId;
        private String baseVersionName;
        private int baseVolumeId;
        private String baseVolumeName;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private long createTime;
        private int createUserId;
        private String createUserName;
        private int customerSchoolId;
        private String customerSchoolName;
        private int difficulty;
        private String examAnalysis;
        private String examAnswer;
        private String examExplain;
        private String examStem;
        private int examTypeId;
        private int flagShare;
        private int holdUserId;
        private String holdUserName;
        private int hots;
        private String id;
        private int optionNumber;
        private int score;
        private int templateStyleId;
        private String templateStyleName;
        private int type;
        private long updateTime;
        private List<ExamOptionsBeans> examOptionss;

        public int getBaseEducationId() {
            return baseEducationId;
        }

        public void setBaseEducationId(int baseEducationId) {
            this.baseEducationId = baseEducationId;
        }

        public String getBaseEducationName() {
            return baseEducationName;
        }

        public void setBaseEducationName(String baseEducationName) {
            this.baseEducationName = baseEducationName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getBaseLevelId() {
            return baseLevelId;
        }

        public void setBaseLevelId(int baseLevelId) {
            this.baseLevelId = baseLevelId;
        }

        public String getBaseLevelName() {
            return baseLevelName;
        }

        public void setBaseLevelName(String baseLevelName) {
            this.baseLevelName = baseLevelName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseVersionId() {
            return baseVersionId;
        }

        public void setBaseVersionId(int baseVersionId) {
            this.baseVersionId = baseVersionId;
        }

        public String getBaseVersionName() {
            return baseVersionName;
        }

        public void setBaseVersionName(String baseVersionName) {
            this.baseVersionName = baseVersionName;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public int getDifficulty() {
            return difficulty;
        }

        public void setDifficulty(int difficulty) {
            this.difficulty = difficulty;
        }

        public String getExamAnalysis() {
            return examAnalysis;
        }

        public void setExamAnalysis(String examAnalysis) {
            this.examAnalysis = examAnalysis;
        }

        public String getExamAnswer() {
            return examAnswer;
        }

        public void setExamAnswer(String examAnswer) {
            this.examAnswer = examAnswer;
        }

        public String getExamExplain() {
            return examExplain;
        }

        public void setExamExplain(String examExplain) {
            this.examExplain = examExplain;
        }

        public String getExamStem() {
            return examStem;
        }

        public void setExamStem(String examStem) {
            this.examStem = examStem;
        }

        public int getExamTypeId() {
            return examTypeId;
        }

        public void setExamTypeId(int examTypeId) {
            this.examTypeId = examTypeId;
        }

        public int getFlagShare() {
            return flagShare;
        }

        public void setFlagShare(int flagShare) {
            this.flagShare = flagShare;
        }

        public int getHoldUserId() {
            return holdUserId;
        }

        public void setHoldUserId(int holdUserId) {
            this.holdUserId = holdUserId;
        }

        public String getHoldUserName() {
            return holdUserName;
        }

        public void setHoldUserName(String holdUserName) {
            this.holdUserName = holdUserName;
        }

        public int getHots() {
            return hots;
        }

        public void setHots(int hots) {
            this.hots = hots;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getOptionNumber() {
            return optionNumber;
        }

        public void setOptionNumber(int optionNumber) {
            this.optionNumber = optionNumber;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getTemplateStyleId() {
            return templateStyleId;
        }

        public void setTemplateStyleId(int templateStyleId) {
            this.templateStyleId = templateStyleId;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public List<ExamOptionsBeans> getExamOptionss() {
            return examOptionss;
        }

        public void setExamOptionss(List<ExamOptionsBeans> examOptionss) {
            this.examOptionss = examOptionss;
        }

        public static class ExamOptionsBeans {
            /**
             * analysis :
             * content : <p>1</p>
             * right : true
             * seq : 1
             */

            private String analysis;
            private String content;
            private boolean right;
            private int seq;

            public String getAnalysis() {
                return analysis;
            }

            public void setAnalysis(String analysis) {
                this.analysis = analysis;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public boolean isRight() {
                return right;
            }

            public void setRight(boolean right) {
                this.right = right;
            }

            public int getSeq() {
                return seq;
            }

            public void setSeq(int seq) {
                this.seq = seq;
            }
        }
    }

    public static class ExamOptionsBean implements Comparable<ExamOptionsBean> {
        /**
         * analysis :
         * content : 68
         * right : true
         * seq : 0
         */

        private String analysis;
        private String content;
        private boolean right;
        private int seq;

        public String getAnalysis() {
            return analysis;
        }

        public void setAnalysis(String analysis) {
            this.analysis = analysis;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptionsBean o) {
            return Integer.compare(seq, o.seq);
        }
    }

    public static class PointsBean {
        /**
         * id : 383
         * levelId : 2
         * name : 平行线的性质
         * no : 15817
         * nodeIdPath : 0.347.373.383
         * parentId : 373
         * subjectId : 2
         */

        private int id;
        private int levelId;
        private String name;
        private String no;
        private String nodeIdPath;
        private String parentId;
        private int subjectId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevelId() {
            return levelId;
        }

        public void setLevelId(int levelId) {
            this.levelId = levelId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getNodeIdPath() {
            return nodeIdPath;
        }

        public void setNodeIdPath(String nodeIdPath) {
            this.nodeIdPath = nodeIdPath;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }
    }
}