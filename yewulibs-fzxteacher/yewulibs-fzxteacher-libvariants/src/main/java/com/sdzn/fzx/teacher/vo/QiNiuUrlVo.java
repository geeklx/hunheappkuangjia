package com.sdzn.fzx.teacher.vo;

public class QiNiuUrlVo {

    /**
     * fileUrl : https://file.znclass.com/31a639ff-e49d-475e-98d6-2c543cc8f4e1.jpg
     */

    private String fileUrl;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
