package com.sdzn.fzx.teacher.vo.me;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/8
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextBookClassVo {
    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
