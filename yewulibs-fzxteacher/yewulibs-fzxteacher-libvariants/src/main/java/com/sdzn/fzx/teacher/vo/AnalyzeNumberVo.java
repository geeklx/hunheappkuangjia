package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/20
 * 修改单号：
 * 修改内容:
 */
public class AnalyzeNumberVo {
    /**
     * total : 4
     * data : [{"studentCount":10,"examTemplateId":1,"examTemplateStyleId":15,"examId":7,"scoreTotal":2,"examTemplateStyleName":"选择题","accuracy":3,"id":7,"scoreGet":0,"rightCount":0,"scoreAvg":0},{"studentCount":10,"examTemplateId":1,"examTemplateStyleId":15,"examId":8,"scoreTotal":2,"examTemplateStyleName":"选择题","accuracy":3,"id":8,"scoreGet":0,"rightCount":0,"scoreAvg":0},{"studentCount":10,"examTemplateId":1,"examTemplateStyleId":15,"examId":9,"scoreTotal":2,"examTemplateStyleName":"选择题","accuracy":3,"id":9,"scoreGet":0,"rightCount":0,"scoreAvg":0},{"studentCount":10,"examTemplateId":1,"examTemplateStyleId":15,"examId":10,"scoreTotal":2,"examTemplateStyleName":"选择题","accuracy":1,"id":10,"scoreGet":2,"rightCount":1,"scoreAvg":2}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * studentCount : 10
         * examTemplateId : 1
         * examTemplateStyleId : 15
         * examId : 7
         * scoreTotal : 2.0
         * examTemplateStyleName : 选择题
         * accuracy : 3
         * id : 7
         * scoreGet : 0.0
         * rightCount : 0
         * scoreAvg : 0.0
         */

        private int studentCount;
        private int examTemplateId;
        private int examTemplateStyleId;
        private int examId;
        private double scoreTotal;
        private String examTemplateStyleName;
        private int accuracy;
        private int id;
        private double scoreGet;
        private int rightCount;
        private double scoreAvg;

        public int getStudentCount() {
            return studentCount;
        }

        public void setStudentCount(int studentCount) {
            this.studentCount = studentCount;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public int getExamId() {
            return examId;
        }

        public void setExamId(int examId) {
            this.examId = examId;
        }

        public double getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(double scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public String getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getAccuracy() {
            return accuracy;
        }

        public void setAccuracy(int accuracy) {
            this.accuracy = accuracy;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getScoreGet() {
            return scoreGet;
        }

        public void setScoreGet(double scoreGet) {
            this.scoreGet = scoreGet;
        }

        public int getRightCount() {
            return rightCount;
        }

        public void setRightCount(int rightCount) {
            this.rightCount = rightCount;
        }

        public double getScoreAvg() {
            return scoreAvg;
        }

        public void setScoreAvg(double scoreAvg) {
            this.scoreAvg = scoreAvg;
        }
    }
}
