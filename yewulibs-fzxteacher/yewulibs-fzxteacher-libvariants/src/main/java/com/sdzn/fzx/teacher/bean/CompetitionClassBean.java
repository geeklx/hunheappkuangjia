package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class CompetitionClassBean {


    /**
     * classesId : 112
     * classesName : 初一一班
     * gradeId : 7
     * gradeName : 初一
     * schoolClassesGroupsRes : []
     */

    private String classesId;
    private String classesName;
    private int gradeId;
    private String gradeName;
    private List<?> schoolClassesGroupsRes;

    public String getClassesId() {
        return classesId;
    }

    public void setClassesId(String classesId) {
        this.classesId = classesId;
    }

    public String getClassesName() {
        return classesName;
    }

    public void setClassesName(String classesName) {
        this.classesName = classesName;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public List<?> getSchoolClassesGroupsRes() {
        return schoolClassesGroupsRes;
    }

    public void setSchoolClassesGroupsRes(List<?> schoolClassesGroupsRes) {
        this.schoolClassesGroupsRes = schoolClassesGroupsRes;
    }
}
