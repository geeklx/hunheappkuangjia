package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;

/**
 * 首页 view
 */
public interface SendIsHideViews extends IView {
    void onSendIsHideSuccess(Object object);

    void onSendIsHideNodata(String msg);

    void onSendIsHideFail(String msg);
}
