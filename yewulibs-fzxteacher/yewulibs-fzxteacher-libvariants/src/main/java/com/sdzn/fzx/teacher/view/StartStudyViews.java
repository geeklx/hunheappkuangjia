package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.StartStudyBean;

/**
 * 首页 view
 */
public interface StartStudyViews extends IView {
    void onStartStudySuccess(StartStudyBean startStudyBean);

//    void onStartStudyNodata(String msg);

    void onStartStudyFail(String msg);
}
