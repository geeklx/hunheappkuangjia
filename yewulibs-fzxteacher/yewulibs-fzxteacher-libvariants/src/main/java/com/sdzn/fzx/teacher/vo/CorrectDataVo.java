package com.sdzn.fzx.teacher.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wangc on 2018/6/26 0026.
 */

public class CorrectDataVo implements Parcelable {
    /**
     * seq : 1、简答题
     * id : 117835
     * list : 0
     * src : http://115.28.91.19:9001/exam/2018/06/1b37ebd4737846dbb4da0e656e96c069.png
     */

    private String eid;
    private String seq;
    private int id;
    private String list;
    private String src;

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.eid);
        dest.writeString(this.seq);
        dest.writeInt(this.id);
        dest.writeString(this.list);
        dest.writeString(this.src);
    }

    public CorrectDataVo() {
    }

    protected CorrectDataVo(Parcel in) {
        this.eid = in.readString();
        this.seq = in.readString();
        this.id = in.readInt();
        this.list = in.readString();
        this.src = in.readString();
    }

    public static final Parcelable.Creator<CorrectDataVo> CREATOR = new Parcelable.Creator<CorrectDataVo>() {
        @Override
        public CorrectDataVo createFromParcel(Parcel source) {
            return new CorrectDataVo(source);
        }

        @Override
        public CorrectDataVo[] newArray(int size) {
            return new CorrectDataVo[size];
        }
    };
}
