package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.TreeBean;
import com.sdzn.fzx.teacher.view.TeachingCoursesViews;
import com.sdzn.fzx.teacher.view.TreeViews;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -Tree树结构
 */
public class TreePresenter extends Presenter<TreeViews> {

    public void queryTree(String token,String volumeid) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryTree(BuildConfig3.SERVER_ISERVICE_NEW2 + "/knowledgecenter/bookChapter/list", token, volumeid)
                .enqueue(new Callback<ResponseSlbBean1<List<TreeBean>>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<List<TreeBean>>> call, Response<ResponseSlbBean1<List<TreeBean>>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onTreeNodata(response.body().getMessage());
                            return;
                        }
                        getView().onTreeSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<List<TreeBean>>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onTreeFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
