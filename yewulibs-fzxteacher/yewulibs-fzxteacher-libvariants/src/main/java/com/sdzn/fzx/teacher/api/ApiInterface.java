//package com.sdzn.fzx.teacher.api;
//
//import com.sdzn.fzx.teacher.BuildConfig3;
//
///**
// * 描述：API请求地址
// */
//public class ApiInterface {
//
//    public static final String QUERY_VERSION_INFO = "http://doc.znclass.com/usercenter/sysVersionAppInfo/selectOne";//查询版本信息
//    public static final String QUERY_SHOUYE_INFO = BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/home/data";//查询首页信息
//    public static final String QUERY_CEHUA_INFO = BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/home/broadside/list";//侧边栏数据
//    public static final String QUERY_TAB_INFO = BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/home/tabJump/list";//Tab标题
//    public static final String LOGIN_INFO = BuildConfig3.SERVER_ISERVICE_NEW2 + "/teacher/login/token";//老师端登录
//    public static final String TEACHER_INFO = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/teacherInfo";//个人中心获取资料
//    public static final String TEACHER_INFO_UPDATE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/update/teacherInfo";//更新老师信息-头像
//    public static final String TEACHER_MOBILE_UPDATE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/update/mobile";//更新老师信息-手机号
//    public static final String TEACHER_PASSWORD_UPDATE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/update/password";//更新老师信息-密码
//    public static final String SEND_VERIFYCODE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/login/sendVerifyCode";//发送短信验证码
//    public static final String CHECK_VERIFYCODE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/teacher/check/smsToken";//验证验证码
//    public static final String QINIU_TOKEN = BuildConfig3.SERVER_ISERVICE_NEW2 + "/knowledgecenter/resource/getUploadToken";//获取七牛token
//    public static final String QINIU_URL = BuildConfig3.SERVER_ISERVICE_NEW2 + "/knowledgecenter/resource/getUrlByFileName";//七牛获取token
//    public static final String TEACHER_CLASSES = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/teach/classes";//任教信息
//    public static final String TEACHER_LOGIN_MOBILE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/find/teacherMobile";//忘记密码--通过账号/手机号 获取手机号
//    public static final String TEACHER_LOGIN_PASSWORD_UPDATE = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/teacher/forget/password";//忘记密码--更新密码
//    public static final String TEACHER_SUBJECTDATA = BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacherBookVersion/list";//忘记密码--更新密码
//    public static final String QUERY_TOKEN = INTERFACE_ADDRESS + "/teacher/login/refresh/token";//刷新token

//
//
//}

