package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class ClassesBean {
    /**
     * classesId : 112
     * classesName : 初一一班
     * gradeId : 7
     * gradeName : 初一
     * schoolClassesGroupsRes : []
     * isChecked;//是否选中
     * hiddenRadio;//是否需要隐藏radio
     */

    private String classesId;
    private String classesName;
    private int gradeId;
    private String gradeName;
    private List<?> schoolClassesGroupsRes;
    public boolean isChecked;//是否选中
    public boolean hiddenRadio;//是否需要隐藏radio

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isHiddenRadio() {
        return hiddenRadio;
    }

    public void setHiddenRadio(boolean hiddenRadio) {
        this.hiddenRadio = hiddenRadio;
    }

    public String getClassesId() {
        return classesId;
    }

    public void setClassesId(String classesId) {
        this.classesId = classesId;
    }

    public String getClassesName() {
        return classesName;
    }

    public void setClassesName(String classesName) {
        this.classesName = classesName;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public List<?> getSchoolClassesGroupsRes() {
        return schoolClassesGroupsRes;
    }

    public void setSchoolClassesGroupsRes(List<?> schoolClassesGroupsRes) {
        this.schoolClassesGroupsRes = schoolClassesGroupsRes;
    }
}

