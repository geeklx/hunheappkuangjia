package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/25
 * 修改单号：
 * 修改内容:
 */
public class ExamAnalyzeVo {


    /**
     * data : {"examTemplateId":1,"examTemplateStyleId":57,"examTemplateStyleName":"选择题","score":-1,"examList":[{"examTemplateId":1,"examTemplateStyleId":57,"examTemplateStyleName":"选择题","emptyCount":4,"examSeq":3,"answerList":[{"examSeq":3,"optionSeq":1,"optionList":[{"examSeq":3,"optionSeq":1,"myAnswer":"A","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":2,"myAnswer":"B","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":3,"myAnswer":"C","count":1,"rate":100,"right":false,"myOldAnswer":"C","clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":4,"myAnswer":"D","count":0,"rate":0,"right":true,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null}]}],"unAnswer":{"rate":0,"unAnswer":0}}],"typicalErrorList":[],"excellentAnswerList":[{"lessonAnswerExamId":10789,"lessonTaskStudentId":1877,"lessonTaskId":54,"sex":1,"lessonTaskDetailsId":390,"photo":null,"userStudentName":"学生62","userStudentId":5166}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * examTemplateId : 1
         * examTemplateStyleId : 57
         * examTemplateStyleName : 选择题
         * score : -1.0
         * examList : [{"examTemplateId":1,"examTemplateStyleId":57,"examTemplateStyleName":"选择题","emptyCount":4,"examSeq":3,"answerList":[{"examSeq":3,"optionSeq":1,"optionList":[{"examSeq":3,"optionSeq":1,"myAnswer":"A","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":2,"myAnswer":"B","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":3,"myAnswer":"C","count":1,"rate":100,"right":false,"myOldAnswer":"C","clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":4,"myAnswer":"D","count":0,"rate":0,"right":true,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null}]}],"unAnswer":{"rate":0,"unAnswer":0}}]
         * typicalErrorList : []
         * excellentAnswerList : [{"lessonAnswerExamId":10789,"lessonTaskStudentId":1877,"lessonTaskId":54,"sex":1,"lessonTaskDetailsId":390,"photo":null,"userStudentName":"学生62","userStudentId":5166}]
         */

        private int examTemplateId;
        private int examTemplateStyleId;
        private String examTemplateStyleName;
        private double score;
        private List<ExamListBean> examList;
        private List<ExcellentAnswerListBean> typicalErrorList;
        private List<ExcellentAnswerListBean> excellentAnswerList;

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public String getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public List<ExamListBean> getExamList() {
            return examList;
        }

        public void setExamList(List<ExamListBean> examList) {
            this.examList = examList;
        }

        public List<ExcellentAnswerListBean> getTypicalErrorList() {
            return typicalErrorList;
        }

        public void setTypicalErrorList(List<ExcellentAnswerListBean> typicalErrorList) {
            this.typicalErrorList = typicalErrorList;
        }

        public List<ExcellentAnswerListBean> getExcellentAnswerList() {
            return excellentAnswerList;
        }

        public void setExcellentAnswerList(List<ExcellentAnswerListBean> excellentAnswerList) {
            this.excellentAnswerList = excellentAnswerList;
        }

        public static class ExamListBean {
            /**
             * examTemplateId : 1
             * examTemplateStyleId : 57
             * examTemplateStyleName : 选择题
             * emptyCount : 4
             * examSeq : 3
             * answerList : [{"examSeq":3,"optionSeq":1,"optionList":[{"examSeq":3,"optionSeq":1,"myAnswer":"A","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":2,"myAnswer":"B","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":3,"myAnswer":"C","count":1,"rate":100,"right":false,"myOldAnswer":"C","clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":4,"myAnswer":"D","count":0,"rate":0,"right":true,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null}]}]
             * unAnswer : {"rate":0,"unAnswer":0}
             */

            private int examTemplateId;
            private int examTemplateStyleId;
            private String examTemplateStyleName;
            private int emptyCount;
            private int examSeq;
            private UnAnswerBean unAnswer;
            private List<AnswerListBean> answerList;

            public int getExamTemplateId() {
                return examTemplateId;
            }

            public void setExamTemplateId(int examTemplateId) {
                this.examTemplateId = examTemplateId;
            }

            public int getExamTemplateStyleId() {
                return examTemplateStyleId;
            }

            public void setExamTemplateStyleId(int examTemplateStyleId) {
                this.examTemplateStyleId = examTemplateStyleId;
            }

            public String getExamTemplateStyleName() {
                return examTemplateStyleName;
            }

            public void setExamTemplateStyleName(String examTemplateStyleName) {
                this.examTemplateStyleName = examTemplateStyleName;
            }

            public int getEmptyCount() {
                return emptyCount;
            }

            public void setEmptyCount(int emptyCount) {
                this.emptyCount = emptyCount;
            }

            public int getExamSeq() {
                return examSeq;
            }

            public void setExamSeq(int examSeq) {
                this.examSeq = examSeq;
            }

            public UnAnswerBean getUnAnswer() {
                return unAnswer;
            }

            public void setUnAnswer(UnAnswerBean unAnswer) {
                this.unAnswer = unAnswer;
            }

            public List<AnswerListBean> getAnswerList() {
                return answerList;
            }

            public void setAnswerList(List<AnswerListBean> answerList) {
                this.answerList = answerList;
            }

            public static class UnAnswerBean {
                /**
                 * rate : 0.0
                 * unAnswer : 0
                 */

                private double rate;
                private int unAnswer;

                public double getRate() {
                    return rate;
                }

                public void setRate(double rate) {
                    this.rate = rate;
                }

                public int getUnAnswer() {
                    return unAnswer;
                }

                public void setUnAnswer(int unAnswer) {
                    this.unAnswer = unAnswer;
                }
            }

            public static class AnswerListBean {
                /**
                 * examSeq : 3
                 * optionSeq : 1
                 * optionList : [{"examSeq":3,"optionSeq":1,"myAnswer":"A","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":2,"myAnswer":"B","count":0,"rate":0,"right":false,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":3,"myAnswer":"C","count":1,"rate":100,"right":false,"myOldAnswer":"C","clozeSeq":null,"children":null,"answerType":null},{"examSeq":3,"optionSeq":4,"myAnswer":"D","count":0,"rate":0,"right":true,"myOldAnswer":null,"clozeSeq":null,"children":null,"answerType":null}]
                 */

                private int examSeq;
                private int optionSeq;
                private List<OptionListBean> optionList;

                public int getExamSeq() {
                    return examSeq;
                }

                public void setExamSeq(int examSeq) {
                    this.examSeq = examSeq;
                }

                public int getOptionSeq() {
                    return optionSeq;
                }

                public void setOptionSeq(int optionSeq) {
                    this.optionSeq = optionSeq;
                }

                public List<OptionListBean> getOptionList() {
                    return optionList;
                }

                public void setOptionList(List<OptionListBean> optionList) {
                    this.optionList = optionList;
                }

                public static class OptionListBean {
                    /**
                     * examSeq : 3
                     * optionSeq : 1
                     * myAnswer : A
                     * count : 0
                     * rate : 0.0
                     * right : false
                     * myOldAnswer : null
                     * clozeSeq : null
                     * children : null
                     * answerType : null
                     */

                    private int examSeq;
                    private int optionSeq;
                    private String myAnswer;
                    private int count;
                    private double rate;
                    private boolean right;
                    private Object myOldAnswer;
                    private Object clozeSeq;
                    private Object children;
                    private Object answerType;

                    public int getExamSeq() {
                        return examSeq;
                    }

                    public void setExamSeq(int examSeq) {
                        this.examSeq = examSeq;
                    }

                    public int getOptionSeq() {
                        return optionSeq;
                    }

                    public void setOptionSeq(int optionSeq) {
                        this.optionSeq = optionSeq;
                    }

                    public String getMyAnswer() {
                        return myAnswer;
                    }

                    public void setMyAnswer(String myAnswer) {
                        this.myAnswer = myAnswer;
                    }

                    public int getCount() {
                        return count;
                    }

                    public void setCount(int count) {
                        this.count = count;
                    }

                    public double getRate() {
                        return rate;
                    }

                    public void setRate(double rate) {
                        this.rate = rate;
                    }

                    public boolean isRight() {
                        return right;
                    }

                    public void setRight(boolean right) {
                        this.right = right;
                    }

                    public Object getMyOldAnswer() {
                        return myOldAnswer;
                    }

                    public void setMyOldAnswer(Object myOldAnswer) {
                        this.myOldAnswer = myOldAnswer;
                    }

                    public Object getClozeSeq() {
                        return clozeSeq;
                    }

                    public void setClozeSeq(Object clozeSeq) {
                        this.clozeSeq = clozeSeq;
                    }

                    public Object getChildren() {
                        return children;
                    }

                    public void setChildren(Object children) {
                        this.children = children;
                    }

                    public Object getAnswerType() {
                        return answerType;
                    }

                    public void setAnswerType(Object answerType) {
                        this.answerType = answerType;
                    }
                }
            }
        }

        public static class ExcellentAnswerListBean {
            /**
             * lessonAnswerExamId : 10789
             * lessonTaskStudentId : 1877
             * lessonTaskId : 54
             * sex : 1
             * lessonTaskDetailsId : 390
             * photo : null
             * userStudentName : 学生62
             * userStudentId : 5166
             */

            private int lessonAnswerExamId;
            private int lessonTaskStudentId;
            private int lessonTaskId;
            private int sex;
            private int lessonTaskDetailsId;
            private Object photo;
            private String userStudentName;
            private int userStudentId;

            public int getLessonAnswerExamId() {
                return lessonAnswerExamId;
            }

            public void setLessonAnswerExamId(int lessonAnswerExamId) {
                this.lessonAnswerExamId = lessonAnswerExamId;
            }

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public int getLessonTaskId() {
                return lessonTaskId;
            }

            public void setLessonTaskId(int lessonTaskId) {
                this.lessonTaskId = lessonTaskId;
            }

            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public int getLessonTaskDetailsId() {
                return lessonTaskDetailsId;
            }

            public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
                this.lessonTaskDetailsId = lessonTaskDetailsId;
            }

            public Object getPhoto() {
                return photo;
            }

            public void setPhoto(Object photo) {
                this.photo = photo;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }
        }
    }
}
