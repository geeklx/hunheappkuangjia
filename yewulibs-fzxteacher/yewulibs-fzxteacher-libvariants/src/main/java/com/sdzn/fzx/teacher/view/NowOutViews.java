package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;

/**
 * 首页 view
 */
public interface NowOutViews extends IView {
    void onNowOutSuccess(Object object);

    void onNowOutNodata(String msg);

    void onNowOutFail(String msg);
}
