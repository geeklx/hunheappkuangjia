package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/6
 * 修改单号：
 * 修改内容:
 */
public class QiNiuHashVo {

    /**
     * hash : lvSmPcLNcgXmlewmdd1XVox1Ufrl
     * key : 1075f040-74d6-4069-9357-94bfdc81e053knc_simplelife.mp4
     * persistentId : z0.5c7f359d38b9f349c8a0adce
     */

    private String hash;
    private String key;
    private String persistentId;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPersistentId() {
        return persistentId;
    }

    public void setPersistentId(String persistentId) {
        this.persistentId = persistentId;
    }
}
