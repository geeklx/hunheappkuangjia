package com.sdzn.fzx.teacher.api.network;

import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.module.StatusVo;

import com.sdzn.fzx.teacher.vo.BoxInfoBean;
import com.sdzn.fzx.teacher.vo.chatroom.ChatOtherBean;
import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.sdzn.fzx.teacher.vo.chatroom.GroupListBean;
import com.sdzn.fzx.teacher.vo.AnalyzeNumberVo;
import com.sdzn.fzx.teacher.vo.AnalyzeStatisticsVo;
import com.sdzn.fzx.teacher.vo.AnalyzeVo;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.CharactersListVo;
import com.sdzn.fzx.teacher.vo.CorrectPicVo;
import com.sdzn.fzx.teacher.vo.CorrectVo;
import com.sdzn.fzx.teacher.vo.DelTask;
import com.sdzn.fzx.teacher.vo.ExamAnalyzeVo;
import com.sdzn.fzx.teacher.vo.ExamCorrectVo;
import com.sdzn.fzx.teacher.vo.ExamVo;
import com.sdzn.fzx.teacher.vo.ExplainVo;
import com.sdzn.fzx.teacher.vo.FuFenBean;
import com.sdzn.fzx.teacher.vo.GroupStudent;
import com.sdzn.fzx.teacher.vo.GroupStudentDetils;
import com.sdzn.fzx.teacher.vo.GroupVo;
import com.sdzn.fzx.teacher.vo.HotFixVo;
import com.sdzn.fzx.teacher.vo.ImgList;
import com.sdzn.fzx.teacher.vo.InClassStatus;
import com.sdzn.fzx.teacher.vo.LessonListVo;
import com.sdzn.fzx.teacher.vo.LessonResource;
import com.sdzn.fzx.teacher.vo.LessonVo;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.OverallVo;
import com.sdzn.fzx.teacher.vo.PrepareLessonsVo;
import com.sdzn.fzx.teacher.vo.QiniuConfig;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.RefreshTokenBean;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;
import com.sdzn.fzx.teacher.vo.RgAnalyze;
import com.sdzn.fzx.teacher.vo.RgDetailsVo;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StringData;
import com.sdzn.fzx.teacher.vo.StudentDetailExam;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.teacher.vo.StudentList;
import com.sdzn.fzx.teacher.vo.StudentListTiWen;
import com.sdzn.fzx.teacher.vo.StudentListVo;
import com.sdzn.fzx.teacher.vo.StudentTaskExamListVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;
import com.sdzn.fzx.teacher.vo.TaskListVo;
import com.sdzn.fzx.teacher.vo.TeachClassVo;
import com.sdzn.fzx.teacher.vo.UndoCorrectDataVo;
import com.sdzn.fzx.teacher.vo.UpdateVo;
import com.sdzn.fzx.teacher.vo.UserBean;
import com.sdzn.fzx.teacher.vo.UserTeacherInfo;
import com.sdzn.fzx.teacher.vo.VerifyUserNameBean;
import com.sdzn.fzx.teacher.vo.VersionBean;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.sdzn.fzx.teacher.vo.me.DeleteVolumeVo;
import com.sdzn.fzx.teacher.vo.me.ReviewInfo;
import com.sdzn.fzx.teacher.vo.me.ReviewInfoAdd;
import com.sdzn.fzx.teacher.vo.me.TellSetVo;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;
import com.sdzn.fzx.teacher.vo.me.VersionVo;
import com.sdzn.fzx.teacher.vo.me.VolumeVo;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by bin_li on 2017/4/13.
 */

public class NetWorkService {


    public interface TestService {
        @POST("/auth/account/info")
        Observable<StatusVo<Object>> control();
    }


    //正式区域

    public interface RefreshToken {
        @POST(BuildConfig3.AUTH + "/login/refresh/token/teacher/pad")
        Call<RefreshTokenBean> refresh(@Query("refresh_token") String refreshToken, @Query("accountId") String accountId);

    }


    public interface LoginService {
        @POST(BuildConfig3.AUTH + "/login/teacher/pad")
        @FormUrlEncoded
        Observable<StatusVo<LoginBean>> login(@Field("name") String name, @Field("password") String password, @Field("deviceId") String deviceId, @Field("deviceType") String deviceType);

        @POST("/customer/school/box/listForAndriod")
        @FormUrlEncoded
        Observable<StatusVo<BoxInfoBean>> getBoxUrl(@Field("customerSchoolId") String customerSchoolId);
    }

    public interface ForgetPswService {
        @POST(BuildConfig3.AUTH + "/login/teacher/pad/account/validate")
        @FormUrlEncoded
        Observable<StatusVo<VerifyUserNameBean>> verityUserName(@Field("name") String idCards);

        @POST(BuildConfig3.AUTH + "/login/code/get")
        @FormUrlEncoded
        Observable<StatusVo<Object>> sendVerityCode(@Field("phone") String phone, @Field("type") String type);

        @POST(BuildConfig3.AUTH + "/login/code/validate")
        @FormUrlEncoded
        Observable<StatusVo<Object>> checkVerityCode(@Field("phone") String phone, @Field("code") String code);

        @POST(BuildConfig3.AUTH + "/login/teacher/password/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> checkVerityCode(@Field("phone") String phone, @Field("code") String code, @Field("newPassword") String newPassword, @Field("confirmPassword") String confirmPassword);

    }

    public interface GetUserInfoService {
        @GET("auth/account/info")
        Observable<StatusVo<UserBean>> getUserInfo();
    }

    public interface MainService {
        @POST("/teach/task/list")
        @FormUrlEncoded
        Observable<StatusVo<TaskListVo>> getTaskList(@FieldMap Map<String, String> params);

        @POST("/teach/lesson/list")
        @FormUrlEncoded
        Observable<StatusVo<LessonListVo>> getLessonList(@FieldMap Map<String, String> params);

        @POST("/auth/teach/class/teacher/list")
        @FormUrlEncoded
        Observable<StatusVo<SyncClassVo>> getClassList(@Field("subjectId") String subjectId);

        @POST("/auth/class/group/point/sort")
        @FormUrlEncoded
        Observable<StatusVo<RankVo>> getRank(@FieldMap Map<String, String> params);

        @POST("/teach/task/rate/list")
        @FormUrlEncoded
        Observable<StatusVo<CorrectVo>> getCorrect(@FieldMap Map<String, String> params);

        @POST("/customer/app/version/new")
        @FormUrlEncoded
        Observable<StatusVo<UpdateVo>> checkUpdate(@FieldMap Map<String, String> params);

        @POST("teach/dict/illegal/characters/list")
        Observable<StatusVo<CharactersListVo>> getCharactersList();

        @POST("/upgrade/AppVersionShortController/selectLastOne")
        @FormUrlEncoded
        Observable<StatusVo<HotFixVo>> checkHotfix(@Field("programName") int programName);
    }

    public interface ScoreService {
        @POST("/auth/class/group/point/pc/sort")
        @FormUrlEncoded
        Observable<StatusVo<GroupVo>> getGroupList(@FieldMap Map<String, String> params);

        @POST("/auth/class/group/point/add")
        @FormUrlEncoded
        Observable<StatusVo<Object>> modifyPoint(@FieldMap Map<String, String> params);
    }

    public interface CorrectService {
        @POST("/teach/student/exam/newPicture/save")
        @FormUrlEncoded
        Observable<StatusVo<CorrectPicVo>> saveCorrectPic(@FieldMap Map<String, String> params);

        @POST("/teach/student/exam/picture/retrun")
        @FormUrlEncoded
        Observable<StatusVo<UndoCorrectDataVo>> undoCorrectPic(@FieldMap Map<String, String> params);
    }

    public interface TeachActivityService {
        /**
         * @param baseEducationId   版本
         * @param baseGradeId       年级
         * @param baseVolumeId      册别
         * @param baseSubjectId     科目
         * @param status            发布状态： 0 全部；1待发布2；进行中；3已截止；4批改中
         * @param baseClassId       班级id
         * @param typeReview        复习类型
         * @param startTime         开始时间
         * @param endTime           结束时间
         * @param chapterId         章节id
         * @param chapterNodeIdPath 章节id路径
         * @param type              类型：1同步，2复习
         * @param page              当前页
         * @param rows              每页条数
         * @param keyword           关键字
         * @return
         */
        @POST("/teach/task/list")
        @FormUrlEncoded
        Observable<StatusVo<SyncTaskVo>> getTaskList(@Field("baseEducationId") String baseEducationId,
                                                     @Field("baseGradeId") String baseGradeId,
                                                     @Field("baseVolumeId") String baseVolumeId,
                                                     @Field("baseSubjectId") String baseSubjectId,
                                                     @Field("sceneId") String sceneId,
                                                     @Field("status") String status,
                                                     @Field("classId") String baseClassId,
                                                     @Field("typeReview") String typeReview,
                                                     @Field("startTime") String startTime,
                                                     @Field("endTime") String endTime,
                                                     @Field("chapterId") String chapterId,
                                                     @Field("chapterNodeIdPath") String chapterNodeIdPath,
                                                     @Field("type") String type,
                                                     @Field("page") String page,
                                                     @Field("rows") String rows,
                                                     @Field("keyword") String keyword);

        @POST("/auth/user/teacher/version/list")
        @FormUrlEncoded
        Observable<StatusVo<VersionBean>> getVersionList(@Field("baseSubjectId") String baseSubjectId);

        @POST("/teach/CategoryController/list")
        @FormUrlEncoded
        Observable<StatusVo<NodeBean>> getNodeList(@Field("subjectId") String baseSubjectId,
                                                   @Field("levelId") String baseLevelId,
                                                   @Field("gradeId") String baseGradeId,
                                                   @Field("versionId") String baseVersionId,
                                                   @Field("volumeId") String baseVolumeId);

        @POST("/auth/teach/class/teacher/list")
        @FormUrlEncoded
        Observable<StatusVo<SyncClassVo>> getClassList(@Field("subjectId") String subjectId);

        @GET("/auth/teach/class/teacher/list")
        Observable<StatusVo<SyncClassVo>> getAllClassList();

        @POST("/teach/lesson/sync/execute")
        @FormUrlEncoded
        Observable<StatusVo<Object>> execute(@Field("lessonId") String lessonId);

        @POST("/auth/user/teacher/review/list")
        Observable<StatusVo<ReviewVersionBean>> getReviewList();

        @POST("/teach/task/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> delTaskItem(@Field("id") String id);

        @POST("/teach/task/answer/send")
        @FormUrlEncoded
        Observable<StatusVo<Object>> sendAnswer(@Field("id") String id);

        @POST("/teach/task/delete/valid")
        @FormUrlEncoded
        Observable<StatusVo<DelTask>> delTask(@Field("id") String id);

        @POST("/teach/task/toggleHide")
        @FormUrlEncoded
        Observable<StatusVo<Object>> toggleHide(@Field("taskId") Integer taskId, @Field("isHide") Integer isHide);

        @POST("/teach/lesson/lib/delete")
        @FormUrlEncoded
        Observable<StatusVo<DelTask>> delLessonTask(@Field("id") String id);

        @POST("/teach/lesson/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> delLesson(@Field("lessonId") String lessonId);
    }


    public interface HeadUploadService {
        @POST("/auth/user/teacher/head/upload")
        @Multipart
        Observable<StatusVo<Object>> uploadHead(@Part("file\"; filename=\"avatar.jpg") RequestBody file);

    }

    public interface SavePhoneService {
        @POST("/auth/user/teacher/phone/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> SavePhone(@Field("phone") String phone);

        @POST("/auth/phone/code/get")
        @FormUrlEncoded
        Observable<StatusVo<Object>> sendVerityCode(@Field("phone") String phone);
    }

    public interface ChangePasswordService {
        @POST("/auth/account/password/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> ChangePassword(@Field("oldPassword") String oldPassword, @Field("newPassword") String newPassword, @Field("confirmPassword") String confirmPassword);
    }

    public interface ReviewInfoAddService {
        @POST("/auth/user/teacher/review/select/list")
        @FormUrlEncoded
        Observable<StatusVo<ReviewInfoAdd>> ReviewInfoAdd(@Field("levelId") Integer levelId, @Field("educationId") Integer educationId);

        @POST("/auth/user/teacher/review/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> SaveReviewInfoAdd(@Field("reviewsJSON") String reviewsJSON);
    }

    public interface GetReviewInfoService {
        @GET("/auth/user/teacher/review/list")
        Observable<StatusVo<ReviewInfo>> GetReviewInfo();
    }

    public interface GetTextbookInfoService {
        @GET("/auth/user/teacher/version/subject/list")
        Observable<StatusVo<TextbookInfoVo>> GetTextbookInfo();
    }

    public interface GetClassGroupingService {
        @POST("/auth/class/group/student/list")
        @FormUrlEncoded
        Observable<StatusVo<ClassGroupingVo>> GetClassGrouping(@Field("classId") String classId);

        @POST("/auth/class/group/point/new/list")
        @FormUrlEncoded
        Observable<StatusVo<FuFenBean>> GetGroupStudent(@Field("classId") String classId, @Field("subjectId") String subjectId);
    }

    public interface UpdateInfoService {
        @POST("/auth/user/teacher/conf/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> updateInfo(@Field("id") String code, @Field("value") String value);
    }

    public interface GetVersionListService {
        @POST("/customer/school/version/list")
        @FormUrlEncoded
        Observable<StatusVo<VersionVo>> VersionList(@FieldMap Map<String, Object> params);

        @POST("/base/teach/volume/list")
        @FormUrlEncoded
        Observable<StatusVo<VolumeVo>> volumeList(@FieldMap Map<String, Object> params);


        @POST("/auth/user/teacher/version/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> saveData(@FieldMap Map<String, Object> params);

        @POST("/auth/user/teacher/version/info")
        @FormUrlEncoded
        Observable<StatusVo<DeleteVolumeVo>> DeleteVolumeList(@FieldMap Map<String, Object> params);
    }

    public interface LessonVOService {
        @POST("/teach/lesson/all/list")
        @FormUrlEncoded
        Observable<StatusVo<LessonVo>> getLessonVo(@FieldMap Map<String, Object> params);

    }

    public interface TakeOfficeService {
        @GET("/auth/teacher/post/subject/class/list")
        Observable<StatusVo<TakeOfficeVo>> TakeOffice();

    }

    public interface GetTextbookService {
        @GET("/auth/user/teacher/version/list")
        Observable<StatusVo<ReviewInfo>> GetTextbook();
    }

    public interface PrepareLessonsService {
        @POST("/teach/lesson/lib/list")
        @FormUrlEncoded
        Observable<StatusVo<PrepareLessonsVo>> getPrepareLessons(@Field("lessonId") String lessonId);

    }

    public interface ResourceLessonsService {
        @POST("/teach/lesson/resource/list")
        @FormUrlEncoded
        Observable<StatusVo<LessonResource>> getResourceLessons(@Field("lessonId") String lessonId);

    }


    public interface LessonsActivityService {
        @POST("/teach/task/list")
        @FormUrlEncoded
        Observable<StatusVo<SyncTaskVo>> getLessonsActivity(@FieldMap Map<String, Object> params);

    }

    public interface releaseSaveService {
        @POST("/teach/lesson/lib/release/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> releaseSave(@FieldMap Map<String, Object> params);

    }

    public interface resourceReleaseService {
        @POST("/teach/lesson/resource/release")
        @FormUrlEncoded
        Observable<StatusVo<Object>> resourceRelease(@FieldMap Map<String, Object> params);

    }

    public interface DeleteTextbookService {
        @POST("/auth/user/teacher/version/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> DeleteTextbook(@Field("id") String id);

    }

    public interface GetTellSetListService {
        @GET("/auth/user/teacher/conf/list")
        Observable<StatusVo<TellSetVo>> TellSetList();
    }

    public interface GetExamListService {
        @POST("/teach/lesson/lib/info")
        @FormUrlEncoded
        Observable<StatusVo<ExamVo>> GetExamList(@FieldMap Map<String, String> params);

    }
//####################################原生

    /**
     *
     */
    public interface GetStudentListService {
        @POST("/teach/task/class/transcript")
        @FormUrlEncoded
        Observable<StatusVo<StudentList>> GetStudentList(@FieldMap Map<String, String> params);

    }

    public interface GetStudentStatiscService {
        @POST("/teach/task/class/completion")
        @FormUrlEncoded
        Observable<StatusVo<StatisticsVo>> StudentStatisc(@Field("id") String id, @Field("classGroupId") String classGroupId);

    }

    public interface GetStudentDetailsService {
        @POST("/teach/task/class/completion")
        @FormUrlEncoded
        Observable<StatusVo<StatisticsVo>> StudentStatisc(@Field("id") String id);

    }

    public interface GetStudentDetailService {
        @POST("/teach/lesson/answer/exam/list")
        @FormUrlEncoded
        Observable<StatusVo<StudentDetails>> StudentStatisc(@Field("lessonTaskStudentId") String id);

    }

    public interface GetCottectStudentListService {
        //批改的学生列表
        @POST("/teach/lesson/task/student/correct/list")
        @FormUrlEncoded
        Observable<StatusVo<StudentListVo>> StudentStatisc(@Field("lessonTaskId") String id, @Field("classId") String classId);

    }

    public interface GetTeacherAnalysisListService {
        @POST("/analysis/TeacherAnalysisController/showTeacherTaskAnalysisInfo")
        @FormUrlEncoded
        Observable<StatusVo<StringData>> getAnalysis(@Field("teacherExamId") String examVirtualSetResultId);

    }

    public interface GetStudentDetailTwoService {
        //查询学生某个任务的作答结果及老师批语
        @POST("/teach/lesson/answer/exam/list")
        @FormUrlEncoded
        Observable<StatusVo<StudentDetails>> StudentStatisc(@Field("lessonTaskStudentId") String id, @Field("examType") String examType);

    }

    public interface GetAnalyzeListService {
        @POST("/teach/lesson/task/detail/list")
        @FormUrlEncoded
        Observable<StatusVo<AnalyzeVo>> AnalyzeStatisc(@Field("lessonTaskId") String lessonTaskId, @Field("parentId") String parentId, @Field("type") String type);

    }


    public interface GetRgAnalyzeListService {
        @POST("/teach/lesson/task/detail/list")
        @FormUrlEncoded
        Observable<StatusVo<RgAnalyze>> AnalyzeStatisc(@Field("lessonTaskId") String lessonTaskId, @Field("parentId") String parentId, @Field("type") String type);

    }


    public interface GetRgDetailAnalyzeListService {
        @POST("/teach/lesson/task/resource/analyse")
        @FormUrlEncoded
        Observable<StatusVo<RgDetailsVo>> AnalyzeStatisc(@Field("lessonTaskDetailsId") String lessonTaskId);

    }

    public interface GetExplainListService {
        @POST("/teach/task/exam/list")
        @FormUrlEncoded
        Observable<StatusVo<ExplainVo>> AnalyzeStatisc(@Field("lessonTaskId") String lessonTaskId, @Field("type") String type);

    }


    public interface GetExplainExamListService {
        @POST("/teach/lesson/answer/exam/correct/type/list")
        @FormUrlEncoded
        Observable<StatusVo<ExamCorrectVo>> AnalyzeStatisc(@Field("lessonTaskDetailsId") String lessonTaskId);

    }

    public interface GetOverallExamListService {
        @POST("/teach/lesson/task/student/face/list")
        @FormUrlEncoded
        Observable<StatusVo<OverallVo>> AnalyzeStatisc(@Field("lessonTaskId") String lessonTaskId, @Field("correctType") String correctType);

    }

    public interface updateExamTypeService {
        @POST("/teach/lesson/answer/exam/type/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> AnalyzeStatisc(@Field("id") String id, @Field("correctType") String correctType);

    }


    public interface updateStudentExamTypeService {
        @POST("/teach/lesson/task/student/type/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> AnalyzeStatisc(@Field("id") String id, @Field("correctType") String correctType);

    }

    public interface getImgListService {
        @POST("/teach/lesson/answer/exam/img/list")
        @FormUrlEncoded
        Observable<StatusVo<ImgList>> AnalyzeStatisc(@Field("lessonTaskStudentId") String idlessonTaskStudentId);

    }

    public interface getCorrectSaveListService {
        @POST("/teach/lesson/answer/exam/correct/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> AnalyzeStatisc(@FieldMap Map<String, Object> params);

    }


    public interface getexamNumberList {
        @POST("/teach/lesson/task/exam/number/list")
        @FormUrlEncoded
        Observable<StatusVo<AnalyzeNumberVo>> AnalyzeStatisc(@Field("lessonTaskId") String lessonTaskId);

    }


    public interface getexamStudentList {
        @POST("/teach/lesson/task/exam/student/list")
        @FormUrlEncoded
        Observable<StatusVo<AnalyzeStatisticsVo>> AnalyzeStatisc(@Field("lessonTaskId") String lessonTaskId, @Field("lessonTaskDetailsId") String lessonTaskDetailsId);

    }


    /**
     *
     */
    public interface GetGroupStudentListService {
        @POST("/auth/class/group/list")
        @FormUrlEncoded
        Observable<StatusVo<GroupStudent>> GetStudentList(@Field("classId") String classId);

    }

    /**
     *
     */
    public interface GetGroupDetilsListService {
        @POST("/teach/task/group/transcript")
        @FormUrlEncoded
        Observable<StatusVo<GroupStudentDetils>> GetStudentList(@FieldMap Map<String, Object> params);

    }


    /**
     *
     */
    public interface GetExamAnalyseListService {
        @POST("/teach/lesson/task/exam/analyse")
        @FormUrlEncoded
        Observable<StatusVo<ExamAnalyzeVo>> GetStudentList(@Field("lessonTaskDetailsId") String classId);

    }

    /**
     *
     */
    public interface GetExamInfoService {
        @POST("/teach/lesson/answer/exam/info")
        @FormUrlEncoded
        Observable<StatusVo<StudentDetailExam>> GetStudentList(@Field("lessonAnswerExamId") String lessonAnswerExamId);
    }

    public interface GetUserTeacherInfoService {
        @GET("/auth/user/teacher/info")
        Observable<StatusVo<UserTeacherInfo>> getUserInfo();
    }

    public interface QiniuControllerService {
        @POST("teach/QiniuController/getQiniuConfig")
        Observable<StatusVo<QiniuConfig>> getQiniuConfig();
    }

    /*教学统计老师信息*/
    public interface GetTeacherStatisticsService {
        @POST("/auth/teach/class/teacher/list")
        @FormUrlEncoded
        Observable<StatusVo<TeachClassVo>> StudentStatisc(@Field("subjectId") String subjectId);

    }

    /*查询全班学生的统计信息*/
    public interface GetshowTeacherAssessementHistoryService {
        @POST("/analysis/TeacherAnalysisController/showTeacherAssessementHistory")
        @FormUrlEncoded
        Observable<StatusVo<StringData>> StudentStatisc(@Field("classId") String classId, @Field("courseId") String courseId, @Field("type") String type);

    }

    /*查询某一个学生的统计信息*/
    public interface GetshowStudentAssessementHistoryService {
        @POST("/analysis/StudentAnalysisController/showStudentAssessementHistory")
        @FormUrlEncoded
        Observable<StatusVo<StringData>> StudentStatisc(@Field("studentId") String studentId, @Field("courseId") String courseId, @Field("type") String type);

    }


    public interface SaveUserInfoService {

        @POST("auth/AppversionController/saveTeacherVersion")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getUserInfo(@Field("teacherId") String teacherId, @Field("appVersion") String appVersion);
    }

    public interface GroupDiscussionService {
        @POST("/group/insert/teachGroupChatLib")
        @FormUrlEncoded
        Observable<StatusVo<Object>> SaveGroupDiscussion(@FieldMap Map<String, Object> params);

        @POST("/group/list/GroupChatLib")
        @FormUrlEncoded
        Observable<StatusVo<GroupChatBean>> GroupChatLib(@Field("lessonId") String lessonId, @Field("page") String page);

        @POST("/group/list/teachGroupChatTask")
        @FormUrlEncoded
        Observable<StatusVo<GroupListBean>> getGroupChatList(@FieldMap Map<String, Object> params);

        @POST("/group/inTimePublish/teachGroupChatLib")
        @FormUrlEncoded
        Observable<StatusVo<Object>> publishGroupDiscussion(@FieldMap Map<String, Object> params);

        @POST("/group/publish/teachGroupChatLib")
        @FormUrlEncoded
        Observable<StatusVo<Object>> publishGroup(@FieldMap Map<String, Object> params);

        @POST("/group/delete/GroupChat")
        @FormUrlEncoded
        Observable<StatusVo<Object>> deleteGroupChat(@Field("groupChatTaskId") String groupChatTaskId);

        @POST("/group/delete/GroupChatLib")
        @FormUrlEncoded
        Observable<StatusVo<Object>> deleteGroupLib(@Field("groupChatLibId") String groupChatLibId);

        @POST("/group/stop/GroupChat")
        @FormUrlEncoded
        Observable<StatusVo<Object>> stopGroupChat(@Field("groupChatTaskId") String groupChatTaskId);

        @POST("/group/list/result/GroupChatTask")
        @FormUrlEncoded
        Observable<StatusVo<ChatOtherBean>> resultGroupChat(@Field("groupChatTaskId") String groupChatTaskId);

        @POST("/group/set/GroupChatResultDao/score")
        @FormUrlEncoded
        Observable<StatusVo<Object>> setGroupScore(@Field("groupChatResultId") String groupChatResultId, @Field("score") String score);
    }

    public interface SaveDoodleService {
        @POST("/teach/QiniuController/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> publishDoodle(@FieldMap Map<String, Object> params);

    }

    public interface ToolsService {
        @POST("/inclass/class/Behavior")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getClassBehavior(@Field("classId") String classId, @Field("type") String type, @Field("classTime") String classTime);

        @POST("/inclass/class/responder/send")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getClassResponder(@Field("classId") String classId, @Field("questionId") String questionId, @Field("type") String type, @Field("studentId") String studentId, @Field("teacherId") String teacherId);

        @POST("/auth/teach/class/student/list")
        @FormUrlEncoded
        Observable<StatusVo<StudentListTiWen>> getClassStudent(@Field("classId") String classId);

        @POST("/auth/class/group/student/point/add")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getAddFuFen(@FieldMap Map<String, Object> params);


        @POST("/inclass/teacher/status")
        @FormUrlEncoded
        Observable<StatusVo<InClassStatus>> getInClassStatus(@Field("teacherId") String teacherId);
    }

    /*
     * 批改接口
     * */
    public interface CorrectingWayService {
        /*查询所有分组的学生*/
        @POST("/auth/class/group/student/classGroupStudentList")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getCorrectingList(@Field("classId") String classId);

        /*保存批改方式的设置*/
        @POST("/correction/task/correct/user/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> SaveCorrectionMethod(@FieldMap Map<String, Object> params);

        /*按题批改，查询所有题号*/
        @POST("/teach/task/exam/list")
        @FormUrlEncoded
        Observable<StatusVo<StudentTaskExamListVo>> GetStudentTaskExamList(@Field("lessonTaskId") String lessonTaskId, @Field("type") int type, @Field("queryExamType") int queryExamType);


        /*按题批改，查询某道题的所有学生作答结果，并组织老师批语*/
        @POST("/teach/lesson/answer/exam/correct/allStudent")
        @FormUrlEncoded
        Observable<StatusVo<AnswerExamCorrectAllStudentVo>> GetAnswerExamCorrectAllStudent(@Field("lessonTaskDetailsId") int lessonTaskDetailsId);

        /*保存老师评语*/
        @POST("/correction/task/answer/correct/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> SaveComment(@FieldMap Map<String, Object> params);

        /*老师按题批改保存*/
        @POST("/teach/lesson/answer/exam/correctByExam/save")
        @FormUrlEncoded
        Observable<StatusVo<AnswerExamCorrectAllStudentVo>> correctByExamSave(@Field("correctResult") String correctResult);

        //按题批改，查询某道试题所有学生作答图片列表
        @POST("/teach/lesson/answer/exam/correctByExam/img/list")
        @FormUrlEncoded
        Observable<StatusVo<ImgList>> MarkingAnalyzeStatisc(@Field("lessonTaskDetailsId") String lessonTaskDetailsId);
    }
}
