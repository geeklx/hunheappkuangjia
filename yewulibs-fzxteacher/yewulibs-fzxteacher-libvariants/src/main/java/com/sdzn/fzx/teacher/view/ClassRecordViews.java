package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ClassRecordBean;

/**
 * 首页 view
 */
public interface ClassRecordViews extends IView {
    void onClassRecordSuccess(ClassRecordBean teachingCoursesBean);

    void onClassRecordNodata(String msg);

    void onClassRecordFail(String msg);
}
