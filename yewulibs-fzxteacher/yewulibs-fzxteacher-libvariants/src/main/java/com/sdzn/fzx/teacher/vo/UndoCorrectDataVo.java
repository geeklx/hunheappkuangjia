package com.sdzn.fzx.teacher.vo;

/**
 * Created by wangc on 2018/6/26 0026.
 */

public class UndoCorrectDataVo {

    /**
     * data : http://115.28.91.19:9001/exam/2018/06/1b37ebd4737846dbb4da0e656e96c069.png
     */

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
