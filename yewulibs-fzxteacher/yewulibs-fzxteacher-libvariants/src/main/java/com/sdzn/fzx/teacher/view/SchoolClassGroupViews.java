package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.SchoolClassGroupBean;
import com.sdzn.fzx.teacher.bean.SubjectData1;

public interface SchoolClassGroupViews extends IView {

    void getSchoolClassGroupSuccess(SchoolClassGroupBean schoolClassGroupBean);

    void onSchoolClassGroupFailed(String msg);
}
