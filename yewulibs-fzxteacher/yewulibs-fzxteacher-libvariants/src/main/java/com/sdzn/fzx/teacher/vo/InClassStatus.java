package com.sdzn.fzx.teacher.vo;

public class InClassStatus {

    /**
     * data : {"id":2,"classId":531,"schoolId":95,"teacherId":663,"status":0,"classTime":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * classId : 531
         * schoolId : 95
         * teacherId : 663
         * status : 0
         * classTime : null
         */

        private int id;
        private int classId;
        private int schoolId;
        private int teacherId;
        private int status;
        private String classTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public int getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(int schoolId) {
            this.schoolId = schoolId;
        }

        public int getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(int teacherId) {
            this.teacherId = teacherId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getClassTime() {
            return classTime;
        }

        public void setClassTime(String classTime) {
            this.classTime = classTime;
        }
    }
}
