package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class TreeBean {

    /**
     * data : {"id":2002622,"name":"第一单元","parentId":0,"parentName":"","seq":0,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0}
     * children : [{"data":{"id":2002623,"name":"春","parentId":2002622,"parentName":"第一单元","seq":0,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002624,"name":"济南的冬天","parentId":2002622,"parentName":"第一单元","seq":1,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002625,"name":"*雨的四季","parentId":2002622,"parentName":"第一单元","seq":2,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002626,"name":"古代诗歌四首","parentId":2002622,"parentName":"第一单元","seq":3,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002627,"name":"字音字形","parentId":2002622,"parentName":"第一单元","seq":4,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002628,"name":"正确使用词语","parentId":2002622,"parentName":"第一单元","seq":5,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002629,"name":"标点符号","parentId":2002622,"parentName":"第一单元","seq":6,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002630,"name":"辨析病句","parentId":2002622,"parentName":"第一单元","seq":7,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002631,"name":"组句成段","parentId":2002622,"parentName":"第一单元","seq":8,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002632,"name":"语言的综合应用","parentId":2002622,"parentName":"第一单元","seq":9,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002633,"name":"文学文化常识","parentId":2002622,"parentName":"第一单元","seq":10,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002634,"name":"默写","parentId":2002622,"parentName":"第一单元","seq":11,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002635,"name":"记叙文阅读","parentId":2002622,"parentName":"第一单元","seq":12,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002636,"name":"诗歌阅读","parentId":2002622,"parentName":"第一单元","seq":13,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2002637,"name":"作文训练","parentId":2002622,"parentName":"第一单元","seq":14,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[{"data":{"id":2002638,"name":"记叙文写作","parentId":2002637,"parentName":"作文训练","seq":0,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true}],"parent":{},"leaf":false}]
     * parent : {}
     * leaf : false
     */

    private DataBean data;
    private ParentBean parent;
    private boolean leaf;
    private List<ChildrenBean> children;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public ParentBean getParent() {
        return parent;
    }

    public void setParent(ParentBean parent) {
        this.parent = parent;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public List<ChildrenBean> getChildren() {
        return children;
    }

    public void setChildren(List<ChildrenBean> children) {
        this.children = children;
    }

    public static class DataBean {
        /**
         * id : 2002622
         * name : 第一单元
         * parentId : 0
         * parentName :
         * seq : 0
         * bookVolumeId : 556
         * bookVolumeName : 七年级上册
         * bookVersionId : 10228
         * bookVersionName : 人教版（2016）
         * subjectId : 2
         * subjectName : 语文
         * levelId : 2
         * levelName : 初中
         * nodeIdPath :
         * nodeNamePath :
         * isDelete : 0
         */

        private String id;
        private String name;
        private String parentId;
        private String parentName;
        private int seq;
        private int bookVolumeId;
        private String bookVolumeName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getParentName() {
            return parentName;
        }

        public void setParentName(String parentName) {
            this.parentName = parentName;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getBookVolumeId() {
            return bookVolumeId;
        }

        public void setBookVolumeId(int bookVolumeId) {
            this.bookVolumeId = bookVolumeId;
        }

        public String getBookVolumeName() {
            return bookVolumeName;
        }

        public void setBookVolumeName(String bookVolumeName) {
            this.bookVolumeName = bookVolumeName;
        }
    }

    public static class ParentBean {
    }

    public static class ChildrenBean {
        /**
         * data : {"id":2002623,"name":"春","parentId":2002622,"parentName":"第一单元","seq":0,"bookVolumeId":556,"bookVolumeName":"七年级上册","bookVersionId":10228,"bookVersionName":"人教版（2016）","subjectId":2,"subjectName":"语文","levelId":2,"levelName":"初中","nodeIdPath":"","nodeNamePath":"","isDelete":0}
         * children : []
         * parent : {}
         * leaf : true
         */

        private DataBean data;
        private ParentBean parent;
        private boolean leaf;
        private List<ChildrenBean> children;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public ParentBean getParent() {
            return parent;
        }

        public void setParent(ParentBean parent) {
            this.parent = parent;
        }

        public boolean isLeaf() {
            return leaf;
        }

        public void setLeaf(boolean leaf) {
            this.leaf = leaf;
        }

        public List<ChildrenBean> getChildren() {
            return children;
        }

        public void setChildren(List<ChildrenBean> children) {
            this.children = children;
        }

        public static class DataBeanX {
        }

        public static class ParentBeanX {
        }
    }
}
