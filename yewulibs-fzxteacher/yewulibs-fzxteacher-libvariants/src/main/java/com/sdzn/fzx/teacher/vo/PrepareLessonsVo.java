package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/5
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class PrepareLessonsVo {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 8
         * name : 1
         * userCreateId : 6
         * userCreateName : 赵春青
         * timeCreate : 1519277544000
         * timeUpdate : null
         * lessonId : 21
         * lessonName : 我们的民族小学1
         * countExam : 10
         * countResource : 0
         */

        private int id;
        private String name;
        private int userCreateId;
        private String userCreateName;
        private long timeCreate;
        private Object timeUpdate;
        private int lessonId;
        private String lessonName;
        private int countExam;
        private int countResource;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUserCreateId() {
            return userCreateId;
        }

        public void setUserCreateId(int userCreateId) {
            this.userCreateId = userCreateId;
        }

        public String getUserCreateName() {
            return userCreateName;
        }

        public void setUserCreateName(String userCreateName) {
            this.userCreateName = userCreateName;
        }

        public long getTimeCreate() {
            return timeCreate;
        }

        public void setTimeCreate(long timeCreate) {
            this.timeCreate = timeCreate;
        }

        public Object getTimeUpdate() {
            return timeUpdate;
        }

        public void setTimeUpdate(Object timeUpdate) {
            this.timeUpdate = timeUpdate;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public int getCountExam() {
            return countExam;
        }

        public void setCountExam(int countExam) {
            this.countExam = countExam;
        }

        public int getCountResource() {
            return countResource;
        }

        public void setCountResource(int countResource) {
            this.countResource = countResource;
        }
    }
}
