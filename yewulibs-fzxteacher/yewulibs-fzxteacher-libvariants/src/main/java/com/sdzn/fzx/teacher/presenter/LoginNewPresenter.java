package com.sdzn.fzx.teacher.presenter;

import android.app.Activity;
import android.content.Intent;

import com.blankj.utilcode.util.AppUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.view.LoginViews;
import com.sdzn.fzx.teacher.vo.LoginBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class LoginNewPresenter extends Presenter<LoginViews> {
    public void login(final String name, String psw, Activity activity) {
        final String deviceId = AndroidUtil.getDeviceID(App2.get());
//        if ("/auth".equals(BuildConfig3.AUTH)) {
//            psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
//        }
//        psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
        final ProgressDialogManager pdm = new ProgressDialogManager(activity);
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("登录中...");//...
        RetrofitNetNew.build(Api.class, getIdentifier())
                .login(BuildConfig3.SERVER_ISERVICE_NEW2 + "/teacher/login/token", name, psw)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<LoginBean.DataBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<LoginBean.DataBean>> call, Response<ResponseSlbBean1<LoginBean.DataBean>> response) {
                        pdm.cancelWaiteDialog();
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().loginFailed(response.body().getMessage());
                            return;
                        }
                        getView().loginSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<LoginBean.DataBean>> call, Throwable t) {
                        pdm.cancelWaiteDialog();
                        if (!hasView()) {
                            return;
                        }
                        getView().loginFailed(t.getMessage());
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


//    public void login(final String name, String psw) {
//        if (!vertifyNum(name, psw)) {
//            return;
//        }
//        final String deviceId = AndroidUtil.getDeviceID( App2.get());
////        CrashReport.putUserData( App2.get(), "studentName", name);
////        CrashReport.putUserData( App2.get(), "studentPwd", psw);
//        if ("/auth".equals(BuildConfig3.AUTH)) {
//            psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
//        }
//        Network.createService(NetWorkService.LoginService.class)
//                .login(name, psw, deviceId, "Android")
//                .map(new StatusFunc<LoginBean>())
////                .subscribeOn(Schedulers.io())
////                .observeOn(AndroidSchedulers.mainThread())
////                .subscribe(new ProgressSubscriber<LoginBean>(new SubscriberListener<LoginBean>() {
////                    @Override
////                    public void onNext(LoginBean o) {
////
////                        mView.loginSuccess(o);
////                    }
////
////                    @Override
////                    public void onError(Throwable e) {
////
////                        if (e instanceof ApiException) {
////                            ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
////                            return;
////                        }
////                        ToastUtil.showShortlToast("登录失败");
////
////                    }
////
////                    @Override
////                    public void onCompleted() {
////
////                    }
////                }, mActivity));
//
//
//    }

    public void forgetPsw() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ForgetPswActivity");
        startActivity(intent);
    }


    public boolean vertifyNum(final String userName, final String psw) {
        if (userName.isEmpty()) {
            ToastUtil.showShortlToast("请输入账号");
        } else if (psw.isEmpty()) {
            ToastUtil.showShortlToast("请输入密码");
        }
        if (userName.length() == 8 || StringUtils.isMobile(userName)) {
            if (StringUtils.vertifyPsw(psw)) {
                return true;
            } else {
                ToastUtil.showShortlToast("账号或密码错误");
            }
        } else {
            ToastUtil.showShortlToast("账号或密码错误");
        }
        return false;

    }

    /**
     * 启动mqtt
     */
//    public void startMqttService() {
//        Intent intent = new Intent(mActivity, MqttService.class);
//        mActivity.startService(intent);
//
//    }

//    public void getInClassStatus(){
//        Network.createTokenService(NetWorkService.ToolsService.class)
//                .getInClassStatus(String.valueOf(SPUtils.getLoginBean().getData().getUser().getId()))
//                .map(new StatusFunc<InClassStatus>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<InClassStatus>() {
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            ApiException apiException = (ApiException) e;
//                            StatusVo status = apiException.getStatus();
//                            if (status != null && status.getMsg() != null) {
//                                ToastUtil.showLonglToast(status.getMsg());
//                            } else {
//
//                            }
//                        } else {
//
//                        }
//                    }
//
//                    @Override
//                    public void onNext(InClassStatus o) {
//                        getView().inClassStatusResult(o);
//                    }
//                });
//
//    }
}
