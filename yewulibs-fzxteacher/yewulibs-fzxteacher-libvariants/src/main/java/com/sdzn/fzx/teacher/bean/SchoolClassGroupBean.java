package com.sdzn.fzx.teacher.bean;


import java.io.Serializable;
import java.util.List;

public class SchoolClassGroupBean implements Serializable {

    /**
     * allStudent : 10
     * notGrouped : 0
     * groups : [{"groupId":"67","groupName":"尖子组","studentCount":6},{"groupId":"68","groupName":"冲刺组","studentCount":4}]
     */

    private int allStudent;
    private int notGrouped;
    private List<GroupsBean> groups;

    public int getAllStudent() {
        return allStudent;
    }

    public void setAllStudent(int allStudent) {
        this.allStudent = allStudent;
    }

    public int getNotGrouped() {
        return notGrouped;
    }

    public void setNotGrouped(int notGrouped) {
        this.notGrouped = notGrouped;
    }

    public List<GroupsBean> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupsBean> groups) {
        this.groups = groups;
    }

    public static class GroupsBean implements Serializable {
        /**
         * groupId : 67
         * groupName : 尖子组
         * studentCount : 6
         * isSelected
         */

        private String groupId;
        private String groupName;
        private int studentCount;
        private boolean isSelected = false;

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
        public boolean isSelected() {
            return isSelected;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getStudentCount() {
            return studentCount;
        }

        public void setStudentCount(int studentCount) {
            this.studentCount = studentCount;
        }
    }
}
