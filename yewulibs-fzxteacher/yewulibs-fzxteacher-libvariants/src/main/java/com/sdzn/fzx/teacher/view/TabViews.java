package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.RecyclerTabBean;

/**
 * 首页 view
 */
public interface TabViews extends IView {
    void onTabSuccess(RecyclerTabBean recyclerTabBean);

    void onTabNodata(String msg);

    void onTabFail(String msg);

}
