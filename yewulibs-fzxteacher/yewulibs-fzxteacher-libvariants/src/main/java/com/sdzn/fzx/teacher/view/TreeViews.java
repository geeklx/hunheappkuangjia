package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.TreeBean;

import java.util.List;

/**
 * 首页 view
 */
public interface TreeViews extends IView {
    void onTreeSuccess(List<TreeBean> treeBeanList);

    void onTreeNodata(String msg);

    void onTreeFail(String msg);
}
