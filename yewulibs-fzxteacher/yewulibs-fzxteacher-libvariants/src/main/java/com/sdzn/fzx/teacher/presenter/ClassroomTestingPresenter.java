package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.ClassroomTestingBean;
import com.sdzn.fzx.teacher.view.ClassroomTestingViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class ClassroomTestingPresenter extends Presenter<ClassroomTestingViews> {

    public void queryClassroomTesting(String token, String chapterId,String classid,String keyWord,String status,String startTime,String endTime, int page, int limit) {
        JSONObject requestData = new JSONObject();
        requestData.put("chapterId", chapterId);
        requestData.put("classId", classid);
        requestData.put("keyWord", keyWord);
        requestData.put("status", status);
        requestData.put("startTime", startTime);
        requestData.put("endTime", endTime);
        requestData.put("page", page);
        requestData.put("limit", limit);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryClassroomTesting(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/ClassRoomTesting/list", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<ClassroomTestingBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ClassroomTestingBean>> call, Response<ResponseSlbBean1<ClassroomTestingBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onClassroomTestingNodata(response.body().getMessage());
                            return;
                        }
                        getView().onClassroomTestingSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ClassroomTestingBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onClassroomTestingFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
