package com.sdzn.fzx.teacher.bean;

import java.io.Serializable;
import java.util.List;

public class GrzxRecActBean1 implements Serializable {
    /**
     * list : [{"id":1,"picture":"https://file.fuzhuxian.com/grzx_1.png","seq":1,"type":0,"act":"app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdkcActivity","name":"我的课程"},{"id":2,"picture":"https://file.fuzhuxian.com/grzx_2.png","seq":2,"type":0,"act":"app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.KtjxShouyeActivity","name":"课堂教学"},{"id":3,"picture":"https://file.fuzhuxian.com/grzx_3.png","seq":3,"type":0,"act":"app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.ZzxxShouyeActivity","name":"自主学习"},{"id":4,"picture":"https://file.fuzhuxian.com/grzx_4.png","seq":4,"type":0,"act":"app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.ZyJiLuActivity","name":"作业"}]
     * title : 课堂流程
     */

    private String title;
    private List<GrzxRecActBean2> list;

    public GrzxRecActBean1() {
    }

    public GrzxRecActBean1(String title, List<GrzxRecActBean2> courseList) {
        this.title = title;
        this.list = courseList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<GrzxRecActBean2> getList() {
        return list;
    }

    public void setList(List<GrzxRecActBean2> list) {
        this.list = list;
    }
}
