package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.blankj.utilcode.util.LogUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.view.ResourcesViews;
import com.sdzn.fzx.teacher.view.TeachingCoursesContentViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResourcesPresenter extends Presenter<ResourcesViews> {

    public void AddResources(String token,String classIds,String lessonLibCourseId,String lessonTaskId,String answerType,String startTime,String endTime) {
        JSONObject requestData = new JSONObject();
        requestData.put("classIds", classIds);
        requestData.put("lessonLibCourseId", lessonLibCourseId);
        requestData.put("lessonTaskId", lessonTaskId);
        requestData.put("answerType", answerType);
        requestData.put("startTime", startTime);
        requestData.put("endTime", endTime);

        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        LogUtils.e("AddResources1111111111111111"+requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .AddResources(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/course/class/room/publish", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
//                        if (response.body().getCode() != 0) {
//                            getView().onResourcesFail(response.body().getMessage());
//                            return;
//                        }
                        Log.e("eeee","--"+ response.body().getResult());
                        getView().onResourcesSuccess(response.body().getResult().toString());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onResourcesFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

}
