package com.sdzn.fzx.teacher.vo.me;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ClassGroupingVoListVo {

    private String classGroupName;
    private String points;

    private List<ClassGroupingVo.DataBean> list;

    public ClassGroupingVoListVo() {

        this.list = new ArrayList<>();
    }

    public ClassGroupingVoListVo(String classGroupName,String points, List<ClassGroupingVo.DataBean> list) {
        this.classGroupName = classGroupName;
        this.list = list;
        this.points = points;
    }


    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getClassGroupName() {
        return classGroupName;
    }

    public void setClassGroupName(String classGroupName) {
        this.classGroupName = classGroupName;
    }

    public List<ClassGroupingVo.DataBean> getList() {
        return list;
    }

    public void setList(List<ClassGroupingVo.DataBean> list) {
        this.list = list;
    }
}
