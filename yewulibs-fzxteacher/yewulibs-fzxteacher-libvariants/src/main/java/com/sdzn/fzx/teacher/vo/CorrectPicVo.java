package com.sdzn.fzx.teacher.vo;

/**
 * @author Reisen at 2018-11-01
 */
public class CorrectPicVo {
    private CorrectPicPath data;

    public CorrectPicPath getData() {
        return data;
    }

    public void setData(CorrectPicPath data) {
        this.data = data;
    }

    public static class CorrectPicPath {
        private String realPath;
        private String smallerImgPath;

        public String getSmallerImgPath() {
            return smallerImgPath;
        }

        public void setSmallerImgPath(String smallerImgPath) {
            this.smallerImgPath = smallerImgPath;
        }

        public String getRealPath() {
            return realPath;
        }

        public void setRealPath(String realPath) {
            this.realPath = realPath;
        }
    }
}
