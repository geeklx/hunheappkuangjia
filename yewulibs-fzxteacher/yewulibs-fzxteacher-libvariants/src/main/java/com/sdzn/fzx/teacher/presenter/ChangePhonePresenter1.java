package com.sdzn.fzx.teacher.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.view.ChangePhoneViews;

import java.io.InputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class ChangePhonePresenter1 extends Presenter<ChangePhoneViews> {

    private String tokenStr;


    public void QueryImgToken() {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImgToken(BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/valicode/imgToken")
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onImgTokenFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnImgTokenSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onImgTokenFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    public void QueryImg(String imgToken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImg(BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/valicode/img", imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        InputStream io = response.body().byteStream();
                        getView().OnImgSuccess(io);
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onImgFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    public void sendVerityCode(String tel, String valicode, String imgToken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .sendYZM(BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/login/sendVerifyCode", tel, valicode, imgToken)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnVerityCodeFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnVerityCodeSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnVerityCodeFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    public void checkVerityNum(String token, final String phoneNum, final String code) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .updatePhone(BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/update/mobile", token, phoneNum, code)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onChangPhoneFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnChangPhoneSuccess(response.body().getMessage());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onChangPhoneFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
