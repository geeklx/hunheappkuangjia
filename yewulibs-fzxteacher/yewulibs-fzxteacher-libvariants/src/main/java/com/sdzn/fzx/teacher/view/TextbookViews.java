package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;

/**
 *
 */

public interface TextbookViews extends IView {
    void setTextbookInfoVo(TextbookInfoVo textbookInfoVo);
}
