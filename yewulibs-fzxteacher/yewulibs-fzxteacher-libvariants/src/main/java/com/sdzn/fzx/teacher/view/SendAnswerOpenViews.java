package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;

/**
 * 首页 view
 */
public interface SendAnswerOpenViews extends IView {
    void onSendAnswerOpenSuccess(Object object);

    void onSendAnswerOpenNodata(String msg);

    void onSendAnswerOpenFail(String msg);
}
