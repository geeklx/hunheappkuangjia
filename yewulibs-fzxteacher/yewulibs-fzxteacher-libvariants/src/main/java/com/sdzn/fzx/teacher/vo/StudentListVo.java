package com.sdzn.fzx.teacher.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class StudentListVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 56304
         * lessonTaskId : 1515
         * baseSubjectId : 1
         * baseSubjectName : 语文
         * status : 2
         * userStudentId : 5166
         * userStudentName : 学生62
         * userTeacherId : 233
         * userTeacherName : 王银勇
         * answerTime : 1545374223000
         * submitTime : 1545374239000
         * useTime : 16
         * scoreObjective : -1
         * scoreSubjective : -1
         * scoreTotal : -1
         * countExam : null
         * countResource : null
         * countRight : 0
         * countError : 0
         * countHalf : 0
         * classGroupId : 0
         * classGroupName : 未分组
         * isCorrect : 0
         * correctTime : null
         * correctType : 3
         * scope : 2018
         * isRead : 1
         * photo : null
         */

        private int id;
        private int lessonTaskId;
        private int baseSubjectId;
        private String baseSubjectName;
        private int status;
        private int userStudentId;
        private String userStudentName;
        private int userTeacherId;
        private String userTeacherName;
        private long answerTime;
        private long submitTime;
        private int useTime;
        private String scoreObjective;
        private String scoreSubjective;
        private String scoreTotal;
        private Object countExam;
        private Object countResource;
        private int countRight;
        private int countError;
        private int countHalf;
        private int classGroupId;
        private String classGroupName;
        private int isCorrect;
        private Object correctTime;
        private int correctType;
        private int scope;
        private int isRead;
        private Object photo;
        private String ryExamResultId;

        public String getScoreSubjective() {
            return scoreSubjective;
        }

        public void setScoreSubjective(String scoreSubjective) {
            this.scoreSubjective = scoreSubjective;
        }

        public String getRyExamResultId() {
            return ryExamResultId;
        }

        public void setRyExamResultId(String ryExamResultId) {
            this.ryExamResultId = ryExamResultId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public int getUserTeacherId() {
            return userTeacherId;
        }

        public void setUserTeacherId(int userTeacherId) {
            this.userTeacherId = userTeacherId;
        }

        public String getUserTeacherName() {
            return userTeacherName;
        }

        public void setUserTeacherName(String userTeacherName) {
            this.userTeacherName = userTeacherName;
        }

        public long getAnswerTime() {
            return answerTime;
        }

        public void setAnswerTime(long answerTime) {
            this.answerTime = answerTime;
        }

        public long getSubmitTime() {
            return submitTime;
        }

        public void setSubmitTime(long submitTime) {
            this.submitTime = submitTime;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public String getScoreObjective() {
            return new BigDecimal(scoreObjective).stripTrailingZeros().toPlainString();
        }

        public void setScoreObjective(String scoreObjective) {
            this.scoreObjective = scoreObjective;
        }

        public String getScoreTotal() {
            return new BigDecimal(scoreTotal).stripTrailingZeros().toPlainString();
        }

        public void setScoreTotal(String scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public Object getCountExam() {
            return countExam;
        }

        public void setCountExam(Object countExam) {
            this.countExam = countExam;
        }

        public Object getCountResource() {
            return countResource;
        }

        public void setCountResource(Object countResource) {
            this.countResource = countResource;
        }

        public int getCountRight() {
            return countRight;
        }

        public void setCountRight(int countRight) {
            this.countRight = countRight;
        }

        public int getCountError() {
            return countError;
        }

        public void setCountError(int countError) {
            this.countError = countError;
        }

        public int getCountHalf() {
            return countHalf;
        }

        public void setCountHalf(int countHalf) {
            this.countHalf = countHalf;
        }

        public int getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(int classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public Object getCorrectTime() {
            return correctTime;
        }

        public void setCorrectTime(Object correctTime) {
            this.correctTime = correctTime;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public Object getPhoto() {
            return photo;
        }

        public void setPhoto(Object photo) {
            this.photo = photo;
        }
    }
}
