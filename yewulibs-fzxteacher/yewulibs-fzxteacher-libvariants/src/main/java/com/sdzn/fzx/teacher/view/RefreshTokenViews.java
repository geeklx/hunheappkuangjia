package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.RefreshTokenRecActBean;

/**
 * 首页 view
 */
public interface RefreshTokenViews extends IView {
    void onRefreshTokenSuccess(RefreshTokenRecActBean refreshTokenRecActBean);

    void onRefreshTokenNodata(String msg);

    void onRefreshTokenFail(String msg);
}
