package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.blankj.utilcode.util.SPUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.view.ShouyeViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class ShouyePresenter extends Presenter<ShouyeViews> {
    public void queryShouye(String token, String subjectid) {
        JSONObject requestData = new JSONObject();
        requestData.put("subjectId", subjectid);
//        MyLogUtil.e("ssssssssssssss",BuildConfig3.SERVER_ISERVICE_NEW2+"");
//        MyLogUtil.e("ssssssssssssss", SPUtils.getInstance().getString("版本地址2", "https://www.baidu.com")+"");
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryShouye(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/home/data", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<ShouyeBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ShouyeBean>> call, Response<ResponseSlbBean1<ShouyeBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onIndexNodata(response.body().getCode(),response.body().getMessage());
                            return;
                        }
                        getView().onIndexSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ShouyeBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onIndexFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
