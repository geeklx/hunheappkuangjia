package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;

/**
 * 首页 view
 */
public interface TeacherQuestionViews extends IView {
    void onTeacherQuestionSuccess(Object object);

    void onTeacherQuestionNodata(String msg);

    void onTeacherQuestionFail(String msg);
}
