package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.view.NowOutViews;
import com.sdzn.fzx.teacher.view.SendIsHideViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -老师撤回
 */
public class SendIsHidePresenter extends Presenter<SendIsHideViews> {

    public void addSendIsHide(String token, String lessonTaskCourseId,Boolean isHide) {
        JSONObject requestData = new JSONObject();
        requestData.put("lessonTaskCourseId", lessonTaskCourseId);
        requestData.put("isHide", isHide);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .addSendIsHide(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/ClassRoomTesting/sendIsHide", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onSendIsHideNodata(response.body().getMessage());
                            return;
                        }
                        getView().onSendIsHideSuccess(response.body().getMessage());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onSendIsHideFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
