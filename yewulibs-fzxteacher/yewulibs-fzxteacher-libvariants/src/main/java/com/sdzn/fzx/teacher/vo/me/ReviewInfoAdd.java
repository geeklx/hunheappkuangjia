package com.sdzn.fzx.teacher.vo.me;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ReviewInfoAdd {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * reviewList : [{"id":null,"createAccountId":null,"createUserName":null,"createTime":null,"userTeacherId":null,"userTeacherName":null,"baseGradeId":6,"baseGradeName":"六年级","reviewType":1,"name":null,"reviewTypeName":"上学期期中复习","status":false},{"id":null,"createAccountId":null,"createUserName":null,"createTime":null,"userTeacherId":null,"userTeacherName":null,"baseGradeId":6,"baseGradeName":"六年级","reviewType":2,"name":null,"reviewTypeName":"上学期期末复习","status":false},{"id":null,"createAccountId":null,"createUserName":null,"createTime":null,"userTeacherId":null,"userTeacherName":null,"baseGradeId":6,"baseGradeName":"六年级","reviewType":3,"name":null,"reviewTypeName":"下学期期中复习","status":false},{"id":null,"createAccountId":null,"createUserName":null,"createTime":null,"userTeacherId":null,"userTeacherName":null,"baseGradeId":6,"baseGradeName":"六年级","reviewType":4,"name":null,"reviewTypeName":"下学期期末复习","status":false}]
         * baseGradeId : 6
         * baseGradeName : 六年级
         */

        private int baseGradeId;
        private String baseGradeName;
        private List<ReviewListBean> reviewList;

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public List<ReviewListBean> getReviewList() {
            return reviewList;
        }

        public void setReviewList(List<ReviewListBean> reviewList) {
            this.reviewList = reviewList;
        }

        public static class ReviewListBean {
            /**
             * id : null
             * createAccountId : null
             * createUserName : null
             * createTime : null
             * userTeacherId : null
             * userTeacherName : null
             * baseGradeId : 6
             * baseGradeName : 六年级
             * reviewType : 1
             * name : null
             * reviewTypeName : 上学期期中复习
             * status : false
             */

            private Object id;
            private Object createAccountId;
            private Object createUserName;
            private Object createTime;
            private Object userTeacherId;
            private Object userTeacherName;
            private int baseGradeId;
            private String baseGradeName;
            private int reviewType;
            private Object name;
            private String reviewTypeName;
            private boolean status;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public Object getCreateAccountId() {
                return createAccountId;
            }

            public void setCreateAccountId(Object createAccountId) {
                this.createAccountId = createAccountId;
            }

            public Object getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(Object createUserName) {
                this.createUserName = createUserName;
            }

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public Object getUserTeacherId() {
                return userTeacherId;
            }

            public void setUserTeacherId(Object userTeacherId) {
                this.userTeacherId = userTeacherId;
            }

            public Object getUserTeacherName() {
                return userTeacherName;
            }

            public void setUserTeacherName(Object userTeacherName) {
                this.userTeacherName = userTeacherName;
            }

            public int getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(int baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public String getBaseGradeName() {
                return baseGradeName;
            }

            public void setBaseGradeName(String baseGradeName) {
                this.baseGradeName = baseGradeName;
            }

            public int getReviewType() {
                return reviewType;
            }

            public void setReviewType(int reviewType) {
                this.reviewType = reviewType;
            }

            public Object getName() {
                return name;
            }

            public void setName(Object name) {
                this.name = name;
            }

            public String getReviewTypeName() {
                return reviewTypeName;
            }

            public void setReviewTypeName(String reviewTypeName) {
                this.reviewTypeName = reviewTypeName;
            }

            public boolean isStatus() {
                return status;
            }

            public void setStatus(boolean status) {
                this.status = status;
            }
        }
    }
}
