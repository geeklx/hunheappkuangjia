package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.StartStudyBean;

/**
 * 首页 view
 */
public interface ExploreReleaseViews extends IView {
    void onSExploreReleaseSuccess(String s);
    void onExploreReleaseFail(String msg);
}
