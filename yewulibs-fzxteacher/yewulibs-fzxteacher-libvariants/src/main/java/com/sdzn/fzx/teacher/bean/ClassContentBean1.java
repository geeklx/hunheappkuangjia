package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class ClassContentBean1 {

    /**
     * result : {"sourseList":[{"id":1642,"name":"执迷不悟.mp3","type":0,"resourceType":0,"resourceText":"","resourceUrl":"http://file.znclass.com/1607395253050执迷不悟.mp3","title":"","content":"","chatContentPicList":[],"libCourseDetailList":[]},{"id":1643,"name":"39 4.5.3函数模型的应用.pptx","type":0,"resourceType":0,"resourceText":"","resourceUrl":"http://file.znclass.com/160739525306239 4.5.3函数模型的应用.pptx","title":"","content":"","chatContentPicList":[],"libCourseDetailList":[]},{"id":1644,"name":"小视频.mp4","type":0,"resourceType":0,"resourceText":"","resourceUrl":"http://file.znclass.com/1607395253058小视频.mp4","title":"","content":"","chatContentPicList":[],"libCourseDetailList":[]}],"testIngList":[{"id":1641,"name":"课堂检测","type":3,"libCourseDetailList":[{"id":1243,"examId":"5fcf1c1ab1729e24b747d42b","lessonId":456,"lessonLibId":909,"lessonLibCourseId":1641,"examTemplateId":4,"examTemplateStyleId":"5","examTemplateStyleName":"语言表达","isSubjective":0,"status":1,"score":2,"examSource":"","seq":0,"examText":"{\"abilityStructures\":[5272],\"analyticMethods\":[],\"baseLevelId\":1,\"baseLevelName\":\"初中\",\"baseSubjectId\":2,\"baseSubjectName\":\"语文\",\"chapterIds\":[2002622,2002623,2017415,2017416],\"createTime\":1604942475482,\"difficulty\":3,\"downCount\":0,\"examAnalysis\":\"紧扣题目作答，注意字数，合理即可。\",\"examAnswer\":\"【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。\",\"examOptions\":[],\"examStem\":\"朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）\",\"examTypeId\":4,\"favTime\":0,\"flagOpen\":0,\"flagShare\":0,\"id\":\"5fcf1c1ab1729e24b747d42b\",\"knowledgePointIds\":[15299],\"optionNumber\":0,\"partnerExamId\":\"76046143\",\"sourceId\":1,\"sourceName\":\"自有题库\",\"templateStyleId\":5,\"templateStyleName\":\"语言表达\",\"viewCount\":0}","rootExamId":"","exam":{"id":"5fcf1c1ab1729e24b747d42b","examTypeId":4,"examStem":"朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）","examExplain":"","examAnalysis":"紧扣题目作答，注意字数，合理即可。","examAnswer":"【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。","examOptions":[],"optionNumber":0,"customerSchoolId":0,"customerSchoolName":"","baseLevelId":1,"baseLevelName":"初中","baseEducationId":0,"baseEducationName":"","baseSubjectId":2,"baseSubjectName":"语文","baseVersionId":0,"baseVersionName":"","baseGradeId":0,"baseGradeName":"","baseVolumeId":0,"baseVolumeName":"","chapterNodeIdPath":"","chapterNodeNamePath":"","createUserId":0,"createUserName":"","createTime":"1604942475482","updateTime":0,"hots":0,"difficulty":3,"templateStyleId":5,"templateStyleName":"语言表达","flagShare":0,"flagOpen":0,"sourceId":1,"sourceName":"自有题库","zoneIdPath":"","zoneNamePath":"","zoneId":0,"zoneName":"","partnerExamId":"76046143","downCount":0,"viewCount":0,"favTime":0,"children":[],"status":0,"bloomTheoryId":0,"analyticMethods":[],"abilityStructures":[5272],"subjectQualityIds":[],"applicationScenarioIds":[],"chapterIds":[2002622,2002623,2017415,2017416],"knowledgePointIds":[15299],"rejection":"","rejectionState":0,"belongDate":"","importantLevel":0,"labelBeans":[{"val":"语言表达","desc":"","id":{},"name":"","dataList":[]},{"val":"中等","desc":"","id":{},"name":"","dataList":[]},{"val":"2020-11-10","desc":"上传时间","id":{},"name":"","dataList":[]},{"val":"0","desc":"使用次数","id":{},"name":"","dataList":[]},{"val":"表达应用","desc":"学科能力","id":{},"name":"","dataList":[]}],"knowledgePointNames":[],"chapterNames":[],"isCollect":0,"rootExamId":""}},{"id":1244,"examId":"5fcf1c1cb1729e24b747d42c","lessonId":456,"lessonLibId":909,"lessonLibCourseId":1641,"examTemplateId":4,"examTemplateStyleId":"5","examTemplateStyleName":"语言表达","isSubjective":0,"status":1,"score":2,"examSource":"","seq":1,"examText":"{\"abilityStructures\":[5272],\"analyticMethods\":[],\"baseLevelId\":1,\"baseLevelName\":\"初中\",\"baseSubjectId\":2,\"baseSubjectName\":\"语文\",\"chapterIds\":[2002622,2002623,2017415,2017416],\"createTime\":1604942471933,\"difficulty\":2,\"downCount\":0,\"examAnalysis\":\"按要求作答。\",\"examAnswer\":\"<span rt='ryquestiongroup'>小题1<\/span>:(1)春天是绿的(2)绿的小草(3)绿的嫩芽(4)绿的心愿\",\"examOptions\":[],\"examStem\":\"请仿照画线的句子的句式，再写几句话，给春天绘色。<br/><span rt='ryquestiongroup'>小题1<\/span>:<u>春天是红的，红的春联，红的桃花，红的脸颊<\/u>；<span data-ph=\\\"1\\\">____<\/span>，<span data-ph=\\\"2\\\">____<\/span>，<span data-ph=\\\"3\\\">____<\/span>，<span data-ph=\\\"4\\\">____<\/span>；春天是蓝的，蓝的天空，蓝的大海，蓝的梦想。\",\"examTypeId\":4,\"favTime\":0,\"flagOpen\":0,\"flagShare\":0,\"id\":\"5fcf1c1cb1729e24b747d42c\",\"knowledgePointIds\":[15299],\"optionNumber\":0,\"partnerExamId\":\"75348977\",\"sourceId\":1,\"sourceName\":\"自有题库\",\"templateStyleId\":5,\"templateStyleName\":\"语言表达\",\"viewCount\":0}","rootExamId":"","exam":{"id":"5fcf1c1cb1729e24b747d42c","examTypeId":4,"examStem":"请仿照画线的句子的句式，再写几句话，给春天绘色。<br/><span rt='ryquestiongroup'>小题1<\/span>:<u>春天是红的，红的春联，红的桃花，红的脸颊<\/u>；<span data-ph=\"1\">____<\/span>，<span data-ph=\"2\">____<\/span>，<span data-ph=\"3\">____<\/span>，<span data-ph=\"4\">____<\/span>；春天是蓝的，蓝的天空，蓝的大海，蓝的梦想。","examExplain":"","examAnalysis":"按要求作答。","examAnswer":"<span rt='ryquestiongroup'>小题1<\/span>:(1)春天是绿的(2)绿的小草(3)绿的嫩芽(4)绿的心愿","examOptions":[],"optionNumber":0,"customerSchoolId":0,"customerSchoolName":"","baseLevelId":1,"baseLevelName":"初中","baseEducationId":0,"baseEducationName":"","baseSubjectId":2,"baseSubjectName":"语文","baseVersionId":0,"baseVersionName":"","baseGradeId":0,"baseGradeName":"","baseVolumeId":0,"baseVolumeName":"","chapterNodeIdPath":"","chapterNodeNamePath":"","createUserId":0,"createUserName":"","createTime":"1604942471933","updateTime":0,"hots":0,"difficulty":2,"templateStyleId":5,"templateStyleName":"语言表达","flagShare":0,"flagOpen":0,"sourceId":1,"sourceName":"自有题库","zoneIdPath":"","zoneNamePath":"","zoneId":0,"zoneName":"","partnerExamId":"75348977","downCount":0,"viewCount":0,"favTime":0,"children":[],"status":0,"bloomTheoryId":0,"analyticMethods":[],"abilityStructures":[5272],"subjectQualityIds":[],"applicationScenarioIds":[],"chapterIds":[2002622,2002623,2017415,2017416],"knowledgePointIds":[15299],"rejection":"","rejectionState":0,"belongDate":"","importantLevel":0,"labelBeans":[{"val":"语言表达","desc":"","id":{},"name":"","dataList":[]},{"val":"较易","desc":"","id":{},"name":"","dataList":[]},{"val":"2020-11-10","desc":"上传时间","id":{},"name":"","dataList":[]},{"val":"0","desc":"使用次数","id":{},"name":"","dataList":[]},{"val":"表达应用","desc":"学科能力","id":{},"name":"","dataList":[]}],"knowledgePointNames":[],"chapterNames":[],"isCollect":0,"rootExamId":""}}]}]}
     */

    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<SourseListBean> sourseList;
        private List<TestIngListBean> testIngList;

        public List<SourseListBean> getSourseList() {
            return sourseList;
        }

        public void setSourseList(List<SourseListBean> sourseList) {
            this.sourseList = sourseList;
        }

        public List<TestIngListBean> getTestIngList() {
            return testIngList;
        }

        public void setTestIngList(List<TestIngListBean> testIngList) {
            this.testIngList = testIngList;
        }

        public static class SourseListBean {
            /**
             * id : 1642
             * name : 执迷不悟.mp3
             * type : 0
             * resourceType : 0
             * resourceText :
             * resourceUrl : http://file.znclass.com/1607395253050执迷不悟.mp3
             * title :
             * content :
             * chatContentPicList : []
             * libCourseDetailList : []
             */

            private int id;
            private String name;
            private int type;
            private int resourceType;
            private String resourceText;
            private String resourceUrl;
            private String title;
            private String content;
            private List<?> chatContentPicList;
            private List<?> libCourseDetailList;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getResourceType() {
                return resourceType;
            }

            public void setResourceType(int resourceType) {
                this.resourceType = resourceType;
            }

            public String getResourceText() {
                return resourceText;
            }

            public void setResourceText(String resourceText) {
                this.resourceText = resourceText;
            }

            public String getResourceUrl() {
                return resourceUrl;
            }

            public void setResourceUrl(String resourceUrl) {
                this.resourceUrl = resourceUrl;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public List<?> getChatContentPicList() {
                return chatContentPicList;
            }

            public void setChatContentPicList(List<?> chatContentPicList) {
                this.chatContentPicList = chatContentPicList;
            }

            public List<?> getLibCourseDetailList() {
                return libCourseDetailList;
            }

            public void setLibCourseDetailList(List<?> libCourseDetailList) {
                this.libCourseDetailList = libCourseDetailList;
            }
        }

        public static class TestIngListBean {
            /**
             * id : 1641
             * name : 课堂检测
             * type : 3
             * libCourseDetailList : [{"id":1243,"examId":"5fcf1c1ab1729e24b747d42b","lessonId":456,"lessonLibId":909,"lessonLibCourseId":1641,"examTemplateId":4,"examTemplateStyleId":"5","examTemplateStyleName":"语言表达","isSubjective":0,"status":1,"score":2,"examSource":"","seq":0,"examText":"{\"abilityStructures\":[5272],\"analyticMethods\":[],\"baseLevelId\":1,\"baseLevelName\":\"初中\",\"baseSubjectId\":2,\"baseSubjectName\":\"语文\",\"chapterIds\":[2002622,2002623,2017415,2017416],\"createTime\":1604942475482,\"difficulty\":3,\"downCount\":0,\"examAnalysis\":\"紧扣题目作答，注意字数，合理即可。\",\"examAnswer\":\"【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。\",\"examOptions\":[],\"examStem\":\"朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）\",\"examTypeId\":4,\"favTime\":0,\"flagOpen\":0,\"flagShare\":0,\"id\":\"5fcf1c1ab1729e24b747d42b\",\"knowledgePointIds\":[15299],\"optionNumber\":0,\"partnerExamId\":\"76046143\",\"sourceId\":1,\"sourceName\":\"自有题库\",\"templateStyleId\":5,\"templateStyleName\":\"语言表达\",\"viewCount\":0}","rootExamId":"","exam":{"id":"5fcf1c1ab1729e24b747d42b","examTypeId":4,"examStem":"朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）","examExplain":"","examAnalysis":"紧扣题目作答，注意字数，合理即可。","examAnswer":"【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。","examOptions":[],"optionNumber":0,"customerSchoolId":0,"customerSchoolName":"","baseLevelId":1,"baseLevelName":"初中","baseEducationId":0,"baseEducationName":"","baseSubjectId":2,"baseSubjectName":"语文","baseVersionId":0,"baseVersionName":"","baseGradeId":0,"baseGradeName":"","baseVolumeId":0,"baseVolumeName":"","chapterNodeIdPath":"","chapterNodeNamePath":"","createUserId":0,"createUserName":"","createTime":"1604942475482","updateTime":0,"hots":0,"difficulty":3,"templateStyleId":5,"templateStyleName":"语言表达","flagShare":0,"flagOpen":0,"sourceId":1,"sourceName":"自有题库","zoneIdPath":"","zoneNamePath":"","zoneId":0,"zoneName":"","partnerExamId":"76046143","downCount":0,"viewCount":0,"favTime":0,"children":[],"status":0,"bloomTheoryId":0,"analyticMethods":[],"abilityStructures":[5272],"subjectQualityIds":[],"applicationScenarioIds":[],"chapterIds":[2002622,2002623,2017415,2017416],"knowledgePointIds":[15299],"rejection":"","rejectionState":0,"belongDate":"","importantLevel":0,"labelBeans":[{"val":"语言表达","desc":"","id":{},"name":"","dataList":[]},{"val":"中等","desc":"","id":{},"name":"","dataList":[]},{"val":"2020-11-10","desc":"上传时间","id":{},"name":"","dataList":[]},{"val":"0","desc":"使用次数","id":{},"name":"","dataList":[]},{"val":"表达应用","desc":"学科能力","id":{},"name":"","dataList":[]}],"knowledgePointNames":[],"chapterNames":[],"isCollect":0,"rootExamId":""}},{"id":1244,"examId":"5fcf1c1cb1729e24b747d42c","lessonId":456,"lessonLibId":909,"lessonLibCourseId":1641,"examTemplateId":4,"examTemplateStyleId":"5","examTemplateStyleName":"语言表达","isSubjective":0,"status":1,"score":2,"examSource":"","seq":1,"examText":"{\"abilityStructures\":[5272],\"analyticMethods\":[],\"baseLevelId\":1,\"baseLevelName\":\"初中\",\"baseSubjectId\":2,\"baseSubjectName\":\"语文\",\"chapterIds\":[2002622,2002623,2017415,2017416],\"createTime\":1604942471933,\"difficulty\":2,\"downCount\":0,\"examAnalysis\":\"按要求作答。\",\"examAnswer\":\"<span rt='ryquestiongroup'>小题1<\/span>:(1)春天是绿的(2)绿的小草(3)绿的嫩芽(4)绿的心愿\",\"examOptions\":[],\"examStem\":\"请仿照画线的句子的句式，再写几句话，给春天绘色。<br/><span rt='ryquestiongroup'>小题1<\/span>:<u>春天是红的，红的春联，红的桃花，红的脸颊<\/u>；<span data-ph=\\\"1\\\">____<\/span>，<span data-ph=\\\"2\\\">____<\/span>，<span data-ph=\\\"3\\\">____<\/span>，<span data-ph=\\\"4\\\">____<\/span>；春天是蓝的，蓝的天空，蓝的大海，蓝的梦想。\",\"examTypeId\":4,\"favTime\":0,\"flagOpen\":0,\"flagShare\":0,\"id\":\"5fcf1c1cb1729e24b747d42c\",\"knowledgePointIds\":[15299],\"optionNumber\":0,\"partnerExamId\":\"75348977\",\"sourceId\":1,\"sourceName\":\"自有题库\",\"templateStyleId\":5,\"templateStyleName\":\"语言表达\",\"viewCount\":0}","rootExamId":"","exam":{"id":"5fcf1c1cb1729e24b747d42c","examTypeId":4,"examStem":"请仿照画线的句子的句式，再写几句话，给春天绘色。<br/><span rt='ryquestiongroup'>小题1<\/span>:<u>春天是红的，红的春联，红的桃花，红的脸颊<\/u>；<span data-ph=\"1\">____<\/span>，<span data-ph=\"2\">____<\/span>，<span data-ph=\"3\">____<\/span>，<span data-ph=\"4\">____<\/span>；春天是蓝的，蓝的天空，蓝的大海，蓝的梦想。","examExplain":"","examAnalysis":"按要求作答。","examAnswer":"<span rt='ryquestiongroup'>小题1<\/span>:(1)春天是绿的(2)绿的小草(3)绿的嫩芽(4)绿的心愿","examOptions":[],"optionNumber":0,"customerSchoolId":0,"customerSchoolName":"","baseLevelId":1,"baseLevelName":"初中","baseEducationId":0,"baseEducationName":"","baseSubjectId":2,"baseSubjectName":"语文","baseVersionId":0,"baseVersionName":"","baseGradeId":0,"baseGradeName":"","baseVolumeId":0,"baseVolumeName":"","chapterNodeIdPath":"","chapterNodeNamePath":"","createUserId":0,"createUserName":"","createTime":"1604942471933","updateTime":0,"hots":0,"difficulty":2,"templateStyleId":5,"templateStyleName":"语言表达","flagShare":0,"flagOpen":0,"sourceId":1,"sourceName":"自有题库","zoneIdPath":"","zoneNamePath":"","zoneId":0,"zoneName":"","partnerExamId":"75348977","downCount":0,"viewCount":0,"favTime":0,"children":[],"status":0,"bloomTheoryId":0,"analyticMethods":[],"abilityStructures":[5272],"subjectQualityIds":[],"applicationScenarioIds":[],"chapterIds":[2002622,2002623,2017415,2017416],"knowledgePointIds":[15299],"rejection":"","rejectionState":0,"belongDate":"","importantLevel":0,"labelBeans":[{"val":"语言表达","desc":"","id":{},"name":"","dataList":[]},{"val":"较易","desc":"","id":{},"name":"","dataList":[]},{"val":"2020-11-10","desc":"上传时间","id":{},"name":"","dataList":[]},{"val":"0","desc":"使用次数","id":{},"name":"","dataList":[]},{"val":"表达应用","desc":"学科能力","id":{},"name":"","dataList":[]}],"knowledgePointNames":[],"chapterNames":[],"isCollect":0,"rootExamId":""}}]
             */

            private int id;
            private String name;
            private int type;
            private List<LibCourseDetailListBean> libCourseDetailList;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public List<LibCourseDetailListBean> getLibCourseDetailList() {
                return libCourseDetailList;
            }

            public void setLibCourseDetailList(List<LibCourseDetailListBean> libCourseDetailList) {
                this.libCourseDetailList = libCourseDetailList;
            }

            public static class LibCourseDetailListBean {
                /**
                 * id : 1243
                 * examId : 5fcf1c1ab1729e24b747d42b
                 * lessonId : 456
                 * lessonLibId : 909
                 * lessonLibCourseId : 1641
                 * examTemplateId : 4
                 * examTemplateStyleId : 5
                 * examTemplateStyleName : 语言表达
                 * isSubjective : 0
                 * status : 1
                 * score : 2
                 * examSource :
                 * seq : 0
                 * examText : {"abilityStructures":[5272],"analyticMethods":[],"baseLevelId":1,"baseLevelName":"初中","baseSubjectId":2,"baseSubjectName":"语文","chapterIds":[2002622,2002623,2017415,2017416],"createTime":1604942475482,"difficulty":3,"downCount":0,"examAnalysis":"紧扣题目作答，注意字数，合理即可。","examAnswer":"【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。","examOptions":[],"examStem":"朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）","examTypeId":4,"favTime":0,"flagOpen":0,"flagShare":0,"id":"5fcf1c1ab1729e24b747d42b","knowledgePointIds":[15299],"optionNumber":0,"partnerExamId":"76046143","sourceId":1,"sourceName":"自有题库","templateStyleId":5,"templateStyleName":"语言表达","viewCount":0}
                 * rootExamId :
                 * exam : {"id":"5fcf1c1ab1729e24b747d42b","examTypeId":4,"examStem":"朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）","examExplain":"","examAnalysis":"紧扣题目作答，注意字数，合理即可。","examAnswer":"【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。","examOptions":[],"optionNumber":0,"customerSchoolId":0,"customerSchoolName":"","baseLevelId":1,"baseLevelName":"初中","baseEducationId":0,"baseEducationName":"","baseSubjectId":2,"baseSubjectName":"语文","baseVersionId":0,"baseVersionName":"","baseGradeId":0,"baseGradeName":"","baseVolumeId":0,"baseVolumeName":"","chapterNodeIdPath":"","chapterNodeNamePath":"","createUserId":0,"createUserName":"","createTime":"1604942475482","updateTime":0,"hots":0,"difficulty":3,"templateStyleId":5,"templateStyleName":"语言表达","flagShare":0,"flagOpen":0,"sourceId":1,"sourceName":"自有题库","zoneIdPath":"","zoneNamePath":"","zoneId":0,"zoneName":"","partnerExamId":"76046143","downCount":0,"viewCount":0,"favTime":0,"children":[],"status":0,"bloomTheoryId":0,"analyticMethods":[],"abilityStructures":[5272],"subjectQualityIds":[],"applicationScenarioIds":[],"chapterIds":[2002622,2002623,2017415,2017416],"knowledgePointIds":[15299],"rejection":"","rejectionState":0,"belongDate":"","importantLevel":0,"labelBeans":[{"val":"语言表达","desc":"","id":{},"name":"","dataList":[]},{"val":"中等","desc":"","id":{},"name":"","dataList":[]},{"val":"2020-11-10","desc":"上传时间","id":{},"name":"","dataList":[]},{"val":"0","desc":"使用次数","id":{},"name":"","dataList":[]},{"val":"表达应用","desc":"学科能力","id":{},"name":"","dataList":[]}],"knowledgePointNames":[],"chapterNames":[],"isCollect":0,"rootExamId":""}
                 */

                private int id;
                private String examId;
                private int lessonId;
                private int lessonLibId;
                private int lessonLibCourseId;
                private int examTemplateId;
                private String examTemplateStyleId;
                private String examTemplateStyleName;
                private int isSubjective;
                private int status;
                private int score;
                private String examSource;
                private int seq;
                private String examText;
                private String rootExamId;
                private ExamBean exam;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getExamId() {
                    return examId;
                }

                public void setExamId(String examId) {
                    this.examId = examId;
                }

                public int getLessonId() {
                    return lessonId;
                }

                public void setLessonId(int lessonId) {
                    this.lessonId = lessonId;
                }

                public int getLessonLibId() {
                    return lessonLibId;
                }

                public void setLessonLibId(int lessonLibId) {
                    this.lessonLibId = lessonLibId;
                }

                public int getLessonLibCourseId() {
                    return lessonLibCourseId;
                }

                public void setLessonLibCourseId(int lessonLibCourseId) {
                    this.lessonLibCourseId = lessonLibCourseId;
                }

                public int getExamTemplateId() {
                    return examTemplateId;
                }

                public void setExamTemplateId(int examTemplateId) {
                    this.examTemplateId = examTemplateId;
                }

                public String getExamTemplateStyleId() {
                    return examTemplateStyleId;
                }

                public void setExamTemplateStyleId(String examTemplateStyleId) {
                    this.examTemplateStyleId = examTemplateStyleId;
                }

                public String getExamTemplateStyleName() {
                    return examTemplateStyleName;
                }

                public void setExamTemplateStyleName(String examTemplateStyleName) {
                    this.examTemplateStyleName = examTemplateStyleName;
                }

                public int getIsSubjective() {
                    return isSubjective;
                }

                public void setIsSubjective(int isSubjective) {
                    this.isSubjective = isSubjective;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public int getScore() {
                    return score;
                }

                public void setScore(int score) {
                    this.score = score;
                }

                public String getExamSource() {
                    return examSource;
                }

                public void setExamSource(String examSource) {
                    this.examSource = examSource;
                }

                public int getSeq() {
                    return seq;
                }

                public void setSeq(int seq) {
                    this.seq = seq;
                }

                public String getExamText() {
                    return examText;
                }

                public void setExamText(String examText) {
                    this.examText = examText;
                }

                public String getRootExamId() {
                    return rootExamId;
                }

                public void setRootExamId(String rootExamId) {
                    this.rootExamId = rootExamId;
                }

                public ExamBean getExam() {
                    return exam;
                }

                public void setExam(ExamBean exam) {
                    this.exam = exam;
                }

                public static class ExamBean {
                    /**
                     * id : 5fcf1c1ab1729e24b747d42b
                     * examTypeId : 4
                     * examStem : 朱自清在《春》中运用了大量的比喻，使文章语言无比精彩，提升了文章的感染力。请仿照文中的语句，运用比喻的修辞手法写一段文字。（50字左右）
                     * examExplain :
                     * examAnalysis : 紧扣题目作答，注意字数，合理即可。
                     * examAnswer : 【例文】又是一个春天来临了，三角梅的红色花瓣就像红金子捶成的薄片，只要轻轻撞击就能发出动人的声音。太阳照射的时候，它的每朵花都泛溢着红色的光晕。风儿一吹，每朵花都像一只火红的蝴蝶张开了翅膀，扇动着，奋争着，仿佛急欲挣脱枝头，翩翩飞去。
                     * examOptions : []
                     * optionNumber : 0
                     * customerSchoolId : 0
                     * customerSchoolName :
                     * baseLevelId : 1
                     * baseLevelName : 初中
                     * baseEducationId : 0
                     * baseEducationName :
                     * baseSubjectId : 2
                     * baseSubjectName : 语文
                     * baseVersionId : 0
                     * baseVersionName :
                     * baseGradeId : 0
                     * baseGradeName :
                     * baseVolumeId : 0
                     * baseVolumeName :
                     * chapterNodeIdPath :
                     * chapterNodeNamePath :
                     * createUserId : 0
                     * createUserName :
                     * createTime : 1604942475482
                     * updateTime : 0
                     * hots : 0
                     * difficulty : 3
                     * templateStyleId : 5
                     * templateStyleName : 语言表达
                     * flagShare : 0
                     * flagOpen : 0
                     * sourceId : 1
                     * sourceName : 自有题库
                     * zoneIdPath :
                     * zoneNamePath :
                     * zoneId : 0
                     * zoneName :
                     * partnerExamId : 76046143
                     * downCount : 0
                     * viewCount : 0
                     * favTime : 0
                     * children : []
                     * status : 0
                     * bloomTheoryId : 0
                     * analyticMethods : []
                     * abilityStructures : [5272]
                     * subjectQualityIds : []
                     * applicationScenarioIds : []
                     * chapterIds : [2002622,2002623,2017415,2017416]
                     * knowledgePointIds : [15299]
                     * rejection :
                     * rejectionState : 0
                     * belongDate :
                     * importantLevel : 0
                     * labelBeans : [{"val":"语言表达","desc":"","id":{},"name":"","dataList":[]},{"val":"中等","desc":"","id":{},"name":"","dataList":[]},{"val":"2020-11-10","desc":"上传时间","id":{},"name":"","dataList":[]},{"val":"0","desc":"使用次数","id":{},"name":"","dataList":[]},{"val":"表达应用","desc":"学科能力","id":{},"name":"","dataList":[]}]
                     * knowledgePointNames : []
                     * chapterNames : []
                     * isCollect : 0
                     * rootExamId :
                     */

                    private String id;
                    private int examTypeId;
                    private String examStem;
                    private String examExplain;
                    private String examAnalysis;
                    private String examAnswer;
                    private int optionNumber;
                    private int customerSchoolId;
                    private String customerSchoolName;
                    private int baseLevelId;
                    private String baseLevelName;
                    private int baseEducationId;
                    private String baseEducationName;
                    private int baseSubjectId;
                    private String baseSubjectName;
                    private int baseVersionId;
                    private String baseVersionName;
                    private int baseGradeId;
                    private String baseGradeName;
                    private int baseVolumeId;
                    private String baseVolumeName;
                    private String chapterNodeIdPath;
                    private String chapterNodeNamePath;
                    private int createUserId;
                    private String createUserName;
                    private String createTime;
                    private int updateTime;
                    private int hots;
                    private int difficulty;
                    private int templateStyleId;
                    private String templateStyleName;
                    private int flagShare;
                    private int flagOpen;
                    private int sourceId;
                    private String sourceName;
                    private String zoneIdPath;
                    private String zoneNamePath;
                    private int zoneId;
                    private String zoneName;
                    private String partnerExamId;
                    private int downCount;
                    private int viewCount;
                    private int favTime;
                    private int status;
                    private int bloomTheoryId;
                    private String rejection;
                    private int rejectionState;
                    private String belongDate;
                    private int importantLevel;
                    private int isCollect;
                    private String rootExamId;
                    private List<?> examOptions;
                    private List<?> children;
                    private List<?> analyticMethods;
                    private List<Integer> abilityStructures;
                    private List<?> subjectQualityIds;
                    private List<?> applicationScenarioIds;
                    private List<Integer> chapterIds;
                    private List<Integer> knowledgePointIds;
                    private List<LabelBeansBean> labelBeans;
                    private List<?> knowledgePointNames;
                    private List<?> chapterNames;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public int getExamTypeId() {
                        return examTypeId;
                    }

                    public void setExamTypeId(int examTypeId) {
                        this.examTypeId = examTypeId;
                    }

                    public String getExamStem() {
                        return examStem;
                    }

                    public void setExamStem(String examStem) {
                        this.examStem = examStem;
                    }

                    public String getExamExplain() {
                        return examExplain;
                    }

                    public void setExamExplain(String examExplain) {
                        this.examExplain = examExplain;
                    }

                    public String getExamAnalysis() {
                        return examAnalysis;
                    }

                    public void setExamAnalysis(String examAnalysis) {
                        this.examAnalysis = examAnalysis;
                    }

                    public String getExamAnswer() {
                        return examAnswer;
                    }

                    public void setExamAnswer(String examAnswer) {
                        this.examAnswer = examAnswer;
                    }

                    public int getOptionNumber() {
                        return optionNumber;
                    }

                    public void setOptionNumber(int optionNumber) {
                        this.optionNumber = optionNumber;
                    }

                    public int getCustomerSchoolId() {
                        return customerSchoolId;
                    }

                    public void setCustomerSchoolId(int customerSchoolId) {
                        this.customerSchoolId = customerSchoolId;
                    }

                    public String getCustomerSchoolName() {
                        return customerSchoolName;
                    }

                    public void setCustomerSchoolName(String customerSchoolName) {
                        this.customerSchoolName = customerSchoolName;
                    }

                    public int getBaseLevelId() {
                        return baseLevelId;
                    }

                    public void setBaseLevelId(int baseLevelId) {
                        this.baseLevelId = baseLevelId;
                    }

                    public String getBaseLevelName() {
                        return baseLevelName;
                    }

                    public void setBaseLevelName(String baseLevelName) {
                        this.baseLevelName = baseLevelName;
                    }

                    public int getBaseEducationId() {
                        return baseEducationId;
                    }

                    public void setBaseEducationId(int baseEducationId) {
                        this.baseEducationId = baseEducationId;
                    }

                    public String getBaseEducationName() {
                        return baseEducationName;
                    }

                    public void setBaseEducationName(String baseEducationName) {
                        this.baseEducationName = baseEducationName;
                    }

                    public int getBaseSubjectId() {
                        return baseSubjectId;
                    }

                    public void setBaseSubjectId(int baseSubjectId) {
                        this.baseSubjectId = baseSubjectId;
                    }

                    public String getBaseSubjectName() {
                        return baseSubjectName;
                    }

                    public void setBaseSubjectName(String baseSubjectName) {
                        this.baseSubjectName = baseSubjectName;
                    }

                    public int getBaseVersionId() {
                        return baseVersionId;
                    }

                    public void setBaseVersionId(int baseVersionId) {
                        this.baseVersionId = baseVersionId;
                    }

                    public String getBaseVersionName() {
                        return baseVersionName;
                    }

                    public void setBaseVersionName(String baseVersionName) {
                        this.baseVersionName = baseVersionName;
                    }

                    public int getBaseGradeId() {
                        return baseGradeId;
                    }

                    public void setBaseGradeId(int baseGradeId) {
                        this.baseGradeId = baseGradeId;
                    }

                    public String getBaseGradeName() {
                        return baseGradeName;
                    }

                    public void setBaseGradeName(String baseGradeName) {
                        this.baseGradeName = baseGradeName;
                    }

                    public int getBaseVolumeId() {
                        return baseVolumeId;
                    }

                    public void setBaseVolumeId(int baseVolumeId) {
                        this.baseVolumeId = baseVolumeId;
                    }

                    public String getBaseVolumeName() {
                        return baseVolumeName;
                    }

                    public void setBaseVolumeName(String baseVolumeName) {
                        this.baseVolumeName = baseVolumeName;
                    }

                    public String getChapterNodeIdPath() {
                        return chapterNodeIdPath;
                    }

                    public void setChapterNodeIdPath(String chapterNodeIdPath) {
                        this.chapterNodeIdPath = chapterNodeIdPath;
                    }

                    public String getChapterNodeNamePath() {
                        return chapterNodeNamePath;
                    }

                    public void setChapterNodeNamePath(String chapterNodeNamePath) {
                        this.chapterNodeNamePath = chapterNodeNamePath;
                    }

                    public int getCreateUserId() {
                        return createUserId;
                    }

                    public void setCreateUserId(int createUserId) {
                        this.createUserId = createUserId;
                    }

                    public String getCreateUserName() {
                        return createUserName;
                    }

                    public void setCreateUserName(String createUserName) {
                        this.createUserName = createUserName;
                    }

                    public String getCreateTime() {
                        return createTime;
                    }

                    public void setCreateTime(String createTime) {
                        this.createTime = createTime;
                    }

                    public int getUpdateTime() {
                        return updateTime;
                    }

                    public void setUpdateTime(int updateTime) {
                        this.updateTime = updateTime;
                    }

                    public int getHots() {
                        return hots;
                    }

                    public void setHots(int hots) {
                        this.hots = hots;
                    }

                    public int getDifficulty() {
                        return difficulty;
                    }

                    public void setDifficulty(int difficulty) {
                        this.difficulty = difficulty;
                    }

                    public int getTemplateStyleId() {
                        return templateStyleId;
                    }

                    public void setTemplateStyleId(int templateStyleId) {
                        this.templateStyleId = templateStyleId;
                    }

                    public String getTemplateStyleName() {
                        return templateStyleName;
                    }

                    public void setTemplateStyleName(String templateStyleName) {
                        this.templateStyleName = templateStyleName;
                    }

                    public int getFlagShare() {
                        return flagShare;
                    }

                    public void setFlagShare(int flagShare) {
                        this.flagShare = flagShare;
                    }

                    public int getFlagOpen() {
                        return flagOpen;
                    }

                    public void setFlagOpen(int flagOpen) {
                        this.flagOpen = flagOpen;
                    }

                    public int getSourceId() {
                        return sourceId;
                    }

                    public void setSourceId(int sourceId) {
                        this.sourceId = sourceId;
                    }

                    public String getSourceName() {
                        return sourceName;
                    }

                    public void setSourceName(String sourceName) {
                        this.sourceName = sourceName;
                    }

                    public String getZoneIdPath() {
                        return zoneIdPath;
                    }

                    public void setZoneIdPath(String zoneIdPath) {
                        this.zoneIdPath = zoneIdPath;
                    }

                    public String getZoneNamePath() {
                        return zoneNamePath;
                    }

                    public void setZoneNamePath(String zoneNamePath) {
                        this.zoneNamePath = zoneNamePath;
                    }

                    public int getZoneId() {
                        return zoneId;
                    }

                    public void setZoneId(int zoneId) {
                        this.zoneId = zoneId;
                    }

                    public String getZoneName() {
                        return zoneName;
                    }

                    public void setZoneName(String zoneName) {
                        this.zoneName = zoneName;
                    }

                    public String getPartnerExamId() {
                        return partnerExamId;
                    }

                    public void setPartnerExamId(String partnerExamId) {
                        this.partnerExamId = partnerExamId;
                    }

                    public int getDownCount() {
                        return downCount;
                    }

                    public void setDownCount(int downCount) {
                        this.downCount = downCount;
                    }

                    public int getViewCount() {
                        return viewCount;
                    }

                    public void setViewCount(int viewCount) {
                        this.viewCount = viewCount;
                    }

                    public int getFavTime() {
                        return favTime;
                    }

                    public void setFavTime(int favTime) {
                        this.favTime = favTime;
                    }

                    public int getStatus() {
                        return status;
                    }

                    public void setStatus(int status) {
                        this.status = status;
                    }

                    public int getBloomTheoryId() {
                        return bloomTheoryId;
                    }

                    public void setBloomTheoryId(int bloomTheoryId) {
                        this.bloomTheoryId = bloomTheoryId;
                    }

                    public String getRejection() {
                        return rejection;
                    }

                    public void setRejection(String rejection) {
                        this.rejection = rejection;
                    }

                    public int getRejectionState() {
                        return rejectionState;
                    }

                    public void setRejectionState(int rejectionState) {
                        this.rejectionState = rejectionState;
                    }

                    public String getBelongDate() {
                        return belongDate;
                    }

                    public void setBelongDate(String belongDate) {
                        this.belongDate = belongDate;
                    }

                    public int getImportantLevel() {
                        return importantLevel;
                    }

                    public void setImportantLevel(int importantLevel) {
                        this.importantLevel = importantLevel;
                    }

                    public int getIsCollect() {
                        return isCollect;
                    }

                    public void setIsCollect(int isCollect) {
                        this.isCollect = isCollect;
                    }

                    public String getRootExamId() {
                        return rootExamId;
                    }

                    public void setRootExamId(String rootExamId) {
                        this.rootExamId = rootExamId;
                    }

                    public List<?> getExamOptions() {
                        return examOptions;
                    }

                    public void setExamOptions(List<?> examOptions) {
                        this.examOptions = examOptions;
                    }

                    public List<?> getChildren() {
                        return children;
                    }

                    public void setChildren(List<?> children) {
                        this.children = children;
                    }

                    public List<?> getAnalyticMethods() {
                        return analyticMethods;
                    }

                    public void setAnalyticMethods(List<?> analyticMethods) {
                        this.analyticMethods = analyticMethods;
                    }

                    public List<Integer> getAbilityStructures() {
                        return abilityStructures;
                    }

                    public void setAbilityStructures(List<Integer> abilityStructures) {
                        this.abilityStructures = abilityStructures;
                    }

                    public List<?> getSubjectQualityIds() {
                        return subjectQualityIds;
                    }

                    public void setSubjectQualityIds(List<?> subjectQualityIds) {
                        this.subjectQualityIds = subjectQualityIds;
                    }

                    public List<?> getApplicationScenarioIds() {
                        return applicationScenarioIds;
                    }

                    public void setApplicationScenarioIds(List<?> applicationScenarioIds) {
                        this.applicationScenarioIds = applicationScenarioIds;
                    }

                    public List<Integer> getChapterIds() {
                        return chapterIds;
                    }

                    public void setChapterIds(List<Integer> chapterIds) {
                        this.chapterIds = chapterIds;
                    }

                    public List<Integer> getKnowledgePointIds() {
                        return knowledgePointIds;
                    }

                    public void setKnowledgePointIds(List<Integer> knowledgePointIds) {
                        this.knowledgePointIds = knowledgePointIds;
                    }

                    public List<LabelBeansBean> getLabelBeans() {
                        return labelBeans;
                    }

                    public void setLabelBeans(List<LabelBeansBean> labelBeans) {
                        this.labelBeans = labelBeans;
                    }

                    public List<?> getKnowledgePointNames() {
                        return knowledgePointNames;
                    }

                    public void setKnowledgePointNames(List<?> knowledgePointNames) {
                        this.knowledgePointNames = knowledgePointNames;
                    }

                    public List<?> getChapterNames() {
                        return chapterNames;
                    }

                    public void setChapterNames(List<?> chapterNames) {
                        this.chapterNames = chapterNames;
                    }

                    public static class LabelBeansBean {
                        /**
                         * val : 语言表达
                         * desc :
                         * id : {}
                         * name :
                         * dataList : []
                         */

                        private String val;
                        private String desc;
                        private IdBean id;
                        private String name;
                        private List<?> dataList;

                        public String getVal() {
                            return val;
                        }

                        public void setVal(String val) {
                            this.val = val;
                        }

                        public String getDesc() {
                            return desc;
                        }

                        public void setDesc(String desc) {
                            this.desc = desc;
                        }

                        public IdBean getId() {
                            return id;
                        }

                        public void setId(IdBean id) {
                            this.id = id;
                        }

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public List<?> getDataList() {
                            return dataList;
                        }

                        public void setDataList(List<?> dataList) {
                            this.dataList = dataList;
                        }

                        public static class IdBean {
                        }
                    }
                }
            }
        }
    }
}
