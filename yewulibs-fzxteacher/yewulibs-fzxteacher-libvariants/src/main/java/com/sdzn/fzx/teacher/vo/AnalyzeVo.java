package com.sdzn.fzx.teacher.vo;


import androidx.annotation.NonNull;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/31
 * 修改单号：
 * 修改内容:
 */
public class AnalyzeVo {
    /**
     * total : 3
     * data : [{"id":20000,"examId":"5c121aaeb2c0131b25983330","examTemplateId":4,"examTemplateStyleId":359,"examTemplateStyleName":"默写","examEmptyCount":0,"examDifficulty":1,"resourceId":null,"resourceType":null,"resourceName":null,"type":1,"lessonLibId":2455,"parentId":0,"seq":2,"lessonTaskId":2053,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1544690350661,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":3,\"examAnalysis\":\"答案：<br />日月之行&nbsp; 若出其中&nbsp; 星汉灿烂&nbsp; 若出其里（重点字：星汉）\",\"examAnswer\":\"1. 日月之行 <br>2. 若出其中 <br>3. 星汉灿烂 <br>4. 若出其里 <br>\",\"examId\":\"5c121aaeb2c0131b25983330\",\"examOptions\":[],\"examStem\":\"《观沧海》中表现作者伟大胸襟的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":1,\"id\":\"5c5195b21cb07676c580b8bb\",\"label\":\"（2016秋\u2022宁河县月考）\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":72,\"partnerExamId\":\"87116caa-c9a7-4694-9f89-d34d702f2387\",\"points\":[{\"eName\":\"chinese\",\"id\":3762,\"levelId\":2,\"name\":\"名篇名句默写\",\"no\":\"EF\",\"nodeIdPath\":\"0.3729.3747.3762\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":359,\"templateStyleName\":\"默写\",\"type\":1,\"updateTime\":1544690381746,\"viewCount\":34,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","resourceText":null,"score":-1,"examSeq":1,"childList":[],"libExamId":null,"libResourceId":null,"taskExamId":"5c5195b21cb07676c580b8bb","taskResourceId":null},{"id":20001,"examId":"5c5193b31cb07676c580b8b4","examTemplateId":4,"examTemplateStyleId":359,"examTemplateStyleName":"默写","examEmptyCount":0,"examDifficulty":1,"resourceId":null,"resourceType":null,"resourceName":null,"type":1,"lessonLibId":2455,"parentId":0,"seq":3,"lessonTaskId":2053,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1548850099129,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":14,\"examAnalysis\":\"答案为：<br />（1）树木丛生，百草丰茂。&nbsp;&nbsp;日月之行，若出其中。星汉灿烂，若出其里&nbsp;（重点字：灿烂）<br />（2）夕阳西下，断肠人在天涯（重点字：涯）<br />&nbsp;（3）海日生残夜，江春入旧年；潮平两岸阔，风正一帆悬；乡书何处达？归雁洛阳边（重点字：悬）<br />（4）我寄愁心与明月，随君直到夜郎西（重点字：郎）\",\"examAnswer\":\"1. 树木丛生 <br>2. 百草丰茂 <br>3. 日月之行 <br>4. 若出其中 <br>5. 星汉灿烂 <br>6. 若出其里 <br>7. 夕阳西下 <br>8. 断肠人在天涯 <br>9. 海日生残夜 <br>10. 江春入旧年 <br>11. 潮平两岸阔 <br>12. 风正一帆悬 <br>13. 乡书何处达 <br>14. 归雁洛阳边 <br>15. 我寄愁心与明月 <br>16. 随君直到夜郎西 <br>\",\"examId\":\"5c5193b31cb07676c580b8b4\",\"examOptions\":[],\"examStem\":\"根据要求默写。<br />（1）《观沧海》中展现海岛勃勃生机的诗句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；展现大海的壮丽景象和诗人博大胸怀的诗句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（2）崔颢在《黄鹤楼》中写道\u201c日暮乡关何处是？烟波江上使人愁\u201d，而马致远在《天净沙\u2022秋思》中也有两句与此意境相似的句子：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（3）《次北固山下》一诗中即景抒情又蕴含自然理趣的两句诗是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；描写涨潮时水面宽阔的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；表达乡愁的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>？<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（4）《闻王昌龄左迁龙标遥有此寄》中借明月以抒发思念家乡、怀念朋友的感情的名句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5c5195b21cb07676c580b8bc\",\"label\":\"\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":100,\"partnerExamId\":\"522af16d-63bc-4ab4-9110-fe1e35a036b5\",\"points\":[{\"eName\":\"chinese\",\"id\":3762,\"levelId\":2,\"name\":\"名篇名句默写\",\"no\":\"EF\",\"nodeIdPath\":\"0.3729.3747.3762\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":359,\"templateStyleName\":\"默写\",\"type\":1,\"updateTime\":1548850099129,\"viewCount\":39,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","resourceText":null,"score":-1,"examSeq":2,"childList":[],"libExamId":null,"libResourceId":null,"taskExamId":"5c5195b21cb07676c580b8bc","taskResourceId":null},{"id":20002,"examId":"5c121ab6b2c0131b25983331","examTemplateId":4,"examTemplateStyleId":301,"examTemplateStyleName":"简答题","examEmptyCount":0,"examDifficulty":2,"resourceId":null,"resourceType":null,"resourceName":null,"type":1,"lessonLibId":2455,"parentId":0,"seq":4,"lessonTaskId":2053,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1544690358274,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":2,\"downCount\":4,\"examAnalysis\":\"（1）通过阅读，理解诗歌内容，把握各个意象之间的关系，是解答本题的关键。前三行全是写景，十八字白描勾勒出这样一幅生动的深秋晚景图。\u201c枯藤老树昏鸦\u201d中\u201c枯\u201d\u201c老\u201d\u201c昏\u201d三个词描绘出当时诗人所处的悲凉氛围。\u201c小桥流水人家\u201d描绘了一幅安宁、和谐的景象，与沦落异乡的游子相映，使图景带上悲凉的气氛。使\u201c断肠人\u201d更添悲愁。\u201c古道西风瘦马\u201d正是诗人当时自己的真实写照，他长期奔波与劳累已不言而喻了。这与归巢的昏鸦与团聚的人家真可谓构成了鲜明的对照。作者寄情于物表达天涯沦落人的凄苦之情。因此描绘的是一幅萧瑟（萧索）、荒凉的深秋晚景图。<br />（2）通过反复诵读体会诗歌的情感，学生应全面了解背景，把握形象内涵，体会意境特点，明确抒情方式。这里通过秋天意象的描写，表达了游子在秋天思念故乡、倦于漂泊的孤寂愁苦之情。<br />答案：<br />（1）萧瑟（萧索）、荒凉。<br />（2）孤寂愁苦，思念家乡。（答\u201c思乡\u201d或\u201c飘零天涯的游子在秋天思念故乡、倦于漂泊的凄苦愁楚之情\u201d也可）<br />翻译：<br />&nbsp;&nbsp;&nbsp; 枯萎的藤蔓，垂老的古树，黄昏时的乌鸦，扑打着翅膀，落在光秃秃的枝桠上。纤巧别致的小桥，潺潺的流水，低矮破旧的几间茅屋，愈发显得安谧而温馨。荒凉的古道上，一匹消瘦憔悴的马载着同样疲惫憔悴的异乡游子，在异乡的西风里踌躇而行。夕阳渐渐落山了，但是在外的游子，何处是归宿？家乡在何方？念及此，天涯漂泊的游子怎能不愁肠寸断！\",\"examAnswer\":\"答案见解析\",\"examId\":\"5c121ab6b2c0131b25983331\",\"examOptions\":[],\"examStem\":\"<BDO class=mathjye-aligncenter>天净沙\u2022秋思<br />马致远<br />枯藤老树昏鸦，<br />小桥流水人家。<br />古道西风瘦马。<br />夕阳西下，<br />断肠人在天涯。<br /><\/BDO>（1）这首小令的前三句描绘出一幅怎样的深秋晚景图？<br />（2）这首小令中的\u201c断肠人在天涯\u201d抒发了作者什么样的思想感情？\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":1,\"id\":\"5c5195b21cb07676c580b8bd\",\"label\":\"（2014\u2022娄底）\",\"labelReportID\":\"3262b2d3-9bc3-4eb2-bff6-1fee01aaa889\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":82,\"partnerExamId\":\"e2775eba-c032-42fc-963a-e6f74af47003\",\"points\":[{\"eName\":\"chinese\",\"id\":3770,\"levelId\":2,\"name\":\"爱情闺怨诗\",\"no\":\"EN\",\"nodeIdPath\":\"0.3729.3747.3770\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":301,\"templateStyleName\":\"简答题\",\"type\":1,\"updateTime\":1544690381749,\"viewCount\":393,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","resourceText":null,"score":-1,"examSeq":3,"childList":[],"libExamId":null,"libResourceId":null,"taskExamId":"5c5195b21cb07676c580b8bd","taskResourceId":null}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 20000
         * examId : 5c121aaeb2c0131b25983330
         * examTemplateId : 4
         * examTemplateStyleId : 359
         * examTemplateStyleName : 默写
         * examEmptyCount : 0
         * examDifficulty : 1
         * resourceId : null
         * resourceType : null
         * resourceName : null
         * type : 1
         * lessonLibId : 2455
         * parentId : 0
         * seq : 2
         * lessonTaskId : 2053
         * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","baseVolumeId":1,"baseVolumeName":"上册","chapterId":2888,"chapterName":"第一单元","chapterNodeIdPath":"0.2888","chapterNodeNamePath":"第一单元","createTime":1544690350661,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"downCount":3,"examAnalysis":"答案：<br />日月之行&nbsp; 若出其中&nbsp; 星汉灿烂&nbsp; 若出其里（重点字：星汉）","examAnswer":"1. 日月之行 <br>2. 若出其中 <br>3. 星汉灿烂 <br>4. 若出其里 <br>","examId":"5c121aaeb2c0131b25983330","examOptions":[],"examStem":"《观沧海》中表现作者伟大胸襟的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>。","examTypeId":4,"favTime":0,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":1,"id":"5c5195b21cb07676c580b8bb","label":"（2016秋•宁河县月考）","lessonLibId":2455,"lessonTaskId":2053,"optionNumber":0,"paperCount":72,"partnerExamId":"87116caa-c9a7-4694-9f89-d34d702f2387","points":[{"eName":"chinese","id":3762,"levelId":2,"name":"名篇名句默写","no":"EF","nodeIdPath":"0.3729.3747.3762","parentId":"3747","subjectId":1}],"score":0,"sourceId":3,"sourceName":"菁优网","templateStyleId":359,"templateStyleName":"默写","type":1,"updateTime":1544690381746,"viewCount":34,"zoneIdPath":"0.15.223.5025","zoneName":"高新区"}
         * resourceText : null
         * score : -1.0
         * examSeq : 1
         * childList : []
         * libExamId : null
         * libResourceId : null
         * taskExamId : 5c5195b21cb07676c580b8bb
         * taskResourceId : null
         */

        private int id;
        private String examId;
        private int examTemplateId;
        private int examTemplateStyleId;
        private String examTemplateStyleName;
        private int examEmptyCount;
        private int examDifficulty;
        private Object resourceId;
        private Object resourceType;
        private Object resourceName;
        private int type;
        private int lessonLibId;
        private int parentId;
        private int seq;
        private int lessonTaskId;
        private String examText;
        private Object resourceText;
        private int score;
        private int examSeq;
        private Object libExamId;
        private Object libResourceId;
        private String taskExamId;
        private Object taskResourceId;
        private List<DataBean> childList;
        private ExamText ExamTextVo;

        public ExamText getExamTextVo() {
            return ExamTextVo;
        }

        public void setExamTextVo(ExamText examTextVo) {
            ExamTextVo = examTextVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExamId() {
            return examId;
        }

        public void setExamId(String examId) {
            this.examId = examId;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public String getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getExamEmptyCount() {
            return examEmptyCount;
        }

        public void setExamEmptyCount(int examEmptyCount) {
            this.examEmptyCount = examEmptyCount;
        }

        public int getExamDifficulty() {
            return examDifficulty;
        }

        public void setExamDifficulty(int examDifficulty) {
            this.examDifficulty = examDifficulty;
        }

        public Object getResourceId() {
            return resourceId;
        }

        public void setResourceId(Object resourceId) {
            this.resourceId = resourceId;
        }

        public Object getResourceType() {
            return resourceType;
        }

        public void setResourceType(Object resourceType) {
            this.resourceType = resourceType;
        }

        public Object getResourceName() {
            return resourceName;
        }

        public void setResourceName(Object resourceName) {
            this.resourceName = resourceName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(int lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public Object getResourceText() {
            return resourceText;
        }

        public void setResourceText(Object resourceText) {
            this.resourceText = resourceText;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public Object getLibExamId() {
            return libExamId;
        }

        public void setLibExamId(Object libExamId) {
            this.libExamId = libExamId;
        }

        public Object getLibResourceId() {
            return libResourceId;
        }

        public void setLibResourceId(Object libResourceId) {
            this.libResourceId = libResourceId;
        }

        public String getTaskExamId() {
            return taskExamId;
        }

        public void setTaskExamId(String taskExamId) {
            this.taskExamId = taskExamId;
        }

        public Object getTaskResourceId() {
            return taskResourceId;
        }

        public void setTaskResourceId(Object taskResourceId) {
            this.taskResourceId = taskResourceId;
        }

        public List<DataBean> getChildList() {
            return childList;
        }

        public void setChildList(List<DataBean> childList) {
            this.childList = childList;
        }
    }

    /**
     * 答题内容bean
     */
    public static class ExamOptionBean implements Comparable<ExamOptionBean> {

        private int id;//试题答案选项id
        private int isRight;//是否正确答案
        private int lessonAnswerExamId;//试题答案id
        private int lessonTaskStudentId;//学生对应试卷的id
        /**
         * 先按;切, 再按,切
         */
        private String myAnswer;//答题内容, 填空题
        private String myAnswerHtml;
        private String myOldAnswer;//最开始提交的作答结果集
        private int seq;//选择:第几个选项;填空:第几个空;完形填空:第几个小题;简答:2=拍照1=手写;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public int getLessonAnswerExamId() {
            return lessonAnswerExamId;
        }

        public void setLessonAnswerExamId(int lessonAnswerExamId) {
            this.lessonAnswerExamId = lessonAnswerExamId;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public String getMyAnswer() {
            return myAnswer;
        }

        public void setMyAnswer(String myAnswer) {
            this.myAnswer = myAnswer;
        }

        public String getMyAnswerHtml() {
            return myAnswerHtml;
        }

        public void setMyAnswerHtml(String myAnswerHtml) {
            this.myAnswerHtml = myAnswerHtml;
        }

        public String getMyOldAnswer() {
            return myOldAnswer;
        }

        public void setMyOldAnswer(String myOldAnswer) {
            this.myOldAnswer = myOldAnswer;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptionBean o) {
            return Integer.compare(seq, o.seq);
        }
    }

    public static class ExamOptions implements Comparable<ExamOptions> {
        private String analysis;
        private String content;
        private boolean right;
        private int seq;//选项序号
        private List<StudentDetails.DataBean.ExamOptions> list;

        public List<StudentDetails.DataBean.ExamOptions> getList() {
            return list;
        }

        public void setList(List<StudentDetails.DataBean.ExamOptions> list) {
            this.list = list;
        }

        public String getAnalysis() {
            return analysis;
        }

        public void setAnalysis(String analysis) {
            this.analysis = analysis;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptions o) {
            return Integer.compare(seq, o.seq);
        }
    }

}
