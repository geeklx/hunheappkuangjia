package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * zs
 */
public class StudentListTiWen {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5289
         * accountId : 5838
         * accountName : 28406710
         * photo : null
         * realName : nose
         * studentNumber : null
         * identityId : 972120181204180880
         * scope : 2018
         * sex : 0
         */

        private int id;
        private int accountId;
        private String accountName;
        private Object photo;
        private String realName;
        private Object studentNumber;
        private String identityId;
        private int scope;
        private int sex;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAccountId() {
            return accountId;
        }

        public void setAccountId(int accountId) {
            this.accountId = accountId;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public Object getPhoto() {
            return photo;
        }

        public void setPhoto(Object photo) {
            this.photo = photo;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public Object getStudentNumber() {
            return studentNumber;
        }

        public void setStudentNumber(Object studentNumber) {
            this.studentNumber = studentNumber;
        }

        public String getIdentityId() {
            return identityId;
        }

        public void setIdentityId(String identityId) {
            this.identityId = identityId;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }
    }
}
