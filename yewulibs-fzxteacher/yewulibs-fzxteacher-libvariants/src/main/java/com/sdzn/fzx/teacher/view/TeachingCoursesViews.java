package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.TeachingCoursesBean;

/**
 * 首页 view
 */
public interface TeachingCoursesViews extends IView {
    void onTeachingCoursesSuccess(TeachingCoursesBean teachingCoursesBean);

    void onTeachingCoursesNodata(String msg);

    void onTeachingCoursesFail(String msg);
}
