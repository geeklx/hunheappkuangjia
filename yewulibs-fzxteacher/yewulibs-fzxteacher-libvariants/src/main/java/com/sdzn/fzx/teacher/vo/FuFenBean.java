package com.sdzn.fzx.teacher.vo;

import java.util.List;

public class FuFenBean {

    /**
     * total : 4
     * data : [{"groupId":0,"groupName":"未分组","studentList":[{"id":null,"classId":604,"className":null,"studentId":5120,"studentName":"学生16","classGroupId":0,"classGroupName":"未分组","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":null}],"points":0},{"groupId":253,"groupName":"测试1组","studentList":[{"id":807,"classId":604,"className":"十一班","studentId":4596,"studentName":"测试15","classGroupId":253,"classGroupName":"测试1组","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":null},{"id":808,"classId":604,"className":"十一班","studentId":4597,"studentName":"测试16","classGroupId":253,"classGroupName":"测试1组","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":null},{"id":809,"classId":604,"className":"十一班","studentId":4598,"studentName":"测试17","classGroupId":253,"classGroupName":"测试1组","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":null}],"points":0},{"groupId":254,"groupName":"测试2组","studentList":[{"id":815,"classId":604,"className":"十一班","studentId":4599,"studentName":"测试18","classGroupId":254,"classGroupName":"测试2组","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":null}],"points":0},{"groupId":255,"groupName":"测试3组","studentList":[{"id":821,"classId":604,"className":"十一班","studentId":5289,"studentName":"nose","classGroupId":255,"classGroupName":"测试3组","createAccountId":null,"createTime":null,"createUserName":null,"sex":0,"photo":null},{"id":820,"classId":604,"className":"十一班","studentId":5290,"studentName":"apple","classGroupId":255,"classGroupName":"测试3组","createAccountId":null,"createTime":null,"createUserName":null,"sex":0,"photo":null},{"id":819,"classId":604,"className":"十一班","studentId":5291,"studentName":"leg ","classGroupId":255,"classGroupName":"测试3组","createAccountId":null,"createTime":null,"createUserName":null,"sex":0,"photo":null},{"id":818,"classId":604,"className":"十一班","studentId":5292,"studentName":"book ","classGroupId":255,"classGroupName":"测试3组","createAccountId":null,"createTime":null,"createUserName":null,"sex":0,"photo":null},{"id":817,"classId":604,"className":"十一班","studentId":5293,"studentName":"cat","classGroupId":255,"classGroupName":"测试3组","createAccountId":null,"createTime":null,"createUserName":null,"sex":0,"photo":null}],"points":0}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * groupId : 0
         * groupName : 未分组
         * studentList : [{"id":null,"classId":604,"className":null,"studentId":5120,"studentName":"学生16","classGroupId":0,"classGroupName":"未分组","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":null}]
         * points : 0
         */

        private int groupId;
        private String groupName;
        private int points;
        private List<StudentListBean> studentList;

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public List<StudentListBean> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<StudentListBean> studentList) {
            this.studentList = studentList;
        }

        public static class StudentListBean {
            /**
             * id : null
             * classId : 604
             * className : null
             * studentId : 5120
             * studentName : 学生16
             * classGroupId : 0
             * classGroupName : 未分组
             * createAccountId : null
             * createTime : null
             * createUserName : null
             * sex : 1
             * photo : null
             */

            private int id;
            private int classId;
            private String className;
            private int studentId;
            private String studentName;
            private int classGroupId;
            private String classGroupName;
            private String createAccountId;
            private String createTime;
            private String createUserName;
            private int sex;
            private String photo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getClassId() {
                return classId;
            }

            public void setClassId(int classId) {
                this.classId = classId;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }

            public int getStudentId() {
                return studentId;
            }

            public void setStudentId(int studentId) {
                this.studentId = studentId;
            }

            public String getStudentName() {
                return studentName;
            }

            public void setStudentName(String studentName) {
                this.studentName = studentName;
            }

            public int getClassGroupId() {
                return classGroupId;
            }

            public void setClassGroupId(int classGroupId) {
                this.classGroupId = classGroupId;
            }

            public String getClassGroupName() {
                return classGroupName;
            }

            public void setClassGroupName(String classGroupName) {
                this.classGroupName = classGroupName;
            }

            public String getCreateAccountId() {
                return createAccountId;
            }

            public void setCreateAccountId(String createAccountId) {
                this.createAccountId = createAccountId;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }
    }
}
