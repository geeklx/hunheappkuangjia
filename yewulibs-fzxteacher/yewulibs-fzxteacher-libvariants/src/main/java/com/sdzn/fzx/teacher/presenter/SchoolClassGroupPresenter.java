package com.sdzn.fzx.teacher.presenter;


import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.SchoolClassGroupBean;
import com.sdzn.fzx.teacher.view.SchoolClassGroupViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class SchoolClassGroupPresenter extends Presenter<SchoolClassGroupViews> {

    public void getClassGroup( String token,String classId) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .getClassGroup(BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/schoolClassGroup/groups/count", token, classId)
                .enqueue(new Callback<ResponseSlbBean1<SchoolClassGroupBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<SchoolClassGroupBean>> call, Response<ResponseSlbBean1<SchoolClassGroupBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
//                        if (response.body().getCode() != 0) {
//                            getView().OnSubjectNodata(response.body().getMessage());
//                            return;
//                        }
                        getView().getSchoolClassGroupSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<SchoolClassGroupBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onSchoolClassGroupFailed("");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


}
