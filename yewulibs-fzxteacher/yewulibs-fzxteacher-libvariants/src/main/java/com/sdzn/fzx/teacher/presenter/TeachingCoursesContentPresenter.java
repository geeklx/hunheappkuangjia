package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.view.TeachingCoursesContentViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeachingCoursesContentPresenter extends Presenter<TeachingCoursesContentViews> {
    public void queryCoursesContent(String token,String lessonId) {
        //ClassContentBean

        JSONObject requestData = new JSONObject();
        requestData.put("lessonId", lessonId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryCoursesContent(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/classRoomStaty/selectClassRoomStatyContent", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<ClassContentBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ClassContentBean>> call, Response<ResponseSlbBean1<ClassContentBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCoursesContentNodata(response.body().getMessage());
                            return;
                        }
                        Log.e("eeee","--"+ response.body().getResult());
                        getView().onCoursesContentSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ClassContentBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCoursesContentFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

}
