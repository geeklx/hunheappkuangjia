package com.sdzn.fzx.teacher.vo.me;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/12
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TellSetVo {
    /**
     * total : 4
     * data : [{"id":37,"code":"BAD_SURFACE","name":"粗糙卷面","value":"1","describe":null,"userTeacherId":17},{"id":38,"code":"GOOD_SURFAC","name":"优秀卷面","value":"1","describe":null,"userTeacherId":17},{"id":39,"code":"BAD_ANSWER","name":"典型错误","value":"1","describe":null,"userTeacherId":17},{"id":40,"code":"GOOD_ANSWER","name":"优秀作答","value":"1","describe":null,"userTeacherId":17}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 37
         * code : BAD_SURFACE
         * name : 粗糙卷面
         * value : 1
         * describe : null
         * userTeacherId : 17
         */

        private int id;
        private String code;
        private String name;
        private String value;
        private String describe;
        private int userTeacherId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescribe() {
            return describe;
        }

        public void setDescribe(String describe) {
            this.describe = describe;
        }

        public int getUserTeacherId() {
            return userTeacherId;
        }

        public void setUserTeacherId(int userTeacherId) {
            this.userTeacherId = userTeacherId;
        }
    }
}
