package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;

/**
 *
 */

public interface TakeOfficeViews extends IView {
    void setTakeOfficeData(TakeOfficeVo takeOfficeData);

    void onFailed(String msg);
}
