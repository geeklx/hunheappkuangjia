package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ClassContentBean;

public interface ResourcesViews extends IView {
    void onResourcesSuccess(String str);
    void onResourcesFail(String msg);
}
