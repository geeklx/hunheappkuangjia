package com.sdzn.fzx.teacher.vo;

import java.util.List;

public class TaskListVo {

    /**
     * total : 18
     * data : [{"id":543,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522219725000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522252800000,"endTime":1522339200000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":1,"countTeacherCorrect":0,"useTimeAvg":1720374,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"},{"id":539,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522217172000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522217172000,"endTime":1522252800000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":1,"countTeacherCorrect":0,"useTimeAvg":21,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"},{"id":524,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522216433000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522216433000,"endTime":1522339200000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":0,"countTeacherCorrect":0,"useTimeAvg":0,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 543
         * name : 哈哈哈
         * chapterId : 2145
         * chapterName : 预备篇
         * chapterNodeIdPath : 0.2145
         * chapterNodeNamePath : 预备篇
         * baseVolumeId : 10
         * baseVolumeName : 人教新目标版
         * baseSubjectId : 3
         * baseSubjectName : 英语老师
         * baseGradeId : 7
         * baseGradeName : 七年级
         * createUserId : 41
         * createUserName : 王春晓
         * createTime : 1522219725000
         * type : 1
         * typeReview : null
         * typeReviewName : null
         * classId : 22
         * className : 五班
         * startTime : 1522252800000
         * endTime : 1522339200000
         * countExam : 2
         * countRescoure : 1
         * score : 2
         * scoreAvg : 0
         * countStudentTotal : 5
         * countStudentSub : 1
         * countTeacherCorrect : 0
         * useTimeAvg : 1720374
         * answerViewType : 1
         * answerViewOpen : 0
         * lessonLibId : 237
         * lessonLibName : 哈哈哈
         * lessonId : 320
         * lessonName : 002
         * scope : 2017
         * status : 3
         * statusName : 已截止
         */

        private int id;
        private String name;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private int baseVolumeId;
        private String baseVolumeName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseGradeId;
        private String baseGradeName;
        private int createUserId;
        private String createUserName;
        private long createTime;
        private int type;
        private Object typeReview;
        private Object typeReviewName;
        private int classId;
        private String className;
        private long startTime;
        private long endTime;
        private int countExam;
        private int countRescoure;
        private double score;
        private double scoreAvg;
        private int countStudentTotal;
        private int countStudentSub;
        private int countTeacherCorrect;
        private double useTimeAvg;
        private int answerViewType;
        private int answerViewOpen;
        private int lessonLibId;
        private String lessonLibName;
        private int lessonId;
        private String lessonName;
        private int scope;
        private int status;
        private String statusName;
        private String teacherExamId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public Object getTypeReview() {
            return typeReview;
        }

        public void setTypeReview(Object typeReview) {
            this.typeReview = typeReview;
        }

        public Object getTypeReviewName() {
            return typeReviewName;
        }

        public void setTypeReviewName(Object typeReviewName) {
            this.typeReviewName = typeReviewName;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public int getCountExam() {
            return countExam;
        }

        public void setCountExam(int countExam) {
            this.countExam = countExam;
        }

        public int getCountRescoure() {
            return countRescoure;
        }

        public void setCountRescoure(int countRescoure) {
            this.countRescoure = countRescoure;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public double getScoreAvg() {
            return scoreAvg;
        }

        public void setScoreAvg(double scoreAvg) {
            this.scoreAvg = scoreAvg;
        }

        public int getCountStudentTotal() {
            return countStudentTotal;
        }

        public void setCountStudentTotal(int countStudentTotal) {
            this.countStudentTotal = countStudentTotal;
        }

        public int getCountStudentSub() {
            return countStudentSub;
        }

        public void setCountStudentSub(int countStudentSub) {
            this.countStudentSub = countStudentSub;
        }

        public int getCountTeacherCorrect() {
            return countTeacherCorrect;
        }

        public void setCountTeacherCorrect(int countTeacherCorrect) {
            this.countTeacherCorrect = countTeacherCorrect;
        }

        public double getUseTimeAvg() {
            return useTimeAvg;
        }

        public void setUseTimeAvg(double useTimeAvg) {
            this.useTimeAvg = useTimeAvg;
        }

        public int getAnswerViewType() {
            return answerViewType;
        }

        public void setAnswerViewType(int answerViewType) {
            this.answerViewType = answerViewType;
        }

        public int getAnswerViewOpen() {
            return answerViewOpen;
        }

        public void setAnswerViewOpen(int answerViewOpen) {
            this.answerViewOpen = answerViewOpen;
        }

        public int getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(int lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public String getLessonLibName() {
            return lessonLibName;
        }

        public void setLessonLibName(String lessonLibName) {
            this.lessonLibName = lessonLibName;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getStatusName() {
            return statusName;
        }

        public void setStatusName(String statusName) {
            this.statusName = statusName;
        }

        public String getTeacherExamId() {
            return teacherExamId;
        }

        public void setTeacherExamId(String teacherExamId) {
            this.teacherExamId = teacherExamId;
        }
    }
}
