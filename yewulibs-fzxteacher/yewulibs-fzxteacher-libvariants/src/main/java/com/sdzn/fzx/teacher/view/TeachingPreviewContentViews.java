package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ClassContentBean;
import com.sdzn.fzx.teacher.bean.ClassObjectivesBean;

public interface TeachingPreviewContentViews extends IView {

    void onCoursesOjectivesSuccess(ClassObjectivesBean objectivesBean);


}
