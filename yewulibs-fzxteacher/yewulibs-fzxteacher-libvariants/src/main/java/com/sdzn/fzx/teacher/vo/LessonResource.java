package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/5
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class LessonResource {
    /**
     * total : 1
     * data : [{"id":19,"resourceId":633,"resourceName":"AAAAAAAAAAAAA.jpg","resourceType":4,"resourceText":null,"flagSync":null,"seq":null,"lessonId":40,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","count":null}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 19
         * resourceId : 633
         * resourceName : AAAAAAAAAAAAA.jpg
         * resourceType : 4
         * resourceText : null
         * flagSync : null
         * seq : null
         * lessonId : 40
         * customerSchoolId : 2
         * customerSchoolName : 山东大学附属中学
         * count : null
         */

        private int id;
        private int resourceId;
        private String resourceName;
        private int resourceType;
        private String resourceText;
        private Object flagSync;
        private Object seq;
        private int lessonId;
        private int customerSchoolId;
        private String customerSchoolName;
        private String resourceSuffix;
        private Object count;

        public String getResourceSuffix() {
            return resourceSuffix;
        }

        public void setResourceSuffix(String resourceSuffix) {
            this.resourceSuffix = resourceSuffix;
        }

        public int getConvertStatus() {
            return convertStatus;
        }

        public void setConvertStatus(int convertStatus) {
            this.convertStatus = convertStatus;
        }

        private int convertStatus;
        private boolean isChecked = true;

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getResourceId() {
            return resourceId;
        }

        public void setResourceId(int resourceId) {
            this.resourceId = resourceId;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public String getResourceText() {
            return resourceText;
        }

        public void setResourceText(String resourceText) {
            this.resourceText = resourceText;
        }

        public Object getFlagSync() {
            return flagSync;
        }

        public void setFlagSync(Object flagSync) {
            this.flagSync = flagSync;
        }

        public Object getSeq() {
            return seq;
        }

        public void setSeq(Object seq) {
            this.seq = seq;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public Object getCount() {
            return count;
        }

        public void setCount(Object count) {
            this.count = count;
        }
    }
}
