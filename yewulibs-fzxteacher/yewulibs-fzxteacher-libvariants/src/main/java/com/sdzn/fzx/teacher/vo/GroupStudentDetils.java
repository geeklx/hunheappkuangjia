package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/21
 * 修改单号：
 * 修改内容:
 */
public class GroupStudentDetils {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 254
         * name : 测试2组
         * students : [{"id":625,"userStudentId":4599,"userStudentName":"测试18","status":0,"photo":null,"isCorrect":0,"scoreTotal":0,"useTime":0,"classGroupId":254,"classGroupName":"测试2组","correctType":3},{"id":626,"userStudentId":4600,"userStudentName":"测试19","status":0,"photo":null,"isCorrect":0,"scoreTotal":0,"useTime":0,"classGroupId":254,"classGroupName":"测试2组","correctType":3},{"id":627,"userStudentId":5289,"userStudentName":"nose","status":0,"photo":null,"isCorrect":0,"scoreTotal":0,"useTime":0,"classGroupId":254,"classGroupName":"测试2组","correctType":3}]
         */

        private int id;
        private String name;
        private List<StudentList.DataBean> students;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<StudentList.DataBean> getStudents() {
            return students;
        }

        public void setStudents(List<StudentList.DataBean> students) {
            this.students = students;
        }
    }
}
