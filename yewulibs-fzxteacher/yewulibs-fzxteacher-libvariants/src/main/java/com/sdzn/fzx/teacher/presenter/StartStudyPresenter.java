package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.StartStudyBean;
import com.sdzn.fzx.teacher.view.NowOutViews;
import com.sdzn.fzx.teacher.view.StartStudyViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -老师撤回
 */
public class StartStudyPresenter extends Presenter<StartStudyViews> {

    public void addStartStudy(String token, String classId,String lessonId,String subjectId) {
        JSONObject requestData = new JSONObject();
        requestData.put("classId", classId);
        requestData.put("lessonId", lessonId);
        requestData.put("subjectId", subjectId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .addStartStudy(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/classRoomStaty/startStudy", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<StartStudyBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<StartStudyBean>> call, Response<ResponseSlbBean1<StartStudyBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
//                        if (response.body().getCode() != 0) {
//                            getView().onStartStudyNodata(response.body().getMessage());
//                            return;
//                        }
                        getView().onStartStudySuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<StartStudyBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onStartStudyFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
