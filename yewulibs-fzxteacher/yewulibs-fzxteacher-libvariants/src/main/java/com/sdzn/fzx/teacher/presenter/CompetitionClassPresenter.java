package com.sdzn.fzx.teacher.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.CompetitionClassBean;
import com.sdzn.fzx.teacher.view.CompetitionClassViews;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -上课记录选择班级
 */
public class CompetitionClassPresenter extends Presenter<CompetitionClassViews> {

    public void queryCompetitionClass(String token, String teacherId) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryCompetitionClass(BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacher/classes", token, teacherId)
                .enqueue(new Callback<ResponseSlbBean1<List<CompetitionClassBean>>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<List<CompetitionClassBean>>> call, Response<ResponseSlbBean1<List<CompetitionClassBean>>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCompetitionClassNodata(response.body().getMessage());
                            return;
                        }
                        getView().onCompetitionClassSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<List<CompetitionClassBean>>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCompetitionClassFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
