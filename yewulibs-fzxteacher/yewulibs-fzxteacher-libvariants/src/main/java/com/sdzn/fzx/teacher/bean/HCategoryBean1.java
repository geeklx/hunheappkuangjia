package com.sdzn.fzx.teacher.bean;

import java.io.Serializable;
import java.util.List;

public class HCategoryBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;//
    private String name;//
    private int icon;//
    private String url;
    private List<OneBean2.ListBean> list;

    public List<OneBean2.ListBean> getList() {
        return list;
    }

    public void setList(List<OneBean2.ListBean> list) {
        this.list = list;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public HCategoryBean1() {
    }
    public HCategoryBean1(String code,String name) {
        this.code = code;
        this.name = name;
    }

    public HCategoryBean1(String code,String name, String url) {
        this.code = code;
        this.name = name;
        this.url = url;
    }
    public HCategoryBean1(String code, String name, String url, List<OneBean2.ListBean> list) {
        this.code = code;
        this.name = name;
        this.url = url;
        this.list = list;
    }

    public HCategoryBean1(String code, String name, int icon) {
        this.code = code;
        this.name = name;
        this.icon = icon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
