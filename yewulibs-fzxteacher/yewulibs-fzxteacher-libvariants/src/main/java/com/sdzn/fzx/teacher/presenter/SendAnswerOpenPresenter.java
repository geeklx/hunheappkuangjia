package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.view.NowOutViews;
import com.sdzn.fzx.teacher.view.SendAnswerOpenViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -老师发布答案
 */
public class SendAnswerOpenPresenter extends Presenter<SendAnswerOpenViews> {

    public void addSendAnswerOpen(String token, String lessonTaskCourseId) {
        JSONObject requestData = new JSONObject();
        requestData.put("lessonTaskCourseId", lessonTaskCourseId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .addSendAnswerOpen(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/ClassRoomTesting/sendAnswerOpen", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onSendAnswerOpenNodata(response.body().getMessage());
                            return;
                        }
                        getView().onSendAnswerOpenSuccess(response.body().getMessage());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onSendAnswerOpenFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
