package com.sdzn.fzx.teacher.vo;

import java.util.List;

public class GroupVo {

    /**
     * total : 2
     * data : [{"id":null,"classId":12,"className":"一班","classGroupId":22,"classGroupName":"敬业组","createAccountId":null,"createTime":null,"createUserName":null,"points":4,"type":1,"subjectId":1,"subjectName":"语文","pointsCount":3},{"id":null,"classId":12,"className":"一班","classGroupId":24,"classGroupName":"友善组","createAccountId":null,"createTime":null,"createUserName":null,"points":7,"type":1,"subjectId":1,"subjectName":"语文","pointsCount":6}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : null
         * classId : 12
         * className : 一班
         * classGroupId : 22
         * classGroupName : 敬业组
         * createAccountId : null
         * createTime : null
         * createUserName : null
         * points : 4
         * type : 1
         * subjectId : 1
         * subjectName : 语文
         * pointsCount : 3
         */

        private Object id;
        private int classId;
        private String className;
        private int classGroupId;
        private String classGroupName;
        private Object createAccountId;
        private Object createTime;
        private Object createUserName;
        private int points;
        private int type;
        private int subjectId;
        private String subjectName;
        private int pointsCount;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(int classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public Object getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(Object createAccountId) {
            this.createAccountId = createAccountId;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(Object createUserName) {
            this.createUserName = createUserName;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public int getPointsCount() {
            return pointsCount;
        }

        public void setPointsCount(int pointsCount) {
            this.pointsCount = pointsCount;
        }
    }
}
