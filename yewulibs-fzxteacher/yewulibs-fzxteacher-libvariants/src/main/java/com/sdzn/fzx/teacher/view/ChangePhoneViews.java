package com.sdzn.fzx.teacher.view;


import com.haier.cellarette.libmvp.mvp.IView;

import java.io.InputStream;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface ChangePhoneViews extends IView {


    void OnVerityCodeSuccess(Object success);
    void OnVerityCodeFailed(String msg);

    void OnChangPhoneSuccess(String success);

    void OnImgTokenSuccess(Object imgToken);

    void OnImgSuccess(InputStream img);

    void onChangPhoneFailed(String msg);

    void onImgTokenFailed(String msg);

    void onImgFailed(String msg);

}
