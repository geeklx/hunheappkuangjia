package com.sdzn.fzx.teacher.bean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/8/11
 */
public class JstokenUserinfoBean {
    private String Token;
    private String Userinfo;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getUserinfo() {
        return Userinfo;
    }

    public void setUserinfo(String userinfo) {
        Userinfo = userinfo;
    }
}
