package com.sdzn.fzx.teacher.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.TeacherVolumeBean;
import com.sdzn.fzx.teacher.bean.TreeBean;
import com.sdzn.fzx.teacher.view.TeacherVolumeViews;
import com.sdzn.fzx.teacher.view.TreeViews;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -老师册别
 */
public class TeacherVolumePresenter extends Presenter<TeacherVolumeViews> {

    public void queryTeacherVolume(String token,String openid) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryTeacherVolume(BuildConfig3.SERVER_ISERVICE_NEW2 + "/usercenter/teacherBookVersion/list", token, openid)
                .enqueue(new Callback<ResponseSlbBean1<TeacherVolumeBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<TeacherVolumeBean>> call, Response<ResponseSlbBean1<TeacherVolumeBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onTeacherVolumeNodata(response.body().getMessage());
                            return;
                        }
                        getView().onTeacherVolumeSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<TeacherVolumeBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onTeacherVolumeFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
