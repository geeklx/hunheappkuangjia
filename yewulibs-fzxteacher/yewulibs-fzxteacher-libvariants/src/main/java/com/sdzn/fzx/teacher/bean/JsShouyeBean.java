package com.sdzn.fzx.teacher.bean;

import com.sdzn.fzx.teacher.vo.LoginBean;

public class JsShouyeBean {
    private LoginBean.DataBean.UserBean content;
    private Object datatype;
    private String datetime;

    public LoginBean.DataBean.UserBean getContent() {
        return content;
    }

    public void setContent(LoginBean.DataBean.UserBean content) {
        this.content = content;
    }

    public Object getDatatype() {
        return datatype;
    }

    public void setDatatype(Object datatype) {
        this.datatype = datatype;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
