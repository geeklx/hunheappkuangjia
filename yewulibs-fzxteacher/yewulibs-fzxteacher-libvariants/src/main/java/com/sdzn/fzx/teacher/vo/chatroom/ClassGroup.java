package com.sdzn.fzx.teacher.vo.chatroom;

public class ClassGroup {
    private String mClassId;
    private String mGroupId;

    public ClassGroup(String mClassId, String mGroupId) {
        this.mClassId = mClassId;
        this.mGroupId = mGroupId;
    }

    public String getmClassId() {
        return mClassId;
    }

    public void setmClassId(String mClassId) {
        this.mClassId = mClassId;
    }

    public String getmGroupId() {
        return mGroupId;
    }

    public void setmGroupId(String mGroupId) {
        this.mGroupId = mGroupId;
    }
}
