package com.sdzn.fzx.teacher.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
@Entity
public class SearchHistory {
    @Id
    private Long id;
    private String searchStr;
    private Date date;
    private int subject;
    private String scene;

    @Generated(hash = 459623993)
    public SearchHistory(Long id, String searchStr, Date date, int subject,
                         String scene) {
        this.id = id;
        this.searchStr = searchStr;
        this.date = date;
        this.subject = subject;
        this.scene = scene;
    }

    @Generated(hash = 1905904755)
    public SearchHistory() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSearchStr() {
        return this.searchStr;
    }

    public void setSearchStr(String searchStr) {
        this.searchStr = searchStr;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getSubject() {
        return this.subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public String getScene() {
        return this.scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }
}
