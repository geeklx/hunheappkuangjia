package com.sdzn.fzx.teacher.vo;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/20a
 * 修改单号：
 * 修改内容:
 */
public class CorrectingWayVo implements Serializable {
    /**
     * groupName : 未分组
     * groupId : 0
     * students : [{"classId":393,"className":null,"studentId":7049,"studentName":"fukyl","groupId":0,"groupName":"未分组","accountId":8655,"accountName":"71345680"},{"classId":393,"className":null,"studentId":7050,"studentName":"gukyl","groupId":0,"groupName":"未分组","accountId":8656,"accountName":"04983125"},{"classId":393,"className":null,"studentId":7055,"studentName":"nukyl","groupId":0,"groupName":"未分组","accountId":8661,"accountName":"93701564"},{"classId":393,"className":null,"studentId":7056,"studentName":"liukyl","groupId":0,"groupName":"未分组","accountId":8662,"accountName":"08795362"},{"classId":393,"className":null,"studentId":7057,"studentName":"laukyl","groupId":0,"groupName":"未分组","accountId":8663,"accountName":"81390452"},{"classId":393,"className":null,"studentId":7058,"studentName":"lbkyl","groupId":0,"groupName":"未分组","accountId":8664,"accountName":"24136057"},{"classId":393,"className":null,"studentId":7059,"studentName":"lckyl","groupId":0,"groupName":"未分组","accountId":8665,"accountName":"72846503"},{"classId":393,"className":null,"studentId":7060,"studentName":"lekyl","groupId":0,"groupName":"未分组","accountId":8666,"accountName":"28954310"},{"classId":393,"className":null,"studentId":7061,"studentName":"lfkyl","groupId":0,"groupName":"未分组","accountId":8667,"accountName":"19738204"},{"classId":393,"className":null,"studentId":7062,"studentName":"lgkyl","groupId":0,"groupName":"未分组","accountId":8668,"accountName":"86710439"},{"classId":393,"className":null,"studentId":7063,"studentName":"lhkyl","groupId":0,"groupName":"未分组","accountId":8669,"accountName":"59047631"},{"classId":393,"className":null,"studentId":7064,"studentName":"likyl","groupId":0,"groupName":"未分组","accountId":8670,"accountName":"27105864"},{"classId":393,"className":null,"studentId":7065,"studentName":"lykyl","groupId":0,"groupName":"未分组","accountId":8671,"accountName":"98035471"},{"classId":393,"className":null,"studentId":7066,"studentName":"lwkyl","groupId":0,"groupName":"未分组","accountId":8672,"accountName":"13709528"},{"classId":393,"className":null,"studentId":7067,"studentName":"lnkyl","groupId":0,"groupName":"未分组","accountId":8673,"accountName":"84179256"},{"classId":393,"className":null,"studentId":7300,"studentName":"apple","groupId":0,"groupName":"未分组","accountId":8923,"accountName":"66040161"},{"classId":393,"className":null,"studentId":7301,"studentName":"orange","groupId":0,"groupName":"未分组","accountId":8924,"accountName":"34987800"},{"classId":393,"className":null,"studentId":7302,"studentName":"paper","groupId":0,"groupName":"未分组","accountId":8925,"accountName":"40119715"},{"classId":393,"className":null,"studentId":7303,"studentName":"duk","groupId":0,"groupName":"未分组","accountId":8926,"accountName":"87750349"},{"classId":393,"className":null,"studentId":7304,"studentName":"hua","groupId":0,"groupName":"未分组","accountId":8927,"accountName":"78097591"},{"classId":393,"className":null,"studentId":7305,"studentName":"huahua","groupId":0,"groupName":"未分组","accountId":8928,"accountName":"88324416"}]
     */

    private String groupName;
    private int groupId;
    private List<StudentsBean> students;

    public CorrectingWayVo(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public List<StudentsBean> getStudents() {
        return students;
    }

    public void setStudents(List<StudentsBean> students) {
        this.students = students;
    }

    public void addstudents(StudentsBean studentsBean) {
        if (students == null)
            students = new ArrayList<>();
        students.add(studentsBean);
    }
    //返回 参数 在当分组中的位置,对象
    public Map<Integer, StudentsBean> getStatusBySelect() {
        if (students == null) {
            return null;
        }
        Map<Integer, StudentsBean> integerCorrectingWayVoHashMap = null;
        for (int i = 0; i < students.size(); i++/**/) {
            StudentsBean studentsBean = students.get(i);
            if (studentsBean.isStatus()) {
                if (integerCorrectingWayVoHashMap == null) {
                    integerCorrectingWayVoHashMap = new HashMap();
                }
                integerCorrectingWayVoHashMap.put(i, studentsBean);
            }
        }
        return integerCorrectingWayVoHashMap;
    }

    public static class StudentsBean implements Serializable {
        /**
         * classId : 393
         * className : null
         * studentId : 7049
         * studentName : fukyl
         * groupId : 0
         * groupName : 未分组
         * accountId : 8655
         * accountName : 71345680
         */

        private int classId;
        private Object className;
        private int studentId;
        private String studentName;
        private int groupId;
        private String groupName;
        private int accountId;
        private String accountName;
        private boolean status;
        private int modificationtime;

        public int getModificationtime() {
            return modificationtime;
        }

        public void setModificationtime(int modificationtime) {
            this.modificationtime = modificationtime;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public StudentsBean(String studentName) {
            this.studentName = studentName;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public Object getClassName() {
            return className;
        }

        public void setClassName(Object className) {
            this.className = className;
        }

        public int getStudentId() {
            return studentId;
        }

        public void setStudentId(int studentId) {
            this.studentId = studentId;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getAccountId() {
            return accountId;
        }

        public void setAccountId(int accountId) {
            this.accountId = accountId;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }
    }
}