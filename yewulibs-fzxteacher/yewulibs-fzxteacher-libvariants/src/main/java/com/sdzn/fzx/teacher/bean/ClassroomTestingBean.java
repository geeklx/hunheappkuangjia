package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class ClassroomTestingBean {

    /**
     * rows : [{"lessonId":452,"lessonLibId":901,"lessonTaskId":1883,"name":"春","startTime":"2020-12-08 14:07:16","endTime":"2020-12-08 14:32:16","status":"3","classId":112,"className":"一班","countExam":14,"allNum":10,"alreadyNum":17,"notNum":-7,"answerViewType":2,"answerViewOpen":1,"isHide":0,"id":2145},{"lessonId":452,"lessonLibId":901,"lessonTaskId":1883,"name":"春","startTime":"2020-12-08 14:07:15","endTime":"2020-12-08 14:32:15","status":"3","classId":112,"className":"一班","countExam":14,"allNum":10,"alreadyNum":2,"notNum":8,"answerViewType":2,"answerViewOpen":2,"isHide":0,"id":2144},{"lessonId":452,"lessonLibId":901,"lessonTaskId":1883,"name":"春","startTime":"2020-12-08 13:53:26","endTime":"2020-12-08 13:58:26","status":"3","classId":112,"className":"一班","countExam":14,"allNum":10,"alreadyNum":4,"notNum":6,"answerViewType":2,"answerViewOpen":2,"isHide":0,"id":2142},{"lessonId":441,"lessonLibId":879,"lessonTaskId":1854,"name":"春","startTime":"2020-12-04 17:29:23","endTime":"2020-12-04 17:39:23","status":"3","classId":112,"className":"一班","countExam":3,"allNum":10,"alreadyNum":2,"notNum":8,"answerViewType":2,"answerViewOpen":2,"isHide":0,"id":2119},{"lessonId":440,"lessonLibId":877,"lessonTaskId":1835,"name":"春","startTime":"2020-12-04 16:16:57","endTime":"2020-12-04 17:56:57","status":"3","classId":112,"className":"一班","countExam":2,"allNum":10,"alreadyNum":0,"notNum":10,"answerViewType":1,"answerViewOpen":1,"isHide":0,"id":2104}]
     * total : 5
     */

    private int total;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * lessonId : 452
         * lessonLibId : 901
         * lessonTaskId : 1883
         * name : 春
         * startTime : 2020-12-08 14:07:16
         * endTime : 2020-12-08 14:32:16
         * status : 3
         * classId : 112
         * className : 一班
         * countExam : 14
         * allNum : 10
         * alreadyNum : 17
         * notNum : -7
         * answerViewType : 2
         * answerViewOpen : 1
         * isHide : 0
         * id : 2145
         */

        private String lessonId;
        private String lessonLibId;
        private String lessonTaskId;
        private String name;
        private String startTime;
        private String endTime;
        private String status;
        private String classId;
        private String className;
        private String countExam;
        private String allNum;
        private String alreadyNum;
        private int notNum;
        private String answerViewType;
        private int answerViewOpen;
        private int isHide;
        private String id;

        public String getLessonId() {
            return lessonId;
        }

        public void setLessonId(String lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(String lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public String getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(String lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getCountExam() {
            return countExam;
        }

        public void setCountExam(String countExam) {
            this.countExam = countExam;
        }

        public String getAllNum() {
            return allNum;
        }

        public void setAllNum(String allNum) {
            this.allNum = allNum;
        }

        public String getAlreadyNum() {
            return alreadyNum;
        }

        public void setAlreadyNum(String alreadyNum) {
            this.alreadyNum = alreadyNum;
        }

        public int getNotNum() {
            return notNum;
        }

        public void setNotNum(int notNum) {
            this.notNum = notNum;
        }

        public String getAnswerViewType() {
            return answerViewType;
        }

        public void setAnswerViewType(String answerViewType) {
            this.answerViewType = answerViewType;
        }

        public int getAnswerViewOpen() {
            return answerViewOpen;
        }

        public void setAnswerViewOpen(int answerViewOpen) {
            this.answerViewOpen = answerViewOpen;
        }

        public int getIsHide() {
            return isHide;
        }

        public void setIsHide(int isHide) {
            this.isHide = isHide;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}