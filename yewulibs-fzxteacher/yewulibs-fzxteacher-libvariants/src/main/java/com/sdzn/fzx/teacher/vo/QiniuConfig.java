package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/1
 * 修改单号：
 * 修改内容:
 */
public class QiniuConfig {

    /**
     * data : {"id":1,"keyCode":null,"keyValue":null,"accessKey":null,"secretKey":null,"bucket":"fuzhuxian","domian":"http://file.fuzhuxian.com","expires":360,"imageStyle":"imageView2/5/w/200/h/200/q/50"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * keyCode : null
         * keyValue : null
         * accessKey : null
         * secretKey : null
         * bucket : fuzhuxian
         * domian : http://file.fuzhuxian.com
         * expires : 360
         * imageStyle : imageView2/5/w/200/h/200/q/50
         */

        private int id;
        private Object keyCode;
        private Object keyValue;
        private Object accessKey;
        private Object secretKey;
        private String bucket;
        private String domian;
        private int expires;
        private String imageStyle;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getKeyCode() {
            return keyCode;
        }

        public void setKeyCode(Object keyCode) {
            this.keyCode = keyCode;
        }

        public Object getKeyValue() {
            return keyValue;
        }

        public void setKeyValue(Object keyValue) {
            this.keyValue = keyValue;
        }

        public Object getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(Object accessKey) {
            this.accessKey = accessKey;
        }

        public Object getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(Object secretKey) {
            this.secretKey = secretKey;
        }

        public String getBucket() {
            return bucket;
        }

        public void setBucket(String bucket) {
            this.bucket = bucket;
        }

        public String getDomian() {
            return domian;
        }

        public void setDomian(String domian) {
            this.domian = domian;
        }

        public int getExpires() {
            return expires;
        }

        public void setExpires(int expires) {
            this.expires = expires;
        }

        public String getImageStyle() {
            return imageStyle;
        }

        public void setImageStyle(String imageStyle) {
            this.imageStyle = imageStyle;
        }
    }
}
