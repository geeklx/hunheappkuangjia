package com.sdzn.fzx.teacher.vo.chatroom;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 课程包下
 */
public class GroupChatBean {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * id : 12
         * chatTitle : 版本
         * chatContent : 版本
         * createUserId : 216
         * lessonId : 0
         * subjectId : 1
         * createTime : 2019-08-20T04:42:08.000+0000
         * endTime : 2019-08-20T04:42:08.000+0000
         *
         *
         */

        private int id;
        private String chatTitle;
        private String chatContent;
        private int createUserId;
        private String lessonId;
        private String subjectId;
        private String createTime;
        private String endTime;

        private List<TeachGroupChatContentPicsBean> teachGroupChatContentPics;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getChatTitle() {
            return chatTitle;
        }

        public void setChatTitle(String chatTitle) {
            this.chatTitle = chatTitle;
        }

        public String getChatContent() {
            return chatContent;
        }

        public void setChatContent(String chatContent) {
            this.chatContent = chatContent;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getLessonId() {
            return lessonId;
        }

        public void setLessonId(String lessonId) {
            this.lessonId = lessonId;
        }

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.chatTitle);
            dest.writeString(this.chatContent);
            dest.writeInt(this.createUserId);
            dest.writeString(this.lessonId);
            dest.writeString(this.subjectId);
            dest.writeString(this.createTime);
            dest.writeString(this.endTime);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.id = in.readInt();
            this.chatTitle = in.readString();
            this.chatContent = in.readString();
            this.createUserId = in.readInt();
            this.lessonId = in.readString();
            this.subjectId = in.readString();
            this.createTime = in.readString();
            this.endTime = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };


        public List<TeachGroupChatContentPicsBean> getTeachGroupChatContentPics() {
            return teachGroupChatContentPics;
        }

        public void setTeachGroupChatContentPics(List<TeachGroupChatContentPicsBean> teachGroupChatContentPics) {
            this.teachGroupChatContentPics = teachGroupChatContentPics;
        }

        public static class TeachGroupChatContentPicsBean implements Parcelable {
            /**
             * id : 18
             * picUrl : http://csfile.fuzhuxian.com/ad38d4a8-db4a-4ee7-95c4-ea16e131f25d.jpg
             * groupChatLibId : 13
             * groupChatTaskId : null
             * createTime : 2019-08-24T09:37:55.000+0000
             */

            private int id;
            private String picUrl;
            private int groupChatLibId;
            private String groupChatTaskId;
            private String createTime;

            public TeachGroupChatContentPicsBean(String picUrl) {
                this.picUrl = picUrl;

            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public int getGroupChatLibId() {
                return groupChatLibId;
            }

            public void setGroupChatLibId(int groupChatLibId) {
                this.groupChatLibId = groupChatLibId;
            }

            public String getGroupChatTaskId() {
                return groupChatTaskId;
            }

            public void setGroupChatTaskId(String groupChatTaskId) {
                this.groupChatTaskId = groupChatTaskId;
            }


            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.id);
                dest.writeString(this.picUrl);
                dest.writeInt(this.groupChatLibId);
                dest.writeString(this.groupChatTaskId);
                dest.writeString(this.createTime);
            }

            public TeachGroupChatContentPicsBean() {
            }


            protected TeachGroupChatContentPicsBean(Parcel in) {
                this.id = in.readInt();
                this.picUrl = in.readString();
                this.groupChatLibId = in.readInt();
                this.groupChatTaskId = in.readString();
                this.createTime = in.readString();
            }

            public static final Creator<TeachGroupChatContentPicsBean> CREATOR = new Creator<TeachGroupChatContentPicsBean>() {
                @Override
                public TeachGroupChatContentPicsBean createFromParcel(Parcel source) {
                    return new TeachGroupChatContentPicsBean(source);
                }

                @Override
                public TeachGroupChatContentPicsBean[] newArray(int size) {
                    return new TeachGroupChatContentPicsBean[size];
                }
            };
        }

    }
}
