package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class TeacherVolumeBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * subjectId : 2
         * subjectName : 语文
         * bookVolumeRes : [{"id":"244","bookVolumeId":556,"bookVersionId":10228,"name":"人教版（2016）-七年级上册"},{"id":"245","bookVolumeId":899,"bookVersionId":10228,"name":"人教版（2016）-七年级下册"},{"id":"246","bookVolumeId":1218,"bookVersionId":10228,"name":"人教版（2016）-九年级上册"},{"id":"247","bookVolumeId":1013,"bookVersionId":10228,"name":"人教版（2016）-八年级上册"},{"id":"248","bookVolumeId":1323,"bookVersionId":10228,"name":"人教版（2016）-九年级下册"},{"id":"249","bookVolumeId":1144,"bookVersionId":10228,"name":"人教版（2016）-八年级下册"}]
         */

        private String subjectId;
        private String subjectName;
        private List<BookVolumeResBean> bookVolumeRes;

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public List<BookVolumeResBean> getBookVolumeRes() {
            return bookVolumeRes;
        }

        public void setBookVolumeRes(List<BookVolumeResBean> bookVolumeRes) {
            this.bookVolumeRes = bookVolumeRes;
        }

        public static class BookVolumeResBean {
            /**
             * id : 244
             * bookVolumeId : 556
             * bookVersionId : 10228
             * name : 人教版（2016）-七年级上册
             */

            private String id;
            private String bookVolumeId;
            private String bookVersionId;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getBookVolumeId() {
                return bookVolumeId;
            }

            public void setBookVolumeId(String bookVolumeId) {
                this.bookVolumeId = bookVolumeId;
            }

            public String getBookVersionId() {
                return bookVersionId;
            }

            public void setBookVersionId(String bookVersionId) {
                this.bookVersionId = bookVersionId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
