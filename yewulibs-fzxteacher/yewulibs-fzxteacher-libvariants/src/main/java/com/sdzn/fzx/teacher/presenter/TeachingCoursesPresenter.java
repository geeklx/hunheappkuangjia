package com.sdzn.fzx.teacher.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.TeachingCoursesBean;
import com.sdzn.fzx.teacher.view.TeachingCoursesViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -课堂教学列表
 */
public class TeachingCoursesPresenter extends Presenter<TeachingCoursesViews> {

    public void queryTeachingCourses(String token,String chapterId,int page,int limit) {
        JSONObject requestData = new JSONObject();
        requestData.put("chapterId", chapterId);
        requestData.put("page", page);
        requestData.put("limit", limit);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());


        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryTeachingCourses(BuildConfig3.SERVER_ISERVICE_NEW2 + "/course/api/classRoomStaty/selectClassLibListByKointId", token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<TeachingCoursesBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<TeachingCoursesBean>> call, Response<ResponseSlbBean1<TeachingCoursesBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onTeachingCoursesNodata(response.body().getMessage());
                            return;
                        }
                        getView().onTeachingCoursesSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<TeachingCoursesBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onTeachingCoursesFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });

//        RetrofitNetNew.build(Api.class, getIdentifier())
//                .queryTree(token, requestBody)
//                .enqueue(new Callback<ResponseSlbBean1<List<TreeBean>>>() {
//                    @Override
//                    public void onResponse(Call<ResponseSlbBean1<List<TreeBean>>> call, Response<ResponseSlbBean1<List<TreeBean>>> response) {
//                        if (!hasView()) {
//                            return;
//                        }
//                        if (response.body() == null) {
//                            return;
//                        }
//                        if (response.body().getCode() != 0) {
//                            getView().onCehuaNodata(response.body().getMessage());
//                            return;
//                        }
//                        getView().onCehuaSuccess(response.body().getResult());
//                        call.cancel();
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseSlbBean1<List<TreeBean>>> call, Throwable t) {
//                        if (!hasView()) {
//                            return;
//                        }
//                        String string = BanbenUtils.getInstance().error_tips;
//                        getView().onCehuaFail(string);
//                        t.printStackTrace();
//                        call.cancel();
//                    }
//                });
    }
}
