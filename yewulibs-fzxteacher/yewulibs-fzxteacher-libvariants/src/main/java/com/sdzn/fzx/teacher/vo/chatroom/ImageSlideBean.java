package com.sdzn.fzx.teacher.vo.chatroom;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageSlideBean implements Parcelable {
    String mPic;
    String mGroupId;
    int id;

    public ImageSlideBean(int id, String mPic, String mGroupId) {
        this.mPic = mPic;
        this.mGroupId = mGroupId;
        this.id = id;
    }

    public String getmPic() {
        return mPic;
    }

    public void setmPic(String mPic) {
        this.mPic = mPic;
    }

    public String getmGroupId() {
        return mGroupId;
    }

    public void setmGroupId(String mGroupId) {
        this.mGroupId = mGroupId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPic);
        dest.writeString(this.mGroupId);
        dest.writeInt(this.id);
    }

    protected ImageSlideBean(Parcel in) {
        this.mPic = in.readString();
        this.mGroupId = in.readString();
        this.id = in.readInt();
    }

    public static final Creator<ImageSlideBean> CREATOR = new Creator<ImageSlideBean>() {
        @Override
        public ImageSlideBean createFromParcel(Parcel source) {
            return new ImageSlideBean(source);
        }

        @Override
        public ImageSlideBean[] newArray(int size) {
            return new ImageSlideBean[size];
        }
    };
}
