package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-20
 */
public class CharactersListVo {
    private List<String> data;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
