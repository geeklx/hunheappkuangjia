package com.example.app5mykc.view.lesson;

import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.LessonResource;
import com.sdzn.fzx.teacher.vo.PrepareLessonsVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/28
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface LessonDetailsViwe extends BaseView {
    public void onClassSuccessed(SyncClassVo classVo);

    public void onPrepareLessonsVoSuccessed(PrepareLessonsVo prepareLessonsVo);

    public void onLessonResourceSuccessed(LessonResource lessonResource);

    public void onSyncTaskVoSuccessed(SyncTaskVo syncTaskVo);

    public void onDelSuccessed();

    public void onDelFailed();

    void onGroupChatSuccessed(GroupChatBean chatBean);

    void onRefresh();
}
