package com.example.app5mykc.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/5
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class PublishAdapter extends BaseAdapter {

    private List<SyncClassVo.DataBean> list;
    private Context context;
    private boolean flag = true;

    public PublishAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<SyncClassVo.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        final SyncClassVo.DataBean dataBean = list.get(i);
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_pub_gridview_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        if (!TextUtils.isEmpty(dataBean.getClassName())) {
            holder.viewName.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
        } else {
            holder.viewName.setText("");
        }

        holder.check.setEnabled(dataBean.isChecked());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataBean.setChecked(!dataBean.isChecked());
                notifyDataSetChanged();
            }
        });
        return view;
    }

    public List<SyncClassVo.DataBean> getCheckedList() {

        List<SyncClassVo.DataBean> retList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isChecked())
                retList.add(list.get(i));
        }
        return retList;
    }


    static class ViewHolder {
        ImageView check;
        TextView viewName;
        LinearLayout relativeLayout;

        ViewHolder(View view) {
            check = view.findViewById(R.id.check);
            viewName = view.findViewById(R.id.viewName);
            relativeLayout = view.findViewById(R.id.all);
        }
    }
}
