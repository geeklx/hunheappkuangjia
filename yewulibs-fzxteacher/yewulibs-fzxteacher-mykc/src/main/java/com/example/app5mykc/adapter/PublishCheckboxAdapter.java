package com.example.app5mykc.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.chatroom.DiscussionClassGroup;
import com.example.app5libbase.listener.chatroom.OnItemPublishListener;

import java.util.ArrayList;
import java.util.List;

/**
 * zs
 */

public class PublishCheckboxAdapter extends BaseAdapter {

    private List<DiscussionClassGroup.DataB> list;
    private Context context;
    private boolean flag = true;
    private OnItemPublishListener listener;

    public PublishCheckboxAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<DiscussionClassGroup.DataB> list, OnItemPublishListener listener) {
        this.list = list;
        this.listener = listener;
        notifyDataSetChanged();
    }

    public void setCheckedIsTrue(boolean checkedIsTrue) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, new DiscussionClassGroup.DataB(list.get(i).getGroupId(), list.get(i).getGroupName(), checkedIsTrue));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        final DiscussionClassGroup.DataB dataBean = list.get(i);
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.chat_check_publish_gridview_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        if (!TextUtils.isEmpty(dataBean.getGroupName())) {
            holder.viewName.setText(dataBean.getGroupName());
        } else {
            holder.viewName.setText("");
        }

        holder.check.setEnabled(dataBean.isCheck());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataBean.setCheck(!dataBean.isCheck());
                notifyDataSetChanged();

                listener.setOnCheckedListener(getCheckedList().size());
            }
        });
        return view;
    }


    public List<DiscussionClassGroup.DataB> getCheckedList() {

        List<DiscussionClassGroup.DataB> retList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isCheck()) {
                retList.add(list.get(i));
            }
        }
        return retList;
    }


    static class ViewHolder {
        ImageView check;
        TextView viewName;
        LinearLayout relativeLayout;

        ViewHolder(View view) {
            check = view.findViewById(R.id.check);
            viewName = view.findViewById(R.id.viewName);
            relativeLayout = view.findViewById(R.id.all);
        }
    }

}
