package com.example.app5mykc.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5mykc.adapter.PublishAdapter;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.datefive.CustomDatePicker;
import com.example.app5libbase.views.NoScrollGridView;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PublishActivity extends Activity implements View.OnClickListener {
    private RelativeLayout activityPublish;
    private TextView publishTv;
    private NoScrollGridView publishObject;
    private RadioButton beforeTv;
    private RadioButton inTv;
    private RadioButton afterTv;
    private RelativeLayout quickPublish;
    private ImageView quickImg;
    private RelativeLayout timingPublish;
    private ImageView timingImg;
    private TextView dateChoose;
    private TextView dateChooseTxt;
    private RadioButton personageTv;
    private RadioButton abortTv;
    private RadioButton teacherTv;
    private LinearLayout cancel;
    private LinearLayout confirm;


    private PublishAdapter publishAdapter;
    private Map<Integer, Object> map;
    private String lessonLibId;
    private String lessonLibName;
    private String name;
    private CustomDatePicker customDatePicker2, customDatePicker1;


    private String releaseTime = "";
    private String endTime = "";
    private String answerViewType;
    private String type = "1";
    private String sceneId, sceneName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);
        activityPublish = (RelativeLayout) findViewById(R.id.activity_publish);
        publishTv = (TextView) findViewById(R.id.publish_tv);
        publishObject = (NoScrollGridView) findViewById(R.id.publish_object);
        beforeTv = (RadioButton) findViewById(R.id.before_tv);
        inTv = (RadioButton) findViewById(R.id.in_tv);
        afterTv = (RadioButton) findViewById(R.id.after_tv);
        quickPublish = (RelativeLayout) findViewById(R.id.quick_publish);
        quickImg = (ImageView) findViewById(R.id.quick_img);
        timingPublish = (RelativeLayout) findViewById(R.id.timing_publish);
        timingImg = (ImageView) findViewById(R.id.timing_img);
        dateChoose = (TextView) findViewById(R.id.date_choose);
        dateChooseTxt = (TextView) findViewById(R.id.date_choose_txt);
        personageTv = (RadioButton) findViewById(R.id.personage_tv);
        abortTv = (RadioButton) findViewById(R.id.abort_tv);
        teacherTv = (RadioButton) findViewById(R.id.teacher_tv);
        cancel = (LinearLayout) findViewById(R.id.cancel);
        confirm = (LinearLayout) findViewById(R.id.confirm);
        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);
        quickPublish.setOnClickListener(this);
        timingPublish.setOnClickListener(this);
        dateChooseTxt.setOnClickListener(this);
        dateChoose.setOnClickListener(this);

        initData();
        lessonLibId = getIntent().getStringExtra("lessonLibId");
        lessonLibName = getIntent().getStringExtra("lessonLibName");
        name = getIntent().getStringExtra("name");
        publishTv.setText(lessonLibName);
        show();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PublishActivity成功");
                }
            }
        }
    }


    protected void initData() {
        publishAdapter = new PublishAdapter(PublishActivity.this);
        publishObject.setAdapter(publishAdapter);

        map = new HashMap<>();
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getClassList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        publishAdapter.setList(o.getData());
                    }
                });


    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.cancel) {
            finish();
        } else if (id == R.id.date_choose_txt) {
            if (dateChooseTxt.getText().toString().isEmpty()) {
                customDatePicker2.show(sdf.format(new Date()));
            } else {
                customDatePicker2.show(dateChooseTxt.getText().toString());
            }
        } else if (id == R.id.date_choose) {
            if (dateChoose.getText().toString().isEmpty()) {
                customDatePicker1.show(sdf.format(new Date()));
            } else {
                customDatePicker1.show(dateChoose.getText().toString());
            }
        } else if (id == R.id.confirm) {
            buileParams();
        } else if (id == R.id.quick_publish) {
            type = "1";
            quickImg.setVisibility(View.VISIBLE);
            timingImg.setVisibility(View.GONE);
            dateChoose.setVisibility(View.GONE);
            quickPublish.setBackground(getResources().getDrawable(R.drawable.shape_resouser_item));
            timingPublish.setBackground(getResources().getDrawable(R.drawable.shape_resouser_hui));
        } else if (id == R.id.timing_publish) {
            type = "2";
            timingImg.setVisibility(View.VISIBLE);
            dateChoose.setVisibility(View.VISIBLE);
            quickImg.setVisibility(View.GONE);
            timingPublish.setBackground(getResources().getDrawable(R.drawable.shape_resouser_item));
            quickPublish.setBackground(getResources().getDrawable(R.drawable.shape_resouser_hui));
        }
    }

    private void confirmMap(Map<String, Object> map) {
        Network.createTokenService(NetWorkService.releaseSaveService.class)
                .releaseSave(map)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getStatus().getCode() == 21706) {
                                ToastUtil.showLonglToast("截止时间小于发布时间");
                                return;
                            }
                            if (((ApiException) e).getStatus().getCode() == 21707) {
                                ToastUtil.showLonglToast("该活动下试题或资源不存在，发布失败！");
                                return;
                            }

                            if (((ApiException) e).getStatus().getCode() == 21708) {
                                ToastUtil.showLonglToast("该活动下存在未转码成功的资源，发布失败！");
                                return;
                            }
                            if (((ApiException) e).getStatus().getCode() == 20001) {
                                ToastUtil.showLonglToast(((ApiException) e).getStatus().getMsg() + "");
                                return;
                            }
                            ToastUtil.showLonglToast(((ApiException) e).getStatus().getMsg() + "");
                        }

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("发布成功");
                        finish();
                    }
                }, PublishActivity.this));
    }

    private void buileParams() {
        if (TextUtils.isEmpty(endTime)) {
            ToastUtil.showShortlToast("截止时间不能为空");
            return;
        }
        String now = sdf.format(new Date());
        if ("1".equals(type)) {//立即发布
            if (now.compareTo(endTime) >= 0) {
                ToastUtil.showShortlToast("截止时间不能小于当前时间");
                return;
            }
            releaseTime = now;
        } else {//定时发布
            if (TextUtils.isEmpty(releaseTime)) {
                ToastUtil.showShortlToast("发布时间不能为空");
                return;
            }
            if (now.compareTo(endTime) >= 0) {
                ToastUtil.showShortlToast("截止时间不能小于当前时间");
                return;
            }
            if (now.compareTo(releaseTime) > 0) {
                ToastUtil.showShortlToast("发布时间不能小于当前时间");
                return;
            }
        }
        Map<String, Object> params = new HashMap<>();
        JSONArray jsonArray = new JSONArray();

        final List<SyncClassVo.DataBean> classCheckedList = publishAdapter.getCheckedList();
        for (int i = 0; i < classCheckedList.size(); i++) {
            final SyncClassVo.DataBean classVo = classCheckedList.get(i);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("baseGradeId", classVo.getBaseGradeId());
                jsonObject.put("classId", classVo.getClassId());
                jsonObject.put("baseGradeName", classVo.getBaseGradeName());
                jsonObject.put("className", classVo.getClassName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            map.put(i, jsonObject);
        }

        if (map.size() == 0) {
            ToastUtil.showLonglToast("请选择公布班级");
            return;
        }
        if (type == null) {
            ToastUtil.showLonglToast("请选择发布时间");
            return;
        }
        params.put("type", type);
        for (Map.Entry<Integer, Object> entry : map.entrySet()) {
            if (entry.getValue() != null) {
                jsonArray.put(entry.getValue());
            }
        }
        params.put("lessonLibId", lessonLibId);
        params.put("lessonLibName", lessonLibName);
        params.put("name", name);
        params.put("baseSubjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
        params.put("baseSubjectName", SubjectSPUtils.getCurrentSubject().getSubjectName());
        params.put("gradeClassJSON", jsonArray.toString());
        params.put("releaseTime", releaseTime + ":00");
        params.put("endTime", endTime + ":00");
        if (personageTv.isChecked()) {
            answerViewType = "1";
        }
        if (abortTv.isChecked()) {
            answerViewType = "2";
        }
        if (teacherTv.isChecked()) {
            answerViewType = "2";
        }

        if (beforeTv.isChecked()) {
            sceneId = "1";
            sceneName = "课前预习";
        }
        if (inTv.isChecked()) {
            sceneId = "2";
            sceneName = "课中讲解";
        }
        if (afterTv.isChecked()) {
            sceneId = "3";
            sceneName = "课后巩固";
        }
        if (answerViewType == null) {
            ToastUtil.showLonglToast("请选择公布答案方式");
            return;
        }
        if (sceneId == null) {
            ToastUtil.showLonglToast("请选择发布场景");
            return;
        }
        params.put("answerViewType", answerViewType);
        params.put("sceneId", sceneId);
        params.put("sceneName", sceneName);

        confirmMap(params);
    }

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);

    private void show() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        }
        endTime = new SimpleDateFormat("yyyy-MM-dd ", Locale.CHINA).format(calendar.getTime()) + "22:00";
        dateChooseTxt.setText(endTime);
        String now = sdf.format(date);
//        dateChooseTxt.setText(now.split(" ")[0]);
//        dateChooseTxt.setText(now);
        releaseTime = now;
//        endTime = now;
//        dateChoose.setText(now.split(" ")[0]);
        dateChoose.setText(now);
        customDatePicker2 = new CustomDatePicker(this, new CustomDatePicker.ResultHandler() {
            @Override
            public void handle(String time) { // 回调接口，获得选中的时间
                dateChooseTxt.setText(time);
                endTime = time;
            }
        }, now, "2028-12-31 00:00"); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker2.showSpecificTime(true); // 显示时和分
        customDatePicker2.setIsLoop(true); // 允许循环滚动

        customDatePicker1 = new CustomDatePicker(this, new CustomDatePicker.ResultHandler() {
            @Override
            public void handle(String time) { // 回调接口，获得选中的时间
                releaseTime = time;
                dateChoose.setText(time);
            }
        }, now, "2028-12-31 00:00"); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker1.showSpecificTime(true); // 显示时和分
        customDatePicker1.setIsLoop(true); // 允许循环滚动
    }

}
