package com.example.app5mykc.fragment.lesson;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.listener.OnCalenderSelectListener;
import com.example.app5libbase.listener.OnSearchClickListener;
import com.example.app5libbase.pop.TaskSearchPop;
import com.example.app5mykc.presenter.lensson.LessonReviewPresenter;
import com.example.app5mykc.presenter.lensson.LessonSyncPresenter;
import com.example.app5mykc.view.lesson.LessonReviewView;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libbase.util.TaskComparator;
import com.example.app5libbase.views.CalenderClearEditText;
import com.example.app5libbase.views.CalenderDialog;
import com.example.app5libbase.views.ClearableEditText;
import com.example.app5libbase.views.EmptyRecyclerView;
import com.example.app5libbase.views.ImageHintEditText;
import com.example.app5libbase.views.ReviewVersionSpinner;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.teacher.vo.LessonVo;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class LessonReviewFragment extends MBaseFragment<LessonReviewPresenter> implements LessonReviewView, OnRefreshListener, OnLoadMoreListener, View.OnClickListener {


    private RelativeLayout head;
    private TextView versionText;
    private View line;
    private CalenderClearEditText dateChooseTxt;
    private ImageHintEditText btnSearch;
    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView swipeTarget;
    private ReviewVersionSpinner versionSpinner;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;

    public static final String SAVE_CATEGORY = "save_category";

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter taskAdapter;

    private List<LessonSyncPresenter
            .MyCountDownTimer> downTimers = new ArrayList<>();

    private int currPage = 0;
    private int pageSize = 10;
    private int historyCount = 0;
    private List<ReviewVersionBean.DataBean> versionList;
    private TaskSearchPop taskSearchPop;
    private String keyWord;
    private CalenderDialog calendarDialog;
    private String startTime;
    private String endTime;

    public LessonReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lesson_review, container, false);
        head = (RelativeLayout) view.findViewById(R.id.head);
        versionText = (TextView) view.findViewById(R.id.version_text);
        line = (View) view.findViewById(R.id.line);
        dateChooseTxt = (CalenderClearEditText) view.findViewById(R.id.date_choose_txt);
        btnSearch = (ImageHintEditText) view.findViewById(R.id.btnSearch);
        refreshLayout = (SmartRefreshLayout) view.findViewById(R.id.refreshLayout);
        swipeTarget = (EmptyRecyclerView) view.findViewById(R.id.swipe_target);
        versionSpinner = (ReviewVersionSpinner) view.findViewById(R.id.version_spinner_view);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        versionText.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        dateChooseTxt.setOnClickListener(this);
        initView();
        initData();
        return view;
    }

    private void initData() {

        mPresenter.getVersionList();

    }

    @Override
    public void initPresenter() {
        mPresenter = new LessonReviewPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    private void initView() {

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));

        swipeTarget.setEmptyView(llTaskEmpty);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == historyCount) {
                    return 3;
                }
                return 1;
            }
        });
        swipeTarget.setLayoutManager(gridLayoutManager);
        taskAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        swipeTarget.setAdapter(taskAdapter);

        versionSpinner.setOnItemChoosedListener(new ReviewVersionSpinner.ItemChoosedListener() {
            @Override
            public void onItemChoosed(int pos) {
                onVersionChoose(versionList.get(pos));
            }

        });
     /*   swipeTarget.addOnItemTouchListener(new OnItemTouchListener(swipeTarget) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                vh.getAdapterPosition();
                Object itemData = itemEntityList.getItemData(vh.getAdapterPosition());
                if (itemData instanceof LessonVo.DataBean) {
                    LessonVo.DataBean dataBean = (LessonVo.DataBean) itemData;
                    Intent intent = new Intent(getActivity(), LessonDetailsFragment.class);
                    intent.putExtra("lessonId", dataBean.getId() + "");
                    intent.putExtra("lessonName", dataBean.getName() + "");
                    intent.putExtra("titleName", dataBean.getChapterName() + "");
                    startActivity(intent);
                }
            }
        });*/
    }


    private void onVersionChoose(ReviewVersionBean.DataBean bean) {
        versionText.setText(bean.getReviewTypeName());
        mPresenter.setBaseGradeId(bean.getBaseGradeId());
        currPage = 1;
        getLesson();
    }

    @Override
    public void setLessonVoData(LessonVo lessonVo) {
        cancilLoadState();
        for (LessonSyncPresenter.MyCountDownTimer myCountDownTimer : downTimers) {
            myCountDownTimer.cancel();
        }


        // 第一页，需要先清空
        if (currPage == 1) {
            itemEntityList.clear();
            historyCount = 0;
        }
        if (!itemEntityList.getItems().contains("最近一周")) {
            // 添加标签
            itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, "最近一周")
                    .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            holder.setText(R.id.tvDecoration, (String) itemData);
                        }
                    });
        }

        if (lessonVo != null && lessonVo.getData() != null && lessonVo.getData().size() > 0) {

            List<LessonVo.DataBean> data = lessonVo.getData();
            Collections.sort(data, new TaskComparator());

            if (itemEntityList.getItems().contains("历史课程")) {
                itemEntityList.addItems(R.layout.lesson_sync_item, data)
                        .addOnBind(R.layout.lesson_sync_item, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                mPresenter.bindTaskListHolder(holder, (LessonVo.DataBean) itemData);

                            }
                        });
            } else {
                for (LessonVo.DataBean dataBean : data) {
                    long time = dataBean.getTimeUpdate();
                    int i = DateUtil.differentDays(time, System.currentTimeMillis());
                    if (i <= 7) {
                        if (!itemEntityList.getItems().contains("最近一周")) {
                            // 添加标签
                            itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, "最近一周")
                                    .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                                        @Override
                                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                            holder.setText(R.id.tvDecoration, (String) itemData);
                                        }
                                    });
                        }
                    } else if (!itemEntityList.getItems().contains("历史课程")) {
                        // 添加标签
                        historyCount = itemEntityList.getItemCount();
                        itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, "历史课程")
                                .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                                    @Override
                                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                        holder.setText(R.id.tvDecoration, (String) itemData);
                                    }
                                });
                    }

                    itemEntityList.addItem(R.layout.lesson_sync_item, dataBean)
                            .addOnBind(R.layout.lesson_sync_item, new Y_OnBind() {
                                @Override
                                public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                    mPresenter.bindTaskListHolder(holder, (LessonVo.DataBean) itemData);
                                }
                            });
                }
            }

            taskAdapter.notifyDataSetChanged();
            if (data.size() < pageSize) {
                refreshLayout.setEnableLoadMore(false);
            } else {
                refreshLayout.setEnableLoadMore(true);
            }
        } else if (itemEntityList.getItemCount() == 1) {

            swipeTarget.setEmptyView(llTaskEmpty);
            refreshLayout.setEnableLoadMore(false);
            itemEntityList.clear();
            taskAdapter.notifyDataSetChanged();
            ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText("暂无数据");

            refreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void onVersionSuccessed(ReviewVersionBean reviewVersionBean) {
        this.versionList = reviewVersionBean.getData();
        if (versionList == null || versionList.size() == 0) {

            return;
        }
        onVersionChoose(versionList.get(0));
    }

    @Override
    public void onVersionFailed() {

    }

    @Override
    public void onSyncSuccessed() {
        getLesson();
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 1;
        getLesson();
    }

    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        getLesson();
    }

    /**
     * 获取课程列表
     */
    private void getLesson() {
        Map<String, Object> params = new HashMap<>();

        params.put("page", String.valueOf(currPage));
        params.put("rows", String.valueOf(pageSize));
        params.put("lessonType", "1");
        params.put("type", "2");
        if (!TextUtils.isEmpty(mPresenter.getChapterNodeIdPath())) {
            params.put("chapterNodeIdPath", mPresenter.getChapterNodeIdPath());
        }
        if (!TextUtils.isEmpty(keyWord)) {
            params.put("keyWord", keyWord);
        }
        params.put("subjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
        if (!TextUtils.isEmpty(startTime)) {
            params.put("startTime", startTime);
        }
        if (!TextUtils.isEmpty(endTime)) {
            params.put("endTime", endTime);
        }
        mPresenter.getLessonVo(params);
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        } else if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadMore();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.version_text) {
            if (versionSpinner.getVisibility() == View.GONE) {
                versionSpinner.setList(versionList);
            } else {
                versionSpinner.setVisibility(View.GONE);
            }
        } else if (id == R.id.btnSearch) {
            showPop();
        } else if (id == R.id.date_choose_txt) {
            showCalendarDialog();
        }
    }

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(getActivity(), new OnSearchClickListener() {

                @Override
                public void onSearch(String searchStr) {

                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    keyWord = searchStr;
                    getLesson();


                }
            });
        }

        taskSearchPop.showPopupWindow(SAVE_CATEGORY, btnSearch, SubjectSPUtils.getCurrentSubject().getSubjectId());
    }

    private void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(activity, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    dateChooseTxt.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  至  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    startTime = sdf.format(startCalendar.getTimeInMillis());
                    endTime = sdf.format(endCalendar.getTimeInMillis());
                    getLesson();
                }
            });
            dateChooseTxt.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    startTime = null;
                    endTime = null;
                    getLesson();
                    calendarDialog = null;
                }
            });
        }
        calendarDialog.show();
    }

}
