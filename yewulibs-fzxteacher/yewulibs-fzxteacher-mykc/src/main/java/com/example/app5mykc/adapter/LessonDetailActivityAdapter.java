package com.example.app5mykc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.views.RoundProgressBar;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/28
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class LessonDetailActivityAdapter extends BaseAdapter {
    private Context mCxt;

    private List<String> mData;


    public LessonDetailActivityAdapter(Context mCxt, List<String> mData) {
        this.mCxt = mCxt;
        this.mData = mData;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;


        if (view == null) {
            view = LayoutInflater.from(mCxt).inflate(R.layout.item_activity_layout, null);
            holder = new ViewHolder(view);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        return view;
    }

    static class ViewHolder {
        TextView activityName;
        TextView tvClass;
        TextView resCount;
        TextView startTime;
        TextView endTime;
        RoundProgressBar roundProgressBar;
        TextView tvRight;
        TextView tvSeparator2;
        TextView tvCount2;
        LinearLayout llProgress;
        TextView tvStatus;
        TextView changedTxt;
        TextView answerTxt;
        TextView jiangjieTxt;
        TextView previewTxt;
        TextView delTxt;

        ViewHolder(View view) {
            activityName = view.findViewById(R.id.activity_name);
            tvClass = view.findViewById(R.id.tvClass);
            startTime = view.findViewById(R.id.start_time);
            resCount = view.findViewById(R.id.res_count);
            roundProgressBar = view.findViewById(R.id.roundProgressBar);
            endTime = view.findViewById(R.id.end_time);
            tvRight = view.findViewById(R.id.tvRight);
            tvSeparator2 = view.findViewById(R.id.tvSeparator2);
            tvCount2 = view.findViewById(R.id.tvCount2);
            llProgress = view.findViewById(R.id.llProgress);
            tvStatus = view.findViewById(R.id.tvStatus);
            changedTxt = view.findViewById(R.id.changed_txt);
            answerTxt = view.findViewById(R.id.answer_txt);
            jiangjieTxt = view.findViewById(R.id.jiangjie_txt);
            previewTxt = view.findViewById(R.id.preview_txt);
            delTxt = view.findViewById(R.id.del_txt);
        }
    }
}




