package com.example.app5mykc.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.TeachActivityAdapter;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.base.BaseView;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5mykc.fragment.lesson.LessonReviewFragment;
import com.example.app5mykc.fragment.lesson.LessonSyncFragment;
import com.example.app5libbase.baseui.presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的课程
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class LessonFragment extends MBaseFragment<BasePresenter> implements BaseView {
    public static final int SYNC_TASK = 1;
    public static final int SYNC_REVIEW = 2;
    public static final int[] TASK_TYPES = {SYNC_TASK, SYNC_REVIEW};


    RadioButton leftRb;
    RadioButton rightRb;
    RadioGroup radioGroup;
    ViewPager viewpager;
    private TeachActivityAdapter mAdapter;

    public static LessonFragment newInstance(Bundle bundle) {
        LessonFragment fragment = new LessonFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new MainPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_lesson, container, false);
        leftRb = rootView.findViewById(R.id.left_rb);
        rightRb = rootView.findViewById(R.id.right_rb);
        radioGroup = rootView.findViewById(R.id.radioGroup);
        viewpager = rootView.findViewById(R.id.lessen_viewpager);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new TeachActivityAdapter(getChildFragmentManager());
        viewpager.setAdapter(mAdapter);
        List<Fragment> list = new ArrayList<>();
        LessonSyncFragment fragment = new LessonSyncFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tag", SYNC_TASK);
        fragment.setArguments(bundle);

        LessonReviewFragment fragment2 = new LessonReviewFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putInt("tag", SYNC_REVIEW);
        fragment2.setArguments(bundle1);

        list.add(fragment);
        // list.add(fragment2);
        mAdapter.setDatas(list);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0)
                    leftRb.setChecked(true);
                else if (position == 1)
                    rightRb.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.left_rb) {
                    viewpager.setCurrentItem(0);
                } else if (i == R.id.right_rb) {
                    viewpager.setCurrentItem(1);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }
}
