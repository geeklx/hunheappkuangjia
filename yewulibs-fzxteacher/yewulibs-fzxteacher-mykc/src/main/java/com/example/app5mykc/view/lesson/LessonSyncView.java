package com.example.app5mykc.view.lesson;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.LessonVo;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/9
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface LessonSyncView extends BaseView {
    void setLessonVoData(LessonVo lessonVo);

    public void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> list);

    public void onNodeSuccessed(List<NodeBean.DataBeanXX> list);

    public void onSyncSuccessed();


}
