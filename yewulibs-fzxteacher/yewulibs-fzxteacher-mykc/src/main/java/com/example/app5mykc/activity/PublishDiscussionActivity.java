package com.example.app5mykc.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5mykc.presenter.PublishDiscussionPresenter;
import com.example.app5mykc.view.PublishDiscussionView;
import com.sdzn.fzx.teacher.vo.chatroom.ClassGroup;
import com.sdzn.fzx.teacher.vo.chatroom.DiscussionClassGroup;
import com.example.app5libbase.util.chatroom.ChatUtils;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.datefive.CustomDatePicker;
import com.example.app5libbase.views.EmptyRecyclerView;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.sdzn.fzx.student.libutils.app.App2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * 列表中点击发布  弹出的发布页（课程详情-发布）
 */

public class PublishDiscussionActivity extends MBaseActivity<PublishDiscussionPresenter> implements PublishDiscussionView, View.OnClickListener {
    private EmptyRecyclerView recyclerView;
    private TextView dateChooseTxt;
    private LinearLayout cancel;
    private TextView tvCancel;
    private LinearLayout confirm;
    private TextView tvConfirm;


    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter mAdapter;

    private String teachGroupChatLibId, lessonId, volumeName, volumeId, chapterName, chapterId;

    @Override
    public void initPresenter() {
        mPresenter = new PublishDiscussionPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_discussion);

        recyclerView = (EmptyRecyclerView) findViewById(R.id.recyclerView);
        dateChooseTxt = (TextView) findViewById(R.id.date_choose_txt);
        cancel = (LinearLayout) findViewById(R.id.cancel);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        confirm = (LinearLayout) findViewById(R.id.confirm);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);

        dateChooseTxt.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        teachGroupChatLibId = getIntent().getStringExtra("teachGroupChatLibId");
        lessonId = getIntent().getStringExtra("lessonId");
        volumeName = getIntent().getStringExtra("volumeName");
        volumeId = getIntent().getStringExtra("volumeId");
        chapterName = getIntent().getStringExtra("chapterName");
        chapterId = getIntent().getStringExtra("chapterId");


        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PublishDiscussionActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        LinearLayoutManager linearLayoutManagerLesson = new LinearLayoutManager(App2.get());
        recyclerView.setLayoutManager(linearLayoutManagerLesson);
        recyclerView.setNestedScrollingEnabled(false);
        show();
        mAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        recyclerView.setAdapter(mAdapter);
    }


    /**
     * 此处不建 presenter 了
     */
    @Override
    protected void initData() {
        mPresenter.getClassList();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.date_choose_txt) {
            if (dateChooseTxt.getText().toString().isEmpty()) {
                customDatePicker2.show(sdf.format(new Date()));
            } else {
                customDatePicker2.show(dateChooseTxt.getText().toString());
            }
        } else if (id == R.id.tv_confirm) {
            if ("[]".equals(buildGroupInfo(allList).toString()) || "[]".equals(buildClassInfo(classList).toString())) {
                ToastUtil.showShortlToast("请选择发布对象");
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("teachGroupChatLibId", teachGroupChatLibId);
                map.put("baseVolumeId", volumeId);
                map.put("baseVolumeName", volumeName);
                map.put("chapterId", chapterId);
                map.put("chapterName", chapterName);
                map.put("publishGroupJson", buildGroupInfo(allList));
                map.put("publishClassJson", buildClassInfo(classList));
                map.put("endTime", endTime + ":00");
                mPresenter.publishGroupList(map);
            }
        } else if (id == R.id.tv_cancel) {
            finish();
        }
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
        PublishDiscussionActivity.this.finish();
    }

    @Override
    public void onClassSuccessed(SyncClassVo o) {
        for (int a = 0; a < o.getData().size(); a++) {
            mPresenter.getClassGroupList(o.getData().get(a).getClassId(), o.getData().get(a).getBaseGradeName() + o.getData().get(a).getClassName());
        }
    }


    @Override
    public void setClassGropingData(ClassGroupingVo classGroupingVo, String classId, String className) {
        DiscussionClassGroup classGroup = new DiscussionClassGroup();
        List<DiscussionClassGroup.DataB> classList = new ArrayList<>();
        classGroup.setClassName(className);
        classGroup.setClassId(classId);
        for (int data = 0; data < classGroupingVo.getData().size(); data++) {
            classList.add(new DiscussionClassGroup.DataB(String.valueOf(classGroupingVo.getData().get(data).getClassGroupId()), classGroupingVo.getData().get(data).getClassGroupName()));
        }
        classGroup.setDataBList(classList);

        itemEntityList.addItem(R.layout.chat_check_publish_grid, classGroup)
                .addOnBind(R.layout.chat_check_publish_grid, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        mPresenter.bindHolder(recyclerView.getHeight(), holder, (DiscussionClassGroup) itemData);
                    }
                });
        mAdapter.notifyDataSetChanged();

    }

    private List<ClassGroup> allList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();

    @Override
    public void getClassGroupInfo(String classId, List<DiscussionClassGroup.DataB> groupsList) {
        Iterator<ClassGroup> iterator = allList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getmClassId().trim().equals(classId.trim())) {
                iterator.remove();
            }
        }
        allList.addAll(ChatUtils.copyIterator(iterator));

        for (int add = 0; add < groupsList.size(); add++) {
            allList.add(new ClassGroup(classId, groupsList.get(add).getGroupId()));
        }

        classList.clear();
        for (ClassGroup group : allList) {
            if (!classList.contains(group.getmClassId())) {
                classList.add(group.getmClassId());
            }
        }

    }

    @Override
    public void publishSuccessed() {
        ToastUtil.showShortlToast("成功发布");
        PublishDiscussionActivity.this.finish();
    }

    private JSONArray buildClassInfo(List<String> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (String dataStr : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("classId", dataStr);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private JSONArray buildGroupInfo(List<ClassGroup> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (ClassGroup dataGroup : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("classId", dataGroup.getmClassId());
                jsonObject.put("groupId", dataGroup.getmGroupId());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
    private String endTime = "";
    private CustomDatePicker customDatePicker2;

    private void show() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        }
//        endTime = new SimpleDateFormat("yyyy-MM-dd ", Locale.CHINA).format(calendar.getTime()) + "22:00:00";
        String now = sdf.format(date);
        endTime = DateUtil.getDateStr(now, 10);
        dateChooseTxt.setText(endTime);

        customDatePicker2 = new CustomDatePicker(this, new CustomDatePicker.ResultHandler() {
            @Override
            public void handle(String time) { // 回调接口，获得选中的时间
                endTime = time;
                dateChooseTxt.setText(endTime);
            }
        }, now, "2028-12-31 00:00:00"); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker2.showSpecificTime(true); // 显示时和分
        customDatePicker2.setIsLoop(true); // 允许循环滚动


    }

}
