package com.example.app5mykc.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.example.app5libbase.R;
import com.example.app5libbase.app.SchoolBoxWatcher;
import com.example.app5libbase.baseui.activity.preview.DocViewActivity;
import com.example.app5libbase.baseui.activity.preview.ImageDisplayActivity;
import com.example.app5libbase.baseui.activity.preview.PDFActivity;
import com.example.app5libbase.baseui.activity.preview.PlayerActivity;
import com.sdzn.fzx.teacher.vo.LessonResource;
import com.sdzn.fzx.teacher.vo.ResourceVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/1
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class LessonDatailResourceAdapter extends RecyclerView.Adapter<LessonDatailResourceAdapter.ViewHolder> {

    private List<LessonResource.DataBean> list;
    private Activity context;

    public LessonDatailResourceAdapter(List<LessonResource.DataBean> list, Activity context) {

        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.resource_lesson_gridview_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        ResourceVo.ResourceTextVo resourceTextVo = new Gson().fromJson(list.get(i).getResourceText(), ResourceVo.ResourceTextVo.class);

        if (list.get(i).getResourceType() == 1) {
            if ("txt".equals(resourceTextVo.getResourceSuffix())) {
                viewHolder.icon.setImageResource(R.mipmap.txt);
            } else {
                viewHolder.icon.setImageResource(R.mipmap.word);
            }

        }
        if (list.get(i).getResourceType() == 2) {
            viewHolder.icon.setImageResource(R.mipmap.ppt);
        }
        if (list.get(i).getResourceType() == 3) {
            viewHolder.icon.setImageResource(R.mipmap.shipin);
        }
        if (list.get(i).getResourceType() == 4) {
            viewHolder.icon.setImageResource(R.mipmap.img);
        }
        if (list.get(i).getResourceType() == 5) {
            viewHolder.icon.setImageResource(R.mipmap.yinpin);
        }
        viewHolder.textView.setText(list.get(i).getResourceName());
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skipActivity(list.get(i).getResourceText(), context);
            }
        });
        if (list.get(i).getConvertStatus() == 1) {
            viewHolder.transcodImg.setVisibility(View.VISIBLE);
            viewHolder.transcodImg.setImageResource(R.mipmap.shuaxin_icon);
        }
        if (list.get(i).getConvertStatus() == 2) {
            viewHolder.transcodImg.setVisibility(View.GONE);
        }
        if (list.get(i).getConvertStatus() == 3) {
            viewHolder.transcodImg.setVisibility(View.VISIBLE);
            viewHolder.transcodImg.setImageResource(R.mipmap.shibei_icon);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView textView;
        private LinearLayout linearLayout;
        private ImageView transcodImg;

        public ViewHolder(View arg0) {

            super(arg0);
            icon = arg0.findViewById(R.id.img);
            textView = arg0.findViewById(R.id.name);
            linearLayout = arg0.findViewById(R.id.all);
            transcodImg = arg0.findViewById(R.id.transcod_img);
        }

    }

    private void skipActivity(String data, final Context activity) {
        final ResourceVo.ResourceTextVo resourceTextVo = new Gson().fromJson(data, ResourceVo.ResourceTextVo.class);
        // 1文档类2演示稿3视频类4图片类5音频类*/
        switch (resourceTextVo.getResourceType()) {
            case 1:
            case 2:
                showSelectOpenDialog(new OpenListener() {
                    @Override
                    public void openByApp() {
                        Intent intentDoc = new Intent(activity, PDFActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getConvertPath());
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        activity.startActivity(intentDoc);
                    }

                    @Override
                    public void openByOther() {
                        Intent intentDoc = new Intent(activity, DocViewActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getResourcePath());
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        switch (resourceTextVo.getResourceType()) {
                            case 1:
                                if ("txt".equals(resourceTextVo.getResourceSuffix())) {
                                    intentDoc.putExtra("type", "txt");
                                } else {
                                    intentDoc.putExtra("type", "doc");
                                }
                                break;
                            case 2:
                                intentDoc.putExtra("type", "ppt");
                                break;
                        }
                        activity.startActivity(intentDoc);
                    }
                });
                break;
            case 3:


                SchoolBoxWatcher.getFileUrlAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), new SchoolBoxWatcher.SchoolBosUrl() {
                    @Override
                    public void bosUrl(String url) {
                        Intent intentVideo = new Intent(activity, PlayerActivity.class);
                        intentVideo.putExtra("videoUrl", url);
                        intentVideo.putExtra("title", resourceTextVo.getResourceName());
                        activity.startActivity(intentVideo);
                    }
                });

                break;
            case 4:

                SchoolBoxWatcher.getFileUrlAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), new SchoolBoxWatcher.SchoolBosUrl() {
                    @Override
                    public void bosUrl(String url) {
                        Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
                        intentImage.putExtra("photoUrl", url);
                        activity.startActivity(intentImage);
                    }
                });

                break;
            case 5:

                SchoolBoxWatcher.getFileUrlAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), new SchoolBoxWatcher.SchoolBosUrl() {
                    @Override
                    public void bosUrl(String url) {
                        Intent intentRadio = new Intent(activity, PlayerActivity.class);
                        intentRadio.putExtra("radioUrl", url);
                        intentRadio.putExtra("title", resourceTextVo.getResourceName());
                        activity.startActivity(intentRadio);
                    }
                });

                break;
            case 6:
                Intent intentDoc = new Intent(activity, PDFActivity.class);
                intentDoc.putExtra("path", resourceTextVo.getConvertPath());
                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                activity.startActivity(intentDoc);
                break;
        }
    }

    private Dialog showSelectOpenDialog(final OpenListener listener) {
        final Dialog dialog = new Dialog(context, R.style.MyDialog);
        View view = context.getLayoutInflater().inflate(R.layout.dialog_album_or_camera, null);
        view.findViewById(R.id.tv_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByApp();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByOther();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
        return dialog;
    }

    interface OpenListener {
        void openByApp();

        void openByOther();
    }
}
