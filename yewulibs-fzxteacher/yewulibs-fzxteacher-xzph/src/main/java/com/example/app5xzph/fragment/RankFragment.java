package com.example.app5xzph.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.listener.OnCalenderSelectListener;
import com.example.app5libbase.listener.OnClassClickListener;
import com.example.app5libbase.pop.ClassPop;
import com.example.app5xzph.presenter.RankPresenter;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CalenderClearEditText;
import com.example.app5libbase.views.CalenderDialog;
import com.example.app5libbase.views.ClearableEditText;
import com.example.app5libbase.views.GroupRankView;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.example.app5xzph.view.RankView;

import java.util.Calendar;
import java.util.List;

/**
 * 小组排行
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class RankFragment extends MBaseFragment<RankPresenter> implements RankView, View.OnClickListener {

    private TextView tvClass;
    private RadioGroup rgGroup;
    private RadioButton rbToday;
    private RadioButton rbWeek;
    private RadioButton rbMonth;
    private RadioButton rbUser;
    private CalenderClearEditText tvDate;
    private GroupRankView rankView1;
    private View line1;
    private RecyclerView rvRank1;
    private GroupRankView rankView2;
    private View line2;
    private RecyclerView rvRank2;
    private GroupRankView rankView3;
    private View line3;
    private RecyclerView rvRank3;

    private ClassPop classPop;
    private List<SyncClassVo.DataBean> syncClassVoDatas;
    private SyncClassVo.DataBean syncClassVoData;

    private Y_ItemEntityList itemEntityListRank1 = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter rank1Adapter;
    private Y_ItemEntityList itemEntityListRank2 = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter rank2Adapter;
    private Y_ItemEntityList itemEntityListRank3 = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter rank3Adapter;

    private String currentTimeStyle = "1";

    private String startTime;
    private String endTime;

    private CalenderDialog calendarDialog;

    public static RankFragment newInstance(Bundle bundle) {
        RankFragment fragment = new RankFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new RankPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rank, container, false);


        tvClass = (TextView) rootView.findViewById(R.id.tvClass);
        rgGroup = (RadioGroup) rootView.findViewById(R.id.rgGroup);
        rbToday = (RadioButton) rootView.findViewById(R.id.rbToday);
        rbWeek = (RadioButton) rootView.findViewById(R.id.rbWeek);
        rbMonth = (RadioButton) rootView.findViewById(R.id.rbMonth);
        rbUser = (RadioButton) rootView.findViewById(R.id.rbUser);
        tvDate = (CalenderClearEditText) rootView.findViewById(R.id.tvDate);
        rankView1 = (GroupRankView) rootView.findViewById(R.id.rankView1);
        line1 = (View) rootView.findViewById(R.id.line1);
        rvRank1 = (RecyclerView) rootView.findViewById(R.id.rvRank1);
        rankView2 = (GroupRankView) rootView.findViewById(R.id.rankView2);
        line2 = (View) rootView.findViewById(R.id.line2);
        rvRank2 = (RecyclerView) rootView.findViewById(R.id.rvRank2);
        rankView3 = (GroupRankView) rootView.findViewById(R.id.rankView3);
        line3 = (View) rootView.findViewById(R.id.line3);
        rvRank3 = (RecyclerView) rootView.findViewById(R.id.rvRank3);
        tvClass.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        initView();
        initData();
        return rootView;
    }

    private void initData() {
        mPresenter.getClassList();
    }

    private void initView() {
        rankView1.setTitle("总排行");
        rankView2.setTitle("课堂活跃");
        rankView3.setTitle("任务作答");

        rgGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbToday) {
                    if (!currentTimeStyle.equals("1")) {
                        currentTimeStyle = "1";
                        getRankList(syncClassVoData);
                    }
                    tvDate.setVisibility(View.GONE);
                } else if (checkedId == R.id.rbWeek) {
                    if (!currentTimeStyle.equals("2")) {
                        currentTimeStyle = "2";
                        getRankList(syncClassVoData);
                    }
                    tvDate.setVisibility(View.GONE);
                } else if (checkedId == R.id.rbMonth) {
                    if (!currentTimeStyle.equals("3")) {
                        currentTimeStyle = "3";
                        getRankList(syncClassVoData);
                    }
                    tvDate.setVisibility(View.GONE);
                } else if (checkedId == R.id.rbUser) {
                    if (!currentTimeStyle.equals("4")) {
                        currentTimeStyle = "4";
                        if (TextUtils.isEmpty(startTime) || TextUtils.isEmpty(endTime)) {
                            showCalendarDialog();
                        } else {
                            getRankList(syncClassVoData);
                        }
                    }
                    tvDate.setVisibility(View.VISIBLE);
                }
            }
        });

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(activity);
        rvRank1.setLayoutManager(linearLayoutManager1);
        rank1Adapter = new Y_MultiRecyclerAdapter(activity, itemEntityListRank1);
        rvRank1.setAdapter(rank1Adapter);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(activity);
        rvRank2.setLayoutManager(linearLayoutManager2);
        rank2Adapter = new Y_MultiRecyclerAdapter(activity, itemEntityListRank2);
        rvRank2.setAdapter(rank2Adapter);
        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(activity);
        rvRank3.setLayoutManager(linearLayoutManager3);
        rank3Adapter = new Y_MultiRecyclerAdapter(activity, itemEntityListRank3);
        rvRank3.setAdapter(rank3Adapter);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvClass) {
            showPop(tvClass, new OnClassClickListener() {
                @Override
                public void onClassClick(SyncClassVo.DataBean dataBean) {
                    tvClass.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
                    getRankList(dataBean);
                    syncClassVoData = dataBean;
                }
            });
        } else if (id == R.id.tvDate) {
            showCalendarDialog();
        }
    }

    private void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(activity, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    tvDate.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  至  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    startTime = String.valueOf(startCalendar.getTimeInMillis());
                    endTime = String.valueOf(endCalendar.getTimeInMillis());
                    getRankList(syncClassVoData);
                }
            });

            tvDate.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    startTime = null;
                    endTime = null;
                    getRankList(syncClassVoData);
                    calendarDialog = null;
                }
            });
        }
        calendarDialog.show();
    }

    private void showPop(View view, OnClassClickListener onClassClickListener) {
        if (classPop == null) {
            classPop = new ClassPop(activity, onClassClickListener);
        } else {
            classPop.setOnClassClickListener(onClassClickListener);
        }
        if (syncClassVoDatas != null) {
            classPop.setClassVos(syncClassVoDatas);
        }
        classPop.showPopupWindow(view);
    }

    @Override
    public void getClassVoSuccess(SyncClassVo syncClassVo) {
        if (syncClassVo != null && syncClassVo.getData() != null && syncClassVo.getData().size() > 0) {
            syncClassVoDatas = syncClassVo.getData();
            syncClassVoData = syncClassVoDatas.get(0);
            tvClass.setText(syncClassVoData.getBaseGradeName() + syncClassVoData.getClassName());
            getRankList(syncClassVoData);
        }
    }

    private void getRankList(SyncClassVo.DataBean dataBean) {
        if (dataBean != null) {
            itemEntityListRank1.clear();
            rank1Adapter.notifyDataSetChanged();
            itemEntityListRank2.clear();
            rank2Adapter.notifyDataSetChanged();
            itemEntityListRank3.clear();
            rank3Adapter.notifyDataSetChanged();
            rankView1.setGroup1("");
            rankView1.setGroup2("");
            rankView1.setGroup3("");
            rankView2.setGroup1("");
            rankView2.setGroup2("");
            rankView2.setGroup3("");
            rankView3.setGroup1("");
            rankView3.setGroup2("");
            rankView3.setGroup3("");
            mPresenter.getRankList(dataBean.getClassId(), null, currentTimeStyle, startTime, endTime);
            mPresenter.getRankList(dataBean.getClassId(), "2", currentTimeStyle, startTime, endTime);
            mPresenter.getRankList(dataBean.getClassId(), "1", currentTimeStyle, startTime, endTime);
        }
    }

    @Override
    public void getRankVo1Success(RankVo rankVo) {
        for (int i = 0; i < 3; i++) {
            String groupName = null;
            if (rankVo != null && rankVo.getData() != null && rankVo.getData().size() > i) {
                RankVo.DataBean dataBean = rankVo.getData().get(i);
                groupName = dataBean.getClassGroupName();
            }
            if (i == 0) {
                rankView1.setGroup1(groupName);
            } else if (i == 1) {
                rankView1.setGroup2(groupName);
            } else {
                rankView1.setGroup3(groupName);
            }
        }

        itemEntityListRank1.clear();
        if (rankVo != null && rankVo.getData() != null) {
            itemEntityListRank1.addItems(R.layout.item_fragment_rank, rankVo.getData())
                    .addOnBind(R.layout.item_fragment_rank, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindRankHolder(holder, (RankVo.DataBean) itemData);
                        }
                    });
        }
        rank1Adapter.notifyDataSetChanged();
    }

    @Override
    public void getRankVo2Success(RankVo rankVo) {
        for (int i = 0; i < 3; i++) {
            String groupName = null;
            if (rankVo != null && rankVo.getData() != null && rankVo.getData().size() > i) {
                RankVo.DataBean dataBean = rankVo.getData().get(i);
                groupName = dataBean.getClassGroupName();
            }
            if (i == 0) {
                rankView2.setGroup1(groupName);
            } else if (i == 1) {
                rankView2.setGroup2(groupName);
            } else {
                rankView2.setGroup3(groupName);
            }
        }

        itemEntityListRank2.clear();
        if (rankVo != null && rankVo.getData() != null) {
            itemEntityListRank2.addItems(R.layout.item_fragment_rank, rankVo.getData())
                    .addOnBind(R.layout.item_fragment_rank, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindRankHolder(holder, (RankVo.DataBean) itemData);
                        }
                    });
        }
        rank2Adapter.notifyDataSetChanged();
    }

    @Override
    public void getRankVo3Success(RankVo rankVo) {
        for (int i = 0; i < 3; i++) {
            String groupName = null;
            if (rankVo != null && rankVo.getData() != null && rankVo.getData().size() > i) {
                RankVo.DataBean dataBean = rankVo.getData().get(i);
                groupName = dataBean.getClassGroupName();
            }
            if (i == 0) {
                rankView3.setGroup1(groupName);
            } else if (i == 1) {
                rankView3.setGroup2(groupName);
            } else {
                rankView3.setGroup3(groupName);
            }
        }

        itemEntityListRank3.clear();
        if (rankVo != null && rankVo.getData() != null) {
            itemEntityListRank3.addItems(R.layout.item_fragment_rank, rankVo.getData())
                    .addOnBind(R.layout.item_fragment_rank, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindRankHolder(holder, (RankVo.DataBean) itemData);
                        }
                    });
        }
        rank3Adapter.notifyDataSetChanged();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }
}