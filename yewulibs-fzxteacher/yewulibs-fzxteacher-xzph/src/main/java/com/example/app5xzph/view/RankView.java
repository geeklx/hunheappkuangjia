package com.example.app5xzph.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public interface RankView extends BaseView {
    void getClassVoSuccess(SyncClassVo syncClassVo);

    void getRankVo1Success(RankVo rankVo);

    void getRankVo2Success(RankVo rankVo);

    void getRankVo3Success(RankVo rankVo);

    void networkError(String msg);
}
