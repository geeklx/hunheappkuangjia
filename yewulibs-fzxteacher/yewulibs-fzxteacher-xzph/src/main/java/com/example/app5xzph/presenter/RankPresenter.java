package com.example.app5xzph.presenter;

import android.text.TextUtils;

import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5xzph.view.RankView;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class RankPresenter extends BasePresenter<RankView, BaseActivity> {

    private Subscription subscribe1;

    public void getClassList() {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        subscribe1 = Network.createTokenService(NetWorkService.MainService.class)
                .getClassList(String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()))
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<SyncClassVo>(new SubscriberListener<SyncClassVo>() {
                    @Override
                    public void onNext(SyncClassVo syncClassVo) {
                        mView.getClassVoSuccess(syncClassVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe1);
    }

    public void getRankList(String classId, final String type, String timeStyle, String startTime, String endTime) {
        Map<String, String> params = new HashMap<>();
        params.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
        params.put("classId", classId);
        if (!TextUtils.isEmpty(type)) {
            params.put("type", type);
        }
        params.put("timeStyle", timeStyle);
        if ("4".equals(timeStyle)) {
            params.put("startTime", startTime);
            params.put("endTime", endTime);
        }
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .getRank(params)
                .map(new StatusFunc<RankVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<RankVo>(new SubscriberListener<RankVo>() {
                    @Override
                    public void onNext(RankVo rankVo) {
                        if (TextUtils.isEmpty(type)) {
                            mView.getRankVo1Success(rankVo);
                        } else if ("2".equals(type)) {
                            mView.getRankVo2Success(rankVo);
                        } else if ("1".equals(type)) {
                            mView.getRankVo3Success(rankVo);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void bindRankHolder(GeneralRecyclerViewHolder holder, RankVo.DataBean itemData) {
        holder.setText(R.id.tvNum, String.valueOf(holder.getAdapterPosition() + 1));
        String classGroupName = itemData.getClassGroupName();
        if (TextUtils.isEmpty(classGroupName)) {
            holder.setText(R.id.tvLogo, "无");
            holder.setText(R.id.tvName, "暂无");
        } else {
            holder.setText(R.id.tvLogo, itemData.getClassGroupName().substring(0, 1));
        }
        holder.setText(R.id.tvName, itemData.getClassGroupName());
        holder.setText(R.id.tvPoint, itemData.getPoints() + "分");
    }
}