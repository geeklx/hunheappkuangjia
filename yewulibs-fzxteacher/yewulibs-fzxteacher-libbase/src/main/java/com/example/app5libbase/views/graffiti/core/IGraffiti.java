package com.example.app5libbase.views.graffiti.core;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created on 27/06/2018.
 */

public interface IGraffiti {
    /**
     * 获取当前涂鸦坐标系中的单位大小，该单位参考dp，独立于图片
     */
    public float getSizeUnit();

    /**
     * 设置图片旋转值
     */
    public void setGraffitiRotation(int degree);

    /**
     * 获取图片旋转值
     */
    public int getGraffitiRotation();

    /**
     * 设置图片缩放倍数
     */
    public void setGraffitiScale(float scale, float pivotX, float pivotY);

    /**
     * 获取图片缩放倍数
     */
    public float getGraffitiScale();

    /**
     * 获取画笔
     */
    public IGraffitiPen getPen();

    /**
     * 设置画笔形状
     */
    public void setShape(IGraffitiShape shape);

    /**
     * 获取画笔形状
     */
    public IGraffitiShape getShape();

    /**
     * 设置图片偏移量x
     */
    public void setGraffitiTranslation(float transX, float transY);


    /**
     * 设置图片偏移量x
     */
    public void setGraffitiTranslationX(float transX);

    /**
     * 获取图片偏移量x
     */
    public float getGraffitiTranslationX();

    /**
     * 设置图片偏移量y
     */
    public void setGraffitiTranslationY(float transY);

    /**
     * 获取图片偏移量y
     */
    public float getGraffitiTranslationY();

    /**
     * 设置大小
     */
    public void setSize(float paintSize);

    /**
     * 获取大小
     */
    public float getSize();

    /**
     * 设置颜色
     */
    public void setColor(IGraffitiColor color);

    /**
     * 获取颜色
     */
    public IGraffitiColor getColor();

    /**
     * 最小缩放倍数限制
     */
    public void setGraffitiMinScale(float minScale);

    /**
     * 最小缩放倍数限制
     */
    public float getGraffitiMinScale();

    /**
     * 最大缩放倍数限制
     */
    public void setGraffitiMaxScale(float maxScale);

    /**
     * 最大缩放倍数限制
     */
    public float getGraffitiMaxScale();

    /**
     * 添加item
     */
    public void addItem(IGraffitiItem graffitiItem);

    /**
     * 移除item
     */
    public void removeItem(IGraffitiItem graffitiItem);

    /**
     * 获取所有的涂鸦
     */
    public List<IGraffitiItem> getAllItem();

    /**
     * 是否允许涂鸦显示在图片边界之外
     */
    public void setIsDrawableOutside(boolean isDrawableOutside);

    void setIsJustDrawOriginal(boolean isJustDrawOriginal);

    /**
     * 撤销一步
     */
    public boolean undo();

    /**
     * 指定撤销的步数
     */
    public boolean undo(int step);

    boolean isUndo();

    /**
     * 获取当前显示的图片(无涂鸦)
     */
    public Bitmap getBitmap();

    /**
     * 获取当前显示的图片(包含涂鸦)
     */
    public Bitmap getGraffitiBitmap();

    Bitmap save();

    /**
     * 刷新
     */
    public void invalidate();

    void clear();

}
