package com.example.app5libbase.base;


import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BaseView;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * 描述：BasePresenter
 * -
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BasePresenter<V extends BaseView, A extends BaseActivity> {
    public V mView;
    public A mActivity;

//    protected Application appContext;
    protected List<Subscription> subscriptions = new ArrayList<>();

    public void attachView(V v, A a) {
        this.mView = v;
        this.mActivity = a;
//        this.appContext =  appContext;
    }

    public void detachView() {
        for (Subscription subscription : subscriptions) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();//取消訂閱
            }
        }
        this.mView = null;
        this.mActivity = null;
//        this.appContext = null;
    }
}
