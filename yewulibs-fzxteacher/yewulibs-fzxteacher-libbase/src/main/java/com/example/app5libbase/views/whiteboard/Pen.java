package com.example.app5libbase.views.whiteboard;

/**
 * 画笔
 */
public enum Pen {
    HAND, // 手绘
    HIGHLIGHTER,//荧光笔
    COPY, // 仿制
    ERASER // 橡皮擦
}

