package com.example.app5libbase.views.exam;

/**
 * @author Reisen at 2018-08-23
 */
public class CharSequenceImp implements CharSequence {
    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return "";
    }
}
