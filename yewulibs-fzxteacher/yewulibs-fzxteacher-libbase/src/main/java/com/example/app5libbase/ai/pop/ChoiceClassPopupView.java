package com.example.app5libbase.ai.pop;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.BjClassAdapter;
import com.example.app5libbase.ai.pop.bean.TextModel1;
import com.lxj.xpopup.core.CenterPopupView;
import com.sdzn.fzx.teacher.bean.TeachingCoursesBean;

import java.util.ArrayList;
import java.util.List;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * 选择班级
 */
public class ChoiceClassPopupView extends CenterPopupView {
    private TextView tvTitle;
    private RecyclerView rlPopup;
    private TextView tvCancel;
    private TextView tvConfirm;

    private Context mContext;
    BjClassAdapter bjClassAdapter;
    private TeachingCoursesBean.RowsBean classesBeans;
    private List<TextModel1> mTextModels;

    public ChoiceClassPopupView(@NonNull Context context, TeachingCoursesBean.RowsBean classesBeans) {
        super(context);
        this.mContext = context;
        this.classesBeans = classesBeans;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_center_yewu_class;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Log.e("tag", "CustomDrawerPopupView onCreate");
        tvTitle = (TextView) findViewById(R.id.tv_title);
        rlPopup = (RecyclerView) findViewById(R.id.rl_popup);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);

        tvCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typeIndex.equals("")) {
                    ToastUtils.showShort("请选择班级");
                } else {
                    Intent intent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.ClassAllContentAct");
                    intent1.putExtra("lessonId", lessonId);
                    intent1.putExtra("courseName", courseName);
                    intent1.putExtra("classesId", classesId);
                    startActivity(intent1);
                    dismiss();
//                    ToastUtils.showShort(typeIndex + lessonId);
                }
            }
        });
        initRv();
    }

    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

    private String typeIndex = "";//根据传入的参数判断唯一性
    private String lessonId = "";//课程id
    private String courseName = "";//课程名称
    private String classesId = "";//班级id

    private void initRv() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        rlPopup.setLayoutManager(gridLayoutManager);
        mTextModels = new ArrayList<>();
        bjClassAdapter = new BjClassAdapter();
        rlPopup.setAdapter(bjClassAdapter);

        initData(classesBeans);
        bjClassAdapter.setNewData(mTextModels);

        bjClassAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                typeIndex = mTextModels.get(position).getText();
                lessonId = mTextModels.get(position).getLessonId();
                courseName = mTextModels.get(position).getCourseName();
                classesId = mTextModels.get(position).getClassesId();
                bjClassAdapter.setClickPosition(typeIndex);
//                bjClassAdapter.setSelection(position);
//                bjClassAdapter.notifyDataSetChanged();
//                ToastUtils.showShort(typeIndex);
            }
        });
    }

    private void initData(TeachingCoursesBean.RowsBean classesBeans) {
        mTextModels = new ArrayList<>();
        if (classesBeans != null) {
            for (int i = 0; i < classesBeans.getClasses().size(); i++) {
                mTextModels.add(new TextModel1(classesBeans.getClasses().get(i).getClassesName(), classesBeans.getLessonId(), classesBeans.getCourseName(), classesBeans.getClasses().get(i).getClassesId()));
            }
        }
    }
}
