package com.example.app5libbase.listener;

import com.sdzn.fzx.teacher.vo.SyncClassVo;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/2/1
 */
public interface OnClassClickListener {
    void onClassClick(SyncClassVo.DataBean dataBean);
}
