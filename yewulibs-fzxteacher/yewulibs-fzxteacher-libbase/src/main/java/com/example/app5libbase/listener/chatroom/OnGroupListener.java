package com.example.app5libbase.listener.chatroom;


/**
 * 添加注释
 * <p>
 * 小组讨论模块
 */
public interface OnGroupListener {

    /**
     * ClassId
     */
    void onSearch(String classId);

    void onVersionId(String version);

    void onNodeId(String node);

}
