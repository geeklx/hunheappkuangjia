package com.example.app5libbase.views.whiteboard;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * 仿制的定位器
 */
public class CopyLocation {

    private float mPaintSize;
    private float mCopyStartX, mCopyStartY; // 仿制的坐标
    private float mTouchStartX, mTouchStartY; // 开始触摸的坐标
    private float mX, mY; // 当前位置
    private Paint mPaint;
    private boolean isRelocating = true; // 正在定位中
    private boolean isCopying = false; // 正在仿制绘图中

    public CopyLocation(float x, float y, float mPaintSize) {
        mX = x;
        mY = y;
        mTouchStartX = x;
        mTouchStartY = y;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        this.mPaintSize = mPaintSize;
        mPaint.setStrokeWidth(mPaintSize);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
    }


    public void updateLocation(float x, float y) {
        mX = x;
        mY = y;
    }

    public void setStartPosition(float x, float y) {
        mCopyStartX = mX;
        mCopyStartY = mY;
        mTouchStartX = x;
        mTouchStartY = y;
    }

    public void drawItSelf(Canvas canvas) {
        mPaint.setStrokeWidth(mPaintSize / 4);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(0xaa666666); // 灰色
        DrawUtil.drawCircle(canvas, mX, mY, mPaintSize / 2 + mPaintSize / 8, mPaint);

        mPaint.setStrokeWidth(mPaintSize / 16);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(0xaaffffff); // 白色
        DrawUtil.drawCircle(canvas, mX, mY, mPaintSize / 2 + mPaintSize / 32, mPaint);

        mPaint.setStyle(Paint.Style.FILL);
        if (!isCopying) {
            mPaint.setColor(0x44ff0000); // 红色
            DrawUtil.drawCircle(canvas, mX, mY, mPaintSize / 2, mPaint);
        } else {
            mPaint.setColor(0x44000088); // 蓝色
            DrawUtil.drawCircle(canvas, mX, mY, mPaintSize / 2, mPaint);
        }
    }

    /**
     * 判断是否点中
     */
    public boolean isInIt(float x, float y) {
        if ((mX - x) * (mX - x) + (mY - y) * (mY - y) <= mPaintSize * mPaintSize) {
            return true;
        }
        return false;
    }

}