package com.example.app5libbase.ai.pop.provider;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.example.app5libbase.ai.pop.bean.GRZXBaseRecActBean;


public class Style1Provider extends BaseItemProvider<GRZXBaseRecActBean, BaseViewHolder> {

    @Override
    public int viewType() {
        return GRZXBaseRecActAdapter.STYLE_ONE;
    }

    @Override
    public int layout() {
        return R.layout.popup_right_recycleview_item1;
    }

    @Override
    public void convert(BaseViewHolder helper, GRZXBaseRecActBean data, int position) {
//        if (position % 2 == 0) {
//            helper.setImageResource(R.id.iv, R.drawable.ic_zhaoliying);
//        }else{
//            helper.setImageResource(R.id.iv, R.drawable.img01);
//        }
        helper.setText(R.id.tv, data.getGrzxRecActBean1().getTitle() + "");
    }

    @Override
    public void onClick(BaseViewHolder helper, GRZXBaseRecActBean data, int position) {

//        Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onLongClick(BaseViewHolder helper, GRZXBaseRecActBean data, int position) {
//        Toast.makeText(mContext, "longClick", Toast.LENGTH_SHORT).show();
        return true;
    }
}
