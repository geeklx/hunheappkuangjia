package com.example.app5libbase.base;

import android.os.Bundle;
import android.view.View;

import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.inthecheesefactory.thecheeselibrary.fragment.support.v4.app.NestedActivityResultFragment;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BaseFragment1 extends NestedActivityResultFragment {

    public BaseActWebActivity1 activity;
//    protected Application appContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActWebActivity1) getActivity();
//       appContext = activity.getApplication();
    }

    /*防重复点击*/
    public abstract class OnMultiClickListener implements View.OnClickListener {
        // 两次点击按钮之间的点击间隔不能少于1000毫秒
        private final int MIN_CLICK_DELAY_TIME = 1000;
        private long lastClickTime;

        public abstract void onMultiClick(View v);

        @Override
        public void onClick(View v) {
            long curClickTime = System.currentTimeMillis();
            if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
                // 超过点击间隔后再将lastClickTime重置为当前点击时间
                lastClickTime = curClickTime;
                onMultiClick(v);
            }
        }
    }

    @Override
    public void onDestroy() {
        activity = null;
//        appContext = null;
        //  EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}