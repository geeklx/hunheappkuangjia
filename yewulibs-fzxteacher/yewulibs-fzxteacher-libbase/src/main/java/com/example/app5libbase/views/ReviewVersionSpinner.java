package com.example.app5libbase.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;

import java.util.List;

/**
 * ReviewVersionSpinner〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ReviewVersionSpinner extends RelativeLayout {

    private Context context;
    private View bgView;
    private LinearLayout containerLy;

    private ListView listView;
    private ItemAdapter itemAdapter;

    public ReviewVersionSpinner(Context context) {
        this(context, null);
    }

    public ReviewVersionSpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReviewVersionSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.context = context;
        final View spinnerView = LayoutInflater.from(context).inflate(R.layout.spinner_view_layout, null);
        bgView = spinnerView.findViewById(R.id.bg_View);
        containerLy = spinnerView.findViewById(R.id.list_container);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(spinnerView, layoutParams);
        bgView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ReviewVersionSpinner.this.setVisibility(View.GONE);
            }
        });

        dealViews();
    }


    private void dealViews() {
        listView = new ListView(context);
        itemAdapter = new ItemAdapter(context);
        listView.setAdapter(itemAdapter);
        listView.setBackgroundColor(getResources().getColor(R.color.white));
        final int width = UiUtils.dp2px(280);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT);
        containerLy.addView(listView, layoutParams);
    }

    public boolean setList(List<ReviewVersionBean.DataBean> list) {
        itemAdapter.setData(list);
        setVisibility(View.VISIBLE);
        return true;
    }

    public class ItemAdapter extends BaseAdapter {

        private Context context;

        private int curItem;

        private List<ReviewVersionBean.DataBean> mData;

        public void setData(List<ReviewVersionBean.DataBean> mData) {
            this.mData = mData;
            notifyDataSetChanged();
        }

        public ItemAdapter(Context context) {
            this.context = context;
        }

        public int getCurItem() {
            return curItem;
        }

        public void setCurItem(int curItem) {
            this.curItem = curItem;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData == null ? 0 : mData.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            final ReviewVersionBean.DataBean volumeListBean = mData.get(i);
            SpinnerViewHolder holder;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.item_spinner_layout, null);
                holder = new SpinnerViewHolder();
                holder.name = view.findViewById(R.id.item_name);
                view.setTag(holder);
            } else {
                holder = (SpinnerViewHolder) view.getTag();
            }

            holder.name.setText(volumeListBean.getBaseGradeName() + volumeListBean.getReviewTypeName());

            holder.name.setEnabled(false);

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemChoosed(i);
                    }
                    ReviewVersionSpinner.this.setVisibility(View.GONE);
                }
            });
            return view;
        }
    }

    private ItemChoosedListener listener;

    public void setOnItemChoosedListener(ItemChoosedListener listener) {
        this.listener = listener;
    }

    public interface ItemChoosedListener {
        void onItemChoosed(int pos);
    }


    public static class SpinnerViewHolder {
        public TextView name;
    }
}
