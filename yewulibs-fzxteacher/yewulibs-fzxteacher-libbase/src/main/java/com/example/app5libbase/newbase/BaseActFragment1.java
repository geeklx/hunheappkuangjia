package com.example.app5libbase.newbase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.just.agentweb.LocalBroadcastManagers;
import com.just.agentweb.base.BaseCurrencyAgentWebFragment;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.BuildConfig3;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BaseActFragment1 extends BaseCurrencyAgentWebFragment {
    private long mCurrentMs = System.currentTimeMillis();
    private MessageReceiverIndex mMessageReceiver;
    public String Token;

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("activityRefresh".equals(intent.getAction())) {
                    if (mAgentWeb != null && !mAgentWeb.getWebCreator().getWebView().getUrl().equals(BuildConfig3.SERVER_ISERVICE_NEW1 + "/setToken")) {
                        mAgentWeb.getUrlLoader().reload();
                    }
//                    loadWebSite(mAgentWeb.getWebCreator().getWebView().getUrl());
                }
            } catch (Exception e) {
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        setup(rootView, savedInstanceState);
        return rootView;
    }

    protected abstract int getLayoutId();


    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        Token = "Bearer " + (String) SPUtils.getInstance().getString("token", "");
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);
    }

    /*防重复点击*/
    public abstract class OnMultiClickListener implements View.OnClickListener {
        // 两次点击按钮之间的点击间隔不能少于1000毫秒
        private final int MIN_CLICK_DELAY_TIME = 1000;
        private long lastClickTime;

        public abstract void onMultiClick(View v);

        @Override
        public void onClick(View v) {
            long curClickTime = System.currentTimeMillis();
            if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
                // 超过点击间隔后再将lastClickTime重置为当前点击时间
                lastClickTime = curClickTime;
                onMultiClick(v);
            }
        }
    }

    public String ids;

    public void call(Object value) {
        LogUtils.e("BaseActFragment-----call");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}