package com.example.app5libbase.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.app5libbase.R;
import com.example.app5libpublic.cons.Subject;

/**
 * 自定义图标说明
 *
 * @author wangchunxiao
 * @date 2018/1/9
 */
public class ChartLegendView extends LinearLayout {

    private View vDot;
    private TextView tvName;
    private TextView tvCount;
    private Context context;

    public ChartLegendView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.view_main_fragment_chart_legend, this, true);
        vDot = findViewById(R.id.vDot);
        tvName = findViewById(R.id.tvName);
        tvCount = findViewById(R.id.tvCount);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ChartLegendView);
        if (attributes != null) {
            Drawable drawable = attributes.getDrawable(R.styleable.ChartLegendView_dot);
            if (drawable != null) {
                vDot.setBackgroundDrawable(drawable);
            }
            String subject = attributes.getString(R.styleable.ChartLegendView_subject);
            boolean aBoolean = attributes.getBoolean(R.styleable.ChartLegendView_show, true);
            if (!aBoolean) {
                tvCount.setVisibility(GONE);
            }
            setText(subject);
            int count = attributes.getInteger(R.styleable.ChartLegendView_count, 0);
            int color = attributes.getColor(R.styleable.ChartLegendView_text_color, 0);
            if (color != 0) {
                tvCount.setTextColor(color);
            }
            tvCount.setText(String.valueOf(count));

            attributes.recycle();
        }
    }

    private void setText(String subject) {
        if (subject != null) {
            if (subject.equals(Subject.dili.getName()) || subject.equals(Subject.shengwu.getName()) || subject.equals(Subject.yingyu.getName())) {
                tvCount.setTextColor(context.getResources().getColor(R.color.fragment_main_chart_green));
            } else if (subject.equals(Subject.huaxue.getName()) || subject.equals(Subject.lishi.getName()) || subject.equals(Subject.yuwen.getName())) {
                tvCount.setTextColor(context.getResources().getColor(R.color.fragment_main_chart_yellow));
            } else if (subject.equals(Subject.shuxue.getName()) || subject.equals(Subject.wuli.getName()) || subject.equals(Subject.zhengzhi.getName())) {
                tvCount.setTextColor(context.getResources().getColor(R.color.fragment_main_chart_violet));
            } else if (subject.equals(Subject.other.getName())) {
                tvCount.setTextColor(context.getResources().getColor(R.color.fragment_main_chart_other));
            }
            tvName.setText(subject);
        }
    }

    public void setSubject(String subject) {
        setText(subject);
    }

    public void setCount(int count) {
        tvCount.setText(String.valueOf(count));
    }
}
