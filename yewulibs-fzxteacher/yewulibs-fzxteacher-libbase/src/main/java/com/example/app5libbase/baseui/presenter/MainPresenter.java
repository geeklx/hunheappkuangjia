package com.example.app5libbase.baseui.presenter;

import android.view.View;
import android.view.ViewGroup;

import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.baseui.view.MainView;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libbase.views.RoundProgressBar;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.CorrectVo;
import com.sdzn.fzx.teacher.vo.LessonListVo;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.TaskListVo;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class MainPresenter extends BasePresenter<MainView, BaseActivity> {

    private SyncClassVo syncClassVoData;
    private int currentData = 0;

    public void getTaskList() {
        Map<String, String> params1 = new HashMap<>();
        params1.put("baseSubjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
        params1.put("status", "0");
        params1.put("rows", "3");
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .getTaskList(params1)
                .map(new StatusFunc<TaskListVo>())
                .flatMap(new Func1<TaskListVo, Observable<StatusVo<LessonListVo>>>() {
                    @Override
                    public Observable<StatusVo<LessonListVo>> call(final TaskListVo taskListVo) {
                        currentData = 1;
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.getTaskListSuccess(taskListVo);
                            }
                        });
                        Map<String, String> params2 = new HashMap<>();
                        params2.put("lessonType", "1");
                        params2.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
                        params2.put("page", "1");
                        params2.put("rows", "3");
                        return Network.createTokenService(NetWorkService.MainService.class).getLessonList(params2);
                    }
                })
                .map(new StatusFunc<LessonListVo>())
                .flatMap(new Func1<LessonListVo, Observable<StatusVo<SyncClassVo>>>() {
                    @Override
                    public Observable<StatusVo<SyncClassVo>> call(final LessonListVo lessonListVo) {
                        currentData = 2;
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.getLessonListSuccess(lessonListVo);
                            }
                        });
                        return Network.createTokenService(NetWorkService.MainService.class).getClassList(String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
                    }
                })
                .map(new StatusFunc<SyncClassVo>())
                .flatMap(new Func1<SyncClassVo, Observable<StatusVo<RankVo>>>() {
                    @Override
                    public Observable<StatusVo<RankVo>> call(final SyncClassVo syncClassVo) {
                        syncClassVoData = syncClassVo;
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.getClassVoSuccess(syncClassVo);
                            }
                        });
                        Map<String, String> params3 = new HashMap<>();
                        if (syncClassVo != null && syncClassVo.getData() != null && syncClassVo.getData().size() > 0) {
                            SyncClassVo.DataBean dataBean = syncClassVo.getData().get(0);
                            params3.put("classId", dataBean.getClassId());
                            params3.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
                            params3.put("timeStyle", "0");
                        }
                        return Network.createTokenService(NetWorkService.MainService.class).getRank(params3);
                    }
                })
                .map(new StatusFunc<RankVo>())
                .flatMap(new Func1<RankVo, Observable<StatusVo<CorrectVo>>>() {
                    @Override
                    public Observable<StatusVo<CorrectVo>> call(final RankVo rankVo) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.getRankSuccess(rankVo);
                            }
                        });
                        Map<String, String> params4 = new HashMap<>();
                        if (syncClassVoData != null && syncClassVoData.getData() != null && syncClassVoData.getData().size() > 0) {
                            SyncClassVo.DataBean dataBean = syncClassVoData.getData().get(0);
                            params4.put("classId", dataBean.getClassId());
                            params4.put("baseSubjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
                        }
                        return Network.createTokenService(NetWorkService.MainService.class).getCorrect(params4);
                    }
                })
                .map(new StatusFunc<CorrectVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<CorrectVo>(new SubscriberListener<CorrectVo>() {
                    @Override
                    public void onNext(CorrectVo correctVo) {
                        mView.getCorrectSuccess(correctVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                        if (currentData == 0) {
                            mView.getTaskListFailure();
                        } else if (currentData == 1) {
                            mView.getLessonListFailure();
                        }
                    }

                    @Override
                    public void onCompleted() {
                        mView.onCompleted();
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void getRank(String classId) {
        Map<String, String> params = new HashMap<>();
        params.put("classId", classId);
        params.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
        params.put("timeStyle", "1");
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .getRank(params)
                .map(new StatusFunc<RankVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<RankVo>(new SubscriberListener<RankVo>() {
                    @Override
                    public void onNext(RankVo rankVo) {
                        mView.getRankSuccess(rankVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                        mView.onCompleted();
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void getCorrect(String classId) {
        Map<String, String> params = new HashMap<>();
        params.put("classId", classId);
        params.put("baseSubjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .getCorrect(params)
                .map(new StatusFunc<CorrectVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<CorrectVo>(new SubscriberListener<CorrectVo>() {
                    @Override
                    public void onNext(CorrectVo correctVo) {
                        mView.getCorrectSuccess(correctVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                        mView.onCompleted();
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void bindTaskHolder(int rvHeight, GeneralRecyclerViewHolder holder, TaskListVo.DataBean itemData) {
        holder.setText(R.id.tvTaskName, itemData.getName());
        holder.setText(R.id.tvClass, itemData.getBaseGradeName() + itemData.getClassName());
        holder.setText(R.id.tvDate, DateUtil.getTimeStrByTimemillis(itemData.getEndTime(), "yyyy-MM-dd HH:mm"));
        ((RoundProgressBar) holder.getChildView(R.id.roundProgressBar)).setMax(itemData.getCountStudentTotal());
        ((RoundProgressBar) holder.getChildView(R.id.roundProgressBar)).setProgress(itemData.getCountStudentSub());
        holder.setText(R.id.tvRight, String.valueOf(itemData.getCountStudentSub()));
        holder.setText(R.id.tvCount2, String.valueOf(itemData.getCountStudentTotal()));
        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlTaskItem).getLayoutParams();
        layoutParams.height = rvHeight / 3;
        holder.getChildView(R.id.rlTaskItem).setLayoutParams(layoutParams);
    }

    public void bindLessonHolder(int rvHeight, GeneralRecyclerViewHolder holder, LessonListVo.DataBean itemData) {
        holder.setText(R.id.tvTaskName, itemData.getName());
        if (itemData.getType() == 1) {
            holder.setText(R.id.tvChapter, itemData.getChapterNodeNamePath());
        } else {
            holder.setText(R.id.tvChapter, itemData.getTypeReviewName());
        }
        holder.setText(R.id.tvDate, DateUtil.getTimeStrByTimemillis(itemData.getTimeUpdate(), "yyyy-MM-dd HH:mm"));
        holder.getChildView(R.id.ivBorrow).setVisibility(itemData.getUserHoldId() == itemData.getUserCreateId() ? View.GONE : View.VISIBLE);
        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlLessonItem).getLayoutParams();
        layoutParams.height = rvHeight / 3;
        holder.getChildView(R.id.rlLessonItem).setLayoutParams(layoutParams);
    }


}
