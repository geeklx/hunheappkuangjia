package com.example.app5libbase.baseui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.service.FadeService;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.views.CustomDialog;
import com.sdzn.fzx.student.libutils.util.Log;

import net.ossrs.yasea.SimplePublisher;

/**
 * @author zs
 */

public class ScreenConfigActivity extends BaseActWebActivity1 {
    private TextView tv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_config_screen;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        tv = findViewById(R.id.tv);
        vivView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入ScreenConfigActivity成功");
                }
            }
        }
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void vivView() {
        if (SimplePublisher.getInstance().isRecording()) {
            SimplePublisher.getInstance().stop();
            TimeBaseUtil.getInstance().stopScreen();
            finish();
        } else {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(activity)) {
                    Log.i("------------------L164");
                    Intent intent2 = new Intent(activity, FadeService.class);
                    activity.startService(intent2);
                } else {
                    //若没有权限，提示获取.
                    Log.i("------------------L169");
                    CustomDialog.Builder builder = new CustomDialog.Builder(activity);
                    builder.setTitle("权限提示");
                    builder.setMessage("开启悬浮窗权限可提升投屏体验，是否去开启?");
                    builder.setPositive("去开启", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent3 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                            Toast.makeText(activity, "需要取得权限以使用悬浮窗", Toast.LENGTH_SHORT).show();
                            activity.startActivity(intent3);
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setNegative("放弃", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (ActivityManager.getForegroundActivity() != null) {
                                TimeBaseUtil.getInstance().customScan();
                            }
//                            HiddenAnimUtils.newInstance(activity, llTools, 280, animatorListenerAdapter).toggle();
                            dialogInterface.dismiss();
                        }
                    });
                    CustomDialog dialog = builder.create();
                    dialog.show();
                    return;
                }
            } else {
                //SDK在23以下，不用管.
                Log.i("------------------L196");
                Intent intent1 = new Intent(activity, FadeService.class);
                Log.i("------------------L197");
                activity.startService(intent1);
            }

            // 扫描二维码
//            if (ActivityManager.getForegroundActivity() != null) {
//
//            }
            if (ActivityUtils.getActivityList() != null && ActivityUtils.getActivityList().size() > 0) {
                TimeBaseUtil.getInstance().customScan();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
