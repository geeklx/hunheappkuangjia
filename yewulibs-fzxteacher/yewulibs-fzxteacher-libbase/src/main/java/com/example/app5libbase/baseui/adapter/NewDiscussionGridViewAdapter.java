package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.example.app5libbase.listener.chatroom.OnDiscussionSaveListener;

import java.util.List;

/**
 *
 */

public class NewDiscussionGridViewAdapter extends BaseAdapter {
    private Context context;
    private List<GroupChatBean.DataBean> list;
    LayoutInflater layoutInflater;


    public NewDiscussionGridViewAdapter(Context context, List<GroupChatBean.DataBean> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {//注意此处
        return list.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = layoutInflater.inflate(R.layout.item_discuss_grid_n, null);
        RelativeLayout content_view = (RelativeLayout) convertView.findViewById(R.id.content_view);
        LinearLayout mImageView = (LinearLayout) convertView.findViewById(R.id.ll_image);
        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        TextView tv_chapter = (TextView) convertView.findViewById(R.id.tv_chapter);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView time = (TextView) convertView.findViewById(R.id.time);
        RelativeLayout edit = convertView.findViewById(R.id.edit);
        RelativeLayout preview = convertView.findViewById(R.id.preview);
        RelativeLayout del = convertView.findViewById(R.id.del);
        RelativeLayout sync = convertView.findViewById(R.id.sync);
        if (position>0&&position < list.size()+1) {

            mImageView.setVisibility(View.GONE);
            content_view.setVisibility(View.VISIBLE);

            tv_chapter.setText(list.get(position-1).getChatTitle());
            name.setText(list.get(position-1).getChatContent());
            if (list.get(position-1).getCreateTime().length()>10){ time.setText(list.get(position-1).getCreateTime().substring(0,10));}
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((OnDiscussionSaveListener)context).onEdit(String.valueOf(list.get(position-1).getId()),
                            list.get(position-1).getChatTitle(),list.get(position-1).getChatContent(),list.get(position-1).getEndTime(),list.get(position-1).getTeachGroupChatContentPics());
                }
            });
            preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((OnDiscussionSaveListener)context).onPreview(String.valueOf(list.get(position-1).getId()),
                            list.get(position-1).getChatTitle(),list.get(position-1).getChatContent(),list.get(position-1).getCreateTime(),list.get(position-1).getTeachGroupChatContentPics());
                }
            });
            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((OnDiscussionSaveListener)context).onDelete(String.valueOf(list.get(position-1).getId()));
                }
            });
            sync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((OnDiscussionSaveListener)context).onPublish(String.valueOf(list.get(position-1).getId()));
                }
            });

            content_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((OnDiscussionSaveListener)context).onEdit(String.valueOf(list.get(position-1).getId()),
                            list.get(position-1).getChatTitle(),list.get(position-1).getChatContent(),list.get(position-1).getEndTime(),list.get(position-1).getTeachGroupChatContentPics());

                }
            });


        } else if (position==0){
            mImageView.setVisibility(View.VISIBLE);
            content_view.setVisibility(View.GONE);

        }

        return convertView;
    }

}
