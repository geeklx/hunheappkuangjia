package com.example.app5libbase.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.example.app5libbase.controller.UserController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.vo.LoginBean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * SP的工具类
 * Created by Tony on 16/6/6.
 */
public class SubjectSPUtils {
    /**
     * 保存在手机里面的文件名
     */
    public static final String FILE_NAME = "share_data";

    public static final String GUIDANCE_TAG = "guidance_tag";

    /**
     * 用于实现账号互顶，用一个单一的uuid，组成唯一mqtt的topic
     * 当有新设备登陆的时候，服务端会发一个mqtt消息到该话题上，
     * 收到消息后可以提示用户有其他的设备上登陆了该账号
     */
    public static final String SINGLE_UUID = "mqtt_uuid";

    public static final String LOGIN_USER_NUM = "login_user_name";

    /**
     * 保存／获取本地  登陆成功返回的信息
     */
    public static final String SAVE_LOGIN_INFO = "login_info";
    public static final String SAVE_SUBJECT_INFO = "subject_info";

    /**
     * 保存/获取文件名上传规则
     */
    public static final String SAVE_UPLOAD_RULE = "uploadRule";


    public static final String SAVE_UMENG_DEVICE_TOKEN = "umeng_device_token";
    /**
     * 使用小工具时  存储的classId
     */
    public static final String CLASS_ID = "class_id";

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     *
     * @param context
     * @param key
     * @param object
     */
    public static void put(Context context, String key, Object object) {

        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }

        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param context
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object get(Context context, String key, Object defaultObject) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);

        if (defaultObject instanceof String) {
            return sp.getString(key, (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sp.getLong(key, (Long) defaultObject);
        }

        return null;
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param context
     * @param key
     */
    public static void remove(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 清除所有数据
     *
     * @param context
     */
    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean contains(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.contains(key);
    }

    /**
     * 返回所有的键值对
     *
     * @param context
     * @return
     */
    public static Map<String, ?> getAll(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.getAll();
    }

    /**
     * 创建一个解决SharedPreferencesCompat.apply方法的一个兼容类
     *
     * @author zhy
     */
    private static class SharedPreferencesCompat {
        private static final Method sApplyMethod = findApplyMethod();

        /**
         * 反射查找apply的方法
         *
         * @return
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        private static Method findApplyMethod() {
            try {
                Class clz = SharedPreferences.Editor.class;
                return clz.getMethod("apply");
            } catch (NoSuchMethodException e) {
            }

            return null;
        }

        /**
         * 如果找到则使用apply执行，否则使用commit
         *
         * @param editor
         */
        public static void apply(SharedPreferences.Editor editor) {
            try {
                if (sApplyMethod != null) {
                    sApplyMethod.invoke(editor);
                    return;
                }
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
            editor.commit();
        }
    }

    private static LoginBean.DataBean.SubjectListBean mSubjectListBean;


    public static LoginBean getLoginBean() {
        LoginBean mLoginBean = null;
        String loginInfo = (String) SubjectSPUtils.get( App2.get(), SubjectSPUtils.SAVE_LOGIN_INFO, "");
        if (!TextUtils.isEmpty(loginInfo)) {
            try {
                mLoginBean = new Gson().fromJson(loginInfo, LoginBean.class);
            } catch (Exception e) {
                mLoginBean = null;
            }
        }
        return mLoginBean;
    }

    public static void saveLoginBean(LoginBean loginBean) {
        final String loginJsonStr = new Gson().toJson(loginBean);
        put( App2.get(), SAVE_LOGIN_INFO, loginJsonStr);

    }

    /**
     * 保存文件名上传规则
     */
    public static void saveFileRuleSet(List<String> arr) {
        String json = new Gson().toJson(arr);
        put( App2.get(), SAVE_UPLOAD_RULE, json);
    }

    public static List<String> getFileRuleSet() {
        String json = (String) get( App2.get(), SAVE_UPLOAD_RULE,"");
        return new Gson().fromJson(json,new TypeToken<ArrayList<String>>(){}.getType());
    }

    public static void saveCurrentSubject(LoginBean.DataBean.SubjectListBean subjectListBean) {
        mSubjectListBean = subjectListBean;
        final String subjectJsonStr = new Gson().toJson(subjectListBean);
        put( App2.get(), SAVE_SUBJECT_INFO, subjectJsonStr);

        UserController.saveSubject(subjectListBean.getSubjectId());
    }

    public static LoginBean.DataBean.SubjectListBean getCurrentSubject() {
        if (mSubjectListBean != null) {
            return mSubjectListBean;
        }
        String subjectInfo = (String) SubjectSPUtils.get( App2.get(), SubjectSPUtils.SAVE_SUBJECT_INFO, "");
        if (!TextUtils.isEmpty(subjectInfo)) {
            try {
                mSubjectListBean = new Gson().fromJson(subjectInfo, LoginBean.DataBean.SubjectListBean.class);
            } catch (Exception e) {
                mSubjectListBean = null;
            }
        }
        return mSubjectListBean;
    }
}
