package com.example.app5libbase.base;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * Created by bin_li on 2017/4/18.
 */

public class BaseFragmentPresenter<V extends BaseView, A extends BaseFragment> {
    public V mView;
    public A mActivity;

//    public Application App2.get();
    protected List<Subscription> subscriptions = new ArrayList<>();

    public void attachView(V v, A a) {
        this.mView = v;
        this.mActivity = a;
//        this.App2.get() =  App2.get();
    }

    public void detachView() {
        this.mView = null;
        this.mActivity = null;
//        this.App2.get() = null;
        for (Subscription subscription : subscriptions) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();//取消訂閱
            }
        }
    }

}
