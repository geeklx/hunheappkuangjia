package com.example.app5libbase.baseui.fragment;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.listener.OnPaletteTouchListener;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.example.app5libbase.views.whiteboard.GraffitiView;
import com.example.app5libbase.views.whiteboard.Pen;
import com.example.app5libbase.views.whiteboard.Shape;

import java.io.File;

/**
 * 画板
 */
public class PaletteFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

//    @ViewInject(R.id.viewGray)
//    private View viewGray;

    private GraffitiView graffitiview;
    private RelativeLayout rlTools;
    private RadioGroup radioGroup;
    private RadioButton rbShape;
    private RadioButton rbEraser;
    private RadioButton rbPen;
    private RadioButton rbClear;
    private View line;
    private TextView tvFinish;
    private RadioGroup rgShape;
    private RadioButton rbShapeRect;
    private RadioButton rbShapeOval;
    private RadioButton rbShapeLine;
    private RadioGroup rgColor;
    private RadioButton rbColorWhite;
    private RadioButton rbColorRed;
    private RadioButton rbColorBlack;
    private RadioButton rbColorYellow;
    private RadioButton rbColorGreen;
    private RadioGroup rgSize;
    private RadioButton rbSize1;
    private RadioButton rbSize2;
    private RadioButton rbSize3;
    private RadioButton rbSize4;
    private RadioButton rbSize5;

    private String type;

    private final static int NONE = -1;
    private final static int SHAPE = 0;
    private final static int ERASER = 1;
    private final static int PEN = 2;
    private int tool = NONE;

    private final static int size1 = 6;
    private final static int size2 = 8;
    private final static int size3 = 10;
    private final static int size4 = 12;
    private final static int size5 = 14;

    private RadioButton shapeButton;
    private RadioButton shapeColorButton;
    private RadioButton shapeSizeButton;
    private RadioButton eraserSizeButton;
    private RadioButton penColorButton;
    private RadioButton penSizeButton;

    private Shape shape;
    private int shapeColor;
    private int penColor;
    private int shapeSize;
    private int eraserSize;
    private int penSize;

    //    private ObjectAnimator viewGrayHideAnimator;
//    private ObjectAnimator viewGrayShowAnimator;
    private ObjectAnimator rlToolsHideAnimator;
    private ObjectAnimator rlToolsShowAnimator;

    public PaletteFragment() {
    }

    public static PaletteFragment newInstance(Bundle bundle) {
        PaletteFragment paletteFragment = new PaletteFragment();
        paletteFragment.setArguments(bundle);
        return paletteFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_palette, container, false);
        graffitiview = (GraffitiView) rootView.findViewById(R.id.graffitiview);
        rlTools = (RelativeLayout) rootView.findViewById(R.id.rlTools);
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);
        rbShape = (RadioButton) rootView.findViewById(R.id.rbShape);
        rbEraser = (RadioButton) rootView.findViewById(R.id.rbEraser);
        rbPen = (RadioButton) rootView.findViewById(R.id.rbPen);
        rbClear = (RadioButton) rootView.findViewById(R.id.rbClear);
        line = (View) rootView.findViewById(R.id.line);
        tvFinish = (TextView) rootView.findViewById(R.id.tvFinish);
        rgShape = (RadioGroup) rootView.findViewById(R.id.rgShape);
        rbShapeRect = (RadioButton) rootView.findViewById(R.id.rbShapeRect);
        rbShapeOval = (RadioButton) rootView.findViewById(R.id.rbShapeOval);
        rbShapeLine = (RadioButton) rootView.findViewById(R.id.rbShapeLine);
        rgColor = (RadioGroup) rootView.findViewById(R.id.rgColor);
        rbColorWhite = (RadioButton) rootView.findViewById(R.id.rbColorWhite);
        rbColorRed = (RadioButton) rootView.findViewById(R.id.rbColorRed);
        rbColorBlack = (RadioButton) rootView.findViewById(R.id.rbColorBlack);
        rbColorYellow = (RadioButton) rootView.findViewById(R.id.rbColorYellow);
        rbColorGreen = (RadioButton) rootView.findViewById(R.id.rbColorGreen);
        rgSize = (RadioGroup) rootView.findViewById(R.id.rgSize);
        rbSize2 = (RadioButton) rootView.findViewById(R.id.rbSize2);
        rbSize3 = (RadioButton) rootView.findViewById(R.id.rbSize3);
        rbSize4 = (RadioButton) rootView.findViewById(R.id.rbSize4);
        rbSize5 = (RadioButton) rootView.findViewById(R.id.rbSize5);
        tvFinish.setOnClickListener(this);
        initView();
        return rootView;
    }

    private void initView() {
        radioGroup.setOnCheckedChangeListener(this);
        rgShape.setOnCheckedChangeListener(this);
        rgColor.setOnCheckedChangeListener(this);
        rgSize.setOnCheckedChangeListener(this);

        shapeButton = rbShapeRect;
        shapeColorButton = rbColorRed;
        shapeSizeButton = rbSize1;
        eraserSizeButton = rbSize1;
        penColorButton = rbColorRed;
        penSizeButton = rbSize1;

        shape = Shape.HOLLOW_RECT;
        shapeColor = getResources().getColor(R.color.palette_red);
        penColor = getResources().getColor(R.color.palette_red);
        shapeSize = size1;
        eraserSize = size1;
        penSize = size1;

        // 默认选中黑色
        rbPen.setChecked(true);
        rbColorRed.setChecked(true);
        rbSize1.setChecked(true);

        if ("tuya".equals(type)) {
            graffitiview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    try {
                        File file = new File(FileUtil.getResPath(), "capture.png");
                        Bitmap photo = BitmapFactory.decodeFile(file.getAbsolutePath());
                        graffitiview.setImageBg(photo);
                        graffitiview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        graffitiview.setOnPaletteTouchListener(new OnPaletteTouchListener() {
            @Override
            public void hide() {
                toolsAnimHide();
            }

            @Override
            public void show() {
                toolsAnimShow();
            }
        });

//        viewGray.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        rlTools.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void toolsAnimShow() {
//        if (viewGrayHideAnimator != null && viewGrayHideAnimator.isRunning()) {
//            viewGrayHideAnimator.cancel();
//        }
//        viewGray.setVisibility(View.VISIBLE);
        rlTools.setVisibility(View.VISIBLE);
//        viewGrayShowAnimator = ObjectAnimator.ofFloat(viewGray, "alpha", 0f, 1f);
//        viewGrayShowAnimator.setDuration(200);
//        viewGrayShowAnimator.setRepeatCount(0);
//        viewGrayShowAnimator.start();

        if (rlToolsHideAnimator != null && rlToolsHideAnimator.isRunning()) {
            rlToolsHideAnimator.cancel();
        }
//        viewGray.setVisibility(View.VISIBLE);
        rlTools.setVisibility(View.VISIBLE);
        rlToolsShowAnimator = ObjectAnimator.ofFloat(rlTools, "alpha", 0f, 1f);
        rlToolsShowAnimator.setDuration(200);
        rlToolsShowAnimator.setRepeatCount(0);
        rlToolsShowAnimator.start();
    }

    private void toolsAnimHide() {
//        if (viewGrayShowAnimator != null && viewGrayShowAnimator.isRunning()) {
//            viewGrayShowAnimator.cancel();
//        }
//        viewGrayHideAnimator = ObjectAnimator.ofFloat(viewGray, "alpha", 1f, 0f);
//        viewGrayHideAnimator.setDuration(200);
//        viewGrayHideAnimator.setRepeatCount(0);
//        viewGrayHideAnimator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                viewGray.setVisibility(View.GONE);
//                rlTools.setVisibility(View.GONE);
//                viewGrayHideAnimator.removeAllListeners();
//            }
//        });
//        viewGrayHideAnimator.start();

        if (rlToolsShowAnimator != null && rlToolsShowAnimator.isRunning()) {
            rlToolsShowAnimator.cancel();
        }
        rlToolsHideAnimator = ObjectAnimator.ofFloat(rlTools, "alpha", 1f, 0f);
        rlToolsHideAnimator.setDuration(200);
        rlToolsHideAnimator.setRepeatCount(0);
        rlToolsHideAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
//                viewGray.setVisibility(View.GONE);
                rlTools.setVisibility(View.GONE);
                rlToolsHideAnimator.removeAllListeners();
            }
        });
        rlToolsHideAnimator.start();
    }

    @Override
    public void onDetach() {
        graffitiview.recycleBitmap();
        super.onDetach();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rbShape) {
            setShapeRadio();
        } else if (checkedId == R.id.rbEraser) {
            setEraserRadio();
        } else if (checkedId == R.id.rbPen) {
            setPenRadio();
        } else if (checkedId == R.id.rbClear) {
            clear();
        } else if (checkedId == R.id.rbShapeRect) {
            setShape(Shape.HOLLOW_RECT, rbShapeRect);
        } else if (checkedId == R.id.rbShapeOval) {
            setShape(Shape.OVAL, rbShapeOval);
        } else if (checkedId == R.id.rbShapeLine) {
            setShape(Shape.LINE, rbShapeLine);
        } else if (checkedId == R.id.rbColorWhite) {
            setColor(getResources().getColor(R.color.white), rbColorWhite);
        } else if (checkedId == R.id.rbColorRed) {
            setColor(getResources().getColor(R.color.palette_red), rbColorRed);
        } else if (checkedId == R.id.rbColorBlack) {
            setColor(getResources().getColor(R.color.black), rbColorBlack);
        } else if (checkedId == R.id.rbColorYellow) {
            setColor(getResources().getColor(R.color.palette_yellow), rbColorYellow);
        } else if (checkedId == R.id.rbColorGreen) {
            setColor(getResources().getColor(R.color.palette_green), rbColorGreen);
        } else if (checkedId == R.id.rbSize1) {
            setSize(size1, rbSize1);
        } else if (checkedId == R.id.rbSize2) {
            setSize(size2, rbSize2);
        } else if (checkedId == R.id.rbSize3) {
            setSize(size3, rbSize3);
        } else if (checkedId == R.id.rbSize4) {
            setSize(size4, rbSize4);
        } else if (checkedId == R.id.rbSize5) {
            setSize(size5, rbSize5);
        }
    }

    /**
     * 清楚画布内容
     */
    private void clear() {
        rgShape.setVisibility(View.GONE);
        rgColor.setVisibility(View.GONE);
        rgSize.setVisibility(View.GONE);
        graffitiview.clear();
        radioGroup.clearCheck();
    }

    /**
     * 设置画笔参数
     */
    private void setPenRadio() {
        tool = PEN;

        rgShape.setVisibility(View.GONE);
        rgColor.setVisibility(View.VISIBLE);
        rgSize.setVisibility(View.VISIBLE);

        graffitiview.setPen(Pen.HAND);
        graffitiview.setShape(Shape.HAND_WRITE);

        if (rgColor.getCheckedRadioButtonId() == penColorButton.getId()) {
            setColor(penColor, penColorButton);
        } else {
            penColorButton.setChecked(true);
        }
        if (rgSize.getCheckedRadioButtonId() == penSizeButton.getId()) {
            setSize(penSize, penSizeButton);
        } else {
            penSizeButton.setChecked(true);
        }
    }

    /**
     * 设置橡皮擦参数
     */
    private void setEraserRadio() {
        tool = ERASER;

        rgShape.setVisibility(View.GONE);
        rgColor.setVisibility(View.GONE);
        rgSize.setVisibility(View.VISIBLE);

        graffitiview.setPen(Pen.ERASER);
        graffitiview.setShape(Shape.HAND_WRITE);

        if (rgSize.getCheckedRadioButtonId() == eraserSizeButton.getId()) {
            setSize(eraserSize, eraserSizeButton);
        } else {
            eraserSizeButton.setChecked(true);
        }
    }

    /**
     * 设置形状参数
     */
    private void setShapeRadio() {
        tool = SHAPE;

        rgShape.setVisibility(View.VISIBLE);
        rgColor.setVisibility(View.VISIBLE);
        rgSize.setVisibility(View.VISIBLE);

        if (rgShape.getCheckedRadioButtonId() == shapeButton.getId()) {
            setShape(shape, shapeButton);
        } else {
            shapeButton.setChecked(true);
        }
        if (rgColor.getCheckedRadioButtonId() == shapeColorButton.getId()) {
            setColor(shapeColor, shapeColorButton);
        } else {
            shapeColorButton.setChecked(true);
        }
        if (rgSize.getCheckedRadioButtonId() == shapeSizeButton.getId()) {
            setSize(shapeSize, shapeSizeButton);
        } else {
            shapeSizeButton.setChecked(true);
        }
    }

    private void setShape(Shape shape, RadioButton button) {
        graffitiview.setShape(shape);
        this.shape = shape;
        shapeButton = button;
    }

    private void setColor(int color, RadioButton button) {
        if (tool == PEN) {
            graffitiview.setColor(color);
            penColor = color;
            penColorButton = button;
        } else if (tool == SHAPE) {
            graffitiview.setColor(color);
            shapeColor = color;
            shapeColorButton = button;
        }
    }

    private void setSize(int size, RadioButton button) {
        if (tool == SHAPE) {
            graffitiview.setShapeSize(size);
            shapeSize = size;
            shapeSizeButton = button;
        } else if (tool == PEN) {
            graffitiview.setPaintSize(size);
            penSize = size;
            penSizeButton = button;
        } else if (tool == ERASER) {
            graffitiview.setEraserSize(size);
            eraserSize = size;
            eraserSizeButton = button;
        }
    }

    public void onClick(View view) {
        if (view.getId() == R.id.tvFinish) {
            activity.finish();
        }
    }
}