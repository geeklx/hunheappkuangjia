package com.example.app5libbase.listener.chatroom;

/**
 * 选择照片
 *
 * @author wangchunxiao
 * @date 2018/3/7
 */
public interface AlbumOrCameraListener {
    void selectAlbum();

    void selectCamera();
}
