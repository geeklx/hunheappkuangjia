package com.example.app5libbase.ai.pop;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.OnItemClickLitener;
import com.example.app5libbase.ai.pop.adapter.RekeaseAdapter;
import com.example.app5libbase.ai.pop.bean.TextList;
import com.example.app5libpublic.event.UpdataPhoto;
import com.google.gson.Gson;
import com.lxj.xpopup.core.CenterPopupView;
import com.sdzn.fzx.teacher.bean.SchoolClassGroupBean;
import com.sdzn.fzx.teacher.presenter.SchoolClassGroupPresenter;
import com.sdzn.fzx.teacher.view.SchoolClassGroupViews;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 课堂检测发布
 */
public class ReleaseObjectPopupView extends CenterPopupView implements SchoolClassGroupViews {
    private TextView tvTitle;
    private TextView tvCancel;
    private TextView tvConfirm;
    private Context mContext;
    private RecyclerView rlrelease;

    private SchoolClassGroupPresenter schoolClassGroupPresenter;
    List<SchoolClassGroupBean.GroupsBean> dataList;//学科集合
    private String classesId;
    RekeaseAdapter rekeaseAdapter;


    public ReleaseObjectPopupView(@NonNull Context context, String classesId) {
        super(context);
        this.mContext = context;
        this.classesId = classesId;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_center_yewu_release_object;
    }

    private List<SchoolClassGroupBean.GroupsBean> selectDatas = new ArrayList<>();

    @Override
    protected void onCreate() {
        super.onCreate();
        Log.e("tag", "CustomDrawerPopupView onCreate");
        schoolClassGroupPresenter = new SchoolClassGroupPresenter();
        schoolClassGroupPresenter.onCreate(this);
        schoolClassGroupPresenter.getClassGroup("Bearer " + (String) SPUtils.getInstance().getString("token", ""), classesId);

        tvTitle = (TextView) findViewById(R.id.tv_title);
        rlrelease = findViewById(R.id.rlrelease);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);
        tvCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventReleaseObject(takevalue()));
                dismiss();
            }
        });
        /*初始化列表控件*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        rlrelease.setLayoutManager(linearLayoutManager);

    }

    private String json;
    List<TextList> listmutiple;
    TextList textList;

    /**
     * 选中拿到的数据
     */
    public String takevalue() {
        listmutiple = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            if (rekeaseAdapter.isSelected.get(i)) {
                textList = new TextList();
                rekeaseAdapter.isSelected.put(i, true);
                textList.setClassId(classesId);
                textList.setGroupId(dataList.get(i).getGroupId());
                textList.setGroupName(dataList.get(i).getGroupName());
                listmutiple.add(textList);
            }
        }
        for (int i = 0; i < listmutiple.size(); i++) {
            json = new Gson().toJson(listmutiple);
        }
        //        ToastUtils.showLong(json);
        return json;
    }

    @Override
    public void getSchoolClassGroupSuccess(SchoolClassGroupBean schoolClassGroupBean) {
        dataList = new ArrayList<>();
        if (schoolClassGroupBean != null) {
            dataList.addAll(schoolClassGroupBean.getGroups());
        }
        rekeaseAdapter = new RekeaseAdapter(mContext, dataList);
        rlrelease.setAdapter(rekeaseAdapter);
        rekeaseAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!rekeaseAdapter.isSelected.get(position)) {
                    rekeaseAdapter.isSelected.put(position, true); // 修改map的值保存状态
                    rekeaseAdapter.notifyItemChanged(position);
                    selectDatas.add(dataList.get(position));

                } else {
                    rekeaseAdapter.isSelected.put(position, false); // 修改map的值保存状态
                    rekeaseAdapter.notifyItemChanged(position);
                    selectDatas.remove(dataList.get(position));
                }
            }
        });
    }

    @Override
    public void onSchoolClassGroupFailed(String msg) {
        ToastUtils.showShort(msg);
    }


    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }


    @Override
    public String getIdentifier() {
        return null;
    }
}
