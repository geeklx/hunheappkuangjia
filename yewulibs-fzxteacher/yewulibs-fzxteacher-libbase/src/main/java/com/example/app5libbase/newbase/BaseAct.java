package com.example.app5libbase.newbase;

import android.view.View;
import android.view.ViewGroup;

import com.example.app5libbase.R;
import com.just.agentweb.base.BaseCurrencyAgentWebActivity;

public class BaseAct extends BaseCurrencyAgentWebActivity {
    /*绑定控件*/
    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = this.findViewById(R.id.ll_base_container);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }
}
