package com.example.app5libbase.base;

import android.app.Activity;
import android.content.Intent;

import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.msg.MqttService;
import com.example.app5libbase.service.TimerService;
import com.sdzn.fzx.student.libutils.app.App2;

import net.ossrs.yasea.SimplePublisher;

import java.util.ArrayList;

/**
 * 描述：维护Activity栈
 * <p>
 * 创建人：zhangchao
 * 创建时间：16/7/28
 */
public class ActivityManager {

    private static final ArrayList<Activity> liveActivityList = new ArrayList<Activity>();
    private static final ArrayList<Activity> visibleActivityList = new ArrayList<Activity>();
    private static final ArrayList<Activity> foregroundActivityList = new ArrayList<Activity>();

    public static void addLiveActivity(Activity Activity) {
        if (!liveActivityList.contains(Activity)) {
            liveActivityList.add(Activity);
        }
    }

    public static void addVisibleActivity(Activity Activity) {
        if (!visibleActivityList.contains(Activity)) {
            visibleActivityList.add(Activity);
        }
    }

    public static void removeLiveActivity(Activity activity) {


        liveActivityList.remove(activity);
        visibleActivityList.remove(activity);
        foregroundActivityList.remove(activity);
    }

    public static void removeVisibleActivity(Activity Activity) {
        visibleActivityList.remove(Activity);
    }

    public static void addForegroundActivity(Activity Activity) {
        if (!foregroundActivityList.contains(Activity)) {
            foregroundActivityList.add(Activity);
        }
    }

    public static void removeForegroundActivity(Activity Activity) {
        foregroundActivityList.remove(Activity);
    }

    public static void finishAll() {
        for (Activity activity : liveActivityList) {
            activity.finish();
        }
        for (Activity activity : visibleActivityList) {
            activity.finish();
        }
        for (Activity activity : foregroundActivityList) {
            activity.finish();
        }
    }

    public static void exit() {
        SimplePublisher.getInstance().stop();
        App2.get().stopService(new Intent( App2.get(), TimerService.class));
         App2.get().stopService(new Intent( App2.get(), MqttService.class));
        if (TimeBaseUtil.getInstance().mMediaProjection != null) {
            TimeBaseUtil.getInstance().mMediaProjection.stop();
            TimeBaseUtil.getInstance().mMediaProjection = null;
        }
        finishAll();
    }

    public static Activity getForegroundActivity() {
        if (liveActivityList.size() > 0) {
            return liveActivityList.get(liveActivityList.size() - 1);
        }
        return null;
    }
}
