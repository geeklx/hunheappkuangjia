package com.example.app5libbase.ai.pop.bean;

/**
 * Created by user on 28-06-2018.
 */

public class TextList {
    public String classId;
    public String groupId;
    public String groupName;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}

