package com.example.app5libbase.login.presenter;

import android.os.Handler;
import android.os.Message;

import com.example.app5libbase.login.view.VerifyNumViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.Api;

import java.io.InputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * VerifyNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VerifyNumPresenter1 extends Presenter<VerifyNumViews> {

    public void QueryImgToken() {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImgToken(BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/valicode/imgToken")
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnImgTokenFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnImgTokenSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnImgTokenFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    public void QueryImg(String imgToken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImg(BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/valicode/img", imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        InputStream io = response.body().byteStream();
                        getView().OnImgSuccess(io);
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnImgFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    /*发送验证码*/
    public void sendVerityCode(String phone,String imgcode,String imgtoken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .sendYZM(BuildConfig3.SERVER_ISERVICE_NEW2 + "/student/login/sendVerifyCode", phone,imgcode,imgtoken)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onVerityCodeFailed(response.body().getMessage());
                            return;
                        }
                        getView().onVerityCodeSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onVerityCodeFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    /*验证验证码*/
    public void checkVerityNum(final String phoneNum, final String code) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .checkYZM(BuildConfig3.SERVER_ISERVICE_NEW2 + "/teacher/check/smsToken", phoneNum, code)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCheckVerityFailed(response.body().getMessage());
                            return;
                        }
                        if (String.valueOf(response.body().getResult()).equals("true")) {
                            getView().onCheckVeritySuccess(phoneNum, code);
                        } else {
                            getView().onCheckVerityFailed(response.body().getMessage());
                        }

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCheckVerityFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
