package com.example.app5libbase.newbase;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.CustomDrawerPopupView;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.views.CalenderClearEditText;
import com.example.app5libbase.views.ClearableEditText;
import com.example.app5libbase.views.XRecyclerView;
import com.example.baselibrary.base.BaseAppManager;
import com.just.agentweb.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.view.ExpandViewRect;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;

import java.util.Calendar;

import me.jessyan.autosize.AutoSizeCompat;

public abstract class BaseActWebActivity1 extends BaseAct {
    public TextView tvBack;//返回
    public TextView tvTitleName;//标题名称
    public XRecyclerView recyclerViewTitle;//RecyclerView滑动选择
    public TextView tvMyTitle;//个人中心按钮
    public TextView tvSousuoTitle;//搜索按钮
    public TextView tvWeizhi;//时间按钮
    public TextView tvTijiao;//提交按钮
    public TextView tvViewStatistics;//查看统计
    public CalenderClearEditText tvZankaiShijian;//时间选择器Edtext

    private long mCurrentMs = System.currentTimeMillis();
    public Activity activity;
    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("activityRefresh".equals(intent.getAction()) && !TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.example.app5home.MainActivityIndex")) {
                    final LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(activity)
                            .dismissOnBackPressed(false)
                            .asLoading("加载中")
                            .show();
                    loadingPopup.delayDismissWith(500, new Runnable() {
                        @Override
                        public void run() {
                            loadingPopup.destroy();
                        }
                    });
                    if (mAgentWeb != null && !mAgentWeb.getWebCreator().getWebView().getUrl().equals(BuildConfig3.SERVER_ISERVICE_NEW1 + "/setToken")) {
                        mAgentWeb.getUrlLoader().reload();
                    }
//                   String weburl= mAgentWeb.getWebCreator().getWebView().getUrl().substring(mAgentWeb.getWebCreator().getWebView().getUrl().lastIndexOf("#/"));
//                    if (mAgentWeb != null&&!weburl.equals("#/setToken")) {
//                        loadWebSite(mAgentWeb.getWebCreator().getWebView().getUrl());
//                    }
                }
            } catch (Exception e) {
            }
        }
    }


    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        activity = this;
        TimeBaseUtil.getInstance().init(activity);
        BaseAppManager.getInstance().add(this);
        final LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(this)
                .dismissOnBackPressed(false)
                .asLoading("加载中")
                .show();
        loadingPopup.delayDismissWith(500, new Runnable() {
            @Override
            public void run() {
                loadingPopup.destroy();
            }
        });
        setup(savedInstanceState);
    }

    /*设置属性*/
    protected void setup(@Nullable Bundle savedInstanceState) {
        tvBack = (TextView) findViewById(R.id.tv_back);//返回
        tvTitleName = (TextView) findViewById(R.id.tv_title_name);//标题名称
        recyclerViewTitle = (XRecyclerView) findViewById(R.id.recycler_view_title);//recyclerVie滑动
        tvViewStatistics=findViewById(R.id.tv_view_statistics);//查看统计
        tvSousuoTitle = (TextView) findViewById(R.id.tv_sousuo_title);//搜索按钮
        tvWeizhi = (TextView) findViewById(R.id.tv_weizhi);//时间按钮
        tvZankaiShijian = (CalenderClearEditText) findViewById(R.id.tv_zankai_shijian);//时间选择器Edtext
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);//个人中心按钮
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);
        clickListener();
    }

    /*加载布局*/
    protected abstract int getLayoutId();


    /*设置标题*/
    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "");
    }

    protected void setTitleContent(String title, String content) {
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 20) {
                title = title.substring(0, 20).concat("...");
            }
        }
        if (TextUtils.isEmpty(content)) {
            if (tvTitleName != null) {
                tvTitleName.setText(title);
            }
            return;
        }
        if (TextUtils.equals(title, content)) {
            if (tvTitleName != null) {
                tvTitleName.setText(title);
            }
        } else {
            if (tvTitleName != null) {
                tvTitleName.setText(content);
            }
        }
    }


    private BaseOnClickListener mListener;

    public void setBaseOnClickListener(BaseOnClickListener listener) {
        mListener = listener;
    }

    CustomDrawerPopupView customDrawerPopupView;

    //个人中心
    public void Titlegrzx() {
        customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {

            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(false) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .setPopupCallback(new SimpleCallback() {
                    @Override
                    public void onCreated(BasePopupView popupView) {
                        super.onCreated(popupView);
                    }

                    @Override
                    public void onShow(BasePopupView popupView) {
                        super.onShow(popupView);
                        Log.e("tag", "onShow");
                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {
                        super.onDismiss(popupView);
                        Log.e("tag", "onDismiss");
                    }

                    //如果你自己想拦截返回按键事件，则重写这个方法，返回true即可
                    @Override
                    public boolean onBackPressed(BasePopupView popupView) {
//                        ToastUtils.showShort("我拦截的返回按键，按返回键XPopup不会关闭了");
                        return true;
                    }
                })
                .asCustom(customDrawerPopupView)
                .show();
    }


    /*返回*/
    public void TitleBack() {
        finish();
        Intent msgIntent = new Intent();
        msgIntent.setAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
    }

    //时间
    public void Titleshijian() {
        showCalendarDialog();
    }

    //展开时间
    public void Titlezankaishijian() {
    }

    /*搜索*/
    public void Titlesousuo() {
    }
    /*查看统计*/
    public void ViewStatistics() {
    }
    /*时间选择器-------------------------------------开始*/
    private CalenderDialog calendarDialog;//日历dialog

    public String startTime;//开始时间
    public String endTime;//结束时间

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(this, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    tvZankaiShijian.setVisibility(View.VISIBLE);
                    tvWeizhi.setVisibility(View.GONE);
                    tvZankaiShijian.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  ~  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    startTime = String.valueOf(startCalendar.getTimeInMillis());
                    endTime = String.valueOf(endCalendar.getTimeInMillis());
                    listener.returnRefresh(startTime, endTime);
                }
            });
        }
        calendarDialog.show();

    }

    //回调时间数据
    private static refreshOnDisplayListener listener;

    public interface refreshOnDisplayListener {
        void returnRefresh(String startTime, String endTime);
    }

    public static void setOnDisplayRefreshListener(refreshOnDisplayListener myListener) {
        listener = myListener;
    }

    /*时间选择器-------------------------------------结束*/
    @Override
    public void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();//恢复
        }
        super.onResume();
        TimeBaseUtil.getInstance().resume(activity);
    }

    /**
     * activity是否显示 initpop    false即 不显示, 默认是显示
     */

    public void setReturnPop(boolean isShowPop) {
        TimeBaseUtil.getInstance().setReturnPop(isShowPop);
    }

    @Override
    public void onPause() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause(); //暂停应用内所有WebView ， 调用mWebView.resumeTimers();/mAgentWeb.getWebLifeCycle().onResume(); 恢复。
        }
        super.onPause();
        if (customDrawerPopupView != null && customDrawerPopupView.isShow()) {
            customDrawerPopupView.dismiss();
        }
        TimeBaseUtil.getInstance().dismissCountDownPop();
    }

    @Override
    protected void onDestroy() {
        BaseAppManager.getInstance().remove(this);
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
        TimeBaseUtil.getInstance().destroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }


    /*点击事件添加回调*/
    protected void clickListener() {
        if (tvZankaiShijian != null) {
            tvZankaiShijian.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    tvWeizhi.setVisibility(View.VISIBLE);
                    tvZankaiShijian.setVisibility(View.GONE);
                }
            });
        }
        /*查看统计*/
        if (tvViewStatistics != null) {
            tvViewStatistics.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.ViewStatistics();
                    }
                }
            });
        }
        /*返回点击*/
        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleBack();
                    }
                }
            });
            ExpandViewRect.expandViewTouchDelegate(tvBack, 5, 5, 20, 20);
        }
        //个人中心
        if (tvMyTitle != null) {
            tvMyTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlegrzx();
                    }
                }
            });
        }
        /*时间点击时间*/
        if (tvWeizhi != null) {
            tvWeizhi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titleshijian();
                    }
                }
            });
        }
        /*搜索点击事件*/
        if (tvSousuoTitle != null) {
            tvSousuoTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlesousuo();
                    }
                }
            });
        }
        /*展开时间*/
        if (tvZankaiShijian != null) {
            tvZankaiShijian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlezankaishijian();
                    }
                }
            });
        }

    }

    /**
     * @param LayoutStyle 返回判断
     */
    protected void TitleShowHideState(int LayoutStyle) {
        if (LayoutStyle == 1) { /*返回，Title，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 2) { /*返回，Title，个人中心，recuclerView*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 3) {   /*显示返回，标题*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 4) { /*返回，Title，个人中心，recuclerView,搜索*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            tvSousuoTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 5) {
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvViewStatistics.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
        }
    }

    public void isToken() {
        if (!TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.example.app5libbase.login.activity.LoginActivity")) {
            ActivityUtils.finishAllActivities();
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        TimeBaseUtil.getInstance().activityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_MEDIA_PROJECTION_CAPTURE) {
//            TimeBaseUtil.getInstance().mMediaProjection = TimeBaseUtil.getInstance().mMediaProjectionManager.getMediaProjection(resultCode, data);
//            TimeBaseUtil.getInstance().getScreen();
//
//        } else if (requestCode == REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN) {
//            TimeBaseUtil.getInstance().mMediaProjection = TimeBaseUtil.getInstance().mMediaProjectionManager.getMediaProjection(resultCode, data);
//            TimeBaseUtil.getInstance().getBaiBANScreen();
//        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
