package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.TeachClassVo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class TeachStatisticsTopAdapter extends BaseRcvAdapter<TeachClassVo.DataBean> {

    public TeachStatisticsTopAdapter(Context context) {
        super(context, new ArrayList<TeachClassVo.DataBean>());
    }

    public void clear() {
        mList.clear();
    }

    public void add(List<TeachClassVo.DataBean> list) {
        mList.addAll(list);
    }

    public TeachClassVo.DataBean get(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = BaseViewHolder.get(context, null, parent, R.layout.item_statistics_top);
        setListener(parent, holder, viewType);
        return holder;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, TeachClassVo.DataBean bean) {

        TextView tv = holder.getView(R.id.name);
        ImageView image = holder.getView(R.id.image);
        View view = holder.getView(R.id.view);

        tv.setText(bean.getBaseGradeName() + bean.getClassName() + "");
        if (bean.isSelected()){
            image.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            tv.setTextColor(Color.parseColor("#5BA2EA"));
        }else {
            image.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            tv.setTextColor(Color.parseColor("#323C47"));

        }



    }


}
