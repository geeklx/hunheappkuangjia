package com.example.app5libbase.listener.chatroom;

import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;

import java.util.List;

public interface OnDiscussionSaveListener {

    void onEdit(String id,String title,String content,String endTime,List<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> list);

    void onPreview(String id,String title,String content,String endTime,List<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> list);

    void onDelete(String id);

    void onPublish(String id);
}
