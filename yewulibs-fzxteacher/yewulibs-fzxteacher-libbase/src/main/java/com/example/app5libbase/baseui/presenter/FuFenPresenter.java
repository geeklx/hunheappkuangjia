package com.example.app5libbase.baseui.presenter;

import com.example.app5libbase.baseui.activity.FuFenActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.baseui.view.FuFenView;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.FuFenBean;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/17.
 */
public class FuFenPresenter extends BasePresenter<FuFenView,FuFenActivity> {

    public void getClassGroupList( String classId) {
        Network.createTokenService(NetWorkService.GetClassGroupingService.class)
                .GetGroupStudent(classId,String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()))
                .map(new StatusFunc<FuFenBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<FuFenBean>(new SubscriberListener<FuFenBean>() {
                    @Override
                    public void onNext(FuFenBean classGroupingVo) {
                        mView.getClassGroupDataSuccess(classGroupingVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.onError(status.getMsg());
                            } else {
                                mView.onError("数据获取失败");
                            }
                        } else {
                            mView.onError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));


    }
    public void  addFuFen(Map<String, Object> params){

        Network.createTokenService(NetWorkService.ToolsService.class)
                .getAddFuFen(params)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>(){

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable!=null) {
//                            mView.onError(throwable.toString());
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        mView.getAddSuccess();
                    }
                },mActivity,true,false,false,""));
    }



}
