package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.util.AndroidTeacherUtils;
import com.sdzn.fzx.student.libutils.util.UiUtils;

import java.lang.reflect.Method;

/**
 * 工具弹窗按钮
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class ToolButtonPop extends PopupWindow implements View.OnClickListener {

    private Activity activity;
    private ToolsPop toolsPop;
    private ImageView ivToolClose;

    public ToolButtonPop(final Activity activity) {
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_tool_button, null);
        ivToolClose = (ImageView) contentView.findViewById(R.id.ivToolClose);
        ivToolClose.setOnClickListener(this);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.setPopupWindowTouchModal(this, false);
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
//                if (activity instanceof MainActivity) {
//                    MainActivity mainActivity = (MainActivity) activity;
//                    showPopupWindow(mainActivity.getContentView(mainActivity));
//                    EventBus.getDefault().post(new OnBackEvent());
//                } else {
//                    activity.finish();
//                }
            }
        });
    }

    public void onClick(View view) {
        if (view.getId() == R.id.ivToolClose) {// TODO 显示工具列表
            if (toolsPop == null) {
                toolsPop = new ToolsPop(activity);
            }
            toolsPop.showPopupWindow(TimeBaseUtil.getInstance().getContentView(activity));
        }
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(final View parent) {
        if (!this.isShowing()) {
            this.showAtLocation(parent, Gravity.NO_GRAVITY, UiUtils.dp2px(11), AndroidTeacherUtils.getWindowHeight(activity) - UiUtils.dp2px(58));
        } else {
            this.dismiss();
        }
    }

    public void dismissToolsPop() {
        if (this.isShowing()) {
            this.dismiss();
        }
    }

    private void setPopupWindowTouchModal(PopupWindow popupWindow, boolean touchModal) {
        if (null == popupWindow) {
            return;
        }
        try {
            Method method = PopupWindow.class.getDeclaredMethod("setTouchModal",
                    boolean.class);
            method.setAccessible(true);
            method.invoke(popupWindow, touchModal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}