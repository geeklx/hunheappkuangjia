package com.example.app5libbase.ai.pop.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.LogUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.bean.SchoolClassGroupBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
public class RekeaseAdapter extends RecyclerView.Adapter<RekeaseAdapter.MyViewHolder> {
    private Context context;
    private List<SchoolClassGroupBean.GroupsBean> list;
    public static HashMap<Integer, Boolean> isSelected;


    public RekeaseAdapter(Context context, List<SchoolClassGroupBean.GroupsBean> list) {
        this.context = context;
        this.list = list;
        init();
    }

    // 初始化 设置所有item都为未选择
    public void init() {
        isSelected = new HashMap<Integer, Boolean>();
        for (int i = 0; i < list.size(); i++) {
            isSelected.put(i, false);
        }
    }
    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                context).inflate(R.layout.recycleview_movegroup_item1, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.checkbox.setText(list.get(position).getGroupName());
        holder.checkbox.setChecked(isSelected.get(position));
        holder.itemView.setSelected(isSelected.get(position));
        if (mOnItemClickLitener != null) {
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(holder.checkbox, holder.getAdapterPosition());
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }
    /**
     * ViewHolder的类，用于缓存控件
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkbox;

        //因为删除有可能会删除中间条目，然后会造成角标越界，所以必须整体刷新一下！
        public MyViewHolder(View view) {
            super(view);
            checkbox = (CheckBox) view.findViewById(R.id.checkbox);
        }
    }
}

/*public class RekeaseAdapter extends BaseQuickAdapter<SchoolClassGroupBean.GroupsBean, BaseViewHolder> {
    Set<Integer> mutilSelectedList =new TreeSet<>();
    public RekeaseAdapter() {
        super(R.layout.recycleview_movegroup_item1);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final SchoolClassGroupBean.GroupsBean item) {
        final TextView checkbox = helper.itemView.findViewById(R.id.checkbox);
        checkbox.setText(String.valueOf(item.getGroupName()));
        checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mutilSelectedList.contains(helper.getAdapterPosition())) {
                    mutilSelectedList.remove(helper.getAdapterPosition());
                    checkbox.setSelected(false);
                    helper.itemView.setSelected(item.isSelected());
                    LogUtils.e("取消选中");
                } else {
                    mutilSelectedList.add(helper.getAdapterPosition());
                    checkbox.setSelected(true);
                    helper.itemView.setSelected(item.isSelected());
                    LogUtils.e("选中");
                }
            }
        });
    }

    @Override
    public void remove(int position) {
        super.remove(position);
    }
}*/
