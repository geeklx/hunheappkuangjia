package com.example.app5libbase.ai.pop;

import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.lxj.xpopup.core.CenterPopupView;
import com.sdzn.fzx.student.libutils.view.MyRadioGroup;
import com.sdzn.fzx.teacher.presenter.ResourcesPresenter;
import com.sdzn.fzx.teacher.view.ResourcesViews;

import java.util.Calendar;
import java.util.Date;

/**
 * 课堂检测发布
 */
public class QuestionsReleasePopupView extends CenterPopupView implements ResourcesViews {
    private TextView tvTitle;
    private TextView tvCancel;
    private TextView tvConfirm;
    private MyRadioGroup radiogroup;
    private RadioButton radioButton, radioButton2, radioButton3, radioButton4;
    private EditText EditQuestions;
    private Context mContext;
    private String answerType = null;
    private String ClassesId;
    private String lessonLibCourseId;
    private String lessonTaskId;
    private String discusstime = null;
    private String startTime;
    private String endTime;
    ResourcesPresenter resourcesPresenter;

    public QuestionsReleasePopupView(@NonNull Context context, String ClassesId, String lessonLibCourseId, String lessonTaskId) {
        super(context);
        this.mContext = context;
        this.ClassesId = ClassesId;
        this.lessonLibCourseId = lessonLibCourseId;
        this.lessonTaskId = lessonTaskId;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_center_yewu_questions;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Log.e("tag", "CustomDrawerPopupView onCreate");
        resourcesPresenter = new ResourcesPresenter();
        resourcesPresenter.onCreate(this);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);
        EditQuestions = (EditText) findViewById(R.id.edit_questions);
        radiogroup = findViewById(R.id.radiogroup);
        radioButton = findViewById(R.id.radioButton);
        radioButton2 = findViewById(R.id.radioButton2);
        radioButton3 = findViewById(R.id.radioButton3);
        radioButton4 = findViewById(R.id.radioButton4);

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // 判断选中哪个
                if (checkedId == R.id.radioButton) {
                    answerType = "2";
                } else if (checkedId == R.id.radioButton2) {
                    answerType = "3";
                } else if (checkedId == R.id.radioButton3) {
                    answerType = "1";
                } else if (checkedId == R.id.radioButton4) {
                    answerType = "4";
                }
            }
        });
        tvCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                discusstime = EditQuestions.getText().toString().trim();
                if (discusstime == null) {
                    ToastUtils.showShort("请输入讨论时长");
                    return;
                } else if (answerType == null) {
                    ToastUtils.showShort("请选择答案公布方式");
                    return;
                } else {
                    Calendar ca = Calendar.getInstance();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        startTime = formatDate(ca.getTime());//TimeUtils.getNowString();
                        ca.set(Calendar.MINUTE, ca.get(Calendar.MINUTE) + Integer.parseInt(discusstime));
                        endTime = formatDate(ca.getTime());
                    }
//                    ToastUtils.showShort(startTime + "---------" + endTime);
                    resourcesPresenter.AddResources("Bearer " + (String) SPUtils.getInstance().getString("token", ""),
                            ClassesId, lessonLibCourseId, lessonTaskId, answerType, startTime, endTime);
                }
                dismiss();
            }
        });
        initRv();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

    private void initRv() {

    }

    @Override
    public void onResourcesSuccess(String str) {
        ToastUtils.showShort("发布成功");
    }

    @Override
    public void onResourcesFail(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
