package com.example.app5libbase.views.graffiti.core;

public interface IGraffitiPen {
    /**
     * 深度拷贝
     */
    public IGraffitiPen copy();

}
