package com.example.app5libbase.baseui.presenter;

import android.view.View;

import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.baseui.view.ScoreView;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.GroupVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class ScorePresenter extends BasePresenter<ScoreView, BaseActivity> {

    private Subscription subscribe1;
    private Subscription subscribe2;
    private SyncClassVo syncClassVoData;
    private String classId;
    private String className;

    private String type = "1";

    public void getGroupList(final String type) {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        subscribe1 = Network.createTokenService(NetWorkService.MainService.class)
                .getClassList(String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()))
                .map(new StatusFunc<SyncClassVo>())
                .flatMap(new Func1<SyncClassVo, Observable<StatusVo<GroupVo>>>() {
                    @Override
                    public Observable<StatusVo<GroupVo>> call(SyncClassVo syncClassVo) {
                        syncClassVoData = syncClassVo;
                        Map<String, String> params = new HashMap<>();
                        if (syncClassVo != null && syncClassVo.getData() != null && syncClassVo.getData().size() > 0) {
                            SyncClassVo.DataBean dataBean = syncClassVo.getData().get(0);
                            classId = dataBean.getClassId();
                            className = dataBean.getClassName();
                            params.put("classId", classId);
                            params.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
                            params.put("type", type);
                            params.put("timeStyle", "0");
                        }
                        return Network.createTokenService(NetWorkService.ScoreService.class).getGroupList(params);
                    }
                })
                .map(new StatusFunc<GroupVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<GroupVo>(new SubscriberListener<GroupVo>() {
                    @Override
                    public void onNext(GroupVo groupVo) {
                        mView.getGroupListSuccess(groupVo);
                        mView.getClassVoSuccess(syncClassVoData);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe1);
    }

    public void setClassId(String classId) {
        if (classId == null) {
            return;
        }
        this.classId = classId;
    }

    public void getGroupListWithClassId(final String type) {
        this.type = type;
        if (subscribe2 != null && subscribe2.isUnsubscribed()) {
            subscribe2.unsubscribe();
            subscriptions.remove(subscribe2);
        }
        Map<String, String> params = new HashMap<>();
        params.put("classId", classId);
        params.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
        params.put("type", type);
        params.put("timeStyle", "0");
        subscribe2 = Network.createTokenService(NetWorkService.ScoreService.class)
                .getGroupList(params)
                .map(new StatusFunc<GroupVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<GroupVo>(new SubscriberListener<GroupVo>() {
                    @Override
                    public void onNext(GroupVo groupVo) {
                        mView.getGroupListSuccess(groupVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe2);
    }

    public void bindScoreHolder(GeneralRecyclerViewHolder holder, GroupVo.DataBean itemData) {
        if (itemData != null) {
            holder.setText(R.id.tvLogo, itemData.getClassGroupName().substring(0, 1));
            holder.setText(R.id.tvName, itemData.getClassGroupName());
            holder.setText(R.id.tvTotal, String.valueOf(itemData.getPoints()));
            holder.setText(R.id.tvCount, String.valueOf(itemData.getPointsCount()));
        } else {
            holder.setText(R.id.tvLogo, "无");
            holder.setText(R.id.tvName, "暂无");
            holder.setText(R.id.tvTotal, "0");
            holder.setText(R.id.tvCount, "0");
        }

        MyOnClickListener myOnClickListener = new MyOnClickListener(itemData);
        holder.getChildView(R.id.rbPlus1).setOnClickListener(myOnClickListener);
        holder.getChildView(R.id.rbPlus2).setOnClickListener(myOnClickListener);
        holder.getChildView(R.id.rbPlus3).setOnClickListener(myOnClickListener);
        holder.getChildView(R.id.rbReduce1).setOnClickListener(myOnClickListener);
        holder.getChildView(R.id.rbReduce2).setOnClickListener(myOnClickListener);
        holder.getChildView(R.id.rbReduce3).setOnClickListener(myOnClickListener);
    }

    private class MyOnClickListener implements View.OnClickListener {

        private GroupVo.DataBean itemData;

        public MyOnClickListener(GroupVo.DataBean itemData) {
            this.itemData = itemData;
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.rbPlus1) {
                modifyPoint(String.valueOf(itemData.getClassGroupId()), itemData.getClassGroupName(), "1");
            } else if (id == R.id.rbPlus2) {
                modifyPoint(String.valueOf(itemData.getClassGroupId()), itemData.getClassGroupName(), "2");
            } else if (id == R.id.rbPlus3) {
                modifyPoint(String.valueOf(itemData.getClassGroupId()), itemData.getClassGroupName(), "3");
            } else if (id == R.id.rbReduce1) {
                modifyPoint(String.valueOf(itemData.getClassGroupId()), itemData.getClassGroupName(), "-1");
            } else if (id == R.id.rbReduce2) {
                modifyPoint(String.valueOf(itemData.getClassGroupId()), itemData.getClassGroupName(), "-2");
            } else if (id == R.id.rbReduce3) {
                modifyPoint(String.valueOf(itemData.getClassGroupId()), itemData.getClassGroupName(), "-3");
            }
        }
    }

    private void modifyPoint(String groupId, String groupName, String point) {
        Map<String, String> params = new HashMap<>();
        params.put("subjectId", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()));
        params.put("subjectName", String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectName()));
        params.put("classId", classId);
        params.put("className", className);
        params.put("groupId", groupId);
        params.put("groupName", groupName);
        params.put("type", type);
        params.put("points", point);
        Subscription subscribe = Network.createTokenService(NetWorkService.ScoreService.class)
                .modifyPoint(params)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        mView.modifyPointSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }
}