package com.example.app5libbase.login.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.login.presenter.ForgetPswNumPresenter1;
import com.example.app5libbase.login.presenter.VerifyNumPresenter1;
import com.example.app5libbase.login.view.ForgetPswNumViews;
import com.example.app5libbase.login.view.VerifyNumViews;
import com.example.app5libbase.util.DialogUtils;
import com.example.app5libbase.util.YanzhengUtil;
import com.sdzn.fzx.student.libutils.util.StringUtils;

import java.io.InputStream;

/**
 * 忘记密码
 * A simple {@link Fragment} subclass.
 */
public class VerifyNumFragment extends BaseFragment implements VerifyNumViews, ForgetPswNumViews, View.OnClickListener {
    VerifyNumPresenter1 mPresenter;
    ForgetPswNumPresenter1 forgetPswNumPresenter1;
    private ImageView Effectiveness;//图形码
    private TextView TitleCount;//标题名称
    private TextView countDownBtn;//获取验证码按钮
    private LinearLayout back;//返回
    private EditText verityNumEdit;//验证码输入框
    private EditText userNumEdit;//手机号输入框
    private EditText valicodeEdit;//图形验证码输入框
    private Button verityNumIcon;//验证码图标
    private Button userNumIcon;//手机号图标
    private Button nextBtn;//验证码图标
    private ImageView editDelImg;//手机号清空按钮


    public VerifyNumFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verify_num, container, false);
        initPresenter();
        findview(view);
        initView();
        onclick();
        return view;
    }

    public void initPresenter() {
        forgetPswNumPresenter1 = new ForgetPswNumPresenter1();
        forgetPswNumPresenter1.onCreate(this);

        mPresenter = new VerifyNumPresenter1();
        mPresenter.onCreate(this);
    }


    private void findview(View view) {
        TitleCount = view.findViewById(R.id.tv_title_count);
        countDownBtn = view.findViewById(R.id.count_down_btn);
        back = view.findViewById(R.id.ll_back);
        verityNumEdit = view.findViewById(R.id.verity_num_edit);
        userNumEdit = view.findViewById(R.id.user_num_edit);
        valicodeEdit = view.findViewById(R.id.valicode_edit);
        verityNumIcon = view.findViewById(R.id.verity_num_icon);
        userNumIcon = view.findViewById(R.id.user_num_icon);
        nextBtn = view.findViewById(R.id.next_btn);
        editDelImg = view.findViewById(R.id.edit_del_img);
        Effectiveness = view.findViewById(R.id.iv_effectiveness);
        TitleCount.setText("忘记密码");
    }

    private void onclick() {
        back.setOnClickListener(this);
        editDelImg.setOnClickListener(this);
        Effectiveness.setOnClickListener(this);
        countDownBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        userNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                userNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        verityNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                verityNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void initView() {
        mPresenter.QueryImgToken();
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestory();
        forgetPswNumPresenter1.onDestory();
        YanzhengUtil.timer_des();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ll_back) {
            getActivity().finish();
        } else if (id == R.id.edit_del_img) {
            userNumEdit.setText("");
        } else if (id == R.id.count_down_btn) {
            final String userName = userNumEdit.getText().toString();
            if (userName.equals("")) {
                ToastUtils.showShort("手机号不能为空");
                return;
            }
            forgetPswNumPresenter1.verityUserName(userName);
        } else if (id == R.id.next_btn) {
            if (userNumEdit.getText().toString().trim() == null) {
                ToastUtils.showShort("手机号不能为空");
                return;
            } else if (!StringUtils.isMobile(userNumEdit.getText().toString().trim()) && userNumEdit.getText().toString().trim().length() != 11) {
                ToastUtils.showShort("请输入正确的账号或手机号");
                return;
            } else if (valicodeEdit.getText().toString().trim() == null) {
                ToastUtils.showShort("图形验证码不能为空");
                return;
            } else if (verityNumEdit.getText().toString().trim() == null) {
                ToastUtils.showShort("验证码不能为空");
                return;
            }
            mPresenter.checkVerityNum(userNumEdit.getText().toString().trim(), verityNumEdit.getText().toString());
        } else if (id == R.id.iv_effectiveness) {
            mPresenter.QueryImgToken();
        }
    }

    /*验证码返回状态*/
    @Override
    public void onVerityCodeSuccess(Object success) {
        ToastUtils.showShort(String.valueOf(success));
        YanzhengUtil.startTime(60 * 1000, countDownBtn);//倒计时bufen
    }

    @Override
    public void onVerityCodeFailed(String msg) {
        valicodeEdit.setText("");
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    @Override
    public void onCheckVeritySuccess(final String phone, final String code) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        ResetPswFragment fragment = new ResetPswFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ResetPswFragment.USER_PHONE, phone);
        bundle.putString(ResetPswFragment.VERIFT_CODE, code);
        fragment.setArguments(bundle);

        transaction.replace(R.id.forget_psw_fragment, fragment);
        transaction.commit();
        transaction.addToBackStack("tag");
    }

    /*验证验证码失败*/
    @Override
    public void onCheckVerityFailed(String msg) {
        ToastUtils.showShort(msg);
        valicodeEdit.setText("");
        mPresenter.QueryImgToken();
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        Bitmap bitimg = BitmapFactory.decodeStream(inputStream);
        Glide.with(getActivity()).load(bitimg).into(Effectiveness);
    }

    @Override
    public void OnImgFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void OnImgTokenFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void verifySuccess(String tel) {
        if (tel.equals("{}")) {
            new DialogUtils().createTipDialog(getActivity()).buildText("该账号未绑定手机号，\n" +
                    "请联系学校管理员找回密码。").show();
            return;
        }
        if (userNumEdit.getText().toString().trim().equals("")) {
            ToastUtils.showShort("手机号不能为空");
            return;
        } else if (valicodeEdit.getText().toString().trim().equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
            return;
        }
        mPresenter.sendVerityCode(tel, valicodeEdit.getText().toString().trim(), imgToken);
    }


    private long mCurrentMs = System.currentTimeMillis();

    @Override
    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
}
