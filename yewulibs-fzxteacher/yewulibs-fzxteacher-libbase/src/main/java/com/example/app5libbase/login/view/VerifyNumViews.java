package com.example.app5libbase.login.view;


import com.haier.cellarette.libmvp.mvp.IView;

import java.io.InputStream;

/**
 * VerifyNumView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface VerifyNumViews extends IView {
    void onVerityCodeSuccess(Object success);
    void onVerityCodeFailed(String msg);

    void onCheckVeritySuccess(final String phone, final String code);
    void onCheckVerityFailed(String msg);

    void OnImgTokenSuccess(Object imgtoken);
    void OnImgTokenFailed(String msg);

    void OnImgSuccess(InputStream img);
    void OnImgFailed(String msg);

//    void onFailed(String msg);
}
