package com.example.app5libbase.ai.pop.adapter;

import android.view.View;

public interface OnItemClickLitener {
	void onItemClick(View view, int position);
}