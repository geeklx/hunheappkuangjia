package com.example.app5libbase.util;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.app5libbase.R;

/**
 * DialogUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class DialogUtils {

    private TextView confirm;
    private TextView content;
    private AlertDialog alertDialog;

    public DialogUtils createTipDialog(Context context) {
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_i_know_layout, null);
        alertDialog = new AlertDialog.Builder(context).setView(view).create();
        alertDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        content = (TextView) view.findViewById(R.id.tip_content_txt);
        confirm = (TextView) view.findViewById(R.id.sure_btn);
        alertDialog.setCancelable(false);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        return this;
    }

    public Dialog buildText(String contentText, String sureTxt) {
        content.setText(contentText);

        if (TextUtils.isEmpty(sureTxt)) {
            confirm.setText("确定");
        } else {
            confirm.setText(sureTxt);
        }
        return alertDialog;
    }

    public Dialog buildText(String contentText) {
        return buildText(contentText, null);
    }

}
