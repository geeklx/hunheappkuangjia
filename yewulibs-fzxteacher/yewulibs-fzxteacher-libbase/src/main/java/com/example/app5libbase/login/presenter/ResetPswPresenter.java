package com.example.app5libbase.login.presenter;

import android.text.TextUtils;
import android.util.Base64;

import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.login.activity.ForgetPswActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.login.view.ResetPswView;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * ResetPswPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ResetPswPresenter extends BasePresenter<ResetPswView, ForgetPswActivity> {

    public void changePsw(final String phoneNum, final String code, String newPsw, String surePsw) {
        if (!verityPsw(newPsw, surePsw)) {
            return;
        }
        if ("/auth".equals(BuildConfig3.AUTH)) {
            newPsw = Base64.encodeToString(newPsw.getBytes(),Base64.NO_WRAP);
            surePsw = Base64.encodeToString(surePsw.getBytes(),Base64.NO_WRAP);
        }

        Network.createService(NetWorkService.ForgetPswService.class)
                .checkVerityCode(phoneNum, code, newPsw, surePsw)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        mView.changePswSucced();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    private boolean verityPsw(final String newPsw, final String surePsw) {
        if (!TextUtils.equals(newPsw, surePsw)) {
            ToastUtil.showShortlToast("两次密码输入不一致");

            return false;
        }

        if (!StringUtils.vertifyPsw(newPsw)) {
            ToastUtil.showShortlToast("请输入6-16位数字、字母密码");
            return false;
        }

        return true;
    }

}
