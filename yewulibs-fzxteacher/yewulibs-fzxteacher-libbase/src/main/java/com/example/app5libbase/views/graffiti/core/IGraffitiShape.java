package com.example.app5libbase.views.graffiti.core;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * 图形
 */
public interface IGraffitiShape {

     /**
      * 绘制
      * @param paint 绘制的画笔
      */
     void draw(Canvas canvas, IGraffitiItem graffitiItem, Paint paint);

     /**
      * 深度拷贝
      */
     IGraffitiShape copy();
}
