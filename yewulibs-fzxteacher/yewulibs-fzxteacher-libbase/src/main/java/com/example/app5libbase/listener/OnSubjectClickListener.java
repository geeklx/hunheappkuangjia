package com.example.app5libbase.listener;

import com.sdzn.fzx.teacher.vo.LoginBean;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/2/1
 */
public interface OnSubjectClickListener {
    void onSubjectClick(LoginBean.DataBean.SubjectListBean subject);
}
