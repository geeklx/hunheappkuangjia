package com.example.app5libbase.base;

import android.os.Bundle;

import com.inthecheesefactory.thecheeselibrary.fragment.support.v4.app.NestedActivityResultFragment;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BaseFragment extends NestedActivityResultFragment {

    public BaseActivity activity;
//    protected Application appContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
//       appContext = activity.getApplication();
    }

    @Override
    public void onDestroy() {
        activity = null;
//        appContext = null;
        //  EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}