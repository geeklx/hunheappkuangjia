package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.dao.SearchHistory;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.controller.SearchController;
import com.example.app5libbase.listener.OnItemTouchListener;
import com.example.app5libbase.listener.OnSearchClickListener;
import com.example.app5libbase.util.InputTools;
import com.example.app5libbase.views.ClearEditText;
import com.example.app5libbase.views.ImageHintEditText;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class TaskSearchPop extends PopupWindow implements View.OnClickListener {
    private ClearEditText etSearch;
    private LinearLayout llHistory;
    private RecyclerView rvHistory;
    private TextView tvClear;

    private Activity activity;

    private List<SearchHistory> searchHistories;

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter historyAdapter;

    private OnSearchClickListener onSearchClickListener;

    private int subject;
    private String scene;

    public TaskSearchPop(Activity activity, OnSearchClickListener onSearchClickListener) {
        this.onSearchClickListener = onSearchClickListener;
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_task_search, null);

        etSearch = (ClearEditText) contentView.findViewById(R.id.etSearch);
        llHistory = (LinearLayout) contentView.findViewById(R.id.llHistory);
        rvHistory = (RecyclerView) contentView.findViewById(R.id.rvHistory);
        tvClear = (TextView) contentView.findViewById(R.id.tvClear);
        tvClear.setOnClickListener(this);
        this.setContentView(contentView);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));
        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                TaskSearchPop.this.onSearchClickListener.onTextChanged(etSearch.getText().toString());
                InputTools.hideKeyboard(etSearch);
            }
        });
        setEditorAction();
    }

    private void setHistorys() {
        searchHistories = SearchController.queryByTime(scene, subject);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvHistory.setLayoutManager(linearLayoutManager);
        addItems();
        historyAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvHistory.setAdapter(historyAdapter);
        if (searchHistories.size() > 0) {
            setHistoryVisible(true);
        } else {
            setHistoryVisible(false);
        }
        rvHistory.addOnItemTouchListener(new OnItemTouchListener(rvHistory) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder viewHolder) {
                SearchHistory searchHistory = searchHistories.get(viewHolder.getAdapterPosition());
                String searchStr = searchHistory.getSearchStr();
                TaskSearchPop.this.onSearchClickListener.onSearch(searchStr);
                etSearch.setText(searchStr);
                etSearch.setSelection(searchStr.length());
                dismiss();
            }
        });
    }

    private void addItems() {
        itemEntityList.clear();
        itemEntityList.addItems(R.layout.item_task_search_history, searchHistories)
                .addOnBind(R.layout.item_task_search_history, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        SearchHistory searchHistory = (SearchHistory) itemData;
                        holder.setText(R.id.tvHistory, searchHistory.getSearchStr());
                    }
                });
    }

    private void setHistoryVisible(boolean visible) {
        if (visible) {
            llHistory.setVisibility(View.VISIBLE);
        } else {
            llHistory.setVisibility(View.GONE);
        }
    }

    private void setEditorAction() {
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    // 搜索
                    String searchStr = etSearch.getText().toString().trim();
                    if (searchStr.length() > 0) {
                        TaskSearchPop.this.onSearchClickListener.onSearch(searchStr);

                        SearchHistory history = SearchController.saveHistory(scene, searchStr, subject);

                        List<SearchHistory> removeHistories = new ArrayList<>();
                        for (SearchHistory searchHistory : searchHistories) {
                            if (searchHistory.getSearchStr() != null && searchHistory.getSearchStr().equals(searchStr)) {
                                removeHistories.add(searchHistory);
                            }
                        }
                        searchHistories.removeAll(removeHistories);
                        if (searchHistories.size() > 9) {
                            searchHistories.remove(searchHistories.size() - 1);
                        }
                        searchHistories.add(0, history);
                    }
                    addItems();
                    historyAdapter.notifyDataSetChanged();
                    setHistoryVisible(true);
                    dismiss();
                    return true;
                }
                return false;
            }
        });
    }

    public void onClick(View view) {
        if (view.getId() == R.id.tvClear) {
            SearchController.clearHistory(searchHistories);
            searchHistories.clear();
            llHistory.setVisibility(View.GONE);
            historyAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(String scene, ImageHintEditText parent, int subject) {
        this.scene = scene;
        this.subject = subject;
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.setWidth(parent.getWidth());
            ViewGroup.LayoutParams etLayoutParams = etSearch.getLayoutParams();
            etLayoutParams.width = parent.getWidth();
            etSearch.setLayoutParams(etLayoutParams);
            ViewGroup.LayoutParams llLayoutParams = llHistory.getLayoutParams();
            llLayoutParams.width = parent.getWidth();
            llHistory.setLayoutParams(llLayoutParams);

            etSearch.setText(parent.getText());
            etSearch.setSelection(parent.getText().length());
            this.showAsDropDown(parent, 0, -parent.getHeight());
            InputTools.showKeyboard(etSearch);
            setHistorys();
        } else {
            this.dismiss();
        }
    }
}
