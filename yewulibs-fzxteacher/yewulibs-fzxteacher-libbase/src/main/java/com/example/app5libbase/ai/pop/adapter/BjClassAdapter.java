package com.example.app5libbase.ai.pop.adapter;

import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.bean.TextModel;
import com.example.app5libbase.ai.pop.bean.TextModel1;

public class BjClassAdapter extends BaseQuickAdapter<TextModel1, BaseViewHolder> {
    private String clickId = "";

    public BjClassAdapter() {
        super(R.layout.recycleview_movegroup_item);
    }

    //选中传过来的ID对应的item     也可以设置默认
    public void setClickPosition(String clickId) {
        this.clickId = clickId;
        notifyDataSetChanged();
    }

    @Override
    protected void convert(BaseViewHolder helper, TextModel1 item) {
        CheckBox checkbox = helper.itemView.findViewById(R.id.checkbox);
        checkbox.setText(String.valueOf(item.getText()));
        helper.addOnClickListener(R.id.checkbox);

        if (clickId == item.getText()) {
            checkbox.setChecked(true);
            helper.itemView.setSelected(true);
        } else {
            checkbox.setChecked(false);
            helper.itemView.setSelected(false);
        }
    }

    @Override
    public void remove(int position) {
        super.remove(position);
    }
}
