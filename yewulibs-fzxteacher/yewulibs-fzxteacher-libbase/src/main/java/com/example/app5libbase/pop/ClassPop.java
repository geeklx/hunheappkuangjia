package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.listener.OnClassClickListener;
import com.example.app5libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 学科弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class ClassPop extends PopupWindow {
    RecyclerView rvClass;

    private Activity activity;

    private List<SyncClassVo.DataBean> classVos = new ArrayList<>();

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter classAdapter;

    private OnClassClickListener onClassClickListener;

    public ClassPop(Activity activity, OnClassClickListener onClassClickListener) {
        this.onClassClickListener = onClassClickListener;
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_class, null);
        rvClass = contentView.findViewById(R.id.rvClass);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        setSubjects();
    }

    private void setSubjects() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvClass.setLayoutManager(linearLayoutManager);
        classAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvClass.setAdapter(classAdapter);
        rvClass.addOnItemTouchListener(new OnItemTouchListener(rvClass) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                SyncClassVo.DataBean dataBean = classVos.get(position);
                ClassPop.this.onClassClickListener.onClassClick(dataBean);
                dismiss();
            }
        });
    }

    private void addItems() {
        itemEntityList.clear();
        itemEntityList.addItems(R.layout.item_main_class, classVos)
                .addOnBind(R.layout.item_main_class, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        SyncClassVo.DataBean classVo = (SyncClassVo.DataBean) itemData;
                        holder.setText(R.id.tvClass, classVo.getBaseGradeName() + classVo.getClassName());
                    }
                });
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (classVos == null || classVos.size() == 0) {
            return;
        }
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }

    public void setClassVos(List<SyncClassVo.DataBean> syncClassVos) {
        if (syncClassVos != null && syncClassVos.size() > 0) {
            this.classVos.clear();
            this.classVos.addAll(syncClassVos);
            addItems();
            classAdapter.notifyDataSetChanged();
        }
    }

    public void setOnClassClickListener(OnClassClickListener onClassClickListener) {
        this.onClassClickListener = onClassClickListener;
    }
}
