package com.example.app5libbase.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.app5libbase.R;

/**
 * LoadStatusView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class LoadStatusView extends RelativeLayout {

    private View emptyView;

    private View errorView;

    private View loadingView;

    private RelativeLayout reloadRy;

    public LoadStatusView(Context context) {
        super(context);
        initView();
    }

    public LoadStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public LoadStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final View errorLayout = inflater.inflate(R.layout.error_layout, null);
        final View emptyLayout = inflater.inflate(R.layout.empty_layout, null);
        reloadRy = errorLayout.findViewById(R.id.reload_ry);
        reloadRy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reloadListener != null)
                    reloadListener.onReloadClicked();
            }
        });
        setEmptyView(emptyLayout);
        setErrorView(errorLayout);
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(this.emptyView, params);
    }

    public void setErrorView(View errorView) {
        this.errorView = errorView;
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(this.errorView, params);
    }

    public void setLoadingView(View loadingView) {
        this.loadingView = loadingView;
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(this.loadingView, params);
    }

    public void onEmpty() {
        setVisibility(View.VISIBLE);
        if (emptyView != null)
            emptyView.setVisibility(View.VISIBLE);
        if (errorView != null)
            errorView.setVisibility(View.GONE);
        if (loadingView != null)
            loadingView.setVisibility(View.GONE);

    }

    public void onError() {
        setVisibility(View.VISIBLE);
        if (emptyView != null)
            emptyView.setVisibility(View.GONE);
        if (errorView != null)
            errorView.setVisibility(View.VISIBLE);
        if (loadingView != null)
            loadingView.setVisibility(View.GONE);
    }

    public void onLoadding() {
        setVisibility(View.VISIBLE);
        if (emptyView != null)
            emptyView.setVisibility(View.GONE);
        if (errorView != null)
            errorView.setVisibility(View.GONE);
        if (loadingView != null)
            loadingView.setVisibility(View.VISIBLE);
    }

    public void onSuccess() {
        setVisibility(View.GONE);
    }

    public ReloadListener reloadListener;

    public void setReloadListener(ReloadListener reloadListener) {
        this.reloadListener = reloadListener;
    }

    public interface ReloadListener {
        public void onReloadClicked();
    }
}
