package com.example.app5libbase.listener.chatroom;

public interface DiscussionDeleteImage {

    void deleteImage(int position);
}
