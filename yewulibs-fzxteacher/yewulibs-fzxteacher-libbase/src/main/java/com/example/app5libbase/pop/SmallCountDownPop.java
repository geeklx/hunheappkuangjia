package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.example.app5libpublic.constant.Constant;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmallCountDownPop extends PopupWindow implements View.OnClickListener {

    private Activity activity;
    private RelativeLayout rlTime;
    private ImageView daojishi;
    private TextView tvCountDown;
    private Button btnClose;
    View contentView;
    final ImageView imageView;
    RotateAnimation animation;


    public SmallCountDownPop(final Activity activity) {
        this.activity = activity;
        contentView = LayoutInflater.from(activity).inflate(R.layout.xiao_popup_tools_countdown, null);
        rlTime = (RelativeLayout) contentView.findViewById(R.id.rl_time);
        daojishi = (ImageView) contentView.findViewById(R.id.daojishi);
        tvCountDown = (TextView) contentView.findViewById(R.id.tvCountDown);
        btnClose = contentView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.setPopupWindowTouchModal(this, false);
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        imageView = (ImageView) contentView.findViewById(R.id.daojishi);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //AppLike.showSmallCountDown = false;
                Constant.setShowCountDown(true);
                dismissPop();
               /* countDownPop.setAnimation();
                countDownPop.showPopupWindow(getContentView(activity));*/
            }
        });
    }

    public View getContentView(Activity context) {
        return ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btnClose) {// 关闭倒计时
            dismissPop();
            Constant.setShowCountDown(false);
        }
    }

    public void setAnimation() {
        animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(3000);//设置动画持续时间
        animation.setRepeatCount(Animation.INFINITE);//设置重复次数
        animation.setFillAfter(false);//动画执行完后是否停留在执行完的状态
        animation.setStartOffset(0);//执行前的等待时间
        LinearInterpolator lir = new LinearInterpolator();
        animation.setInterpolator(lir);
        imageView.setAnimation(animation);
        animation.startNow();
    }

    public void setTime(int time) {
        String str = DateUtil.millsecondsToStr(time * 1000);
        richText(tvCountDown, str, ":");

    }

    /**
     * 设置富文本，改变textView部分文字颜色
     *
     * @param tv
     * @param str
     * @param regExp
     */
    public static void richText(TextView tv, String str, String regExp) {
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        Pattern p = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        while (m.find()) {
            int start = m.start(0);
            int end = m.end(0);
            style.setSpan(new ForegroundColorSpan(Color.BLACK), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //指定位置文本的字体颜色
        }
        tv.setText(style);
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(final View parent) {
        this.showAtLocation(parent, Gravity.TOP | Gravity.END, 10, 20);
    }

    public void dismissPop() {
        if (this.isShowing()) {
            animation.cancel();
            this.dismiss();
        }
    }

    private void setPopupWindowTouchModal(PopupWindow popupWindow, boolean touchModal) {
        if (null == popupWindow) {
            return;
        }
        try {
            Method method = PopupWindow.class.getDeclaredMethod("setTouchModal",
                    boolean.class);
            method.setAccessible(true);
            method.invoke(popupWindow, touchModal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
