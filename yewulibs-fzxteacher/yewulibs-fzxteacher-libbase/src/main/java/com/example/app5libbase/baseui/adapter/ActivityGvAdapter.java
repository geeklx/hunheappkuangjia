package com.example.app5libbase.baseui.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.DelActivityDialog;
import com.example.app5libbase.views.RoundProgressBar;
import com.sdzn.fzx.teacher.vo.DelTask;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;
import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * ActivityGvAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ActivityGvAdapter extends BaseAdapter {
    private Context mCxt;

    private List<SyncTaskVo.DataBean> mData;
    private DelActivityDialog delActivityDialog;


    private int delPos = -1;

    public void setData(List<SyncTaskVo.DataBean> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    public void addData(List<SyncTaskVo.DataBean> data) {
        this.mData.addAll(data);
        notifyDataSetChanged();
    }

    public ActivityGvAdapter(Activity cxt) {
        this.mCxt = cxt;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        final SyncTaskVo.DataBean dataBean = mData.get(i);
        if (view == null) {
            view = LayoutInflater.from(mCxt).inflate(R.layout.item_activity_layout, null);
            holder = new ViewHolder();
            holder.activity_name = view.findViewById(R.id.activity_name);
            holder.tvClass = view.findViewById(R.id.tvClass);
            holder.res_count = view.findViewById(R.id.res_count);
            holder.start_time = view.findViewById(R.id.start_time);
            holder.end_time = view.findViewById(R.id.end_time);
            holder.roundProgressBar = view.findViewById(R.id.roundProgressBar);
            holder.llProgress = view.findViewById(R.id.llProgress);
            holder.tvRight = view.findViewById(R.id.tvRight);
            holder.tvCount2 = view.findViewById(R.id.tvCount2);
            holder.tvStatus = view.findViewById(R.id.tvStatus);
            holder.changed_txt = view.findViewById(R.id.changed_txt);
            holder.changed_count_txt = view.findViewById(R.id.changed_count_txt);
            holder.answer_txt = view.findViewById(R.id.answer_txt);
            holder.jiangjie_txt = view.findViewById(R.id.jiangjie_txt);
            holder.preview_txt = view.findViewById(R.id.preview_txt);
            holder.del_txt = view.findViewById(R.id.del_txt);
            holder.changed_ry = view.findViewById(R.id.changed_ry);
            holder.contentView = view.findViewById(R.id.content_view);
            holder.tvClass_scene = view.findViewById(R.id.tvClass_scene);
            holder.hide_txt = view.findViewById(R.id.hide_txt);
            holder.mutual_txt = view.findViewById(R.id.mutual_txt);//互批
            holder.tvCorrection_status = view.findViewById(R.id.tvCorrection_status);//批改状态
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.activity_name.setText(dataBean.getName());
        holder.tvClass.setText(dataBean.getClassName());
        holder.res_count.setText("试题" + dataBean.getCountExam() + "/资源" + dataBean.getCountRescoure());
        holder.start_time.setText(StringUtils.transTime(dataBean.getStartTime(), "yyyy-MM-dd HH:mm") + "-" + StringUtils.transTime(dataBean.getEndTime(), "yyyy-MM-dd HH:mm"));
        holder.end_time.setText(StringUtils.transTime(dataBean.getEndTime(), "yyyy-MM-dd HH:mm"));
        final float value = dataBean.getCountStudentSub() / (float) dataBean.getCountStudentTotal() * 100;
        holder.roundProgressBar.setProgress((int) value);
        holder.tvRight.setText(dataBean.getCountStudentSub() + "");
        holder.tvCount2.setText(dataBean.getCountStudentTotal() + "");
        holder.tvStatus.setText(dataBean.getStatusName() + "");
        int count = dataBean.getCountStudentSub() - dataBean.getCountTeacherCorrect();
        holder.changed_count_txt.setText(count + "");

        holder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toShow(dataBean, "transcript");
            }
        });


        holder.answer_txt.setEnabled(dataBean.getAnswerViewOpen() == 0);
        if (count <= 0) {
            holder.changed_count_txt.setVisibility(View.GONE);
        } else {
            holder.changed_count_txt.setVisibility(View.VISIBLE);
        }

        holder.roundProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toShow(dataBean, "transcript");
            }
        });
        holder.changed_ry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toShow(dataBean, "correct");
            }
        });
        holder.answer_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPublishAnswer(dataBean.getId());
                dataBean.setAnswerViewOpen(1);
                ActivityGvAdapter.this.notifyDataSetChanged();
            }
        });
        holder.jiangjie_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toShow(dataBean, "representative");
            }
        });

        holder.preview_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent answerIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.TaskShowNativeActivity");
                Bundle bundle = new Bundle();
                bundle.putString("name", dataBean.getName());
                bundle.putString("lessonId", String.valueOf(dataBean.getLessonId()));
                bundle.putString("libId", String.valueOf(dataBean.getLessonLibId()));
                answerIntent.putExtras(bundle);
                mCxt.startActivity(answerIntent);
            }
        });
        holder.mutual_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataBean.getCorrectType() != 1 && dataBean.getCorrectType() != 2) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectingWayActivity");
                    intent.putExtra("classId", String.valueOf(dataBean.getClassId()));//当前班级id
                    intent.putExtra("libId", String.valueOf(dataBean.getId()));//当前界面任务id
                    mCxt.startActivity(intent);
                    delPos = i;
                    onRefreshl();
                } else {
                    ToastUtil.showLonglToast("已设置过批改");
                }
            }
        });
        holder.del_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delPos = i;
                delteTask();
            }
        });
        if (dataBean.getCorrectType() == 0) {
            holder.tvCorrection_status.setText("没有人批改过");
            holder.tvCorrection_status.setVisibility(View.GONE);
        } else if (dataBean.getCorrectType() == 1) {
            holder.tvCorrection_status.setText("指定人批改");
            holder.tvCorrection_status.setVisibility(View.VISIBLE);
        } else if (dataBean.getCorrectType() == 2) {
            holder.tvCorrection_status.setText("随机批改");
            holder.tvCorrection_status.setVisibility(View.VISIBLE);
        }

        if (dataBean.getSceneId() != 0) {
            if (dataBean.getSceneId() == 1) {
                holder.tvClass_scene.setText("课前预习");
                holder.tvClass_scene.setTextColor(mCxt.getResources().getColor(R.color.pubbule));
                holder.tvClass_scene.setBackgroundResource(R.drawable.bg_status_rb_checked_in_class);
            }
            if (dataBean.getSceneId() == 2) {
                holder.tvClass_scene.setText("课中讲解");
                holder.tvClass_scene.setTextColor(mCxt.getResources().getColor(R.color.color_FF6D4A));
                holder.tvClass_scene.setBackgroundResource(R.drawable.bg_status_rb_checked_in_consolidate);
            }
            if (dataBean.getSceneId() == 3) {
                holder.tvClass_scene.setTextColor(mCxt.getResources().getColor(R.color.color_FFAC4C));
                holder.tvClass_scene.setBackgroundResource(R.drawable.bg_status_rb_checked_in_explain);
                holder.tvClass_scene.setText("课后巩固");
            }
        }


        holder.hide_txt.setVisibility(View.VISIBLE);
        if (dataBean.getIsHide() == 0) {
            // 任务是非隐藏状态
            holder.hide_txt.setText("可见");
            Drawable top = mCxt.getResources().getDrawable(R.mipmap.task_see_can);
            holder.hide_txt.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
        } else if (dataBean.getIsHide() == 1) {
            // 任务是隐藏状态
            holder.hide_txt.setText("不可见");
            Drawable top = mCxt.getResources().getDrawable(R.mipmap.task_see_not);
            holder.hide_txt.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
        } else {
            // 其他状态
        }
        holder.hide_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                taskToggleHide(dataBean.getId(), (dataBean.getIsHide() == 0 ? 1 : 0), i);
            }
        });

        return view;
    }

    /**
     * 切换任务隐藏状态
     */
    void taskToggleHide(Integer taskId, Integer isHide, final Integer viewIndex) {

        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .toggleHide(taskId, isHide)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {

                        Double isHide = (Double) ((LinkedTreeMap) ((LinkedTreeMap) o).get("data")).get("isHide");
                        final SyncTaskVo.DataBean dataBean = mData.get(viewIndex);
                        dataBean.setIsHide(isHide.intValue());
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, (Activity) mCxt));
    }

    private void toShow(SyncTaskVo.DataBean dataBean, String type) {
        Intent answerIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectNativeActivity");
        Bundle bundle = new Bundle();
        bundle.putString("name", dataBean.getName());
        bundle.putString("classId", String.valueOf(dataBean.getClassId()));
        bundle.putString("id", String.valueOf(dataBean.getId()));
        bundle.putString("type", type);
        bundle.putString("teacherExamId", dataBean.getTeacherExamId() == null ? "" : dataBean.getTeacherExamId());
        answerIntent.putExtras(bundle);
        mCxt.startActivity(answerIntent);
    }

    private static class ViewHolder {
        TextView activity_name;
        TextView tvClass;
        TextView res_count;
        TextView start_time;
        TextView end_time;
        RoundProgressBar roundProgressBar;
        LinearLayout llProgress;
        TextView tvRight;
        TextView tvCount2;
        TextView tvStatus, tvClass_scene;

        TextView changed_txt;
        TextView changed_count_txt;
        TextView answer_txt;
        TextView jiangjie_txt;
        TextView preview_txt;
        TextView del_txt;
        RelativeLayout changed_ry;
        View contentView;
        TextView hide_txt;
        TextView mutual_txt;
        TextView tvCorrection_status;

    }

    private ItemDisposalListener listener;

    private ItemClickRefreshlListener refreshlListener;

    public void setItemDisposalListener(ItemDisposalListener l) {
        this.listener = l;
    }

    public void setItemClickRefreshlListener(ItemClickRefreshlListener l) {
        this.refreshlListener = l;
    }

    public interface ItemClickRefreshlListener {
        void onRefreshl(SyncTaskVo.DataBean dataBean);
    }

    public interface ItemDisposalListener {
        void onDelet(SyncTaskVo.DataBean dataBean);

        void onPublishAnswer(int id);
    }

    void onRefreshl() {
        if (refreshlListener != null) {
            refreshlListener.onRefreshl(mData.get(delPos));
            notifyDataSetChanged();
        }
    }

    /**
     * 删除任务提示
     */
    void delteTask() {
        //SyncTaskVo.DataBean dataBean = mData.get(delPos).getId();
        String id = String.valueOf(mData.get(delPos).getId());


        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .delTask(id)
                .map(new StatusFunc<DelTask>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<DelTask>(new SubscriberListener<DelTask>() {
                    @Override
                    public void onNext(DelTask o) {

                        setSwitch(o.getData());

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, (Activity) mCxt));


    }

    private void setSwitch(int data) {
        switch (data) {
            case 1:
                CreatDelActivityDialog("删除后学生端试题或资源任务将自动撤回，确认删除吗？");
                break;
            case 2:
                CreatDelActivityDialog("已有学生开始作答，删除后学生端试题或资源将自动撤回，确认删除吗？");
                break;
            case 3:
                createTipDialog(mCxt, "已有学生提交任务，不能删除").show();
                break;
        }
    }

    void CreatDelActivityDialog(String s) {
        delActivityDialog = new DelActivityDialog()
                .creatDialog((Activity) mCxt)
                .buildText(s)
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                        if (listener != null) {
                            listener.onDelet(mData.get(delPos));
                            mData.remove(mData.get(delPos));
                            notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancel() {
                    }
                });
        delActivityDialog.show();
    }

    public Dialog createTipDialog(Context context, String contextTxt) {
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_q_know_layout, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(context).setView(view).create();
        alertDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView confirm = (TextView) view.findViewById(R.id.sure_btn);
        final TextView content = (TextView) view.findViewById(R.id.tip_content_txt);
        content.setText(contextTxt);
        alertDialog.setCancelable(false);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        return alertDialog;
    }
}
