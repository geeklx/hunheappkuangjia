package com.example.app5libbase.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;
import com.sdzn.fzx.teacher.vo.NodeBean;

import java.util.ArrayList;
import java.util.List;

/**
 * SpinnerView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SpinnerView extends RelativeLayout {

    /**
     * 1，教材版本
     * 2，选择章节
     * 3，复习
     */
    public static final int[] TAG_ID = {1, 2, 3};

    private Context context;
    private View bgView;
    private LinearLayout containerLy;
    private int curTagId;

    private List<ListView> mViewList;


    public SpinnerView(Context context) {
        this(context, null);
    }

    public SpinnerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpinnerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.context = context;
        final View spinnerView = LayoutInflater.from(context).inflate(R.layout.spinner_view_layout, null);
        bgView = spinnerView.findViewById(R.id.bg_View);
        containerLy = spinnerView.findViewById(R.id.list_container);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(spinnerView, layoutParams);
        bgView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
                SpinnerView.this.setVisibility(View.GONE);
            }
        });

        mViewList = new ArrayList<>();
        reset();
    }

    public void reset() {
        containerLy.removeAllViews();
        mViewList.clear();

    }

    public void setData(List<NodeBean.DataBeanXX> bean) {
        setVisibility(View.VISIBLE);
        reset();
        initBean(bean, 1);
        dealList(bean);
    }

    private void initBean(List<NodeBean.DataBeanXX> list, int pageIndex) {
        if (list!=null){
            for (NodeBean.DataBeanXX beanXX : list) {
                beanXX.setPageIndex(pageIndex);
                if (beanXX.getChildren() != null) {
                    initBean(beanXX.getChildren(), pageIndex + 1);
                }
            }
        }

    }

    private boolean dealList(List<NodeBean.DataBeanXX> list) {
        if (list == null || list.size() <= 0) {
            return false;
        }
        ListView listView = null;
        ItemAdapter itemAdapter = null;
//        final String nodeIdPath = list.get(0).getData().getNodeIdPath();
        int count = list.get(0).getPageIndex();
//        if (!TextUtils.isEmpty(nodeIdPath)) {//这段什么意思?
//            final String replace = nodeIdPath.replace(".", "");
//            count = nodeIdPath.length() - replace.length();
//        }


        if (mViewList.size() >= count) {
            for (int i = count; i < mViewList.size(); i++) {
                final ListView remove = mViewList.remove(i);
                containerLy.removeView(remove);
            }
            listView = mViewList.get(count - 1);
            itemAdapter = (ItemAdapter) listView.getAdapter();
        }
        if (listView == null) {
            listView = new ListView(context);
            listView.setBackground(getResources().getDrawable(R.drawable.bg_list));
            itemAdapter = new ItemAdapter(context);
            listView.setAdapter(itemAdapter);
            mViewList.add(listView);
            final int width = UiUtils.dp2px(280);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT);
            containerLy.addView(listView, layoutParams);
        }

        itemAdapter.setData(list);

        return true;
    }

    public class ItemAdapter extends BaseAdapter {

        private Context context;

        private int curItem;

        private List<NodeBean.DataBeanXX> mData;

        public void setData(List<NodeBean.DataBeanXX> mData) {
            this.mData = mData;
            notifyDataSetChanged();
        }

        public ItemAdapter(Context context) {
            this.context = context;
        }

        public int getCurItem() {
            return curItem;
        }

        public void setCurItem(int curItem) {
            this.curItem = curItem;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData == null ? 0 : mData.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            final NodeBean.DataBeanXX.DataBean data = mData.get(i).getData();
            SpinnerViewHolder holder;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.item_spinner_layout, null);
                holder = new SpinnerViewHolder();
                holder.name = view.findViewById(R.id.item_name);
                view.setTag(holder);
            } else {
                holder = (SpinnerViewHolder) view.getTag();
            }
            holder.name.setEnabled(false);

            holder.name.setText(data.getName());

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    setCurItem(i);
                    onChoosed(data);
                    if (data.isLeaf()) {//最后的节点
                        SpinnerView.this.setVisibility(View.GONE);
                    }
                    if (!dealList(mData.get(i).getChildren())) {
                        SpinnerView.this.setVisibility(View.GONE);
                    }
                    int count = mData.get(i).getPageIndex();
//                    final String nodeIdPath = data.getNodeIdPath();
//                    if (!TextUtils.isEmpty(nodeIdPath)) {
//                        final String replace = nodeIdPath.replace(".", "");
//                        count = nodeIdPath.length() - replace.length();
//                    }
                    if (count > 3) {
                        SpinnerView.this.setVisibility(View.GONE);
                    }
                }
            });
            return view;
        }
    }

    private void onChoosed(NodeBean.DataBeanXX.DataBean data) {
        if (itemChoosedListener != null)
            itemChoosedListener.onItemChoosed(data);
    }


    public static class SpinnerViewHolder {
        public TextView name;
    }

    private ItemChooseListener itemChoosedListener;

    public void setItemChoosedListener(ItemChooseListener itemChoosedListener) {
        this.itemChoosedListener = itemChoosedListener;
    }

    public static interface ItemChooseListener {
        void onItemChoosed(NodeBean.DataBeanXX.DataBean data);
    }
}
