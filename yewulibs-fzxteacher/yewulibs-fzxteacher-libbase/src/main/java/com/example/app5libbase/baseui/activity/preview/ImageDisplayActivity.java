package com.example.app5libbase.baseui.activity.preview;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.util.ImageLoaderOptions;

import uk.co.senab.photoview.PhotoViewOne;
import uk.co.senab.photoview.PhotoViewOneAttacher;


public class ImageDisplayActivity extends BaseActivity {

    PhotoViewOne ivDisplay;
    private String photoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);
        ivDisplay = findViewById(R.id.iv_display);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入ImageDisplayActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        ImageLoaderOptions.displayImage(photoUrl, ivDisplay);
        ivDisplay.setOnViewTapListener(new PhotoViewOneAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float x, float y) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void initData() {
        photoUrl = getIntent().getStringExtra("photoUrl");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }
}
