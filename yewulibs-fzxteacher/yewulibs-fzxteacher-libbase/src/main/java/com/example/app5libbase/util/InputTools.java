package com.example.app5libbase.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.sdzn.fzx.student.libutils.annotations.InputMethod;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 描述：输入法管理类
 *
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class InputTools {
	public static boolean isKeyboard(@InputMethod int inputMethod) {
		return inputMethod == InputMethod.KeyBoard || inputMethod == InputMethod.NONE;
	}

	public static boolean isPenWrite(@InputMethod int inputMethod) {
		return inputMethod == InputMethod.PenWrite;
	}

	public static boolean isPicture(@InputMethod int inputMethod) {
		return inputMethod == InputMethod.Picture;
	}

	public static boolean isHandWrite(@InputMethod int inputMethod) {
		return inputMethod == InputMethod.HandWrite;
	}
	// 隐藏虚拟键盘
	public static void hideKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) v.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm.isActive()) {
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		}
	}

	// 显示虚拟键盘
	public static void showKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) v.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		imm.toggleSoftInput(1000, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/**
	 * 强制显示或者关闭系统键盘
	 * @param txtSearchKey
	 * @param status 0关闭1打开
	 */
	public static void keyBoard(final EditText txtSearchKey, final int status) {

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				InputMethodManager m = (InputMethodManager) txtSearchKey
						.getContext().getSystemService(
								Context.INPUT_METHOD_SERVICE);
				if (status == 1) {
					m.showSoftInput(txtSearchKey,
							InputMethodManager.SHOW_FORCED);
				} else if (status == 0){
					m.hideSoftInputFromInputMethod(txtSearchKey.getWindowToken(), 0);
				}
			}
		}, 100);
	}

	// 通过定时器强制隐藏虚拟键盘
	public static void timerHideKeyboard(final View v) {
		if(v != null){
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					InputMethodManager imm = (InputMethodManager) v.getContext()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					if (imm.isActive()) {
						imm.hideSoftInputFromWindow(v.getWindowToken(),
								0);
					}
				}
			}, 10);
		}
	}

	// 输入法是否显示着
	public static boolean keyBoard(EditText edittext) {
		boolean bool = false;
		InputMethodManager imm = (InputMethodManager) edittext.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm.isActive()) {
			bool = true;
		}
		return bool;
	}
}