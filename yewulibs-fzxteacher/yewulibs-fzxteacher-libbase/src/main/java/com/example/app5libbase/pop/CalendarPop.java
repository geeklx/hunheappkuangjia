package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;
import com.tubb.calendarselector.library.CalendarSelector;
import com.tubb.calendarselector.library.FullDay;
import com.tubb.calendarselector.library.MonthView;
import com.tubb.calendarselector.library.SCDateUtils;
import com.tubb.calendarselector.library.SCMonth;
import com.tubb.calendarselector.library.SegmentSelectListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CalendarPop extends PopupWindow {
    private RecyclerView rvCalendar;
    private Activity activity;
    private LinearLayoutManager linearLayoutManager;
    private List<SCMonth> data;

    private CalendarSelector selector;


    public CalendarPop(Activity activity) {
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_calender, null);
        rvCalendar = contentView.findViewById(R.id.rvCalendar);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
//        int height = AndroidUtil.getWindowHeight(activity)
//                - UiUtils.dp2px(120);
//        this.setHeight(height);
        this.setHeight(UiUtils.dp2px(390));
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        linearLayoutManager = new LinearLayoutManager(activity);
        rvCalendar.setLayoutManager(linearLayoutManager);
        ((SimpleItemAnimator) rvCalendar.getItemAnimator()).setSupportsChangeAnimations(false);
        data = getData();
        segmentMode();
    }

    public List<SCMonth> getData() {
        List<SCMonth> months = SCDateUtils.generateMonths(1970, 2018, SCMonth.SUNDAY_OF_WEEK);
        int currentMonth = Calendar.getInstance(Locale.CHINA).get(Calendar.MONTH) + 1;
        List<SCMonth> removeMonths = new ArrayList<>();
        for (int i = 0; i < 12 - currentMonth; i++) {
            removeMonths.add(months.get(months.size() - 1 - i));
        }
        months.removeAll(removeMonths);
        return months;
    }


    /**
     * segment mode
     */
    private void segmentMode() {
        data = getData();

        selector = new CalendarSelector(data, CalendarSelector.SEGMENT);
        selector.setSegmentSelectListener(new SegmentSelectListener() {
            @Override
            public void onSegmentSelect(FullDay startDay, FullDay endDay) {
            }

            @Override
            public boolean onInterceptSelect(FullDay selectingDay) { // one day intercept
                return super.onInterceptSelect(selectingDay);
            }

            @Override
            public boolean onInterceptSelect(FullDay startDay, FullDay endDay) { // segment days intercept
                return super.onInterceptSelect(startDay, endDay);
            }

            @Override
            public void selectedSameDay(FullDay sameDay) { // selected the same day
                super.selectedSameDay(sameDay);
            }
        });
        rvCalendar.setAdapter(new CalendarAdpater(data));
        moveToPosition(linearLayoutManager, data.size() - 1);
    }

    public void setSegmentSelectListener(SegmentSelectListener listener) {
        selector.setSegmentSelectListener(listener);
    }

    /**
     * RecyclerView 移动到当前位置，
     *
     * @param manager 设置RecyclerView对应的manager
     * @param n       要跳转的位置
     */
    public void moveToPosition(LinearLayoutManager manager, int n) {
        manager.scrollToPositionWithOffset(n, 0);
        manager.setStackFromEnd(true);
    }

    class CalendarAdpater extends RecyclerView.Adapter<CalendarViewHolder> {

        List<SCMonth> months;

        public CalendarAdpater(List<SCMonth> months) {
            this.months = months;
        }

        @Override
        public CalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CalendarViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar, parent, false));
        }

        @Override
        public void onBindViewHolder(CalendarViewHolder holder, int position) {
            SCMonth scMonth = months.get(position);
            holder.tvMonthTitle.setText(String.format("%d年 %d月", scMonth.getYear(), scMonth.getMonth()));
            holder.monthView.setSCMonth(scMonth);
            selector.bind(rvCalendar, holder.monthView, position);
        }

        @Override
        public int getItemCount() {
            return months.size();
        }
    }

    class CalendarViewHolder extends RecyclerView.ViewHolder {

        TextView tvMonthTitle;
        MonthView monthView;

        public CalendarViewHolder(View itemView) {
            super(itemView);
            tvMonthTitle = (TextView) itemView.findViewById(R.id.tvMonthTitle);
            monthView = (MonthView) itemView.findViewById(R.id.ssMv);
        }
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
//            this.showAsDropDown(parent, 0, 10);
            this.showAtLocation(parent, Gravity.CENTER, 0, 0);
        } else {
            this.dismiss();
        }
    }
}
