package com.example.app5libbase.baseui.activity.preview;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseActivity;
import com.sdzn.fzx.teacher.BuildConfig3;

public class PDFActivity extends BaseActivity implements View.OnClickListener {

    TextView tvPdfTitle;
    TextView tvClose;
    WebView mWebView;
    private String path;
    private String name;
    private long size;
    private long startTime;
    private PDFFragment pdfFragment;
    private static final String TAG_PDF = "pdfFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        tvPdfTitle = findViewById(R.id.tv_video_title);
        tvClose = findViewById(R.id.tv_close);
        mWebView = findViewById(R.id.webview);
        tvClose.setOnClickListener(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PDFActivity成功");
                }
            }
        }
    }


    @Override
    protected void initView() {
        tvPdfTitle.setText(name);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        String url = "http://ow365.cn/?i=" + BuildConfig3.OFFICE_ID + "&n=5&furl=" + path;
        mWebView.loadUrl(url);
    }

    @Override
    protected void initData() {
        name = getIntent().getStringExtra("name");
        path = getIntent().getStringExtra("path");
        size = getIntent().getLongExtra("size", 0);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return false;
            }
            back();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void back() {
        Intent intent = new Intent();
        intent.putExtra("time", (System.currentTimeMillis() - startTime) / 1000);
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View v) {
        onBackPressed();
    }
}