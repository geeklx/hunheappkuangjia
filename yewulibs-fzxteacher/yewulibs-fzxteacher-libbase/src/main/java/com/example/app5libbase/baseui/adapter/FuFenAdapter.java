package com.example.app5libbase.baseui.adapter;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVoListVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/19.
 */
public class FuFenAdapter extends BaseRcvAdapter<ClassGroupingVoListVo> {
    private FuFenStudentAdapter.OnStudentClickListener mListener;

    public FuFenAdapter(Context context, List<ClassGroupingVoListVo> mList, FuFenStudentAdapter.OnStudentClickListener listener) {
        super(context, R.layout.item_fufen_group, mList);
        mListener = listener;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, ClassGroupingVoListVo vo) {
        holder.setText(R.id.tv_group_name, vo.getClassGroupName());
        holder.setText(R.id.tv_group_score, vo.getPoints());
        RecyclerView rv = holder.getView(R.id.rv);
        RecyclerView.LayoutManager layoutManager = rv.getLayoutManager();
        if (layoutManager == null) {
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rv.setLayoutManager(layoutManager);
        }

        List<ClassGroupingVo.DataBean> list = vo.getList();
        if (list == null) {
            list = new ArrayList<>();
        }
        FuFenStudentAdapter adapter;
        if (rv.getAdapter() == null || !(rv.getAdapter() instanceof FuFenStudentAdapter)) {
            adapter = new FuFenStudentAdapter(context, list);
            rv.setAdapter(adapter);
        } else {
            adapter = (FuFenStudentAdapter) rv.getAdapter();
        }
        adapter.setData(list);
        if (mListener != null) {
            adapter.setOnItemClickListener(mListener);
        }
    }
}
