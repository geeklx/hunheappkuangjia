package com.example.app5libbase.ai.pop.bean;

public class TextModel1 {
    public String text; //RadioButton的文字
    public String lessonId; //RadioButton的文字
    public String courseName; //RadioButton的文字
    public String classesId; //RadioButton的文字

    public String getClassesId() {
        return classesId;
    }

    public void setClassesId(String classesId) {
        this.classesId = classesId;
    }

    public TextModel1(String text, String lessonId, String courseName, String classesId) {
        this.text = text;
        this.lessonId = lessonId;
        this.classesId = classesId;
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getLessonId() {
        return lessonId;
    }

    public void setLessonId(String lessonId) {
        this.lessonId = lessonId;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
