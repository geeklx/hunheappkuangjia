package com.example.app5libbase.views.graffiti;

/**
 * 涂鸦框架相关的回调
 * Created by huangziwei on 2017/3/17.
 */

public interface IGraffitiListener {

    /**
     * 准备工作已经完成
     */
    void onReady();

    void isUndo(boolean isUndo);

}
