package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.example.app5libpublic.constant.Constant;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountDownPop extends PopupWindow implements View.OnClickListener {

    private Activity activity;
    private RelativeLayout rlCountDownPop;
    private Button btnClose;
    private ImageView daojishi;
    private LinearLayout llTime;
    private TextView tvCountDown;
    private Button close;
    private RelativeLayout rlTime;
    private ImageView SmallDaojishi;
    private TextView SmallTvCountDown;

    View contentView;
    final ImageView imageView;
    ImageView smallImageView;

    RotateAnimation animation;
    boolean flag = true;
    private View view;

    public CountDownPop(final Activity activity) {
        this.activity = activity;
        contentView = LayoutInflater.from(activity).inflate(R.layout.popup_tools_countdown, null);
        rlCountDownPop = (RelativeLayout) contentView.findViewById(R.id.rl_countDownPop);
        btnClose = (Button) contentView.findViewById(R.id.btnClose);
        daojishi = (ImageView) contentView.findViewById(R.id.daojishi);
        llTime = (LinearLayout) contentView.findViewById(R.id.ll_time);
        tvCountDown = (TextView) contentView.findViewById(R.id.tvCountDown);
        close = (Button) contentView.findViewById(R.id.close);
        rlTime = (RelativeLayout) contentView.findViewById(R.id.rl_time);
        SmallDaojishi = (ImageView) contentView.findViewById(R.id.Small_daojishi);
        SmallTvCountDown = (TextView) contentView.findViewById(R.id.Small_tvCountDown);
        btnClose.setOnClickListener(this);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.setPopupWindowTouchModal(this, false);
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        imageView = (ImageView) contentView.findViewById(R.id.daojishi);
        smallImageView = (ImageView) contentView.findViewById(R.id.Small_daojishi);
        Button close = (Button) contentView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 关闭倒计时
                dismissPop();
                Constant.setShowCountDown(false);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.GONE);
                llTime.setVisibility(View.GONE);
                rlTime.setVisibility(View.VISIBLE);
                Constant.setShowSmallCountDown(false);
                dismissPop();
                showPopupWindow();
            }
        });

        rlTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.VISIBLE);
                llTime.setVisibility(View.VISIBLE);
                rlTime.setVisibility(View.GONE);
                dismissPop();
                showCenterPopupWindow();
                Constant.setShowSmallCountDown(true);
            }
        });
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btnClose) {// 关闭倒计时
            dismissPop();
            Constant.setShowCountDown(false);
        }
    }

    public void setAnimation() {
        animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(3000);//设置动画持续时间
        animation.setRepeatCount(Animation.INFINITE);//设置重复次数
        animation.setFillAfter(false);//动画执行完后是否停留在执行完的状态
        animation.setStartOffset(0);//执行前的等待时间
        LinearInterpolator lir = new LinearInterpolator();
        animation.setInterpolator(lir);
        imageView.setAnimation(animation);
        animation.startNow();


    }

    public void setSmallAnimation() {

        animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(3000);//设置动画持续时间
        animation.setRepeatCount(Animation.INFINITE);//设置重复次数
        animation.setFillAfter(false);//动画执行完后是否停留在执行完的状态
        animation.setStartOffset(0);//执行前的等待时间
        LinearInterpolator lir = new LinearInterpolator();
        animation.setInterpolator(lir);
        smallImageView.setAnimation(animation);
        animation.startNow();


    }

    public void setTime(int time) {
        String str = DateUtil.millsecondsToStr(time * 1000);
        richText(tvCountDown, str, ":");
        richText(SmallTvCountDown, str, ":");
    }

    /**
     * 设置富文本，改变textView部分文字颜色
     *
     * @param tv
     * @param str
     * @param regExp
     */
    public static void richText(TextView tv, String str, String regExp) {
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        //  Pattern p = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE);


        String pattern = "-?[0-9]\\d*";
        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);


        //  Matcher m = p.matcher(str);
        while (m.find()) {
            int start = m.start(0);
            int end = m.end(0);
            //文字字体
            TypefaceSpan ab = new TypefaceSpan("DISPLAYFREETFB");
            style.setSpan(ab, 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            style.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //指定位置文本的字体颜色
        }
        tv.setText(style);
    }

    void showPopupWindow() {
        setSmallAnimation();
        this.showAtLocation(view, Gravity.TOP | Gravity.END, 10, 20);
    }

    void showCenterPopupWindow() {
        setAnimation();
        this.showAtLocation(view, Gravity.CENTER, 0, 0);

    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(final View parent) {
        if (parent == null) {
            return;
        }
        view = parent;
        if (!this.isShowing()) {
            if (Constant.getShowSmallCountDown()) {
                setAnimation();
                imageView.setVisibility(View.VISIBLE);
                llTime.setVisibility(View.VISIBLE);
                rlTime.setVisibility(View.GONE);
                this.showAtLocation(parent, Gravity.CENTER, 0, 0);
            } else {
                setSmallAnimation();
                imageView.setVisibility(View.GONE);
                llTime.setVisibility(View.GONE);
                rlTime.setVisibility(View.VISIBLE);
                this.showAtLocation(parent, Gravity.TOP | Gravity.END, 10, 20);
            }
        } else {
            this.dismiss();
        }
    }

    public void dismissPop() {
        if (this.isShowing()) {
            this.dismiss();
        }
    }

    private void setPopupWindowTouchModal(PopupWindow popupWindow, boolean touchModal) {
        if (null == popupWindow) {
            return;
        }
        try {
            Method method = PopupWindow.class.getDeclaredMethod("setTouchModal",
                    boolean.class);
            method.setAccessible(true);
            method.invoke(popupWindow, touchModal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
