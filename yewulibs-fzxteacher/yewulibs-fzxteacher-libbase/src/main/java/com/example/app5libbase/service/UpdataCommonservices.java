package com.example.app5libbase.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libutils.util.MmkvUtils;
import com.sdzn.fzx.teacher.bean.RefreshTokenRecActBean;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.presenter.RefreshTokenPresenter;
import com.sdzn.fzx.teacher.presenter.ShouyePresenter;
import com.sdzn.fzx.teacher.view.RefreshTokenViews;
import com.sdzn.fzx.teacher.view.ShouyeViews;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class UpdataCommonservices extends Service implements ShouyeViews, RefreshTokenViews {

    public static final String HUIBEN_READINGTIME_ACTION = "HUIBEN_READINGTIME_ACTION";
    ShouyePresenter shouyePresenter;
    RefreshTokenPresenter refreshTokenPresenter;
    // 通过静态方法创建ScheduledExecutorService的实例
    private ScheduledExecutorService mScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();//Executors.newScheduledThreadPool(2);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MsgBinder();
    }

    @Override
    public String getIdentifier() {
        return System.currentTimeMillis() + "";
    }


    public class MsgBinder extends Binder {
        public UpdataCommonservices getService() {
            return UpdataCommonservices.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);

        refreshTokenPresenter = new RefreshTokenPresenter();
        refreshTokenPresenter.onCreate(this);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    //启动service
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && !TextUtils.isEmpty(intent.getAction())) {
            String action = intent.getAction();
            if (action.equals(HUIBEN_READINGTIME_ACTION)) {
                if(mScheduledExecutorService!=null){
                    //定时服务
                    mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                        @Override
                        public void run() {
//                        Log.e("onIndexSuccess", "定时刷新接口");
//                        LogUtils.e("onStartCommand--------" + SPUtils.getInstance().getString("shouye_class_id"));
                            shouyePresenter.queryShouye("Bearer " + (String) SPUtils.getInstance().getString("token", ""), SPUtils.getInstance().getString("shouye_class_id"));
//                        refreshTokenPresenter.queryRefreshToken(SPUtils.getInstance().getString("refreshToken", ""));
                        }
                    }, 1, 20, TimeUnit.SECONDS);//SECONDS--秒);//MILLISECONDS--毫秒
                }
            }
        }
        return START_STICKY;
    }

    @Override
    public void onIndexSuccess(ShouyeBean shouyeBean) {
        MmkvUtils.getInstance().set_common_json("ShouyeBean", JSON.toJSONString(shouyeBean), ShouyeBean.class);
    }

    @Override
    public void onIndexNodata(int code, String msg) {
        if (code == 2000) {
            ToastUtils.showShort("用户登录信息已失效，请重新登录");
            isToken();
        } else {
            ToastUtils.showShort(msg);
        }
    }

    public void isToken() {
        if (!TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.example.app5libbase.login.activity.LoginActivity")) {
            ActivityUtils.finishAllActivities();
//            Intent intent3 = new Intent(this, UpdataCommonservices.class);
//            stopService(intent3);// 关闭服务
            mScheduledExecutorService.shutdown();
            SPUtils.getInstance().remove("token");
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onIndexFail(String msg) {
//        ToastUtils.showShort(msg);
    }


    @Override
    public void onRefreshTokenSuccess(RefreshTokenRecActBean refreshTokenRecActBean) {
        SPUtils.getInstance().put("token", refreshTokenRecActBean.getAccess_token());
        SPUtils.getInstance().put("refreshToken", refreshTokenRecActBean.getRefresh_token());
        ToastUtils.showLong(SPUtils.getInstance().getString("token"));
    }

    @Override
    public void onRefreshTokenNodata(String msg) {

    }

    @Override
    public void onRefreshTokenFail(String msg) {

    }

    @Override
    public void onDestroy() {
        if (mScheduledExecutorService!=null){
            mScheduledExecutorService.shutdown();
        }
        shouyePresenter.onDestory();
        super.onDestroy();
    }
}
