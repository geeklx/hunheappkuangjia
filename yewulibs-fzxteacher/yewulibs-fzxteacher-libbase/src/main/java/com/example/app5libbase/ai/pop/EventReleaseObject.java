package com.example.app5libbase.ai.pop;

import com.example.app5libpublic.event.Event;

public class EventReleaseObject extends Event {
    private String json;

    public EventReleaseObject(String json) {
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
