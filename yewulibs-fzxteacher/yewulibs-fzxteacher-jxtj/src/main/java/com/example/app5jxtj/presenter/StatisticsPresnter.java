package com.example.app5jxtj.presenter;

import com.example.app5jxtj.fragment.StatisticsFragment;
import com.example.app5jxtj.view.StatisticsView;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libbase.views.StatisticsGraphView;
import com.google.gson.Gson;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.vo.StringData;
import com.sdzn.fzx.teacher.vo.TeachClassVo;
import com.sdzn.fzx.teacher.vo.TeachStatisticsVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/8
 * 修改单号：
 * 修改内容:
 */
public class StatisticsPresnter extends BasePresenter<StatisticsView, BaseActivity> {
    private String timeId = "0";
    private List<StatisticsGraphView.ItemInfoVo> itemInfoVoList;

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }

    private String json = "{\n" +
            "\t\"wholeLevel\": \"C\",\n" +
            "\t\"lineChart\": [{\n" +
            "\t\t\"date\": \"2019-03-01\",\n" +
            "\t\t\"score\": \"B\"\n" +
            "\t}, {\n" +
            "\t\t\"date\": \"2019-03-02\",\n" +
            "\t\t\"score\": \"C\"\n" +
            "\t}, {\n" +
            "\t\t\"date\": \"2019-03-03\",\n" +
            "\t\t\"score\": \"C\"\n" +
            "\t}, {\n" +
            "\t\t\"date\": \"2019-03-04\",\n" +
            "\t\t\"score\": \"C\"\n" +
            "\t}, {\n" +
            "\t\t\"date\": \"2019-03-05\",\n" +
            "\t\t\"score\": \"D\"\n" +
            "\t}, {\n" +
            "\t\t\"date\": \"2019-03-07\",\n" +
            "\t\t\"score\": \"E\"\n" +
            "\t}],\n" +
            "\t\"pieChart\": {\n" +
            "\t\t\"aplus\": {\n" +
            "\t\t\t\"accuracyForAplus\": \"1.0\",\n" +
            "\t\t\t\"pointNamesForAplus\": [\"有理数\",\"有理数\",\"有理数\"]\n" +
            "\t\t},\n" +
            "\t\t\"a\": {\n" +
            "\t\t\t\"pointNamesForA\": [\"有理数\"],\n" +
            "\t\t\t\"accuracyForA\": \"28.57\"\n" +
            "\t\t},\n" +
            "\t\t\"b\": {\n" +
            "\t\t\t\"pointNamesForB\": [\"数轴\"],\n" +
            "\t\t\t\"accuracyForB\": \"28.57\"\n" +
            "\t\t},\n" +
            "\t\t\"c\": {\n" +
            "\t\t\t\"pointNamesForC\": [],\n" +
            "\t\t\t\"accuracyForC\": \"0.0\"\n" +
            "\t\t},\n" +
            "\t\t\"d\": {\n" +
            "\t\t\t\"pointNamesForD\": [\"正数和负数\"],\n" +
            "\t\t\t\"accuracyForD\": \"42.86\"\n" +
            "\t\t},\n" +
            "\t\t\"e\": {\n" +
            "\t\t\t\"pointNamesForE\": [],\n" +
            "\t\t\t\"accuracyForE\": \"1.0\"\n" +
            "\t\t}\n" +
            "\t}\n" +
            "}";

    public void getStatistics(String classId, String courseId, String type) {
        //查看全部班级
        Network.createTokenService(NetWorkService.GetshowTeacherAssessementHistoryService.class)
                .StudentStatisc(classId, courseId, type)
                .map(new StatusFunc<StringData>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StringData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.setLineViewBean(new ArrayList<StatisticsGraphView.LineViewBean>());
                    }

                    @Override
                    public void onNext(StringData o) {
                        Gson gson = new Gson();
                        TeachStatisticsVo teachStatisticsVo = gson.fromJson(o.getData(), TeachStatisticsVo.class);
                        mView.setTeachStatisticsVo(teachStatisticsVo);
                        List<StatisticsGraphView.LineViewBean> lineViewBeans = dealRightRateList(setRate(teachStatisticsVo));
                        mView.setLineViewBean(lineViewBeans);
                    }
                });
    }


    public void getStudentStatistics(String studentId, String courseId, String type) {
        //查询某一个学生的信息
        Network.createTokenService(NetWorkService.GetshowStudentAssessementHistoryService.class)
                .StudentStatisc(studentId, courseId, type)
                .map(new StatusFunc<StringData>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StringData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.setLineViewBean(new ArrayList<StatisticsGraphView.LineViewBean>());
                    }

                    @Override
                    public void onNext(StringData o) {
                        Gson gson = new Gson();
                        TeachStatisticsVo teachStatisticsVo = gson.fromJson(o.getData(), TeachStatisticsVo.class);
                        mView.setTeachStatisticsVo(teachStatisticsVo);
                        List<StatisticsGraphView.LineViewBean> lineViewBeans = dealRightRateList(setRate(teachStatisticsVo));
                        mView.setLineViewBean(lineViewBeans);
                    }
                });
    }


   /* public void getLineViewBean() {
        Gson gson = new Gson();
        TeachStatisticsVo teachStatisticsVo = gson.fromJson(json, TeachStatisticsVo.class);
        List<StatisticsGraphView.LineViewBean> lineViewBeans = dealRightRateList(setRate(teachStatisticsVo));
        mView.setLineViewBean(lineViewBeans);
    }*/

    private TeachStatisticsVo setRate(TeachStatisticsVo teachStatisticsVo) {
        for (int i = 0; i < teachStatisticsVo.getLineChart().size(); i++) {
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("A")) {
                teachStatisticsVo.getLineChart().get(i).setRate(1);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("B")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.8f);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("C")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.6f);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("D")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.4f);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("E")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.2f);
            }
        }
        return teachStatisticsVo;
    }


    private List<StatisticsGraphView.LineViewBean> dealRightRateList(TeachStatisticsVo bean) {
        if (bean == null)
            return null;

        // final List<RightRateBean.DataBean> rightDataList = bean.getData();
        final List<TeachStatisticsVo.LineChartBean> rightDataList = bean.getLineChart();

        List<StatisticsGraphView.LineViewBean> retList = new ArrayList<>();

        ArrayList<StatisticsGraphView.ItemInfoVo> mylist = new ArrayList<>();
/*        ArrayList<StatisticsGraphView.ItemInfoVo> averageList = new ArrayList<>();
        ArrayList<StatisticsGraphView.ItemInfoVo> toplist = new ArrayList<>();*/


        final StatisticsGraphView.LineViewBean myLineBean = new StatisticsGraphView.LineViewBean();
        final StatisticsGraphView.LineViewBean averageLineBean = new StatisticsGraphView.LineViewBean();
        final StatisticsGraphView.LineViewBean topLinBean = new StatisticsGraphView.LineViewBean();

        for (int j = 0; j < rightDataList.size(); j++) {
            StatisticsGraphView.ItemInfoVo myVo = new StatisticsGraphView.ItemInfoVo();
         /*   StatisticsGraphView.ItemInfoVo averageVo = new StatisticsGraphView.ItemInfoVo();
            StatisticsGraphView.ItemInfoVo topVo = new StatisticsGraphView.ItemInfoVo();*/
            String day = "";
            String score;
            if (timeId.equals(StatisticsFragment.timeIds[0])) {
                day = rightDataList.get(j).getDate();
                score = rightDataList.get(j).getScore();
            } else if (timeId.equals(StatisticsFragment.timeIds[1])) {
                day = rightDataList.get(j).getDate();
                score = rightDataList.get(j).getScore();
            } else {
                day = rightDataList.get(j).getDate();
                score = rightDataList.get(j).getScore();
            }

            myVo.setValue(rightDataList.get(j).getRate());
            myVo.setName(day);
            myVo.setScore(score);
     /*       averageVo.setValue(rightDataList.get(j).getClassRate());
            averageVo.setName(day);
            topVo.setValue(rightDataList.get(j).getHighRate());
            topVo.setName(day)*/
            ;
            mylist.add(myVo);
            /*averageList.add(averageVo);
            toplist.add(topVo);*/
        }
        myLineBean.setItemVo(mylist);
        myLineBean.setColor(StatisticsGraphView.COLOR_COLUM_COLORS[0]);
        myLineBean.setName("我的");
   /*     averageLineBean.setItemVo(averageList);
        averageLineBean.setColor(StatisticsGraphView.COLOR_COLUM_COLORS[1]);
        averageLineBean.setName("平均");
        topLinBean.setItemVo(toplist);
        topLinBean.setColor(StatisticsGraphView.COLOR_COLUM_COLORS[2]);
        topLinBean.setName("最高");*/

        retList.add(myLineBean);
  /*      retList.add(averageLineBean);
        retList.add(topLinBean);*/

        return retList;
    }

    public void getStatisticsClass() {
        Network.createTokenService(NetWorkService.GetTeacherStatisticsService.class)
                .StudentStatisc(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<TeachClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TeachClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(TeachClassVo o) {
                        mView.setTeachClassVo(o);

                    }
                });
    }


    public void getGroupStudentClass(String classId) {
        Network.createTokenService(NetWorkService.GetClassGroupingService.class)
                .GetClassGrouping(classId)
                .map(new StatusFunc<ClassGroupingVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ClassGroupingVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ClassGroupingVo o) {
                        mView.setClassGroupingVo(o);

                    }
                });
    }
}
