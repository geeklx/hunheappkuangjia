package com.example.app5jxtj.fragment;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.util.SubjectSPUtils;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.TeachStatisticsAdapter;
import com.example.app5libbase.baseui.adapter.TeachStatisticsTopAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5jxtj.presenter.StatisticsPresnter;
import com.example.app5jxtj.view.StatisticsView;
import com.example.app5libbase.views.NoScrollGridView;
import com.example.app5libbase.views.SpinerPopWindow;
import com.example.app5libbase.views.StatisticsGraphView;
import com.sdzn.fzx.teacher.vo.TeachClassVo;
import com.sdzn.fzx.teacher.vo.TeachStatisticsVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.List;

//import com.github.mikephil.charting.formatter.PercentFormatter;

/**
 * 教学统计
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class StatisticsFragment extends MBaseFragment<StatisticsPresnter> implements StatisticsView, OnChartValueSelectedListener, View.OnClickListener {
    protected Typeface tfRegular;
    protected Typeface tfLight;
    private static final int PERMISSION_STORAGE = 0;
    protected final String[] partiess = new String[]{
            "A", "B", "C", "D", "E", "F", "G", "H",
            "I", "J", "K", "L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
    };
    public static final String[] timeIds = {"3", "1", "2"};
    private RecyclerView recyclerView;
    private TextView tv;
    private TextView tvNumber;
    private TextView classSpinnerTxt;
    private TextView classSpinnerTxtTwo;
    private RelativeLayout rl;
    private StatisticsGraphView timeAnalyzeView;
    private View line;
    private TextView tv3;
    //暂无数据外层
    private LinearLayout llLeftPir;
    //暂无数据
    private ImageView ivNodata;
    private PieChart chart;
    //本界面GridVIew
    private LinearLayout llGridView;
    private TextView selectTvTwo;
    private NoScrollGridView gridView;

    private List<String> parties = new ArrayList<>();
    private List<Float> percent = new ArrayList<>();
    private List<TeachClassVo.DataBean> mList = new ArrayList<>();
    private SpinerPopWindow classChoosePop, timeChoosePop;
    private TeachStatisticsTopAdapter teachStatisticsTopAdapter;
    private TeachStatisticsAdapter teachStatisticsAdapter;
    private ClassGroupingVo classGrouping;
    private TeachStatisticsVo teachStatistics;
    private TeachClassVo classVo;
    private int mSelectedPos = -1;//选择班级

    public static StatisticsFragment newInstance(Bundle bundle) {
        StatisticsFragment fragment = new StatisticsFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new StatisticsPresnter();
        mPresenter.attachView(this, activity);
        //parties.put
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View rootView = inflater.inflate(R.layout.fragment_statistics, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        tv = (TextView) rootView.findViewById(R.id.tv);
        tvNumber = (TextView) rootView.findViewById(R.id.tv_number);
        classSpinnerTxt = (TextView) rootView.findViewById(R.id.class_spinner_txt);
        classSpinnerTxtTwo = (TextView) rootView.findViewById(R.id.class_spinner_txt_two);
        rl = (RelativeLayout) rootView.findViewById(R.id.rl);
        timeAnalyzeView = (StatisticsGraphView) rootView.findViewById(R.id.time_analyze_view);
        line = (View) rootView.findViewById(R.id.line);
        tv3 = (TextView) rootView.findViewById(R.id.tv3);
        llLeftPir = (LinearLayout) rootView.findViewById(R.id.ll_left_pir);
        ivNodata = (ImageView) rootView.findViewById(R.id.iv_nodata);
        chart = (PieChart) rootView.findViewById(R.id.chart1);
        llGridView = (LinearLayout) rootView.findViewById(R.id.ll_gridView);
        selectTvTwo = (TextView) rootView.findViewById(R.id.select_tv_two);
        gridView = (NoScrollGridView) rootView.findViewById(R.id.gridView);

        classSpinnerTxt.setOnClickListener(this);
        classSpinnerTxtTwo.setOnClickListener(this);

        teachStatisticsTopAdapter = new TeachStatisticsTopAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(teachStatisticsTopAdapter);
        teachStatisticsTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (mSelectedPos != position) {
                    //先取消上个item的勾选状态
                    mList.get(mSelectedPos).setSelected(false);
                    teachStatisticsTopAdapter.notifyItemChanged(mSelectedPos);
                    //设置新Item的勾选状态
                    mSelectedPos = position;
                    mList.get(mSelectedPos).setSelected(true);
                    teachStatisticsTopAdapter.notifyItemChanged(mSelectedPos);

                    mPresenter.getGroupStudentClass(mList.get(mSelectedPos).getClassId() + "");

                    mPresenter.getStatistics(mList.get(mSelectedPos).getClassId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", "");

                }
            }
        });


//        mPresenter.getStatisticsClass();

        tfRegular = Typeface.createFromAsset(activity.getAssets(), "OpenSans-Regular.ttf");
        tfLight = Typeface.createFromAsset(activity.getAssets(), "OpenSans-Light.ttf");
        initView();

        initChatView();

        //  setData(4,10);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getStatisticsClass();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // saveToGallery();
            } else {
                Toast.makeText(getContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void initView() {
        tvNumber.setText("A");
        tvNumber.setSelected(true);
        tvNumber.setBackground(getResources().getDrawable(R.drawable.bg_select_selector_2));
        selectTvTwo.setSelected(true);
        selectTvTwo.setText("");

        classChoosePop = new SpinerPopWindow(getActivity());
        classChoosePop.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                if (classGrouping == null) return;
                classSpinnerTxt.setText(pos == 0 ? "全班水平" : classGrouping.getData().get(pos - 1).getStudentName());
                onChooseClass(pos);
            }
        });

        timeChoosePop = new SpinerPopWindow(getActivity());
        timeChoosePop.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                classSpinnerTxtTwo.setText(pos == 0 ? "近一周" : "近一月");
                onTime(pos);
            }


        });
    }

    private void initChatView() {
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setCenterTextTypeface(tfLight);
        chart.setCenterText("");


        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(0);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        chart.animateY(1400, Easing.EaseInOutQuad);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(7f);
        l.setYOffset(0f);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);

        //new 1
        chart.setDrawEntryLabels(false);
    }

    private void setData(int count, float range) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry(percent.get(i),
                    parties.get(i),
                    getResources().getDrawable(R.drawable.star)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");//未做知识点

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(tfLight);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
        chart.setCenterTextSize(12f);
        chart.setCenterTextColor(0xff323C47);
        chart.setCenterTextTypeface(Typeface.DEFAULT_BOLD);
    }

    private SpannableString generateCenterSpannableText(String content) {

        SpannableString s = new SpannableString(content);
 /*       s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);*/
        return s;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onValueSelected(Entry entry, Highlight highlight) {
        PieEntry pieEntry = (PieEntry) entry;

        if (pieEntry.getLabel().length() > 3) {
            StringBuilder sb = new StringBuilder(pieEntry.getLabel());
            sb.insert(3, "\n");
            chart.setCenterText(sb.toString());
        } else {
            return;
        }


        if (pieEntry.getLabel().substring(0, 2).contains("A+")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getAplus().getPointNamesForAplus(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("A")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getA().getPointNamesForA(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("B")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getB().getPointNamesForB(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("C")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getC().getPointNamesForC(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("D")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getD().getPointNamesForD(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("E")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getE().getPointNamesForE(), getActivity());
        }

        selectTvTwo.setText(pieEntry.getLabel().substring(0, 2));
        gridView.setAdapter(teachStatisticsAdapter);
    }

    @Override
    public void onNothingSelected() {

    }

    protected void saveToGallery(Chart chart, String name) {
        if (chart.saveToGallery(name + "_" + System.currentTimeMillis(), 70))
            Toast.makeText(getActivity(), "Saving SUCCESSFUL!",
                    Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getActivity(), "Saving FAILED!", Toast.LENGTH_SHORT)
                    .show();
    }

    @Override
    public void setTeachStatisticsVo(TeachStatisticsVo teachStatisticsVo) {
        teachStatistics = teachStatisticsVo;
        chart.clear();

        tvNumber.setText(teachStatisticsVo.getWholeLevel());
        parties.clear();
        percent.clear();
        if (teachStatisticsVo.getPieChart().getAplus() != null && teachStatisticsVo.getPieChart().getAplus().getAccuracyForAplus() != null) {
            parties.add("A+  " + teachStatisticsVo.getPieChart().getAplus().getAccuracyForAplus());
            parties.add("A  " + teachStatisticsVo.getPieChart().getA().getAccuracyForA());
            parties.add("B  " + teachStatisticsVo.getPieChart().getB().getAccuracyForB());
            parties.add("C  " + teachStatisticsVo.getPieChart().getC().getAccuracyForC());
            parties.add("D  " + teachStatisticsVo.getPieChart().getD().getAccuracyForD());
            parties.add("E  " + teachStatisticsVo.getPieChart().getE().getAccuracyForE());

            percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getAplus().getAccuracyForAplus().split("%")[0]));
            percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getA().getAccuracyForA().split("%")[0]));
            percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getB().getAccuracyForB().split("%")[0]));
            percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getC().getAccuracyForC().split("%")[0]));
            percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getD().getAccuracyForD().split("%")[0]));
            percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getE().getAccuracyForE().split("%")[0]));
            setData(6, 10);
            for (IDataSet<?> set : chart.getData().getDataSets())
                set.setDrawValues(!set.isDrawValuesEnabled());
            chart.invalidate();
            //new
            if (percent.size() > 0) {
                if (percent.get(0) > 0) {
                    setCenterStr(parties.get(0));
                } else if (percent.get(1) > 0) {
                    setCenterStr(parties.get(1));
                } else if (percent.get(2) > 0) {
                    setCenterStr(parties.get(2));
                }
            }
            selectTvTwo.setText("A+ ");
        }
        if (teachStatisticsVo.getPieChart().getAplus() != null && teachStatisticsVo.getPieChart().getAplus().getPointNamesForAplus() != null) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(teachStatisticsVo.getPieChart().getAplus().getPointNamesForAplus(), getActivity());
            gridView.setAdapter(teachStatisticsAdapter);
        }
    }

    private void setCenterStr(String str) {
        if (str.length() > 3) {
            StringBuilder sb = new StringBuilder(parties.get(0));
            sb.insert(3, "\n");
            chart.setCenterText(sb.toString());
        }
    }

    private List<StatisticsGraphView.ItemInfoVo> showitemVo = new ArrayList<>();

    @Override
    public void setLineViewBean(List<StatisticsGraphView.LineViewBean> lineViewBean) {
        for (int i = 0; i < lineViewBean.size(); i++) {
            showitemVo = lineViewBean.get(i).getItemVo();
        }

        if (lineViewBean != null) {
            timeAnalyzeView.setData(lineViewBean);
        }
        if (showitemVo.size() != 0) {
            chart.setVisibility(View.VISIBLE);
            ivNodata.setVisibility(View.GONE);
            llLeftPir.setVisibility(View.GONE);
            llGridView.setVisibility(View.VISIBLE);
        } else {
            llGridView.setVisibility(View.GONE);
            ivNodata.setVisibility(View.VISIBLE);
            llLeftPir.setVisibility(View.VISIBLE);
            chart.setVisibility(View.GONE);
        }
    }

    /*默认第一次进入*/
    @Override
    public void setTeachClassVo(TeachClassVo teachClassVo) {
        classVo = teachClassVo;
        teachStatisticsTopAdapter.clear();
        mList.clear();
        if (teachClassVo.getData().size() > 0) {
            for (int i = 0; i < teachClassVo.getData().size(); i++) {
                if (i == 0) {
                    teachClassVo.getData().get(0).setSelected(true);
                    mSelectedPos = 0;
                }
                mList.add(teachClassVo.getData().get(i));
            }
        }
        teachStatisticsTopAdapter.add(mList);
        teachStatisticsTopAdapter.notifyDataSetChanged();

        mPresenter.getGroupStudentClass(teachClassVo.getData().get(0).getClassId() + "");

        mPresenter.getStatistics(teachClassVo.getData().get(0).getClassId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", "1");
    }


    @Override
    public void setClassGroupingVo(ClassGroupingVo classGroupingVo) {

        classGrouping = classGroupingVo;
        List<String> list = new ArrayList<>();
        list.add("全班水平");
        for (int i = 0; i < classGroupingVo.getData().size(); i++) {
            list.add(classGroupingVo.getData().get(i).getStudentName());
        }
        classChoosePop.setPopDatas(list);

        List<String> timeList = new ArrayList<>();
        timeList.add("近一周");
        timeList.add("近一月");


        timeChoosePop.setPopDatas(timeList);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.class_spinner_txt) {
            classChoosePop.showAsDropDown(classSpinnerTxt);
        } else if (id == R.id.class_spinner_txt_two) {
            timeChoosePop.showAsDropDown(classSpinnerTxtTwo);
        }
    }

    private int myId = 0;
    private int type = 1;//1 一周  2月

    //根据参数查询班级整体水平
    //ckassid
    //courseId学科
    //type当前是
    void onChooseClass(int id) {
        myId = id;
        if (id == 0) {
            //全部
            mPresenter.getStatistics(classVo.getData().get(0).getClassId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", String.valueOf(type));
        } else {
            //某一个学生
            mPresenter.getStudentStatistics(classGrouping.getData().get((id - 1)).getStudentId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", String.valueOf(type));
        }
    }

    //pos代表的是当前选中的一周还是一个月
    private void onTime(int pos) {
        if (myId == 0) {//全班
            if (pos == 0) {
                type = 1;
                mPresenter.getStatistics(classVo.getData().get(mSelectedPos).getClassId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", String.valueOf(type));
            } else {
                type = 2;
                mPresenter.getStatistics(classVo.getData().get(mSelectedPos).getClassId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", String.valueOf(type));
            }
        } else {
            if (pos == 0) {
                type = 1;
                mPresenter.getStudentStatistics(classGrouping.getData().get((myId - 1)).getStudentId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", String.valueOf(type));
            } else {
                type = 2;
                mPresenter.getStudentStatistics(classGrouping.getData().get((myId - 1)).getStudentId() + "", SubjectSPUtils.getCurrentSubject().getSubjectId() + "", String.valueOf(type));
            }
        }
    }

}
