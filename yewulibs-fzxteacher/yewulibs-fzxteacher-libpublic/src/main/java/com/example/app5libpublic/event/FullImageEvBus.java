package com.example.app5libpublic.event;

public class FullImageEvBus {
    boolean isDel;

    public FullImageEvBus(boolean isDel) {
        this.isDel = isDel;
    }

    public boolean isDel() {
        return isDel;
    }

    public void setDel(boolean del) {
        isDel = del;
    }
}
