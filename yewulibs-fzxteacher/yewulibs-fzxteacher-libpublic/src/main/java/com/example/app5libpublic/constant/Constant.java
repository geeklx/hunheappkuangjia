package com.example.app5libpublic.constant;

public class Constant {

    public static String BOX_URL = null;
    public static String QINIU_CONFIG = "http://file.fuzhuxian.com";



    public static boolean showCountDown = false;
    public static boolean showHeadCountDown = false;//头部
    public static boolean showSmallCountDown = true;

    public static boolean getShowCountDown() {
        return showCountDown;
    }

    public static void setShowCountDown(boolean isBoolean) {
        Constant.showCountDown = isBoolean;
    }

    public static boolean getShowHeadCountDown() {
        return showHeadCountDown;
    }

    public static void setShowHeadCountDown(boolean isBoolean) {
        Constant.showHeadCountDown = isBoolean;
    }


    public static boolean getShowSmallCountDown() {
        return showSmallCountDown;
    }

    public static void setShowSmallCountDown(boolean isBoolean) {
        Constant.showSmallCountDown = isBoolean;
    }


}
