package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.app5grzx.AndroidMeInterface;
import com.example.app5grzx.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.teacher.BuildConfig3;

/**
 * A simple {@link Fragment} subclass.教材信息
 */
public class TextbookFragment extends BaseActFragment1 {

    public static TextbookFragment newInstance(Bundle bundle) {
        TextbookFragment fragment = new TextbookFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_bkjl;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidMeInterface(mAgentWeb, getActivity()));
            loadWebSite(BuildConfig3.SERVER_ISERVICE_NEW1 + "/userCenter?step=0"); // 刷新
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(com.example.app5libbase.R.id.ll_base_container_bkjl);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }
}
