package com.example.app5grzx.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.app5grzx.R;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5grzx.presenter.UploadFragmentPresenter;
import com.example.app5grzx.view.UploadFragmentView;
import com.example.app5libbase.util.DialogUtils;
import com.example.app5libbase.util.LoadDialogUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.CharactersListVo;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.File;
import java.net.URI;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadFragment extends MBaseFragment<UploadFragmentPresenter> implements UploadFragmentView {

    RelativeLayout uploadData;
    public static final int REQ_CODE_ALBUM = 1002;
    private String path = "";//文件路径
    private PopupWindow popupWindow;
    private View view;
    private ProgressBar progressBar;
    private TextView tvCount;
    private RelativeLayout relativeLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload, container, false);
        uploadData = view.findViewById(R.id.upload_data);
        initData();
        return view;
    }

    public static UploadFragment newInstance(Bundle bundle) {
        UploadFragment fragment = new UploadFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new UploadFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    void initData() {
        uploadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCharactersList();
                view = v;
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQ_CODE_ALBUM);
            }
        });
    }

    private void getCharactersList() {
        Network.createTokenService(NetWorkService.MainService.class)
                .getCharactersList()
                .map(new StatusFunc<CharactersListVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<CharactersListVo>(new SubscriberListener<CharactersListVo>() {
                    @Override
                    public void onNext(CharactersListVo bean) {
                        if (bean != null && bean.getData() != null && !bean.getData().isEmpty()) {
                            SubjectSPUtils.saveFileRuleSet(bean.getData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, activity, false, true, false, ""));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            try {
                Uri uri;
                uri = geturi(getContext(), data);
                File file;
                if (uri.toString().indexOf("file") == 0) {
                    file = new File(new URI(uri.toString()));
                    path = file.getPath();
                } else {
                    path = getPath(uri);
                    file = new File(path);
                }
                if (!file.exists()) {
                    ToastUtil.showShortlToast("上传失败");
                    CrashReport.postCatchedException(new RuntimeException("FileNotFound: " + file.getAbsolutePath()));
                    return;
                }
                List<String> fileRuleSet = SubjectSPUtils.getFileRuleSet();
                String fileName = file.getName();
                if (fileName.startsWith(".")) {
                    ToastUtil.showShortlToast("文件名不能以.开头");
                    return;
                }
                for (String s : fileRuleSet) {
                    if (fileName.contains(s)) {
                        StringBuilder builder = new StringBuilder("文件名不可包含以下字符: ");
                        for (String s1 : fileRuleSet) {
                            builder.append("  ".equals(s1) ? "  (空格)" : s1).append(" ,");
                        }
                        ToastUtil.showShortlToast(builder.toString());
                        return;
                    }
                }
                if (file.length() > 380 * 1024 * 1024) {
//                            "文件大于100M";
                    new DialogUtils().createTipDialog(getActivity()).buildText("提示\n\n微课视频的大小不能超过350M哦～").show();
                } else {
                    popupWindow = showProgressBar(getActivity(), getActivity());
                    popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

                    // mPresenter.uploadFile(file);
                    mPresenter.getQiNiuToken(file);
                }

            } catch (Exception e) {
                String a = e + "";
            } catch (OutOfMemoryError e) {
                String a = e + "";
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public static Uri geturi(Context context, android.content.Intent intent) {
        Uri uri = intent.getData();
        String type = intent.getType();
        if (uri.getScheme().equals("file") && (type.contains("image/"))) {
            String path = uri.getEncodedPath();
            if (path != null) {
                path = Uri.decode(path);
                ContentResolver cr = context.getContentResolver();
                StringBuffer buff = new StringBuffer();
                buff.append("(").append(MediaStore.Images.ImageColumns.DATA).append("=")
                        .append("'" + path + "'").append(")");
                Cursor cur = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Images.ImageColumns._ID},
                        buff.toString(), null, null);
                int index = 0;
                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                    index = cur.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    // set _id value
                    index = cur.getInt(index);
                }
                if (index == 0) {
                    // do nothing
                } else {
                    Uri uri_temp = Uri
                            .parse("content://media/external/images/media/"
                                    + index);
                    if (uri_temp != null) {
                        uri = uri_temp;
                        Log.i("urishi", uri.toString());
                    }
                }
            }
        }
        return uri;
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    @Override
    public void setProgressBar(int progress) {
        progressBar.setProgress(progress);
        tvCount.setText(progress + "%");
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                mPresenter.concalUpload();
            }
        });
    }

    @Override
    public void uploadSuccess() {
        popupWindow.dismiss();
        new LoadDialogUtils().createTipDialog(getActivity()).buildText("上传成功！\n\n" +
                "请至网页端个人空间查看～").show();
    }


    public PopupWindow showProgressBar(final Context mContext, final Activity activity) {

        // 一个自定义的布局，作为显示的内容
        final View contentView = LayoutInflater.from(mContext).inflate(
                R.layout.show_progressbar, null);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setOutsideTouchable(true);
        // popupWindow.setFocusable(true); // 获取焦点

        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        progressBar = contentView.findViewById(R.id.progress_horizontal);
        tvCount = contentView.findViewById(R.id.count);
        relativeLayout = contentView.findViewById(R.id.cancel);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });

        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);

        return popupWindow;
    }

    @Override
    public void uploadFailure() {
        popupWindow.dismiss();
        new LoadDialogUtils().createTipDialog(getActivity()).buildText("上传失败！\n\n" + "请重新操作～").show();
    }
}
