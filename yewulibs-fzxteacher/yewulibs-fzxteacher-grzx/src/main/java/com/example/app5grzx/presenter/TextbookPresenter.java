package com.example.app5grzx.presenter;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5grzx.view.TextbookView;
import com.sdzn.fzx.teacher.vo.me.ReviewInfo;
import com.sdzn.fzx.teacher.vo.me.TellSetVo;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextbookPresenter extends BasePresenter<TextbookView, BaseActivity> {

    public void getReviewInfoList() {
        Network.createTokenService(NetWorkService.GetReviewInfoService.class)
                .GetReviewInfo()
                .map(new StatusFunc<ReviewInfo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ReviewInfo>(new SubscriberListener<ReviewInfo>() {
                    @Override
                    public void onNext(ReviewInfo reviewInfo) {
                        mView.setReviewInfo(reviewInfo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void getTextbookList() {
        Network.createTokenService(NetWorkService.GetTextbookInfoService.class)
                .GetTextbookInfo()
                .map(new StatusFunc<TextbookInfoVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<TextbookInfoVo>(new SubscriberListener<TextbookInfoVo>() {
                    @Override
                    public void onNext(TextbookInfoVo textbookInfoVo) {
                        mView.setTextbookInfoVo(textbookInfoVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }


    public void updateInfo(String id, String value) {
        Network.createTokenService(NetWorkService.UpdateInfoService.class)
                .updateInfo(id, value)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object object) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }


    public void TellSetList() {
        Network.createTokenService(NetWorkService.GetTellSetListService.class)
                .TellSetList()
                .map(new StatusFunc<TellSetVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<TellSetVo>(new SubscriberListener<TellSetVo>() {
                    @Override
                    public void onNext(TellSetVo tellSetVo) {
                        mView.setTellSetVo(tellSetVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }
}
