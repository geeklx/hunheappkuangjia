package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5grzx.R;
import com.example.app5grzx.adapter.TakeOfficeAdapter;
import com.sdzn.fzx.teacher.presenter.TakeOfficePresenter1;
import com.sdzn.fzx.teacher.view.TakeOfficeViews;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.controller.UserController;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;

/**
 * A simple {@link Fragment} subclass.任教信息
 */
public class TakeOfficeFragment extends BaseFragment implements TakeOfficeViews {
    TakeOfficePresenter1 mPresenter;
    ListView listview;
    private View footview;
    private TakeOfficeAdapter takeOfficeAdapter;

    public static TakeOfficeFragment newInstance(Bundle bundle) {
        TakeOfficeFragment fragment = new TakeOfficeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public TakeOfficeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_take_office, container, false);
        listview = view.findViewById(R.id.listview);
        initPresenter();
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {
        mPresenter.getTakeOfficeInfo("Bearer "+(String) SPUtils.getInstance().getString("token", "")
                , UserController.getLoginBean().getData().getUser().getId(), UserController.getLoginBean().getData().getUser().getCustomerSchoolId());

    }

    private void initView() {
        footview = LayoutInflater.from(getActivity()).inflate(R.layout.textbook_gridview_itme, null);
        TextView name = footview.findViewById(R.id.name);
//        name.setText("长按头像也可切换学科哦，快去试试吧～");
        listview.addFooterView(footview);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public void initPresenter() {
        mPresenter = new TakeOfficePresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    public void setTakeOfficeData(TakeOfficeVo takeOfficeData) {
        takeOfficeAdapter = new TakeOfficeAdapter(takeOfficeData.getData(), getActivity());
        listview.setAdapter(takeOfficeAdapter);
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
