package com.example.app5grzx;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.google.gson.Gson;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.LocalBroadcastManagers;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.student.libutils.util.UrlEncodeUtils;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.JumpNewActBean;

import java.io.UnsupportedEncodingException;

/**
 * Created by cenxiaozhong on 2017/5/14.
 * source code  https://github.com/Justson/AgentWeb
 */

public class AndroidMeInterface {
    private Handler deliver = new Handler(Looper.getMainLooper());
    private AgentWeb agent;
    private Activity context;

    public AndroidMeInterface(AgentWeb agent, Activity context) {
        this.agent = agent;
        this.context = context;
    }
    /**
     * token失效跳转登录页
     * str取到的参数无用
     */
    @JavascriptInterface
    public void JumpLogin(String str) {
        if (!TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.example.app5libbase.login.activity.LoginActivity")) {
            ActivityUtils.finishAllActivities();
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
