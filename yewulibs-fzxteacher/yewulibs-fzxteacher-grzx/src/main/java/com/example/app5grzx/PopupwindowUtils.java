package com.example.app5grzx;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.example.app5grzx.adapter.VersionAdapter;
import com.sdzn.fzx.teacher.vo.me.TextBookClassVo;

import java.util.List;


/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2017/6/22
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class PopupwindowUtils {

    private static onClick onClick = null;
    private static onDialogClick onDialogClick = null;
    private static onBottomDialogClick onBottomDialogClick = null;

    public static void setonClick(onClick iBack) {
        onClick = iBack;
    }

    public static void onDialogClick(onDialogClick iBack) {
        onDialogClick = iBack;
    }

    public static void SetOnBottomDialogClick(onBottomDialogClick iBack) {
        onBottomDialogClick = iBack;
    }

    public static PopupWindow showHeadPopupWindow(final Context mContext, final Activity activity) {

        // 一个自定义的布局，作为显示的内容
        final View contentView = LayoutInflater.from(mContext).inflate(
                R.layout.headpopupwindow_item, null);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));

        contentView.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onClickCamera();
            }
        });
        contentView.findViewById(R.id.photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onClickPhoto();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);

        return popupWindow;
    }

    public static PopupWindow dialogPopupWindow(final Context mContext, final Activity activity) {

        // 一个自定义的布局，作为显示的内容
        final View contentView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_itme, null);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));

        contentView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDialogClick.onClickCancel();
            }
        });
        contentView.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDialogClick.onClickConfirm();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.8f;
        activity.getWindow().setAttributes(lp);

        return popupWindow;
    }


    public static PopupWindow bottomPopupWindow(final Context mContext, final Activity activity, final List<TextBookClassVo> list, int hight) {

        // 一个自定义的布局，作为显示的内容
        final View contentView = LayoutInflater.from(mContext).inflate(
                R.layout.bottom_popuwindow, null);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, hight, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));

        contentView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        ListView listView = contentView.findViewById(R.id.listview);
        listView.setAdapter(new VersionAdapter(list, activity));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String s = list.get(i).getName();
                int id = list.get(i).getId();
                onBottomDialogClick.onClickListViwe(s, id);
                popupWindow.dismiss();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.8f;
        activity.getWindow().setAttributes(lp);

        return popupWindow;
    }

    public interface onClick {
        void onClickPhoto();

        void onClickCamera();
    }

    public interface onDialogClick {
        void onClickCancel();

        void onClickConfirm();
    }

    public interface onBottomDialogClick {
        void onClickCancel();

        void onClickListViwe(String listItem, int id);
    }
}
