package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextbookAdapter extends BaseAdapter {
    private List<TextbookInfoVo.DataBean> list;
    private Context context;
    private addOnClick addOnClick;
    private TextbootGridViewAdapter textbootGridViewAdapter;

    public TextbookAdapter(List<TextbookInfoVo.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setAddOnClick(TextbookAdapter.addOnClick addOnClick) {
        this.addOnClick = addOnClick;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.textbook_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.subjectAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOnClick.onClick(list.get(i).getBaseSubjectName(), list.get(i).getBaseSubjectId());
            }
        });
        if (!TextUtils.isEmpty(list.get(i).getBaseSubjectName())) {
            holder.subjectName.setText(list.get(i).getBaseSubjectName());
        }

        if (list.get(i).getVolumeList() != null) {
            textbootGridViewAdapter = new TextbootGridViewAdapter(list.get(i).getVolumeList(), context);
            holder.gridView.setAdapter(textbootGridViewAdapter);
        } else {
            holder.gridView.setAdapter(null);
            holder.gridView.setVisibility(View.GONE);
        }


        holder.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int j, long l) {
                addOnClick.onDetils(list.get(i).getBaseSubjectName(),
                        list.get(i).getBaseSubjectId(),
                        list.get(i).getVolumeList().get(j).getId(),
                        list.get(i).getVolumeList().get(j).getBaseVersionId(),
                        list.get(i).getVolumeList().get(j).getBaseVersionName()
                );
            }
        });
        return view;
    }

    static class ViewHolder {
        TextView subjectName;
        RelativeLayout subjectAdd;
        GridView gridView;

        ViewHolder(View view) {
            subjectName = view.findViewById(R.id.subject_name);
            subjectAdd = view.findViewById(R.id.subject_add);
            gridView = view.findViewById(R.id.gridView);
        }
    }

    public interface addOnClick {
        void onClick(String subjectName, Integer subjectId);

        void onDetils(String subjectName, Integer subjectId, int id, Integer versionId, String versionName);
    }
}
