package com.example.app5grzx.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.example.app5grzx.R;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.util.YanzhengUtil;
import com.example.app5libbase.views.ClearableEditText;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.presenter.ChangePhonePresenter1;
import com.sdzn.fzx.teacher.view.ChangePhoneViews;

import java.io.InputStream;


public class ChangePhoneFragment extends BaseFragment implements ChangePhoneViews, View.OnClickListener {
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView titleTv;
    private LinearLayout confirmLy;
    private TextView phoneTx;
    private ClearableEditText userNumEdit;
    private TextView codeTx;
    private ClearableEditText userCodeEdit;
    private TextView countDownBtn;
    private ImageView Effectiveness;
    private EditText valicodeEdit;

    private String userPhone;
    private String valicode;
    private String title;
    private int flag;
    ChangePhonePresenter1 mPresenter;

    public ChangePhoneFragment() {
        // Required empty public constructor
    }

    public static ChangePhoneFragment newInstance(Bundle bundle) {
        ChangePhoneFragment fragment = new ChangePhoneFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initPresenter();
        View view = inflater.inflate(R.layout.fragment_change_phone, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        titleTv = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        phoneTx = (TextView) view.findViewById(R.id.phone_tx);
        userNumEdit = (ClearableEditText) view.findViewById(R.id.user_num_edit);
        codeTx = (TextView) view.findViewById(R.id.code_tx);
        userCodeEdit = (ClearableEditText) view.findViewById(R.id.user_code_edit);
        countDownBtn = (TextView) view.findViewById(R.id.count_down_btn);
        Effectiveness = view.findViewById(R.id.iv_effectiveness);
        valicodeEdit = view.findViewById(R.id.valicode_edit);

        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        countDownBtn.setOnClickListener(this);
        Effectiveness.setOnClickListener(this);
        initData();
        mPresenter.QueryImgToken();
        return view;
    }

    private void initData() {
        if (getArguments() != null) {
            title = getArguments().getString("title");
            flag = getArguments().getInt("flag");
            titleTv.setText(title);
        }

    }

    public void initPresenter() {
        mPresenter = new ChangePhonePresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestory();
        YanzhengUtil.timer_des();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (id == R.id.confirm_ly) {
            confirm();
        } else if (id == R.id.count_down_btn) {
            getCode();
        } else if (id == R.id.iv_effectiveness) {
            mPresenter.QueryImgToken();
        }
    }

    private String editcode;

    private void confirm() {
        userPhone = userNumEdit.getText().toString();
        editcode = userCodeEdit.getText().toString().trim();
        valicode = valicodeEdit.getText().toString().trim();
        if (userPhone.equals("")) {
            ToastUtil.showCenterBottomToast("手机号不能为空");
        } else if (editcode.equals("")) {
            ToastUtil.showCenterBottomToast("验证码不能为空");
        } else if (valicode.equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.checkVerityNum("Bearer " + (String) SPUtils.getInstance().getString("token", ""), userPhone, userCodeEdit.getText().toString());
        }
    }

    private void getCode() {
        userPhone = userNumEdit.getText().toString();
        valicode = valicodeEdit.getText().toString().trim();
        if (!StringUtils.isMobile(userPhone)) {
            ToastUtils.showShort("请输入正确的手机号格式");
        } else if (valicode.equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.sendVerityCode(userPhone, valicode, imgToken);
        }
    }

    @Override
    public void OnVerityCodeSuccess(Object success) {
        ToastUtils.showShort(String.valueOf(success));
        YanzhengUtil.startTime(60 * 1000, countDownBtn);//倒计时bufen
    }

    @Override
    public void OnVerityCodeFailed(String msg) {
        valicodeEdit.setText("");
        ToastUtils.showShort("获取验证码失败");
        mPresenter.QueryImgToken();
    }

    @Override
    public void OnChangPhoneSuccess(String success) {
        ToastUtils.showShort(success);
        getFragmentManager().popBackStack();
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        Bitmap bitimg = BitmapFactory.decodeStream(inputStream);
        Glide.with(getActivity()).load(bitimg).into(Effectiveness);
    }

    @Override
    public void onChangPhoneFailed(String msg) {
        valicodeEdit.setText("");
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    @Override
    public void onImgTokenFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onImgFailed(String msg) {
        ToastUtils.showShort(msg);
    }


    @Override
    public String getIdentifier() {
        return null;
    }
}
