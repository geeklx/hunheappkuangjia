package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5grzx.R;

import com.example.app5libbase.util.CircleTransform;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ClassGroupingGridviewAdapter extends BaseAdapter {

    private List<ClassGroupingVo.DataBean> list;

    private Context context;

    public ClassGroupingGridviewAdapter(List<ClassGroupingVo.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.class_grouping_gridview_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(list.get(i).getPhoto())) {
            Glide.with(context).load(list.get(i).getPhoto()).apply(new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(context))).into(holder.photo);
        }
        if (!TextUtils.isEmpty(list.get(i).getStudentName())) {
            holder.studentName.setText(list.get(i).getStudentName());
        }
        if (list.get(i).getSex() == 1) {
            holder.sexImg.setImageResource(R.mipmap.nan_icon);
        } else {
            holder.sexImg.setImageResource(R.mipmap.nv_icon);
        }
        return view;
    }

    static class ViewHolder {
        ImageView photo;
        TextView studentName;
        ImageView sexImg;

        ViewHolder(View view) {
            photo = view.findViewById(R.id.photo);
            studentName = view.findViewById(R.id.student_name);
            sexImg = view.findViewById(R.id.sex_img);
        }
    }
}
