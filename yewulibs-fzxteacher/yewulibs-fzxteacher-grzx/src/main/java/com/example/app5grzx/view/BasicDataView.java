package com.example.app5grzx.view;


import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.UserBean;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/15
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface BasicDataView extends BaseView {
    void getUserInfo(UserBean loginBean);
}
