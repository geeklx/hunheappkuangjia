package com.example.app5grzx.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.example.app5libpublic.event.UpdateSubject;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libbase.views.DelActivityDialog;
import com.example.app5libbase.views.NoScrollGridView;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TakeOfficeAdapter extends BaseAdapter {
    private List<TakeOfficeVo.DataBean> list;
    private Context context;
    private int subjectId;
    private DelActivityDialog delActivityDialog;

    public TakeOfficeAdapter(List<TakeOfficeVo.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
//        subjectId = SubjectSPUtils.getCurrentSubject().getSubjectId();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.teach_info_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.roleName.setText(list.get(i).getRole());
        holder.subjectName.setText(list.get(i).getSubjectName());
        holder.selectLl.setVisibility(View.GONE);

//        holder.selectImg.setImageResource(subjectId == list.get(i).getSubjectId() ? R.mipmap.chengse_sel : R.mipmap.cengse_nor);

        TakeOfficeGridViewAdapter takeOfficeGridViewAdapter = new TakeOfficeGridViewAdapter(list.get(i).getClassList(), context);
        holder.className.setAdapter(takeOfficeGridViewAdapter);
//        holder.selectLl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (subjectId == list.get(i).getSubjectId()) {
////                    return;
////                }
////                ActivityGvAdapter((Activity) context, list.get(i).getSubjectName(), list.get(i).getSubjectId()).show();
//            }
//        });


        return view;
    }

//    public DelActivityDialog ActivityGvAdapter(Activity cxt, final String text, final int id) {
//        delActivityDialog = new DelActivityDialog()
//                .creatDialog(cxt)
//                .buildText("是否切换当前学科为" + text)
//                .buildListener(new DelActivityDialog.OptionListener() {
//                    @Override
//                    public void onConfirmed() {
//                        LoginBean.DataBean.SubjectListBean subjectListBean = new LoginBean.DataBean.SubjectListBean();
//                        subjectListBean.setSubjectId(id);
//                        subjectListBean.setSubjectName(text);
//                        SubjectSPUtils.saveCurrentSubject(subjectListBean);
//                        EventBus.getDefault().post(new UpdateSubject());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                    }
//                });
//        return delActivityDialog;
//    }

    static class ViewHolder {
        TextView roleTv;
        TextView roleName;
        TextView subjectTv;
        TextView subjectName;
        TextView classTv;
        NoScrollGridView className;
        ImageView selectImg;
        LinearLayout selectLl;

        ViewHolder(View view) {
            roleTv = view.findViewById(R.id.role_tv);
            roleName = view.findViewById(R.id.role_name);
            subjectTv = view.findViewById(R.id.subject_tv);
            subjectName = view.findViewById(R.id.subject_name);
            classTv = view.findViewById(R.id.class_tv);
            className = view.findViewById(R.id.class_name);
            selectImg = view.findViewById(R.id.select);
            selectLl = view.findViewById(R.id.select_rl);
        }
    }
}
