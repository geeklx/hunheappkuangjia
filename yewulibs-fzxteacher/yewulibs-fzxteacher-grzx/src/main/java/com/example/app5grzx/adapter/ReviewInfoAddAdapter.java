package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.ReviewInfoAdd;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ReviewInfoAddAdapter extends BaseAdapter {

    private List<ReviewInfoAdd.DataBean> dataBeanList;
    private Context context;
    private ReviewInfoAddGridviewAdapter reviewInfoAddGridviewAdapter;

    private setDataClick setDataClick;

    public ReviewInfoAddAdapter(List<ReviewInfoAdd.DataBean> dataBeanList, Context context) {
        this.dataBeanList = dataBeanList;
        this.context = context;
    }

    public void setDataClick(ReviewInfoAddAdapter.setDataClick onCheckClick) {
        this.setDataClick = onCheckClick;
    }

    @Override
    public int getCount() {
        return dataBeanList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataBeanList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_review_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(dataBeanList.get(i).getBaseGradeName())) {
            holder.gradeName.setText(dataBeanList.get(i).getBaseGradeName());
        } else {
            holder.gradeName.setText("");
        }
        reviewInfoAddGridviewAdapter = new ReviewInfoAddGridviewAdapter(dataBeanList.get(i).getReviewList(), context);
        holder.gridView.setAdapter(reviewInfoAddGridviewAdapter);
        reviewInfoAddGridviewAdapter.setOnCheckClick(new ReviewInfoAddGridviewAdapter.onCheckClick() {
            @Override
            public void setReviewType(int reviewType) {
                setDataClick.setData(reviewType, dataBeanList.get(i).getBaseGradeName(), dataBeanList.get(i).getBaseGradeId());
            }
        });
        return view;
    }

    static class ViewHolder {
        TextView gradeName;
        GridView gridView;

        ViewHolder(View view) {
            gradeName = view.findViewById(R.id.grade_name);
            gridView = view.findViewById(R.id.gridView);
        }
    }

    public interface setDataClick {
        void setData(int reviewType, String gradeName, int id);
    }
}
