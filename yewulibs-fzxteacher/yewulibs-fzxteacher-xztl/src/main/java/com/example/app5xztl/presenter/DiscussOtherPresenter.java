package com.example.app5xztl.presenter;



import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.chatroom.ChatOtherBean;
import com.example.app5xztl.view.DiscussOtherView;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2019/8/28 0012.
 */

public class DiscussOtherPresenter extends BasePresenter<DiscussOtherView, BaseActivity> {


    public void getDiscussResult(String groupChatTaskId){
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .resultGroupChat(groupChatTaskId)
                .map(new StatusFunc<ChatOtherBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ChatOtherBean>(new SubscriberListener<ChatOtherBean>(){

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable!=null) {
                            mView.netError(throwable.toString());
                        }
                    }

                    @Override
                    public void onNext(ChatOtherBean chatOtherBean) {
                        mView.getDiscussResult(chatOtherBean);
                    }
                },mActivity,true,false,false,""));

    }

    public void setScore(String groupChatTaskId,String score){
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .setGroupScore(groupChatTaskId,score)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>(){

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable!=null) {
                            mView.netError(throwable.toString());
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        mView.successScore(o.toString());
                    }
                });

    }



}
