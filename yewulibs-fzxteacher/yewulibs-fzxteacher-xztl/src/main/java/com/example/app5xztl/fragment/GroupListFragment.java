package com.example.app5xztl.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libbase.listener.chatroom.OnGroupListener;
import com.example.app5libbase.util.chatroom.SpacesItemAllDecoration;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.listener.OnItemTouchListener;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.EmptyRecyclerView;
import com.example.app5xztl.presenter.GroupListPresenter;
import com.example.app5xztl.view.GroupListView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.vo.chatroom.GroupListBean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 小组讨论-- recyclerview展示界面
 */

public class GroupListFragment extends MBaseFragment<GroupListPresenter> implements GroupListView, OnRefreshListener, OnLoadMoreListener, OnGroupListener {
    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView rv;
    private LinearLayout llEmpty;
    private ImageView ivEmpty;
    private TextView tvEmpty;

    private int itemCount = 0;//标签  历史位置

    private int subjectId;//学科
    private int currPage = 1;
    private int pageSize = 10;

    private Y_MultiRecyclerAdapter disAdapter;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private String searchStr;//搜索
    private long currentTime;

    public static GroupListFragment newInstance(Bundle bundle) {
        GroupListFragment fragment = new GroupListFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new GroupListPresenter();
        mPresenter.attachView(this, activity);
    }

    /**
     * 获取数据
     */
    private void getGroupDis() {
//        Map<String, String> params = new HashMap<>();
//        params.put("userStudentId", UserController.getUserId());
//        if (subjectId > 0) {
//            params.put("baseSubjectId", String.valueOf(subjectId));
//        }
//        if (searchStr != null) {
//
//        }
//        params.put("page", String.valueOf(currPage));
//        params.put("rows", String.valueOf(pageSize));
//        mPresenter.getGroupDisData(params);
    }

    /**
     * 搜索   GroupDiscussionFragment-->传入
     */
    public void search(String searchStr) {
        currPage = 1;
        itemEntityList.clear();
        this.searchStr = searchStr;

        getData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_discuss, container, false);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        rv = (EmptyRecyclerView) rootView.findViewById(R.id.rv);
        llEmpty = (LinearLayout) rootView.findViewById(R.id.llEmpty);
        ivEmpty = (ImageView) rootView.findViewById(R.id.ivEmpty);
        tvEmpty = (TextView) rootView.findViewById(R.id.tvEmpty);

        initView();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        currPage = 1;
        getData();
        disAdapter.notifyDataSetChanged();
    }

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));
        rv.setEmptyView(llEmpty);
//        LinearLayoutManager linearLayoutManagerF = new LinearLayoutManager(App2.get());
//        rv.setLayoutManager(linearLayoutManagerF);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(App2.get(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int count = 1;
                if (position == 0 || position == itemCount) {
                    return 2;
                }
                return count;
            }
        });
        rv.addItemDecoration(new SpacesItemAllDecoration(10));
        rv.setLayoutManager(gridLayoutManager);
        disAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        rv.setAdapter(disAdapter);
        rv.addOnItemTouchListener(new OnItemTouchListener(rv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
//                Intent intent = new Intent(getActivity(), DiscussActivity.class);
//                startActivity(intent);
                Object itemData = itemEntityList.getItemData(vh.getAdapterPosition());
//                if (itemData instanceof TaskListVo.DataBean) {
//
//                }
            }
        });

    }

    private void getData() {
        if (versionId == null) {
            versionId = "";
        }
        if (nodeId == null) {
            nodeId = "";
        }
        if (classId.isEmpty() || "".equals(classId)) {

        }
        Map<String, Object> params = new HashMap<>();
        params.put("subjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
        params.put("classId", classId);
        params.put("baseVolumeId", versionId);
        params.put("chapterId", nodeId);
        params.put("rows", 20);
        params.put("page", currPage);
        mPresenter.getDiscussData(params);

    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 1;
        itemEntityList.clear();
        getData();
        cancilLoadState();
    }

    /**
     * 加载更多  此处进行页数处理    getGroupDis(); currPage++;
     */
    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        getData();
    }

    private List<GroupListBean.DataBean> nowData = new ArrayList<>();
    private List<GroupListBean.DataBean> oldData = new ArrayList<>();

    private void setDis(List<GroupListBean.DataBean> dis) {
        for (int a = 0; a < dis.size(); a++) {
            if (dis.get(a).getState() == 0 && !itemEntityList.getItems().contains("当前讨论")) {
                itemEntityList.addItem(R.layout.item_fragment_discuss_top, "当前讨论")
                        .addOnBind(R.layout.item_fragment_discuss_top, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                holder.setText(R.id.tvName, (String) itemData);
                            }
                        });
            } else if (dis.get(a).getState() == 1 && !itemEntityList.getItems().contains("历史讨论")) {
//                itemCount = itemEntityList.getItemCount();
                itemEntityList.addItem(R.layout.item_fragment_discuss_top, "历史讨论")
                        .addOnBind(R.layout.item_fragment_discuss_top, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                holder.setText(R.id.tvName, (String) itemData);
                            }
                        });
            }
            itemEntityList.addItem(R.layout.item_fragment_discuss_n, dis.get(a))
                    .addOnBind(R.layout.item_fragment_discuss_n, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindHolder(rv.getHeight(), holder, (GroupListBean.DataBean) itemData, subjectId);
                        }
                    });


        }
    }

    @Override
    public void getDiscussSuccess(GroupListBean discussBeanList) {
        currentTime = Calendar.getInstance().getTimeInMillis();
        cancilLoadState();
        // 第一页，需要先清空
        if (currPage == 1) {
            itemCount = 0;
            itemEntityList.clear();
        }
        nowData.clear();
        oldData.clear();


        if (discussBeanList.getData().size() > 0) { //此处数据判断
            for (int i = 0; i < discussBeanList.getData().size(); i++) {
                if (discussBeanList.getData().get(i).getState() == 0) {
                    nowData.add(discussBeanList.getData().get(i));
                } else {
                    oldData.add(discussBeanList.getData().get(i));
                }
            }
            if (currPage == 1) {
                if (nowData.size() > 0) {
                    itemCount = nowData.size() + 1;
                } else {
                    itemCount = 0;
                }
            }
            setDis(nowData);
            setDis(oldData);

            disAdapter.notifyDataSetChanged();
            if (discussBeanList.getData().size() < 20) {
                refreshLayout.setEnableLoadMore(false);
            } else {
                refreshLayout.setEnableLoadMore(true);
            }

        } else {
            itemEntityList.clear();
            disAdapter.notifyDataSetChanged();
            rv.setEmptyView(llEmpty);
            refreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void returnSuccess() {
        currPage = 1;
        itemEntityList.clear();
        getData();
    }

    @Override
    public void failedText(String s) {
        ToastUtil.showShortlToast("操作失败：" + s);
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        } else if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadMore();
        }
    }

    /**
     * @param classId
     */
    private String versionId = "", nodeId = "", classId = "";

    /**
     * 传过来 classId
     *
     * @param
     */
    @Override
    public void onSearch(String cId) {
        classId = cId;
        currPage = 1;
        itemEntityList.clear();
        getData();
    }

    @Override
    public void onVersionId(String version) {
        versionId = version;
    }

    @Override
    public void onNodeId(String node) {
        nodeId = node;
        currPage = 1;
        itemEntityList.clear();
        getData();

    }
}
