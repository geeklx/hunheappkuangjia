package com.example.app5xztl.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libpublic.event.FullImageEvBus;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.util.ImageLoaderOptions;
import com.example.app5libbase.views.CustomDialog;

import org.greenrobot.eventbus.EventBus;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * 作者：Rance on 2019/8/29
 */
public class FullImageActivity extends BaseActivity {
    private RelativeLayout activityDisplay;
    private PhotoView ivDisplay;
    private TextView tvDel;

    private String photoUrl;
    private boolean showDel = false;
    public static int RESULT_OK_FULL_IMAGE = 1200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        activityDisplay = (RelativeLayout) findViewById(R.id.activity_display);
        ivDisplay = (PhotoView) findViewById(R.id.iv_display);
        tvDel = (TextView) findViewById(R.id.tvDel);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入FullImageActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        ImageLoaderOptions.displayImage(photoUrl, ivDisplay);
        ivDisplay.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float x, float y) {
                back(false);
            }
        });
        tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("确定删除吗？", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        back(true);
                    }
                });
            }
        });
        if (showDel) {
            tvDel.setVisibility(View.VISIBLE);
        } else {
            tvDel.setVisibility(View.GONE);
        }
    }

    public void showDialog(String msg, final DialogInterface.OnClickListener listener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setMessage(msg);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.onClick(dialog, which);
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void initData() {
        photoUrl = getIntent().getStringExtra("photoUrl");
        showDel = getIntent().getBooleanExtra("showDel", false);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            back(false);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void back(boolean isDel) {
        if (showDel) {
            EventBus.getDefault().post(new FullImageEvBus(isDel));
        }
        finish();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }

}
