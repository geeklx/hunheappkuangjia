package com.example.app5xztl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.app5libbase.R;
import com.example.app5libbase.listener.chatroom.DiscussionDeleteImage;
import com.example.app5libbase.util.chatroom.GlideHelper;

import java.util.List;

public class NewImageGridViewAdapter extends BaseAdapter {
    private Context context;
    private List<String> list;
    LayoutInflater layoutInflater;
    private ImageView mImageView,image_close;

    public NewImageGridViewAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {//注意此处
        return list.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = layoutInflater.inflate(R.layout.grid_item_upload_image, null);
        mImageView = (ImageView) convertView.findViewById(R.id.image);
        image_close = (ImageView) convertView.findViewById(R.id.image_close);
        if (position < list.size()) {
            Glide.with(context).load(list.get(position)).into(mImageView);

            GlideHelper.load(context,list.get(position),mImageView);

        }else{
            mImageView.setBackgroundResource(R.mipmap.image_upload_gray);
            image_close.setVisibility(View.GONE);
        }
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DiscussionDeleteImage)context).deleteImage(position);
            }
        });
        return convertView;
    }

}
