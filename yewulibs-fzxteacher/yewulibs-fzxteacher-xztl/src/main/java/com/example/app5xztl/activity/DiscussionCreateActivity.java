package com.example.app5xztl.activity;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libpublic.event.FullImageEvBus;
import com.example.app5libbase.listener.chatroom.AlbumOrCameraListener;
import com.example.app5libbase.listener.chatroom.DiscussionDeleteImage;
import com.example.app5libbase.util.chatroom.ChatUtils;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.example.app5libbase.util.GlideImageLoader;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.datefive.CustomDatePicker;
import com.example.app5libbase.views.EmptyRecyclerView;
import com.example.app5libbase.views.NoScrollGridView;
import com.example.app5libbase.views.SpinnerView;
import com.example.app5libbase.views.VersionSpinner;
import com.example.app5xztl.adapter.NewImageGridViewAdapter;
import com.example.app5xztl.presenter.DiscussionCreatePresenter;
import com.example.app5xztl.view.DiscussionCreateView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.VersionBean;
import com.sdzn.fzx.teacher.vo.chatroom.ClassGroup;
import com.sdzn.fzx.teacher.vo.chatroom.DiscussionClassGroup;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;

import static com.example.app5xztl.presenter.DiscussionNewPresenter.REQ_CODE_CAMERA;

public class DiscussionCreateActivity extends MBaseActivity<DiscussionCreatePresenter> implements DiscussionCreateView,
        View.OnClickListener, DiscussionDeleteImage, EasyPermissions.PermissionCallbacks {
    private RelativeLayout rl;
    private TextView tvBack;
    private TextView tvCreate;
    private View view;
    private RelativeLayout head;
    private TextView tvMaterials;
    private TextView versionText;
    private View line;
    private TextView nodeText;
    private TextView tvTitleNum;
    private EditText etTitle;
    private TextView tvTextTitle;
    private TextView tvContentNum;
    private EditText etContent;
    private TextView tvTextContent;
    private NoScrollGridView gridView;
    private EmptyRecyclerView recyclerView;
    private TextView dateChooseTxt;
    private SpinnerView spinnerView;
    private VersionSpinner versionSpinner;


    NewImageGridViewAdapter gridViewAdapter;
    final List<String> mDatas = new ArrayList<>();
    private int lessonId = 0;

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter mAdapter;

    private static final int REQUECT_CODE_CAMERA = 1100;
    private int deletePos;
    private int numSelect = 6;


    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Dialog dialog;

    @Override
    public void initPresenter() {
        mPresenter = new DiscussionCreatePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_create);
        dialog = new Dialog(DiscussionCreateActivity.this, R.style.notice_dialog);

        rl = (RelativeLayout) findViewById(R.id.rl);
        tvBack = (TextView) findViewById(R.id.tv_back);
        tvCreate = (TextView) findViewById(R.id.tv_create);
        view = (View) findViewById(R.id.view);
        head = (RelativeLayout) findViewById(R.id.head);
        tvMaterials = (TextView) findViewById(R.id.tv_materials);
        versionText = (TextView) findViewById(R.id.version_text);
        line = (View) findViewById(R.id.line);
        nodeText = (TextView) findViewById(R.id.node_text);
        tvTitleNum = (TextView) findViewById(R.id.tv_title_num);
        etTitle = (EditText) findViewById(R.id.et_title);
        tvTextTitle = (TextView) findViewById(R.id.tv_text_title);
        tvContentNum = (TextView) findViewById(R.id.tv_content_num);
        etContent = (EditText) findViewById(R.id.et_content);
        tvTextContent = (TextView) findViewById(R.id.tv_text_content);
        gridView = (NoScrollGridView) findViewById(R.id.gridView);
        recyclerView = (EmptyRecyclerView) findViewById(R.id.recyclerView);
        dateChooseTxt = (TextView) findViewById(R.id.date_choose_txt);
        spinnerView = (SpinnerView) findViewById(R.id.spinner_view);
        versionSpinner = (VersionSpinner) findViewById(R.id.version_spinner_view);

        EventBus.getDefault().register(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入DiscussionCreateActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        versionText.setOnClickListener(this);
        nodeText.setOnClickListener(this);
        dateChooseTxt.setOnClickListener(this);
        show();
        tvBack.setOnClickListener(this);
        tvCreate.setOnClickListener(this);
        etTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvTitleNum.setText(String.valueOf(150 - charSequence.length()) + "字");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvContentNum.setText(String.valueOf(500 - charSequence.length()) + "字");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        LinearLayoutManager linearLayoutManagerLesson = new LinearLayoutManager(App2.get());
        recyclerView.setLayoutManager(linearLayoutManagerLesson);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new OnItemTouchListener(recyclerView) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                if (vh.getAdapterPosition() < 0 || vh.getAdapterPosition() > itemEntityList.getItemCount()) {
                    return;
                }
//                LessonListVo.DataBean dataBean = (LessonListVo.DataBean) itemEntityList.getItemData(vh.getAdapterPosition());

            }
        });

        versionSpinner.setOnItemChoosedListener(new VersionSpinner.ItemChoosedListener() {
            @Override
            public void onItemChoosed(VersionBean.DataBean.VolumeListBean bean) {
                onVersionChoose(bean);
            }
        });

        spinnerView.setItemChoosedListener(new SpinnerView.ItemChooseListener() {
            @Override
            public void onItemChoosed(NodeBean.DataBeanXX.DataBean data) {
                onNodeChoose(data);
            }
        });

    }

    @Override
    protected void initData() {
        mPresenter.getClassList();

        gridViewAdapter = new NewImageGridViewAdapter(this, mDatas);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == adapterView.getChildCount() - 1) {//添加
                    mPresenter.showSelectImgDialog(new AlbumOrCameraListener() {
                        @Override
                        public void selectAlbum() {
                            if (numSelect == 0) {
                                ToastUtil.showShortlToast("图片最多选6张");
                            } else {
                                new PhotoPickConfig.Builder(mPresenter.mActivity)
                                        .imageLoader(new GlideImageLoader())
                                        .showCamera(false)
                                        .maxPickSize(numSelect)
                                        .spanCount(8)
                                        .clipPhoto(false)
                                        .build();
                            }
                        }

                        @Override
                        public void selectCamera() {
                            requestPermissiontest();
                        }
                    });
                } else {
                    deletePos = i;

                    Intent intentImage = new Intent(AppUtils.getAppPackageName() + ".hs.act.FullImageActivity");
                    intentImage.putExtra("photoUrl", mDatas.get(i));
                    intentImage.putExtra("showDel", true);
                    startActivity(intentImage);
                }

            }
        });

    }


    @Override
    public void networkError(String msg) {

    }


    @Override
    public void onUploadPicSuccess(UploadPicVo uploadVos) {
        for (int a = 0; a < uploadVos.getData().size(); a++) {
            mDatas.add(uploadVos.getData().get(a).getOriginalPath());
            if (mDatas.size() > 6) {
                ToastUtil.showShortlToast("图片最多选6张");
            }
        }
        numSelect = 6 - mDatas.size();
        if (gridViewAdapter != null) {
            gridViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClassSuccessed(SyncClassVo o) {
        for (int a = 0; a < o.getData().size(); a++) {
            mPresenter.getClassGroupList(o.getData().get(a).getClassId(), o.getData().get(a).getBaseGradeName() + o.getData().get(a).getClassName());
        }
    }


    @Override
    public void setClassGropingData(ClassGroupingVo classGroupingVo, String classId, String className) {
        DiscussionClassGroup classGroup = new DiscussionClassGroup();
        List<DiscussionClassGroup.DataB> classList = new ArrayList<>();
        classGroup.setClassName(className);
        classGroup.setClassId(classId);
        for (int data = 0; data < classGroupingVo.getData().size(); data++) {
            classList.add(new DiscussionClassGroup.DataB(String.valueOf(classGroupingVo.getData().get(data).getClassGroupId()), classGroupingVo.getData().get(data).getClassGroupName()));
        }
        classGroup.setDataBList(classList);

        itemEntityList.addItem(R.layout.item_discussion_create, classGroup)
                .addOnBind(R.layout.item_discussion_create, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        mPresenter.bindHolder(recyclerView.getHeight(), holder, (DiscussionClassGroup) itemData, position);
                    }
                });
        mAdapter.notifyDataSetChanged();


    }

    private List<ClassGroup> allList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();

    @Override
    public void getClassGroupInfo(String classId, List<String> groupsList) {
        Iterator<ClassGroup> iterator = allList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getmClassId().trim().equals(classId.trim())) {
                iterator.remove();
            }
        }
        allList.addAll(ChatUtils.copyIterator(iterator));

        for (int add = 0; add < groupsList.size(); add++) {
            allList.add(new ClassGroup(classId, groupsList.get(add)));
        }

        classList.clear();
        for (ClassGroup group : allList) {
            if (!classList.contains(group.getmClassId())) {
                classList.add(group.getmClassId());
            }
        }

    }

    private JSONArray buildClassInfo(List<String> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (String dataStr : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("classId", dataStr);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private JSONArray buildGroupInfo(List<ClassGroup> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (ClassGroup dataGroup : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("classId", dataGroup.getmClassId());
                jsonObject.put("groupId", dataGroup.getmGroupId());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private void resultSend(List<Photo> pathList) {
        mPresenter.uploadSubjectivePhoto(pathList);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {
            //相册返回图片  图片选择器
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(path);
                    paths.add(photo);
                    resultSend(paths);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        resultSend(photoLists);
                    }
                }

            }
        } else if (requestCode == REQ_CODE_CAMERA) {
            //拍照返回
            if (resultCode == Activity.RESULT_OK) {
                mPresenter.startClipPic(false);
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {

                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(UCrop.getOutput(data));
                paths.add(photo);
                resultSend(paths);
            }
        } else if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            //从设置页面返回，判断权限是否申请。
            if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
//                Toast.makeText(this, "权限申请成功!", Toast.LENGTH_SHORT).show();
                mPresenter.toSystemCamera();
                dialog.dismiss();
            } else {
                requestPermissiontest();
//                Toast.makeText(this, "权限申请失败!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(FullImageEvBus event) {
        if (event.isDel()) {
            deleteImage(deletePos);
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.version_text) {
            spinnerView.setVisibility(View.GONE);

            if (versionSpinner.getVisibility() == View.GONE) {
                mPresenter.getVersionList();
                versionSpinner.setList(versionList);
            } else {
                versionSpinner.setVisibility(View.GONE);
            }
        } else if (id == R.id.node_text) {
            versionSpinner.setVisibility(View.GONE);

            if (spinnerView.getVisibility() == View.GONE) {
                spinnerView.setData(nodeList);
            } else {
                spinnerView.setVisibility(View.GONE);
            }
        } else if (id == R.id.tv_back) {
            DiscussionCreateActivity.this.finish();
        } else if (id == R.id.tv_create) {
            if (mDatas.size() > 6) {
                ToastUtil.showShortlToast("最多上传六张图");
            } else if (etTitle.getText().toString().trim().length() < 1) {
                ToastUtil.showShortlToast("标题不能为空");
            } else if ("[]".equals(buildGroupInfo(allList).toString()) || "[]".equals(buildClassInfo(classList).toString())) {
                ToastUtil.showShortlToast("请选择发布小组");
            } else {
                Map<String, Object> param = new HashMap<>();
                param.put("chatTitle", etTitle.getText().toString());
                param.put("chatContent", etContent.getText().toString());
                param.put("subjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
                param.put("chatPicJson", buildChatPic(mDatas));
                param.put("publishGroupJson", buildGroupInfo(allList).toString());
                param.put("publishClassJson", buildClassInfo(classList).toString());
                param.put("endTime", endTime + ":00");
                mPresenter.createGroupDiscussion(param);
            }
        } else if (id == R.id.date_choose_txt) {
            if (dateChooseTxt.getText().toString().isEmpty()) {
                customDatePicker2.show(sdf.format(new Date()));
            } else {
                customDatePicker2.show(dateChooseTxt.getText().toString());
            }
        }

    }

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
    private String endTime = "";
    private CustomDatePicker customDatePicker2;

    private void show() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        }

        String now = sdf.format(date);
        //向后延时间
        endTime = DateUtil.getDateStr(now, 10);
        dateChooseTxt.setText(endTime);

        customDatePicker2 = new CustomDatePicker(this, new CustomDatePicker.ResultHandler() {
            @Override
            public void handle(String time) { // 回调接口，获得选中的时间
                endTime = time + "";
                dateChooseTxt.setText(endTime);
            }
        }, now, "2028-12-31 00:00:00"); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker2.showSpecificTime(true); // 显示时和分
        customDatePicker2.setIsLoop(true); // 允许循环滚动


    }

    private JSONArray buildChatPic(List<String> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (String url : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pic", url);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    @Override
    public void deleteImage(int position) {
        if (mDatas.size() > 0) {
            mDatas.remove(position);
            numSelect = 6 - mDatas.size();
        }
        gridViewAdapter.notifyDataSetChanged();
    }

    /**
     * 顶部
     */
    private ArrayList<VersionBean.DataBean.VolumeListBean> versionList;

    @Override
    public void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> retList) {
        this.versionList = retList;
        if (retList == null || retList.size() == 0) {
            return;
        }
        if (versionSpinner.getVisibility() == View.VISIBLE) {
            versionSpinner.setList(versionList);
        }
    }

    private void onVersionChoose(VersionBean.DataBean.VolumeListBean bean) {
        versionText.setText(bean.getBaseVersionName() + bean.getBaseGradeName() + bean.getBaseVolumeName());

        mPresenter.setBaseGradeId(bean.getBaseGradeId());
        mPresenter.setBaseVolumeId(bean.getBaseVolumeId());
        mPresenter.setBaseVolumeName(bean.getBaseGradeName() + bean.getBaseVolumeName());

        mPresenter.getNodeList(bean.getBaseVersionId());
    }

    private List<NodeBean.DataBeanXX> nodeList;

    @Override
    public void onNodeSuccessed(List<NodeBean.DataBeanXX> list) {

        if (list == null) {
            list = new ArrayList<>();
        }

        NodeBean.DataBeanXX dataBeanXX = new NodeBean.DataBeanXX();
        NodeBean.DataBeanXX.DataBean dataBean = new NodeBean.DataBeanXX.DataBean();
        dataBean.setName("全部章节");
        dataBean.setNodeNamePath("全部章节");
        dataBean.setLeaf(true);
        dataBeanXX.setData(dataBean);
        list.add(0, dataBeanXX);

        onNodeChoose(dataBean);
        nodeList = list;

    }

    @Override
    public void onTitleFailed(String e) {
        ToastUtil.showShortlToast(e);
    }

    @Override
    public void createSuccess() {
        DiscussionCreateActivity.this.finish();
    }

    private void onNodeChoose(NodeBean.DataBeanXX.DataBean data) {
        nodeText.setText(data.getNodeNamePath());
//        mPresenter.setChapterNodeIdPath(data.getNodeIdPath());
//        mPresenter.setChapterNodeName(data.getNodeNamePath());
        mPresenter.setChapterNodeIdPath(String.valueOf(data.getId()));
        mPresenter.setChapterNodeName(data.getNodeNamePath());


    }
    //点击空白处收起键盘

    @Override

    public boolean onTouchEvent(MotionEvent event) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            if (DiscussionCreateActivity.this.getCurrentFocus().getWindowToken() != null) {

                imm.hideSoftInputFromWindow(DiscussionCreateActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            }

        }

        return super.onTouchEvent(event);

    }

    public void requestPermissiontest() {
        if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            mPresenter.toSystemCamera();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }

//        MPermissions.requestPermissions(this, REQUECT_CODE_CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    /* @PermissionGrant(REQUECT_CODE_CAMERA)
     public void requestSdcardSuccess() {
         mPresenter.toSystemCamera();
     }

     @PermissionDenied(REQUECT_CODE_CAMERA)
     public void requestSdcardFailed() {
         ToastUtil.showShortlToast("请设置相机权限");
     }
 */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, ev)) {
                hideKeyboard(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 获取InputMethodManager，隐藏软键盘
     *
     * @param token
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 16061);
                }
            });
        }
    }
}
