package com.example.app5xztl.view;

import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.example.app5libbase.base.BaseView;

public interface DiscussionNewView extends BaseView {

    void networkError(String msg);


    void onUploadPicSuccess(UploadPicVo uploadVos);

    void success(String str);

}
