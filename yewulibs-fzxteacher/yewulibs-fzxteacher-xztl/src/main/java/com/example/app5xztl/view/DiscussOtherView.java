package com.example.app5xztl.view;


import com.sdzn.fzx.teacher.vo.chatroom.ChatOtherBean;
import com.example.app5libbase.base.BaseView;

/**
 * Created by Administrator on 2019/8/28 0011.
 */

public interface DiscussOtherView extends BaseView {
    void getDiscussResult(ChatOtherBean otherBean);

    void netError(String s);

    void successScore(String s);

}
