package com.example.app5xztl.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.donkingliang.labels.LabelsView;
import com.example.app5libbase.R;
import com.example.app5libbase.listener.chatroom.OnGroupListener;
import com.example.app5libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.SpinnerView;
import com.example.app5libbase.views.VersionSpinner;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.VersionBean;
import com.example.app5xztl.presenter.GroupDiscussionPresenter;
import com.example.app5xztl.view.GroupDiscussionView;

import java.util.ArrayList;
import java.util.List;

/**
 * 小组讨论主界面
 */
public class GroupDiscussionFragment extends MBaseFragment<GroupDiscussionPresenter> implements GroupDiscussionView, View.OnClickListener {
    private List<Fragment> fragments;
    //此处 没有切换多种fragment
    private int containerId = R.id.framelayoutGroup;
    private Fragment currFragment;

    private TextView tvTitle;
    private View line1;
    private RelativeLayout head;
    private TextView versionText;
    private View line;
    private TextView nodeText;
    private TextView tvCreate;
    private LinearLayout llHead;
    private LabelsView labelsView;
    private View viewBottom;
    private FrameLayout framelayoutGroup;
    private SpinnerView spinnerView;
    private VersionSpinner versionSpinner;


    private boolean secondRequest = false;

    public GroupDiscussionFragment() {
        // Required empty public constructor
    }

    public static GroupDiscussionFragment newInstance(Bundle bundle) {
        GroupDiscussionFragment fragment = new GroupDiscussionFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new GroupDiscussionPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group_discussion, container, false);
        tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
        line1 = (View) rootView.findViewById(R.id.line_1);
        head = (RelativeLayout) rootView.findViewById(R.id.head);
        versionText = (TextView) rootView.findViewById(R.id.version_text);
        line = (View) rootView.findViewById(R.id.line);
        nodeText = (TextView) rootView.findViewById(R.id.node_text);
        tvCreate = (TextView) rootView.findViewById(R.id.tv_create);
        llHead = (LinearLayout) rootView.findViewById(R.id.ll_head);
        labelsView = (LabelsView) rootView.findViewById(R.id.labels);
        viewBottom = (View) rootView.findViewById(R.id.view_bottom);
        framelayoutGroup = (FrameLayout) rootView.findViewById(R.id.framelayoutGroup);
        spinnerView = (SpinnerView) rootView.findViewById(R.id.spinner_view);
        versionSpinner = (VersionSpinner) rootView.findViewById(R.id.version_spinner_view);
        initData();
        initFragment();
        initView();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (secondRequest) {
            mPresenter.getClassList();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        versionText.setOnClickListener(this);
        nodeText.setOnClickListener(this);
        tvCreate.setOnClickListener(this);
        versionSpinner.setOnItemChoosedListener(new VersionSpinner.ItemChoosedListener() {
            @Override
            public void onItemChoosed(VersionBean.DataBean.VolumeListBean bean) {
                onVersionChoose(bean);
            }
        });

        spinnerView.setItemChoosedListener(new SpinnerView.ItemChooseListener() {
            @Override
            public void onItemChoosed(NodeBean.DataBeanXX.DataBean data) {
                onNodeChoose(data);
            }
        });

    }

    private void initData() {
        mPresenter.getClassList();
        mPresenter.getVersionList();
    }


    /**
     * 章节
     * GroupListFragment groupListFragment = (GroupListFragment) fragments.get(0);
     */

    private void initFragment() {
        fragments = new ArrayList<>();

        fragments.add(GroupListFragment.newInstance(null));
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.add(containerId, currFragment).commit();
    }

//    private void showAssignedFragment(int fragmentIndex) {
//        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        Fragment fragment = fragments.get(fragmentIndex);
//        if (currFragment != fragment) {
//            if (!fragment.isAdded()) {
//                ft.hide(currFragment).add(containerId, fragment, fragment.getClass().getName());
//            } else {
//                ft.hide(currFragment).show(fragment);
//            }
//        }
//        currFragment = fragment;
//        ft.commit();
//    }


    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);

    }

    /**
     * 传值
     */
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnSearch) {
            ((OnGroupListener) fragments.get(0)).onSearch("");
            //传classId
        } else if (id == R.id.tv_create) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.DiscussionCreateActivity");
            startActivity(startIntent);
        } else if (id == R.id.version_text) {
            spinnerView.setVisibility(View.GONE);

            if (versionSpinner.getVisibility() == View.GONE) {
                mPresenter.getVersionList();
                versionSpinner.setList(versionList);
            } else {
                versionSpinner.setVisibility(View.GONE);
            }
        } else if (id == R.id.node_text) {
            versionSpinner.setVisibility(View.GONE);

            if (spinnerView.getVisibility() == View.GONE) {
                spinnerView.setData(nodeList);
            } else {
                spinnerView.setVisibility(View.GONE);
            }
        }

    }

    /**
     * 顶部
     */
    private ArrayList<VersionBean.DataBean.VolumeListBean> versionList;

    @Override
    public void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> retList) {
        this.versionList = retList;
        if (retList == null || retList.size() == 0) {
            return;
        }
        if (versionSpinner.getVisibility() == View.VISIBLE) {
            versionSpinner.setList(versionList);
        }
    }

    private void onVersionChoose(VersionBean.DataBean.VolumeListBean bean) {
        versionText.setText(bean.getBaseVersionName() + bean.getBaseGradeName() + bean.getBaseVolumeName());

        mPresenter.setBaseGradeId(bean.getBaseGradeId());
        mPresenter.setBaseVolumeId(bean.getBaseVolumeId());
        mPresenter.setBaseVolumeName(bean.getBaseVolumeName());

        mPresenter.getNodeList(bean.getBaseVersionId());

        ((OnGroupListener) fragments.get(0)).onVersionId(bean.getBaseVolumeId());
    }

    private List<NodeBean.DataBeanXX> nodeList;

    @Override
    public void onNodeSuccessed(List<NodeBean.DataBeanXX> list) {

        if (list == null) {
            list = new ArrayList<>();
        }

        NodeBean.DataBeanXX dataBeanXX = new NodeBean.DataBeanXX();
        NodeBean.DataBeanXX.DataBean dataBean = new NodeBean.DataBeanXX.DataBean();
        dataBean.setName("全部章节");
        dataBean.setNodeNamePath("全部章节");
        dataBean.setLeaf(true);
        dataBeanXX.setData(dataBean);
        list.add(0, dataBeanXX);

//        onNodeChoose(dataBean);
        nodeList = list;
        if (nodeList.size() > 0) {
            /*当选册别时默认为 全部章节
             */
            nodeText.setText("全部章节");
            ((OnGroupListener) fragments.get(0)).onNodeId("");
        }
    }

    @Override
    public void onTitleFailed() {
    }

    @Override
    public void onClassonError() {
        secondRequest = true;
    }

    @Override
    public void onClassSuccessed(final SyncClassVo o) {

        if (o.getData().size() > 0) {
            labelsView.setLabels(o.getData(), new LabelsView.LabelTextProvider<SyncClassVo.DataBean>() {
                @Override
                public CharSequence getLabelText(TextView label, int position, SyncClassVo.DataBean data) {
                    return data.getClassName();
                }
            });
            labelsView.clearAllSelect();
            labelsView.setSelects(0);
            ((OnGroupListener) fragments.get(0)).onSearch(String.valueOf(o.getData().get(0).getClassId()));
            labelsView.setOnLabelClickListener(new LabelsView.OnLabelClickListener() {
                @Override
                public void onLabelClick(TextView textView, Object ob, int i) {
                    ((OnGroupListener) fragments.get(0)).onSearch(String.valueOf(o.getData().get(i).getClassId()));
                }
            });
        } else {
            if (!secondRequest) {
                //第一次没有classList,请求第二遍
                secondRequest = true;
            }
        }
    }

    private void onNodeChoose(NodeBean.DataBeanXX.DataBean data) {
        nodeText.setText(data.getNodeNamePath());
        mPresenter.setChapterNodeIdPath(String.valueOf(data.getId()));
        mPresenter.setChapterNodeName(data.getName());

        ((OnGroupListener) fragments.get(0)).onNodeId(String.valueOf(data.getId()));


    }


}
