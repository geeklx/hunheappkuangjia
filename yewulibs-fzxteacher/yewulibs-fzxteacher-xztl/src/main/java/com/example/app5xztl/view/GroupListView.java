package com.example.app5xztl.view;


import com.sdzn.fzx.teacher.vo.chatroom.GroupListBean;
import com.example.app5libbase.base.BaseView;

/**
 * Created by Administrator on 2019/8/21 0011.
 */

public interface GroupListView extends BaseView {
    void getDiscussSuccess(GroupListBean discussBeanList);

    void returnSuccess();

    void failedText(String s);
}
