package com.example.app5xztl.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5xztl.adapter.EllGridViewAdapter;
import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.views.NoScrollGridView;

import java.util.ArrayList;

public class PreviewDiscussionActivity extends BaseActivity implements View.OnClickListener {

    private View viewHead;
    private ImageView ivClose;
    private LinearLayout llTitle;
    private TextView tvTitleEll;
    private LinearLayout llContent;
    private TextView tvContentEll;
    private NoScrollGridView gridView;
    private RelativeLayout rlDate;
    private TextView tvDateEll;
    private ImageView imRound;
    private TextView tvChapter;

    EllGridViewAdapter ellGridViewAdapter;
    private ArrayList<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> pics;
    private Bundle bundle;
    private String title, content, endTime, chapterName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_discussion);


        viewHead = (View) findViewById(R.id.view_head);
        ivClose = (ImageView) findViewById(R.id.iv_close);
        llTitle = (LinearLayout) findViewById(R.id.ll_title);
        tvTitleEll = (TextView) findViewById(R.id.tv_title);
        llContent = (LinearLayout) findViewById(R.id.ll_content);
        tvContentEll = (TextView) findViewById(R.id.tv_content);
        gridView = (NoScrollGridView) findViewById(R.id.gridView);
        rlDate = (RelativeLayout) findViewById(R.id.rl_date);
        tvDateEll = (TextView) findViewById(R.id.tv_date);
        imRound = (ImageView) findViewById(R.id.im_round);
        tvChapter = (TextView) findViewById(R.id.tv_chapter_name);


        bundle = getIntent().getExtras();
        chapterName = bundle.getString("chapterName");
        title = bundle.getString("title");
        content = bundle.getString("content");
        endTime = bundle.getString("endTime");
        pics = bundle.getParcelableArrayList("pic");
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PreviewDiscussionActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        ivClose.setOnClickListener(this);
        if (pics.size() > 0) {
            ellGridViewAdapter = new EllGridViewAdapter(PreviewDiscussionActivity.this, pics);
            gridView.setAdapter(ellGridViewAdapter);
        }
        tvTitleEll.setText(title);
        tvContentEll.setText(content);
        tvChapter.setText(chapterName);
        tvDateEll.setText("创建时间：" + endTime);//改为创建时间

    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_close) {
            PreviewDiscussionActivity.this.finish();
        }
    }
}
