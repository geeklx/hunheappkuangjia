package com.example.app5xztl.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5xztl.adapter.EllListGridViewAdapter;
import com.example.app5xztl.adapter.OtherRecyclerViewAdapter;
import com.sdzn.fzx.teacher.vo.chatroom.ChatOtherBean;
import com.sdzn.fzx.teacher.vo.chatroom.GroupListBean;
import com.example.app5libbase.listener.chatroom.OnGroupScoreListener;
import com.example.app5libbase.listener.chatroom.OtherScoreListener;
import com.example.app5xztl.presenter.DiscussOtherPresenter;
import com.example.app5libbase.util.chatroom.ScorePop;
import com.example.app5xztl.view.DiscussOtherView;
import com.example.app5libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 小组结果
 */

public class DiscussOtherActivity extends MBaseActivity<DiscussOtherPresenter> implements DiscussOtherView, View.OnClickListener, OtherScoreListener {
    OtherRecyclerViewAdapter mOtherRecyclerViewAdapter;
    private RelativeLayout rlTop;
    private TextView tvBack;
    private ImageView imMember;
    private TextView tvTitle;
    private View line;
    private FrameLayout frameLayout;
    private RecyclerView mTvRecycler;


    @Override
    public void initPresenter() {
        mPresenter = new DiscussOtherPresenter();
        mPresenter.attachView(this, this);
    }

    private String title, content, endTime, chapterName = "";
    private int groupChatTaskId, id, state;
    private Bundle bundle;
    private List<GroupListBean.DataBean.TeachGroupChatContentPicsBean> pics;
    private EllListGridViewAdapter ellGridViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discuss_other);

        rlTop = (RelativeLayout) findViewById(R.id.rl_top);
        tvBack = (TextView) findViewById(R.id.tvBack);
        imMember = (ImageView) findViewById(R.id.imMember);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        line = (View) findViewById(R.id.line);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        mTvRecycler = (RecyclerView) findViewById(R.id.my_recycler);


        groupChatTaskId = getIntent().getIntExtra("groupChatTaskId", 0);
        bundle = getIntent().getExtras();
        state = bundle.getInt("state");
        groupChatTaskId = bundle.getInt("groupChatTaskId");
        title = bundle.getString("title");
        content = bundle.getString("content");
        endTime = bundle.getString("endTime");
        pics = bundle.getParcelableArrayList("pic");
        chapterName = bundle.getString("chapterName");
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入DiscussOtherActivity成功");
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getDiscussResult(String.valueOf(groupChatTaskId));
    }

    @Override
    protected void initView() {
        tvBack.setOnClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mTvRecycler.setLayoutManager(layoutManager);
        List<ChatOtherBean.DataBean> data = new ArrayList<>();
        mOtherRecyclerViewAdapter = new OtherRecyclerViewAdapter(DiscussOtherActivity.this, title, content, endTime, state, pics, chapterName, data);
        mTvRecycler.setAdapter(mOtherRecyclerViewAdapter);
    }

    @Override
    protected void initData() {


    }

    @Override
    public void getDiscussResult(ChatOtherBean otherBean) {
        if (otherBean.getData().size() > 0) {
            List<ChatOtherBean.DataBean> mDatas = new ArrayList<>();
            for (int a = 0; a < otherBean.getData().size(); a++) {
                if (otherBean.getData().get(a).getTeachGroupChatResultPics().size() > 0) {
                    mDatas.add(otherBean.getData().get(a));
                }
            }
            if (mOtherRecyclerViewAdapter != null) {
                mOtherRecyclerViewAdapter.setListData(mDatas);
            }
        }

    }

    @Override
    public void netError(String s) {

    }

    @Override
    public void successScore(String s) {
        ToastUtil.showShortlToast("已打分");
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tvBack) {
            DiscussOtherActivity.this.finish();
        }
    }

    private ScorePop classPop;

    @Override
    public void onMyScore(final View views, final String chatTaskId) {
        showPop(views, new OnGroupScoreListener() {
            @Override
            public void onScoreClick(String score) {
                ((TextView) views).setText(score + " 分");
                mPresenter.setScore(chatTaskId, score);
                classPop.dismiss();
            }
        });
    }

    private void showPop(View view, OnGroupScoreListener listener) {
        classPop = new ScorePop(activity, listener);
        classPop.showPopupWindow(view);
    }
}
