package com.example.app5xztl.view;

import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.VersionBean;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.ArrayList;
import java.util.List;

public interface DiscussionCreateView extends BaseView {


    void networkError(String msg);

    void onUploadPicSuccess(UploadPicVo uploadVos);

    void onClassSuccessed(SyncClassVo o);

    void setClassGropingData(ClassGroupingVo classGroupingVo, String classId, String className);

    void getClassGroupInfo(String classId, List<String> groupsList);


    void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> list);

    void onNodeSuccessed(List<NodeBean.DataBeanXX> list);

    void onTitleFailed(String e);

    void createSuccess();

}
