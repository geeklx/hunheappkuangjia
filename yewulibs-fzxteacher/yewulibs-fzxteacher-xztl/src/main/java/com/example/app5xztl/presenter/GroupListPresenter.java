package com.example.app5xztl.presenter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.chatroom.GroupListBean;
import com.example.app5xztl.view.GroupListView;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.views.CustomDialog;

import java.util.ArrayList;
import java.util.Map;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2019/8/22 0012.
 */

public class GroupListPresenter extends BasePresenter<GroupListView, BaseActivity> {

    public void getGroupDisData(Map<String, String> params) {

    }

    private Subscription subscribe1;

    public void getDiscussData(Map<String, Object> params) {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        subscribe1 = Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .getGroupChatList(params)
                .map(new StatusFunc<GroupListBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<GroupListBean>(new SubscriberListener<GroupListBean>() {
                    @Override
                    public void onNext(GroupListBean taskListVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getDiscussSuccess(taskListVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
//                                mView.networkError(status.getMsg());
                            } else {
//                                mView.networkError("数据获取失败");
                            }
                        } else {
//                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, false, ""));
        subscriptions.add(subscribe1);

    }

    private String msg = "";

    public void bindHolder(int rvHeight, GeneralRecyclerViewHolder holder, final GroupListBean.DataBean groupBean, int subjectId) {
        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlItem).getLayoutParams();
        layoutParams.height = rvHeight / 3;
        holder.getChildView(R.id.rlItem).setLayoutParams(layoutParams);


        if (groupBean.getState() == 0) {
            holder.getChildView(R.id.tvStatus).setVisibility(View.VISIBLE);
            ((ImageView) holder.getChildView(R.id.imDiscuss)).setImageDrawable(mActivity.getResources().getDrawable(R.mipmap.discuss_group_stop));
            ((ImageView) holder.getChildView(R.id.imDiscuss)).setMaxHeight(25);
            holder.setText(R.id.tvDiscuss, "停止讨论");
            msg = "该任务还未结束，删除后学生将无法讨论\n" +
                    "确定要删除吗？";
            holder.getChildView(R.id.llDiscuss).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new CustomDialog.Builder(mActivity).setMessage("确定要结束讨论吗？")
                            .setPositive("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    stopGroupChat(String.valueOf(groupBean.getId()));
                                }
                            }).setNegative("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
                }
            });

        } else {
            holder.setText(R.id.tvDiscuss, "查看结果");
            holder.getChildView(R.id.tvStatus).setVisibility(View.GONE);
            ((ImageView) holder.getChildView(R.id.imDiscuss)).setImageDrawable(mActivity.getResources().getDrawable(R.mipmap.yulang_icon));
            msg = "删除后学生将看不到该任务\n" +
                    "确定要删除吗？";
            holder.getChildView(R.id.llDiscuss).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {//跳转到结果页

                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.DiscussOtherActivity");
                    Bundle bundle = new Bundle();
                    bundle.putInt("groupChatTaskId",groupBean.getId());
                    bundle.putString("title", groupBean.getChatTitle());
                    bundle.putString("endTime",groupBean.getEndTime());
                    bundle.putString("content",groupBean.getChatContent());
                    bundle.putInt("state",groupBean.getState());
                    bundle.putString("chapterName",groupBean.getChapterName());
                    ArrayList<GroupListBean.DataBean.TeachGroupChatContentPicsBean> pic = (ArrayList<GroupListBean.DataBean.TeachGroupChatContentPicsBean>) groupBean.getTeachGroupChatContentPics();
                    bundle.putParcelableArrayList("pic", pic);
                    intent.putExtras(bundle);
                    mActivity.startActivity(intent);
                }
            });
        }
        holder.setText(R.id.tvName, groupBean.getChatTitle());
        holder.setText(R.id.tvContent, groupBean.getChatContent());
        holder.setText(R.id.tvDate, groupBean.getCreateTime() + "--" + groupBean.getEndTime());

//        Subject subject = Subject.subjects.get(subjectId);
//        if (subject != null) {
//            ((ImageView) holder.getChildView(R.id.ivXueke)).setImageDrawable(mActivity.getResources().getDrawable(subject.getDrawableId()));
//        }

        holder.getChildView(R.id.llDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CustomDialog.Builder(mActivity).setMessage(msg)
                        .setPositive("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deleteGroupList(String.valueOf(groupBean.getId()));
                            }
                        }).setNegative("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
            }
        });
        holder.getChildView(R.id.rlItem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.DiscussOtherActivity");
                Bundle bundle = new Bundle();
                bundle.putInt("groupChatTaskId",groupBean.getId());
                bundle.putString("title", groupBean.getChatTitle());
                bundle.putString("endTime",groupBean.getEndTime());
                bundle.putString("content",groupBean.getChatContent());
                bundle.putInt("state",groupBean.getState());
                bundle.putString("chapterName",groupBean.getChapterName());
                ArrayList<GroupListBean.DataBean.TeachGroupChatContentPicsBean> pic = (ArrayList<GroupListBean.DataBean.TeachGroupChatContentPicsBean>) groupBean.getTeachGroupChatContentPics();
                bundle.putParcelableArrayList("pic", pic);
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
            }
        });

    }

    private void deleteGroupList(String groupChatTaskId) {
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .deleteGroupChat(groupChatTaskId)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.failedText(e.toString());

                    }

                    @Override
                    public void onNext(Object o) {
                        mView.returnSuccess();

                    }
                },mActivity,true,false,false,""));
    }

    private void stopGroupChat(String groupChatTaskId) {
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .stopGroupChat(groupChatTaskId)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.failedText(e.toString());

                    }

                    @Override
                    public void onNext(Object o) {
                        mView.returnSuccess();

                    }
                });
    }


}
