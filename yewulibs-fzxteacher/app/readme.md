> 新增控件说明:
>
> [FillBlankCheckTextView](src/main/java/com/sdzn/fzx/teacher/views/exam/FillBlankCheckTextView.java)
>
----

#### FillBlankCheckTextView
----
新增教师批改填空题控件: 
[FillBlankCheckTextView](src/main/java/com/sdzn/fzx/teacher/views/exam/FillBlankCheckTextView.java)

使用方法:

在布局文件中:


```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto" 
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <!--trueAnswerIcon:正确图标
        falseAnswerIcon:错误图标
        defaultTextColor:默认文字颜色
        trueTextColor:正确答案颜色
        falseTextColor:错误答案颜色-->
    <com.example.app5libpublic.views.exam.FillBlankCheckTextView
        android:id="@+id/tv"
        app:trueAnswerIcon="@mipmap/img_true"
        app:falseAnswerIcon="@mipmap/img_false"
        app:defaultTextColor="@android:color/black"
        app:trueTextColor="@color/trueColor"
        app:falseTextColor="@color/falseColor"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>
</RelativeLayout>

```

在java文件中:

```java
import android.util.SparseArray;

import com.sdzn.fzx.teacher.app.AppLike;
import com.example.app5libpublic.views.exam.FillBlankCheckTextView;
import com.sdzn.fzx.teacher.vo.ClozeAnswerBean;

class Demo {
    String exam = "试题题干";//题干
    SparseArray<ClozeAnswerBean> array;//作答结果
    FillBlankCheckTextView mTextView;
    
    void init() {
        //设置内容
        mTextView.setHtmlBody(exam, array);

        //设置空位图片点击回调:
        mTextView.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                //点击图片空位时触发回调, 返回点击的图片src
            }
        });

        //获取view中作答结果用于展示
        SparseArray<ClozeAnswerBean> answerMap = mTextView.getAnswerMap();

        //修改批改状态后需调用'refresh()'方法刷新数据
        mTextView.refresh();
    }
}
```
