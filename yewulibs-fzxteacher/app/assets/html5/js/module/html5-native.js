function getPreview (param) {
  getAllParams(param);
  PreviewActivity = new PreviewActivity();
}

/**
 *  共用
 */
//发送系统消息
function html5SendSystemInfoToNative (msg) {
  alert(msg);
}

//点击页面上的图片调用原生放大查看
function html5SendPicToNative (src) {
  try {
    window.webkit.messageHandlers.showImage.postMessage(src);
  } catch (e) {
    console.log(e);
  }
  try {
    interfaceName.showImage(src);
  } catch (e) {
    console.log(1);
  }
}

function html5SendOpenImageNative (src) {
  try {
    window.webkit.messageHandlers.openImage.postMessage(src);
  } catch (e) {
    console.log(e);
  }
  try {
    interfaceName.openImage(src);
  } catch (e) {
    console.log(1);
  }
}

//预览资源
function html5ScanResource (data) {

}


//显示loading
function showLoading () {
  interfaceName.showLoading();
}

//隐藏loading
function hideLoading () {
  interfaceName.hideLoading();
}

function getUptoken() {
    var param = {};
    param.host = getUrlParam("host")
    param.access_token = getUrlParam("access_token");
    CorrectHandler.getStudentUptoken(param, this, function (flag, data) {
        if (flag) {
            localStorage.setItem("upToken", data.result.imageStyle);
        }
    });
}
