/**
 * Created by wuhai on 2018/2/6.
 */
var StudentListItems = function (parentView, data) {
    this.parentView = parentView;
    this.data = data;
    this.init();
};
StudentListItems.template = ' <tr><td> <div class= "tab_td_text" ><div sid = "student_info"class = "sysIco img_sys_ico" > ' +
    '<img src = "img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg"/></div > ' +
    '<div  sid="student_name" style="margin-left:20px;"><span style = "margin-left: -30px;" ></span></div></div ></td ><td ><span sid = "student_status" style="padding-left: 15px;">' +
    ' <span>  </span> </span ></td > <td><div sid = "student_time" > <span>  </span >' +
    ' </div ></td ></tr> ';

StudentListItems.prototype.init = function () {
    // this.view = $(StudentListItem.template);
    // this.parentView.append(this.view);
    this.getList();
};
StudentListItems.prototype.getList = function () {
    this.parentView.empty();
    for (var i = 0, len = this.data.length; i < len; i++) {
        this.view = $(StudentListItems.template);
        this.view.find("[sid=student_name]").text(this.data[i].userStudentName);
        if (this.data[i].isRead == 1) {
            this.view.find("[sid=student_status]").text("已完成");
            this.view.find("[sid=student_status]").addClass("number_text_full");
            this.view.find("[sid=student_time]").text(parseInt(this.data[i].useTime / 60) + '分' + this.data[i].useTime % 60 + '秒');
        } else {
            this.view.find("[sid=student_status]").text("未查看");
            this.view.find("[sid=student_status]").addClass("number_text_gray");
            this.view.find("[sid=student_time]").text("——");
        }
        this.studentImageView = this.view.find('[sid=student_image]');
        if (!this.data[i].photo) {
            this.studentImageView.attr('src', 'img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg');
        } else {
            this.studentImageView.attr("src", this.data[i].photo);
        }
        this.parentView.append(this.view);
    }
};
