
var CorrectNav = function (view, data) {
    this.view = view;
    this.data = data;
    //navLabel集合
    this.navLabelList = [];
    //选择值
    this.selectChooserLabel = null;
    this.checkIndex;
    this.init();
};

CorrectNav.prototype.init = function () {

    this.left = this.view.find("[cid=left]");
    this.right = this.view.find("[cid=right]");
    this.center = this.view.find("[cid=center]");
    //清除
    this.center.empty();
    if (this.data.length > 0) {
        for (var i = 0; i < this.data.length; i++) {
            var html = CorrectNav.phtml.clone();
            var d = this.data[i];
            html.attr("nid", d.id);
            var that = this;
            if (d.isCorrect == 1) {
                html.addClass("opt_corr");
            }
            html.off("click").on("click", this, function (evt) {
                var examList = $("[cid=anchor]");
                //if(that.checkIndex!=undefined)
                //    $(that.checkIndex).removeClass("opt");
                $.each(examList, function (i, j) {
                    $(j).parent().removeClass("opt");
                })
                var view = $("body").find("[cid=middle_content]");

                var name = "name=correctName" + $(this).attr("nid");
                var examli = view.find("[" + name + "]");

                var self = evt.data;
                view.animate({scrollTop: view.scrollTop() + examli.offset().top - view.offset().top}, 200);
                examli.parent().addClass("opt");
                examli.parent().find(".exam-scan").trigger("click");
            })
            html.html(d.examSeq);
            this.navLabelList.push(html)
            this.center.append(html);

        }
        setTimeout(function () {
            that.navLabelList[0].trigger("click");
        }, 500);
    }
    this.setWidth(this.center);
};


CorrectNav.prototype.setWidth = function (obj) {
    if (obj.length === 0){
        return
    }
    var repre_right_Left = obj.offset().left;
    var wh = $(obj).parent().width() - repre_right_Left - 25;
    obj.width(wh);
};
CorrectNav.phtml = $('<div class="circle cir_gray"></div>');