var Correct = function (activityData,flagIndex) {
    this.view = $('[cid=container]');
    this.activityData = activityData;
    this.flagIndex = flagIndex;
    this.initView();
};


Correct.prototype.initView = function () {
    this.view.load('html/activity/activity_detail/correct/correct.html', this.init.bind(this));
};

Correct.prototype.init = function () {
    //初始化页面组件及结构
    var o = this.view.find("[cid=correct]");
    this.setHeight(o);
    this.student_view = this.view.find("[sid=datum_left]");       //学生列表
    this.correct_right = this.view.find("[cid=correct_right]");   //右侧工具
    this.correct_middle = this.view.find("[cid=correct_middle]"); //中间试题
    this.middle_content = this.view.find("[cid=middle_content]");
    //处理页面数据及绘制内容
    this.initStudentListView();
};

Correct.prototype.setHeight = function (obj) {
    var repre_right_top = $(obj).offset().top;
    var heig = $(document).height() - repre_right_top;
    obj.height(heig - 10);
};



/**
 * 初始化学生列表
 */
Correct.prototype.initStudentListView = function () {
    console.log("初始化学生列表");
    var param = {};
    param.lessonTaskId = this.activityData.id;                 //任务id
    param.classId = this.activityData.classId;                 //班级id
    CorrectHandler.getStudentCorrectList(param, this, this.getStudentListCallBack);

};

var currectArray = [];
var arrSave;
Correct.prototype.getStudentListCallBack = function (flag, data) {
    this.student_view.empty();
    if (flag) {
        if (data.result && data.result.data) {
            this.data = data.result.data;
            var arr = [];
            for (var i = 0, len = this.data.length; i < len; i++) {
                arr.push(new CorrectListItem(i, this, this.student_view, this.data[i], this.correct_right, this.correct_middle, this.middle_content));
            }
            arr[0].view.trigger('click');
            arrSave = arr;
        } else {
            this.correct_middle.html('<div class="mi-blank"><div class="mi-blank-label">暂无内容~</div></div>');
        }
    } else {
        alert('获取学生列表失败');
    }
};


//每一个学生对象
var CorrectListItem = function (index, obj, studentView, data, rightView, middleView, middleContent) {

    this.index = index;
    this.obj = obj;
    this.studentView = studentView;
    this.rightView = rightView;
    this.middleView = middleView;
    this.middle_content = middleContent
    this.data = data;
    this.init();
};

CorrectListItem.template = '<div class="list_item">' +
    '<div class="tab_td_text">' +
    '<div cid="img_sys_ico" class="sysIco img_sys_ico" style="margin-top: 3px;">' +
    '<img src="img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg">' +
    '</div>' +
    '<div style="position:relative;margin-left: 10px;overflow: hidden;   text-overflow:ellipsis;    white-space: nowrap;width: 80px;;line-height: 38px;">' +
    '<span cid="studentName"></span>' +
    '<i class="redNode" sid="redNode"></i>' +
    '</div>' +
    '</div>' +
    '</div>';

CorrectListItem.prototype.init = function () {
    this.view = $(CorrectListItem.template);
    if (this.data.photo != null) {
        this.view.find("[cid=img_sys_ico]").children("img").attr('src', this.data.photo);
        this.middleView.find("[cid=img_sys_ico]").children("img").attr('src', this.data.photo);
    }
    this.view.find("[cid=studentName]").text(this.data.userStudentName);
    this.studentView.append(this.view);
    if (this.data.isCorrect == 0) {
        this.view.find("[sid=redNode]").removeClass('dn');
    } else {
        this.view.find("[sid=redNode]").addClass('dn');
    }
    this.view.off('click').on('click', this, this.handleClick);
};

CorrectListItem.prototype.handleClickSave = function (stuData) {
    currectArray = [];
    var self = stuData.data;
    var param = {};
    param.lessonTaskStudentId = self.id;  //学生任务id
    param.examType = 2;
    $(this).siblings().removeClass("on");
    $(this).addClass("on");
    CorrectHandler.getAnswerExamList(param, stuData, stuData.getCorrectDetail);
};

CorrectListItem.prototype.handleClick = function (evt) {
    currectArray = [];
    var self = evt.data;
    var param = {};
    param.lessonTaskStudentId = self.data.id;  //学生任务id
    param.examType = 2;
    $(this).siblings().removeClass("on");
    $(this).addClass("on");
    CorrectHandler.getAnswerExamList(param, self, self.getCorrectDetail);
};


CorrectListItem.prototype.getCorrectDetail = function (flag, res) {
    if (flag) {
        //初始化题目列表
        this.nav = new CorrectNav(this.obj.view, res.result.data);

        this.middleView.find("[cid=middle_content]").empty();
        this.rightView.find('[sid=title_nav]').empty();
        console.log(res.result.extend);
        if (res.result.data && res.result.data.length > 0) {
            var that = this;
            this.datasArray = new Array();
            var count = 0;
            $(res.result.data).each(function (key, value) {
                //此处绘制题目相关内容：
                var label = $(CorrectListItem.examTemplate).clone(true);
                label.attr("eid", value.id);
                label.find("[cid=anchor]").attr("name", "correctName" + value.id);
                var tempExamObj = new ExamAnswerPreview(value, value.examSeq + '.', 1, that);
                label.find('[sid=box]').append(tempExamObj.view);
                tempExamObj.view.css("font-size","16px");
                if(tempExamObj.view.find(".exam-scan")){
                  tempExamObj.view.find(".exam-scan").css("font-size","16px");
                }

                label.find('[sid=answer-btn]').on('click', {
                    'obj': that,
                    'examObj': tempExamObj
                }, that.changeAnswerFun);


                //右侧 未批改题数
                if (value.isCorrect === 0)
                    count++;

                // 优秀解答 典型错误
                var typicalCaseView = label.find('[sid=typical_case]');
                new TypicalCase(typicalCaseView, value.id, value.correctType);
                that.middleView.find("[cid=middle_content]").append(label);

                //1正确 2错误 3半对
                //主键id
                //父节点id
                var examText = JSON.parse(value.examText);
                switch (examText.examTypeId) {
                    case 4:
                        if (value.isAnswer == 1) {
                            var param = {};
                            param.id = value.id;
                            param.parentId = value.parentId;
                            param.isRight = value.isRight;
                            param.score = value.score;
                            param.fullScore = value.fullScore;
                            param.isCorrent = value.isCorrect;
                            that.datasArray.push(param);
                        }
                    case 16:
                        for (var i = 0, len = value.examList.length; i < len; i++) {
                            if (value.examList[i].isAnswer == 1) {
                                var param = {};
                                param.id = value.examList[i].id;
                                param.parentId = value.examList[i].parentId;
                                param.isRight = value.examList[i].isRight;
                                param.score = value.examList[i].score;
                                param.fullScore = value.examList[i].fullScore;
                                param.isCorrent = value.examList[i].isCorrect;
                                that.datasArray.push(param);
                            }
                        }
                    default:
                        console.log('未找到对应题型：' + examText.examTypeId);
                }
                // 加戳事件
                var examText = JSON.parse(value.examText);
                switch (examText.examTypeId) {
                    case 4:
                        tempExamObj.view.on('click', {
                            'obj': that,
                            'examObj': tempExamObj,
                            'index': key,
                            'allData': res.result.data,
                            'datasArray': that.datasArray
                        }, that.shortCorrectFun);
                        break;
                    case 16:
                        that.addClickEvent(tempExamObj, that.datasArray, res.result.data, key);
                        break;
                    default:
                        console.log('未找到对应题型：' + examText.examTypeId);
                }
            });
            that.rightView.find('[sid=noCorrect]').text(count);
            //top数据
            this.middleView.find("[cid=middle_stuName]").text(this.data.userStudentName);
            if (this.data.scoreObjective < 0) {
                this.middleView.find("[cid=middle_stuScore]").text("客观题得分：" + "—");
            } else {
                this.middleView.find("[cid=middle_stuScore]").text("客观题得分：" + this.data.scoreObjective + "分");
            }

            // 卷面相关
            this.surfaceView = that.middleView.find('[sid=surface]');
            this.surface = new Surface(this.surfaceView, this.data.id, res.result.extend);

            // 填充右侧题号列表
            //this.titleNav = new ItemNav(that.rightView.find('[sid=title_nav]'), res.result.data);
            this.titleNav = new ItemNav(that.rightView.find('[sid=title_nav]'), res.result.data, this);

            //主观题总题数
            that.rightView.find('[sid=c_total]').text(res.result.total);

            //小工具
            that.rightView.find('[sid=rightButton]').off('click').on('click', this, this.rightClick);
            that.rightView.find('[sid=notRightButton]').off('click').on('click', this, this.notRightClick);
            that.rightView.find('[sid=wrongButton]').off('click').on('click', this, this.wrongClick);
            that.rightView.find('[sid=wipeButton]').off('click').on('click', this, this.wipeClick);

            // 快捷键
            $(document).keydown(function (event) {
                if (event.keyCode == 49) {
                    that.rightClicks(that);
                } else if (event.keyCode == 50) {
                    that.notRightClicks(that);
                } else if (event.keyCode == 51) {
                    that.wrongClicks(that);
                } else if (event.keyCode == 52) {
                    that.wipeClicks(that);
                }
            });

            //保存批改
            that.rightView.find('[sid=saveButton]').off('click').on('click', {
                'obj': this,
                'datasArray': this.datasArray
            }, this.saveCorrectClick);

        } else {
            this.middleView.html('<div class="empty-style"></div>');
        }
    } else {
        alert('获取试题列表失败');
    }
};

CorrectListItem.prototype.addClickEvent = function (obj, datasArray, allData, parentIndex) {
    var subExamArr = obj.view.find('.exam-scan');
    for (var i = 0; i < subExamArr.length; i++) {
        $(subExamArr[i]).on('click', {
            'obj': this,
            'examView': $(subExamArr[i]),
            'examObj': obj,
            'index': i,
            'datasArray': this.datasArray,
            'allData': allData,
            'parentIndex': parentIndex
        }, this.zongCorrectFun);
    }
};

// 显示隐藏答案
CorrectListItem.prototype.changeAnswerFun = function (e) {
    var examObj = e.data.examObj;
    if (!$(this).hasClass('active')) {
        examObj.showAnswer();
        $(this).addClass('active');
        $(this).text('隐藏答案');
    } else {
        examObj.hideAnswer();
        $(this).removeClass('active');
        $(this).text('显示答案');
    }
};

CorrectListItem.examTemplate = '<div> ' +
    '<div style="background-color: #ffffff;min-height: 30px;margin:0px 10px;padding: 10px;position: relative" sid="box"> ' +
    '<a cid="anchor"/>' +
    '<div style="position: absolute;right:0px;" class="cus-radio s-d-show-answer-con" sid="typical_case"> ' +
    '<div class="f_left"><span class="cus-radio-con answer-btn" sid="answer-btn">显示答案&nbsp;&nbsp;&nbsp;</span></div> ' +
    '<div class="f_left" sid="typical_case_good"><span class="cus-radio-pic" sid="pic" key="1" ></span><span ' +
    'class="cus-radio-con">优秀解答</span></div> ' +
    '<div class="f_left"  sid="typical_case_bad"><span class="cus-radio-pic" sid="pic" key="1"></span> ' +
    '<span class="cus-radio-con">典型错误</span></div> ' +
    '</div></div>';

var isclick = 0;
CorrectListItem.prototype.rightClick = function (e) {
    isclick = 1;
    var self = e.data;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right_on');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe');
};

CorrectListItem.prototype.wrongClick = function (e) {
    isclick = 2;
    var self = e.data;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong_on');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe');
};

CorrectListItem.prototype.notRightClick = function (e) {
    isclick = 3;
    var self = e.data;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight_on');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe');
};

CorrectListItem.prototype.wipeClick = function (e) {
    isclick = 4;
    var self = e.data;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe_on');
};


CorrectListItem.prototype.rightClicks = function (e) {
    isclick = 1;
    var self = e;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right_on');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe');
};

CorrectListItem.prototype.wrongClicks = function (e) {
    isclick = 2;
    var self = e;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong_on');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe');
};

CorrectListItem.prototype.notRightClicks = function (e) {
    isclick = 3;
    var self = e;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight_on');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe');
};

CorrectListItem.prototype.wipeClicks = function (e) {
    isclick = 4;
    var self = e;
    self.rightView.find('[sid=rightButton]').attr('class', 'button right');
    self.rightView.find('[sid=wrongButton]').attr('class', 'button wrong');
    self.rightView.find('[sid=notRightButton]').attr('class', 'button notRight');
    self.rightView.find('[sid=wipeButton]').attr('class', 'button wipe_on');
};
// var currectArray = [];
CorrectListItem.prototype.shortCorrectFun = function (e) {
    var index = e.data.index;
    var allData = e.data.allData;
    var examObj = e.data.examObj;
    var obj = e.data.obj;
    var datasArray = e.data.datasArray;
    if (isclick === 1) {
        examObj.view.find('[sid=exam-scan-answer-s]').find('[sid=flag]').attr("class", "flag true-answer");
        allData[index].isRight = 1;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.id) {
                datasArray[i].isRight = 1;
            }
        }
    } else if (isclick === 2) {
        examObj.view.find('[sid=exam-scan-answer-s]').find('[sid=flag]').attr("class", "flag false-answer");
        allData[index].isRight = 2;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.id) {
                datasArray[i].isRight = 2;
            }
        }
    } else if (isclick === 3) {
        examObj.view.find('[sid=exam-scan-answer-s]').find('[sid=flag]').attr("class", "flag half-true");
        allData[index].isRight = 3;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.id) {
                datasArray[i].isRight = 3;
            }
        }
    } else if (isclick === 4) {
        examObj.view.find('[sid=exam-scan-answer-s]').find('[sid=flag]').attr("class", "flag");
        allData[index].isRight = 0;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.id) {
                datasArray[i].isRight = 0;
            }
        }
    }

    currectArray = datasArray;
    //obj.rightView.find('[sid=title_nav]').empty();
    //var titleNav = new ItemNav(obj.rightView.find('[sid=title_nav]'), allData,obj);
    var noCurr = [];
    for (var i = 0; i < currectArray.length; i++) {
        if (currectArray[i].isRight == 0) {
            noCurr.push(currectArray[i]);
        }
    }
    //obj.rightView.find('[sid=noCorrect]').text(noCurr.length);
};

CorrectListItem.prototype.zongCorrectFun = function (e) {
    var obj = e.data.obj;
    var examView = e.data.examView;
    var examObj = e.data.examObj;
    var index = e.data.index;
    var datasArray = e.data.datasArray;
    var allData = e.data.allData;
    var parentIndex = e.data.parentIndex;

    if (isclick === 1) {
        examView.find('[sid=flag]').attr("class", "flag true-answer");
        allData[parentIndex].examList[index].isRight = 1;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.examList[index].id) {
                datasArray[i].isRight = 1;
            }
        }
    } else if (isclick === 2) {
        examView.find('[sid=flag]').attr("class", "flag false-answer");
        allData[parentIndex].examList[index].isRight = 2;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.examList[index].id) {
                datasArray[i].isRight = 2;
            }
        }

    } else if (isclick === 3) {
        examView.find('[sid=flag]').attr("class", "flag half-true");
        allData[parentIndex].examList[index].isRight = 3;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.examList[index].id) {
                datasArray[i].isRight = 3;
            }
        }

    } else if (isclick === 4) {
        examView.find('[sid=flag]').attr("class", "flag");
        allData[parentIndex].examList[index].isRight = 0;
        for (var i = 0; i < datasArray.length; i++) {
            if (datasArray[i].id == examObj.data.examList[index].id) {
                datasArray[i].isRight = 0;
            }
        }
    }
    currectArray = datasArray;

    obj.rightView.find('[sid=title_nav]').empty();
    var titleNav = new ItemNav(obj.rightView.find('[sid=title_nav]'), allData, obj);

    var noCurr = [];
    for (var i = 0; i < currectArray.length; i++) {
        if (currectArray[i].isRight == 0) {
            noCurr.push(currectArray[i]);
        }
    }
    //obj.rightView.find('[sid=noCorrect]').text(noCurr.length);

};


CorrectListItem.prototype.saveCorrectAll = function (correctArr) {
    var self = this;
    var noCurr = [];
    if (correctArr[0].fullScore > 0) {
        for (var i = 0; i < correctArr.length; i++) {
            if (correctArr[i].isCorrent == 0) {
                noCurr.push(correctArr[i]);
            }
        }
    }
    //如果存在isRight为0时执行此方法
    if (noCurr.length != 0) {
        self.allCorrect();
    } else {

        console.log(self);
        //  -分情况下
        if (correctArr[0].fullScore <= 0) {
            for (var i = 0; i < correctArr.length; i++) {
                correctArr[i].isRight = -1;
                correctArr[i].score = -1;
                currectArray[i].isCorrent = 1;
            }
        } else {
            // 设置分数情况
            for (var i = 0; i < correctArr.length; i++) {
                correctArr[i].isRight = correctArr[i].score > 0 ? correctArr[i].score === correctArr[i].fullScore ? 1 : 3 : 2;
                currectArray[i].isCorrent = 1;
            }
        }
        allWR = 0;
        var param = {};
        param.correctJson = JSON.stringify(correctArr);
        param.lessonTaskId = self.data.lessonTaskId;
        param.lessonTaskStudentId = self.data.id;
        CorrectHandler.examCorrectSave(param, self, self.correctCallBack);
    }

}

CorrectListItem.prototype.saveCorrectClick = function (e) {
    var self = e.data.obj;
    var datasArray = e.data.datasArray;
    if (currectArray.length == 0) {
        currectArray = datasArray;
    }
    self.saveCorrectAll(currectArray);
};

CorrectListItem.prototype.allCorrect = function () {
    var self = this;
    $.get("html/activity/activity_detail/correct/allCorrect.html", function (result) {
        self.addTemplate = $(result);
        $("body").append(self.addTemplate);
        self.initAllcorrect();
    });
}

CorrectListItem.prototype.initAllcorrect = function () {
    this.cancelBtn = this.addTemplate.find('[sid=cancel]');
    this.okBtn = this.addTemplate.find('[sid=save]');
    this.okBtn.on('click', this, this.funOk);
    this.cancelBtn.on('click', this, this.funCancel);
    this.allRightView = this.addTemplate.find('[sid=allRight]');
    this.allWrongView = this.addTemplate.find('[sid=allWrong]');
    this.allRightView.find('[sid=pic]').on('click', {'obj': this, 'type': 'right'}, this.onClick);
    this.allWrongView.find('[sid=pic]').on('click', {'obj': this, 'type': 'wrong'}, this.onClick);
};

var allWR = 0;
CorrectListItem.prototype.onClick = function (e) {
    var self = e.data.obj;
    var type = e.data.type;
    if (type == 'right') {
        self.allRightView.find('[sid=pic]').addClass('active');
        self.allWrongView.find('[sid=pic]').removeClass('active');
        allWR = 1;
    }
    if (type == 'wrong') {
        self.allRightView.find('[sid=pic]').removeClass('active');
        self.allWrongView.find('[sid=pic]').addClass('active');
        allWR = 2;
    }
};

CorrectListItem.prototype.funOk = function (e) {
    var self = e.data;
    //  -分情况下
    if (currectArray[0].fullScore <= 0) {
        if (allWR == 1) {
            for (var i = 0; i < currectArray.length; i++) {
                if (currectArray[i].isCorrent == 0) {
                    currectArray[i].isRight = 1;
                    currectArray[i].score = -1;
                    currectArray[i].isCorrent = 1;
                }
            }
        } else if (allWR == 2) {
            for (var i = 0; i < currectArray.length; i++) {
                if (currectArray[i].isCorrent == 0) {
                    currectArray[i].isRight = 2;
                    currectArray[i].score = -1;
                    currectArray[i].isCorrent = 1;
                }
            }
        } else {
            Information.show(2, "请选择全对或全错！");
            return;
        }
    } else {
        // 设置分数情况
        if (allWR == 1) {
            for (var i = 0; i < currectArray.length; i++) {
                if (currectArray[i].isCorrent == 0) {
                    currectArray[i].score = currectArray[i].fullScore;
                    currectArray[i].isCorrent = 1;
                    currectArray[i].isRight = 1;
                }
            }
        } else if (allWR == 2) {
            for (var i = 0; i < currectArray.length; i++) {
                if (currectArray[i].isCorrent == 0) {
                    currectArray[i].score = 0;
                    currectArray[i].isRight = 2;
                    currectArray[i].isCorrent = 1;
                }

            }
        } else {
            Information.show(2, "请选择全对或全错！");
            return;
        }
    }
    allWR = 0;
    var param = {};
    param.correctJson = JSON.stringify(currectArray);
    param.lessonTaskId = self.data.lessonTaskId;
    param.lessonTaskStudentId = self.data.id;
    CorrectHandler.examCorrectSave(param, self, self.correctCallBack2);

};

CorrectListItem.prototype.correctCallBack = function (flag, data) {
    if (flag) {
        alert("保存成功");
        currectArray = [];
        this.obj.initStudentListView();
        this.middle_content.scrollTop(0);
    } else {
        alert(data.msg);
    }
};

CorrectListItem.prototype.correctCallBack2 = function (flag, data) {
    if (flag) {
        alert("保存成功");
        this.addTemplate.remove();
        currectArray = [];
        this.obj.initStudentListView();
        this.middle_content.scrollTop(0);
    } else {
        alert(data.msg);
    }
};

CorrectListItem.prototype.funCancel = function (e) {
    var self = e.data;
    self.addTemplate.remove();
};




