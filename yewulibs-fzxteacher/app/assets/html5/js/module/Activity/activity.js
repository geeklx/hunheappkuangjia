/*
 * author:lixiaoming
 * */

    var Activity = function () {
        this.subjectId = MyStorage.getSubjectId().id;
        this.view = $('[cid=container]');
        this.initView();
    };


    Activity.prototype.initView = function () {
        this.view.load('html/activity/activity.html', this.init.bind(this));
    };

    Activity.prototype.init = function () {
        //条件查询对象：章节、班级、状态
        this.condition = new Condition(this.view, this.subjectId);
        //条件对象监听
        this.condition.addListener(this,this.onQuery);
        //加载完成后执行首次查询
        this.condition.addLoadedListener(this,this.initQuery);
    };

    Activity.prototype.initQuery = function () {
        this.query(this.condition.getInitParam());
    };

    Activity.prototype.onQuery = function (param) {
        this.query(param);
    };

    //执行查询
    Activity.prototype.query = function (param) {

        //    内容区对象
        new Content(this.view.find('[sid=content]'), param);
    };
