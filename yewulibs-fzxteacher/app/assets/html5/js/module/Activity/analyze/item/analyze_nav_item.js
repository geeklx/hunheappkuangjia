/**
 * Created by ming on 2018/2/7.
 */

    var AnalyzeNavItem = function (parentView, data, index) {
        this.parentView = parentView;
        this.data = data;
        this.index = index;

        this.labelView = null;
        this.colorType = data.accuracy;

        this.init();
    };

    //初始化 绘制、注册监听
    AnalyzeNavItem.prototype.init = function(){
        this.labelView = $(AnalyzeNavItem.template).clone(true);
        //赋值
        this.labelView.text(this.index);
        //添加颜色class
        if(this.colorType == 0)
            this.labelView.addClass('cir_gray');
        if(this.colorType == 1)
            this.labelView.addClass('cir_green');
        if(this.colorType == 2)
            this.labelView.addClass('cir_yellow');
        if(this.colorType == 3)
            this.labelView.addClass('cir_red');
        this.parentView.append(this.labelView);

        //注册监听
        this.labelView.on("click",this,this.onSelect);
    };

    //点击选择事件，触发具体过程在父组件处理。
    AnalyzeNavItem.prototype.onSelect = function(evt){
        var self = evt.data;
        //触发事件
        if(self.listenerFunc)
            self.listenerFunc.call(self.listener,self.data.id);
    };

    //控制选择,添加样式
    AnalyzeNavItem.prototype.select = function() {
        if(this.colorType == 0 && !this.labelView.hasClass('back_gray'))
            this.labelView.addClass('back_gray');
        if(this.colorType == 1 && !this.labelView.hasClass('back_green'))
            this.labelView.addClass('back_green');
        if(this.colorType == 2 && !this.labelView.hasClass('back_yellow'))
            this.labelView.addClass('back_yellow');
        if(this.colorType == 3 && !this.labelView.hasClass('back_red'))
            this.labelView.addClass('back_red');
    };

    //控制不选择,去除样式
    AnalyzeNavItem.prototype.unSelect = function(){
        if(this.colorType == 0 && this.labelView.hasClass('back_gray'))
            this.labelView.removeClass('back_gray');
        if(this.colorType == 1 && this.labelView.hasClass('back_green'))
            this.labelView.removeClass('back_green');
        if(this.colorType == 2 && this.labelView.hasClass('back_yellow'))
            this.labelView.removeClass('back_yellow');
        if(this.colorType == 3 && this.labelView.hasClass('back_red'))
            this.labelView.removeClass('back_red');
    };

    //监听
    AnalyzeNavItem.prototype.addListener = function(listener,listenerFunc){
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

    AnalyzeNavItem.template = '<div class="circle"></div>';
