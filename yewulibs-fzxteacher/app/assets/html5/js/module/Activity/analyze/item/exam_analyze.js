/**
 * Created by ming on 2018/2/11.
 */
var ExamAnalyze = function (view, censusData) {
  this.view = view;
  this.censusData = censusData;

  //是否存在要展示的分析数据，如果不存在，则不添加任何内容
  this.hasContent = false;
  this.init();
};


ExamAnalyze.prototype.init = function () {

  //小题信息
  this.examList = this.censusData.examList;
  console.log(this.examList);
  //学科题型信息
  this.examTemplateId = this.censusData.examTemplateId;
  this.examTemplateStyleName = this.censusData.examTemplateStyleName;

  //用于存放的view，如果检查有内容，添加到view中，如果没有，则不添加
  this.parentView = $(this.parentHtml).clone(true);

  //    添加题目
  this.examTypeView = $(this.examTypeHtml).clone(true);
  this.examTypeView.find('[sid=exam_type]').text(this.examTemplateStyleName);
  this.parentView.append(this.examTypeView);
  this.noAnswer = this.examTypeView.find("[cid=noAnswer]");

  //除填空题外的三种简单题型,不需要编号直接对应书写
  if (this.examTemplateId == 1 || this.examTemplateId == 2 || this.examTemplateId == 3) {
    if (this.examList[0].answerList.length != 0)
      this.createAnaView(this.parentView, this.examTemplateId, this.examList[0].answerList[0].optionList);
    this.noAnswer.html("未作答： " + this.examList[0].unAnswer.rate + "%  " + this.examList[0].unAnswer.unAnswer + "人");
  }

  //填空题，单空
  if (this.examTemplateId == 6 && this.examList.length > 0 && this.examList[0].emptyCount == 1) {
    if (this.examList[0].answerList.length != 0)
      this.createAnaView(this.parentView, this.examTemplateId, this.examList[0].answerList[0].optionList);
  }

  //填空题，多空
  if (this.examTemplateId == 6 && this.examList.length > 0 && this.examList[0].emptyCount > 1) {
    var that = this;
    $(this.examList[0].answerList).each(function (key, value) {
      //绘制填空小空空号
      that.createTitle(that.parentView, that.getBlankSeq(value.optionSeq));
      //绘制填空题分析
      that.createAnaView(that.parentView, that.examTemplateId, value.optionList);
    });
  }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //完形填空
  if (this.examTemplateId === 14 && this.examList) {
    let that = this;
    const answerArr = this.examList[0].answerList;
    let clozeHtml = $(this.clozeHtml).clone();
    if (answerArr.length > 0 && answerArr.length <= 5) {
      $(answerArr).each(function (key, value) {
        that.createTitle(clozeHtml, that.getBlankSeq(value.examSeq), value.unAnswerCount);
        that.createAnaView(clozeHtml, that.examTemplateId, value.optionList);
      });
    } else if (answerArr.length > 5 && answerArr.length <= 10) {
      //绘制标签页
      var selectHtml = $(that.clozeSelectHtml).clone();
      var selectItemHtml = $(that.clozeSelectItemHtml).clone();
      var selectItemHtml2 = $(that.clozeSelectItemHtml).clone();
      var clozeOptionItemHtml = $(this.clozeOptionItemHtml).clone();
      var clozeOptionItemHtml2 = $(this.clozeOptionItemHtml).clone();
      var clozeOptionHtml = $(this.clozeOptionHtml).clone();
      let str = "";
      if (answerArr.length === 6) {
        str = "";
      } else {
        str = "-" + answerArr.length;
      }
      selectItemHtml.text("1-5").css("width", "50%");
      selectItemHtml2.text("6" + str).css("width", "50%");
      selectHtml.append(selectItemHtml);
      selectHtml.append(selectItemHtml2);

      clozeHtml.append(selectHtml);
      for (let i = 0; i < 5; i++) {
        that.createTitle(clozeOptionItemHtml, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml, that.examTemplateId, answerArr[i].optionList);
      }
      for (let i = 5; i < answerArr.length; i++) {
        that.createTitle(clozeOptionItemHtml2, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml2, that.examTemplateId, answerArr[i].optionList);
      }

      clozeOptionHtml.append(clozeOptionItemHtml);
      clozeOptionHtml.append(clozeOptionItemHtml2);
      clozeHtml.append(clozeOptionHtml);
      selectItemHtml.off("click").on("click", function (e) {
        selectItemHtml.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml2.off("click").on("click", function (e) {
        selectItemHtml2.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml2.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml.trigger("click");
    } else if (answerArr.length > 10 && answerArr.length <= 15) {
      //绘制标签页
      var selectHtml = $(that.clozeSelectHtml).clone();
      var selectItemHtml = $(that.clozeSelectItemHtml).clone();
      var selectItemHtml2 = $(that.clozeSelectItemHtml).clone();
      var selectItemHtml3 = $(that.clozeSelectItemHtml).clone();
      var clozeOptionItemHtml = $(this.clozeOptionItemHtml).clone();
      var clozeOptionItemHtml2 = $(this.clozeOptionItemHtml).clone();
      var clozeOptionItemHtml3 = $(this.clozeOptionItemHtml).clone();
      var clozeOptionHtml = $(this.clozeOptionHtml).clone();
      let str = "";
      if (answerArr.length === 11) {
        str = "";
      } else {
        str = "-" + answerArr.length;
      }
      selectItemHtml.text("1-5").css("width", "33%");
      selectItemHtml2.text("6-10").css("width", "33%");
      selectItemHtml3.text("11" + str).css("width", "33%");
      selectHtml.append(selectItemHtml);
      selectHtml.append(selectItemHtml2);
      selectHtml.append(selectItemHtml3);

      clozeHtml.append(selectHtml);
      for (let i = 0; i < 5; i++) {
        that.createTitle(clozeOptionItemHtml, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml, that.examTemplateId, answerArr[i].optionList);
      }
      for (let i = 5; i < 10; i++) {
        that.createTitle(clozeOptionItemHtml2, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml2, that.examTemplateId, answerArr[i].optionList);
      }
      for (let i = 10; i < answerArr.length; i++) {
        that.createTitle(clozeOptionItemHtml3, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml3, that.examTemplateId, answerArr[i].optionList);
      }

      clozeOptionHtml.append(clozeOptionItemHtml);
      clozeOptionHtml.append(clozeOptionItemHtml2);
      clozeOptionHtml.append(clozeOptionItemHtml3);

      clozeHtml.append(clozeOptionHtml);
      selectItemHtml.off("click").on("click", function (e) {
        selectItemHtml.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml2.off("click").on("click", function (e) {
        selectItemHtml2.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml2.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml3.off("click").on("click", function (e) {
        selectItemHtml3.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml3.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml.trigger("click");
    } else if (answerArr.length > 15 && answerArr.length <= 20) {
      //绘制标签页
      var selectHtml = $(that.clozeSelectHtml).clone();
      var selectItemHtml = $(that.clozeSelectItemHtml).clone();
      var selectItemHtml2 = $(that.clozeSelectItemHtml).clone();
      var selectItemHtml3 = $(that.clozeSelectItemHtml).clone();
      var selectItemHtml4 = $(that.clozeSelectItemHtml).clone();
      var clozeOptionItemHtml = $(this.clozeOptionItemHtml).clone();
      var clozeOptionItemHtml2 = $(this.clozeOptionItemHtml).clone();
      var clozeOptionItemHtml3 = $(this.clozeOptionItemHtml).clone();
      var clozeOptionItemHtml4 = $(this.clozeOptionItemHtml).clone();
      var clozeOptionHtml = $(this.clozeOptionHtml).clone();
      let str = "";
      if (answerArr.length === 16) {
        str = "";
      } else {
        str = "-" + answerArr.length;
      }
      selectItemHtml.text("1-5").css("width", "25%");
      selectItemHtml2.text("6-10").css("width", "25%");
      selectItemHtml3.text("11-15").css("width", "25%");
      selectItemHtml4.text("16" + str).css("width", "25%");
      selectHtml.append(selectItemHtml);
      selectHtml.append(selectItemHtml2);
      selectHtml.append(selectItemHtml3);
      selectHtml.append(selectItemHtml4);

      clozeHtml.append(selectHtml);
      for (let i = 0; i < 5; i++) {
        that.createTitle(clozeOptionItemHtml, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml, that.examTemplateId, answerArr[i].optionList);
      }
      for (let i = 5; i < 10; i++) {
        that.createTitle(clozeOptionItemHtml2, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml2, that.examTemplateId, answerArr[i].optionList);
      }
      for (let i = 10; i < 15; i++) {
        that.createTitle(clozeOptionItemHtml3, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml3, that.examTemplateId, answerArr[i].optionList);
      }
      for (let i = 15; i < answerArr.length; i++) {
        that.createTitle(clozeOptionItemHtml4, that.getBlankSeq(answerArr[i].examSeq), answerArr[i].unAnswerCount);
        that.createAnaView(clozeOptionItemHtml4, that.examTemplateId, answerArr[i].optionList);
      }

      clozeOptionHtml.append(clozeOptionItemHtml);
      clozeOptionHtml.append(clozeOptionItemHtml2);
      clozeOptionHtml.append(clozeOptionItemHtml3);
      clozeOptionHtml.append(clozeOptionItemHtml4);
      clozeHtml.append(clozeOptionHtml);
      selectItemHtml.off("click").on("click", function (e) {
        selectItemHtml.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml2.off("click").on("click", function (e) {
        selectItemHtml2.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml2.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml3.off("click").on("click", function (e) {
        selectItemHtml3.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml3.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml4.off("click").on("click", function (e) {
        selectItemHtml4.addClass("cloze-selected").siblings().removeClass("cloze-selected");
        clozeOptionItemHtml4.removeClass("dn").siblings().addClass("dn")
      });
      selectItemHtml.trigger("click");
    } else {
      console.log("错误的选项长度")
    }
    that.parentView.append(clozeHtml)
  }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  var that = this;
  //综合题
  if (this.examTemplateId == 16)
    $(this.examList).each(function (key, value) {
      //选择、判断题小题
      if (value.examTemplateId == 1 || value.examTemplateId == 2 || value.examTemplateId == 3) {
        if (value.answerList.length != 0) {
          //绘制小题题号
          that.createTitle(that.parentView, that.getSmallSeq(value.answerList[0].examSeq));
          //绘制题目分析
          that.createAnaView(that.parentView, value.examTemplateId, value.answerList[0].optionList);
        }


      }
      //    填空题、单空小题
      if (value.examTemplateId == 6 && value.emptyCount == 1) {
        if (value.answerList.length != 0) {
          //绘制小题题号
          that.createTitle(that.parentView, that.getSmallSeq(value.answerList[0].examSeq));
          //绘制题目分析
          that.createAnaView(that.parentView, value.examTemplateId, value.answerList[0].optionList);
        }
      }
      //    填空题多空
      if (value.examTemplateId == 6 && value.emptyCount > 1) {
        $(value.answerList).each(function (k, v) {
          //绘制小题+空号
          that.createTitle(that.parentView, that.getSmallSeq(v.examSeq) + that.getBlankSeq(v.optionSeq));
          //绘制题目分析
          that.createAnaView(that.parentView, value.examTemplateId, v.optionList);
        });
      }
    });

  if (this.hasContent)
    this.view.prepend(this.parentView);
};

//绘制题号
ExamAnalyze.prototype.createTitle = function (parentView, text) {
  var seqView = $(this.examSeqHtml).clone(true);
  seqView.find('[sid=small_seq]').text(text);
  parentView.append(seqView);
};

//绘制内容分发器
ExamAnalyze.prototype.createAnaView = function (parentView, model, optionList) {
  if (model === 1 || model === 2 || model === 3 || model === 6 || model === 14) {
    if (model === 1 || model === 2) {
      this.createSelectView(parentView, optionList);
    }
    if (model === 3) {
      this.createJudgeView(parentView, optionList);
    }
    if (model === 6) {
      this.createBlankView(parentView, optionList);
    }
    if (model === 14) {
      this.createClozeView(parentView, optionList);
    }
    //如果属于这其中一种就需要添加这个界面
    this.hasContent = true;
  }
};

//选择
ExamAnalyze.prototype.createSelectView = function (parentView, optionList) {
  var that = this;
  $(optionList).each(function (key, value) {
    var selectView = $(that.selectExamHtml).clone(true);
    selectView.find('[sid=answer]').text(value.myAnswer);
    selectView.find('[sid=count]').text(value.count + '人');
    selectView.find('[sid=progress]').css('width', Math.floor(value.rate).toFixed(0, "") + "%");
    selectView.find('[sid=progress_text]').text(Math.floor(value.rate).toFixed(0, "") + "%");
    if (value.right) {
      selectView.find('[sid=progress]').addClass('on');
      selectView.find('[sid=answer]').addClass('on');
    }
    parentView.append(selectView);
  });
};

//完形填空小题
ExamAnalyze.prototype.createClozeView = function (parentView, optionList) {
  console.log(optionList);
  var that = this;
  $(optionList).each(function (key, value) {
    var selectView = $(that.selectExamHtml).clone(true);
    selectView.find('[sid=answer]').text(that.getClozeFlag(value));
    selectView.find('[sid=count]').text(value.count + '人');
    selectView.find('[sid=progress]').css('width', Math.floor(value.rate).toFixed(0, "") + "%");
    selectView.find('[sid=progress_text]').text(Math.floor(value.rate).toFixed(0, "") + "%");
    if (value.right) {
      selectView.find('[sid=progress]').addClass('on');
      selectView.find('[sid=answer]').addClass('on');
    }
    parentView.append(selectView);
  });
};
ExamAnalyze.prototype.getClozeFlag = function (value) {
  var arr = ["A", "B", "C", "D"];
  var str = arr[value.clozeSeq - 1];
  return str
}

//判断
ExamAnalyze.prototype.createJudgeView = function (parentView, optionList) {
  var that = this;
  $(optionList).each(function (key, value) {
    var judgeView = $(that.judgeExamHtml).clone(true);
    if (value.myAnswer == '正确')
      value.myAnswer = '√';
    if (value.myAnswer == '错误')
      value.myAnswer = '×';
    judgeView.find('[sid=answer]').text(value.myAnswer);
    judgeView.find('[sid=count]').text(value.count + '人');
    judgeView.find('[sid=progress]').css('width', Math.floor(value.rate).toFixed(0, "") + "%");
    judgeView.find('[sid=progress_text]').text(Math.floor(value.rate).toFixed(0, "") + "%");
    if (value.right) {
      judgeView.find('[sid=progress]').addClass('on');
      judgeView.find('[sid=answer]').addClass('on');
    }

    parentView.append(judgeView);
  });
};

//填空
ExamAnalyze.prototype.createBlankView = function (parentView, optionList) {
  var that = this;
  $(optionList).each(function (key, value) {
    var blankView = $(that.blankExamHtml).clone(true);
    if (value.myAnswer == null || value.myAnswer == undefined || value.myAnswer.trim() == "") {
      if (parentView.find("[cid=lou]").find('[sid=answer]').length <= 0) {
        parentView.find("[cid=noAnswer]").text("漏填 " + value.count + '人');
        parentView.find("[cid=noAnswer]").css("color", "#FF6D4A");
      } else {
        parentView.find("[cid=lou]").find('[sid=answer]').text("漏填 " + value.count + '人');
      }
    } else {
      blankView.find('[sid=answer]').text(value.myAnswer);
      blankView.find('[sid=count]').text(value.count + '人');
    }

    if (value.right)
      blankView.find('[sid=answer]').css('color', '#68C426');
    parentView.append(blankView);
  });
};


ExamAnalyze.prototype.getSmallSeq = function (num) {
  return '(' + num + ')';
};

ExamAnalyze.prototype.getBlankSeq = function (num) {
  return '[' + num + ']';
};

ExamAnalyze.prototype.parentHtml = '<div style="clear:both;" class="type_item" sid="type_parent"></div>';

ExamAnalyze.prototype.examTypeHtml = '<div class="type_title" style="width: 93%"><span sid="exam_type"></span><span style="float: right; display: inline-block" cid="noAnswer"></span></div>';
ExamAnalyze.prototype.examSeqHtml = '<div style="clear:both;height: 30px;margin-left:20px;background-color: #FFFFFF;border-bottom: 1px #EDEDED solid" cid="lou"> ' +
  '<div sid="small_seq" style="float: left;">⑴</div>' +
  '<div style="color: #FF6D4A;;letter-spacing: -0.34px;float: right;" sid="answer"></div> ' +
  '</div>';

ExamAnalyze.prototype.selectExamHtml = '<div cid="seq_item" style="padding-left: 10px;">' +
  '<div style="clear:both;" class="type_item_row"> ' +
  '<div class="quest_number" sid="answer">A</div> ' +
  '<div class="number_people" sid="count"></div> ' +
  '<div class="quest_progress"> ' +
  '<div class="up_pro" sid="progress"></div> ' +
  '<div class="pro_text" sid="progress_text"></div></div> ' +
  '</div>' +
  '</div>';

ExamAnalyze.prototype.judgeExamHtml = '<div cid="seq_item" style="padding-left: 10px;">' +
  '<div style="clear:both;" class="type_item_row"> ' +
  '<div class="quest_number" sid="answer">A</div> ' +
  '<div class="number_people" sid="count"></div> ' +
  '<div class="quest_progress"> ' +
  '<div class="up_pro" sid="progress"></div> ' +
  '<div class="pro_text" sid="progress_text"></div></div> ' +
  '</div>' +
  '</div>';

ExamAnalyze.prototype.blankExamHtml = '<div cid="seq_item" style="padding-left: 10px; line-height: 30px;">' +
  '<div style="clear:both;"> ' +
  '<div style="margin-left:20px;color: #FF6D4A;;letter-spacing: -0.34px;float: left;" sid="answer"></div> ' +
  '<div style="float:right;font-size: 12px;color: #808FA3;letter-spacing: -0.34px;background: #EDEDED;text-align: right;margin-right: 10px;margin-top:  5px;line-height:  20px;" sid="count"></div> ' +
  '</div>' +
  '</div>';

ExamAnalyze.prototype.clozeSelectHtml = '<div style="display: flex;justify-content: space-between;height: 26px;width: 100%">' +
  '' +
  '</div>';

ExamAnalyze.prototype.clozeSelectItemHtml = '<div style="line-height: 26px;height: 26px;text-align: center;box-sizing: border-box;background: #F4F6F9; ">' +
  '' +
  '</div>';
ExamAnalyze.prototype.clozeOptionItemHtml = '<div>' +
  '' +
  '</div>';
ExamAnalyze.prototype.clozeOptionHtml = '<div>' +
  '' +
  '</div>'
ExamAnalyze.prototype.clozeHtml = '<div>' +
  '' +
  '</div>'