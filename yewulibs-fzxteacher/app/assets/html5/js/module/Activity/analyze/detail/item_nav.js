/**
 * Created by ming on 2018/3/15.
 */


//model=1是显示大题对错，model=0或者不传值代表大题不显示对错
var ItemNav = function (view, datas, model) {
    this.view = view;
    this.datas = datas;
    this.model = model;
    this.count = 1;
    this.init();
};

ItemNav.prototype.init = function () {
    var that = this;
    $(this.datas).each(function (key, value) {
        //题目总体
        if(value.fullScore <=0)
            return;
        //if (value.isAnswer == 1) {//验证是否答题
            if ((value.templateId == 6 && value.emptyCount > 1) || (value.templateId == 16 && value.examList.length > 0))
                that.createLine(value.examSeq, null, null, value.templateStyleName, value.isRight, false, value);
            else
                that.createLine(value.examSeq, null, null, value.templateStyleName, value.isRight, true, value);

            //填空题多空
            if (value.templateId == 6 && value.emptyCount > 1)
                $(value.examOptionList).each(function (k, v) {
                    that.createLine(null, null, v.seq, null, v.isRight, true, value);
                });
        //}

        if (value.templateId == 16) {
            $(value.examList).each(function (k, v) {
                //if (v.isAnswer == 1) {//验证是否答题
                    //如果不是填空题
                    if (v.templateId != 6)
                        that.createLine(null, v.examSeq, null, null, v.isRight, true, v);
                    //    如果小题为填空题，但只有一个空,同上处理
                    if (v.templateId == 6 && v.emptyCount == 1)
                        that.createLine(null, v.examSeq, null, null, v.isRight, true, v);
                    //    如果为填空题，多于一个空
                    if (v.templateId == 6 && v.emptyCount > 1) {
                        $(v.examOptionList).each(function (k1, v1) {
                            that.createLine(null, v.examSeq, v1.seq, null, v1.isRight, true, v);
                        });
                    }
                //}
            });
        }
    });
};


ItemNav.prototype.createLine = function (bigSeq, smallSeq, blankSeq, templateStyleName, isRight, flag, value) {
    var phtml = $(this.plate).clone(true);
    var seqNameView = phtml.find('[sid=seq_name]');
    phtml.attr("cid=" + value.id);
    var blankView = phtml.find('[sid=blank]');
    var isRightView = phtml.find('[sid=is_right]');


    //大题（6种）
    if (bigSeq != null && smallSeq == null && blankSeq == null) {
        seqNameView.text(bigSeq + '. ' + templateStyleName);
    }

    //独立填空题小空
    if (bigSeq == null && smallSeq == null && blankSeq != null) {
        blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;");
        seqNameView.text(this.getBlankSeq(blankSeq));
    }

    //综合题独立小题，不包含填空小空
    if (bigSeq == null && smallSeq != null && blankSeq == null) {
        blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;");
        seqNameView.text(this.getSmallSeq(smallSeq));
    }

    //综合题，独立小题，小空
    if (bigSeq == null && smallSeq != null && blankSeq != null) {
        if (blankSeq === 1) {
            blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;");
            seqNameView.text(this.getSmallSeq(smallSeq) + this.getBlankSeq(blankSeq));
        }
        else {
            blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            seqNameView.text(this.getBlankSeq(blankSeq));
        }
    }

    if (!flag && this.model != 1) {
        //   不显示大题的正确情况
    }
    this.view.append(phtml);
    var sliderView = $(phtml).find("[cid=slider_l]");
    var score = $(phtml).find("[cid=score]");
    score.attr("sid", value.id);
    var scroeVal = value.fullScore == -1 ? 0 : value.fullScore;
    var d = {
        sliderId: "slide" + this.count++,
        id: value.id,
        mountId: sliderView,
        range: [0, scroeVal],
        inputMaxLength: 4,
        title: "",
        score: score,
        value:value.score,
        passValueName: "passValueName2",
        initialValue: 0,
        isCorrect:value.isCorrect,
        callback: function () {
            console.log("调用1")
        }
    }
    if (value.templateId != 16) {
        if (value.isAnswer == 1)
            this.slider = new Slider(d, this);
        else
            score.hide();
    } else {
        phtml.find("input").remove();
    }
};


ItemNav.prototype.getSmallSeq = function (num) {
    return '(' + num + ')';
};

ItemNav.prototype.getBlankSeq = function (num) {
    return '<' + num + '>';
};


ItemNav.prototype.plate = '<div class="item" style="height:auto;" cid="navItem" >' +
    '<span sid="blank"></span>' +
    '<span sid="seq_name"></span> ' +
    '<div class="f_right " sid="is_right" ><input class="" cid="score" style="text-align: center;height: 20px;width: 30px;border: 2px solid #ccc ;" ></div> ' +
    '<div cid="slider_l"></div>' +
    '</div>';
