/**
 * Created by ming on 2018/2/9.
 */
    var StudentLabelOne = function (parentView, data, flag) {
        this.parentView = parentView;
        this.data = data;
        this.flag = flag;

        this.labelView = null;
        this.init();
    };


    StudentLabelOne.prototype.init = function () {
        this.labelView = $(this.phtml).clone(true);
        //头像和姓名
        if (this.data.photo == null || this.data.photo == undefined)
            this.labelView.find('[sid=image]').attr('src', 'img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg');
        else
            this.labelView.find('[sid=image]').attr('src', this.data.photo);
        this.controlName(this.flag);
        this.labelView.on('click', this, function (evt) {
            var self = evt.data;
            if (self.listenerFunc)
                self.listenerFunc.call(self.listener, self.data);
        });
        this.parentView.append(this.labelView);
    };

    StudentLabelOne.prototype.controlName = function (flag) {
        if (flag)
            this.labelView.find('[sid=name]').text(this.data.userStudentName);
        else
            this.labelView.find('[sid=name]').text('***');
    };

    StudentLabelOne.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    }

    StudentLabelOne.prototype.phtml = '<div class="icon_list" style="display:inline-block;"> ' +
        '<div class="sysIco img_sys_ico" style="margin-left: auto;margin-right: auto;float: none;"> ' +
        '<img sid="image"> ' +
        '</div> ' +
        '<div style="margin: 5px; text-align: center; font-size: 12px; color: rgb(128, 143, 163); letter-spacing: 0px; height: 10px;" class="name" sid="name"></div> ' +
        '</div>';
