/**
 * Created by wuhai on 2018/2/11.
 */
    var StudentListItem = function (parentView, data, index,self) {
        // this.conView = $('.container');
        this.parentView = parentView;
        this.data = data;
        this.index = index;
        this.parent = self;
        this.showAnswerFlag = false;
        this.init();
    };
    StudentListItem.template = '<div class="exam-answer-item">' +
        '<div class="exam-answer-item-top clearfix" sid="top">' +
        '<div class="l e-a-i-type" sid="type"></div>' +
        '<div class="l e-a-i-name" sid="name"></div><span cid="dScore" class="txts"></span>' +
        '<div class="r remove_icon" sid="remove_icon"></div>' +
        '<div class="type_title_look" sid="look_icon" style="margin-right: 20px;"></div>' +
        '</div>' +
        '<div class="exam-answer-item-bot" sid="bot"></div>' +
        '</div>';

    StudentListItem.prototype.init = function () {
        this.view = $(StudentListItem.template);
        // this.view.find('[sid=look_icon]').on('click', this, this.showAnswer);
        var lookView = this.view.find('[sid=look_icon]');
        var nameView = this.view.find('[sid=name]');
        this.view.find('[sid=remove_icon]').on('click', this, this.removeStudent);
        if (this.data.correctType === 2) {
            this.view.find('[sid=type]').addClass('error').text('典型错误');
        } else {
            this.view.find('[sid=type]').addClass('right').text('优秀解答');
        }
        this.username = this.view.find("[cid=dScore]");

        ApiActivity.getTeachInfo({}, this, function (flag, data) {
            if (flag) {
                if (data != null && data != undefined) {
                    var teacherInfo = data.data;
                    var that = this;
                    $(teacherInfo.confInfo).each(function (key, value) {
                        if (value.code === 'GOOD_ANSWER') {
                            nameView.text("***");
                            if (that.data.isCorrect === 1) {
                                if (value.value === 1) {
                                    if (that.data.score != undefined && that.data.score >= 0) {
                                        that.username.text(that.data.score + '分');
                                    } else {
                                        that.username.hide();
                                    }
                                    lookView.removeClass('shut')
                                } else {
                                    if (that.data.score != undefined && that.data.score >= 0) {
                                        that.username.text(that.data.score + '分');
                                    } else {
                                        that.username.hide();
                                    }
                                    lookView.addClass('shut')
                                }
                            }else{
                                that.username.hide();
                            }
                        }
                    });
                }
                else
                    alert("教师信息数据为空");
            }
            else
                alert(data.msg);
        });
        // var examAnswerObj = new ExamAnswerPreview(d, 10);
        this.examAnswerObj = new ExamAnswerPreview(this.data, this.index + '.');
        this.view.find('[sid=bot]').empty().append(this.examAnswerObj.view);
        this.parentView.append(this.view);
        var that = this;
        var open1 = false;
        lookView.on('click', this, function () {
            open1 = !open1;
            that.updateNameStatus(nameView, open1, that.data.userStudentName);
            that.updateLookStatus(lookView, open1);
        });
    };

    //改动
    // StudentListItem.prototype.updateNameStatus = function (view, isOpen, userName) {
    //     // if(this.data.score != -1){
    //     //   if (isOpen)
    //     //     view.text(userName + '(' + this.data.score + '分)');
    //     //   else
    //     //     view.text("***"+'(' + this.data.score + '分)');
    //     // }else{
    //     //   if (isOpen)
    //     //     view.text(userName);
    //     //   else
    //     //     view.text("***");
    //     // }
    //   if (isOpen) {
    //     if (this.data.score != undefined && this.data.score > 0) {
    //       view.text(this.data.userStudentName);
    //     }
    //     view.text(this.data.userStudentName);
    //
    //   } else {
    //     if (this.data.score != undefined && this.data.score > 0) {
    //       view.text("***");
    //     }
    //     view.text("***");
    //   }
    //
    // };

    //吴改动
    StudentListItem.prototype.updateNameStatus = function (view, isOpen1, userName) {
      if (isOpen1)
        view.text(userName);
      else
        view.text("***");
    };

    StudentListItem.prototype.updateLookStatus = function (view, isOpen1) {
        if (isOpen1)
            view.removeClass('shut')
        else
            view.addClass('shut');
    };
    StudentListItem.prototype.showAnswer = function (e) {
        var self = e.data;
        if (self.showAnswerFlag) {
            self.examAnswerObj.hideAnswer();
        } else {
            self.examAnswerObj.showAnswer();
        }
        self.view.find('[sid=look_icon]').toggleClass('active');
        self.showAnswerFlag = !self.showAnswerFlag;
    }

    // var Representative = require('../representative/representative.js');

    StudentListItem.prototype.removeStudent = function (e) {
        var self = e.data;
        console.log(self.data);
        var lessonTaskId = self.data.lessonTaskId;
        console.log(self);
        var param = {};
        param.id = self.data.id;
        param.correctType = 3;
        ApiActivity.updateTypicalCase(param, self, self.updateSList);
        // Representative.(lessonTaskId);
    }
    StudentListItem.prototype.updateSList=function (flag, data) {
        if (flag) {
            alert( "操作成功");
            // Representative.init();
            //  ActivityDetail.prototype.onRefresh(e);
            // window.location.reload();
            this.parent.parent.getExamList();
        }
        else
            alert( data.msg);
    }
