/**
 * 〈一句话功能简述〉
 * 〈功能详细描述〉
 * @param  [参数1]   [参数1说明]
 * @param  [参数2]   [参数2说明]
 * @return [返回类型说明]
 * @exception/throws [违例类型] [违例说明]
 * @see          [类、类#方法、类#成员]
 * @deprecated
 */

    var Representative = function (lessonTaskId) {
        this.view = $('[cid=container]');
        this.lessonTaskId = lessonTaskId;
        this.initView();
    };
    Representative.prototype.initView = function () {
        this.view.load('html/activity/activity_detail/representative/representative.html', this.init.bind(this))
    };

    Representative.prototype.init = function () {
        //初始化页面组件及结构
        this.repre_right = this.view.find("[cid=repre_right]");
        this.exam_view=this.view.find("[sid=datum_left]");
        var repre_right_top = $(this.repre_right).offset().top;
        var repre_right_left = $(this.repre_right).offset().left;
        var heig = $(document).height() - repre_right_top;
        this.repre_right.height(heig);
        //console.log(heig);
        //处理页面数据及绘制内容
        //console.log(repre_right_top + " - "+repre_right_left);
        this.getExamList();
    };
    Representative.prototype.getExamList = function () {
        this.exam_view.empty();
        this.repre_right.empty();
        var param = {};
        param.lessonTaskId = this.lessonTaskId;
        param.type=1;// 讲解典型
        ApiActivity.getExamList(param, this, this.examList);
    };
    Representative.prototype.examList = function (flag,res) {
        if (flag ) {
            var arr = [];
            if(res.data!=undefined&&res.data.length>0){
                for (var i = 0, len = res.data.length; i < len; i++) {
                    arr.push( new ExamListItem(this.exam_view, res.data[i],this.repre_right,this));
                }
                arr[0].view.trigger('click');
            }

        } else {
            alert('获取试题列表失败');
        }
    };
