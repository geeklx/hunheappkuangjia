/**
 * Created by ming on 2018/1/26.
 */
/*
 * 教学活动label*/

    var ActivityLabel = function (parentView, data) {
        this.parentView = parentView;
        this.data = data;

        this.init();
    };


    ActivityLabel.prototype.init = function () {
        //添加label整体
        this.phtml = $(ActivityLabel.labelTemplate).clone(true);
        this.parentView.append(this.phtml);

        //添加进度圈
        this.plateView = this.phtml.find('[cid=plate]');
        this.plateHtml = $(ActivityLabel.plateTemplate).clone(true);
        this.plateView.append(this.plateHtml);

        //  根据data绘制内容
        //题目
        this.phtml.find('[sid=title]').text(this.data.name);
        //班级名称
        this.phtml.find('[sid=className]').text(this.data.baseGradeName + this.data.className);
        //资源+试题数量
        this.phtml.find('[sid=countExam]').text(this.data.countExam);
        this.phtml.find('[sid=countResource]').text(this.data.countRescoure);
        //起止时间
        this.phtml.find('[sid=startTime]').text(this.getFormatTime(this.data.startTime));
        this.phtml.find('[sid=endTime]').text(this.getFormatTime(this.data.endTime));
        //待批改数量
        var correctCount = this.data.countStudentSub - this.data.countTeacherCorrect;
        console.log(correctCount);
        if(correctCount > 0)
            this.phtml.find('[sid=toCorrect]').text(correctCount);
        else
            this.phtml.find('[sid=toCorrect]').hide();
        //进度圈数据
        this.phtml.find('[sid=countStudentSub]').text(this.data.countStudentSub);
        this.phtml.find('[sid=countStudentTotal]').text(this.data.countStudentTotal);
        this.phtml.find('[sid=type]').text(this.data.statusName);
        //绘制进度圈
        var dashoffset = (this.data.countStudentTotal - this.data.countStudentSub) * 300 / this.data.countStudentTotal + "px";
        this.phtml.find('[sid=round]').css("stroke-dashoffset", dashoffset);
        //发送答案绘制初始状态
        if(this.data.answerViewOpen == 1)
            this.phtml.find('[sid=but_answer_ico]').addClass('on');


        //  添加各种操作监听
        //点击标题，进入题目分析
        this.phtml.find('[sid=title]').on('click', {'obj': this, 'code': 'analyze'}, this.listTiggerEvent);
        //点击删除，弹窗确认删除
        this.phtml.find('[sid=but_remove]').on('click',this,this.deleteActivity);
        //点击讲解，进入题目分析
        this.phtml.find('[sid=but_explain]').on('click', {'obj': this, 'code': 'analyze'}, this.listTiggerEvent);
        //点击答案进行发送答案
        this.phtml.find('[sid=but_answer]').on('click',this,this.sendAnswer);
        //点击批改，进入批改
        this.phtml.find('[sid=but_correct]').on('click', {'obj': this, 'code': 'correct'}, this.listTiggerEvent);
        //点击进度圈进入成绩分析
        this.phtml.find('[cid=plate]').on('click', {'obj': this, 'code': 'transcript'}, this.listTiggerEvent);
        //点击预览，弹出预览弹框
        this.phtml.find('[sid=but_preview]').on('click',this,this.scanActFun);
    };

    ActivityLabel.prototype.scanActFun = function (e) {
      var self = e.data;
      new PreviewActivityBox(self.data.name,{'lessonId': self.data.lessonId, 'libId': self.data.lessonLibId});
    };
    
    //删除活动
    ActivityLabel.prototype.deleteActivity = function (evt) {
        var self = evt.data;
        Delbox.show('删除后学生端试题或资源任务将自动撤回，确认删除吗？',this, function () {
            var param = {};
            param.id = self.data.id;
            ApiActivity.removeActivity(param,this,function (flag, data) {
                console.log(param);
                Delbox.hide();
                if (flag) {
                    alert("删除活动成功");
                    self.phtml.remove();
                }
                else
                    alert(data.msg);
            });

        });
    };

    //发送答案
    ActivityLabel.prototype.sendAnswer= function (evt) {
        var self = evt.data;
        if(self.data.answerViewOpen == 1)
            Information.show(3,"您已经发送过答案");
        else {
            var param = {};
            param.id = self.data.id;
            ApiActivity.sendAnswer(param, this, function(flag,data){
                if (flag) {
                    Information.show(1,"已成功发送答案");
                    self.data.answerViewOpen = 1;
                    self.phtml.find('[sid=but_answer_ico]').addClass('on');
                }
                else
                    alert(data.msg);
            });
        }
    };

    //打开活动详情
    ActivityLabel.prototype.listTiggerEvent = function (evt) {
        var self = evt.data.obj;
        var code = evt.data.code;
        try {
            new ActivityDetail(0,null,self.data, code, null);
        } catch (e) {
            console.log(e);
        }
    };

    ActivityLabel.labelTemplate =
        '<div class="list" cid="list">' +
        '    <div class="row">' +
        '    <div class="row_t">' +
        '    <div class="row_text_ico"></div>' +
        '    <div class="row_text font_way" style="font-size:14px;" sid="title">有理数方程，这个地方可以直接通过来</div>' +
        '</div>' +
        '<div class="row_t">' +
        '    <div class="row_text font_way" sid="className">三年级二班</div>' +
        '    </div>' +
        '    <div class="row_t">' +
        '    <div class="row_text font_way" >试题<span sid="countExam">20</span>／资源 <span sid="countResource">2</span></div>' +
        '</div>' +
        '    <div class="row_t">' +
        '    <div class="row_text">发布时间：<span  sid="startTime">2017-12-02 08:00</span></div>' +
        '</div>' +
        '    <div class="row_t">' +
        '    <div class="row_text">截止时间：<span sid="endTime">2017-12-02 08:00</span></div>' +
        '</div>' +
        '<div class="but_group">' +
        '    <div class="but_ico" sid="but_remove" title="删除任务">' +
        '    <div class="but_remove"></div>' +
        '    <div class="but_text">删除</div>' +
        '    </div>' +
        '    <div class="but_ico" sid="but_preview" title="预览内容">' +
        '    <div class="but_preview" ></div>' +
        '   <div class="but_text">预览</div>' +
        '    </div>' +
        '    <div class="but_ico"  sid="but_explain" title="讲解典型">' +
        '    <div class="but_explain"></div>' +
        '    <div class="but_text">讲解</div>' +
        '    </div>' +
        '    <div class="but_ico"  sid="but_answer" title="发送答案">' +
        '    <div class="but_answer" sid="but_answer_ico"></div>' +
        '    <div class="but_text">答案</div>' +
        '    </div>' +
        '    <div class="but_ico" sid="but_correct" title="批改作业">' +
        '   <div class="anno" sid="toCorrect"></div>' +
        '    <div class="but_correct" ></div>' +
        '    <div class="but_text">批改</div>' +
        '    </div>' +
        '    </div>' +
        '<div class="plate" cid="plate"></div>' +
        '    </div>' +
        '    </div>';
    ActivityLabel.plateTemplate = '<svg viewBox="0 0 100 100">' +
        '        <defs>' +
        '        <linearGradient id="orange_yellow" x1="0" y1="1" x2="0" y2="0">' +
        '        <stop offset="0%" stop-color="#FFD012"></stop>' +
        '        <stop offset="100%" stop-color="#FF6D4A"></stop>' +
        '        </linearGradient>' +
        '        </defs>' +
        '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eaeef2"' +
        '    stroke-width="5" fill-opacity="0"></path>' +
        '        <path sid="round" d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94"' +
        '    stroke-linecap="round" stroke="url(#orange_yellow)" stroke-width="6"' +
        '    fill-opacity="0" style="stroke-dasharray: 300px, 300px;stroke-dashoffset: 150px;transition: stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease;"></path>' +
        '        </svg>' +
        '        <div class="mi-box-label">' +
        '        <div class="mi-box-progress">' +
        '        <span class="progress-now" sid="countStudentSub">15</span>' +
        '        <span>/</span>' +
        '        <span class="progress-total" sid="countStudentTotal">28</span>' +
        '        </div>' +
        '        <div class="mi-box-status">' +
        '        <span sid="type">进行中</span>' +
        '        </div>' +
        '        </div>';

    //年月日时分转化器
    ActivityLabel.prototype.getFormatTime = function (str) {
        var addZero = function (num) {
            if(parseInt(num) < 10){
                num = '0'+num;
            }
            return num;
        }
        var oDate = new Date(str);
        var   oYear = oDate.getFullYear();
        var    oMonth = oDate.getMonth()+1;
        var   oDay = oDate.getDate();
        var   oHour = oDate.getHours();
        var   oMin = oDate.getMinutes();
        var    oSen = oDate.getSeconds();
        var    oTime = oYear +'-'+ addZero(oMonth) +'-'+ addZero(oDay) +' '+ addZero(oHour) +':'+ addZero(oMin);//最后拼接时间s
        return oTime;
    };

