/**
 * Created by ming on 2018/1/26.
 */
//状态选择组的label
//status中包含code和name

define(function (require, exports, module) {

    var StatusLabel = function(parentView, statusData){
        this.statusData = statusData;
        this.parentView = parentView;
        //初始化
        this.init();
    };
    module.exports = StatusLabel;

        //初始化 绘制、注册监听
    StatusLabel.prototype.init = function(){
        this.phtml = $(StatusLabel.phtml).clone(true);
        //赋值
        this.phtml.text(this.statusData.name);
        //添加那妞
        this.parentView.append(this.phtml);
        //注册监听
        this.phtml.on("click",this,this.onSelect);
    };

    //点击选择事件，触发具体过程在父组件处理。
    StatusLabel.prototype.onSelect = function(eve){
        var self = eve.data;
        //触发事件
        if(self.listenerFunc)
            self.listenerFunc.call(self.listener,self.statusData.id);
    };

    //控制选择,添加样式
    StatusLabel.prototype.select = function() {
        if(!this.phtml.hasClass('on'))
            this.phtml.addClass('on');
    };

    //控制不选择,去除样式
    StatusLabel.prototype.unSelect = function(){
        if(this.phtml.hasClass('on'))
            this.phtml.removeClass('on');
    };

    //监听
    StatusLabel.prototype.addListener = function(listener,listenerFunc){
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

    StatusLabel.phtml = '<li></li>';
})