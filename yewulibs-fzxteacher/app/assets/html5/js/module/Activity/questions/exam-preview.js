/**
 * Created by zhangxia on 2018/1/31.
 * examInfo:试题信息 index:序号 从1开始
 * 例如：
 * var temp = new ExamPreview(examInfo,1);
 this.view.append(temp.view);
 */
var ExamPreview = function (examInfo, index) {
    this.examInfo = examInfo;
    this.index = index;
    this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    this.serialNum3 = ["正确", "错误"];
    this.initType();
};

ExamPreview.prototype.initType = function () {
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    switch (this.examInfo.examTypeId) {
        case 1:
            this.scanSingleExam(this.view, this.examInfo, this.index);
            break;
        case 2:
            this.scanSingleExam(this.view, this.examInfo, this.index);
            break;
        case 6:
            this.scanComExam(this.view, this.examInfo, this.index);
            break;
        case 3:
            this.scanJudgeExam(this.view, this.examInfo, this.index);
            break;
        case 4:
            this.scanShortExam(this.view, this.examInfo, this.index);
            break;
        case 14:
            this.scanClozeExam(this.view, this.examInfo, this.index);
            break;
        case 16:
            this.scanAllExam(this.view, this.examInfo, this.index);
            break;
        default:
            console.log('未找到对应题型：' + this.examInfo.examTypeId);
    }
//  添加
};

ExamPreview.prototype.initData = function (indexFlag, data) {
    var viewArr = {};
    viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
    viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
    viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
    viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
    viewArr.answerView = $(ExamPreViewTem.amswerTemplate).clone();
    viewArr.analyzeView = $(ExamPreViewTem.analyzeTemplate).clone();

    viewArr.introView.find('[sid=index]').text(indexFlag);
    viewArr.introView.find('[sid=type]').text(data.templateStyleName);
    viewArr.stemView.html(data.examStem);
    viewArr.stemView.find('a').attr("href","javascript:void 0");
    viewArr.stemView.find('a').find('span').css('color','#323C47');
    viewArr.analyzeView.find('[sid=exam-scan-analyse]').html(data.examAnalysis ? data.examAnalysis : '略');
    return viewArr;
};

//预览单选,多选
ExamPreview.prototype.scanSingleExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);

    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);
    var arr = [];
    for (var i = 0; i < data.examOptions.length; i++) {
        var option = $(ExamPreViewTem.optionTemplate).clone();
        option.find('[sid=index]').text(this.serialNum1[i]);
        option.find('[sid=stem]').html(data.examOptions[i].content);
        viewArr.optionConView.append(option);
        if (data.examOptions[i].right) {
            arr.push(this.serialNum1[i]);
        }
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(arr);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览判断
ExamPreview.prototype.scanJudgeExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
    var arr = [];
    for (var i = 0; i < data.examOptions.length; i++) {
        if (data.examOptions[i].right) {
            arr.push(this.serialNum3[i]);
        }
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(arr);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览填空
ExamPreview.prototype.scanComExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览简答
ExamPreview.prototype.scanShortExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览完形填空
ExamPreview.prototype.scanClozeExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.optionConView = $(ExamPreViewTem.optionAllConTemp).clone();
    parentView.append(viewArr.optionConView);
    console.log(data.examOptions);
    if (data && data.examOptions){
        for (var i = 0; i < data.examOptions.length; i++) {
            var clozeOption = $(ExamPreViewTem.clozeAnswerTemplate).clone();
            clozeOption.find('[sid=flag]').text("（" + (i + 1) + "）");
            for (var j = 0; j < data.examOptions[i].list.length; j++) {
                var clozeOptionItem = $(ExamPreViewTem.clozeOptionItemTemplate).clone();
                clozeOptionItem.find('[sid=index]').text(this.serialNum1[j] + ".");
                clozeOptionItem.find(".scan-cloze-content").text(data.examOptions[i].list[j].content);
                clozeOption.find(".scan-cloze-option").append(clozeOptionItem)
            }
            viewArr.optionConView.append(clozeOption);
        }
    } else {
        console.log("试题无选项")
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(data.examAnswer) ? trimStr(data.examAnswer) : '略');
    parentView.append(viewArr.pointView);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览综合
ExamPreview.prototype.scanAllExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.optionConView = $(ExamPreViewTem.optionAllConTemp).clone();
    parentView.append(viewArr.optionConView);

    for (var i = 0; i < data.examBases.length; i++) {
        switch (data.examBases[i].examTypeId) {
            case 1:
                this.scanSingleExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 2:
                this.scanSingleExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 6:
                this.scanComExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 3:
                this.scanJudgeExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 4:
                this.scanShortExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            default:
                console.log('未找到对应的小题型：' + data.examTypeId);
        }
    }
};

//显示答案和解析
ExamPreview.prototype.showAnswer = function () {
    this.view.find('.exam-scan-h').show();
};

ExamPreview.prototype.hideAnswer = function () {
    this.view.find('.exam-scan-h').hide();
};