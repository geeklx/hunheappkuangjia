// /**
//  * Created by zhangxia on 2018/2/11.
//  */
//
// var ExamAnswerPreview = function (data, index) {
//     this.data = data;
//     this.index = index;
//     this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
//     this.serialNum3 = ["正确", "错误"];
//     this.initType();
// };
//
// ExamAnswerPreview.prototype.initType = function () {
//     this.view = $(ExamPreViewTem.boxTemplate).clone();
//     var examText = JSON.parse(this.data.examText);
//     switch (examText.examTypeId) {
//         case 1:
//             this.scanSingleExam(this.view, this.data, this.index);
//             break;
//         case 2:
//             this.scanSingleExam(this.view, this.data, this.index);
//             break;
//         case 6:
//             this.scanComExam(this.view, this.data, this.index);
//             break;
//         case 3:
//             this.scanJudgeExam(this.view, this.data, this.index);
//             break;
//         case 4:
//             this.scanShortExam(this.view, this.data, this.index);
//             break;
//         case 16:
//             this.scanAllExam(this.view, this.data, this.index);
//             break;
//         default:
//             console.log('未找到对应题型：' + this.data.examTypeId);
//     }
//     this.view.find('.exam-scan-h').addClass('dn');
// };
//
// ExamAnswerPreview.prototype.initData = function (indexFlag, data, type,score) {
//     var viewArr = {};
//     viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
//     viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
//     viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
//     viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
//     viewArr.answerView = $(ExamPreViewTem.amswerTemplate).clone();
//     viewArr.analyzeView = $(ExamPreViewTem.analyzeTemplate).clone();
//     viewArr.shortAnswerView = $(ExamPreViewTem.amswerTemplate1).clone();
//
//     if (indexFlag) {
//         viewArr.introView.find('[sid=index]').text(indexFlag);
//     }
//     if (type) {
//         viewArr.introView.find('[sid=type]').text(this.stage(data.examTypeId));
//     } else {
//         viewArr.introView.find('[sid=type]').text(data.templateStyleName);
//     }
//     if (score && score > 0) {
//
//         viewArr.introView.find('[cid=exam_score]').show();
//     }
//     else {
//         viewArr.introView.find('[cid=exam_score]').hide();
//     }
//     if (data.examTypeId == 6) {
//         var tempStem = $(data.examStem);
//         for (var i = 0; i < data.examOptions.length; i++) {
//             $(tempStem.find('.cus-com')[i]).val('');
//             if (data.examOptions[i].isRight) {
//                 $(tempStem.find('.cus-com')[i]).addClass('false-answer');
//             } else {
//                 $(tempStem.find('.cus-com')[i]).addClass('true-answer');
//             }
//         }
//         viewArr.stemView.append(tempStem);
//     } else {
//         viewArr.stemView.html(data.examStem);
//     }
//     viewArr.analyzeView.find('[sid=exam-scan-analyse]').html(data.examAnalysis ? data.examAnalysis : '略');
//     return viewArr;
// };
//
// //预览单选,多选
// ExamAnswerPreview.prototype.scanSingleExam = function (parentView, data, indexFlag, type) {
//     var examInfo = JSON.parse(data.examText);
//     var viewArr = this.initData(indexFlag, examInfo, type);
//     parentView.append(viewArr.introView);
//     parentView.append(viewArr.stemView);
//     parentView.append(viewArr.optionConView);
//     for (var i = 0; i < examInfo.examOptions.length; i++) {
//         var option = $(ExamPreViewTem.optionTemplate).clone();
//         option.find('[sid=index]').text(this.serialNum1[i]);
//         option.find('[sid=stem]').html(examInfo.examOptions[i].content);
//         viewArr.optionConView.append(option);
//     }
//     viewArr.answerView.find('[sid=exam-scan-answer]').html(examInfo.examAnswer);
//     parentView.append(viewArr.answerView);
//     parentView.append(viewArr.analyzeView);
//     this.setAnswerView(parentView, data);
// };
//
// //预览判断
// ExamAnswerPreview.prototype.scanJudgeExam = function (parentView, data, indexFlag, type) {
//     var examInfo = JSON.parse(data.examText);
//     var viewArr = this.initData(indexFlag, examInfo, type);
//     parentView.append(viewArr.introView);
//     parentView.append(viewArr.stemView);
//     parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
//     viewArr.answerView.find('[sid=exam-scan-answer]').html(examInfo.examAnswer);
//     parentView.append(viewArr.answerView);
//     parentView.append(viewArr.analyzeView);
//     this.setAnswerView(parentView, data);
// };
//
// //预览填空
// ExamAnswerPreview.prototype.scanComExam = function (parentView, data, indexFlag, type) {
//     var examInfo = JSON.parse(data.examText);
//     var viewArr = this.initData(indexFlag, examInfo, type);
//     parentView.append(viewArr.introView);
//     parentView.append(viewArr.stemView);
//     viewArr.answerView.find('[sid=exam-scan-answer]').html(examInfo.examAnswer);
//     parentView.append(viewArr.answerView);
//     parentView.append(viewArr.analyzeView);
//     this.setComAnswerView(parentView, data);
// };
//
// //预览简答
// ExamAnswerPreview.prototype.scanShortExam = function (parentView, data, indexFlag, type) {
//     var examInfo = JSON.parse(data.examText);
//     var viewArr = this.initData(indexFlag, examInfo, type);
//     parentView.append(viewArr.introView);
//     parentView.append(viewArr.stemView);
//     viewArr.answerView.find('[sid=exam-scan-answer]').html(examInfo.examAnswer);
//     parentView.append(viewArr.shortAnswerView);
//     parentView.append(viewArr.answerView);
//     parentView.append(viewArr.analyzeView);
//     this.setShortAnswerView(parentView, data);
// };
//
// //预览综合
// ExamAnswerPreview.prototype.scanAllExam = function (parentView, data, indexFlag) {
//     var viewArr = this.initShortData(indexFlag, data);
//     parentView.append(viewArr.introView);
//     parentView.append(viewArr.stemView);
//     parentView.append(viewArr.smallExamView);
//     for (var i = 0; i < data.examList.length; i++) {
//         var tempView = $(ExamPreViewTem.boxTemplate).clone();
//         switch (data.examList[i].examTemplateId) {
//             case 1:
//                 this.scanSingleExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
//                 break;
//             case 2:
//                 this.scanSingleExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
//                 break;
//             case 6:
//                 this.scanComExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
//                 break;
//             case 3:
//                 this.scanJudgeExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
//                 break;
//             case 4:
//                 this.scanShortExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
//                 break;
//             default:
//                 console.log('未找到对应的小题型：' + data.examTemplateId);
//         }
//         viewArr.smallExamView.append(tempView);
//     }
// };
//
// //显示答案和解析
// ExamAnswerPreview.prototype.showAnswer = function () {
//     this.view.find('.exam-scan-h').removeClass('dn');
// };
//
// ExamAnswerPreview.prototype.hideAnswer = function () {
//     this.view.find('.exam-scan-h').addClass('dn');
// };
//
// ExamAnswerPreview.prototype.stage = function (num) {
//     switch (num) {
//         case 1:
//             return '单选题';
//             break;
//         case 2:
//             return '多选题';
//             break;
//         case 3:
//             return '判断题';
//             break;
//         case 4:
//             return '简答题';
//             break;
//         case 6:
//             return '填空题';
//             break;
//         default:
//             return '未知';
//             break;
//     }
// };
//
// ExamAnswerPreview.prototype.initShortData = function (indexFlag, data) {
//     var shortExamInfo = JSON.parse(data.examText);
//     var viewArr = {};
//     viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
//     viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
//     viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
//     viewArr.smallExamView = $(ExamPreViewTem.optionAllConTemp).clone();
//     if (indexFlag) {
//         viewArr.introView.find('[sid=index]').text(indexFlag);
//     }
//     viewArr.introView.find('[sid=typeimgUP]').text(shortExamInfo.templateStyleName);
//     viewArr.stemView.html(shortExamInfo.examStem);
//     return viewArr;
// };
//
// //简答题的答案
// ExamAnswerPreview.prototype.setShortAnswerView = function (parentView, data) {
//     var status = this.getRightStatus(data.isRight);
//     if (data.isAnswer) {
//         var srcArr = data.examOptionList[0].myAnswer.split(',');
//         for (var i = 0; i < srcArr.length; i++) {
//             var img = $('<img class="imgUP" lid="'+i+'" src="' + srcArr[i] + '">');
//
//             img.on("click", function () {
//                 //ScanPic.show(html5SendPicToNative$(this).attr('src'));
//                 var d={};
//                 d.seq =data.examSeq + "、" + data.templateStyleName;
//                 d.id = data.examOptionList[0].id;
//                 d.list = $(this).attr("lid");
//                 d.src = $(this).attr("src");
//                 // alert("33  "+$(this).attr("src"));
//                 setImgSrc(d);
//                 //html5SendPicToNative($(this).attr('src'));
//             });
//             parentView.find('[sid=exam-scan-answer-s]').append(img);
//         }
//     } else {
//         parentView.find('[sid=exam-scan-answer-s]').append($('<div class="short-no-answer">您未提交该题答案！</div>'));
//         //status = "false-answer";
//     }
//     parentView.find('[sid=exam-scan-answer-s]').find('[sid=flag]').addClass(status);
// };
//
// //填空题的答案
// ExamAnswerPreview.prototype.setComAnswerView = function (parentView, data) {
//     if (data.examOptionList && data.examOptionList.length > 0) {
//         for (var i = 0; i < data.examOptionList.length; i++) {
//             var status = this.getRightStatus(data.examOptionList[i].isRight);
//             $(parentView.find('[sid=exam-scan-stem]').find('.cus-com')[data.examOptionList[i].seq - 1]).addClass(status).val(data.examOptionList[i].myAnswer);
//         }
//     }
// };
//
// //选择题判断题的答案
// ExamAnswerPreview.prototype.setAnswerView = function (parentView, data) {
//     var status = this.getRightStatus(data.isRight);
//     if (data.examOptionList && data.examOptionList.length > 0) {
//         for (var i = 0; i < data.examOptionList.length; i++) {
//             $(parentView.find('[sid=exam-scan-option]').find('[sid=index]')[data.examOptionList[i].seq - 1]).addClass(status);
//
//         }
//     }
// };
//
// //正确 错误 半对 状态
// ExamAnswerPreview.prototype.getRightStatus = function (flag) {
//     var classArr = ["true-answer", "false-answer", "half-true"];
//     var className = '';
//     switch (flag) {
//         case 1:
//             className = classArr[0];
//             break;
//         case 2:
//             className = classArr[1];
//             break;
//         case 3:
//             className = classArr[2];
//             break;
//         default:
//             className = '';
//     }
//     return className;
// };
/**
 * Created by wjh on 2018/11/2.
 */

var ExamAnswerPreview2 = function (data, index, vid, parent) {
  this.data = data;
  this.index = index                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ;
  this.parent = parent;
  this.vid = vid == undefined ? false : true;//false 或 0 （默认）= 不可编辑  ；true 或 1 = 可编辑 ；
  this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  this.serialNum3 = ["正确", "错误"];
  this.imageStyle = localStorage.getItem("upToken");
  this.initType();
};

ExamAnswerPreview2.prototype.initType = function () {
  this.view = $(ExamPreViewTem.boxTemplate).clone();
  var examText = JSON.parse(this.data.examText);
  switch (examText.examTypeId) {
    case 1:
      this.scanSingleExam(this.view, this.data, this.data.examSeq);
      break;
    case 2:
      this.scanSingleExam(this.view, this.data, this.data.examSeq);
      break;
    case 6:
      this.scanComExam(this.view, this.data, this.data.examSeq);
      break;
    case 3:
      this.scanJudgeExam(this.view, this.data, this.data.examSeq);
      break;
    case 4:
      this.scanShortExam(this.view, this.data, this.data.examSeq);
      break;
    case 14:
      this.scanClozeExam(this.view, this.data, this.data.examSeq);
      break;
    case 16:
      this.scanAllExam(this.view, this.data, this.data.examSeq);
      break;
    default:
      console.log('未找到对应题型：' + this.data.examTypeId);
  }
  this.view.find('.exam-scan-h').addClass('dn');
};

ExamAnswerPreview2.prototype.initData = function (indexFlag, data, type, score) {
  var viewArr = {};
  viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
  viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
  viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
  viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
  viewArr.answerView = $(ExamPreViewTem.amswerTemplate).clone();
  viewArr.analyzeView = $(ExamPreViewTem.analyzeTemplate).clone();
  viewArr.shortAnswerView = $(ExamPreViewTem.amswerTemplate1).clone();

  if (indexFlag) {
    viewArr.introView.find('[sid=index]').text(indexFlag);
  }
  if (type) {
    viewArr.introView.find('[sid=type]').text(this.stage(data.examTypeId));
  } else {
    viewArr.introView.find('[sid=type]').text(data.templateStyleName);
  }
  if (score && score > 0) {

    viewArr.introView.find('[cid=exam_score]').show();
    if (data.examTypeId === 14) {
      viewArr.introView.find("[cid=score_text]").text(score * data.optionNumber);
    } else {
      viewArr.introView.find("[cid=score_text]").text(score);
    }
  } else {
    viewArr.introView.find('[cid=exam_score]').hide();
  }
  // viewArr.introView.find('[cid=score_text]').text(score);

  if (data.examTypeId === 6 || data.examTypeId === 14) {
    var tempStem = $("<div>" + data.examStem + "</div>");
    for (var i = 0; i < data.examOptions.length; i++) {
      $(tempStem.find('.cus-com')[i]).val('');
      if (data.examOptions[i].isRight) {
        $(tempStem.find('.cus-com')[i]).addClass('false-answer');
      } else {
        $(tempStem.find('.cus-com')[i]).addClass('true-answer');
      }
    }
    viewArr.stemView.append(tempStem);
  } else {

    viewArr.stemView.html(data.examStem);
  }
  viewArr.analyzeView.find('[sid=exam-scan-analyse]').html(data.examAnalysis ? data.examAnalysis : '略');
  return viewArr;
};

//预览单选,多选
ExamAnswerPreview2.prototype.scanSingleExam = function (parentView, data, indexFlag, type) {
  var examInfo = JSON.parse(data.examText);
  var viewArr = this.initData(indexFlag, examInfo, type, data.fullScore);
  parentView.append(viewArr.introView);
  parentView.append(viewArr.stemView);
  parentView.append(viewArr.optionConView);
  for (var i = 0; i < examInfo.examOptions.length; i++) {
    var option = $(ExamPreViewTem.optionTemplate).clone();
    option.find('[sid=index]').text(this.serialNum1[i]);
    option.find('[sid=stem]').html(examInfo.examOptions[i].content);
    viewArr.optionConView.append(option);
  }
  viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
  parentView.append(viewArr.answerView);
  parentView.append(viewArr.analyzeView);
  parentView.find('a').attr("href", "javascript:void 0");
  parentView.find('a').find('span').css('color', '#323C47');
  this.setAnswerView(parentView, data);
};

//预览判断
ExamAnswerPreview2.prototype.scanJudgeExam = function (parentView, data, indexFlag, type) {
  var examInfo = JSON.parse(data.examText);
  var viewArr = this.initData(indexFlag, examInfo, type, data.fullScore);
  parentView.append(viewArr.introView);
  parentView.append(viewArr.stemView);
  parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
  viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
  parentView.append(viewArr.answerView);
  parentView.append(viewArr.analyzeView);
  parentView.find('a').attr("href", "javascript:void 0");
  parentView.find('a').find('span').css('color', '#323C47');
  this.setAnswerView(parentView, data);
};

//预览填空
ExamAnswerPreview2.prototype.scanComExam = function (parentView, data, indexFlag, type) {
  var examInfo = JSON.parse(data.examText);
  var viewArr = this.initData(indexFlag, examInfo, type, data.fullScore * data.emptyCount);
  parentView.append(viewArr.introView);
  parentView.append(viewArr.stemView);
  viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
  parentView.append(viewArr.answerView);
  parentView.append(viewArr.analyzeView);
  parentView.find('a').attr("href", "javascript:void 0");
  parentView.find('a').find('span').css('color', '#323C47');
  this.setComAnswerView(parentView, data);
};

//预览简答
ExamAnswerPreview2.prototype.scanShortExam = function (parentView, data, indexFlag, type) {
  var examInfo = JSON.parse(data.examText);
  var viewArr = this.initData(indexFlag, examInfo, type, data.fullScore);
  parentView.append(viewArr.introView);
  parentView.append(viewArr.stemView);
  viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
  parentView.append(viewArr.shortAnswerView);
  parentView.append(viewArr.answerView);
  parentView.append(viewArr.analyzeView);
  parentView.find('a').attr("href", "javascript:void 0");
  parentView.find('a').find('span').css('color', '#323C47');
  this.setShortAnswerView(parentView, data);
};

//预览完形填空
ExamAnswerPreview2.prototype.scanClozeExam = function (parentView, data, indexFlag, type) {
  var examInfo = JSON.parse(data.examText);
  var viewArr = this.initData(indexFlag, examInfo, type, data.fullScore);
  parentView.append(viewArr.introView);
  parentView.append(viewArr.stemView);
  viewArr.optionConView = $(ExamPreViewTem.optionAllConTemp).clone();
  parentView.append(viewArr.optionConView);
  if (data && examInfo.examOptions) {
    for (var i = 0; i < examInfo.examOptions.length; i++) {
      var clozeOption = $(ExamPreViewTem.clozeAnswerTemplate).clone();
      clozeOption.find('[sid=flag]').text("（" + (i + 1) + "）");
      for (var j = 0; j < examInfo.examOptions[i].list.length; j++) {
        var clozeOptionItem = $(ExamPreViewTem.clozeOptionItemTemplate).clone();
        clozeOptionItem.find('[sid=index]').text(this.serialNum1[j] + ".");
        clozeOptionItem.find(".scan-cloze-content").text(examInfo.examOptions[i].list[j].content);
        clozeOption.find(".scan-cloze-option").append(clozeOptionItem)
      }
      viewArr.optionConView.append(clozeOption);
    }
  }
  viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(data.examAnswer) ? trimStr(data.examAnswer) : '略');
  this.setClozeAnswerView(parentView, data);
  parentView.append(viewArr.pointView);
  parentView.append(viewArr.answerView);
  parentView.append(viewArr.analyzeView);
  parentView.find('a').attr("href", "javascript:void 0");
  parentView.find('a').find('span').css('color', '#323C47');

};


//预览综合
ExamAnswerPreview2.prototype.scanAllExam = function (parentView, data, indexFlag) {
  var viewArr = this.initShortData(indexFlag, data);
  parentView.append(viewArr.introView);
  parentView.append(viewArr.stemView);
  parentView.append(viewArr.smallExamView);
  parentView.find('a').attr("href", "javascript:void 0");
  parentView.find('a').find('span').css('color', '#323C47');
  for (var i = 0; i < data.examList.length; i++) {
    var tempView = $(ExamPreViewTem.boxTemplate).clone();
    switch (data.examList[i].templateId) {
      case 1:
        this.scanSingleExam(tempView, data.examList[i], '(' + data.examList[i].examSeq + ')', 1);
        break;
      case 2:
        this.scanSingleExam(tempView, data.examList[i], '(' + data.examList[i].examSeq + ')', 1);
        break;
      case 6:
        this.scanComExam(tempView, data.examList[i], '(' + data.examList[i].examSeq + ')', 1);
        break;
      case 3:
        this.scanJudgeExam(tempView, data.examList[i], '(' + data.examList[i].examSeq + ')', 1);
        break;
      case 4:
        this.scanShortExam(tempView, data.examList[i], '(' + data.examList[i].examSeq + ')', 1);
        break;
      default:
        console.log('未找到对应的小题型：' + data.templateId);
    }
    viewArr.smallExamView.append(tempView);
  }
};

//显示答案和解析
ExamAnswerPreview2.prototype.showAnswer = function () {
  this.view.find('.exam-scan-h').removeClass('dn');
};

ExamAnswerPreview2.prototype.hideAnswer = function () {
  this.view.find('.exam-scan-h').addClass('dn');
};

ExamAnswerPreview2.prototype.stage = function (num) {
  switch (num) {
    case 1:
      return '单选题';
      break;
    case 2:
      return '多选题';
      break;
    case 3:
      return '判断题';
      break;
    case 4:
      return '简答题';
      break;
    case 6:
      return '填空题';
      break;
    default:
      return '未知';
      break;
  }
};

ExamAnswerPreview2.prototype.initShortData = function (indexFlag, data) {
  var shortExamInfo = JSON.parse(data.examText);
  var viewArr = {};
  viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
  viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
  viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
  viewArr.smallExamView = $(ExamPreViewTem.optionAllConTemp).clone();
  if (indexFlag) {
    viewArr.introView.find('[sid=index]').text(indexFlag);
  }
  if (data.fullScore && data.fullScore > 0) {
    viewArr.introView.find('[cid=exam_score]').show();
    if (data.templateId == 6) {
      viewArr.introView.find('[cid=score_text]').text(data.fullScore * data.emptyCount);
    } else {
      viewArr.introView.find('[cid=score_text]').text(data.fullScore);
    }
  } else {
    viewArr.introView.find('[cid=exam_score]').hide();
  }
  viewArr.introView.find('[sid=type]').text(shortExamInfo.templateStyleName);
  viewArr.stemView.html(shortExamInfo.examStem);
  return viewArr;
};

//简答题的答案
// ExamAnswerPreview2.prototype.setShortAnswerView = function (parentView, data) {
//   var status = this.getRightStatus(data.isRight);
//   var xscore = parentView.find("[cid=xscore]");
//   if (data.isCorrect == 1)
//     if (data.score >= 0) {
//       xscore.show();
//       xscore.html(data.score + "分");
//     } else {
//       xscore.hide();
//     }
//   else
//     xscore.hide();
//
//   if (data.isAnswer) {
//     var self = this;
//     if (data.examOptionList[0].seq == 2) {
//       var srcArr1 = data.examOptionList[0].myAnswer.split(';')[0].split(',');
//       var srcArr = data.examOptionList[0].myAnswer.split(';')[1].split(',');
//       for (var i = 0; i < srcArr.length; i++) {
//         var img = $('<img class="imgUP" vid="' + data.id + ' ' + i + '" eid="' + data.id + '" lid="' + i + '"  sid="' + srcArr1[i] + '" src="' + srcArr[i] + '">');
//         img.on("click", function () {
//           if (self.vid == false) {
//             ScanPic.show($(this).attr('sid'));
//           } else {
//             var d = {};
//             d.seq = data.examSeq + "、" + data.templateStyleName;
//             d.id = data.examOptionList[0].id;
//             d.list = $(this).attr("lid");
//             d.src = $(this).attr("sid");
//             d.eid = $(this).attr("eid");
//             d.vid = $(this).attr("vid");
//             PicEdit.show(d, self);
//           }
//         });
//         parentView.find('[sid=exam-scan-answer-s]').append(img);
//       }
//     } else {
//       var srcArr = data.examOptionList[0].myAnswer.split(',');
//       for (var i = 0; i < srcArr.length; i++) {
//         var img = $('<img class="imgUP" eid="' + data.id + '"  lid="' + i + '" sid="' + srcArr[i] + '"  src="' + srcArr[i] + '">');
//         img.on("click", function () {
//           if (self.vid == false) {
//             ScanPic.show($(this).attr('sid'));
//           } else {
//             var d = {};
//             d.seq = data.examSeq + "、" + data.templateStyleName;
//             d.id = data.examOptionList[0].id;
//             d.list = $(this).attr("lid");
//             d.src = $(this).attr("sid");
//             d.eid = $(this).attr("eid");
//             PicEdit.show(d, self);
//           }
//         });
//         parentView.find('[sid=exam-scan-answer-s]').append(img);
//       }
//     }
//   } else {
//     parentView.find('[sid=exam-scan-answer-s]').append($('<div class="short-no-answer">该学生未提交该题答案！</div>'));
//     // status = "false-answer";
//   }
//   parentView.find('[sid=exam-scan-answer-s]').find('[sid=flag]').addClass(status);
// };
ExamAnswerPreview2.prototype.setShortAnswerView = function (parentView, data) {
    var status = this.getRightStatus(data.isRight);
    var xscore = parentView.find("[cid=xscore]");
    if (data.isCorrect == 1)
        if (data.score >= 0) {
            xscore.show();
            xscore.html(data.score + "分");
        } else {
            xscore.hide();
        }
    else
        xscore.hide();
    if (data.isAnswer) {
        var self = this;
        var srcArr = data.examOptionList[0].myAnswer.split(',');
        for (var i = 0; i < srcArr.length; i++) {
            var srcThn = srcArr[i] +"?"+ this.imageStyle;
            console.log(this.imageStyle);
            var img = $('<img class="imgUP" eid="' + data.id + '"   lid="' + i + '" sid="' + srcArr[i] + '" src="' + srcThn + '">');
            img.on("click", function () {
                var d = {};
                d.seq = data.examSeq + "、" + data.templateStyleName;
                d.id = data.examOptionList[0].id;
                d.list = $(this).attr("lid");
                d.src = $(this).attr("sid");
                d.eid = $(this).attr("eid");
                if (self.vid == false) {
                    // alert("非批改");
                    setImgSrc(d, 0);
                 } else {
                    // alert("批改");
                    setImgSrc(d, 1);

                }
            });
            parentView.find('[sid=exam-scan-answer-s]').append(img);
        }

    } else {
        parentView.find('[sid=exam-scan-answer-s]').append($('<div class="short-no-answer">您未提交该题答案！</div>'));
        // status = "false-answer";
    }
    parentView.find('[sid=exam-scan-answer-s]').find('[sid=flag]').addClass(status);
};

//填空题的答案
ExamAnswerPreview2.prototype.setComAnswerView = function (parentView, data) {
  parentView.find("input.cus-com").replaceWith($('<span class="black_filling" contenteditable></span>'));
  if (data.examOptionList && data.examOptionList.length > 0) {
    for (var i = 0; i < data.examOptionList.length; i++) {
      var status = this.getComRightStatus(data.examOptionList[i].isRight);
      $(parentView.find('[sid=exam-scan-stem]').find('.black_filling')[data.examOptionList[i].seq - 1]).removeClass('true-answer false-answer').addClass(status).text(data.examOptionList[i].myAnswer);
    }
  }
};

//选择题判断题的答案
ExamAnswerPreview2.prototype.setAnswerView = function (parentView, data) {
  var status = this.getRightStatus(data.isRight);
  if (data.examOptionList && data.examOptionList.length > 0) {
    for (var i = 0; i < data.examOptionList.length; i++) {
      $(parentView.find('[sid=exam-scan-option]').find('[sid=index]')[data.examOptionList[i].seq - 1]).addClass(status);

    }
  }
};


//完型填空的答案
ExamAnswerPreview2.prototype.setClozeAnswerView = function (parentView, data) {
  parentView.find("input.cus-com").replaceWith($('<span class="black_filling"><span sid="clozeIndex"></span><span sid="clozeDetail"></span></span>'));
  if (data.emptyCount) {
    for (var i = 0; i < data.emptyCount; i++) {
      $(parentView.find('[sid=exam-scan-stem]').find('[sid=clozeIndex]')[i]).text(i + 1);
    }
  }
  if (data.examOptionList && data.examOptionList.length > 0) {
    for (var i = 0; i < data.examOptionList.length; i++) {
      var status = this.getComRightStatus(data.examOptionList[i].isRight);
      $(parentView.find('[sid=exam-scan-stem]').find('.black_filling')[data.examOptionList[i].seq - 1]).find('[sid=clozeDetail]').removeClass('true-answer false-answer').addClass(status).html("&nbsp;" + data.examOptionList[i].myAnswer);
    }
  }
};

//正确 错误 半对 状态
ExamAnswerPreview2.prototype.getRightStatus = function (flag) {
  var classArr = ["true-answer", "false-answer", "half-true"];
  var className = '';
  switch (flag) {
    case 1:
      className = classArr[0];
      break;
    case 2:
      className = classArr[1];
      break;
    case 3:
      className = classArr[2];
      break;
    default:
      className = '';
  }
  return className;
};

//正确 错误  状态 填空
ExamAnswerPreview2.prototype.getComRightStatus = function (flag) {
  var classArr = ["true-answer", "false-answer", "half-true"];
  var className = '';
  switch (flag) {
    case 1:
      className = classArr[0];
      break;
    case 0:
      className = classArr[1];
      break;
    default:
      className = '';
  }
  console.log(className);
  return className;
};

