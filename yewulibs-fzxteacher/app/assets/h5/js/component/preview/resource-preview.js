/**
 * Created by zhangxia on 2018/3/6.
 */
/**
 * Created by zhangxia on 2018/2/27.
 */
var ResItem = function (parentObj, data) {
    this.parentObj = parentObj;
    this.resData = data;
    this.data = JSON.parse(this.resData.resourceText);
    this.collFlag = this.resData.isCollect;
    this.init();
};

ResItem.prototype.init = function () {
    this.view = $(ResItem.resBoxTempalte).clone();
    this.resView = $(ResItem.resTempalte).clone();
    this.collView = this.view.find('[sid=ans-e-a-coll]');
    this.collView.off('click').on('click', this, this.collectFun);
    this.resView.find('[sid=name]').text(this.data.resourceName);
    this.resView.find('[sid=name]').attr('title', this.data.resourceName);
    this.picView = this.resView.find('[sid=pic]');
    this.initType(this.data.resourceType, this.picView);
    this.errorView = this.resView.find('[sid=error]');
    if (this.data.convertStatus == 3) {
        this.errorView.addClass('error')
    } else if (this.data.convertStatus == 1) {
        this.errorView.addClass('ing')
    }
    this.view.find('[sid=res]').append(this.resView);
    this.view.find('[sid=res]').on('click', this.scanRes.bind(this));
    this.updateCollect();
};

ResItem.prototype.initType = function (type, view) {
    switch (type) {
        case 5:
            view.addClass('music');
            break;
        case 4:
            view.addClass('pic');
            break;
        case 3:
            view.addClass('mp4');
            break;
        case 2:
            view.addClass('pdf');
            break;
        case 1:
            view.addClass('word');
            break;
        case 6:
            view.addClass('real-pdf');
            break;
    }
};

ResItem.prototype.collectFun = function (e) {
    var self = e.data;
    if (!self.collFlag) {
        LearningGuideHandler.resCollectAdd({
            'resourceId': self.resData.resourceId,
            'lessonId': lessonId,
            'lessonName': lessonName
        }, self, self.collectCallBack);
    } else {
        LearningGuideHandler.resCollectDelete({'resourceCollectId': self.resData.resourceCollectId}, self, self.collectCallBack);
    }
};

ResItem.prototype.scanRes = function () {
    html5ScanResource(this.resData);
};

ResItem.prototype.collectCallBack = function (flag, data) {
    if (flag) {
        this.collFlag = !this.collFlag;
        this.updateCollect();
        if (this.collFlag) {
            this.resData.resourceCollectId = data.result.data;
            html5SendSystemInfoToNative('收藏成功！');
        } else {
            html5SendSystemInfoToNative('取消收藏成功！');
        }
    } else {
        html5SendSystemInfoToNative(data.msg);
    }
};

ResItem.prototype.updateCollect = function () {
    if (this.collFlag) {
        this.collView.addClass('active');
    } else {
        this.collView.removeClass('active');
    }
};


ResItem.resBoxTempalte = $('<div class="ans-e-exam ans-e-res box-s"><div class="ans-e-a-coll coll-res" sid="ans-e-a-coll" ></div><div class="ans-e-a-r" sid="res"></div></div>');

ResItem.resTempalte = '<div><div class="inner-res clearfix">' +
    '    <span class="back" sid="pic"></span>' +
    '    <span class="name" sid="name"></span>' +
    '    <span class="" sid="error"></span>' +
    '</div></div>';