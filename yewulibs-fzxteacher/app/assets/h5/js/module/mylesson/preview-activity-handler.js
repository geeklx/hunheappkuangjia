var ML_ACT_DATA = 'teach/lesson/lib/info';//活动详情

var PreviewActivityHandler = function () {

};

PreviewActivityHandler.operate = function (data, listener, listenerFun) {
  doAjax({
    url: ML_ACT_DATA,
    type: "post",
    data: data,
    dataType: "json",
    success: function (data) {
      if (data.code === 0) {
        listenerFun.call(listener, true, data.result);
      } else {
        listenerFun.call(listener, false, data);
      }
    },
    error: function (data) {
      listenerFun.call(listener, false, data);
    }
  });
};











