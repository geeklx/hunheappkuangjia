    var Steamed = function (activityData) {
        this.activityData = activityData;
        this.view = $('[cid=container]');
        //页面控制器
        this.pageControl = null;
        //记录页面为优秀或者粗糙
        this.model = 1;
        //记录个人眼睛设置信息
        this.isGoodOpen = true;
        this.isBadOpen = true;

        this.initView();
    };

    Steamed.prototype.initView = function () {
        this.view.load('html/activity/activity_detail/steamed/steamed.html?' + Utils.getTimes(), this.init.bind(this));
    };

    Steamed.prototype.init = function () {
        // 初始化眼睛信息后进行页面初始化
        var param = {};
        ApiActivity.getTeachInfo(param, this, function (flag, data) {
            if (flag) {
                if (data != null && data != undefined) {
                    var teacherInfo = data.data;
                    var that = this;
                    $(teacherInfo.confInfo).each(function (key, value) {
                        if (value.code == 'GOOD_SURFAC')
                            that.isGoodOpen = value.value == 0 ? true : false;
                        if (value.code == 'BAD_SURFACE')
                            that.isBadOpen = value.value == 0 ? true : false;
                    });
                }
                else
                    alert("教师信息数据为空");
            }
            else
                alert( data.msg);

            //初始化页面组件及结构
            var o = this.view.find("[cid=exam_layout]");
            this.setHeight(o);
            var that = this;
            $(window).resize(function(){
                that.setHeight(o);
            });

            //处理页面数据及绘制内容

            //  卷面导航,并默认选择
            this.surfaceNavView = this.view.find('[sid=surface_nav]');
            this.surfaceNav = new SurfaceNav(this.surfaceNavView, 'good');
            //学生信息VIew
            this.studentInfoView = this.view.find('[sid=student_info]');
            //内容view
            this.contentView = this.view.find('[sid=content]');

            //监听
            this.surfaceNav.addListener(this, function (code) {
                this.updateSurface(code);
            });
            //初始化
            this.updateSurface(this.surfaceNav.getValue());
        });
    };

    Steamed.prototype.updateSurface = function (code) {
        //页码和翻页view
        this.pageCountView = this.view.find("[sid=page_count]");
        this.pageTurnView = this.view.find("[sid=page_turn]");
        //  查询数据
        var param = {};
        param.lessonTaskId = this.activityData.id;
        if (code == 'good') {
            param.correctType = 1;
            this.model = 1;
        }
        else {
            param.correctType = 2;
            this.model = 2;
        }
        ApiActivity.studentFaceList(param, this, function (flag, data) {
            if (flag) {
                if (data != null && data != undefined) {
                    var datas = data.data;
                    var initPage = 0;
                    if (datas.length > 0)
                        initPage = 1;
                    if (this.pageControl == null) {
                        this.pageControl = new PageControl(this.pageCountView, this.pageTurnView, datas, initPage);
                        //    添加监听
                        this.pageControl.addListener(this, this.changePage);
                    }
                    else
                        this.pageControl.refresh(datas, initPage);

                    //初始化
                    if (datas.length == 0)
                        this.changePage(0);
                    else
                        this.changePage(datas[0]);
                }
                else
                    alert( "数据无效");
            }
            else
                alert( data.msg);
        });
    };
    //改变content内容，data == 0则是空页
    Steamed.prototype.changePage = function (data) {
        //清空信息区内容和内容去内容
        this.studentInfoView.empty();
        this.contentView.empty();
        //如果之前是空页需要显示信息区，并且清空空页dom
        this.studentInfoView.show();
        this.contentView.remove('[sid=blank]');

        if (data == 0) {
            //空页
            this.studentInfoView.hide();
            this.contentView.append($(this.blankTemplate).clone(true));
            return;
        }
        //   绘制学生信息内容
        var phtml = $(this.infoTemplate).clone(true);
        var nameView = phtml.find('[sid=name]');
        var scoreView = phtml.find('[sid=score]');
        var lookView = phtml.find('[sid=look]');
        var deleteView = phtml.find('[sid=remove]');

        var open = false;
        if ((this.model == 1 && this.isGoodOpen) || (this.model == 2 && this.isBadOpen))
            open = true;
        //姓名
        this.updateNameStatus(nameView, open, data.userStudentName);
        //眼睛
        this.updateLookStatus(lookView, open);
        //分数
        scoreView.text(data.scoreTotal);
        //事件
        var that = this;
        lookView.on('click', this, function () {
            open = !open;
            that.updateNameStatus(nameView, open, data.userStudentName);
            that.updateLookStatus(lookView, open);
        });
        deleteView.on('click',this,function () {
            console.log("111");
            that.pageControl.deletePage();
        });
        this.studentInfoView.append(phtml);
        //绘制题目内容 TODO
        var that = this;
        that.examAnswerArr = [];
        $.each(data.answerExamList,function (index,item) {
           var item = new ExamAnswerPreview(item,item.examSeq + '.');
          that.contentView.append(item.view);
          that.examAnswerArr.push(item);
        });
    };

    Steamed.prototype.updateNameStatus = function (view, isOpen, userName) {
        if (isOpen)
            view.text(userName);
        else
            view.text("***");
    };

    Steamed.prototype.updateLookStatus = function (view, isOpen) {
        if (isOpen)
            view.removeClass('shut')
        else
            view.addClass('shut');
    };

    Steamed.prototype.setHeight = function (obj) {
        var repre_right_top = $(obj).offset().top;
        var heig = $(document).height() - repre_right_top;
        obj.height(heig);
    }

    Steamed.prototype.infoTemplate = '  <div style="float:left;">姓名：<span sid="name">李雷</span> （<span sid="score">12</span>分）</div> ' +
        '<div class="type_item" style="float: right;margin-top: 5px;margin-right: 20px;"> ' +
        '<div class="but_remove" style="float: right;margin-top: 5px;" sid="remove"></div> ' +
        '<div class="type_title_look" style="float: right;"sid="look"></div> ' +
        '</div>';

    Steamed.prototype.blankTemplate = '<div class="mi-blank-container" sid="blank" style="height:100%;"> ' +
        '<div class="mi-blank"> ' +
        '<div class="mi-blank-label">暂无内容~</div> ' +
        '</div> ' +
        '</div>';
