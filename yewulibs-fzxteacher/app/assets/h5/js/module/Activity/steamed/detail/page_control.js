//显示份数，上一份，下一份，删除份数的逻辑
    var PageControl = function (countView, turnPageView, datas, currentPage) {
        this.countView = countView;
        this.turnPageView = turnPageView;
        this.datas = datas;
        //当前页面
        this.currentPage = currentPage;
        //总页面
        this.totalPage = datas.length;


        //上一页、下一页是否可用
        this.nextPageOk = false;
        this.prePageOk = false;

        this.init();
    };


    PageControl.prototype.refresh = function (datas, currentPage) {
        this.datas = datas;
        this.currentPage = currentPage;
        this.totalPage = datas.length;

        //初始化页面统计和页面翻页状态
        this.updateStatus();
    };

    PageControl.prototype.init = function () {
        this.currentPageView = this.countView.find('[sid=current_page]');
        this.totalPageView = this.countView.find('[sid=total_page]');
        this.nextPageView = this.turnPageView.find('[sid=next_page]');
        this.prePageView = this.turnPageView.find('[sid=pre_page]');

        //初始化页面统计和页面翻页状态
        this.updateStatus();

        //初始化事件
        var that = this;
        this.nextPageView.on('click',this,function () {
            that.changePage(2);
        });
        this.prePageView.on('click',this,function () {
            that.changePage(1);
        });
    };
    
    PageControl.prototype.deletePage = function () {
        var currentData = this.datas[this.currentPage - 1];
        var param = {};
        param.id = currentData.id;
        param.correctType = 3;
        ApiActivity.updateSurface(param, this, function (flag, data) {
            if(flag)
            {
                alert("操作成功");
                //更新数据和总页数
                this.datas.splice(this.currentPage - 1, 1);
                this.totalPage = this.totalPage - 1;

                if(this.currentPage > this.totalPage)
                    this.currentPage = this.totalPage;
                this.updateStatus();

                this.changePage(3);
            }
            else
                alert(data.msg);
        });
    };

    PageControl.prototype.changePage = function (model) {
    //    model=1 pre， model=2 next, model = 3不变用于删除内容。
        if((model == 1 && this.prePageOk) || (model == 2 && this.nextPageOk))
        {
            if(model == 1 && this.prePageOk)
                this.currentPage = this.currentPage - 1;
            if(model == 2 && this.nextPageOk)
                this.currentPage = this.currentPage + 1;
            this.updateStatus();
            if(this.currentPage > 0 && this.currentPage <= this.totalPage)
            {
                if(this.listenerFunc)
                    this.listenerFunc.call(this.listener, this.datas[this.currentPage - 1]);
            }
            else
            {
                if(this.listenerFunc)
                    this.listenerFunc.call(this.listener, 0);
            }
        }
        //删除
        if(model == 3)
        {
            if(this.currentPage > 0 && this.currentPage <= this.totalPage)
            {
                if(this.listenerFunc)
                    this.listenerFunc.call(this.listener, this.datas[this.currentPage - 1]);
            }
            else
            {
                if(this.listenerFunc)
                    this.listenerFunc.call(this.listener, 0);
            }
        }
    };

    //页码更新
    PageControl.prototype.updateStatus = function () {
        this.currentPageView.text(this.currentPage);
        this.totalPageView.text(this.totalPage);
        this.updatePageTurn();
    };

    //页码上下翻页状态更新
    PageControl.prototype.updatePageTurn = function () {
        if (this.totalPage == 0) {
            this.nextPageView.removeClass('on');
            this.nextPageOk = false;
            this.prePageView.removeClass('on');
            this.prePageOk = false;
            return;
        }

        if (this.currentPage > 1) {
            this.prePageView.addClass('on');
            this.prePageOk = true;
        }
        if (this.currentPage == 1) {

            this.prePageView.removeClass('on');
            this.prePageOk = false;
        }

        if (this.currentPage < this.totalPage) {

            this.nextPageView.addClass('on');
            this.nextPageOk = true;
        }
        if (this.currentPage == this.totalPage) {

            this.nextPageView.removeClass('on');
            this.nextPageOk = false;
        }
    };

    PageControl.prototype.addListener = function (listener,listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };
