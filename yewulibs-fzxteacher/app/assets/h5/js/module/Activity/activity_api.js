/**
 * Created by ming on 2018/1/24.
 */

var TEACHER_INFO_URL = '/auth/user/teacher/info';//获取老师的基本信息
var SUBJECT_CLASS_LIST_URL = '/auth/user/teacher/subject/teach/list';//班级列表
var TEACH_TASK_LIST_URL = '/teach/task/list';//活动列表
var TEACH_TASK_ANSWER_URL = '/teach/task/answer/send';//发送答案
var TEACH_TASK_DELETE_URL = '/teach/task/delete';//删除活动
var AUTH_CLASS_GROUP_LIST_URL = '/auth/class/group/list';//小组列表
var TEACH_TASK_CLASS_TRANSCRIPT_URL = 'teach/task/class/transcript';//成绩单-整体列表
var TEACH_TASK_GROUP_TRANSCRIPT_URL = '/teach/task/group/transcript';//成绩单-分组列表
var TEACH_TASK_CLASS_COMPLETION_URL = '/teach/task/class/completion';//成绩单-chart和详情（小组和整体一起）
var STUDENT_TYPE_UPDATE_URL = '/teach/lesson/task/student/type/update';//跟新卷面
var EXAM_NUMBER_LIST_URL = '/teach/lesson/task/exam/number/list';//题目分析-题号列表
var EXAM_STUDENT_LIST = '/teach/lesson/task/exam/student/list'; //错误、正确、半对学生统计
var EXAM_STUDENT_LIST_URL = '/teach/lesson/task/exam/student/list';//题目分析-单题统计
var EXAM_ANALYSE_URL = '/teach/lesson/task/exam/analyse';//题目分析-试题分析（典型、题目分析）
var EXAM_ANSWER_TYPE_UPDATE_URL = '/teach/lesson/answer/exam/type/update';//作答典型
var TEACH_LESSON_TASK_DETAIL_LIST_URL = '/teach/lesson/task/detail/list';//题目分析中的题目
var TEACH_LESSON_TASK_RESOURCE_ANALYSE='/teach/lesson/task/resource/analyse';
var TEACH_LESSON_TASK_DETAIL_LIST='/teach/lesson/task/detail/list';// 任务明细
var TEACH_TASK_EXAM_LIST='/teach/task/exam/list';
var TEACH_LESSON_ANSWER_EXAM_CORRECT_TYPE_LIST='/teach/lesson/answer/exam/correct/type/list';
var TEACH_EXAM_URL = '/teach/lesson/answer/exam/info';//学生作答
var TEACH_LESSON_ANSWER_EXAM_LIST_URL = "/teach/lesson/answer/exam/list";//学生作答结果列表（包含批改结果）
var EXAM_ANSWER_INFO_URL =  "/teach/lesson/answer/exam/info";//学生作答结果（单题）
var STUDENT_FACE_LIST_URL = "/teach/lesson/task/student/face/list";//卷面查询列表

var ApiActivity = function () {

};

//获取老师的基本信息
ApiActivity.getTeachInfo = function (data, listener, listenerFun) {
    doAjax({
        url: TEACHER_INFO_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {

                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//教师任教班级查询
ApiActivity.classList = function (data, listener, listenerFun) {
    doAjax({
        url: SUBJECT_CLASS_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {

                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//活动查询
ApiActivity.activityList = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_LIST_URL,

        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {

                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//发送答案
ApiActivity.sendAnswer = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_ANSWER_URL,

        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {

                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//删除活动
ApiActivity.removeActivity = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_DELETE_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data)
        }
    })
};

//获取某个班级小组列表
ApiActivity.classGroupList = function (data, listener, listenerFun) {
    doAjax({
        url: AUTH_CLASS_GROUP_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//根据班级查询人员列表
ApiActivity.classTranscript = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_CLASS_TRANSCRIPT_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
//根据班级小组查询人员列表
ApiActivity.groupTranscript = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_GROUP_TRANSCRIPT_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//查询成绩单查询小组chart 和 整体chart
ApiActivity.completion = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_CLASS_COMPLETION_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
//学生题目回答情况
ApiActivity.examList = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_EXAM_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
//卷面优秀、粗糙一般
ApiActivity.updateSurface = function (data, listener, listenerFun) {
    doAjax({
        url: STUDENT_TYPE_UPDATE_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//题号列表
ApiActivity.examNumberList = function (data, listener, listenerFun) {
    doAjax({
        url: EXAM_NUMBER_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
//任务题目列表
ApiActivity.taskDetail = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_LESSON_TASK_DETAIL_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//题目分析-单题数据
ApiActivity.taskDetail = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_LESSON_TASK_DETAIL_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

// 资源分析页面 获取资源列表 任务明细
ApiActivity.examStudentList = function (data, listener, listenerFun) {
    doAjax({
        url: EXAM_STUDENT_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
//
// 资源分析
ApiActivity.resourceAnalyse = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_LESSON_TASK_RESOURCE_ANALYSE,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};


// 试题列表
ApiActivity.getExamList = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_TASK_EXAM_LIST,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//题目分析-题目统计（典型错误、试题分析）
ApiActivity.getExamAnalyse = function (data, listener, listenerFun) {
    doAjax({
        url: EXAM_ANALYSE_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
// 错误半对正确学生统计
ApiActivity.getExamCensusStudentList = function (data, listener, listenerFun) {
    doAjax({
        url: EXAM_STUDENT_LIST,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

// 讲解详情
ApiActivity.getCorrectDetail = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_LESSON_ANSWER_EXAM_CORRECT_TYPE_LIST,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//更新作答典型数据
ApiActivity.updateTypicalCase = function (data, listener, listenerFun) {
    doAjax({
        url: EXAM_ANSWER_TYPE_UPDATE_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
//学生作答结果列表（包含批改结果）
ApiActivity.examAnswerList = function (data, listener, listenerFun) {
    doAjax({
        url: TEACH_LESSON_ANSWER_EXAM_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

//学生作答结果，单题
ApiActivity.examAnswerInfo = function (data, listener, listenerFun) {
    doAjax({
        url: EXAM_ANSWER_INFO_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};

// 资源分析页面 获取资源列表 任务明细
ApiActivity.getResourceList = function (data, listener, listenerFun) {
  doAjax({
    url: TEACH_LESSON_TASK_DETAIL_LIST,
    type: "post",
    data: data,
    success: function (data) {
      if (data.code === 0) {
        listenerFun.call(listener, true, data.result);
      } else {
        listenerFun.call(listener, false, data);
      }
    },
    error: function (data) {
      listenerFun.call(listener, false, data)
    }
  })
};

//卷面查询列表
ApiActivity.studentFaceList = function (data, listener, listenerFun) {
    doAjax({
        url: STUDENT_FACE_LIST_URL,
        type: "post",
        data: data,
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data.result);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data)
        }
    })
};
