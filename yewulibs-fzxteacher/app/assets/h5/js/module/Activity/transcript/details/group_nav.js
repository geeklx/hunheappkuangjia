/**
 * 〈一句话功能简述〉
 * 〈功能详细描述〉
 * @param  [参数1]   [参数1说明]
 * @param  [参数2]   [参数2说明]
 * @return [返回类型说明]
 * @exception/throws [违例类型] [违例说明]
 * @see          [类、类#方法、类#成员]
 * @deprecated
 */
/**
 * Created by zhangxia on 2018/1/8. changed by Lixiaoming
 */
    var GroupNav = function (view,classId,initValue) {
        this.view = view;

        this.classId = classId;
        //设置默认数据
        this.defaultValue = null;
        //置入初始数据
        this.initValue = initValue;
        //navLabel集合
        this.navLabelList = [];
        //选择值
        this.selectChooserLabel = null;

        this.initView();
    };

    GroupNav.prototype.initView = function () {
        var that = this;
        this.view.load('html/activity/activity_detail/navigation.html', function () {
            that.init();
        })
    };

    GroupNav.prototype.init = function () {
        this.left = this.view.find("[cid=left]");
        this.right = this.view.find("[cid=right]");
        this.center = this.view.find("[cid=center]");
        //清除
        this.center.empty();
        //创建初始数据
        var param = {};
        param.classId = this.classId;
        ApiActivity.classGroupList(param, this, this.handleClassGroupList);
    };

    GroupNav.prototype.handleClassGroupList = function (flag, data) {
        var that = this;
        if (flag) {
            if (data != null && data != undefined) {
                var navArray = data.data;
               // 删除全部人员和未分组
               for(var i = navArray.length - 1;i >= 0;i--)
                   if(navArray[i].id == -1 || navArray[i].id == 0)
                       navArray.splice(i,1);

                $(navArray).each(function (key, value) {
                    var data = {};
                    data.code = value.id;
                    data.name = value.groupName;
                    var navLabel = new ChooserLabel(that.center, data, GroupNav.phtml, function (labelView, name) {
                        labelView.text(name);
                    });
                    that.navLabelList.push(navLabel);
                    navLabel.addListener(that, that.onValueChange)
                });

                //设置默认值
                if(navArray.length > 0)
                    this.defaultValue = navArray[0].id;

                //进行初始选择,如果没有初始选额，那么选择默认值
                if (this.initValue != null && this.initValue != undefined)
                    this.select(this.initValue);
                else
                    this.select(this.defaultValue);
            }
            else{
                Information.show(2, "返回数据为NULL");
            }
        }
        else
            Information.show(2, data.msg);
        this.initEvent();

        if(this.listenerFunc)
            this.loadedListenerFunc.call(this.loadedListener,this.getValue());
    };

    GroupNav.prototype.initEvent = function () {
        this.left.on("click", this, this.leftTiggerEvent);
        this.right.on("click", this, this.rightTiggerEvent);
    };
    GroupNav.prototype.leftTiggerEvent = function (evt) {
        var self = evt.data;
        var w = self.center[0].scrollLeft;
        $(self.center).animate({scrollLeft: w - 200}, 100);
        console.log(w);
    };
    GroupNav.prototype.rightTiggerEvent = function (evt) {
        var self = evt.data;
        var w = self.center[0].scrollLeft;
        $(self.center).animate({scrollLeft: 200 + w}, 100);
        console.log(w);
    };

//触发事件:如果选择项之前选择过则不触发
    GroupNav.prototype.onValueChange = function (navCode) {
        if (this.select(navCode))
            if (this.listenerFunc)
                this.listenerFunc.call(this.listener, this.getValue());
    };

    //选择某个班级
    GroupNav.prototype.select = function (navCode) {
        if (this.selectChooserLabel != null && this.selectChooserLabel != undefined) {
            //如果选择项为之前选择项,不做任何改变，返回未执行更新flag。
            if (this.selectChooserLabel.data.code == navCode)
                return false;
            //如果之前有选择，取消之前选择
            if (this.selectChooserLabel != null)
                this.selectChooserLabel.unSelect();
        }

        //如果ChooserCode匹配，选择上ChooserCode对应选项。
        var that = this;
        //记录是否找到元素
        var flag = false;
        $(this.navLabelList).each(function (key, value) {
            if (!flag && value.data.code == navCode) {
                value.select();
                that.selectChooserLabel = value;
                flag = true;
            }
        })
        if (flag)
            return true;
        //如果选项没有对应上，不做任何选择
        this.selectChooserLabel = null;
        return false;
    };

    //恢复到默认值
    GroupNav.prototype.reset = function () {
        this.select(this.defaultValue);
    };

    //得到选择值
    GroupNav.prototype.getValue = function () {

        if (this.selectChooserLabel != null && this.selectChooserLabel != undefined)
            return this.selectChooserLabel.data.code;
        //如果未选择任何数据
        return null;
    };

    //监听
    GroupNav.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

    GroupNav.prototype.addLoadedListener = function (loadedListener, loadedListenerFunc) {
        this.loadedListener = loadedListener;
        this.loadedListenerFunc = loadedListenerFunc;
    }
    GroupNav.phtml = '<div class="lebel"></div>';
