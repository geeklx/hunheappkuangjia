/**
 * Created by ming on 2018/1/26.
 */
define(function (require, exports, module) {
    //引用
    var StatusLabel =  require('../item/status_label.js');

    var StatusCondition = function (view) {
        this.view = view;

        //设置默认数据
        this.defaultValue = 0;
        //置入初始数据
        this.initValue = 0;
        //statusLabel集合
        this.statusLabelList = [];
        //选择值
        this.selectStatusLabel = null;
        this.init();
    };

    module.exports = StatusCondition;

    StatusCondition.prototype.init = function () {
        //    listView盛放按钮View
        this.listView = this.view.find('ul');
        //    创建初始数据
        var statusArray = [
            {
                'id': 0,
                'name': '全部'
            }, {
                'id': 1,
                'name': '待发布'
            }, {
                'id': 2,
                'name': '进行中'
            }, {
                'id': 3,
                'name': '已截止'
            }, {
                'id': 4,
                'name': '批改中'
            }
        ];
        var that = this;
        $(statusArray).each(function (key, value) {
            var statusLabel = new StatusLabel(that.listView, value);
            that.statusLabelList.push(statusLabel);
            statusLabel.addListener(that, that.onValueChange)
        });
        //进行初始选择
        this.select(this.initValue);
    };

    //触发事件:如果选择项之前选择过则不触发
    StatusCondition.prototype.onValueChange = function (statusId) {
        if (this.select(statusId))
            if (this.listenerFunc)
                this.listenerFunc.call(this.listener);
    };

    //选择某个班级
    StatusCondition.prototype.select = function (statusId) {


        if (this.selectStatusLabel != null && this.selectStatusLabel != undefined) {
            //如果选择项为之前选择项,不做任何改变，返回未执行更新flag。
            if (this.selectStatusLabel.statusData.id == statusId)
                return false;
            //如果之前有选择，取消之前选择
            if (this.selectStatusLabel != null)
                this.selectStatusLabel.unSelect();
        }

        //如果StatusId匹配，选择上StatusId对应选项。
        var that = this;
        //记录是否找到元素
        var flag = false;
        $(this.statusLabelList).each(function (key, value) {
            if (!flag && value.statusData.id == statusId) {
                value.select();
                that.selectStatusLabel = value;
                flag = true;
            }
        })
        if (flag)
            return true;
        //如果选项没有对应上，不做任何选择
        this.selectStatusLabel = null;
        return false;
    };

    //恢复到默认值
    StatusCondition.prototype.reset = function () {
        this.select(this.defaultValue);
    };

    //得到初始值
    StatusCondition.prototype.getInitValue = function () {
        return this.initValue;
    };

    //得到选择值
    StatusCondition.prototype.getValue = function () {

        if (this.selectStatusLabel != null && this.selectStatusLabel != undefined)
            return this.selectStatusLabel.statusData.id;
        //如果未选择任何数据
        return null;
    };

    //监听
    StatusCondition.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

})