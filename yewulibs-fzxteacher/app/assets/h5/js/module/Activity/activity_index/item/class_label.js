/**
 * Created by ming on 2018/1/26.
 */

//班级选择组的label

define(function (require, exports, module) {

    var ClassLabel = function(parentView, classData){
        this.classData = classData;
        this.parentView = parentView;
        //初始化
        this.init();
    };
    module.exports = ClassLabel;

        //初始化 绘制、注册监听
    ClassLabel.prototype.init = function(){
        this.phtml = $(ClassLabel.phtml).clone(true);
        //赋值
        this.phtml.text(this.classData.name);
        //添加那妞
        this.parentView.append(this.phtml);
        //注册监听
        this.phtml.on("click",this,this.onSelect);
    };

    //点击选择事件，触发具体过程在父组件处理。
    ClassLabel.prototype.onSelect = function(eve){
        var self = eve.data;
        //触发事件
        if(self.listenerFunc)
            self.listenerFunc.call(self.listener,self.classData.id);
    };

    //控制选择,添加样式
    ClassLabel.prototype.select = function() {
        if(!this.phtml.hasClass('on'))
            this.phtml.addClass('on');
    };

    //控制不选择,去除样式
    ClassLabel.prototype.unSelect = function(){
        if(this.phtml.hasClass('on'))
            this.phtml.removeClass('on');
    };

    //监听
    ClassLabel.prototype.addListener = function(listener,listenerFunc){
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

    ClassLabel.phtml = '<li></li>';
})