    var StudentDetail = function (data, model, lessonData, activityData, navCode, navData) {
        
        this.view = $('[cid=container]');

        this.data = data;
        this.model = model;
        this.lessonData = lessonData;
        this.activityData = activityData;
        this.navCode = navCode;
        this.navData = navData;
        //面包屑
        this.crumbView = null;
        this.crumb = null;

        this.initView();
    };


    StudentDetail.prototype.initView = function () {
        this.view.load('html/activity/activity_detail/details/details.html', this.init.bind(this))
    };

    StudentDetail.prototype.init = function () {
        //面包屑
        this.crumbView = this.view.find('[sid=crumb]');
        this.crumb = new StudentDetailCrumb(this.crumbView, this.model, this.lessonData, this.activityData, this.navCode, this.navData);
        this.crumbView.hide();
        //上方信息区
        //绘制头像，如果没有绘制默认图像
        this.studentImageView = this.view.find('[sid=student_image]');
        if(this.data.photo == null || this.data.photo == undefined)
            this.studentImageView.attr('src','img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg');
        else
            this.studentImageView.attr("src",this.data.photo);

        //绘制名称
        this.studentNameView = this.view.find('[sid=student_name]');
        this.studentNameView.text(this.data.userStudentName);

        //绘制得分
        this.scoreView = this.view.find('[sid=score]');
        this.scoreView.text(this.data.scoreTotal);

        //绘制用时
        this.spendTimeView = this.view.find('[sid=spend_time]');
        this.spendTimeView.text(this.formatSpendTime(this.data.useTime));

        //    绘制总分+班级平均分 + 卷面
        this.totalScoreView = this.view.find('[sid=total_score]');
        this.averageScoreView =this.view.find('[sid=average_score]');
        this.surfaceView = this.view.find('[sid=surface]');

        var param = {};
        param.id = this.activityData.id;
        var that = this;
        ApiActivity.completion(param, this, function (flag, data) {
            if (flag) {
                if (data != null && data != undefined) {
                    that.totalScoreView.text(data.data.scope);
                    that.averageScoreView.text(data.data.scoreAvg);
                }
                else
                    alert("返回数据为NULL");
            }
            else
                alert(data.msg);
        });

        //卷面相关
        this.surface = new Surface(this.surfaceView, this.data.id, this.data.correctType);

        //右侧界面
        this.titleNavContainer = this.view.find('[sid=title_container]');
        this.titleNavView = this.view.find('[sid=title_nav]');

        //中间界面
        this.answerListView = this.view.find('[sid=answer_list]');
        // 调取学生作答数据
        //滚动条
        this.setHeight(this.answerListView);
        this.setHeight(this.titleNavContainer);
        var that=this;
        $(window).resize(function(){
            that.setHeight(that.answerListView);
            that.setHeight(that.titleNavContainer);
        });
        var param = {};
        param.lessonTaskStudentId = this.data.id;
        ApiActivity.examAnswerList(param, this, function (flag, data) {
            if(flag)
            {
                if(data != null || data != undefined)
                {
                    var data = data.data;
                    // 填充右侧题号列表
                    this.titleNav = new TitleNav(this.titleNavView,data,1);
                //   填充中间内容
                    this.answerList = new AnswerList(this.answerListView,data);
                }
            }
            else
                alert(data.msg);

        });
    };

    StudentDetail.prototype.setHeight=function(obj){
        var repre_right_top=obj.offset().top;
        var heig=$(document).height()-repre_right_top;
        obj.height(heig);

    };

    StudentDetail.prototype.formatSpendTime = function (seconds) {
        var spendTime = '';
        if(parseInt(seconds / 3600) > 0)
            spendTime += parseInt(seconds / 3600) + "小时";
        seconds = seconds % 3600;

        if(parseInt(seconds / 60) > 0)
            spendTime += parseInt(seconds / 60) + '分';
        seconds = seconds % 60;

        spendTime += seconds +'秒';
        return spendTime;
    };
