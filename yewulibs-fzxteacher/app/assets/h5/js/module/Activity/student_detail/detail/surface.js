/**
 * Created by ming on 2018/2/6.
 */
//卷面选择控制组件
    var Surface = function (view, taskStudentId, initType) {
        this.view = view;
        this.initType = initType;
        this.taskStudentId = taskStudentId;

        this.isSurfaceGood = false;
        this.isSurfaceBad = false;

        this.surfaceGoodView = null;
        this.surfaceBadView = null;

        this.init();
    };


    Surface.prototype.init = function () {
        this.surfaceGoodView = this.view.find('[sid=surface_good]');
        this.surfaceBadView = this.view.find('[sid=surface_bad]');

        if (this.initType == 1) {
            this.isSurfaceGood = true;
            this.isSurfaceBad = false;
        }
        else if (this.initType == 2) {
            this.isSurfaceGood = false;
            this.isSurfaceBad = true;
        }
        else {
            this.isSurfaceGood = false;
            this.isSurfaceBad = false;
        }
        //根据初始化数据，改变view;
        this.changView();
    //    添加监听
        this.surfaceGoodView.off('click').on('click',{'obj':this, 'type':'good'},this.onClick);
        this.surfaceBadView.off('click').on('click',{'obj':this,'type':'bad'},this.onClick);
    };
    
    Surface.prototype.onClick = function (evt) {
        var self = evt.data.obj;
        var type = evt.data.type;
        if(type == 'good')
        {
            self.isSurfaceGood = !self.isSurfaceGood;
            if(self.isSurfaceGood && self.isSurfaceBad)
                self.isSurfaceBad = false;
        }
        if(type== 'bad')
        {
            self.isSurfaceBad = !self.isSurfaceBad;
            if(self.isSurfaceBad && self.isSurfaceGood)
                self.isSurfaceGood = false;
        }
        self.changView();
        self.updateSurfaceData();
    };

    Surface.prototype.changView = function () {
        if(this.isSurfaceGood)
            this.surfaceGoodView.addClass('on');
        else
            this.surfaceGoodView.removeClass('on');

        if(this.isSurfaceBad)
            this.surfaceBadView.addClass('on');
        else
            this.surfaceBadView.removeClass('on');
    };

    Surface.prototype.updateSurfaceData = function () {
        var param = {};
        param.id = this.taskStudentId;

        if(!this.isSurfaceBad && this.isSurfaceGood)
            param.correctType = 1;
        else if(this.isSurfaceBad && !this.isSurfaceGood)
            param.correctType = 2;
        else if(!this.isSurfaceBad && !this.isSurfaceGood)
            param.correctType = 3;
        else {
            console.log("同时选择了优秀和粗糙，错误！！！！");
            return;
        }
        console.log(this.taskStudentId + " " + param.correctType);
        ApiActivity.updateSurface(param, this, function (flag, data) {
            if(flag)
            {
                alert("操作成功");
            }
            else
                alert(data.msg);
        });
    };
