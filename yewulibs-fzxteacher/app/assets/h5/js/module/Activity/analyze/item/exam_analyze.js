/**
 * Created by ming on 2018/2/11.
 */
    var ExamAnalyze = function (view, censusData) {
        this.view = view;
        this.censusData = censusData;

        /*this.censusData = {
            "examList": [/!*小题列表-除综合题外其他题型为题目本身，同外层数据一致*!/
                {
                    "examTemplateId":1,/!*题型模板id*!/
                    "examTemplateStyleId":1,/!*学科题型id*!/
                    "examTemplateStyleName":"单选题",/!*学科题型名称*!/
                    "emptyCount":4,/!*选项数或者空格数*!/
                    "answerList":[/!*题目作答情况列表，其他题目为一个对象，填空题每空为一个对象*!/
                        {
                            "optionList":[/!*详细作答统计列表*!/
                                {
                                    "examSeq":1,/!*同外层一致，数据无意义*!/
                                    "optionSeq":1,/!*同外层一致，数据无意义*!/
                                    "myAnswer":"A",/!*作答结果*!/
                                    "count":1,/!*作答数量*!/
                                    "rate":20,/!*作答比例*!/
                                    "right":false/!*是否答案*!/
                                },
                                {
                                    "examSeq":1,
                                    "optionSeq":1,
                                    "myAnswer":"B",
                                    "count":1,
                                    "rate":20,
                                    "right":false
                                },
                                {
                                    "examSeq":1,
                                    "optionSeq":1,
                                    "myAnswer":"C",
                                    "count":1,
                                    "rate":20,
                                    "right":true
                                },
                                {
                                    "examSeq":1,
                                    "optionSeq":1,
                                    "myAnswer":"D",
                                    "count":2,
                                    "rate":40,
                                    "right":false
                                }
                            ],
                            "examSeq":1,/!*小题所在顺序，选择等默认1*!/
                            "optionSeq":1/!*空格所在顺序，其他默认1*!/
                        }
                    ]
                },{
                    "examTemplateId":3,/!*题型模板id*!/
                    "examTemplateStyleId":3,/!*学科题型id*!/
                    "examTemplateStyleName":"判断题",/!*学科题型名称*!/
                    "emptyCount":2,/!*选项数或者空格数*!/
                    "answerList":[/!*题目作答情况列表，其他题目为一个对象，填空题每空为一个对象*!/
                        {
                            "optionList":[/!*详细作答统计列表*!/
                                {
                                    "examSeq":1,/!*同外层一致，数据无意义*!/
                                    "optionSeq":1,/!*同外层一致，数据无意义*!/
                                    "myAnswer":"正确",/!*作答结果*!/
                                    "count":1,/!*作答数量*!/
                                    "rate":20,/!*作答比例*!/
                                    "right":true/!*是否答案*!/
                                },
                                {
                                    "examSeq":1,
                                    "optionSeq":1,
                                    "myAnswer":"错误",
                                    "count":1,
                                    "rate":20,
                                    "right":false
                                }
                            ],
                            "examSeq":2,/!*小题所在顺序，选择等默认1*!/
                            "optionSeq":1/!*空格所在顺序，其他默认1*!/
                        }
                    ]
                },
                {
                    "examTemplateId":6,
                    "examTemplateStyleId":6,
                    "examTemplateStyleName":"填空题",
                    "emptyCount":2,
                    "answerList":[
                        {
                            "optionList":[
                                {
                                    "examSeq":2,
                                    "optionSeq":1,
                                    "myAnswer":"相交",
                                    "count":1,
                                    "rate":20,
                                    "right":false
                                },
                                {
                                    "examSeq":2,
                                    "optionSeq":1,
                                    "myAnswer":"相切",
                                    "count":1,
                                    "rate":20,
                                    "right":false
                                },
                                {
                                    "examSeq":2,
                                    "optionSeq":1,
                                    "myAnswer":"相离",
                                    "count":2,
                                    "rate":40,
                                    "right":true
                                }
                            ],
                            "examSeq":3,
                            "optionSeq":1
                        },
                        {
                            "optionList":[
                                {
                                    "examSeq":2,
                                    "optionSeq":2,
                                    "myAnswer":"必要不充分条件",
                                    "count":1,
                                    "rate":20,
                                    "right":true
                                },
                                {
                                    "examSeq":2,
                                    "optionSeq":2,
                                    "myAnswer":"充分不必要条件",
                                    "count":1,
                                    "rate":20,
                                    "right":false
                                },
                                {
                                    "examSeq":2,
                                    "optionSeq":2,
                                    "myAnswer":"既不充分又不必要条件",
                                    "count":2,
                                    "rate":40,
                                    "right":false
                                }
                            ],
                            "examSeq":3,
                            "optionSeq":2
                        }
                    ]
                }
            ],
            "examTemplateId": 16,/!*题型模板id*!/
            "examTemplateStyleId": 16,/!*学科题型id*!/
            "examTemplateStyleName": "混合题",/!*学科题型名称*!/
            "typicalErrorList": [/!*典型错误学生列表*!/
                {
                    "lessonAnswerExamId": 254,
                    "lessonTaskStudentId": 72,
                    "lessonTaskId": 5,
                    "sex": 1,
                    "lessonTaskDetailsId": 4,
                    "photo": "XXX/2a3f29e89bff4e1e81b1d5adb12662ac.png",
                    "userStudentName": "孟杰",
                    "userStudentId": 3
                }
            ],
            "excellentAnswerList": [/!*优秀解答学生列表*!/
                {
                    "lessonAnswerExamId": 249,
                    "lessonTaskStudentId": 71,
                    "lessonTaskId": 5,
                    "sex": 1,
                    "lessonTaskDetailsId": 4,
                    "photo": "XXX/02/2a3f29e89bff4e1e81b1d5adb12662ac.png",
                    "userStudentName": "栗新斌",
                    "userStudentId": 2
                }
            ]
        };*/

        //是否存在要展示的分析数据，如果不存在，则不添加任何内容
        this.hasContent = false;
        this.init();
    };


    ExamAnalyze.prototype.init = function () {

        //小题信息
        this.examList = this.censusData.examList;
        console.log(this.examList);
        //学科题型信息
        this.examTemplateId = this.censusData.examTemplateId;
        this.examTemplateStyleName = this.censusData.examTemplateStyleName;

        //用于存放的view，如果检查有内容，添加到view中，如果没有，则不添加
        this.parentView = $(this.parentHtml).clone(true);

        //    添加题目
        this.examTypeView = $(this.examTypeHtml).clone(true);
        this.examTypeView.find('[sid=exam_type]').text(this.examTemplateStyleName);
        this.parentView.append(this.examTypeView);

        //除填空题外的三种简单题型,不需要编号直接对应书写
        if(this.examTemplateId == 1 || this.examTemplateId == 2 ||  this.examTemplateId == 3)
            this.createAnaView(this.parentView,this.examTemplateId, this.examList[0].answerList[0].optionList);
        //填空题，单空
        if(this.examTemplateId == 6 && this.examList.length > 0 &&  this.examList[0].emptyCount == 1)
            this.createAnaView(this.parentView,this.examTemplateId, this.examList[0].answerList[0].optionList);
        //填空题，多空
        if(this.examTemplateId == 6 && this.examList.length > 0 &&  this.examList[0].emptyCount > 1)
        {
            var that = this;
            $(this.examList[0].answerList).each(function (key, value) {
                //绘制填空小空空号
                that.createTitle(that.parentView, that.getBlankSeq(value.optionSeq));
                //绘制填空题分析
                that.createAnaView(that.parentView, that.examTemplateId, value.optionList);
            });
        }
        var that = this;
        //综合题
        if(this.examTemplateId == 16)
            $(this.examList).each(function (key,value) {
                //选择、判断题小题
                if(value.examTemplateId == 1 || value.examTemplateId == 2 || value.examTemplateId == 3)
                {
                    //绘制小题题号
                    that.createTitle(that.parentView, that.getSmallSeq(value.answerList[0].examSeq));
                    //绘制题目分析
                    that.createAnaView(that.parentView, value.examTemplateId, value.answerList[0].optionList);
                }
                //    填空题、单空小题
                if(value.examTemplateId == 6 && value.emptyCount == 1)
                {
                    //绘制小题题号
                    that.createTitle(that.parentView, that.getSmallSeq(value.answerList[0].examSeq));
                    //绘制题目分析
                    that.createAnaView(that.parentView, value.examTemplateId, value.answerList[0].optionList);
                }
                //    填空题多空
                if(value.examTemplateId == 6 && value.emptyCount > 1)
                {
                    $(value.answerList).each(function (k, v) {
                        //绘制小题+空号
                        that.createTitle(that.parentView, that.getSmallSeq(v.examSeq) + that.getBlankSeq(v.optionSeq));
                        //绘制题目分析
                        that.createAnaView(that.parentView, value.examTemplateId, v.optionList);
                    });
                }
            });

        if (this.hasContent)
            this.view.prepend(this.parentView);
    };

    //绘制题号
    ExamAnalyze.prototype.createTitle = function (parentView, text) {
        var seqView = $(this.examSeqHtml).clone(true);
        seqView.find('[sid=small_seq]').text(text);
        parentView.append(seqView);
    };

    //绘制内容分发器
    ExamAnalyze.prototype.createAnaView = function (parentView, model, optionList) {
        if(model == 1 || model == 2 || model == 3 || model == 6)
        {
            if(model == 1 || model == 2)
                this.createSelectView(parentView,optionList);
            if(model == 3)
                this.createJudgeView(parentView,optionList);
            if(model == 6)
                this.createBlankView(parentView,optionList);
            //如果属于这其中一种就需要添加这个界面
            this.hasContent = true;
        }
    };

    //选择
    ExamAnalyze.prototype.createSelectView = function (parentView, optionList) {
        var that = this;
        $(optionList).each(function (key, value) {
            var selectView = $(that.selectExamHtml).clone(true);
            selectView.find('[sid=answer]').text(value.myAnswer);
            selectView.find('[sid=count]').text(value.count + '人');
            selectView.find('[sid=progress]').css('width', value.rate + '%');
            selectView.find('[sid=progress_text]').text(value.rate + '%');
            if (value.right)
            {
                selectView.find('[sid=progress]').addClass('on');
                selectView.find('[sid=answer]').addClass('on');
            }
            parentView.append(selectView);
        });
    };

    //判断
    ExamAnalyze.prototype.createJudgeView = function (parentView, optionList) {
        var that = this;
        $(optionList).each(function (key, value) {
            var judgeView = $(that.judgeExamHtml).clone(true);
            judgeView.find('[sid=answer]').text(value.myAnswer);
            judgeView.find('[sid=count]').text(value.count + '人');
            judgeView.find('[sid=progress]').css('width', value.rate + '%');
            judgeView.find('[sid=progress_text]').text(value.rate + '%');
            if (value.right)
            {
                judgeView.find('[sid=progress]').addClass('on');
                judgeView.find('[sid=answer]').addClass('on');
            }

            parentView.append(judgeView);
        });
    };

    //填空
    ExamAnalyze.prototype.createBlankView = function (parentView, optionList) {
        var that = this;
        $(optionList).each(function (key, value) {
            var blankView = $(that.blankExamHtml).clone(true);
            if(value.myAnswer == null || value.myAnswer == undefined || value.myAnswer.trim() == "")
                value.myAnswer = "漏填";
            blankView.find('[sid=answer]').text(value.myAnswer);
            blankView.find('[sid=count]').text(value.count + '人');
            if (value.right)
                blankView.find('[sid=answer]').css('color', '#68C426');
            parentView.append(blankView);
        });
    };



    ExamAnalyze.prototype.getSmallSeq = function (num) {
        return '(' + num + ')';
    };

    ExamAnalyze.prototype.getBlankSeq = function (num) {
        return '[' + num + ']';
    };

    ExamAnalyze.prototype.parentHtml = '<div style="clear:both;" class="type_item"></div>';

    ExamAnalyze.prototype.examTypeHtml = '<div class="clear:both; type_title"><span sid="exam_type"></span></div>';
    ExamAnalyze.prototype.examSeqHtml = '<div style="clear:both;height: 30px;background-color: #FFFFFF;border-bottom: 1px #EDEDED solid"> ' +
        '<span sid="small_seq">⑴</span>' +
        '</div>';

    ExamAnalyze.prototype.selectExamHtml = '<div cid="seq_item" style="padding-left: 10px;">' +
        '<div style="clear:both;" class="type_item_row"> ' +
        '<div class="quest_number" sid="answer">A</div> ' +
        '<div class="number_people" sid="count"></div> ' +
        '<div class="quest_progress"> ' +
        '<div class="up_pro" sid="progress"></div> ' +
        '<div class="pro_text" sid="progress_text"></div></div> ' +
        '</div>' +
        '</div>';

    ExamAnalyze.prototype.judgeExamHtml = '<div cid="seq_item" style="padding-left: 10px;">' +
        '<div style="clear:both;width: 100%;height: 18px;margin-bottom: 10px;font-size: 12px;"> ' +
        '<div style="text-align:center;float:left;width: 30px;height: 18px;background: #FF6D4A;font-size: 12px;color: #FFFFFF;letter-spacing: 0;" sid="answer">正确</div> ' +
        '<div style="text-align:center;float:right;width: 27px;height: 18px;font-size: 12px;color: #000000;letter-spacing: 0;" sid="count">8人</div> ' +
        '<div style=";margin-left: 31px;margin-right:28px;height: 100%;word-break:break-all;background: #EDEDED;text-align: right"> ' +
        '<div style="float: left;background: #68C426;width: 50%;height: 100%" sid="progress"></div> ' +
        '<span style="float: right;" sid="progress_text">50%</span></div> ' +
        '</div>' +
        '</div>';

    ExamAnalyze.prototype.blankExamHtml ='<div cid="seq_item" style="padding-left: 10px; line-height: 30px;">' +
        '<div style="clear:both;"> ' +
        '<div style="color: #FF6D4A;;letter-spacing: -0.34px;float: left;" sid="answer">相交</div> ' +
        '<div style="float:right;font-size: 12px;color: #808FA3;letter-spacing: -0.34px;background: #EDEDED;text-align: right;margin-right: 10px;" sid="count">12人</div> ' +
        '</div>' +
        '</div>';
