/**
 * Created by zhangxia on 2018/1/31.
 * examInfo:试题信息 index:序号 从1开始
 * 例如：
 * var temp = new ExamLabel(examInfo,1);
   this.view.append(temp.view);
 */
  var ExamLabel = function (examId, examInfo,index,isShowAnswer) {
    this.examId = examId;
    this.examInfo = examInfo;
    this.index = index;
    this.isShowAnswer = isShowAnswer;
    this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    this.serialNum3 = ["正确", "错误"];

    this.openAnswerView = null;
    this.anchorName = null;
    this.initType();
  };


  ExamLabel.prototype.initType = function () {
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    switch (this.examInfo.examTypeId){
      case 1:this.scanSingleExam(this.view,this.examInfo,this.index);break;
      case 2:this.scanSingleExam(this.view,this.examInfo,this.index);break;
      case 6:this.scanComExam(this.view,this.examInfo,this.index);break;
      case 3:this.scanJudgeExam(this.view,this.examInfo,this.index);break;
      case 4:this.scanShortExam(this.view,this.examInfo,this.index);break;
      case 16:this.scanAllExam(this.view,this.examInfo,this.index);break;
      default:console.log('未找到对应题型：'+this.examInfo.examTypeId);
    }
    // ExamPreViewTem.openAnswer = '<div class="openAnswer" cid="openAnswer"> 显示答案 </div><a name="t13"></a>';
//  添加显示和删除答案
    this.openAnswerView = $(ExamPreViewTem.openAnswer).clone(true);
    this.view.prepend(this.openAnswerView);
    this.controlAnswer(this.isShowAnswer);
  //  添加锚点
    this.anchorName = 'analyze' + this.examId;
    this.anchorView = $('<a name="t13"></a>').clone(true);
    this.view.prepend(this.anchorView);
    this.anchorView.attr('name',this.anchorName);

  //  添加监听
    this.openAnswerView.on('click',this,this.clickOpenAnswer);
    this.view.on('click',this,this.onSelect);
  };

  ExamLabel.prototype.initData = function (indexFlag,data,type) {
    var viewArr = {};
    viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
    viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
    viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
    viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
    viewArr.answerView = $(ExamPreViewTem.amswerTemplate).clone();
    viewArr.analyzeView = $(ExamPreViewTem.analyzeTemplate).clone();

    viewArr.introView.find('[sid=index]').text(indexFlag);
    if(type){
      viewArr.introView.find('[sid=type]').text(this.stage(data.examTypeId));
    }else{
      viewArr.introView.find('[sid=type]').text(data.templateStyleName);
    }
    viewArr.stemView.html(data.examStem);
    viewArr.analyzeView.find('[sid=exam-scan-analyse]').html(data.examAnalysis?data.examAnalysis:'略');
    return viewArr;
  };

  ExamLabel.prototype.stage = function (num) {
    switch (num){
      case 1:return '单选题';break;
      case 2:return '多选题';break;
      case 3:return '判断题';break;
      case 4:return '简答题';break;
      case 6:return '填空题';break;
      default:return '未知';break;
    }
  };

//预览单选,多选
  ExamLabel.prototype.scanSingleExam = function (parentView,data,indexFlag,type) {
    var viewArr = this.initData(indexFlag,data,type);

    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);
    var arr = [];
    for(var i=0;i<data.examOptions.length;i++){
      var option = $(ExamPreViewTem.optionTemplate).clone();
      option.find('[sid=index]').text(this.serialNum1[i]);
      option.find('[sid=stem]').html(data.examOptions[i].content);
      viewArr.optionConView.append(option);
      if(data.examOptions[i].right){
        arr.push(this.serialNum1[i]);
      }
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(arr);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
  };

//预览判断
  ExamLabel.prototype.scanJudgeExam = function (parentView,data,indexFlag,type) {
    var viewArr = this.initData(indexFlag,data,type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
    var arr = [];
    for(var i=0;i<data.examOptions.length;i++){
      if(data.examOptions[i].right){
        arr.push(this.serialNum3[i]);
      }
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(arr);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
  };

//预览填空
  ExamLabel.prototype.scanComExam = function (parentView,data,indexFlag,type) {
    var viewArr = this.initData(indexFlag,data,type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
  };

//预览简答
  ExamLabel.prototype.scanShortExam = function (parentView,data,indexFlag,type) {
    var viewArr = this.initData(indexFlag,data,type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
  };

//预览综合
  ExamLabel.prototype.scanAllExam = function (parentView,data,indexFlag) {
    var viewArr = this.initData(indexFlag,data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.optionConView = $(ExamPreViewTem.optionAllConTemp).clone();
    parentView.append(viewArr.optionConView);

    for(var i=0;i<data.examBases.length;i++){
      switch (data.examBases[i].examTypeId){
        case 1:this.scanSingleExam(viewArr.optionConView,data.examBases[i],'('+parseInt(i+1)+')',1);break;
        case 2:this.scanSingleExam(viewArr.optionConView,data.examBases[i],'('+parseInt(i+1)+')',1);break;
        case 6:this.scanComExam(viewArr.optionConView,data.examBases[i],'('+parseInt(i+1)+')',1);break;
        case 3:this.scanJudgeExam(viewArr.optionConView,data.examBases[i],'('+parseInt(i+1)+')',1);break;
        case 4:this.scanShortExam(viewArr.optionConView,data.examBases[i],'('+parseInt(i+1)+')',1);break;
        default:console.log('未找到对应的小题型：'+data.examTypeId);
      }
    }
  };
//  点击事件
  ExamLabel.prototype.onSelect = function (evt) {
    var self = evt.data;
    if(self.listenerFunc)
        self.listenerFunc.call(self.listener, self.examId);
  };

  ExamLabel.prototype.clickOpenAnswer = function (evt) {
    var self = evt.data;
    self.isShowAnswer = !self.isShowAnswer;
    self.controlAnswer(self.isShowAnswer);
  }

//显示答案和解析
  ExamLabel.prototype.controlAnswer = function (flag) {
    if(flag)
    {
      this.view.find('.exam-scan-h').show();
      this.openAnswerView.text('隐藏答案');
    }
    else {
      this.view.find('.exam-scan-h').hide();
      this.openAnswerView.text('显示答案');
    }
  };

  ExamLabel.prototype.select = function () {
    if(!this.view.hasClass('opt'))
      this.view.addClass('opt');
    // location.href = "#" + this.anchorName;
  };
  
  ExamLabel.prototype.unSelect = function () {
    if(this.view.hasClass('opt'))
        this.view.removeClass('opt');
  };
  
  ExamLabel.prototype.addListener = function (listener, listenerFunc) {
    this.listener = listener;
    this.listenerFunc = listenerFunc;
  };
