/**
 * Created by wuhai on 2018/2/6.
 */
define(function (require, exports, module) {
    var StudentListItem = function (parentView, data) {
        this.parentView = parentView;
        this.data = data;
        this.init();
    };
    module.exports = StudentListItem;
    StudentListItem.template = ' <tr><td> <div class= "tab_td_text" ><div sid = "student_info"class = "sysIco img_sys_ico" > ' +
        '<img src = "img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg"/></div > ' +
        '<div sid="student_name"><span style = "margin-left: -30px;" ></span></div></div ></td ><td ><div sid = "student_status" >' +
        ' <span>  </span> </div ></td > <td><div sid = "student_time" > <span>  </span >' +
        ' </div ></td ></tr> ';

    StudentListItem.prototype.init = function () {
        // this.view = $(StudentListItem.template);
        // this.parentView.append(this.view);
        this.getList();
    };
    StudentListItem.prototype.getList = function () {
        this.parentView.empty();
        for (var i = 0, len = this.data.length; i < len; i++) {
            this.view = $(StudentListItem.template);
            this.view.find("[sid=student_name]").text(this.data[i].userStudentName);
            if (this.data[i].isRead == 1) {
                this.view.find("[sid=student_status]").text("已完成");
                this.view.find("[sid=student_time]").text(parseInt(this.data[i].useTime / 60) + '分' + this.data[i].useTime % 60 + '秒');
            } else {
                this.view.find("[sid=student_status]").text("未查看");
                this.view.find("[sid=student_time]").text("——");
            }
            this.parentView.append(this.view);
        }
    };
})
