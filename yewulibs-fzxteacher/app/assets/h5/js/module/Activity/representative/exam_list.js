/**
 * Created by wuhai on 2018/2/9.
 */
  var ExamListItem = function (parentView, data, rightView ,self) {

    this.parentView = parentView;
    this.rightView = rightView;
    this.parent = self;
    this.data = data;
    this.init();
  };

  ExamListItem.template = ' <div class="list_item"><div class="" sid="exam_item"><span></span></div></div>';

  ExamListItem.prototype.init = function () {
    this.view = $(ExamListItem.template);
    this.view.find("[sid=exam_item]").text(this.data.examSeq + '.' + this.data.examTemplateStyleName);
    this.parentView.append(this.view);
    this.view.off('click').on('click', this, this.handleClick);
  };

  ExamListItem.prototype.handleClick = function (evt) {
    var self = evt.data;
    var param = {};
    // param.lessonTaskDetailsId = 4;
    param.lessonTaskDetailsId = self.data.id;

    $(this).siblings().removeClass("on");
    $(this).addClass("on");
    // param.lessonTaskDetailsId = this.data.lessonTaskDetailsId;
    ApiActivity.getCorrectDetail(param, self, self.getCorrectDetail);
  };
  ExamListItem.prototype.getCorrectDetail = function (flag, res) {
    if (flag) {
      this.rightView.empty();
      if(res.data && res.data.length>0){
        for (var i = 0, len = res.data.length; i < len; i++){
          new StudentListItem(this.rightView, res.data[i],parseInt(i+1),this);
        }
      }else{
        this.rightView.html('<div class="empty-style"></div>');
      }
    }else {
      Information.show(2, '获取典型讲解列表失败');
    }
  };
