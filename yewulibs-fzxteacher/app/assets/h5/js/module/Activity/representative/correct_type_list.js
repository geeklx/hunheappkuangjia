/**
 * Created by wuhai on 2018/2/11.
 */
    var StudentListItem = function (parentView, data, index,self) {
        // this.conView = $('.container');
        this.parentView = parentView;
        this.data = data;
        this.index = index;
        this.parent = self;
        this.showAnswerFlag = false;
        this.init();
    };
    StudentListItem.template = '<div class="exam-answer-item">' +
        '<div class="exam-answer-item-top clearfix" sid="top">' +
        '<div class="l e-a-i-type" sid="type"></div>' +
        '<div class="l e-a-i-name" sid="name"></div>' +
        '<div class="r remove_icon" sid="remove_icon"></div>' +
        '<div class="r look_icon" sid="look_icon"></div>' +
        '</div>' +
        '<div class="exam-answer-item-bot" sid="bot"></div>' +
        '</div>';

    StudentListItem.prototype.init = function () {
        this.view = $(StudentListItem.template);
        // this.view.find('[sid=look_icon]').on('click', this, this.showAnswer);
        var lookView = this.view.find('[sid=look_icon]');
        var nameView = this.view.find('[sid=name]');
        this.view.find('[sid=remove_icon]').on('click', this, this.removeStudent);
        if (this.data.correctType == 2) {
            this.view.find('[sid=type]').addClass('error').text('典型错误');
        } else {
            this.view.find('[sid=type]').addClass('right').text('优秀解答');
        }
        this.view.find("[sid=name]").text('***');
        // var examAnswerObj = new ExamAnswerPreview(d, 10);
        this.examAnswerObj = new ExamAnswerPreview(this.data, this.index + '.');
        this.view.find('[sid=bot]').empty().append(this.examAnswerObj.view);
        this.parentView.append(this.view);
        var that = this;
        lookView.on('click', this, function () {
            open = !open;
            that.updateNameStatus(nameView, open, that.data.userStudentName );
            that.updateLookStatus(lookView, open);
        });
    };

    StudentListItem.prototype.updateNameStatus = function (view, isOpen, userName) {
        if (isOpen)
            view.text(userName + '(' + this.data.score + '分)');
        else
            view.text("***");
    };

    StudentListItem.prototype.updateLookStatus = function (view, isOpen) {
        if (isOpen)
            view.removeClass('shut')
        else
            view.addClass('shut');
    };
    StudentListItem.prototype.showAnswer = function (e) {
        var self = e.data;
        if (self.showAnswerFlag) {
            self.examAnswerObj.hideAnswer();
        } else {
            self.examAnswerObj.showAnswer();
        }
        self.view.find('[sid=look_icon]').toggleClass('active');
        self.showAnswerFlag = !self.showAnswerFlag;
    }

    // var Representative = require('../representative/representative.js');

    StudentListItem.prototype.removeStudent = function (e) {
        var self = e.data;
        console.log(self.data);
        var lessonTaskId = self.data.lessonTaskId;
        console.log(self);
        var param = {};
        param.id = self.data.id;
        param.correctType = 3;
        ApiActivity.updateTypicalCase(param, self, self.updateSList);
        // Representative.(lessonTaskId);
    }
    StudentListItem.prototype.updateSList=function (flag, data) {
        if (flag) {
            alert( "操作成功");
            // Representative.init();
            //  ActivityDetail.prototype.onRefresh(e);
            // window.location.reload();
            this.parent.parent.getExamList();
        }
        else
            alert( data.msg);
    }
