package com.example.app2jrzb.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baijiayun.live.ui.LiveSDKWithUI;
import com.baijiayun.livecore.LiveSDK;
import com.baijiayun.livecore.context.LPConstants;
import com.baijiayun.videoplayer.ui.playback.PBRoomUI;
import com.example.app2jrzb.R;
import com.example.app2jrzb.adapter.LiveTodyAdapter;
import com.example.app2jrzb.presenter.LiveTodyPresenter;
import com.example.app2jrzb.view.LiveTodyView;
import com.example.app2publics.widget.EmptyLayout;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.zbteacher.variants.bean.LiveTodyBean;
import com.sdzn.zbteacher.variants.bean.NewLiveInfo;
import com.sdzn.zbteacher.variants.bean.NewVideoInfo;

import java.util.ArrayList;
import java.util.List;

//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;


public class LiveFragment extends BaseMVPFragment<LiveTodyView, LiveTodyPresenter> implements LiveTodyView, OnRefreshLoadmoreListener, LiveSDKWithUI.LPRoomExitCallback {
    EmptyLayout emptyLayout;
    RecyclerView recyclerVideo;
    SmartRefreshLayout refreshLayout;

    private int pageIndex = 1;//当前页
    private int pageSize = 6;//
    private LiveTodyAdapter todyAdapter;
    private List<LiveTodyBean.RowsBean> mData = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_live;
    }

    public static LiveFragment newInstance() {
        return new LiveFragment();
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initView();
        initData();
    }

    @Override
    protected LiveTodyPresenter createPresenter() {
        return new LiveTodyPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void initView() {
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        recyclerVideo = rootView.findViewById(R.id.swipe_target);
        refreshLayout = rootView.findViewById(R.id.refresh_layout);

        todyAdapter = new LiveTodyAdapter(mContext, mData);
        recyclerVideo.setLayoutManager(new LinearLayoutManager(mContext));
//        recyclerVideo.setItemAnimator(new DefaultItemAnimator());//
        recyclerVideo.setAdapter(todyAdapter);
//        todyAdapter.setOnItemClickListener(this);

        refreshLayout.setOnRefreshLoadmoreListener(this);

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
                goneSwipView();
            }
        });
        todyAdapter.setListener(new LiveTodyAdapter.LiveCourseTodayListener() {
            @Override
            public void toLiving(String kpointId) {
                mPresenter.getLivingInfo(kpointId);

            }

            @Override
            public void toReplay(String kpointId, String courseId) {
                mPresenter.getReplayInfo(kpointId, courseId);
            }

        });

    }

    private void initData() {
        mPresenter.getLiveCourseList(pageIndex, pageSize);
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        initData();
    }

    @Override
    public void listCourseSuccess(List<LiveTodyBean.RowsBean> list) {
        if (list != null) {
            if (pageIndex == 1) {
                mData.clear();
            }
            mData.addAll(list);
            todyAdapter.notifyDataSetChanged();
            refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        }
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        goneSwipView();
    }

    @Override
    public void listCourseEmpty() {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            refreshLayout.setLoadmoreFinished(true);
        }
        goneSwipView();
    }

    @Override
    public void listCourseError(String msg) {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }
        goneSwipView();
    }

    @Override
    public void getLiveInfoSuccess(NewLiveInfo liveInfo) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        LPConstants.LPUserType TYPE = null;

        if ("1".equals(liveInfo.getUser_info().getUserType())) {
            TYPE = LPConstants.LPUserType.Teacher;
        } else if ("2".equals(liveInfo.getUser_info().getUserType())) {
            TYPE = LPConstants.LPUserType.Assistant;
        } else if ("3".equals(liveInfo.getUser_info().getUserType())) {
            TYPE = LPConstants.LPUserType.Visitor;
        } else {
            TYPE = LPConstants.LPUserType.Student;
        }

        LiveSDKWithUI.enterRoom(mContext, Long.valueOf(liveInfo.getRoom_id().trim()), liveInfo.getSign(), new LiveSDKWithUI.LiveRoomUserModel(liveInfo.getUser_info().getUserName(), liveInfo.getUser_info().getUserAvatar(), String.valueOf(liveInfo.getUser_info().getUserNumber()), TYPE), new LiveSDKWithUI.LiveSDKEnterRoomListener() {
            @Override
            public void onError(String msg) {
                ToastUtils.showShort(msg);
            }
        });
//        LiveSDKWithUI.setRoomExitListener(new LiveSDKWithUI.LPRoomExitListener() {
//                @Override
//                public void onRoomExit(Context context, LiveSDKWithUI.LPRoomExitCallback lpRoomExitCallback) {
//
//                }
//            });
    }

    @Override
    public void exit() {
        ToastUtils.showShort("-------------exit");
    }

    @Override
    public void cancel() {
        ToastUtils.showShort("-------------exit");
    }

    @Override
    public void getReplayInfoSuccess(NewVideoInfo videoInfo) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        PBRoomUI.enterPBRoom(getActivity(), videoInfo.getRoomId(), videoInfo.getToken(), "0", new PBRoomUI.OnEnterPBRoomFailedListener() {
            @Override
            public void onEnterPBRoomFailed(String s) {

            }
        });
    }

    @Override
    public void getInfoFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }
}
