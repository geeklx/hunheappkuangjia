package com.example.app2jrzb.presenter;

import android.text.TextUtils;

import com.example.app2jrzb.R;
import com.example.app2jrzb.view.LiveTodyView;
import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.zbteacher.variants.bean.LiveTodyBean;
import com.sdzn.zbteacher.variants.bean.NewLiveInfo;
import com.sdzn.zbteacher.variants.bean.NewVideoInfo;
import com.sdzn.zbteacher.variants.bean.ResultBean;
import com.sdzn.zbteacher.variants.network.RestApi;
import com.sdzn.zbteacher.variants.network.api.CourseService;
import com.sdzn.zbteacher.variants.network.api.ResponseFunc;
import com.sdzn.zbteacher.variants.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

public class LiveTodyPresenter extends BasePresenter<LiveTodyView> {
    public void getLiveCourseList(int pageIndex, int pageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "4");
        map.put("index", String.valueOf(pageIndex));
        map.put("size", String.valueOf(pageSize));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscription = RestApi.getInstance()
                .create(CourseService.class)
                .getLiveTodayInfo(requestBody)
                .compose(TransformUtils.<ResultBean<LiveTodyBean>>defaultSchedulers())
                .map(new ResponseFunc<LiveTodyBean>())
                .subscribe(new MProgressSubscriber<LiveTodyBean>(new SubscriberOnNextListener<LiveTodyBean>() {
                    @Override
                    public void onNext(LiveTodyBean todyResult) {
                        if (todyResult != null && todyResult.getRows() != null && !todyResult.getRows().isEmpty()) {
                            getView().listCourseSuccess(todyResult.getRows());
                        } else {
                            getView().listCourseEmpty();
                        }

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().listCourseError(msg);
                        getView().getInfoFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }

    public void getCourseList(int pageIndex, int pageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "3");
        map.put("index", String.valueOf(pageIndex));
        map.put("size", String.valueOf(pageSize));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscription = RestApi.getInstance()
                .create(CourseService.class)
                .getLiveTodayInfo(requestBody)
                .compose(TransformUtils.<ResultBean<LiveTodyBean>>defaultSchedulers())
                .map(new ResponseFunc<LiveTodyBean>())
                .subscribe(new MProgressSubscriber<LiveTodyBean>(new SubscriberOnNextListener<LiveTodyBean>() {
                    @Override
                    public void onNext(LiveTodyBean todyResult) {
                        if (todyResult != null && todyResult.getRows() != null && !todyResult.getRows().isEmpty()) {
                            getView().listCourseSuccess(todyResult.getRows());
                        } else {
                            getView().listCourseEmpty();
                        }

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().listCourseError(msg);
                        getView().getInfoFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }

    public void getLivingInfo(String kpointId) {
        Map<String, String> map = new HashMap<>();
        map.put("kpointId", kpointId);
        map.put("channel", "2");
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .create(CourseService.class)
                .getNewLiveInfo(requestBody)
                .compose(TransformUtils.<ResultBean<NewLiveInfo>>defaultSchedulers())
                .map(new ResponseFunc<NewLiveInfo>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewLiveInfo>() {
                    @Override
                    public void onNext(NewLiveInfo courses) {
                        if (courses != null) {
                            getView().getLiveInfoSuccess(courses);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().getInfoFailed("" + e.getMessage());

                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

    /**
     * 回放
     *
     * @param kpointId
     */
    public void getReplayInfo(String kpointId, String courseId) {
        Map<String, String> map = new HashMap<>();
        map.put("kpointId", kpointId);
        map.put("courseId", courseId);
        map.put("channel", "2");
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .create(CourseService.class)
                .getNewReplayInfo(requestBody)
                .compose(TransformUtils.<ResultBean<NewVideoInfo>>defaultSchedulers())
                .map(new ResponseFunc<NewVideoInfo>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewVideoInfo>() {
                    @Override
                    public void onNext(NewVideoInfo courses) {
                        if (courses != null) {
                            getView().getReplayInfoSuccess(courses);
                        } else {
                            ToastUtils.showShort("回放教室不存在或已删除");
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().getInfoFailed("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }


}
