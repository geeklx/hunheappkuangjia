package com.sdzn.zbteacher.variants.bean;

public class LiveTodyResult {

    /**
     * code : 0
     * errorCode : 0
     * message :
     * result : {"total":10,"rows":[{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频可删","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":14952,"liveBeginTime":"2020-02-25 13:00:20","courseId":1510,"liveStates":"4","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频2","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":12411,"liveBeginTime":"2020-02-16 17:02:12","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频1","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":12310,"liveBeginTime":"2020-02-16 15:20:29","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":11677,"liveBeginTime":"2020-02-15 09:50:12","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":11446,"liveBeginTime":"2020-02-14 18:15:29","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8954,"liveBeginTime":"2020-02-06 16:16:57","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"测试课程","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8254,"liveBeginTime":"2020-02-05 13:51:49","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8032,"liveBeginTime":"2020-02-04 23:18:09","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"测试课程2","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8031,"liveBeginTime":"2020-02-04 22:33:17","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"直播测试课程","coursePageBuyCount":1101,"kpointTitle":"测试课1","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/gaozhong2yuwen.jpg","kpointId":7099,"liveBeginTime":"2020-02-03 16:54:01","courseId":1177,"liveStates":"6","content":""}]}
     * success : true
     */

    private int code;
    private int errorCode;
    private String message;
    private LiveTodyBean result;
    private boolean success;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;

    }

    public LiveTodyBean getResult() {
        return result;
    }

    public void setResult(LiveTodyBean result) {
        this.result = result;
    }
}
