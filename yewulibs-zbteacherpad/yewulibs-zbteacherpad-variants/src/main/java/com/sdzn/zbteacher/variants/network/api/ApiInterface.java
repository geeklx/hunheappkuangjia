package com.sdzn.zbteacher.variants.network.api;


import com.sdzn.pkt.teacher.hd.BuildConfig3;

/**
 * 描述：API请求地址
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ApiInterface {
    public static final String BASE_URL = BuildConfig3.BASE_ADDRESS;

    public static final String TEACHER_COURSE = "eduLive/api/course/teacherCourse";//今日直播列表
    public static final String USER_LOGIN = "teacher/login/token";//老师助教登录 入口
    public static final String USER_LOGIN_OUT = "teacher/logout/token";//

    public static final String QUERY_VERSION_INFO = "/usercenter/sysVersionAppInfo/selectOne";//查询版本信息

    public static final String ENTER_LIVE_ROOM = "eduLive/api/live/enterLiveRoom";//直播
    public static final String VIEW_PLAY_BACK = "eduLive/api/live/viewPlayBack";//回放

}
