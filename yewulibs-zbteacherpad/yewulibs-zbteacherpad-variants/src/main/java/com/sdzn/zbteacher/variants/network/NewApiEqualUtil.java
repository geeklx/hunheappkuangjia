package com.sdzn.zbteacher.variants.network;


import com.sdzn.pkt.teacher.hd.BuildConfig3;
import com.sdzn.zbteacher.variants.network.api.ApiInterface;

/**
 * 是否跑新接口
 */

public class NewApiEqualUtil {
    public static boolean isEqual(String url) {
        if (url.equals(BuildConfig3.BASE_ADDRESS + "/" + ApiInterface.USER_LOGIN) ||
                url.equals(BuildConfig3.BASE_ADDRESS + "/" + ApiInterface.QUERY_VERSION_INFO)) {
            return false;
        }

        return true;
    }

}
