package com.sdzn.zbteacher.variants.network.api;


import com.sdzn.zbteacher.variants.bean.AccountBean;
import com.sdzn.zbteacher.variants.bean.ResultBean;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * 描述：
 * - 账户相关接口
 * 创建人：baoshengxiang
 * 创建时间：2017/7/13
 */
public interface AccountService {
    String BASE_URL = ApiInterface.BASE_URL;

    //登录

    @FormUrlEncoded
    @POST(ApiInterface.USER_LOGIN)
    Observable<ResultBean<AccountBean>> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST(ApiInterface.USER_LOGIN_OUT)
    Observable<ResultBean<Object>> loginout(@Field("token") String token);


}
