package com.sdzn.zbteacher.variants.bean;

import java.util.List;

public class LiveTodyBean {

    /**
     * total : 10
     * rows : [{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频可删","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":14952,"liveBeginTime":"2020-02-25 13:00:20","courseId":1510,"liveStates":"4","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频2","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":12411,"liveBeginTime":"2020-02-16 17:02:12","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频1","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":12310,"liveBeginTime":"2020-02-16 15:20:29","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":11677,"liveBeginTime":"2020-02-15 09:50:12","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":11446,"liveBeginTime":"2020-02-14 18:15:29","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8954,"liveBeginTime":"2020-02-06 16:16:57","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"测试课程","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8254,"liveBeginTime":"2020-02-05 13:51:49","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"新创建视频","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8032,"liveBeginTime":"2020-02-04 23:18:09","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"智囊高中测试课","coursePageBuyCount":13,"kpointTitle":"测试课程2","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg","kpointId":8031,"liveBeginTime":"2020-02-04 22:33:17","courseId":1510,"liveStates":"6","content":""},{"courseTitle":"直播测试课程","coursePageBuyCount":1101,"kpointTitle":"测试课1","liveUrl":null,"logo":"http://admin.znclass.com/images/upload/course/20171221/gaozhong2yuwen.jpg","kpointId":7099,"liveBeginTime":"2020-02-03 16:54:01","courseId":1177,"liveStates":"6","content":""}]
     */


    private List<RowsBean> rows;

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * courseTitle : 智囊高中测试课
         * coursePageBuyCount : 13
         * kpointTitle : 新创建视频可删
         * liveUrl : null
         * logo : http://admin.znclass.com/images/upload/course/20171221/xiaoxueyuwen.jpg
         * kpointId : 14952
         * liveBeginTime : 2020-02-25 13:00:20
         * courseId : 1510
         * liveStates : 4
         * content :
         */

        private String courseTitle;
        private String kpointTitle;
        private String logo;
        private String kpointId;
        private String liveBeginTime;
        private String liveEndTime;
        private String courseId;
        private String liveStates;
        private String content;


        public String getLiveEndTime() {
            if (liveEndTime!=null) {
                return liveEndTime;
            }
            return "";
        }

        public void setLiveEndTime(String liveEndTime) {
            this.liveEndTime = liveEndTime;
        }

        public String getCourseTitle() {
            return courseTitle;
        }

        public void setCourseTitle(String courseTitle) {
            this.courseTitle = courseTitle;
        }

        public String getKpointTitle() {
            return kpointTitle;
        }

        public void setKpointTitle(String kpointTitle) {
            this.kpointTitle = kpointTitle;
        }


        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getKpointId() {
            return kpointId;
        }

        public void setKpointId(String kpointId) {
            this.kpointId = kpointId;
        }

        public String getLiveBeginTime() {
            if (liveBeginTime!=null) {

                return liveBeginTime;
            }
            return "";
        }

        public void setLiveBeginTime(String liveBeginTime) {
            this.liveBeginTime = liveBeginTime;
        }

        public String getCourseId() {





            return courseId;
        }

        public void setCourseId(String courseId) {
            this.courseId = courseId;
        }

        public String getLiveStates() {
            return liveStates;
        }

        public void setLiveStates(String liveStates) {
            this.liveStates = liveStates;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
