package com.sdzn.zbteacher.variants.bean;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class AccountBean implements Serializable {


    /**
     * access_token : cb06b2c1-21df-4610-8a73-96a85faacf35
     * domain : @teacher.com
     * expires_in : 42261
     * openid : 2524
     * scope : ["userProfile"]
     * token_type : bearer
     * userDetail : {"createTime":"2021-01-29 11:25:07","updateTime":"2021-01-29 11:25:07","id":"2524","name":"黄东月测试","sort":1,"schoolId":170,"gender":2,"status":1,"isDelete":0,"deleteTime":"","levelId":2,"education":"","career":"","introduction":"","certificateNo":"","certificateImage":"","avatar":"http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png","isShow":0,"isSchoolmaster":0,"account":"61363572","mobile":"","email":"","qq":"","weixin":"","freeznTime":"","classManager":"","password":"123456","schoolName":"sdzn","levelName":"初中","schoolTeacherClasses":[],"isFamous":0,"liveSubjectId":0,"liveSubjectName":"","isTypicalMistakes":0,"isGoodAnswer":1,"famousDesc":"","role":5,"genderName":"女","educationId":0}
     */

    private String access_token;
    private String domain;
    private int expires_in;
    private String openid;
    private String token_type;
    private UserDetailBean userDetail;
    private List<String> scope;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public UserDetailBean getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailBean userDetail) {
        this.userDetail = userDetail;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }

    public static class UserDetailBean {
        /**
         * createTime : 2021-01-29 11:25:07
         * updateTime : 2021-01-29 11:25:07
         * id : 2524
         * name : 黄东月测试
         * sort : 1
         * schoolId : 170
         * gender : 2
         * status : 1
         * isDelete : 0
         * deleteTime :
         * levelId : 2
         * education :
         * career :
         * introduction :
         * certificateNo :
         * certificateImage :
         * avatar : http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png
         * isShow : 0
         * isSchoolmaster : 0
         * account : 61363572
         * mobile :
         * email :
         * qq :
         * weixin :
         * freeznTime :
         * classManager :
         * password : 123456
         * schoolName : sdzn
         * levelName : 初中
         * schoolTeacherClasses : []
         * isFamous : 0
         * liveSubjectId : 0
         * liveSubjectName :
         * isTypicalMistakes : 0
         * isGoodAnswer : 1
         * famousDesc :
         * role : 5
         * genderName : 女
         * educationId : 0
         */

        private String createTime;
        private String updateTime;
        private String id;
        private String name;
        private int sort;
        private int schoolId;
        private int gender;
        private int status;
        private int isDelete;
        private String deleteTime;
        private int levelId;
        private String education;
        private String career;
        private String introduction;
        private String certificateNo;
        private String certificateImage;
        private String avatar;
        private int isShow;
        private int isSchoolmaster;
        private String account;
        private String mobile;
        private String email;
        private String qq;
        private String weixin;
        private String freeznTime;
        private String classManager;
        private String password;
        private String schoolName;
        private String levelName;
        private int isFamous;
        private int liveSubjectId;
        private String liveSubjectName;
        private int isTypicalMistakes;
        private int isGoodAnswer;
        private String famousDesc;
        private int role;
        private String genderName;
        private int educationId;
        private List<?> schoolTeacherClasses;

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public int getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(int schoolId) {
            this.schoolId = schoolId;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(int isDelete) {
            this.isDelete = isDelete;
        }

        public String getDeleteTime() {
            return deleteTime;
        }

        public void setDeleteTime(String deleteTime) {
            this.deleteTime = deleteTime;
        }

        public int getLevelId() {
            return levelId;
        }

        public void setLevelId(int levelId) {
            this.levelId = levelId;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getCareer() {
            return career;
        }

        public void setCareer(String career) {
            this.career = career;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getCertificateNo() {
            return certificateNo;
        }

        public void setCertificateNo(String certificateNo) {
            this.certificateNo = certificateNo;
        }

        public String getCertificateImage() {
            return certificateImage;
        }

        public void setCertificateImage(String certificateImage) {
            this.certificateImage = certificateImage;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getIsShow() {
            return isShow;
        }

        public void setIsShow(int isShow) {
            this.isShow = isShow;
        }

        public int getIsSchoolmaster() {
            return isSchoolmaster;
        }

        public void setIsSchoolmaster(int isSchoolmaster) {
            this.isSchoolmaster = isSchoolmaster;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getWeixin() {
            return weixin;
        }

        public void setWeixin(String weixin) {
            this.weixin = weixin;
        }

        public String getFreeznTime() {
            return freeznTime;
        }

        public void setFreeznTime(String freeznTime) {
            this.freeznTime = freeznTime;
        }

        public String getClassManager() {
            return classManager;
        }

        public void setClassManager(String classManager) {
            this.classManager = classManager;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getLevelName() {
            return levelName;
        }

        public void setLevelName(String levelName) {
            this.levelName = levelName;
        }

        public int getIsFamous() {
            return isFamous;
        }

        public void setIsFamous(int isFamous) {
            this.isFamous = isFamous;
        }

        public int getLiveSubjectId() {
            return liveSubjectId;
        }

        public void setLiveSubjectId(int liveSubjectId) {
            this.liveSubjectId = liveSubjectId;
        }

        public String getLiveSubjectName() {
            return liveSubjectName;
        }

        public void setLiveSubjectName(String liveSubjectName) {
            this.liveSubjectName = liveSubjectName;
        }

        public int getIsTypicalMistakes() {
            return isTypicalMistakes;
        }

        public void setIsTypicalMistakes(int isTypicalMistakes) {
            this.isTypicalMistakes = isTypicalMistakes;
        }

        public int getIsGoodAnswer() {
            return isGoodAnswer;
        }

        public void setIsGoodAnswer(int isGoodAnswer) {
            this.isGoodAnswer = isGoodAnswer;
        }

        public String getFamousDesc() {
            return famousDesc;
        }

        public void setFamousDesc(String famousDesc) {
            this.famousDesc = famousDesc;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getGenderName() {
            return genderName;
        }

        public void setGenderName(String genderName) {
            this.genderName = genderName;
        }

        public int getEducationId() {
            return educationId;
        }

        public void setEducationId(int educationId) {
            this.educationId = educationId;
        }

        public List<?> getSchoolTeacherClasses() {
            return schoolTeacherClasses;
        }

        public void setSchoolTeacherClasses(List<?> schoolTeacherClasses) {
            this.schoolTeacherClasses = schoolTeacherClasses;
        }
    }
}
