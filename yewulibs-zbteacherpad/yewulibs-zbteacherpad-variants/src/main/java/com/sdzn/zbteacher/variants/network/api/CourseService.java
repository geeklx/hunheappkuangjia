package com.sdzn.zbteacher.variants.network.api;


import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.sdzn.pkt.teacher.hd.BuildConfig3;
import com.sdzn.zbteacher.variants.bean.LiveTodyBean;
import com.sdzn.zbteacher.variants.bean.NewLiveInfo;
import com.sdzn.zbteacher.variants.bean.NewVideoInfo;
import com.sdzn.zbteacher.variants.bean.ResultBean;
import com.sdzn.zbteacher.variants.bean.VersionInfoBean;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * 描述：
 * - 课程相关接口
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public interface CourseService {

//    String BASE_URL = ApiInterface.BASE_URL;


//    @FormUrlEncoded
//    @POST(ApiInterface.UP_COLLECT)
//    Observable<ResultBean<CollectBean>> upDataCollection(@Field("currentPage") int currentPage, @Field("pageSize") int pageSize);


    //检查更新
  /*  @FormUrlEncoded
    @POST(ApiInterface.QUERY_VERSION_INFO)
    Observable<ResultBean<VersionInfo>> queryVersion(@Field("type") int type);*/
    // 检查更新
//    @Headers({"Content-Type: application/json", "Accept: application/json"})

    @FormUrlEncoded
    @POST(BuildConfig3.BASE_ADDRESS + ApiInterface.QUERY_VERSION_INFO)
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Field("programId") String programId, @Field("type") String type);//@Body RequestBody body@Header("Authorization") String token,


    //课程列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.TEACHER_COURSE)
    Observable<ResultBean<LiveTodyBean>> getLiveTodayInfo(@Body RequestBody body);


    //直播项目
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ENTER_LIVE_ROOM)
    Observable<ResultBean<NewLiveInfo>> getNewLiveInfo(@Body RequestBody body);

    //回放
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.VIEW_PLAY_BACK)
    Observable<ResultBean<NewVideoInfo>> getNewReplayInfo(@Body RequestBody body);

}

