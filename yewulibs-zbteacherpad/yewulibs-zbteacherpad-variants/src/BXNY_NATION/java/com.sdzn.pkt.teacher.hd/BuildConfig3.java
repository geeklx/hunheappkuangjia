package com.sdzn.pkt.teacher.hd;

import com.sdzn.zbteacher.variants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.YYY;

    public static final String FLAVOR = UrlManager.FLAVOR2;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE2;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME2;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS2;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL2;
    public static final String IMAGE_CACHE = UrlManager.IMAGE_CACHE2;
    public static final String ROOT_CACHE = UrlManager.ROOT_CACHE2;
    public static final String APP_DOWNLOAD = UrlManager.APP_DOWNLOAD2;
    public static final String AVATAR_CACHE = UrlManager.AVATAR_CACHE2;
    public static final String CRASH_CACHE = UrlManager.CRASH_CACHE2;

}
