package com.example.app2welcome;

import android.content.Context;
import android.content.Intent;

import com.example.app2welcome.activity.LoginActivity;
import com.example.app2welcome.activity.MainActivity;
import com.example.app2welcome.activity.WebActivity;
import com.example.app2welcome.activity.WelcomeActivity;

import static com.example.app2welcome.activity.MainActivity.AUTO_LOGIN;

public class IntentController {
    public static void toMain(Context context, boolean autoLogin) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(AUTO_LOGIN, autoLogin);
        context.startActivity(intent);
    }

    public static void toLogin(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    public static void toWelcome(Context context) {
        Intent intent = new Intent(context, WelcomeActivity.class);
        context.startActivity(intent);
    }

    public static void toWeb(Context context, String type) {
        Intent changeNameIntent = new Intent(context, WebActivity.class);
        changeNameIntent.putExtra(WebActivity.INTENT_WEB, type);
        context.startActivity(changeNameIntent);
    }
}
