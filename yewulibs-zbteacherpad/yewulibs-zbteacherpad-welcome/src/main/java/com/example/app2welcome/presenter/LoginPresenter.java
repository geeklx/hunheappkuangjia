package com.example.app2welcome.presenter;

import android.content.res.Configuration;
import android.text.TextUtils;

import com.example.app2welcome.R;
import com.example.app2welcome.view.LoginView;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.func.ExceptionFunc;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.zbteacher.variants.bean.AccountBean;
import com.sdzn.zbteacher.variants.bean.ResultBean;
import com.sdzn.zbteacher.variants.network.RestApi;
import com.sdzn.zbteacher.variants.network.api.AccountService;
import com.sdzn.zbteacher.variants.network.api.ResponseFunc;
import com.sdzn.zbteacher.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class LoginPresenter extends BasePresenter<LoginView> {

/*
    public void checkVerion() {
        Subscription subscribe = RestApi.getInstance()
                .create( CourseService.class)
                .queryVersion("1","2")
                .compose(TransformUtils.<ResultBean<VersionInfo>>defaultSchedulers())
                .map(new ResponseFunc<VersionInfo>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<VersionInfo>() {
                    @Override
                    public void onNext(VersionInfo versionInfoBean) {
                        int currVersion = App2Utils.getAppVersionCode(App2.get());
//                        VersionInfoBean versionInfo = versionInfoBean.getVersionInfo();
                        if (versionInfoBean.getVersionNumber() > currVersion) {//此处为
                            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                                getView().updateVersion(versionInfoBean.getVersionInfo(), versionInfoBean.getTargetUrl());
                            }
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        getView().loginFailure(e.getMessage() + "");
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
*/

    public void login(final String username, String password) {
        Subscription subscribe = RestApi.getInstance()
                .create(AccountService.class)
                .login(username, password)
                .compose(TransformUtils.<ResultBean<AccountBean>>defaultSchedulers())
                .map(new ResponseFunc<AccountBean>())
                .onErrorResumeNext(new ExceptionFunc<AccountBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<AccountBean>() {
                    @Override
                    public void onNext(AccountBean userBean) {
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }


    /**
     * 判断当前设备是手机还是平板，代码来自 Google I/O App for Android
     *
     * @return 平板返回 True，手机返回 False
     */
    private boolean isPad() {
        return (App2.get().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
