package com.example.app2grzx.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.AppUtils;
import com.example.app2grzx.R;
import com.example.app2publics.widget.radioview.FragmentTabUtils;
import com.example.app2publics.widget.radioview.RadioLayout;
import com.example.app2publics.widget.radioview.RadioView;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.network.func.ExceptionFunc;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.zbteacher.variants.bean.ResultBean;
import com.sdzn.zbteacher.variants.manager.SPManager;
import com.sdzn.zbteacher.variants.network.RestApi;
import com.sdzn.zbteacher.variants.network.api.AccountService;
import com.sdzn.zbteacher.variants.network.api.ResponseFunc;
import com.sdzn.zbteacher.variants.network.subscriber.MProgressSubscriber;

import java.util.ArrayList;

import rx.Subscription;


//import android.support.v4.app.Fragment;

/**
 * 系统设置
 *
 * @author Reisen at 2017-12-08
 */

public class SystemSettingFragment extends BaseFragment {
    RadioLayout mRadioLayout;
    RadioView mRadioView;
    RadioView mOut;

    private ArrayList<Fragment> mFragments;

    public static SystemSettingFragment newInstance() {
        return new SystemSettingFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_systemsetting;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        initView();
    }

    private void initData() {
        mFragments = new ArrayList<>();
//        mFragments.add(SettingNetworkFragment.newInstance());
        mFragments.add(SettingCacheFragment.newInstance());
        mFragments.add(SettingVersionFragment.newInstance());
        mFragments.add(SettingWebFragment.newInstance("1"));
        mFragments.add(SettingWebFragment.newInstance("2"));

        mFragments.add(SettingAboutFragment.newInstance());

    }

    private void initView() {
        mRadioLayout = rootView.findViewById(R.id.rl_system_radiolayout);
        mRadioView = rootView.findViewById(R.id.rv_cache);
        mOut = rootView.findViewById(R.id.rv_out);
        FragmentTabUtils mTabUtils = new FragmentTabUtils(mContext, getActivity().getSupportFragmentManager(),
                mFragments, R.id.system_container, mRadioLayout, 0, true, false);
        mTabUtils.setNeedAnimation(true);
        mRadioView.setChecked(true);

        mOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showExitDialog();

            }
        });

    }

    private void showExitDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定要退出登录么？").setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
            @Override
            public void onClick(Dialog dialog, int which) {
                logout();
            }
        }).setNegativeButton("取消", new SweetAlertDialog.OnDialogClickListener() {
            @Override
            public void onClick(Dialog dialog, int which) {
                //隐藏dialog
            }
        });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }

    private void logout() {
        Subscription subscribe = RestApi.getInstance()
                .create(AccountService.class)
                .loginout(SPManager.getToken())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseFunc<Object>())
                .onErrorResumeNext(new ExceptionFunc<Object>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtil.showShortlToast(msg);
                    }
                }, getActivity()));

        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.login");
        startActivity(intent);

    }


}
