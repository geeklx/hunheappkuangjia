package com.sdzn.pkt.teacher.hd;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.StrictMode;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.baijiayun.BJYPlayerSDK;
import com.example.app2publics.utils.App2Utils;
import com.example.app2publics.widget.ProgressHeader;
import com.example.app2welcome.activity.MainActivity;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.interfaces.BetaPatchListener;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.unit.Subunits;
import update.UpdateAppUtils;

import static com.tencent.bugly.beta.tinker.TinkerManager.getApplication;

//import android.support.annotation.NonNull;
//import android.support.multidex.MultiDex;

/**
 * zs
 */
public class MyApplication extends MultiDexApplication {
    public static MyApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        UpdateAppUtils.init(this);//初始化软件更新
        configRetrofitNet();
        initRefreshFram();
        initBjy();
        initBugly();
        configShipei();
    }

    private void configShipei() {
        AutoSizeConfig.getInstance().getUnitsManager()
                .setSupportDP(false)
                .setSupportSP(false)
                .setSupportSubunits(Subunits.MM);
        AutoSize.initCompatMultiProcess(mContext);
    }
//    //网络请求
//    private void initOkHttp(){
//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
////                .addInterceptor(new LoggerInterceptor("TAG"))
//                .connectTimeout(5, TimeUnit.SECONDS)
//                .readTimeout(8, TimeUnit.SECONDS)
//                //其他配置
//                .build();
//initCrashReport
//        OkHttpUtils.initClient(okHttpClient);
//    }

    private void initBjy() {
        new BJYPlayerSDK.Builder(this)
                .setDevelopMode(false)
                .setCustomDomain("b96152240")
                .build();
    }
    private void configRetrofitNet() {
        RetrofitNetNew.config();
    }

    private void initBugly() {
        Beta.upgradeDialogLayoutId = R.layout.dialog_update;
        setStrictMode();
        // 设置是否开启热更新能力，默认为true
        Beta.enableHotfix = true;
        // 设置是否自动下载补丁
        Beta.canAutoDownloadPatch = true;
        // 设置是否提示用户重启
        Beta.canNotifyUserRestart = false;
        // 设置是否自动合成补丁
        Beta.canAutoPatch = true;
        /**
         * 补丁回调接口，可以监听补丁接收、下载、合成的回调
         //         */
        Beta.betaPatchListener = new BetaPatchListener() {
            /*补丁下载地址*/
            @Override
            public void onPatchReceived(String patchFileUrl) {
//                Toast.makeText(getApplication(), patchFileUrl, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDownloadReceived(long savedLength, long totalLength) {
//                Toast.makeText(getApplication(), String.format(Locale.getDefault(),
//                        "%s %d%%",
//                        Beta.strNotificationDownloading,
//                        (int) (totalLength == 0 ? 0 : savedLength * 100 / totalLength)), Toast.LENGTH_SHORT).show();
            }

            /*补丁下载成功*/
            @Override
            public void onDownloadSuccess(String patchFilePath) {
//                Toast.makeText(getApplication(), patchFilePath, Toast.LENGTH_SHORT).show();
                Beta.applyDownloadedPatch();
            }

            /*补丁下载失败*/
            @Override
            public void onDownloadFailure(String msg) {
                Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show();
            }

            /*补丁应用成功*/
            @Override
            public void onApplySuccess(String msg) {
//                Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show();
            }

            /*补丁应用失败*/
            @Override
            public void onApplyFailure(String msg) {
                Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show();
            }

            /*补丁回滚*/
            @Override
            public void onPatchRollback() {
//                Toast.makeText(getApplication(), "onPatchRollback", Toast.LENGTH_SHORT).show();
            }
        };

        String packageName = App2Utils.getAppPackageName(mContext);
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
        strategy.setAppVersion(App2Utils.getAppVersionName(mContext));

        strategy.setAppPackageName(packageName);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));

        //正式发布 false    测试true
//        CrashReport.initCrashReport(this, "f94ba11440", true,strategy);
        Bugly.init(getApplicationContext(), "a5cdc01a5f", true, strategy);

    }

    @TargetApi(9)
    protected void setStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
    }


    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    //设置全局的Header构建器
    private void initRefreshFram() {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater(new DefaultRefreshHeaderCreater() {
            @NonNull
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                return new ProgressHeader(context).setSpinnerStyle(SpinnerStyle.Translate);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
        // 安装tinker
        Beta.autoInit = true;
        Beta.canShowUpgradeActs.add(MainActivity.class);
        Beta.installTinker();
    }
}

