package com.example.app4pkt.activity;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4pkt.R;
import com.example.app4pkt.presenter.CourseTopicPresenter;
import com.example.app4pkt.view.CourseTopicView;
import com.example.app4public.adapter.SpellingTitleAdapter;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.variants.bean.SubjectBean;
import com.example.app4pkt.fragment.CourseTopicContentFragment;
import com.example.app4public.widget.TitleBar;
import com.example.app4public.widget.pager.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;



/**
 * 专题课程
 */

public class CourseTopicActivity extends BaseMVPActivity1<CourseTopicView, CourseTopicPresenter>
        implements CourseTopicView {
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    ViewPager viewPager;
    TitleBar titleBar;

    private SpellingTitleAdapter spellingTitleAdapter;
    private List<Fragment> spellListFragment = new ArrayList<>();
    private ArrayList<String> listTitle = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_topic;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入CourseTopicActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.vp_pager);

        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPToken.autoLogin(mContext)) {
                     Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
                    return;
                }
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(startIntent);
            }
        });
        loadData();
    }

    @Override
    protected CourseTopicPresenter createPresenter() {
        return new CourseTopicPresenter();
    }

    private void loadData() {
        spellingTitleAdapter = new SpellingTitleAdapter(getSupportFragmentManager());
        mPresenter.getTopicTitle();
    }


    @Override
    public void onTopicSuccess(List<SubjectBean> subjectList) {
        this.listTitle.clear();
        this.spellListFragment.clear();
        spellListFragment.add(CourseTopicContentFragment.newInstance("-1"));
        listTitle.add("全部");

        for (int i = 0; i < subjectList.size(); i++) {
            spellListFragment.add(CourseTopicContentFragment.newInstance(String.valueOf(subjectList.get(i).getSubjectId())));
            listTitle.add(subjectList.get(i).getSubjectName());
        }
        spellingTitleAdapter.setmDatas(listTitle, spellListFragment);
        viewPager.setAdapter(spellingTitleAdapter);
        viewPager.setCurrentItem(0);
        mPagerSlidingTabStrip.setViewPager(viewPager);
        viewPager.setOffscreenPageLimit(0);

        mPagerSlidingTabStrip.clearConfigSet();
    }

    @Override
    public void onTopicFailed(String msg) {

    }
}
