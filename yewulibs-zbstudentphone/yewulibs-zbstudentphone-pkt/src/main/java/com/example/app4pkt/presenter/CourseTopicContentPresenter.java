package com.example.app4pkt.presenter;

import com.example.app4pkt.view.SpellingContentView;
import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.variants.bean.CourseListBean;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewSchoolFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

public class CourseTopicContentPresenter extends BasePresenter<SpellingContentView> {

    public void getCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTopicCourse(requestBody)
                .compose(TransformUtils.<ResultBean<CourseListBean>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<CourseListBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseListBean>() {
                    @Override
                    public void onNext(CourseListBean courses) {
                        if (courses != null) {
                            getView().getDataCourse(courses.getList().getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().onFailed("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
