package com.example.app4pkt.presenter;

import android.text.TextUtils;

import com.example.app4pkt.R;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.TopicBean;
import com.example.app4pkt.view.CourseTopicView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

/**
 * 专题课程列表
 */

public class CourseTopicPresenter extends BasePresenter<CourseTopicView> {

    public void getTopicTitle() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTopicSpell(1)
                .compose(TransformUtils.<ResultBean<TopicBean>>defaultSchedulers())
                .map(new ResponseNewFunc<TopicBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<TopicBean>() {
                    @Override
                    public void onNext(TopicBean topicBean) {
                        if (topicBean!=null&&topicBean.getRecords().size()>0) {
                            getView().onTopicSuccess(topicBean.getRecords());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onTopicFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
