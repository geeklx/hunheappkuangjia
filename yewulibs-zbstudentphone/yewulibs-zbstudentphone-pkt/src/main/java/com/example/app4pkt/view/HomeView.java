package com.example.app4pkt.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.BannerInfoBean;
import com.sdzn.variants.bean.CourseList;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public interface HomeView extends BaseView {


    void getDataFailure(String msg);

    void getCourseEmpty();

    void getDataCourse(List<CourseList> recommendCourses);

    void getSubjectDataCourse(List<CourseList> subjectCourses);

    void getBannerData(List<BannerInfoBean> infoBeanList);

    void getBannerEmpty();

}
