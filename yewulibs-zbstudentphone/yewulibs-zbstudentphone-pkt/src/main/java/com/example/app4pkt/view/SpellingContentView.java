package com.example.app4pkt.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.CourseList;

import java.util.List;

public interface SpellingContentView extends BaseView {
    void getDataCourse(List<CourseList> courseLists);

    void onFailed(String msg);
}
