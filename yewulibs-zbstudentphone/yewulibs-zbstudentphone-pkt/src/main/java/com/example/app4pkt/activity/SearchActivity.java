package com.example.app4pkt.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4pkt.R;
import com.example.app4pkt.adapter.SearchHistoryAdapter;
import com.sdzn.core.base.BaseActivity1;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.widget.SweetAlertDialog;


import com.example.app4public.manager.SPManager;
import com.example.app4public.manager.constant.CourseCons;
import com.example.app4public.widget.ClearEditText;
import com.example.app4public.widget.flowlayout.FlowLayout;
import com.example.app4public.widget.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;




/**
 * 描述：
 * - 课程搜索（包含主页、直播、点播课程搜搜）
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SearchActivity extends BaseActivity1 implements View.OnClickListener {

    ClearEditText etSearch;
    TagFlowLayout tagSearchHistory;
    RelativeLayout rlHistory;
    ImageView ivClearHistory;
    TextView tvCancel;

    private List<String> searchHistories;
    private SearchHistoryAdapter<String> searchHistoryAdapter;

    private int courseType;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean isSearchTeacher;
    public static final String IS_SEARCH_TEACHER = "isSearchTeacher";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入SearchActivity成功" );
                }
            }
        }
        etSearch = (ClearEditText) findViewById(R.id.et_search);
        rlHistory = (RelativeLayout) findViewById(R.id.Rl_history);
        tagSearchHistory = (TagFlowLayout) findViewById(R.id.tag_search_history);
        ivClearHistory = (ImageView) findViewById(R.id.iv_clear_history);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvCancel.setOnClickListener(this);
        ivClearHistory.setOnClickListener(this);
        initData();
        initView();
    }

    private void initData() {
        isSearchTeacher = getIntent().getBooleanExtra(IS_SEARCH_TEACHER, false);
        courseType = getIntent().getIntExtra("courseType", CourseCons.Type.ALL);
        searchHistories = new ArrayList<>();
        searchHistories.addAll(isSearchTeacher ? SPManager.getSearchTeacherStr() : SPManager.getSearchStr());
        if (searchHistories.isEmpty()) {
            rlHistory.setVisibility(View.GONE);
        } else {
            rlHistory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initView() {
        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();
        etSearch.requestFocusFromTouch();
        searchHistoryAdapter = new SearchHistoryAdapter<>(mContext, searchHistories);
        tagSearchHistory.setAdapter(searchHistoryAdapter);
        tagSearchHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                search(searchHistories.get(position));
                return true;
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        String searchStr = etSearch.getText().toString().trim();
                        search(searchStr);
                        KeyboardUtils.hideSoftInput(SearchActivity.this);
                    }
                    return true;
                }
                return false;
            }
        });
        etSearch.setOnFocusChangedListener(new ClearEditText.onFocusChangedListener() {
            @Override
            public void onFocusChanged(View v, boolean hasFocus) {
                if (etSearch == v) {
                    if (hasFocus) {
                        KeyboardUtils.showSoftInput(mContext, etSearch);
                    } else {
                        KeyboardUtils.hideSoftInput(SearchActivity.this);
                    }
                }
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                KeyboardUtils.showSoftInput(mContext, etSearch);
            }
        }, 200);
    }


    private void showClearDialot() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定删除全部历史记录？")
                .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        clearHistory();
                    }
                }).setCancelable(true).show();
    }

    private void search(String searchStr) {
//        if (TextUtils.isEmpty(searchStr)) {
//            ToastUtils.showShort("请输入搜索内容");
//        } else {
        rlHistory.setVisibility(View.VISIBLE);
        saveSearchStr(searchStr);
        searchHistoryAdapter.notifyDataChanged();
        if (isSearchTeacher) {
//                IntentController.toSearchTeacherResult(this,searchStr);
        } else {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SearchResultActivity");
            startIntent.putExtra("searchStr", searchStr);
            startIntent.putExtra("courseType", courseType);
            startActivity(startIntent);
        }
//        }
    }

    private void saveSearchStr(String searchStr) {
        if (searchStr.isEmpty()) {
            return;
        }
        if (searchHistories.contains(searchStr)) {
            searchHistories.remove(searchStr);
        }
        searchHistories.add(0, searchStr);

        if (searchHistories.size() > 10) {
            for (int i = searchHistories.size() - 1; i > 9; i--) {
                searchHistories.remove(i);
            }
        }

        SPManager.saveSearchStr(searchHistories);

    }

    private void clearHistory() {
        rlHistory.setVisibility(View.GONE);
        searchHistories.clear();
        if (isSearchTeacher) {
            SPManager.saveSearchTeacherStr(searchHistories);
        } else {
            SPManager.saveSearchStr(searchHistories);
        }
        searchHistoryAdapter.notifyDataChanged();
    }

    @Override
    public void onClick(View view) {
        if (R.id.tv_cancel == view.getId()) {
            KeyboardUtils.hideSoftInput(this);
            onBackPressed();
        } else if (R.id.iv_clear_history == view.getId()) {
            showClearDialot();

        }
    }
}
