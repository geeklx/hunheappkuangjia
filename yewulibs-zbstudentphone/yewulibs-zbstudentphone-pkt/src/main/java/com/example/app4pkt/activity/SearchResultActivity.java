package com.example.app4pkt.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4pkt.R;
import com.example.app4pkt.fragment.SpellingContentFragment;
import com.example.app4public.manager.constant.CourseCons;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseActivity1;


/**
 * 描述：
 * - 搜索结果
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class SearchResultActivity extends BaseActivity1 implements View.OnClickListener {

    TextView tvSearch;
    ImageView ivBack;
    ImageView ivCartShop;
    private String searchStr;

    private int courseType;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search_result;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入SearchResultActivity成功");
                }
            }
        }
        tvSearch = (TextView) findViewById(R.id.tv_search);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivCartShop = (ImageView) findViewById(R.id.iv_cart_shop);
        ivBack.setOnClickListener(this);
        ivCartShop.setOnClickListener(this);
        tvSearch.setOnClickListener(this);
        initData();
        initView();
    }

    private void initData() {
        searchStr = getIntent().getStringExtra("searchStr");
        courseType = getIntent().getIntExtra("courseType", CourseCons.Type.ALL);
        //courseType  此处网listfragment传数据，是区分  所有学科还是单一学科
    }

    private void initView() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.fl_search_container, SpellingContentFragment.newInstance(courseType, searchStr)).commit();
        fragmentTransaction.add(R.id.fl_search_container, SpellingContentFragment.newInstance("", searchStr, SpellingContentFragment.IS_SEARCH_IN)).commit();
        tvSearch.setText(searchStr);
    }

    @Override
    public void onClick(View view) {
        if (R.id.iv_back == view.getId() || R.id.tv_search == view.getId()) {
            onBackPressed();
        } else if (R.id.iv_cart_shop == view.getId()) {
            if (!SPToken.autoLogin(mContext)) {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
                return;
            }
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
            startActivity(startIntent);
        }
    }
}
