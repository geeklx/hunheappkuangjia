package com.example.app4mykc.adapter;

import android.content.Context;
import android.widget.TextView;

import com.example.app4mykc.R;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.variants.bean.MineList;

import java.util.List;

/**
 * 描述：我的课程列表
 * -
 * type =1  过时
 */

public class MineCourseAdapter extends BaseRcvAdapter<MineList> {
    private boolean isLiving;
    private static final String LIVE = "LIVE";
    private static final String COURSE = "COURSE";
    private int type=1;//过期

    public MineCourseAdapter(Context context, List<MineList> mList,int type) {
        super(context, R.layout.item_mine_course, mList);
        this.type=type;

    }


    @Override
    public void convert(BaseViewHolder holder, int position, MineList mineCourseBean) {
        ((TextView) (holder.getView(R.id.tv_class))).setLines(2);
        holder.setText(R.id.tv_class, mineCourseBean.getCourseName());
        holder.setImageView(R.id.img_video, "" + mineCourseBean.getLogo());

        if (mineCourseBean.getCourseType().equals(LIVE)) {
            holder.setText(R.id.tv_status,"直");
            holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
            isLiving = true;
            holder.setText(R.id.tv_video_type, "「直播」");
            holder.setText(R.id.tv_schedule, "学习进度：");
            holder.setTextColor(R.id.tv_video_type, context.getResources().getColor(R.color.red));
        } else {
            holder.setText(R.id.tv_status,"点");
            holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
            isLiving = false;
            holder.setText(R.id.tv_video_type, "「点播」");
            holder.setText(R.id.tv_schedule, "学习进度：");
            holder.setTextColor(R.id.tv_video_type, context.getResources().getColor(R.color.live_text_yellow));
        }
        int percent = (int) mineCourseBean.getProgress();
        holder.setText(R.id.tv_schedule_value, String.valueOf(percent));
        holder.setProgress(R.id.progress_schedule, percent, 100);
        if (mineCourseBean.getRecentCourseName() != null && !mineCourseBean.getRecentCourseName().isEmpty()) {
//            holder.setText(R.id.tv_course_content, "近期学习：" + mineCourseBean.getRecentCourseName());
        }
        holder.setText(R.id.tv_date_value, "有效期至：" + TimeUtils.millis2String(TimeUtils.string2Millis(mineCourseBean.getTime()), "yyyy-MM-dd HH:mm"));

        /**
         * 点播
         */
        if (!isLiving) {
            holder.setVisible(R.id.tv_liveing, false);

        } else if (isLiving) {
            //区分右上角课程状态
            holder.setVisible(R.id.tv_liveing, false);
        }
        holder.setVisible(R.id.tv_video_type, false);

        //判断课程是否有效，如果无效只显示title，视频类型
        if (1 == type) {
            holder.setVisible(R.id.view_lose, true);
        }

    }
}
