package com.example.app4home.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseFragment;
import com.example.app4home.R;

import com.example.app4public.manager.constant.CourseCons;
import com.example.app4public.widget.TitleBar;



/**
 * zs
 * <p>
 * 学校课程
 */
public class LiveCourseFragment extends BaseFragment {

    TitleBar titleBar;

    public LiveCourseFragment() {

    }

    public static LiveCourseFragment newInstance() {
        return new LiveCourseFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_live;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        titleBar = (TitleBar) rootView.findViewById(R.id.title_bar);
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {

        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPToken.autoLogin(mContext)) {
                     Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
                    return;
                }
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(startIntent);
            }
        });

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_live_container, CourseFragment.newInstance(CourseCons.Type.LIVING, null)).commit();
    }

}
