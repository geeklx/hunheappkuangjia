package com.example.app4home.course.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.variants.bean.CourseCatalogueBean;
import com.sdzn.variants.bean.CourseDetailBean;
import com.sdzn.variants.bean.CourseIncludeBean;
import com.sdzn.variants.bean.ResultBean;
import com.example.app4home.course.view.CourseDetailView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public class CourseDetailPresenter extends BasePresenter<CourseDetailView> {

    /**
     * 获取组合课程详情
     **/
    public void getCoursePackageDetailData(int courseId) {//组合
        Map<String, String> map = new HashMap<>();
        map.put("courseId", String.valueOf(courseId));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCoursePackageDetail(requestBody)
                .compose(TransformUtils.<ResultBean<CourseDetailBean>>defaultSchedulers())
                .map(new ResponseNewFunc<CourseDetailBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseDetailBean>() {
                    @Override
                    public void onNext(CourseDetailBean packageDetail) {
                        StringBuilder courseDesc = new StringBuilder();
                        ArrayList<String> listCourse = new ArrayList<>();
                        if (packageDetail.getProjectId() != 0) {
                            courseDesc.append("专题名称：" + packageDetail.getProjectName());
                            packageDetail.setCourseDescString(courseDesc.toString());
                        } else {

                            courseDesc.append("科目：");
                            for (CourseCatalogueBean courseCatalogueBean : packageDetail.getCourseList()) {
                                if (courseCatalogueBean.getSubjectName() != null && !courseCatalogueBean.getSubjectName().isEmpty() &&
                                        !listCourse.contains(courseCatalogueBean.getSubjectName())) {
                                    listCourse.add(courseCatalogueBean.getSubjectName());
                                    courseDesc.append(courseCatalogueBean.getSubjectName()).append("、");
                                }
                            }
                            if (!courseDesc.toString().isEmpty()&&courseDesc.toString().length()>3) {
                                courseDesc.deleteCharAt(courseDesc.toString().lastIndexOf("、"));
                                packageDetail.setCourseDescString(courseDesc.toString());
                            }
                        }
//                        if (packageDetail.getProjectId() != 0) {
//                            courseDesc.append("科目：" + packageDetail.getProjectName());
//                        } else {
//                            courseDesc.append("科目：");
//                            for (CourseCatalogueBean courseCatalogueBean : packageDetail.getCourseList()) {
//                                if (courseCatalogueBean.getSubjectName() != null && !courseCatalogueBean.getSubjectName().isEmpty() &&
//                                        !listCourse.contains(courseCatalogueBean.getSubjectName())) {
//                                    listCourse.add(courseCatalogueBean.getSubjectName());
//                                    courseDesc.append(courseCatalogueBean.getSubjectName());//.append("、")
//                                }
//                            }
//                        }
//                        if (!courseDesc.toString().isEmpty()) {
////                            courseDesc.deleteCharAt(courseDesc.lastIndexOf("、"));
//                            packageDetail.setCourseDescString(courseDesc.toString());
//                        }

                        getView().getCourseDetailSuccess(packageDetail);

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = "";
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCourseDetailFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 获取普通课程详情
     */
    public void getCourseDetaiNormal(int courseId) {
        Map<String, String> map = new HashMap<>();
        map.put("courseId", String.valueOf(courseId));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCourseDetailNormal(requestBody)
                .compose(TransformUtils.<ResultBean<CourseIncludeBean>>defaultSchedulers())
                .map(new ResponseNewFunc<CourseIncludeBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseIncludeBean>() {
                    @Override
                    public void onNext(CourseIncludeBean courseIncludeBean) {
                        CourseDetailBean courseDetail = courseIncludeBean.getEduCourse();
                        if (courseIncludeBean.getEduTeacherList() != null && courseIncludeBean.getEduTeacherList().size() > 0) {
                            StringBuilder courseDesc = new StringBuilder();
                            for (CourseIncludeBean.EduTeacherListBean teacherListBean : courseIncludeBean.getEduTeacherList()) {
                                if (teacherListBean.getName() != null && !teacherListBean.getName().isEmpty()) {
                                    courseDesc.append(teacherListBean.getName()).append("、");
                                }
                            }
                            if (!courseDesc.toString().isEmpty()) {
                                courseDesc.deleteCharAt(courseDesc.lastIndexOf("、"));
                                courseDetail.setCourseDescString(courseDesc.toString());
                            }
                        }
//                        if (courseIncludeBean.getEduTeacherList() != null && courseIncludeBean.getEduTeacherList().size() > 0) {
//                            StringBuilder courseDesc = new StringBuilder();
//                            for (CourseIncludeBean.EduTeacherListBean teacherListBean : courseIncludeBean.getEduTeacherList()) {
//                                if (teacherListBean.getName() != null && !teacherListBean.getName().isEmpty()) {
//                                    courseDesc.append(teacherListBean.getName());
//                                }
//                            }
//                            if (!courseDesc.toString().isEmpty()) {
////                                courseDesc.deleteCharAt(courseDesc.lastIndexOf("、"));
//                                courseDetail.setCourseDescString(courseDesc.toString());
//                            }
//                        }

                        getView().getCourseDetailSuccess(courseDetail);

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = "";
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCourseDetailFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

}
