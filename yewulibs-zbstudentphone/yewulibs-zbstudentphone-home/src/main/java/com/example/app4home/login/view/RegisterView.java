package com.example.app4home.login.view;

import com.sdzn.core.base.BaseView;

import java.io.InputStream;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/15
 */
public interface RegisterView extends BaseView {

    void getCodeSuccess();

    void getCodeFailure(String msg);

    void registerSuccess();

    void registerFailure(String msg);

    void OnImgTokenSuccess(Object imgtoken);

    void OnImgSuccess(InputStream inputStream);

    void getFailure(String msg);

}
