package com.example.app4home.mvp.presenter;

import android.text.TextUtils;

import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.manager.SPManager;
import com.example.app4home.mvp.view.SelectSubjectView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

public class SelectSubjectPresenter extends BasePresenter<SelectSubjectView> {
//    public void getGrade() {
//        Subscription subscribe = RestApi.getInstance()
//                .createNew(CourseService.class)
//                .getGradeJson()
//                .compose(TransformUtils.<ResultBean<List<GradeJson>>>defaultSchedulers())
//                .map(new ResponseNewFunc<>())
//                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<GradeJson>>() {
//                    @Override
//                    public void onNext(List<GradeJson> gradeJson) {
//                        if (gradeJson!=null){
//                            getView().onSuccess(gradeJson);
//                        }
//
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        String msg = mActivity.getString(R.string.request_failure_try_again);
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
//                        getView().onFailed(msg);
//                    }
//                }, mActivity, true));
//        addSubscribe(subscribe);
//
//    }


    public void setSubAndGrade(String eduId,String eduName,String levelId,String levelName,String gradeId,String gradeName) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .updateEducaLevel(eduId,eduName,levelId,levelName,gradeId,gradeName)
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        SPManager.saveUser(userBean);
                        SPManager.saveSection(userBean.getSubjectId(), userBean.getSubjectName(),userBean.getEducationId());
                        SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());
                        getView().setSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onFailed(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);

    }


}
