package com.example.app4home.login.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.example.app4home.login.view.RegisterView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/15
 */
public class RegisterPresenter extends BasePresenter<RegisterView> {

    /**
     * 获取 图形验证码  token
     */
    public void QueryImgToken() {
        Map<String, String> requestParams = new HashMap<>();
        String jsonPay = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .QueryImgToken(requestBodyPay)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().OnImgTokenSuccess(obj);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);


    }
    /**
     * 获取 图形验证码  图形
     */
    public void QueryImg(String imgToken) {
        RestApi.getInstance()
                .createNew(CourseService.class)
                .QueryImg(imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        InputStream inputStream = response.body().byteStream();
                        getView().OnImgSuccess(inputStream);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        getView().getFailure("获取图形验证码失败");
                    }
                });

    }


    public void getVerifyCode(String phoneNo,String imgCode,String imgToken) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .sendYZM(phoneNo,imgCode,imgToken)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    public void retrievePassword(String account,String phoneNo,String code) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getRetrievePassword(account,code,phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().registerSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().registerFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);

    }

}
