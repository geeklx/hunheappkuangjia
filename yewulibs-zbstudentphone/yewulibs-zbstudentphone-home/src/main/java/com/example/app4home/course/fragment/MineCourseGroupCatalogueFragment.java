package com.example.app4home.course.fragment;

import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baijiayun.live.ui.LiveSDKWithUI;
import com.baijiayun.livecore.LiveSDK;
import com.baijiayun.livecore.context.LPConstants;
import com.baijiayun.videoplayer.ui.playback.PBRoomUI;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.example.app4home.R;
import com.example.app4home.course.adapter.MineCourseCataloguAdapter;
import com.example.app4home.course.adapter.MineCourseGroupCatalogueAdapter;
import com.sdzn.variants.bean.CourseCatalogueBean;
import com.sdzn.variants.bean.CourseDetailBean;
import com.sdzn.variants.bean.CourseKpointListBean;
import com.sdzn.variants.bean.NewLiveInfo;
import com.sdzn.variants.bean.NewVideoInfo;
import com.example.app4public.event.ToApplyStatusEvent;
import com.example.app4public.manager.constant.CourseCons;
import com.example.app4home.course.presenter.LiveRoomPresenter;
import com.example.app4home.course.view.LiveRoomView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;



/**
 * 描述：
 * - 组合课程目录
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class MineCourseGroupCatalogueFragment extends BaseMVPFragment<LiveRoomView, LiveRoomPresenter> implements LiveRoomView {

    RecyclerView rcvCourseCatalogue;

    private MineCourseGroupCatalogueAdapter courseGroupCatalogueAdapter;
    private MineCourseCataloguAdapter courseCataloguAdapter;
    private List courseCatalogueBeans;
    private int courseType = -1;
    private boolean isFree;//123 是否免费
    private boolean isPurchase = true;
    private CourseKpointListBean mKpointBean;
    private CourseDetailBean courseDetailBean;


    public static MineCourseGroupCatalogueFragment newInstance(int courseType, boolean isPurchase) {
        MineCourseGroupCatalogueFragment courseGroupCatalogueFragment = new MineCourseGroupCatalogueFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("courseType", courseType);
        bundle.putBoolean("isPurchase", isPurchase);
        courseGroupCatalogueFragment.setArguments(bundle);
        return courseGroupCatalogueFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            courseType = getArguments().getInt("courseType");
            isPurchase = getArguments().getBoolean("isPurchase");
        }
    }

    @Override
    protected LiveRoomPresenter createPresenter() {
        return new LiveRoomPresenter();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course_catalogue;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        rcvCourseCatalogue = (RecyclerView) rootView.findViewById(R.id.rcv_course_catalogue);
        EventBus.getDefault().register(this);
        initData();
        initView();
    }

    private void initData() {
        courseCatalogueBeans = new ArrayList<>();

        if (courseType == CourseCons.Type.VIDEO) {
            courseType = CourseCons.Type.VIDEO;
        } else if (courseType == CourseCons.Type.LIVING) {
            courseType = CourseCons.Type.LIVING;
        }

    }

    private void initView() {
        if (courseDetailBean == null) {
            return;
        }
        rcvCourseCatalogue.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL, ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 1));
        rcvCourseCatalogue.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        double price = courseDetailBean.getCurrentPrice();
        isFree = price >= -0.000001 && price <= 0.000001;
        if ("PACKAGE".equals(courseDetailBean.getSellType())) {
            this.courseCatalogueBeans.clear();
            this.courseCatalogueBeans.addAll(courseDetailBean.getCourseList());
            courseGroupCatalogueAdapter = new MineCourseGroupCatalogueAdapter(mContext,
                    courseType, courseCatalogueBeans, courseDetailBean.getIsavaliable(), isFree, courseDetailBean.isRelationLiveCourse());
            int[] section = getLiveingSection();
            courseGroupCatalogueAdapter.setSectionIsOpen(section[0], true);
            rcvCourseCatalogue.setAdapter(courseGroupCatalogueAdapter);
            //滑动到正在直播的章节
            rcvCourseCatalogue.smoothScrollToPosition(section[1]);
            courseGroupCatalogueAdapter.setListener(new MineCourseGroupCatalogueAdapter.LiveCoursePlayerListener() {
                @Override
                public void ontoLiving(CourseKpointListBean kpointBean) {
                    if (isFree && !isPurchase) {
                        if (1 == kpointBean.getFree()) {//试听
                            initLiving(kpointBean);
                        } else {
                            toApply(1, kpointBean);
                        }
                    } else {
                        initLiving(kpointBean);
                    }
                }

                @Override
                public void ontoVideo(CourseKpointListBean kpointBean) {
                    if (isFree && !isPurchase) {
                        if (1 == kpointBean.getFree()) {//试听
                            initVideo(kpointBean);
                        } else {
                            toApply(2, kpointBean);
                        }
                    } else {
                        initVideo(kpointBean);
                    }

                }

                @Override
                public void ontoReplay(CourseKpointListBean kpointBean) {
                    //回放
                    if (isFree && !isPurchase) {
                        if (1 == kpointBean.getFree()) {//试听
                            initReplay(kpointBean);
                        } else {
                            toApply(3, kpointBean);
                        }
                    } else {
                        initReplay(kpointBean);
                    }
                }
            });
        } else {
            this.courseCatalogueBeans.clear();
            for (int i = 0; i < courseDetailBean.getCourseKpointList().size(); i++) {
                this.courseCatalogueBeans.add(courseDetailBean.getCourseKpointList().get(i));
                for (int j = 0; j < courseDetailBean.getCourseKpointList().get(i).getKpointVoList().size(); j++) {
                    this.courseCatalogueBeans.add(courseDetailBean.getCourseKpointList().get(i).getKpointVoList().get(j));
                }
            }
            courseCataloguAdapter = new MineCourseCataloguAdapter(mContext,
                    courseType, courseCatalogueBeans, courseDetailBean.getIsavaliable(), isFree, courseDetailBean.isRelationLiveCourse());
            rcvCourseCatalogue.setAdapter(courseCataloguAdapter);
            courseCataloguAdapter.setListener(new MineCourseCataloguAdapter.LiveCoursePlayerListener() {
                @Override
                public void ontoLiving(CourseKpointListBean kpointBean) {
                    if (isFree && !isPurchase) {
                        if (1 == kpointBean.getFree()) {//试听
                            initLiving(kpointBean);
                        } else {
                            toApply(1, kpointBean);
                        }
                    } else {
                        initLiving(kpointBean);
                    }
                }

                @Override
                public void ontoVideo(CourseKpointListBean kpointBean) {
                    if (isFree && !isPurchase) {
                        if (1 == kpointBean.getFree()) {//试听
                            initVideo(kpointBean);
                        } else {
                            toApply(2, kpointBean);
                        }
                    } else {
                        initVideo(kpointBean);
                    }
                }

                @Override
                public void ontoReplay(CourseKpointListBean kpointBean) {
                    //回放
                    if (isFree && !isPurchase) {
                        if (1 == kpointBean.getFree()) {//试听
                            initReplay(kpointBean);
                        } else {
                            toApply(3, kpointBean);
                        }
                    } else {
                        initReplay(kpointBean);
                    }
                }
            });
        }
    }

    /**
     * 直播
     */
    private void initLiving(CourseKpointListBean kpointBean) {
        mKpointBean = kpointBean;
        mPresenter.getLivingInfo(kpointBean.getKpointId());
    }


    /**
     * 点播
     */
    private void initVideo(CourseKpointListBean kpointBean) {
        mKpointBean = kpointBean;
        mPresenter.getVideoInfo(kpointBean.getKpointId());
    }

    /**
     * 回放
     */
    private void initReplay(CourseKpointListBean kpointBean) {
        mKpointBean = kpointBean;
        mPresenter.getReplayInfo(kpointBean.getKpointId(), courseDetailBean.getCourseId());
    }

    /**
     * 未报名 免费    1直播  2点播  3回放
     */
    private void toApply(int type, CourseKpointListBean kpointBean) {
        if (courseDetailBean == null) {
            return;
        }
        mKpointBean = kpointBean;
        mPresenter.getIsPurchase(courseDetailBean.getCourseId(), type);
    }

    /**
     * 获取当前正在直播的课程, 没有则返回[0,0]
     *
     * @return 返回长度为2的数组, 第一位是正在直播的课程所在位置, 第二位是直播课程的章节所在位置
     */
    private int[] getLiveingSection() {
        int[] sections = new int[2];
        if (courseType != CourseCons.Type.LIVING) {
            return sections;
        }
        int section = 0;
        int position;
        CourseCatalogueBean courseCatalogueBean;
        String status;
        for (; section < courseCatalogueBeans.size(); section++) {
            courseCatalogueBean = (CourseCatalogueBean) courseCatalogueBeans.get(section);
            position = 0;
            for (; position < courseCatalogueBean.getCourseKpointList().size(); position++) {
                status = CourseCons.LiveStatus.upStatus(courseCatalogueBean.getCourseKpointList().get(position).getLiveStates());
                if (CourseCons.LiveStatus.isLiving(status) || CourseCons.LiveStatus.isRest(status)) {
                    sections[0] = section;
                    sections[1] = section + position + 1;
                    return sections;
                }
            }
        }
        return sections;
    }

    public void setData(CourseDetailBean courseDetailBean) {
        this.courseDetailBean = courseDetailBean;
    }

    @Override
    public void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean) {
        LiveSDK.customEnvironmentPrefix = "b96152240";

        LiveSDKWithUI.enterRoomWithVerticalTemplate(mContext, Long.valueOf(liveRoomBean.getRoom_id().trim()), liveRoomBean.getSign(), new LiveSDKWithUI.LiveRoomUserModel(liveRoomBean.getUser_info().getUserName(), liveRoomBean.getUser_info().getUserAvatar(), String.valueOf(liveRoomBean.getUser_info().getUserNumber()), LPConstants.LPUserType.Student), new LiveSDKWithUI.LiveSDKEnterRoomListener() {
            @Override
            public void onError(String msg) {

            }
        });

    }

    @Override
    public void liveRoomInfoOnError(String msg) {
        ToastUtils.showShort(msg);

    }

    @Override
    public void getVideoRoomInfoSuccrss(NewVideoInfo info) {
//        Intent intent = new Intent(getActivity(), VideoPlayActivity.class);
//        intent.putExtra("videoId", Long.valueOf(info.getVideoId()));
//        intent.putExtra("token", info.getToken());
//        intent.putExtra("isOffline", false);
//        startActivity(intent);
        PBRoomUI.startPlayVideo(mContext, Long.valueOf(info.getVideoId()), info.getToken(), null);
    }

    @Override
    public void videoRoomInfoOnError(String msg) {
        ToastUtils.showShort(msg);
    }

    /**
     * 回放
     */
    @Override
    public void getReplayInfoSuccess(NewVideoInfo info) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        PBRoomUI.enterPBRoom(getActivity(), info.getRoomId(), info.getToken(), "0", new PBRoomUI.OnEnterPBRoomFailedListener() {

            @Override
            public void onEnterPBRoomFailed(String msg) {
                ToastUtils.showShort(msg);
            }
        });
    }

    /**
     * 未报名 免费    1直播  2点播  3回放
     */
    @Override
    public void applySuccess(int type) {
        ToastUtils.showShort("报名成功");
        EventBus.getDefault().post(new ToApplyStatusEvent(true));
        if (1 == type) {
            initLiving(mKpointBean);
        } else if (2 == type) {
            initVideo(mKpointBean);
        } else if (3 == type) {
            initReplay(mKpointBean);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setIsPurchase(ToApplyStatusEvent statusEvent) {
        isPurchase = statusEvent.isStatus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
