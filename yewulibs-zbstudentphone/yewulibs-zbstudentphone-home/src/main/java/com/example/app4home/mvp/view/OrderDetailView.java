package com.example.app4home.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.IndentBean;
import com.sdzn.variants.bean.OrderDetail;
import com.sdzn.variants.bean.PayInfoBean;

public interface OrderDetailView extends BaseView {

    void onOrderInfo(OrderDetail orderDetail);

    void onOrderError(String msg);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void cancelSuccess();
}
