package com.example.app4home.course.fragment;

import android.os.Bundle;

import com.sdzn.core.base.BaseFragment;
import com.example.app4home.R;

/**
 * 描述：
 * - 课程评价
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class CourseEvaluationFragment extends BaseFragment {

    public CourseEvaluationFragment() {

    }

    public static CourseEvaluationFragment newInstance() {
        return new CourseEvaluationFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course_evaluation;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {

    }
}
