package com.example.app4home.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.core.base.BaseFragment;
import com.example.app4home.R;

import com.example.app4public.manager.constant.CourseCons;
import com.example.app4public.widget.TitleBar;



/**
 * 描述：
 * - 点播视频观看
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class VideoCourseFragment extends BaseFragment {


    TitleBar titleBar;

    public VideoCourseFragment() {
        // Required empty public constructor
    }

    public static VideoCourseFragment newInstance() {
        return new VideoCourseFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_video;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        titleBar = (TitleBar) rootView.findViewById(R.id.title_bar);
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SearchActivity");
                startIntent.putExtra("courseType", CourseCons.Type.VIDEO);
                startActivity(startIntent);
            }
        });
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(startIntent);
            }
        });

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_live_container, CourseFragment.newInstance(CourseCons.Type.VIDEO, null)).commit();
    }


}
