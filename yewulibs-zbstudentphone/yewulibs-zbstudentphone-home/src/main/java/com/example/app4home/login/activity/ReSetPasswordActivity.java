package com.example.app4home.login.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4home.login.presenter.ReSetPwdPresenter;
import com.example.app4home.login.view.ReSetPwdView;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4home.R;

import com.example.app4public.widget.PwdEditText;


/**
 * 描述：
 * - 修改密码-》重置
 * 创建人：
 * 创建时间：2017/10/25
 */
public class ReSetPasswordActivity extends BaseMVPActivity1<ReSetPwdView, ReSetPwdPresenter> implements ReSetPwdView, View.OnClickListener {

    PwdEditText etNewPassword;
    PwdEditText etPasswordCertain;
    ImageView ivBack;
    Button btnCertain;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reset_password;
    }

    @Override
    protected ReSetPwdPresenter createPresenter() {
        return new ReSetPwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入ReSetPasswordActivity成功");
                }
            }
        }
        etNewPassword = (PwdEditText) findViewById(R.id.et_new_password);
        etPasswordCertain = (PwdEditText) findViewById(R.id.et_password_certain);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        btnCertain = (Button) findViewById(R.id.btn_certain);
        ivBack.setOnClickListener(this);
        btnCertain.setOnClickListener(this);
    }


    @Override
    public void changeSuccess() {
        SPToken.saveToken("");
        Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
        startActivity(startIntent);
    }

    @Override
    public void changeFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onClick(View v) {
        if (R.id.btn_certain == v.getId()) {
            String newPwd = etNewPassword.getText().toString().trim();
            String confirmPwd = etPasswordCertain.getText().toString().trim();
            mPresenter.
                    confirm(newPwd, confirmPwd);
        } else if (R.id.iv_back == v.getId()) {
            onBackPressed();
        }
    }
}
