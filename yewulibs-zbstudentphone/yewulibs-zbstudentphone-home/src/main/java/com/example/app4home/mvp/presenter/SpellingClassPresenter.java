package com.example.app4home.mvp.presenter;

import android.text.TextUtils;

import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.SubjectBean;
import com.example.app4public.manager.SPManager;
import com.example.app4home.mvp.view.SpellingClassView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.List;

import rx.Subscription;

public class SpellingClassPresenter extends BasePresenter<SpellingClassView> {

    public void getSubject(){
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSubjectSpell(SPManager.getSectionId())
                .compose(TransformUtils.<ResultBean<List<SubjectBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<SubjectBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<SubjectBean>>() {
                    @Override
                    public void onNext(List<SubjectBean> subjectSpellBeanList) {
                        getView().onSubjectSuccess(subjectSpellBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onSubjectFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
