package com.example.app4home.login.presenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.UserLoginBean;
import com.example.app4home.login.view.RetrievePwdView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.AccountService;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class RetrievePwdPresenter extends BasePresenter<RetrievePwdView> {

    /**
     * 获取 图形验证码  token
     */
    public void QueryImgToken() {
        Map<String, String> requestParams = new HashMap<>();
        String jsonPay = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .QueryImgToken(requestBodyPay)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().OnImgTokenSuccess(obj);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);


    }

    /**
     * 获取 图形验证码   图形
     */
    public void QueryImg(String imgToken) {
        RestApi.getInstance()
                .createNew(CourseService.class)
                .QueryImg(imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        InputStream inputStream = response.body().byteStream();
                        getView().OnImgSuccess(inputStream);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        getView().getFailure("获取图形验证码失败");
                    }
                });

    }


    public void getVerifyCode(String phoneNo,String imgCode,String imgToken) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .sendYZM(phoneNo,imgCode,imgToken)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }


    public void confirmVerifyCode(String phoneNo, String virifyCode) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .confirmVerifyCode(phoneNo, virifyCode)
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean loginBean) {
                        if (loginBean != null && !loginBean.getAccess_token().isEmpty()) {
                            String token = loginBean.getAccess_token();
                            SPToken.saveToken(token);
                            getView().confirmCodeSuccess();
                        } else {
                            onFail(new ApiException(new Throwable(), 10010));
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().confirmCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }
}
