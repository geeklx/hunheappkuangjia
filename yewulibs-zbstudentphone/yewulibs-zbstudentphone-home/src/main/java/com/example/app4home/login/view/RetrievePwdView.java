package com.example.app4home.login.view;

import android.graphics.Bitmap;

import com.sdzn.core.base.BaseView;

import java.io.InputStream;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public interface RetrievePwdView extends BaseView {

    void getCodeSuccess();

    void getCodeFailure(String msg);

    void confirmCodeSuccess();

    void confirmCodeFailure(String msg);

    void OnImgTokenSuccess(Object imgtoken);

    void OnImgSuccess(InputStream inputStream);

    void getFailure(String msg);

}
