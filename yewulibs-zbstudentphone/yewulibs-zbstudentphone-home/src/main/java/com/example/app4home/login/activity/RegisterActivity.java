package com.example.app4home.login.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.example.app4public.utils.YanzhengUtil;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4home.R;

import com.example.app4public.manager.SPManager;
import com.example.app4home.login.presenter.RegisterPresenter;
import com.example.app4home.login.view.RegisterView;
import com.example.app4public.utils.CountDownTimerUtils;
import com.example.app4public.utils.VerifyUtil;
import com.example.app4public.widget.ClearEditText;
import com.example.app4public.widget.PwdEditText;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;




/**
 * ---->   找回密码  没有手机号的去情况下
 */
public class RegisterActivity extends BaseMVPActivity1<RegisterView, RegisterPresenter> implements RegisterView, View.OnClickListener {

    ClearEditText etAccount;
    EditText etPhone;
    EditText etCode;
    PwdEditText etPassword;
    PwdEditText etPasswords;
    Button btnGetCode;
    Button btnCertain;
    ImageView ivBack;
    private ImageView Effectiveness;
    private EditText valicodeEdit;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected RegisterPresenter createPresenter() {
        return new RegisterPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入RegisterActivity成功" );
                }
            }
        }
        etAccount = (ClearEditText) findViewById(R.id.et_account);
        etPhone = (EditText) findViewById(R.id.et_phone);
        etCode = (EditText) findViewById(R.id.et_code);
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        etPassword = (PwdEditText) findViewById(R.id.et_password);
        etPasswords = (PwdEditText) findViewById(R.id.et_password_s);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        btnCertain = (Button) findViewById(R.id.btn_certain);
        Effectiveness = findViewById(R.id.iv_effectiveness);
        valicodeEdit = findViewById(R.id.valicode_edit);

        Effectiveness.setOnClickListener(this);
        btnGetCode.setOnClickListener(this);
        btnCertain.setOnClickListener(this);
        ivBack.setOnClickListener(this);
//        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (VerifyUtil.isMobileNO(etPhone.getText().toString())) {
//                        etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                    } else {
//                        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
//                            etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.red));
//                            etPhone.setError("手机号格式不正确");
//                        }
//                    }
//                } else {
//                    etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                }
//            }
//        });
        mPresenter.QueryImgToken();

    }


    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String valicode = valicodeEdit.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else  if (TextUtils.isEmpty(valicode)) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.getVerifyCode(phoneNo, valicode, imgToken);
        }
    }

    private void register() {
        String account = etAccount.getText().toString().trim();
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
//        String password = etPassword.getText().toString().trim();
//        String passwordS = etPasswords.getText().toString().trim();
        if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入账号");
        } else if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("account", account);
            params.put("phone", phoneNo);
            params.put("code", code);
//            params.put("password", password);
            mPresenter.retrievePassword(account, phoneNo, code);
        }
    }


    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
        YanzhengUtil.startTime(60 * 1000, btnGetCode);//倒计时
    }

    @Override
    public void getCodeFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    @Override
    public void registerSuccess() {
        String phoneNo = etPhone.getText().toString().trim();
        SPManager.saveLastLoginAccount(phoneNo);
        ToastUtils.showShort("请登录");
         Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
    }

    @Override
    public void registerFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        Glide.with(this).load(bytes).centerCrop().into(Effectiveness);

    }

    @Override
    public void getFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onClick(View view) {
        if (R.id.btn_get_code == view.getId()) {
            getVerifyCode();
        } else if (R.id.btn_certain == view.getId()) {
            register();
        } else if (R.id.iv_back == view.getId()) {
            onBackPressed();
        }  else if (R.id.iv_effectiveness == view.getId()) {
            mPresenter.QueryImgToken();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        YanzhengUtil.timer_des();
    }
}
