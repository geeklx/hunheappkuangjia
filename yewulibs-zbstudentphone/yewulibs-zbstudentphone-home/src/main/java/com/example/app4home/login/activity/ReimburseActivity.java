package com.example.app4home.login.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4home.R;
import com.example.app4public.event.OrderPayEvent;
import com.example.app4home.login.presenter.ReimbursePresenter;
import com.example.app4home.login.view.ReimburseView;
import com.example.app4public.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;




/**
 * 描述：退课申请界面
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/7
 */

public class ReimburseActivity extends BaseMVPActivity1<ReimburseView, ReimbursePresenter> implements ReimburseView, View.OnClickListener {

    TitleBar titleBar;
    EditText etReason;
    Button btConfirm;
    TextView tvWrite;
    LinearLayout llReason;
    private int orderId;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reimburse;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入ReimburseActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        llReason = (LinearLayout) findViewById(R.id.ll_reason);
        etReason = (EditText) findViewById(R.id.et_reason);
        btConfirm = (Button) findViewById(R.id.bt_confirm);
        tvWrite = (TextView) findViewById(R.id.tv_write);
        tvWrite.setOnClickListener(this);
        btConfirm.setOnClickListener(this);

        orderId = getIntent().getIntExtra("orderId", -1);
    }


    @Override
    protected ReimbursePresenter createPresenter() {
        return new ReimbursePresenter();
    }

    @Override
    public void dropSuccess(Object o) {
        ToastUtils.showShort("已申请，请耐心等待");
        EventBus.getDefault().post(new OrderPayEvent(false));
        finish();
    }

    @Override
    public void dropError(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onClick(View view) {
        if (R.id.tv_write == view.getId()) {
            llReason.setVisibility(View.VISIBLE);
        } else if (R.id.bt_confirm == view.getId()) {
            String reason = etReason.getText().toString().trim();
            if (TextUtils.isEmpty(reason)) {

                ToastUtils.showShort("请填写退款理由");
                return;
            }
            mPresenter.dropOut(orderId, reason);

        }
    }
}
