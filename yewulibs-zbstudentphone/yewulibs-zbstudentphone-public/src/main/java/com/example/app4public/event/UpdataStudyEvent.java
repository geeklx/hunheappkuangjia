package com.example.app4public.event;

/**
 * 描述：更新学习进度
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/11
 */

public class UpdataStudyEvent {
    private int Studyid;

    public UpdataStudyEvent(int Studyid) {
        this.Studyid = Studyid;
    }

    public int getStudyId() {
        return Studyid;
    }
}
