package com.example.app4grzx.presenter;

import android.text.TextUtils;

import com.example.app4grzx.R;
import com.example.app4grzx.view.UpdateAccountView;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.UserBean;
import com.sdzn.variants.bean.UserLoginBean;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

/**
 * 更新姓名,学校,班级
 */
public class UpdateAccountPresenter extends BasePresenter<UpdateAccountView> {

    public void updateAccount(String bindAccount) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .bindAccount(bindAccount)//className avatar  name  schoolName
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean loginBean) {
                        if (loginBean!=null) {
                            getView().updateAccountSuccess(loginBean.getUserDetail());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().updateAccountFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }
    public void updateUserInfo(String className, String name, String schoolName) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getChangeInfo(className,"",name,schoolName)//className avatar  name  schoolName
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        getView().updateAccountSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().updateAccountFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

}
