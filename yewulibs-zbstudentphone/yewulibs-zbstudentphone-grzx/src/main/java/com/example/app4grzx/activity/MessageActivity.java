package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4grzx.R;
import com.example.app4grzx.adapter.MessageAdapter;
import com.example.app4grzx.presenter.MessagePresenter;
import com.example.app4grzx.view.MessageView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.variants.bean.MessageBean;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;





/**
 * 描述：消息列表
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class MessageActivity extends BaseMVPActivity1<MessageView, MessagePresenter> implements MessageView, OnRefreshLoadmoreListener, BaseRcvAdapter.OnItemClickListener, View.OnClickListener {

    public static final String ID_MESSAGE = "message_id";
    public static final String TYPE_MESSAGE = "message_type";
    public static final String CONTENT = "message_content";
    public static final String ADD_TIME = "message_add_time";
    TitleBar titleBar;
    RecyclerView recyclerMessage;
    LinearLayout llSelectall;
    TextView tvDel;
    RelativeLayout rlCompile;
    TextView edit;
    SmartRefreshLayout refreshLayout;
    ImageView imgCheckbox;
    EmptyLayout emptyLayout;
    private MessageAdapter messageAdapter;
    private List<MessageBean.LetterListBean> mData = new ArrayList<>();
    private List<String> ids = new ArrayList<>();//存储id用于批量删除
    private List<MessageBean.LetterListBean> mDelData = new ArrayList<>();//存储用于批量删除bean
    private boolean isOkShow = false;//默认是编辑字体，当完成出现时设为true，进入编辑状态
    private int pageIndex = 1;
    private int pageSize = 20;
    private String messageType;//消息的类型


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_message;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入成功");
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        refreshLayout = (SmartRefreshLayout) findViewById(R.id.refresh_layout);
        recyclerMessage = (RecyclerView) findViewById(R.id.swipe_target);
        rlCompile = (RelativeLayout) findViewById(R.id.rl_compile);
        llSelectall = (LinearLayout) findViewById(R.id.ll_selectall);
        imgCheckbox = (ImageView) findViewById(R.id.img_checkbox);
        tvDel = (TextView) findViewById(R.id.tv_del);
        emptyLayout = (EmptyLayout) findViewById(R.id.empty_layout);
        llSelectall.setOnClickListener(this);
        initView();
        initData();

    }

    private void initData() {
        mPresenter.getMessageList(pageIndex, pageSize);
    }

    private void initTitleRight() {
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOkShow) {
                    //当点击的是编辑时
                    edit.setText("完成");
                    ids.clear();
                    refreshLayout.setEnableRefresh(false);
                    refreshLayout.setEnableLoadmore(false);
                    for (MessageBean.LetterListBean bean : mData) {
                        bean.setSelected(false);
                    }
                    rlCompile.setVisibility(View.VISIBLE);
                    messageAdapter.setEdit(true);

                } else {//当点击的是完成时
                    edit.setText("编辑");
                    refreshLayout.setEnableRefresh(true);
                    refreshLayout.setEnableLoadmore(true);
                    rlCompile.setVisibility(View.GONE);
                    messageAdapter.setEdit(false);
                    imgCheckbox.setSelected(false);
                }
                isOkShow = !isOkShow;
                messageAdapter.notifyDataSetChanged();

            }
        });
    }

    private void initView() {
        edit = (TextView) titleBar.getView(R.id.tv_right);

        tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.delMessage(ids);

            }
        });
        recyclerMessage.setLayoutManager(new LinearLayoutManager(mContext));
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
            }
        });
        messageAdapter = new MessageAdapter(mContext, mData);
        recyclerMessage.setAdapter(messageAdapter);
        refreshLayout.setOnRefreshLoadmoreListener(this);
        messageAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
//        pageIndex++;
//        mPresenter.getMessageList(pageIndex, pageSize);
        ToastUtils.showShort("没有更多消息了");
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        mPresenter.getMessageList(pageIndex, pageSize);
    }


    @Override
    protected MessagePresenter createPresenter() {
        return new MessagePresenter();
    }

    @Override
    public void listMessage(List<MessageBean.LetterListBean> messageList) {
        if (messageList != null && messageList.size() > 0) {
            titleBar.getView(R.id.tv_right).setVisibility(View.VISIBLE);
            initTitleRight();
            if (pageIndex == 1) {
                ids.clear();
                mData.clear();
            }
            mData.addAll(messageList);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            messageAdapter.notifyDataSetChanged();
        } else {
            if (pageIndex == 1) {
                titleBar.getView(R.id.tv_right).setVisibility(View.GONE);
                emptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                ToastUtils.showShort("没有更多消息了");
            }
        }
        goneSwipView();
        mDelData.clear();

    }

    @Override
    public void onListError(String msg) {
        if (pageIndex == 1) {
            mData.clear();
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        }
        goneSwipView();
    }

    @Override
    public void delMessage() {
//           mData.removeAll(mDelData);
        pageIndex = 1;
        mPresenter.getMessageList(pageIndex, pageSize);
    }

    @Override
    public void delMessageOnError(String msg) {
        ToastUtils.showShort(msg);

    }

    @Override
    public void delAll() {

    }

    @Override
    public void delAllOnError(String msg) {

    }

    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore(false);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (isOkShow) {
            //焦点应该作用在item的选择框上
            mData.get(position).setSelected(!mData.get(position).isSelected());
            messageAdapter.notifyDataSetChanged();
            if (mData.get(position).isSelected()) {
                ids.add(String.valueOf(mData.get(position).getId()));
                mDelData.add(mData.get(position));
            } else {
                ids.remove(String.valueOf(mData.get(position).getId()));
                mDelData.remove(mData.get(position));
            }
            //判断是否全选
            boolean isAll = true;
            for (MessageBean.LetterListBean bean : mData) {
                if (!bean.isSelected()) {
                    isAll = false;
                }

            }
            imgCheckbox.setSelected(isAll);
        } else {
            mData.get(position).setStatus("1");//进入详情标记为已读

            if (0 == mData.get(position).getType()) {
                messageType = "系统消息";
            } else if (2 == mData.get(position).getType()) {
                messageType = "站内信";
            } else if (5 == mData.get(position).getType()) {
                messageType = "课程消息";
            } else if (6 == mData.get(position).getType()) {
                messageType = "优惠券过期";
            } else {

            }

            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MessageDetailActivity");
            startIntent.putExtra(ID_MESSAGE, String.valueOf(mData.get(position).getId()));
            startIntent.putExtra(TYPE_MESSAGE, messageType);
            startIntent.putExtra(CONTENT, mData.get(position).getContent());
            startIntent.putExtra(ADD_TIME, mData.get(position).getAddTime());
            startActivity(startIntent);
            messageAdapter.notifyDataSetChanged();//表示已读消息
        }
    }

    @Override
    public void onClick(View v) {
        if (imgCheckbox.isSelected()) {
            //是全选时
            ids.clear();
            imgCheckbox.setSelected(false);
            for (MessageBean.LetterListBean bean : mData) {
                bean.setSelected(false);
            }
        } else {
            //非全选时
            for (MessageBean.LetterListBean bean : mData) {
                ids.add(String.valueOf(bean.getId()));
            }
            imgCheckbox.setSelected(true);
            for (MessageBean.LetterListBean bean : mData) {
                bean.setSelected(true);
            }
        }
        messageAdapter.notifyDataSetChanged();
    }
}
