package com.example.app4grzx.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.example.app4public.utils.YanzhengUtil;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4grzx.R;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.event.BindEvent;

import com.example.app4public.manager.SPManager;
import com.example.app4grzx.presenter.MobilePresenter;
import com.example.app4grzx.view.MobileView;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * zs
 */

public class BindingMobileActivity extends BaseMVPActivity1<MobileView, MobilePresenter> implements MobileView, View.OnClickListener {

    private EditText mMobile, mCode;
    private Button bt_confirm;
    private TextView tv_getCode;
    private UserBean userBean;
    private TextWatcher mWatcher;
    private ImageView Effectiveness;
    private EditText valicodeEdit;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_bindmobile;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入BindingMobileActivity成功");
                }
            }
        }
        initData();
        intView();
    }

    private void initData() {
        userBean = SPManager.getUser();
        mPresenter.QueryImgToken();
    }

    private void intView() {
        mMobile = (EditText) findViewById(R.id.tv_mobile);
        mCode = (EditText) findViewById(R.id.tv_code);
        tv_getCode = (TextView) findViewById(R.id.tv_getcode);
        bt_confirm = (Button) findViewById(R.id.bt_confirm);
        Effectiveness = findViewById(R.id.iv_effectiveness);
        valicodeEdit = findViewById(R.id.valicode_edit);
        Effectiveness.setOnClickListener(this);
        tv_getCode.setOnClickListener(this);
        bt_confirm.setOnClickListener(this);
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String mobile = mMobile.getText().toString().trim();
                String code = mCode.getText().toString().trim();
                tv_getCode.setEnabled(!TextUtils.isEmpty(mobile));
                bt_confirm.setEnabled(!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(code));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        mMobile.addTextChangedListener(mWatcher);
        mCode.addTextChangedListener(mWatcher);
    }

    @Override
    public void onClick(View v) {
        final String mobile = mMobile.getText().toString().trim();
        final String code = mCode.getText().toString().trim();
        String valicode = valicodeEdit.getText().toString().trim();
        if (R.id.bt_confirm == v.getId()) {
            mPresenter.confirm(mobile, code);
//                CustomDialog dialog = new CustomDialog.Builder(this)
//                        .setTitle("提示")
//                        .setTitleColor(R.color.textPrimary)
//                        .setMessage("手机号码" + userBean.getMobile() +
//                                "已经被其他账号绑定, 确认解除此绑定, 同时将该手机号与本账号绑定吗？")
//                        .setPositive("确认", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                mPresenter.confirm(mobile, code);
//                            }
//                        }).setNegative("取消", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        }).create();
//                dialog.show();
        } else if (R.id.tv_getcode == v.getId()) {
            mPresenter.gainCode(mobile, valicode, imgToken);
        } else if (R.id.iv_effectiveness == v.getId()) {
            mPresenter.QueryImgToken();
        }

    }

    @Override
    protected MobilePresenter createPresenter() {
        return new MobilePresenter();
    }

    @Override
    public void onSuccess() {
        String mobile = mMobile.getText().toString().trim();
//        boolean isBundling = userBean.isBundlingState();
//        userBean.setBundlingState(isBundling ? 0 : 1);
        userBean.setMobile(mobile);
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        ToastUtils.showShort("绑定成功");
        onBackPressed();
    }

//    private void reLogin() {
//        AppManager.getAppManager().appExit();
//        SPManager.changeLogin(mContext, false);
//         Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
//                startActivity(startIntent);
//    }

    @Override
    public void onError(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
        YanzhengUtil.startTime(60 * 1000, tv_getCode);//倒计时
    }

    @Override
    public void getCodeFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        Glide.with(this).load(bytes).centerCrop().into(Effectiveness);

    }

    @Override
    public void getFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        YanzhengUtil.timer_des();
    }
}
