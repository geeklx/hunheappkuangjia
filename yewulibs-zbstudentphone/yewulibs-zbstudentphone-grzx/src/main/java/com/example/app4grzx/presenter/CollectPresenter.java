package com.example.app4grzx.presenter;

import android.text.TextUtils;

import com.example.app4grzx.R;
import com.example.app4grzx.view.CollectView;
import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.variants.bean.CollectBean;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFuncLogin;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：我的收藏逻辑
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public class CollectPresenter extends BasePresenter<CollectView> {

    /**
     * 查询我的收藏
     *
     * @Field("currentPage") int currentPage, @Field("pageSize") int pageSize
     */
    public void upDataCollect(int currentPage, int pageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("index", String.valueOf(currentPage));
        map.put("size", String.valueOf(pageSize));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(CourseService.class)
                .upDataCollection(requestBody)
                .compose(TransformUtils.<ResultBean<CollectBean>>defaultSchedulers())
                .map(new ResponseNewFuncLogin<CollectBean>())
                .subscribe(new MProgressSubscriber<CollectBean>(new SubscriberOnNextListener<CollectBean>() {


                    @Override
                    public void onNext(CollectBean collectBean) {
                        getView().upDataSuccess(collectBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onError(msg);

                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }

    /**
     * 删除我的收藏某个条目
     *
     * @param courseId
     */
    public void delCollection(String courseId) {
        Map<String, String> map = new HashMap<>();
        map.put("courseId", courseId);
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscription = RestApi.getInstance()
                .createNew(CourseService.class)
                .delCollection(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFuncLogin<>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {

                    @Override
                    public void onNext(Object o) {
                        getView().delSuccess();

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().delError(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);
    }

}
