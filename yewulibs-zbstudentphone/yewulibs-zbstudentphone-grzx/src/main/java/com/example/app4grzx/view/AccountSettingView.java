package com.example.app4grzx.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.GradeJson;
import com.sdzn.variants.bean.UserBean;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public interface AccountSettingView extends BaseView {

    void changePhotoSuccess(String photo);

    void changePhotoError(String msg);

    void perfectAccountSuccess(UserBean userBean);

    void perfectAccountFailure(String msg);

    void onGradeEmpty();
}
