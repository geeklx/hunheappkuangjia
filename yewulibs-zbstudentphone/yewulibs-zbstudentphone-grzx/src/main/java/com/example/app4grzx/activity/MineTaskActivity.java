package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.app4grzx.R;
import com.example.app4grzx.adapter.MineTaskTitleAdapter;
import com.example.app4grzx.fragment.TaskPageFragment;
import com.example.app4public.widget.EmptySchoolLayout;
import com.example.app4public.widget.NoScrollViewPager;
import com.example.app4public.widget.TitleBar;
import com.example.app4public.widget.pager.PagerSlidingTabStrip;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：我的作业
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class MineTaskActivity extends FragmentActivity {
    EmptySchoolLayout emptySchoolLayout;
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    NoScrollViewPager vpCourse;
    View views;
    TitleBar mTitleBar;

    private MineTaskTitleAdapter fragmentAdapter;
    private List<Fragment> listFragment;//定义要装fragment的列表
    private ArrayList<String> listTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_task);
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入MineTaskActivity成功" );
                }
            }
        }
        emptySchoolLayout = findViewById(R.id.empty_layout_mine);
        mPagerSlidingTabStrip = findViewById(R.id.tabs);
        vpCourse = findViewById(R.id.vp_course);
        mTitleBar = findViewById(R.id.title_bar);
        views = findViewById(R.id.view);
        initView();
        initData();
    }


    private void initView() {
        listFragment = new ArrayList<>();
        listFragment.add(TaskPageFragment.newInstance(TaskPageFragment.TYPE_TODY));
        listFragment.add(TaskPageFragment.newInstance(TaskPageFragment.TYPE_RECENTLY));
        listTitle = new ArrayList<>();
        listTitle.add("未作答");
        listTitle.add("已作答");
        fragmentAdapter = new MineTaskTitleAdapter(getSupportFragmentManager());//getChildFragmentManager()
        fragmentAdapter.setmDatas(listTitle, listFragment);
        vpCourse.setAdapter(fragmentAdapter);
        vpCourse.setNoScroll(false);
        vpCourse.setOffscreenPageLimit(2);
        vpCourse.setCurrentItem(0);
        mPagerSlidingTabStrip.setViewPager(vpCourse);

    }

    private void initData() {


    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
