package com.example.app4grzx.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4grzx.R;
import com.sdzn.core.base.BaseActivity1;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.manager.SPManager;
import com.example.app4public.utils.CacheUtils;
import com.example.app4public.utils.GlideCatchUtil;
import com.example.app4public.widget.DialogUtil;
import com.example.app4public.widget.RoundRectImageView;
import com.example.app4public.widget.TitleBar;



/**
 * 描述：系统设置
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class SystemSettingActivity extends BaseActivity1 implements View.OnClickListener {

    public static final String INTENT_WEB = "userType";
    public static final String AUTO_LOGIN = "autoLogin";
    RoundRectImageView imgAvatar;
    TextView tvUserName;
    TextView tvUserClass;
    TextView tvCache;
    TextView tvVersion;
    TextView tvLogout;
    LinearLayout llAbout;
    LinearLayout llClearcache;
    LinearLayout llFk;
    LinearLayout llUser;
    LinearLayout llAgreement;
    LinearLayout llCloseAccount;
    TitleBar titleBar;
    CheckBox switchWifi;
    UserBean userBean;
    private String grade;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_systemsetting;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入SystemSettingActivity成功");
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        imgAvatar = (RoundRectImageView) findViewById(R.id.img_avatar);
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvUserClass = (TextView) findViewById(R.id.tv_user_class);
        switchWifi = (CheckBox) findViewById(R.id.switch_wifi);
        llClearcache = (LinearLayout) findViewById(R.id.ll_clearcache);
        tvCache = (TextView) findViewById(R.id.tv_cache);
        tvVersion = (TextView) findViewById(R.id.tv_version);
        tvLogout = (TextView) findViewById(R.id.tv_logout);
        llAbout = (LinearLayout) findViewById(R.id.ll_about);
        llFk = (LinearLayout) findViewById(R.id.ll_fk);
        llUser = (LinearLayout) findViewById(R.id.ll_user);
        llAgreement = (LinearLayout) findViewById(R.id.ll_agreement);
        llCloseAccount = (LinearLayout) findViewById(R.id.ll_close_account);

        switchWifi.setOnClickListener(this);
        llClearcache.setOnClickListener(this);
        llAbout.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        llFk.setOnClickListener(this);
        llUser.setOnClickListener(this);
        llAgreement.setOnClickListener(this);
        llCloseAccount.setOnClickListener(this);

        userBean = SPManager.getUser();
        initView();

    }

    private void initView() {
        GlideImgManager.loadImage(mContext, "" + userBean.getPicImg(),
                R.mipmap.ic_avatar, R.mipmap.ic_avatar, imgAvatar);
        if (userBean.getStudentName() != null) {
            tvUserName.setText(userBean.getStudentName());
        }
        tvUserClass.setText(userBean.getSubjectName());
        tvVersion.setText(App2Utils.getAppVersionName(mContext));
        switchWifi.setChecked(SPManager.getMobileNetSwitch());
        tvCache.setText(getCacheSizes());
    }


    private void clearCaches() {
        GlideCatchUtil.getInstance().cleanCacheDiskSelf();
        FileUtils.deleteFilesInDir(CacheUtils.getDownloadCache());
        FileUtils.deleteFilesInDir(CacheUtils.getAvatarCache());
        FileUtils.deleteFilesInDir(CacheUtils.getVideoScreenshotCache());
        FileUtils.deleteFilesInDir(CacheUtils.getImageCache());
        FileUtils.deleteFilesInDir(CacheUtils.getAppCache());
        //清除完数据后
        tvCache.setText(getCacheSizes());
        ToastUtils.showShort("清理成功");
    }

    private String getCacheSizes() {
        long imageCacheSize = FileUtils.getDirLength(CacheUtils.getImageCache());
        long appCacheSize = FileUtils.getDirLength(CacheUtils.getAppCache());
        return (imageCacheSize == -1 || appCacheSize == -1) ? "" :
                ConvertUtils.byte2FitMemorySize(imageCacheSize + appCacheSize);
    }

    private void showExitDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定要退出登录吗？")
                .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        logout();
                    }
                })
                .setNegativeButton("取消", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        //隐藏dialog
                    }
                });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }

    private void logout() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
        Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity");
        startIntent.putExtra(AUTO_LOGIN,false);
        startActivity(startIntent);
    }

    @Override
    public void onClick(View view) {
        if (R.id.switch_wifi == view.getId()) {
            SPManager.changeMobileNetSwitch(switchWifi.isChecked());
        } else if (R.id.ll_clearcache == view.getId()) {
            DialogUtil.showDialog(this, "确定要清理缓存吗？", true, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    clearCaches();//清理缓存
                }
            });
        } else if (R.id.ll_about == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AboutActivity");
            startActivity(startIntent);
        } else if (R.id.tv_logout == view.getId()) {
            showExitDialog();
        } else if (R.id.ll_fk == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.FeedbackActivity");
            startActivity(startIntent);
        } else if (R.id.ll_user == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");
            startIntent.putExtra(INTENT_WEB,"1");
            startActivity(startIntent);
        } else if (R.id.ll_agreement == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");
            startIntent.putExtra(INTENT_WEB,"2");
            startActivity(startIntent);
        } else if (R.id.ll_close_account == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AccountRemoveActivity");
            startActivity(startIntent);

        }
    }
}
