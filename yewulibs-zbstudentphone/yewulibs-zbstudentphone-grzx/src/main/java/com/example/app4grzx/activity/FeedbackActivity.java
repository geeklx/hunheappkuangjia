package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.sdzn.variants.network.RestApi;
import com.sdzn.core.base.BaseActivity1;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4grzx.R;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.Map;



import okhttp3.RequestBody;
import rx.Subscription;

/**
 *意见反馈
 */

public class FeedbackActivity extends BaseActivity1 implements View.OnClickListener {
    EditText etContent;
    EditText etPhone;
    Button btnCertain;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_feed_back;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入FeedbackActivity成功" );
                }
            }
        }
        etContent = (EditText) findViewById(R.id.et_content);
        etPhone = (EditText) findViewById(R.id.et_phone);
        btnCertain = (Button) findViewById(R.id.btn_certain);
        btnCertain.setOnClickListener(this);
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.statusBarLightMode(this);
    }

    private void setData(){
        Map<String, String> map = new HashMap<>();
        if (etPhone.getText().toString().trim().isEmpty()){
            map.put("phone", "0");
        }else {
            map.put("phone", etPhone.getText().toString());
        }
        map.put("opinionContent", etContent.getText().toString());
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getOpinion(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<Object>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object courses) {
                      ToastUtils.showShort("提交成功");
                      FeedbackActivity.this.finish();
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        ToastUtils.showShort("" + e.getMessage());

                    }
                }, mContext, false));
    }


    @Override
    public void onClick(View v) {
        if (R.id.btn_certain==v.getId()){
            setData();
        }
    }
}
