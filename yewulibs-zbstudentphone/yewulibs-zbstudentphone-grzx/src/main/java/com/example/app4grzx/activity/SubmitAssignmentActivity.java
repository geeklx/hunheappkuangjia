package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.app4grzx.R;
import com.sdzn.core.base.BaseActivity1;


/**
 * zs
 */
public class SubmitAssignmentActivity extends BaseActivity1 {
    private String Titlenumber;
    private TextView submittitle;
    private RecyclerView rvaddpic;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_submit_assignment;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入SubmitAssignmentActivity成功" );
                }
            }
        }
        rvaddpic = findViewById(R.id.rv_add_pic);
        submittitle = findViewById(R.id.submit_title);
        Titlenumber = getIntent().getStringExtra("titlnumber");
        submittitle.setText("填空题：" + Titlenumber);
        initView();
        initData();
    }

    private void initData() {
    }

    private void initView() {

    }

}
