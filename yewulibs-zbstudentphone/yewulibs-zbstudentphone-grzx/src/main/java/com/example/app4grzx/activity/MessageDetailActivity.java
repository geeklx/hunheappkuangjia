package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.example.app4grzx.R;
import com.example.app4grzx.presenter.MessageDetailPresenter;
import com.example.app4grzx.view.MessageDetailView;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.TitleBar;




/**
 * 描述：消息详情
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class MessageDetailActivity extends BaseMVPActivity1<MessageDetailView, MessageDetailPresenter> implements MessageDetailView {

    TitleBar titleBar;
    TextView tvDate;
    TextView tvMessageContent;
    public static final String ID_MESSAGE = "message_id";
    public static final String TYPE_MESSAGE = "message_type";
    public static final String CONTENT = "message_content";
    public static final String ADD_TIME = "message_add_time";
    EmptyLayout emptyLayout;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_messagestatic;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入MessageDetailActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        tvDate = (TextView) findViewById(R.id.tv_date);
        tvMessageContent = (TextView) findViewById(R.id.tv_message_content);
        emptyLayout = (EmptyLayout) findViewById(R.id.empty_layout);
        if (!TextUtils.isEmpty(getIntent().getStringExtra(ID_MESSAGE))) {
            //更改状态
            mPresenter.getMessageDetail(getIntent().getStringExtra(ID_MESSAGE));
        }
        if (!TextUtils.isEmpty(getIntent().getStringExtra(TYPE_MESSAGE))) {
            titleBar.setTitleText(getIntent().getStringExtra(TYPE_MESSAGE));
        }
//        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mPresenter.getMessageDetail(getIntent().getStringExtra(ID_MESSAGE));
//            }
//        });

        if (!TextUtils.isEmpty(getIntent().getStringExtra(CONTENT))) {
            tvMessageContent.setText(getIntent().getStringExtra(CONTENT));

            String addTime = getIntent().getStringExtra(ADD_TIME);
            tvDate.setText(addTime);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }


    }

    @Override
    protected MessageDetailPresenter createPresenter() {
        return new MessageDetailPresenter();
    }

    @Override
    public void getMessageSuccess() {
        //获取未读消息数
        mPresenter.getUnReadMessageCount();
//        if (messageDetailBean != null) {
//            if (!TextUtils.isEmpty(messageDetailBean.getMsgReceive().getContent())) {
//                tvMessageContent.setText(messageDetailBean.getMsgReceive().getContent());
//            }
//            String addTime = TimeUtils.millis2String(messageDetailBean.getMsgReceive().getAddTime(), "yyyy.MM.dd HH:mm");
//            tvDate.setText(addTime);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
//        } else {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
//        }

    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
