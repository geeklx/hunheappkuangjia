package com.sdzn.variants.network.interceptor;

import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.variants.network.NewApiEqualUtil;
import com.sdzn.variants.network.SPToken;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okio.Buffer;
import okio.BufferedSource;

/**
 * 描述：
 * - okHttp拦截器，用于添加公共参数
 */
public class CommonInterceptor implements Interceptor {
    private final Charset UTF8 = Charset.forName("UTF-8");
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request oldRequest = chain.request();
        String url = oldRequest.url().toString();
        // 添加新的参数
        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host());
        String token = SPToken.getToken();

        Request.Builder builder = oldRequest.newBuilder();
        if (!TextUtils.isEmpty(token)&& NewApiEqualUtil.isNoToken(url)) {
            builder.addHeader("Authorization", "Bearer " + token);
        }
        Request request=builder.method(oldRequest.method(), oldRequest.body())
                    .url(authorizedUrlBuilder.build())
                    .build();
        Response response = chain.proceed(request);

        /*配置log*/
        RequestBody requestBody = oldRequest.body();
        String body = null;
        if (requestBody != null) {
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            Charset charset = UTF8;
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF8);
            }
            body = buffer.readString(charset);
        }

        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
        ResponseBody responseBody = response.body();
        String rBody = null;
        if (HttpHeaders.hasBody(response)) {
            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE); // Buffer the entire body.
            Buffer buffer = source.buffer();
            Charset charset = UTF8;
            MediaType contentType = responseBody.contentType();
            if (contentType != null) {
                try {
                    charset = contentType.charset(UTF8);
                } catch (UnsupportedCharsetException e) {
                    e.printStackTrace();
                }
            }
            rBody = buffer.clone().readString(charset);
        }

        MyLogUtil.d("HTTP_LOG", "---------------HTTP_LOG请求start--------------");
        MyLogUtil.d("HTTP_LOG", String.format("发送请求"));
        MyLogUtil.d("HTTP_LOG", String.format("method：%s", oldRequest.method()));
        MyLogUtil.d("HTTP_LOG", String.format("url：%s", oldRequest.url()));
        MyLogUtil.d("HTTP_LOG", String.format("请求headers: %s", request.headers()));
        MyLogUtil.d("HTTP_LOG", String.format("请求body: %s", body));
        MyLogUtil.d("HTTP_LOG", String.format("收到响应 %s %s %ss", response.code(), response.message(), tookMs));
        MyLogUtil.d("HTTP_LOG", String.format("响应body：%s", rBody));
        MyLogUtil.d("HTTP_LOG", "---------------HTTP_LOG请求end----------------");


        return response;
    }
}