package com.sdzn.variants.network.api;

import android.content.Intent;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.network.SPManager;

import rx.functions.Func1;

/**
 * 描述：
 * - 通用请求返回结果处理
 * 新接口  code=1时，且 errorCode=4002 时 登录失效
 */
public class ResponseNewFuncLogin<T> implements Func1<ResultBean<T>, T> {

    private static final int CODE = 401;
    private static final int CODE_LOSE = 2000;
    //此处逻辑根据约定报文进行修改，实现逻辑即可
    @Override
    public T call(ResultBean<T> httpResult) {
        if (0==httpResult.getCode()) {
            LogUtils.i("LoginResult: " + httpResult.getResult());
            return httpResult.getResult();
        } else {
            if (CODE == httpResult.getCode() || CODE_LOSE == httpResult.getCode()) {//登录失效
                SPManager.changeLogin(App2.get(), false);
                Intent intent1 = new Intent("com.sdzn.pkt.student.phone.hs.act.MainActivity");
                intent1.putExtra("autoLogin", false);
                App2.get().startActivity(intent1);
//                IntentController.toMain(App.mContext,
                throw new ApiException(new Throwable(httpResult.getMessage()),httpResult.getCode());

            } else {
                LogUtils.e("LoginError: " + httpResult.getCode() + ", " + httpResult.getMessage() + ", " + httpResult.getResult());
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());
            }
        }
    }
}