package com.sdzn.pkt.student.phone.wxapi;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import com.sdzn.core.base.BaseActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.SweetAlertDialog;
import com.example.app4public.event.MineCourseEvent;
import com.example.app4public.event.OrderPayEvent;
import com.sdzn.pkt.student.phone.BuildConfig3;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import org.greenrobot.eventbus.EventBus;


public class WXPayEntryActivity extends BaseActivity1 implements IWXAPIEventHandler {

    // IWXAPI 是第三方app和微信通信的openapi接口
    private IWXAPI api;

    @Override
    protected int getLayoutResource() {
        return 0;
    }

    @Override
    protected void onInit(Bundle bundle) {
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, BuildConfig3.WXAPP_ID, false);

        //注意：
        //第三方开发者如果使用透明界面来实现WXEntryActivity，需要判断handleIntent的返回值，如果返回值为false，则说明入参不合法未被SDK处理，应finish当前透明界面，避免外部通过传递非法参数的Intent导致停留在透明界面，引起用户的疑惑
        try {
            boolean isPass = api.handleIntent(getIntent(), this);
            if (isPass) {
                SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext)
                        .setMessage("支付失败，请稍候重试")
                        .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                            @Override
                            public void onClick(Dialog dialog, int which) {
                                WXPayEntryActivity.this.finish();
                            }
                        });
                builder.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        api.handleIntent(intent, this);
    }


    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq baseReq) {

    }

    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    EventBus.getDefault().post(new OrderPayEvent(true));
                    EventBus.getDefault().post(new MineCourseEvent(true));
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    EventBus.getDefault().post(new OrderPayEvent(false));
                    ToastUtils.showShort("支付结果：用户取消了支付");
                    break;
                default:
                    EventBus.getDefault().post(new OrderPayEvent(false));
                    ToastUtils.showShort("支付结果：支付失败");
                    break;
            }
        }
        WXPayEntryActivity.this.finish();
    }
}