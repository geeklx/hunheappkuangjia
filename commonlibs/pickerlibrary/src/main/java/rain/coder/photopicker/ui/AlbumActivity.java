package rain.coder.photopicker.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rain.coder.library.R;
import rain.coder.photopicker.BaseActivity;
import rain.coder.photopicker.adapter.PhotoGalleryAdapter;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.bean.PhotoDirectory;
import rain.coder.photopicker.bean.PhotoPickBean;
import rain.coder.photopicker.controller.PhotoPickConfig;
import rain.coder.photopicker.loader.MediaStoreHelper;

import static rain.coder.photopicker.controller.PhotoPickConfig.EXTRA_PICK_BUNDLE;
import static rain.coder.photopicker.controller.PhotoPickConfig.PICK_REQUEST_CODE;

/**
 * Created by wangc on 2018/3/13 0013.
 */
public class AlbumActivity extends BaseActivity {

    private PhotoGalleryAdapter galleryAdapter;
    private PhotoPickBean pickBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        Bundle bundle = getIntent().getBundleExtra(PhotoPickConfig.EXTRA_PICK_BUNDLE);
        if (bundle == null) {
            throw new NullPointerException("bundle is null,please init it");
        }
        pickBean = bundle.getParcelable(PhotoPickConfig.EXTRA_PICK_BEAN);
        if (pickBean == null) {
            finish();
            return;
        }

        init(bundle);
    }

    private void init(final Bundle bundle) {
        TextView tvCancel = (TextView) this.findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RecyclerView gallery_rv = (RecyclerView) this.findViewById(R.id.gallery_rcl);
        gallery_rv.setLayoutManager(new LinearLayoutManager(this));
        galleryAdapter = new PhotoGalleryAdapter(this);
        gallery_rv.setAdapter(galleryAdapter);

        galleryAdapter.setOnItemClickListener(new PhotoGalleryAdapter.OnItemClickListener() {
            @Override
            public void onClick(String title, ArrayList<Photo> photos) {
                bundle.putParcelableArrayList("photos", photos);
                bundle.putString("title", title);
                Intent intent = new Intent();
                intent.putExtra(EXTRA_PICK_BUNDLE, bundle);
                intent.setClass(AlbumActivity.this, PhotoPickActivity.class);
                AlbumActivity.this.startActivityForResult(intent, PICK_REQUEST_CODE);
                AlbumActivity.this.overridePendingTransition(R.anim.image_pager_enter_animation, 0);
            }
        });

        MediaStoreHelper.getPhotoDirs(this, new MediaStoreHelper.PhotosResultCallback() {
            @Override
            public void onResultCallback(final List<PhotoDirectory> directories) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        galleryAdapter.refresh(directories);
                    }
                });
            }
        });
    }
}
