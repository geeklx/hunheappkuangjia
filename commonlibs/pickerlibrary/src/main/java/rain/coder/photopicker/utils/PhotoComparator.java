package rain.coder.photopicker.utils;

import java.util.Comparator;

import rain.coder.photopicker.bean.Photo;

/**
 * 排序器
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class PhotoComparator implements Comparator<Photo> {
    @Override
    public int compare(Photo p1, Photo p2) {
        long position1 = p1.getPosition();
        long position2 = p2.getPosition();
        if (position1 > position2) {
            return 1;
        } else if (position1 < position2) {
            return -1;
        } else {
            return 0;
        }
    }
}
