package rain.coder.photopicker.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Describe :全部照片
 * Created by Rain on 17-4-28.
 */
public class Photo implements Parcelable {

    private int id;
    private String path;
    private long size;//byte 字节
    private int position;

    public Photo(int id, String path) {
        this.id = id;
        this.path = path;
    }

    public Photo(int id, String path, long size, int position) {
        this.id = id;
        this.path = path;
        this.size = size;
        this.position = position;
    }

    public Photo(int id, String path, long size) {
        this.id = id;
        this.path = path;
        this.size = size;
        this.position = position;
    }

    public Photo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.path);
        dest.writeLong(this.size);
        dest.writeInt(this.position);
    }

    protected Photo(Parcel in) {
        this.id = in.readInt();
        this.path = in.readString();
        this.size = in.readLong();
        this.position = in.readInt();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Photo) {
            Photo photo = (Photo) obj;
            if (photo.getPath() != null) {
                return photo.getPath().equals(getPath());
            }
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return getPath().hashCode();
    }
}
