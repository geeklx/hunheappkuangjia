package rain.coder.photopicker.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rain.coder.library.R;
import rain.coder.photopicker.BaseActivity;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.bean.PhotoPreviewBean;
import rain.coder.photopicker.controller.PhotoPickConfig;
import rain.coder.photopicker.controller.PhotoPreviewConfig;
import rain.coder.photopicker.weidget.HackyViewPager;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Describe :仿微信图片预览
 * Email:baossrain99@163.com
 * Created by Rain on 17-5-3.
 */
public class PhotoPreviewActivity extends BaseActivity implements PhotoViewAttacher.OnPhotoTapListener {

    private static final String TAG = "PhotoPreviewActivity";

    private ArrayList<Photo> photos;    //全部图片集合
    private ArrayList<Photo> selectPhotos;     //选中的图片集合
    private ImageView checkbox;
    private TextView tvConfirm;
    private int pos;
    private int maxPickSize;            //最大选择个数
    private boolean isChecked = false;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        Bundle bundle = getIntent().getBundleExtra(PhotoPreviewConfig.EXTRA_BUNDLE);
        if (bundle == null) {
            throw new NullPointerException("bundle is null,please init it");
        }
        PhotoPreviewBean bean = bundle.getParcelable(PhotoPreviewConfig.EXTRA_BEAN);
        if (bean == null) {
            finish();
            return;
        }
        photos = bean.getPhotos();
        if (photos == null || photos.isEmpty()) {
            finish();
            return;
        }
        maxPickSize = bean.getMaxPickSize();
        selectPhotos = bean.getSelectPhotos();
        final int beginPosition = bean.getPosition();
        setContentView(R.layout.activity_photo_select);

        checkbox = (ImageView) findViewById(R.id.checkbox);
        tvConfirm = (TextView) findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backTo(false);
            }
        });
        TextView tvBack = (TextView) findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backTo(true);
            }
        });
        setCheckbox();
        HackyViewPager viewPager = (HackyViewPager) findViewById(R.id.pager);

        //照片滚动监听，更改ToolBar数据
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                setCheckbox();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //选中
        checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectPhotos == null) {
                    selectPhotos = new ArrayList<>();
                }
                if (selectPhotos.contains(photos.get(pos))) {
                    selectPhotos.remove(photos.get(pos));
                    checkbox.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.weixuanzhong));
                } else {
                    if (maxPickSize == selectPhotos.size()) {
                        checkbox.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.weixuanzhong));
                        return;
                    }
                    selectPhotos.add(photos.get(pos));
                    checkbox.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.xuanzhong));
                }
                setConfirm();
            }
        });

        viewPager.setAdapter(new ImagePagerAdapter());
        viewPager.setCurrentItem(beginPosition);
    }

    private void setCheckbox() {
        if (selectPhotos != null && selectPhotos.contains(photos.get(pos))) {
            checkbox.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.xuanzhong));
            if (pos == 1 && selectPhotos.contains(photos.get(pos - 1))) {
                checkbox.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.xuanzhong));
            }
        } else {
            checkbox.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.weixuanzhong));
        }
        setConfirm();
    }

    private void setConfirm() {
        if (selectPhotos != null && selectPhotos.size() > 0) {
            tvConfirm.setText("确定(" + selectPhotos.size() + ")");
            tvConfirm.setTextColor(0xFFFFFFFF);
            tvConfirm.setBackground(getResources().getDrawable(R.drawable.selector_confirm));
        } else {
            tvConfirm.setText("确定");
            tvConfirm.setTextColor(0xFFB1B5BA);
            tvConfirm.setBackground(null);
        }
    }

    private void backTo(boolean isBackPressed) {
        Intent intent = new Intent();
        intent.putExtra("isBackPressed", isBackPressed);
        intent.putParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST, selectPhotos);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    //单击图片时操作
    @Override
    public void onPhotoTap(View view, float x, float y) {
        finish();
    }

    @Override
    public void onOutsidePhotoTap() {

    }

    private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {

            String bigImgUrl = photos.get(position).getPath();
            View view = LayoutInflater.from(PhotoPreviewActivity.this).inflate(R.layout.item_photo_preview, container, false);
            final PhotoView imageView = (PhotoView) view.findViewById(R.id.iv_media_image);
            imageView.setOnPhotoTapListener(PhotoPreviewActivity.this);


            PhotoPickConfig.imageLoader.displayImage(PhotoPreviewActivity.this, bigImgUrl, imageView, false);

            container.addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.image_pager_exit_animation);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            backTo(true);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}

/*
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *     ┃　　　┃
 *     ┃　　　┃
 *     ┃　　　┗━━━┓
 *     ┃　　　　　　　┣┓
 *     ┃　　　　　　　┏┛
 *     ┗┓┓┏━┳┓┏┛
 *       ┃┫┫　┃┫┫
 *       ┗┻┛　┗┻┛
 *        神兽保佑
 *        代码无BUG!
 */
