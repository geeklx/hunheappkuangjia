package rain.coder.photopicker.ui;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

import rain.coder.library.R;
import rain.coder.photopicker.adapter.PhotoGalleryAdapter;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.bean.PhotoDirectory;

/**
 * 相册弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class AlbumPop extends PopupWindow {

    private Activity activity;
    private PhotoGalleryAdapter galleryAdapter;
    private LinearLayout llContent;
    private int height;
    private OnItemClickListener onItemClickListener;

    public AlbumPop(Activity activity, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.onItemClickListener = onItemClickListener;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_album, null);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        setAlbum(contentView);
    }

    private void setAlbum(View contentView) {
        RecyclerView gallery_rv = (RecyclerView) contentView.findViewById(R.id.gallery_rcl);
        llContent = (LinearLayout) contentView.findViewById(R.id.llContent);
        llContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                llContent.getLayoutParams().height = height;
                llContent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        llContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        gallery_rv.setLayoutManager(new LinearLayoutManager(activity));
        galleryAdapter = new PhotoGalleryAdapter(activity);
        gallery_rv.setAdapter(galleryAdapter);
        galleryAdapter.setOnItemClickListener(new PhotoGalleryAdapter.OnItemClickListener() {
            @Override
            public void onClick(String title, ArrayList<Photo> photos) {
                onItemClickListener.onClick(photos);
                dismiss();
            }
        });
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent, List<PhotoDirectory> directories, int height) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            galleryAdapter.refresh(directories);
            this.height = height;
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }

    public interface OnItemClickListener {
        void onClick(ArrayList<Photo> photos);
    }
}
