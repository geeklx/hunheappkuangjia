package com.example.baselibrary.emptyview.networkview;

public interface NetconListener {
    void net_con_phone();
    void net_con_wifi();
    void net_con_none();
}
