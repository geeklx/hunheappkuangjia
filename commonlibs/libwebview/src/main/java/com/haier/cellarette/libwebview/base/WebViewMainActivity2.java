package com.haier.cellarette.libwebview.base;

import android.os.Bundle;
import android.text.TextUtils;

//import android.support.annotation.Nullable;
/*竖屏webView*/
public class WebViewMainActivity2 extends WebViewActivity {

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String validateurl = getIntent().getStringExtra("validateurl");
        if (TextUtils.isEmpty(validateurl)) {
            finish();
            return;
        }
        url = validateurl;
//        setup();//必须删除bufen
//        mPresenter = PresenterHelper.create(AdPresenter.class, this);
//        mPresenter.getAdUrlById(adId);
        loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
