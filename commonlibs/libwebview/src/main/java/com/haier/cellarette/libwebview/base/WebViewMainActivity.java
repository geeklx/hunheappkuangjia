package com.haier.cellarette.libwebview.base;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.sdzn.fzx.student.libutils.util.Log;

//import android.support.annotation.Nullable;

/*横屏webView*/
public class WebViewMainActivity extends WebViewActivity {

    private String url, AppToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String validateurl = getIntent().getStringExtra("validateurl");
        AppToken = getIntent().getStringExtra("AppToken");
        if (TextUtils.isEmpty(validateurl)) {
            finish();
            return;
        }
        url = validateurl;
        mWebView.loadUrl(url);
    }



    @Override
    protected void onDestroy() {
        mWebView.destroy();
        super.onDestroy();
    }
}
