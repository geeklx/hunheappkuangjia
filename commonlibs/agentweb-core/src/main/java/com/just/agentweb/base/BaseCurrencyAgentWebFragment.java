package com.just.agentweb.base;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.LogUtils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.AgentWebConfig;
import com.just.agentweb.AgentWebSettingsImpl;
import com.just.agentweb.AgentWebUIControllerImplBase;
import com.just.agentweb.CoolIndicatorLayout;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.IAgentWebSettings;
import com.just.agentweb.IWebLayout;
import com.just.agentweb.MiddlewareWebChromeBase;
import com.just.agentweb.MiddlewareWebClientBase;
import com.just.agentweb.PermissionInterceptor;
import com.just.agentweb.R;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;
import com.sdzn.fzx.student.libutils.app.App2;

/**
 * Created by cenxiaozhong on 2017/7/22.
 * source code  https://github.com/Justson/AgentWeb
 */

public abstract class BaseCurrencyAgentWebFragment extends Fragment {

    protected AgentWeb mAgentWeb;
    private MiddlewareWebChromeBase mMiddleWareWebChrome;
    private MiddlewareWebClientBase mMiddleWareWebClient;
//    private ErrorLayoutEntity mErrorLayoutEntity;
    private AgentWebUIControllerImplBase mAgentWebUIController;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (getAgentWebParent() == null) {
            return;
        }
//        ErrorLayoutEntity mErrorLayoutEntity = getErrorLayoutEntity();
        CoolIndicatorLayout mCoolIndicatorLayout = new CoolIndicatorLayout(getActivity());
        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(getAgentWebParent(), new ViewGroup.LayoutParams(-1, -1))
//                .useDefaultIndicator(getIndicatorColor(), getIndicatorHeight())
                .setCustomIndicator(mCoolIndicatorLayout)
                .setWebChromeClient(getWebChromeClient())
                .setWebViewClient(getWebViewClient())
                .setWebView(getWebView())
                .setPermissionInterceptor(getPermissionInterceptor())
                .setWebLayout(getWebLayout())
                .setAgentWebUIController(getAgentWebUIController())
                .interceptUnkownUrl()
                .setOpenOtherPageWays(getOpenOtherAppWay())
                .useMiddlewareWebChrome(getMiddleWareWebChrome())
                .useMiddlewareWebClient(getMiddleWareWebClient())
                .setAgentWebWebSettings(getAgentWebSettings())
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .createAgentWeb()
                .ready()
                .go(getUrl());
        AgentWebConfig.debug();
        getJsInterface();
        super.onViewCreated(view, savedInstanceState);
    }


//    protected @NonNull
//    ErrorLayoutEntity getErrorLayoutEntity() {
//        if (this.mErrorLayoutEntity == null) {
//            this.mErrorLayoutEntity = new ErrorLayoutEntity();
//        }
//        return mErrorLayoutEntity;
//    }

//    protected AgentWeb getAgentWeb() {
//        return this.mAgentWeb;
//    }


//    protected static class ErrorLayoutEntity {
//        private int layoutRes = R.layout.agentweb_error_page;
//        private int reloadId;
//
//        public void setLayoutRes(int layoutRes) {
//            this.layoutRes = layoutRes;
//            if (layoutRes <= 0) {
//                layoutRes = -1;
//            }
//        }
//
//        public void setReloadId(int reloadId) {
//            this.reloadId = reloadId;
//            if (reloadId <= 0) {
//                reloadId = -1;
//            }
//        }
//    }

    @Override
    public void onPause() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause();
        }
        super.onPause();

    }

    @Override
    public void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();
        }
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
//        if (mAgentWeb != null) {
//            mAgentWeb.clearWebCache();//清楚缓存
//            mAgentWeb.destroy();
//            AgentWebConfig.clearDiskCache(App2.get());
//        }
        super.onDestroy();
    }

    private String gettitle_content = "";

    @Nullable
    public String getTitle1() {
        return gettitle_content;
    }
    @Nullable
    protected void getJsInterface() {
    }

    @Nullable
    public String getUrl() {
        return null;
    }

    /**
     * 加载失败错误页的显示
     */
    public void loadWebSite(String url) {
        if (mAgentWeb != null) {
            LogUtils.e(url);
            mAgentWeb.getWebCreator().getWebView().loadUrl(url);
        }
    }

    public @Nullable
    IAgentWebSettings getAgentWebSettings() {
        return AgentWebSettingsImpl.getInstance();
    }


    protected abstract @NonNull
    ViewGroup getAgentWebParent();

    protected @Nullable
    WebChromeClient getWebChromeClient() {
        return null;
    }

    protected @ColorInt
    int getIndicatorColor() {
        return -1;
    }

    protected int getIndicatorHeight() {
        return -1;
    }

    protected @Nullable
    WebViewClient getWebViewClient() {
        return null;
    }


    protected @Nullable
    WebView getWebView() {
        return null;
    }

    protected @Nullable
    IWebLayout getWebLayout() {
        return null;
    }

    protected @Nullable
    PermissionInterceptor getPermissionInterceptor() {
        return null;
    }

    public @Nullable
    AgentWebUIControllerImplBase getAgentWebUIController() {
        return null;
    }

    public @Nullable
    DefaultWebClient.OpenOtherPageWays getOpenOtherAppWay() {
        return null;
    }

    protected @NonNull
    MiddlewareWebChromeBase getMiddleWareWebChrome() {
        return this.mMiddleWareWebChrome = new MiddlewareWebChromeBase() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                setTitle(view, title);
            }
        };
    }

    protected void setTitle(WebView view, String title) {

    }

    protected @NonNull
    MiddlewareWebClientBase getMiddleWareWebClient() {
        return this.mMiddleWareWebClient = new MiddlewareWebClientBase() {
        };
    }
}
