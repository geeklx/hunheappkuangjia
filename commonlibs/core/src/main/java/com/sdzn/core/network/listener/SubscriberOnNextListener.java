package com.sdzn.core.network.listener;

public interface SubscriberOnNextListener<T> {
    void onNext(T t);

    void onFail(Throwable e);
}