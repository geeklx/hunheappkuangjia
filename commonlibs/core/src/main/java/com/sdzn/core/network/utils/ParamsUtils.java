package com.sdzn.core.network.utils;

import android.util.Log;

import java.util.Map;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class ParamsUtils {
    //    public static void logParams(Map<String,String> params){
//        StringBuilder sb = new StringBuilder();
//        for (Map.Entry<String, String> entry : params.entrySet()) {
//            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
//        }
//        Log.i("params", sb.toString());
//    }
    public static void logParams(Map<String, Integer> params) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Integer> entry : params.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        Log.i("params", sb.toString());
    }
}
