package com.sdzn.core.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;

import androidx.annotation.NonNull;

import com.sdzn.core.R;

/**
 * 描述：
 * - 进度dialog
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class MProgressDialog extends Dialog {
    private Context mContext;

    public MProgressDialog(@NonNull Context context) {
        this(context, 0);
    }

    private MProgressDialog(Context context, int theme) {
        super(context, R.style.ProgressDialogStyle);
        this.mContext = context;
        init();
    }

    private void init() {
        setTitle("");
        setContentView(R.layout.dialog_progress);
        // 按返回键是否取消
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        // 设置居中
        Window window = getWindow();
        if (window != null) {
            window.getAttributes().gravity = Gravity.CENTER;
//            WindowManager.LayoutParams lp = window.getAttributes();
//            // 设置背景层透明度
//            lp.dimAmount = 0.2f;
//            window.setAttributes(lp);
//            window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        }
    }


    @Override
    public void show() {
        if (!isShowing() && !((Activity) mContext).isFinishing()) {
            super.show();
        }
    }
}