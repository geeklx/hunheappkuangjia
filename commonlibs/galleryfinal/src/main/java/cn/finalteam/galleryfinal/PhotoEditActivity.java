/*
 * Copyright (C) 2014 pengjianbo(pengjianbosoft@gmail.com), Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package cn.finalteam.galleryfinal;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import cn.finalteam.galleryfinal.model.PhotoInfo;
import cn.finalteam.galleryfinal.model.PhotoTempModel;
import cn.finalteam.galleryfinal.utils.ILogger;
import cn.finalteam.galleryfinal.utils.Utils;
import cn.finalteam.galleryfinal.widget.crop.CropImageActivity;
import cn.finalteam.galleryfinal.widget.crop.CropImageView;
import cn.finalteam.toolsfinal.StringUtils;
import cn.finalteam.toolsfinal.io.FileUtils;
import cn.finalteam.toolsfinal.io.FilenameUtils;

/**
 * Desction:图片裁剪
 * Author:pengjianbo
 * Date:15/10/10 下午5:40
 */
public class PhotoEditActivity extends CropImageActivity implements View.OnClickListener {

    public static final String CROP_PHOTO_ACTION = "crop_photo_action";
    public static final String EDIT_PHOTO_ACTION = "edit_photo_action";

    public static final String SELECT_MAP = "select_map";
    private static final int CROP_SUC = 1;//裁剪成功
    private static final int CROP_FAIL = 2;//裁剪失败
    public static final int ROTATE_UPDATE_PATH = 4;
    public static final int CROP_UPDATE_PATH = 5;

    private TextView mIvBack;
    private TextView mIvRotate;
    private CropImageView mIvCropPhoto;
    private TextView mTvEmptyView;
    private TextView mFabCrop;

    private ArrayList<PhotoInfo> mPhotoList;
    private ProgressDialog mProgressDialog;
    private boolean mRotating;

    private ArrayList<PhotoInfo> mSelectPhotoList;
    private LinkedHashMap<Integer, PhotoTempModel> mPhotoTempMap;
    private File mEditPhotoCacheFile;

    private Drawable mDefaultDrawable;


    private boolean mCropPhotoAction;//裁剪图片动作
    private boolean mEditPhotoAction;//编辑图片动作

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("selectPhotoMap", mSelectPhotoList);
        outState.putSerializable("editPhotoCacheFile", mEditPhotoCacheFile);
        outState.putSerializable("photoTempMap", mPhotoTempMap);

        outState.putBoolean("rotating", mRotating);

        outState.putBoolean("cropPhotoAction", mCropPhotoAction);
        outState.putBoolean("editPhotoAction", mEditPhotoAction);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSelectPhotoList = (ArrayList<PhotoInfo>) getIntent().getSerializableExtra("selectPhotoMap");
        mEditPhotoCacheFile = (File) savedInstanceState.getSerializable("editPhotoCacheFile");
        mPhotoTempMap = new LinkedHashMap<>((HashMap<Integer, PhotoTempModel>) getIntent().getSerializableExtra("results"));

        mRotating = savedInstanceState.getBoolean("rotating");

        mCropPhotoAction = savedInstanceState.getBoolean("cropPhotoAction");
        mEditPhotoAction = savedInstanceState.getBoolean("editPhotoAction");
    }


    @SuppressWarnings("HandlerLeak")
    private Handler mHanlder = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == CROP_SUC) {
                String path = (String) msg.obj;
                PhotoInfo photoInfo = mPhotoList.get(0);
                try {
                    for (Map.Entry<Integer, PhotoTempModel> entry : mPhotoTempMap.entrySet()) {
                        if (entry.getKey() == photoInfo.getPhotoId()) {
                            PhotoTempModel tempModel = entry.getValue();
                            tempModel.setSourcePath(path);
                            tempModel.setOrientation(0);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                toast(getString(R.string.crop_suc));

                Message message = mHanlder.obtainMessage();
                message.what = CROP_UPDATE_PATH;
                message.obj = path;
                mHanlder.sendMessage(message);

            } else if (msg.what == CROP_FAIL) {
                toast(getString(R.string.crop_fail));
            } else if (msg.what == CROP_UPDATE_PATH) {
                if (mPhotoList.get(0) != null) {
                    PhotoInfo photoInfo = mPhotoList.get(0);
                    String path = (String) msg.obj;
                    //photoInfo.setThumbPath(path);
                    try {
                        for (PhotoInfo info : mSelectPhotoList) {
                            if (info != null && info.getPhotoId() == photoInfo.getPhotoId()) {
                                info.setPhotoPath(path);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    photoInfo.setPhotoPath(path);
                    resultAction();
                }
            } else if (msg.what == ROTATE_UPDATE_PATH) {
                if (mPhotoList.get(0) != null) {
                    PhotoInfo photoInfo = mPhotoList.get(0);
                    String path = (String) msg.obj;
                    //photoInfo.setThumbPath(path);
                    try {
                        for (PhotoInfo info : mSelectPhotoList) {
                            if (info != null && info.getPhotoId() == photoInfo.getPhotoId()) {
                                info.setPhotoPath(path);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    photoInfo.setPhotoPath(path);

                    loadImage(photoInfo);
                }

                if (GalleryFinal.getFunctionConfig().isForceCrop() && !GalleryFinal.getFunctionConfig().isForceCropEdit()) {
                    resultAction();
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (GalleryFinal.getFunctionConfig() == null) {
            resultFailureDelayed(getString(R.string.please_reopen_gf));
        } else {
            setContentView(R.layout.gf_activity_photo_edit);
            mDefaultDrawable = getResources().getDrawable(R.drawable.ic_gf_default_photo);

            mSelectPhotoList = (ArrayList<PhotoInfo>) getIntent().getSerializableExtra(SELECT_MAP);
            mCropPhotoAction = this.getIntent().getBooleanExtra(CROP_PHOTO_ACTION, false);
            mEditPhotoAction = this.getIntent().getBooleanExtra(EDIT_PHOTO_ACTION, false);

            if (mSelectPhotoList == null) {
                mSelectPhotoList = new ArrayList<>();
            }
            mPhotoTempMap = new LinkedHashMap<>();
            mPhotoList = new ArrayList<>(mSelectPhotoList);

            mEditPhotoCacheFile = GalleryFinal.getCoreConfig().getEditPhotoCacheFolder();

            if (mPhotoList == null) {
                mPhotoList = new ArrayList<>();
            }

            for (PhotoInfo info : mPhotoList) {
                mPhotoTempMap.put(info.getPhotoId(), new PhotoTempModel(info.getPhotoPath()));
            }

            findViews();
            setListener();

            try {
                File nomediaFile = new File(mEditPhotoCacheFile, ".nomedia");
                if (!nomediaFile.exists()) {
                    nomediaFile.createNewFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            initCrop(mIvCropPhoto, false, 0, 0);
            if (mPhotoList.size() > 0) {
                loadImage(mPhotoList.get(0));
            }
        }
    }

    private void findViews() {
        mIvCropPhoto = findViewById(R.id.iv_crop_photo);
        mIvBack = findViewById(R.id.iv_back);
        mTvEmptyView = findViewById(R.id.tv_empty_view);
        mFabCrop = findViewById(R.id.fab_crop);
        mIvRotate = findViewById(R.id.iv_rotate);
    }

    private void setListener() {
        mIvBack.setOnClickListener(this);
        mFabCrop.setOnClickListener(this);
        mIvRotate.setOnClickListener(this);
    }

    private void loadImage(PhotoInfo photo) {
        mTvEmptyView.setVisibility(View.GONE);
        mIvCropPhoto.setVisibility(View.VISIBLE);

        String path = "";
        if (photo != null) {
            path = photo.getPhotoPath();
        }
        if (GalleryFinal.getFunctionConfig().isCrop()) {
            setSourceUri(Uri.fromFile(new File(path)));
        }

        GalleryFinal.getCoreConfig().getImageLoader().displayImage(this, path, mIvCropPhoto, mDefaultDrawable);
    }

    @Override
    public void setCropSaveSuccess(final File file) {
        Message message = mHanlder.obtainMessage();
        message.what = CROP_SUC;
        message.obj = file.getAbsolutePath();
        mHanlder.sendMessage(message);
    }

    @Override
    public void setCropSaveException(Throwable throwable) {
        mHanlder.sendEmptyMessage(CROP_FAIL);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.fab_crop) {
            if (mPhotoList.size() == 0) {
                return;
            }
            System.gc();
            PhotoInfo photoInfo = mPhotoList.get(0);
            try {
                String ext = FilenameUtils.getExtension(photoInfo.getPhotoPath());
                File toFile;
                if (GalleryFinal.getFunctionConfig().isCropReplaceSource()) {
                    toFile = new File(photoInfo.getPhotoPath());
                } else {
                    toFile = new File(mEditPhotoCacheFile, Utils.getFileName(photoInfo.getPhotoPath()) + "_crop." + ext);
                }

                FileUtils.mkdirs(toFile.getParentFile());
                onSaveClicked(toFile);//保存裁剪
            } catch (Exception e) {
                ILogger.e(e);
            }
        } else if (id == R.id.iv_rotate) {
            rotatePhoto();
        } else if (id == R.id.iv_back) {
            finish();
        }
    }

    private void resultAction() {
        resultData(mSelectPhotoList);
    }

    public void onDisplayImageSuccess() {
        PhotoInfo photoInfo = mPhotoList.get(0);
        String ext = FilenameUtils.getExtension(photoInfo.getPhotoPath());
        if (StringUtils.isEmpty(ext) || !(ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("jpeg"))) {
            toast(getString(R.string.edit_letoff_photo_format));
            return;
        }
        setCropEnabled(true);
    }

    /**
     * 图片旋转
     */
    @SuppressLint("StaticFieldLeak")
    private void rotatePhoto() {
        if (mPhotoList.size() > 0 && mPhotoList.get(0) != null && !mRotating) {
            final PhotoInfo photoInfo = mPhotoList.get(0);
            final String ext = FilenameUtils.getExtension(photoInfo.getPhotoPath());
            if (StringUtils.isEmpty(ext) || !(ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("jpeg"))) {
                toast(getString(R.string.edit_letoff_photo_format));
                return;
            }
            mRotating = true;
            final PhotoTempModel photoTempModel = mPhotoTempMap.get(photoInfo.getPhotoId());
            if (photoTempModel == null) {
                return;
            }
            final String path = photoTempModel.getSourcePath();

            File file;
            if (GalleryFinal.getFunctionConfig().isRotateReplaceSource()) { //裁剪覆盖源文件
                file = new File(path);
            } else {
                file = new File(mEditPhotoCacheFile, Utils.getFileName(path) + "_rotate." + ext);
            }

            final File rotateFile = file;
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mTvEmptyView.setVisibility(View.VISIBLE);
                    mProgressDialog = ProgressDialog.show(PhotoEditActivity.this, "", getString(R.string.waiting), true, false);
                }

                @Override
                protected Bitmap doInBackground(Void... params) {
                    int orientation;
                    if (GalleryFinal.getFunctionConfig().isRotateReplaceSource()) {
                        orientation = 90;
                    } else {
                        orientation = photoTempModel.getOrientation() + 90;
                    }
                    Bitmap bitmap = Utils.rotateBitmap(path, orientation);
                    if (bitmap != null) {
                        Bitmap.CompressFormat format;
                        if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("jpeg")) {
                            format = Bitmap.CompressFormat.JPEG;
                        } else {
                            format = Bitmap.CompressFormat.PNG;
                        }
                        Utils.saveBitmap(bitmap, format, rotateFile);
                    }
                    return bitmap;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                    if (bitmap != null) {
                        bitmap.recycle();

                        mTvEmptyView.setVisibility(View.GONE);

                        if (!GalleryFinal.getFunctionConfig().isRotateReplaceSource()) {
                            int orientation = photoTempModel.getOrientation() + 90;
                            if (orientation == 360) {
                                orientation = 0;
                            }
                            photoTempModel.setOrientation(orientation);
                        }

                        Message message = mHanlder.obtainMessage();
                        message.what = ROTATE_UPDATE_PATH;
                        message.obj = rotateFile.getAbsolutePath();
                        mHanlder.sendMessage(message);
                    } else {
                        mTvEmptyView.setText(R.string.no_photo);
                    }
                    loadImage(photoInfo);
                    mRotating = false;
                }
            }.execute();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recycleImageView(mIvCropPhoto);
    }

    private void recycleImageView(View view) {
        if (view == null) return;
        if (view instanceof ImageView) {
            Drawable drawable = ((ImageView) view).getDrawable();
            if (drawable instanceof BitmapDrawable) {
                Bitmap bmp = ((BitmapDrawable) drawable).getBitmap();
                if (bmp != null && !bmp.isRecycled()) {
                    ((ImageView) view).setImageBitmap(null);
                    bmp.recycle();
                    bmp = null;
                }
            }
        }
    }
}
