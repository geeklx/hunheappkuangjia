package com.sdzn.fzx.student.libutils.app;

/**
 * 描述：全局配置
 * <p>
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public interface Config {

    public static String UPDATE_APP_DOWNLOAD = "Download";
    public static final String APP_CACHE = "sdzn";
    public static final String ROOT_CACHE = "com.sdzn.fuzhuxian.student";


    public final static String TOKEN = "token";

    public static final String DEVICES_DEFULT_TOKEN = "test_token";
    public static final String DEVICES = "Anroid";

    boolean ruanYunDebug = false;
}
