package com.sdzn.fzx.student.libutils.util;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SaveLogUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SaveLogUtils {

    public static void saveInfo(String fileName, String info) {
        if (!Log.isLoggingEnabled())
            return;

        info = transTime(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss") + ":" + info + "\r\n";
        FileOutputStream fos = null;
        try {
            String path = getSDPath() + "/sdzn/demo/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            fos = new FileOutputStream(path + fileName, true);
            fos.write(info.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if(fos!=null){
                fos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void saveInfo(String info) {
        String fileName = "socketLog" + ".txt";
        saveInfo(fileName, info);
    }

    /**
     * 获取SD卡路径
     *
     * @return
     * @throws Exception
     */
    public static String getSDPath() throws Exception {
        String sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory().getAbsolutePath();// 获取根目录
        } else {
            throw new Exception("没有SD卡");
        }
        return sdDir;
    }


    public static String transTime(long time, String str) {
        Date date = new Date(time);
        return transTime(date, str);
    }

    public static String transTime(Date date, String str) {
        SimpleDateFormat format = new SimpleDateFormat(str);
        return format.format(date);
    }
}
