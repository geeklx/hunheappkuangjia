package com.sdzn.fzx.student.libutils.util;


import com.sdzn.fzx.student.libutils.R;

import java.text.SimpleDateFormat;


/**
 * 资源管理工具
 * Created by Xiaoz on 15/10/20.
 */
public class ResUtils {
    public static final String DIR_NAME_BUFFER = "buffer";
    public static final String DIR_NAME_VIDEO = "video";
    public static final String DIR_NAME_PHOTO = "photo";
    public static final String DIR_NAME_DATA = "data";

    public static String getDateFormatName(){
        return getDateFormatName("yyyyMMddHHmmss");
    }
    public static String getDateFormatName(String pattern){
        SimpleDateFormat date = new SimpleDateFormat(pattern);
        String filename = String.valueOf(date.format(System.currentTimeMillis()));
        return filename;
    }

    public static String getPackagePath(){
        String _class = R.class.getCanonicalName();
        String result = _class.replace(".R", "");
        return result;
    }
}
