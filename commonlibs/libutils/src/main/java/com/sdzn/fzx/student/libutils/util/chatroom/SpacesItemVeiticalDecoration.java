package com.sdzn.fzx.student.libutils.util.chatroom;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/27.
 */
public class SpacesItemVeiticalDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemVeiticalDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) != 0) {
            outRect.top = space;
        }
        outRect.bottom = space;
    }
}
