package com.sdzn.fzx.student.libutils.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Reisen at 2018-10-31
 */
public class GsonUtil {
    private static Gson gson;

    static {
        gson = new Gson();
    }

    private GsonUtil() {

    }

    /**
     * 对象转化为json 数据
     */
    public static String toJson(Object object) {
        String gsonString = null;
        if (gson == null) {
            gson = new Gson();
        }
        gsonString = gson.toJson(object);
        return gsonString;
    }

    /**
     * json 数据转化为实体类对象
     */
    public static <T> T fromJson(String gsonString, Class<T> cls) {
        if (gson == null) {
            gson = new Gson();
        }
        return gson.fromJson(gsonString, cls);
    }

    /**
     * Json 数据转化为List集合--集合中为实体类
     */
    public static <T> List<T> gsonToList(String gsonString, Class<T> cls) {
        ArrayList<T> mList = new ArrayList<T>();
        JsonArray array = new JsonParser().parse(gsonString).getAsJsonArray();
        for (final JsonElement elem : array) {
            mList.add(gson.fromJson(elem, cls));
        }
        return mList;

    }

    /**
     * 将数据转化成List集合--集合中为map
     */
    public static <T> List<Map<String, T>> gsonToListMaps(String gsonString) {
        List<Map<String, T>> list = null;
        if (gson != null) {
            list = gson.fromJson(gsonString,
                    new TypeToken<List<Map<String, T>>>() {
                    }.getType());
        }
        return list;
    }

    /**
     * 将json数据转化成map
     */
    public static <T> Map<String, T> gsonToMaps(String gsonString) {
        Map<String, T> map = null;
        if (gson != null) {
            map = gson.fromJson(gsonString, new TypeToken<Map<String, T>>() {
            }.getType());
        }
        return map;
    }
}
