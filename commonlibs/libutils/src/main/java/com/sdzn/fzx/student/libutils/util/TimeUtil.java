package com.sdzn.fzx.student.libutils.util;

/**
 * @author Reisen at 2018-12-26
 */
public class TimeUtil {
    private TimeUtil() {
    }

    public static String ms2HMS(long _ms) {
        _ms/=1000;
        return sec2HMS(_ms);
    }

    public static String sec2HMS(long sec) {
        String HMStime;
        long hour=sec/3600;
        long mint=(sec%3600)/60;
        long sed=sec%60;
        String hourStr=String.valueOf(hour);
        if(hour<10){
            hourStr="0"+hourStr;
        }
        String mintStr=String.valueOf(mint);
        if(mint<10){
            mintStr="0"+mintStr;
        }
        String sedStr=String.valueOf(sed);
        if(sed<10){
            sedStr="0"+sedStr;
        }
        HMStime=hourStr+":"+mintStr+":"+sedStr;
        return HMStime;
    }
}
