package com.sdzn.fzx.student.libutils.annotations;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static com.sdzn.fzx.student.libutils.annotations.CheckState.FALSE;
import static com.sdzn.fzx.student.libutils.annotations.CheckState.TRUE;
import static com.sdzn.fzx.student.libutils.annotations.CheckState.UN_CHECK;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.CLASS;


/**
 * 填空题批改状态
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-11
 */
@Retention(CLASS)
@IntDef({UN_CHECK,TRUE,FALSE})
@Target({METHOD, PARAMETER, FIELD, LOCAL_VARIABLE})
public @interface CheckState {

    /**
     * 图片填空: 未批改
     * 文字填空: 系统未批改
     */
    int UN_CHECK = 0;

    /**
     * 图片填空: 教师手动批对
     * 文字填空: 系统自动批对
     * 文字填空: 教师手动批对
     */
    int TRUE = 1;

    /**
     * 图片填空: 教师手动批错
     * 文字填空: 系统自动批错
     * 文字填空: 教师手动批错
     */
    int FALSE = 2;
}
