package com.example.app1xxrw.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1xxrw.R;
import com.example.app1xxrw.presenter.TaskFileDetailPresenter;
import com.example.app1xxrw.view.TaskFileDetailView;
import com.sdzn.fzx.student.libbase.app.FileDownloadManager;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.CollectVo;
import com.sdzn.fzx.student.vo.TaskFileDetailVo;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/2/5
 */
public class TaskFileDetailActivity extends MBaseActivity<TaskFileDetailPresenter> implements TaskFileDetailView, FileDownloadManager.DownloadCallBack, View.OnClickListener {

    private TextView tvTitle;
    private EmptyRecyclerView rvFiles;
    private TextView tvBack;

    private String lessonId;
    private String classId;
    private String chapterName;

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter fileAdapter;

    @Override
    public void initPresenter() {
        mPresenter = new TaskFileDetailPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_file_detail);
        tvTitle = findViewById(R.id.tvTitle);
        rvFiles = findViewById(R.id.rvFiles);
        tvBack = findViewById(R.id.tvBack);

        tvBack.setOnClickListener(this);
        FileDownloadManager.getInstance().reg(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(TaskFileDetailActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        FileDownloadManager.getInstance().unReg(this);
        super.onDestroy();
    }

    @Override
    protected void initData() {
        lessonId = getIntent().getStringExtra("lessonId");
        classId = getIntent().getStringExtra("classId");
        chapterName = getIntent().getStringExtra("chapterName");
    }

    @Override
    protected void initView() {
        tvTitle.setText(chapterName);
        mPresenter.getTaskFileDetail(lessonId, classId);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
        rvFiles.setLayoutManager(gridLayoutManager);
        fileAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvFiles.setAdapter(fileAdapter);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvBack) {
            finish();
        }
    }
    /*@Event(value = {R.id.tvBack}, type = View.OnClickListener.class)
    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                finish();
                break;
            default:
                break;
        }
    }*/

    @Override
    public void getTaskFileDetailSuccess(TaskFileDetailVo taskFileDetailVo) {
        if (taskFileDetailVo != null && taskFileDetailVo.getData() != null && taskFileDetailVo.getData().size() > 0) {
            itemEntityList.addItems(R.layout.item_fragment_task_file_detail, taskFileDetailVo.getData())
                    .addOnBind(R.layout.item_fragment_task_file_detail, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(final GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            final TaskFileDetailVo.DataBean dataBean = (TaskFileDetailVo.DataBean) itemData;
                            mPresenter.bindTaskFileHolder(holder, dataBean);
                        }
                    });
        }
        fileAdapter.notifyDataSetChanged();
    }

    @Override
    public void collectSuccess(int id, CollectVo collectVo) {
        for (Object o : itemEntityList.getItems()) {
            TaskFileDetailVo.DataBean dataBean = (TaskFileDetailVo.DataBean) o;
            if (dataBean.getLessonResourceId() == id) {
                dataBean.setStatus(true);
                dataBean.setCollectId(collectVo.getData());
            }
        }
        fileAdapter.notifyDataSetChanged();
        ToastUtil.showShortlToast("收藏成功");
    }

    @Override
    public void deleteSuccess(int id) {
        for (Object o : itemEntityList.getItems()) {
            TaskFileDetailVo.DataBean dataBean = (TaskFileDetailVo.DataBean) o;
            if (dataBean.getCollectId() == id) {
                dataBean.setStatus(false);
            }
        }
        fileAdapter.notifyDataSetChanged();
        ToastUtil.showShortlToast("删除成功");
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void progress(String url, int soFar, int total) {
        Log.i("url = " + url + ",\nsoFar = " + soFar + ",\ntotal = " + total);
        for (Object o : itemEntityList.getItems()) {
            TaskFileDetailVo.DataBean dataBean = (TaskFileDetailVo.DataBean) o;
            if (TextUtils.equals(dataBean.getResourcePath(), url)) {
                dataBean.setDownlaoding(true);
                dataBean.setProgress(soFar * 100 / total);
            }
        }
        fileAdapter.notifyDataSetChanged();
    }

    @Override
    public void completed(String url, String path) {
        Log.i("download success:" + url);
        for (Object o : itemEntityList.getItems()) {
            TaskFileDetailVo.DataBean dataBean = (TaskFileDetailVo.DataBean) o;
            if (TextUtils.equals(dataBean.getResourcePath(), url)) {
                dataBean.setDownlaoding(false);
            }
        }
        fileAdapter.notifyDataSetChanged();
    }

    @Override
    public void error(String url, Throwable throwable) {
        Log.i("error: " + url);
        for (Object o : itemEntityList.getItems()) {
            TaskFileDetailVo.DataBean dataBean = (TaskFileDetailVo.DataBean) o;
            if (TextUtils.equals(dataBean.getResourcePath(), url)) {
                dataBean.setDownlaoding(false);
            }
        }
        fileAdapter.notifyDataSetChanged();
    }
}
