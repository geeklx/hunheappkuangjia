package com.example.app1xxrw.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.app1xxrw.R;
import com.example.app1xxrw.listener.OnTaskListener;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.libpublic.baseadapter.TaskPagerAdapter;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;


/**
 * 学习任务内容
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class TaskContentFragment extends BaseFragment implements OnTaskListener {

    //@ViewInject(R.id.pager)
    private ViewPager viewPager;


    private TaskPagerAdapter taskPagerAdapter;

    private OnPageChangeListener onPageChangeListener;

    private int status;

    public static TaskContentFragment newInstance(Bundle bundle) {
        TaskContentFragment fragment = new TaskContentFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_content, container, false);
        viewPager=rootView.findViewById(R.id.pager);

        //x.view().inject(this, rootView);
        initView();
        return rootView;
    }

    private void initView() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelected(position);
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onSubjectItemClick(int position) {
        viewPager.setCurrentItem(position, false);
    }

    @Override
    public void onSearch(String searchStr) {
        TaskListFragment taskListFragment = (TaskListFragment) taskPagerAdapter.getItem(viewPager.getCurrentItem());
        taskListFragment.search(searchStr);
    }

    @Override
    public void onStatusChanged(int status) {
        if (taskPagerAdapter != null) {
            TaskListFragment taskListFragment = (TaskListFragment) taskPagerAdapter.getItem(viewPager.getCurrentItem());
            taskListFragment.setStatus(status);
        } else {
            this.status = status;
        }
    }

    @Override
    public void onSubjectGet(List<SubjectVo.DataBean> subjects) {
        List<Fragment> fragmentArrayList = new ArrayList<>();
        for (int i = 0; i < subjects.size(); i++) {
            SubjectVo.DataBean dataBean = subjects.get(i);
            Bundle bundle = new Bundle();
            bundle.putInt("subjectId", dataBean.getId());
            bundle.putInt("status", status);
            fragmentArrayList.add(TaskListFragment.newInstance(bundle));
        }
        viewPager.setOffscreenPageLimit(0);
        taskPagerAdapter = new TaskPagerAdapter(getChildFragmentManager(), fragmentArrayList);
        viewPager.setAdapter(taskPagerAdapter);
    }

    @Override
    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener;
    }

    private void setSelected(int pos) {
        if (onPageChangeListener != null) {
            onPageChangeListener.onPageChange(pos);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }
}
