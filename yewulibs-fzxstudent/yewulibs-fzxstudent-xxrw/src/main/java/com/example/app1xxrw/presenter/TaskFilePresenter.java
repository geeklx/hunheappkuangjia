package com.example.app1xxrw.presenter;

import com.example.app1xxrw.R;
import com.example.app1xxrw.view.TaskFileView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.vo.TaskFileVo;

import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 任务资料列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskFilePresenter extends BasePresenter<TaskFileView, BaseActivity> {
    private Subscription subscribe;

    public void getTaskFiles(Map<String, String> params) {
        if (subscribe != null && subscribe.isUnsubscribed()) {
            subscribe.unsubscribe();
            subscriptions.remove(subscribe);
        }
        subscribe = Network.createTokenService(NetWorkService.TaskService.class)
                .getTaskFiles(params)
                .map(new StatusFunc<TaskFileVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<TaskFileVo>(new SubscriberListener<TaskFileVo>() {
                    @Override
                    public void onNext(TaskFileVo taskFileVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getTaskFileSuccess(taskFileVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void bindTaskFileHolder(GeneralRecyclerViewHolder holder, TaskFileVo.DataBean itemData) {
        Subject subject = Subject.subjects.get(itemData.getBaseSubjectId());
        if (subject != null) {
            holder.getChildView(R.id.ivXueke).setBackground(App2.get().getResources().getDrawable(subject.getDrawableId()));
        }
        holder.setText(R.id.tvTaskName, itemData.getLessonName());
        holder.setText(R.id.tvChapterName, itemData.getChapterNodeNamePath());
        holder.setText(R.id.tvNum, "资料 " + itemData.getCount());
        long time = itemData.getUpdateTime();
        holder.setText(R.id.tvDate, DateUtil.getTimeStrByTimemillis(time, "yyyy-MM-dd HH:mm"));
    }
}
