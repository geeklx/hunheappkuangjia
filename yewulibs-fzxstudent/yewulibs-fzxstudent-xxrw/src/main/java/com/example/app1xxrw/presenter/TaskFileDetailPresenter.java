package com.example.app1xxrw.presenter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.app1xxrw.R;
import com.example.app1xxrw.view.TaskFileDetailView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.app.FileDownloadManager;
import com.sdzn.fzx.student.libbase.app.SchoolBoxWatcher;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.baseui.activity.DocViewActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.ImageDisplayActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.PDFActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.PlayerActivity;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.CollectVo;
import com.sdzn.fzx.student.vo.ResourceVo;
import com.sdzn.fzx.student.vo.TaskFileDetailVo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 任务资料列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskFileDetailPresenter extends BasePresenter<TaskFileDetailView, BaseActivity> {
    private Subscription subscribe1;
    private Subscription subscribe2;
    private Subscription subscribe3;

    public void getTaskFileDetail(String lessonId, String classId) {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        Map<String, String> params = new HashMap<>();
        params.put("lessonId", lessonId);
        params.put("classId", classId);
        params.put("userStudentId", UserController.getUserId());
        subscribe1 = Network.createTokenService(NetWorkService.TaskService.class)
                .getTaskFileDetail(params)
                .map(new StatusFunc<TaskFileDetailVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<TaskFileDetailVo>(new SubscriberListener<TaskFileDetailVo>() {
                    @Override
                    public void onNext(TaskFileDetailVo taskFileDetailVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getTaskFileDetailSuccess(taskFileDetailVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true));
        subscriptions.add(subscribe1);
    }

    public void collect(final int resourceId, String lessonId, String lessonName) {
        if (subscribe2 != null && subscribe2.isUnsubscribed()) {
            subscribe2.unsubscribe();
            subscriptions.remove(subscribe2);
        }
        Map<String, String> params = new HashMap<>();
        params.put("lessonResourceId", String.valueOf(resourceId));
        params.put("lessonId", lessonId);
        params.put("lessonName", lessonName);
        subscribe2 = Network.createTokenService(NetWorkService.TaskService.class)
                .collect(params)
                .map(new StatusFunc<CollectVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<CollectVo>(new SubscriberListener<CollectVo>() {
                    @Override
                    public void onNext(CollectVo collectVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.collectSuccess(resourceId, collectVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("收藏失败");
                            }
                        } else {
                            mView.networkError("收藏失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true));
        subscriptions.add(subscribe2);
    }

    public void delete(final int resourceCollectId) {
        if (subscribe3 != null && subscribe3.isUnsubscribed()) {
            subscribe3.unsubscribe();
            subscriptions.remove(subscribe3);
        }
        Map<String, String> params = new HashMap<>();
        params.put("resourceCollectId", String.valueOf(resourceCollectId));
        subscribe3 = Network.createTokenService(NetWorkService.TaskService.class)
                .delete(params)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        if (mView == null) {
                            return;
                        }
                        mView.deleteSuccess(resourceCollectId);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("收藏失败");
                            }
                        } else {
                            mView.networkError("收藏失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true));
        subscriptions.add(subscribe3);
    }

    public void bindTaskFileHolder(GeneralRecyclerViewHolder holder, final TaskFileDetailVo.DataBean dataBean) {
        holder.getChildView(R.id.rlItem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ResourceVo.ResourceTextVo resourceTextVo = GsonUtil.fromJson(dataBean.getResourceText(), ResourceVo.ResourceTextVo.class);
                if (resourceTextVo == null) {
                    ToastUtil.showShortlToast("找不到资源");
                    return;
                }
                switch (dataBean.getResourceType()) {
                    case 1:
                    case 2:
                        showSelectOpenDialog(new OpenListener() {
                            @Override
                            public void openByApp() {
                                Intent intentDoc = new Intent(mActivity, PDFActivity.class);
                                intentDoc.putExtra("path",resourceTextVo.getConvertPath());
//                                intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                                mActivity.startActivity(intentDoc);
                            }

                            @Override
                            public void openByOther() {
                                String url = resourceTextVo.getResourcePath();
                                String[] split = url.split("/");
                                String fileName = split[split.length - 1];
                                String path;
                                try {
                                    path = FileUtil.getResPath();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    path = FileUtil.getAppFilesDir();
                                }
                                File file = new File(path, fileName);
                                if (!file.exists()) {
                                    FileDownloadManager.getInstance()
                                            .download(url,file.getAbsolutePath());
                                    return;
                                }
                                Intent intentDoc = new Intent(mActivity, DocViewActivity.class);
                                intentDoc.putExtra("path", resourceTextVo.getResourcePath());
                                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                                switch (resourceTextVo.getResourceType()) {
                                    case 1:
                                        if (".txt".equalsIgnoreCase(resourceTextVo.getResourceSuffix())) {
                                            intentDoc.putExtra("type", "txt");
                                        } else {
                                            intentDoc.putExtra("type", "doc");
                                        }
                                        break;
                                    case 2:
                                        intentDoc.putExtra("type", "ppt");
                                        break;
                                }
                                mActivity.startActivity(intentDoc);
                            }
                        });
                        break;
                    case 3:
                        SchoolBoxWatcher.getFileAddress(dataBean.getId() + "", resourceTextVo.getConvertPath(), mActivity, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                            @Override
                            public void onFailed(Throwable e) {
                                ToastUtil.showShortlToast("获取文件地址失败");
                            }

                            @Override
                            public void onSuccess(String path) {
                                Intent intentVideo = new Intent(mActivity, PlayerActivity.class);
                                intentVideo.putExtra("videoUrl", path);
                                intentVideo.putExtra("title", dataBean.getResourceName());
                                mActivity.startActivity(intentVideo);
                            }
                        });
//                        Intent intentVideo = new Intent(mActivity, PlayerActivity.class);
//                        intentVideo.putExtra("videoUrl", SchoolBoxWatcher.getFileAddress(dataBean.getId() + "", resourceTextVo.getConvertPath()));
//                        intentVideo.putExtra("title", dataBean.getResourceName());
//                        mActivity.startActivity(intentVideo);
                        break;
                    case 4:
                        SchoolBoxWatcher.getFileAddress(dataBean.getId() + "", resourceTextVo.getConvertPath(), mActivity, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                            @Override
                            public void onFailed(Throwable e) {
                                ToastUtil.showShortlToast("获取文件地址失败");
                            }

                            @Override
                            public void onSuccess(String path) {
                                Intent intentImage = new Intent(mActivity, ImageDisplayActivity.class);
                                intentImage.putExtra("photoUrl", path);
                                mActivity.startActivity(intentImage);
                            }
                        });
//                        Intent intentImage = new Intent(mActivity, ImageDisplayActivity.class);
//                        intentImage.putExtra("photoUrl", SchoolBoxWatcher.getFileAddress(dataBean.getId() + "",resourceTextVo.getConvertPath()));
//                        mActivity.startActivity(intentImage);
                        break;
                    case 5:
                        SchoolBoxWatcher.getFileAddress(dataBean.getId() + "", resourceTextVo.getConvertPath(), mActivity, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                            @Override
                            public void onFailed(Throwable e) {
                                ToastUtil.showShortlToast("获取文件地址失败");
                            }

                            @Override
                            public void onSuccess(String path) {
                                Intent intentRadio = new Intent(mActivity, PlayerActivity.class);
                                intentRadio.putExtra("radioUrl", path);
                                intentRadio.putExtra("title", dataBean.getResourceName());
                                mActivity.startActivity(intentRadio);
                            }
                        });
//                        Intent intentRadio = new Intent(mActivity, PlayerActivity.class);
//                        intentRadio.putExtra("radioUrl", SchoolBoxWatcher.getFileAddress(dataBean.getId() + "", resourceTextVo.getConvertPath()));
//                        intentRadio.putExtra("title", dataBean.getResourceName());
//                        mActivity.startActivity(intentRadio);
                        break;
                    case 6:
                        Intent intentDoc = new Intent(mActivity, PDFActivity.class);
                        intentDoc.putExtra("path",resourceTextVo.getConvertPath());
//                        intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        mActivity.startActivity(intentDoc);
                        break;
                }
            }
        });
        holder.setText(R.id.tvFileName, dataBean.getResourceName());
        holder.setText(R.id.tvFileType, dataBean.getSubjectStructureName());
        holder.getChildView(R.id.ivShoucang).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO 收藏
                if (!dataBean.isStatus()) {
                    collect(dataBean.getLessonResourceId(), String.valueOf(dataBean.getLessonId()), dataBean.getLessonName());
                } else {
                    delete(dataBean.getCollectId());
                }
            }
        });
        if (dataBean.isStatus()) {
            ((ImageView)holder.getChildView(R.id.ivShoucang)).setImageBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.mipmap.shouc_icon));
//            holder.getChildView(R.id.ivShoucang).setBackground(mActivity.getResources().getDrawable(R.mipmap.shouc_icon));
        } else {
            ((ImageView)holder.getChildView(R.id.ivShoucang)).setImageBitmap(BitmapFactory.decodeResource(mActivity.getResources(), R.mipmap.shouc1_icon));
//            holder.getChildView(R.id.ivShoucang).setBackground(mActivity.getResources().getDrawable(R.mipmap.shouc1_icon));
        }
        switch (dataBean.getResourceType()) {
            case 1:
                if (".txt".equalsIgnoreCase(dataBean.getResourceSuffix())){
                    ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.txt));
                }else{
                    ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.word));
                }
                break;
            case 2:
                ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.ppt));
                break;
            case 3:
                ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.video_ico));
                break;
            case 4:
                ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.pic_ico));
                break;
            case 5:
                ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.radio_ico));
                break;
            case 6:
                ((ImageView) holder.getChildView(R.id.ivFile)).setBackground(mActivity.getResources().getDrawable(R.mipmap.pdf));
                break;
        }
        //下载进度条
        if (dataBean.isDownlaoding()) {
            holder.getChildView(R.id.rl_progress).setVisibility(View.VISIBLE);
            ((ProgressBar)holder.getChildView(R.id.pb)).setProgress(dataBean.getProgress());
            ((TextView)holder.getChildView(R.id.tv_progress)).setText(dataBean.getProgress() + "%");
        }else {
            holder.getChildView(R.id.rl_progress).setVisibility(View.GONE);
        }
    }

    public Dialog showSelectOpenDialog(final OpenListener listener) {
        final Dialog dialog = new Dialog(mActivity, R.style.Dialog);
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_open_res, null);
        view.findViewById(R.id.tv_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByApp();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByOther();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
        return dialog;
    }

    interface OpenListener {
        void openByApp();

        void openByOther();
    }
}
