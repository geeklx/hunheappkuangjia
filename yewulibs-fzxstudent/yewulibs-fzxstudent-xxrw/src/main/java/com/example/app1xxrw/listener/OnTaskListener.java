package com.example.app1xxrw.listener;


import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public interface OnTaskListener {
    void onSubjectItemClick(int position);

    void onSearch(String searchStr);

    void onStatusChanged(int status);

    void onSubjectGet(List<SubjectVo.DataBean> subjects);

    void setOnPageChangeListener(OnPageChangeListener onPageChangeListener);
}
