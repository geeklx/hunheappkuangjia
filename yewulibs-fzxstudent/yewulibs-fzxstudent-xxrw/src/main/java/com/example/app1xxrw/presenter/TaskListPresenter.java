package com.example.app1xxrw.presenter;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.app1xxrw.R;
import com.example.app1xxrw.view.TaskListView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.views.RoundProgressBar;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.vo.TaskListVo;

import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 任务内容列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskListPresenter extends BasePresenter<TaskListView, BaseActivity> {
    private Subscription subscribe1;

    public void getTasks(Map<String, String> params) {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        subscribe1 = Network.createTokenService(NetWorkService.TaskService.class)
                .getTasks(params)
                .map(new StatusFunc<TaskListVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<TaskListVo>(new SubscriberListener<TaskListVo>() {
                    @Override
                    public void onNext(TaskListVo taskListVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getTaskListSuccess(taskListVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe1);
    }

    public MyCountDownTimer bindTaskListHolder(GeneralRecyclerViewHolder holder, TaskListVo.DataBean itemData) {
        Subject subject = Subject.subjects.get(itemData.getBaseSubjectId());
        if (subject != null) {
            holder.getChildView(R.id.ivXueke).setBackground(App2.get().getResources().getDrawable(subject.getDrawableId()));
        }
        if (itemData.getIsRead() == 0) {
            holder.getChildView(R.id.dot).setVisibility(View.VISIBLE);
        } else {
            holder.getChildView(R.id.dot).setVisibility(View.GONE);
        }
        holder.setText(R.id.tvTaskName, itemData.getName());
        holder.setText(R.id.tvChapterName, itemData.getChapterNodeNamePath());
        holder.setText(R.id.tvNum, "试题 " + itemData.getCountExam() + "/资料 " + itemData.getCountResource());
        holder.setText(R.id.tvDateStart, DateUtil.getTimeStrByTimemillis(itemData.getStartTime(), "yyyy-MM-dd HH:mm"));
        holder.setText(R.id.tvDateEnd, DateUtil.getTimeStrByTimemillis(itemData.getEndTime(), "yyyy-MM-dd HH:mm"));

        if (itemData.getCountExam() == 0) {
            holder.getChildView(R.id.unFinish).setVisibility(View.GONE);
            holder.getChildView(R.id.tvUnFinish).setVisibility(View.GONE);
            holder.getChildView(R.id.tvProgress).setVisibility(View.GONE);
            holder.getChildView(R.id.progressbar).setVisibility(View.GONE);
            holder.getChildView(R.id.llCount).setVisibility(View.GONE);
            holder.getChildView(R.id.finish).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvStatus).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvPub).setVisibility(View.GONE);
            holder.getChildView(R.id.rlProgressBar).setVisibility(View.GONE);
            holder.getChildView(R.id.tvTime).setVisibility(View.GONE);
            if (itemData.getStatus() == 2 || itemData.getStatus() == 3) {
                holder.setText(R.id.tvStatus, App2.get().getResources().getString(R.string.fragment_task_content_text10));
            } else {
                holder.setText(R.id.tvStatus, App2.get().getResources().getString(R.string.fragment_task_content_text3));
            }
/*
            if (itemData.getSceneId() != 0) {
                if (itemData.getSceneId() == 1) {
                    holder.setText(R.id.tvClass_scene, "课前预习");
                }
                if (itemData.getSceneId() == 2) {
                    holder.setText(R.id.tvClass_scene, "课中讲解");
                }
                if (itemData.getSceneId() == 3) {
                    holder.setText(R.id.tvClass_scene, "课后巩固");
                }
            }
*/
            ((TextView) holder.getChildView(R.id.tvStatus)).setTextColor(App2.get().getResources().getColor(R.color.fragment_main_item_num));
            return null;
        }

        if (itemData.getStatus() == 2 || itemData.getStatus() == 3) {
            // 已完成 补交
            holder.getChildView(R.id.unFinish).setVisibility(View.GONE);
            holder.getChildView(R.id.finish).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvStatus).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvPub).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.rlProgressBar).setVisibility(View.VISIBLE);
            if (itemData.getIsCorrect() == 0) {
                // 未批改
                holder.setText(R.id.tvStatus, App2.get().getResources().getString(R.string.fragment_main_text5));
                ((TextView) holder.getChildView(R.id.tvStatus)).setTextColor(App2.get().getResources().getColor(R.color.fragment_main_item_num));
            } else {
                // 已批改
                holder.setText(R.id.tvStatus, App2.get().getResources().getString(R.string.fragment_main_text4));
                ((TextView) holder.getChildView(R.id.tvStatus)).setTextColor(App2.get().getResources().getColor(R.color.fragment_main_item_pigai));
            }
            if (itemData.getAnswerViewOpen() == 0) {
                holder.getChildView(R.id.tvPub).setVisibility(View.GONE);
            } else {
                holder.getChildView(R.id.tvPub).setVisibility(View.VISIBLE);
            }
            if (itemData.getScore() <= 0) {
                holder.getChildView(R.id.roundProgressBar).setVisibility(View.INVISIBLE);
                holder.getChildView(R.id.tvRight).setVisibility(View.INVISIBLE);
                holder.getChildView(R.id.tvCount2).setVisibility(View.INVISIBLE);
                holder.getChildView(R.id.tvSeparator2).setVisibility(View.INVISIBLE);
            } else {
                holder.getChildView(R.id.roundProgressBar).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvRight).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvCount2).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvSeparator2).setVisibility(View.VISIBLE);
                ((RoundProgressBar) holder.getChildView(R.id.roundProgressBar)).setMax(itemData.getScore());
                ((RoundProgressBar) holder.getChildView(R.id.roundProgressBar)).setProgress(itemData.getScoreTotal() <= 0 ? 0 : itemData.getScore());
                holder.setText(R.id.tvRight, String.valueOf(itemData.getScoreTotal()));
                holder.setText(R.id.tvCount2, String.valueOf(itemData.getScore()));
            }
        } else {
            // 未完成
            holder.getChildView(R.id.finish).setVisibility(View.GONE);
            holder.getChildView(R.id.unFinish).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvUnFinish).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvProgress).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.progressbar).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.llCount).setVisibility(View.VISIBLE);
            if (itemData.getCountExam() == 0) {
                holder.getChildView(R.id.tvUnFinish).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.progressbar).setVisibility(View.GONE);
                holder.getChildView(R.id.tvAnswer).setVisibility(View.GONE);
                holder.getChildView(R.id.tvCount).setVisibility(View.GONE);
            } else {
                holder.getChildView(R.id.tvUnFinish).setVisibility(View.GONE);
                holder.getChildView(R.id.progressbar).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvAnswer).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvCount).setVisibility(View.VISIBLE);
                ((ProgressBar) holder.getChildView(R.id.progressbar)).setMax(itemData.getCountExam());
                ((ProgressBar) holder.getChildView(R.id.progressbar)).setProgress(itemData.getCountAnswer() <= 0 ? 0 : itemData.getCountAnswer());
                holder.setText(R.id.tvAnswer, String.valueOf(itemData.getCountAnswer()));
                holder.setText(R.id.tvCount, String.valueOf(itemData.getCountExam()));
            }
        }

        if (itemData.getStatus() == 2 || itemData.getStatus() == 3) {
            // 已完成 补交
            if (itemData.getStatus() == 3) {
                holder.getChildView(R.id.tvTime).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvTime).setBackground(App2.get().getResources().getDrawable(R.mipmap.bujiao_icon));
                holder.setText(R.id.tvTime, "补交");
            } else if (itemData.getStatus() == 2) {
                holder.getChildView(R.id.tvTime).setVisibility(View.GONE);
            }
        } else {
            if (itemData.getOvertime() < 0) {
                holder.getChildView(R.id.tvTime).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvTime).setBackground(App2.get().getResources().getDrawable(R.mipmap.count_down));
                holder.setText(R.id.tvTime, "超时");
            } else if (itemData.getOvertime() <= 30 * 60 * 1000) {
                holder.getChildView(R.id.tvTime).setVisibility(View.VISIBLE);
                holder.getChildView(R.id.tvTime).setBackground(App2.get().getResources().getDrawable(R.mipmap.count_down));
                MyCountDownTimer myCountDownTimer = new MyCountDownTimer((TextView) holder.getChildView(R.id.tvTime), itemData.getOvertime(), 1000);
                myCountDownTimer.start();
                return myCountDownTimer;
            } else {
                holder.getChildView(R.id.tvTime).setVisibility(View.GONE);
            }
        }

        return null;
    }

    public class MyCountDownTimer extends CountDownTimer {
        TextView textView;

        public MyCountDownTimer(TextView textView, long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            this.textView = textView;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (textView != null) {
                textView.setText(DateUtil.millsecondsToStr(millisUntilFinished));
            }
        }

        @Override
        public void onFinish() {
            textView.setText("超时");
            cancel();
        }
    }
}
