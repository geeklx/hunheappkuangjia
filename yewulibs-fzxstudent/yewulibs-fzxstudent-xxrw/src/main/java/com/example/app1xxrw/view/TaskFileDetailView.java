package com.example.app1xxrw.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.CollectVo;
import com.sdzn.fzx.student.vo.TaskFileDetailVo;

/**
 * 任务内容列表PresenterView
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public interface TaskFileDetailView extends BaseView {
    void getTaskFileDetailSuccess(TaskFileDetailVo taskFileDetailVo);

    void collectSuccess(int id, CollectVo collectVo);

    void deleteSuccess(int id);

    void networkError(String msg);
}
