package com.example.app1xztl.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.app1xztl.adapter.holder.ChatAcceptViewHolder;
import com.example.app1xztl.adapter.holder.ChatPerchViewHolder;
import com.example.app1xztl.adapter.holder.ChatSendViewHolder;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;

import java.security.InvalidParameterException;


/**
 * 作者：Rance on 2016/11/29 10:46
 * 邮箱：rance935@163.com
 */
public class ChatAdapter extends RecyclerArrayAdapter<ChatRoomMessage> {

    private onItemClickListener onItemClickListener;
    public Handler handler;

    public ChatAdapter(Context context) {
        super(context);
        handler = new Handler();
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                ChatAcceptViewHolder chatAcceptViewHolder = new ChatAcceptViewHolder(parent, onItemClickListener, handler);
                return chatAcceptViewHolder;
            case 2:
                ChatSendViewHolder chatSendViewHolder = new ChatSendViewHolder(parent, onItemClickListener, handler);
                return chatSendViewHolder;
            case 0:
                /*定义时间
                 */
                ChatPerchViewHolder chatPerchViewHolder = new ChatPerchViewHolder(parent, onItemClickListener, handler);
                return chatPerchViewHolder;
            //抛异常
            default:
                throw new InvalidParameterException();
        }

    }

    @Override
    public int getViewType(int position) {
        ChatRoomMessage object = getItem(position);
        if (object.getDirect()!=null&&object.getDirect() == MsgDirectionEnum.In) {
            return 1;
        } else if (object.getDirect()!=null&&object.getDirect() == MsgDirectionEnum.Out) {
            return 2;
        }else {
            return 0;
        }
    }


    public void addItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface onItemClickListener {
        void onHeaderClick(int position);

        void onImageClick(View view, int position);

        void onVoiceClick(ImageView imageView, int position);
    }
}
