package com.example.app1xztl.fragment;


import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app1xztl.R;
import com.example.app1xztl.listener.OnGroupListener;
import com.example.app1xztl.presenter.GroupDiscussionPresenter;
import com.example.app1xztl.view.GroupDiscussionView;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.libbase.listener.OnSearchClickListener;
import com.sdzn.fzx.student.libbase.pop.TaskSearchPop;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.views.ImageHintEditText;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;


/**
 * 小组讨论主界面
 */
public class GroupDiscussionFragment extends MBaseFragment<GroupDiscussionPresenter> implements GroupDiscussionView, View.OnClickListener {

    // @ViewInject(R.id.rvSubject)
    private RecyclerView rvSubject;

    //@ViewInject(R.id.btnSearch)
    private ImageHintEditText btnSearch;

    private List<Fragment> fragments;//此处 没有切换多种fragment
    private int containerId = R.id.framelayoutTask;
    private Fragment currFragment;

    private Y_MultiRecyclerAdapter subjectAdapter;
    private List<SubjectVo.DataBean> subjects = new ArrayList<>();
    private Y_ItemEntityList itemEntityListSub = new Y_ItemEntityList(); // 学科列表
    private SubjectVo.DataBean currentSubject;

    private TaskSearchPop taskSearchPop;
    private String searchStr;

    public GroupDiscussionFragment() {
        // Required empty public constructor
    }

    public static GroupDiscussionFragment newInstance(Bundle bundle) {
        GroupDiscussionFragment fragment = new GroupDiscussionFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new GroupDiscussionPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group_discussion, container, false);
        rvSubject = rootView.findViewById(R.id.rvSubject);
        btnSearch = rootView.findViewById(R.id.btnSearch);
        // x.view().inject(this, rootView);
        initFragment();
        initView();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (subjects.size() == 0) {
            mPresenter.getSubjects();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        rvSubject.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        subjectAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityListSub);
        rvSubject.setAdapter(subjectAdapter);
        rvSubject.addOnItemTouchListener(new OnItemTouchListener(rvSubject) {
            @Override
            public void onItemClick(final RecyclerView.ViewHolder viewHolder) {
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        int position = viewHolder.getAdapterPosition();
                        if (position >= 0) {
                            setSelected(position);
                        }
                    }
                });
            }
        });
        btnSearch.setOnClickListener(this);

    }

    private void setSelected(int pos) {
        for (int i = 0; i < subjects.size(); i++) {
            SubjectVo.DataBean dataBean = subjects.get(i);
            if (i == pos) {
                dataBean.setSelected(true);
            } else {
                dataBean.setSelected(false);
            }
        }
        currentSubject = subjects.get(pos);
        subjectAdapter.notifyDataSetChanged();


        for (Fragment fragment : fragments) {
            ((OnGroupListener) fragment).onSubjectItemClick(pos);
        }
    }

    /**
     * 章节
     */

    private void initFragment() {
        fragments = new ArrayList<>();

        fragments.add(GroupContentFragment.newInstance(null));
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.add(containerId, currFragment).commit();
    }

//    private void showAssignedFragment(int fragmentIndex) {
//        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        Fragment fragment = fragments.get(fragmentIndex);
//        if (currFragment != fragment) {
//            if (!fragment.isAdded()) {
//                ft.hide(currFragment).add(containerId, fragment, fragment.getClass().getName());
//            } else {
//                ft.hide(currFragment).show(fragment);
//            }
//        }
//        currFragment = fragment;
//        ft.commit();
//    }

    @Override
    public void getSubjectSuccess(SubjectVo subjectVo) {
        subjects.clear();
        SubjectVo.DataBean all = new SubjectVo.DataBean();
        all.setId(0);
        all.setName("全部");
        all.setSelected(true);
        subjects.add(0, all);
        currentSubject = all;
        if (subjectVo != null && subjectVo.getData() != null) {
            List<SubjectVo.DataBean> data = subjectVo.getData();
            subjects.addAll(data);
        }
        setSubjects();

        for (Fragment fragment : fragments) {
            ((OnGroupListener) fragment).onSubjectGet(subjects);
            ((OnGroupListener) fragment).setOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageChange(int pos) {
                    setSelected(pos);
                }
            });
          /*  ((OnGroupListener) fragment).setOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageChange(int pos) {
                    setSelected(pos);
                }
            });*/
        }
    }

    /**
     * 设置学科列表
     */
    private void setSubjects() {
        itemEntityListSub.clear();
        itemEntityListSub.addItems(R.layout.item_fragment_task_content_sub, subjects)
                .addOnBind(R.layout.item_fragment_task_content_sub, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        SubjectVo.DataBean dataBean = (SubjectVo.DataBean) itemData;
                        Subject subject = Subject.subjects.get(dataBean.getId());
                        if (dataBean.isSelected()) {
                            if (subject != null) {
                                ((ImageView) holder.getChildView(R.id.ivXueke)).setImageBitmap(BitmapFactory.decodeResource(getResources(), subject.getDrawableId_sel()));
                            }
                            ((TextView) holder.getChildView(R.id.tvXueke)).setTextColor(getResources().getColor(R.color.fragment_main_chart_line_text));
                        } else {
                            if (subject != null) {
                                ((ImageView) holder.getChildView(R.id.ivXueke)).setImageBitmap(BitmapFactory.decodeResource(getResources(), subject.getDrawableId_nor()));
                            }
                            ((TextView) holder.getChildView(R.id.tvXueke)).setTextColor(getResources().getColor(R.color.fragment_task_subject_item_text_g));
                        }
                        holder.setText(R.id.tvXueke, dataBean.getName());
                    }
                });
        subjectAdapter.notifyDataSetChanged();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    /**
     * 搜索框
     */
    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btnSearch) {
            showPop();
        }
    }

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(activity, new OnSearchClickListener() {
                @Override
                public void onSearch(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);

                    ((OnGroupListener) fragments.get(0)).onSearch(searchStr);//此处只有一个fragment
                    GroupDiscussionFragment.this.searchStr = searchStr;
                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    if (TextUtils.isEmpty(searchStr) && !TextUtils.isEmpty(GroupDiscussionFragment.this.searchStr)) {
                        ((OnGroupListener) fragments.get(0)).onSearch(searchStr);
                    }
                }
            });
        }

        int subjectId = 0;
        if (currentSubject != null) {
            subjectId = currentSubject.getId();
        }
        taskSearchPop.showPopupWindow("groupDiscussion", btnSearch, subjectId);
    }
}
