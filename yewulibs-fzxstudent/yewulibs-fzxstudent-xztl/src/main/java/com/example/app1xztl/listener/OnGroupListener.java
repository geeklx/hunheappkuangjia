package com.example.app1xztl.listener;

import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

/**
 * 添加注释
 *
 * 小组讨论模块
 */
public interface OnGroupListener {
    void onSubjectItemClick(int position);

    void onSubjectGet(List<SubjectVo.DataBean> subjects);

    void setOnPageChangeListener(OnPageChangeListener onPageChangeListener);

    /**
     *可在此处更改成  章节
     */
    void onSearch(String searchStr);

//
//    void onStatusChanged(int status);

}
