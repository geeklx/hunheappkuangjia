package com.example.app1xztl.presenter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.app1xztl.R;
import com.example.app1xztl.view.GroupListView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.vo.chatroombean.ChatDiscussBean;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2019/8/12 0012.
 */

public class GroupListPresenter extends BasePresenter<GroupListView, BaseActivity> {

    public void getGroupDisData(Map<String, String> params) {

    }

    private Subscription subscribe1;

    public void getDiscussData(Map<String, Object> parms) {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        subscribe1 = Network.createTokenService(NetWorkService.ChatService.class)
                .getChatResult(parms)
                .map(new StatusFunc<GroupListBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<GroupListBean>(new SubscriberListener<GroupListBean>() {
                    @Override
                    public void onNext(GroupListBean taskListVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getDiscussSuccess(taskListVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
//                                mView.networkError(status.getMsg());
                            } else {
//                                mView.networkError("数据获取失败");
                            }
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe1);

    }

    public void bindTitleHolder(int type, GeneralRecyclerViewHolder holder, ChatDiscussBean.DataBean oldList) {
        switch (type) {
            case 0:
                holder.setText(R.id.tvName, "当前讨论");
                break;
            case 2:
                holder.setText(R.id.tvName, "历史讨论");
                break;
            default:
                break;
        }

    }

    public void bindHolder(int rvHeight, GeneralRecyclerViewHolder holder, GroupListBean.DataBean groupBean, int subjectId) {
        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlItem).getLayoutParams();
        layoutParams.height = rvHeight / 3;
        holder.getChildView(R.id.rlItem).setLayoutParams(layoutParams);

        if (groupBean.getIsRead() == 0) {
            holder.getChildView(R.id.dot).setVisibility(View.VISIBLE);
        } else {
            holder.getChildView(R.id.dot).setVisibility(View.GONE);
        }

        if (groupBean.getState() == 0) {
            holder.getChildView(R.id.tvStatus).setVisibility(View.VISIBLE);
            ((ImageView) holder.getChildView(R.id.imDiscuss)).setImageDrawable(mActivity.getResources().getDrawable(R.mipmap.im_discuss_add));
            holder.setText(R.id.tvDiscuss, "参与讨论");
        } else {
            holder.setText(R.id.tvDiscuss, "讨论结果");
            holder.getChildView(R.id.tvStatus).setVisibility(View.GONE);
            ((ImageView) holder.getChildView(R.id.imDiscuss)).setImageDrawable(mActivity.getResources().getDrawable(R.mipmap.im_discuss_result));
        }
        holder.setText(R.id.tvName, groupBean.getChatTitle());
        holder.setText(R.id.tvContent, groupBean.getChatContent());
        holder.setText(R.id.tvDate, groupBean.getCreateTime() + "--" + groupBean.getEndTime());

        Subject subject = Subject.subjects.get(subjectId);
        if (subject != null) {
            if (subjectId > 0) {
                ((ImageView) holder.getChildView(R.id.ivXueke)).setImageDrawable(mActivity.getResources().getDrawable(subject.getDrawableId()));
            } else if (subjectId == 0) {
                Subject mSubject = Subject.subjects.get(Integer.parseInt(groupBean.getSubjectId() == null ? "0" : groupBean.getSubjectId()));
                ((ImageView) holder.getChildView(R.id.ivXueke)).setImageDrawable(mActivity.getResources().getDrawable(mSubject.getDrawableId()));

            }
        }

    }


}
