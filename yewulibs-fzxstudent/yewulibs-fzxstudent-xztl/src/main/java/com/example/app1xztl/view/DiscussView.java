package com.example.app1xztl.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.GroupChatTask;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.sdzn.fzx.student.vo.chatroombean.DiscussionPicList;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

/**
 * Created by Administrator on 2019/8/11 0011.
 */

public interface DiscussView extends BaseView {

    void networkError(String msg);

    void onUploadPicSuccess(UploadPicVo uploadVos);

    void onGroupSuccess(GroupListBean.DataBean groupChatTask);

    void returnSuccess();

    void getChatPic(DiscussionPicList picList);
}
