package com.example.app1xztl.presenter;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.core.content.FileProvider;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.activity.ImageSlideActivity;
import com.example.app1xztl.view.DiscussView;
import com.google.gson.Gson;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.listener.AlbumOrCameraListener;
import com.sdzn.fzx.student.libpublic.utils.chatroom.GlideHelper;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.ImageUploadInfoBean;
import com.sdzn.fzx.student.vo.QiNiuUrlVo;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.sdzn.fzx.student.vo.chatroombean.DiscussionPicList;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;
import com.sdzn.fzx.student.vo.chatroombean.ImageSlideBean;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.RequestBody;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.utils.ImageUtils;
import rain.coder.photopicker.utils.UCropUtils;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2019/8/14 0012.
 */

public class DiscussPresenter extends BasePresenter<DiscussView, BaseActivity> {
    public static final int REQ_CODE_CAMERA = 1003;

    public void getChatIsRead(String groupChatTaskId) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("groupChatTaskId", groupChatTaskId);
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Network.createService(NetWorkService.TalkService.class, BuildConfig2.SERVER_ISERVICE_NEW2)
                .getChatIsRead("Bearer " + (String) SPUtils.getInstance().getString("token", ""), requestBody)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {


                    }
                });
    }

    public void getGroupChatTask(String groupChatTaskId,String groupId) {
//        Network.createTokenService(NetWorkService.ChatService.class)
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("groupChatTaskId", groupChatTaskId);
        requestParams.put("groupId", groupId);
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Network.createService(NetWorkService.TalkService.class, BuildConfig2.SERVER_ISERVICE_NEW2)
                .getGroupChatTask("Bearer " + (String) SPUtils.getInstance().getString("token", ""), requestBody)
                .map(new StatusFunc<GroupListBean.DataBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GroupListBean.DataBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
//                        mView.networkError(e.getMessage());
                    }

                    @Override
                    public void onNext(GroupListBean.DataBean o) {
                        mView.onGroupSuccess(o);

                    }
                });
    }

    public void insertChatPic(Map<String, Object> parms) {
        String json = new Gson().toJson(parms);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Network.createService(NetWorkService.TalkService.class, BuildConfig2.SERVER_ISERVICE_NEW2)
                .insertChatTaskPic("Bearer " + (String) SPUtils.getInstance().getString("token", ""), requestBody)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("上传失败");
                            }
                        } else {
                            mView.networkError("上传失败");
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        mView.returnSuccess();

                    }
                }, mActivity, true, true, false, ""));
    }

    public void getChatPic(Map<String, Object> parms) {
//        Network.createTokenService(NetWorkService.ChatService.class)
        String json = new Gson().toJson(parms);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Network.createService(NetWorkService.TalkService.class, BuildConfig2.SERVER_ISERVICE_NEW2)
                .getChatTaskPic("Bearer " + (String) SPUtils.getInstance().getString("token", ""), requestBody)
                .map(new StatusFunc<DiscussionPicList>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<DiscussionPicList>(new SubscriberListener<DiscussionPicList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
//                        mView.networkError(e.getMessage());
                    }

                    @Override
                    public void onNext(DiscussionPicList o) {
                        mView.getChatPic(o);
                    }
                }, mActivity, true, true, false, ""));
    }


    public void bindHolder(int rvHeight, final GeneralRecyclerViewHolder holder, DiscussionPicList.DataBean oldList, final ArrayList<ImageSlideBean> picsBeanList, final boolean isResult, final String groupId) {
//        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlItem).getLayoutParams();
//        layoutParams.height = rvHeight / 3;
//        holder.getChildView(R.id.rlItem).setLayoutParams(layoutParams);
        holder.getChildView(R.id.tv_name).setVisibility(View.VISIBLE);
        holder.setText(R.id.tv_name, oldList.getUserStudentName());

        GlideHelper.load(mActivity, oldList.getPicUrl(), (ImageView) holder.getChildView(R.id.iv_image));

        ((RelativeLayout) holder.getChildView(R.id.rl_image)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, ImageSlideActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position", holder.getAdapterPosition());
                bundle.putString("groupId", groupId);
                bundle.putParcelableArrayList("pics", picsBeanList);
                if (isResult) {
                    bundle.putBoolean("isResult", true);
                } else {
                    bundle.putBoolean("isResult", false);
                }
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
            }
        });

    }

    private String key;

    public void uploadSubjectivePhoto(final List<Photo> photoLists) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
//                RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);

//                RequestParams params = new RequestParams(ApiInterface.INTERFACE_ADDRESS + ApiInterface.QINIU_TOKEN);
//                RequestParams params = new RequestParams(ApiInterface.QINIU_TOKEN);
                RequestParams params = new RequestParams(BuildConfig2.SERVER_ISERVICE_NEW2 + "/knowledgecenter/resource/getUploadToken");
                key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
                    ResponseSlbBean1 responseSlbBean1 = GsonUtil.fromJson(result, ResponseSlbBean1.class);
                    if (responseSlbBean1 == null || responseSlbBean1.getResult() == null) {
                        throw new RuntimeException("");//上传失败: 获取upToken失败
                    }
                    return Observable.just(new ImageUploadInfoBean(key, responseSlbBean1.getResult().toString(), bitmap));
                } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
//                RequestParams params = new RequestParams(ApiInterface.INTERFACE_ADDRESS +"/teach/QiniuController/getUptoken");
//
//                String key = getKey();
//                params.addBodyParameter("key", key);
//                params.addBodyParameter("access_token", UserController.getAccessToken());
//                try {
//                    String result = x.http().getSync(params, String.class);
//                    QiniuUptoken uptoken = GsonUtil.fromJson(result, QiniuUptoken.class);
//                    if (uptoken == null || uptoken.getResult() == null || TextUtils.isEmpty(uptoken.getResult().getUpToken())) {
//                        throw new RuntimeException("上传失败: 获取upToken失败");
//                    }
//                    QiniuUptoken.ResultBean bean = uptoken.getResult();
//                    return Observable.just(new ImageUploadInfoBean(key, bean.getUpToken(), bean.getDomain(), bean.getImageStyle(), bitmap));
//                } catch (Throwable e) {
//                    e.printStackTrace();
//                    throw new RuntimeException(e);
//                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        mView.networkError(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
//
//                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
//                                            .create(bean.getDomain(), key, bean.getImageStyle());
//
//                                    uploadPicVoList.add(dataBean);
//
//                                    if (uploadPicVoList.size() == photoLists.size()) {
//                                        pdm.cancelWaiteDialog();
//                                        UploadPicVo mUploadPicVo = new UploadPicVo();
//                                        mUploadPicVo.setData(uploadPicVoList);
//                                        mView.onUploadPicSuccess(mUploadPicVo);
//                                    }
                                    getQiNiuUrl(key, uploadPicVoList, photoLists, pdm);

                                } else {
                                    pdm.cancelWaiteDialog();
                                    mView.networkError(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }

    private void getQiNiuUrl(String key, final List<UploadPicVo.DataBean> uploadPicVoList, final List<Photo> photoLists, final ProgressDialogManager pdm) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("fileName", key);
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
//        Network.createTokenService(NetWorkService.ChatService.class)
        Network.createService(NetWorkService.TalkService.class, BuildConfig2.SERVER_ISERVICE_NEW2)
                .getQiNiuUrl(requestBody)
                .map(new StatusFunc<QiNiuUrlVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<QiNiuUrlVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(QiNiuUrlVo o) {
//                            o.getFileUrl();

                        uploadPicVoList.add(new UploadPicVo.DataBean(o.getFileUrl()));

                        if (uploadPicVoList.size() == photoLists.size()) {
                            UploadPicVo mUploadPicVo = new UploadPicVo();
                            mUploadPicVo.setData(uploadPicVoList);
                            mView.onUploadPicSuccess(mUploadPicVo);
                        }
//
                    }
                });
        pdm.cancelWaiteDialog();
//        com.alibaba.fastjson.JSONObject requestData = new com.alibaba.fastjson.JSONObject();
//        requestData.put("fileName", key);
//        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
//        RetrofitNetNew.build(Api.class, getIdentifier())
//                .getQiNiuUrl(tokenStr, requestBody)//SPToken.getToken(),
//                .enqueue(new Callback<ResponseSlbBean1<QiNiuUrlVo>>() {
//                    @Override
//                    public void onResponse(Call<ResponseSlbBean1<QiNiuUrlVo>> call, Response<ResponseSlbBean1<QiNiuUrlVo>> response) {
//                        if (!hasView()) {
//                            return;
//                        }
//                        if (response.body() == null) {
//                            return;
//                        }
//                        if (response.body().getCode() != 0) {
//                            getView().onFailed(response.body().getMessage());
//                            return;
//                        }
//
//                        uploadHead(response.body().getResult().getFileUrl());
//                        call.cancel();
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseSlbBean1<QiNiuUrlVo>> call, Throwable t) {
//                        if (!hasView()) {
//                            return;
//                        }
//
//                        t.printStackTrace();
//                        call.cancel();
//                    }
//                });

    }

    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    private File takeImageFile;

    /**
     * 调用相机
     */
    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            if (FileUtil.existsSdcard()) {
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            } else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            Uri uri;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                uri = Uri.fromFile(takeImageFile);
            } else {
                uri = FileProvider.getUriForFile(mActivity, AppUtils.getAppPackageName() + ".fileProvider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities
                        (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    mActivity.grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        mActivity.startActivityForResult(takePictureIntent, REQ_CODE_CAMERA);
    }

    /**
     * 裁剪图片
     */
    public void startClipPic(boolean isFillBlank) {
        if (isFillBlank) {
            GalleryFinal.init(mActivity);
            GalleryFinal.openCrop(takeImageFile.getAbsolutePath(), new GalleryFinal.OnHanlderResultCallback() {
                @Override
                public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                    if (resultList == null || resultList.isEmpty()) {
                        onHanlderFailure(reqeustCode, "裁剪失败");
                        return;
                    }
//                   clipSuccess(resultList.get(0).getPhotoPath());
                    ToastUtil.showShortlToast("CAMERA-----" + resultList.get(0).getPhotoPath());
                }

                @Override
                public void onHanlderFailure(int requestCode, String errorMsg) {
//                    mView.clipFailed(errorMsg);
                }
            });
        } else {
            String imagePath = ImageUtils.getImagePath(mActivity, "/Crop/");
            File corpFile = new File(imagePath + ImageUtils.createFile());
            UCropUtils.start(mActivity, new File(takeImageFile.getAbsolutePath()), corpFile, false);
        }
    }

    /**
     * 发送 图片
     */
    public void showSelectImgDialog(final AlbumOrCameraListener listener) {
        final Dialog dialog = new Dialog(mActivity, R.style.Dialog);
        @SuppressWarnings("inflateParams")
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_album_or_camera, null);
        view.findViewById(R.id.tv_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlbum();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectCamera();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
    }

}
