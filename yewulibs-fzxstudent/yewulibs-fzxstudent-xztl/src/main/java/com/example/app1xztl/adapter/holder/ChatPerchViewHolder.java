package com.example.app1xztl.adapter.holder;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app1xztl.R;
import com.example.app1xztl.adapter.ChatAdapter;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.sdzn.fzx.student.libutils.util.DateUtil;

/**
 * 作者：Rance on 2016/11/29 10:47
 * 邮箱：rance935@163.com
 */
public class ChatPerchViewHolder extends BaseViewHolder<ChatRoomMessage> {

    //@BindView(R.id.chat_item_date)
    private TextView textView;

    private final String SET_DATE = "1";
    private final String SET_NO_HISTORY = "0";

    private ChatAdapter.onItemClickListener onItemClickListener;
    private Handler handler;
    private String path = "";

    public ChatPerchViewHolder(ViewGroup parent, ChatAdapter.onItemClickListener onItemClickListener, Handler handler) {
        super(parent, R.layout.item_chat_perch);
        textView = itemView.findViewById(R.id.chat_item_date);
        this.onItemClickListener = onItemClickListener;
        this.handler = handler;
    }


    @Override
    public void setData(ChatRoomMessage data) {
        if (data.getMsgType() == MsgTypeEnum.custom) {
            if (textView==null){
                return;
            }
            if (SET_DATE.equals(data.getContent())) {
                textView.setText(DateUtil.getTimeStrByTimemillis(data.getTime(), "MM月dd日  HH:mm") + "");
            } else if (SET_NO_HISTORY.equals(data.getContent())) {
                textView.setText("已经没有历史记录啦");
            } else {
                textView.setVisibility(View.GONE);
            }
        }
    }
}
