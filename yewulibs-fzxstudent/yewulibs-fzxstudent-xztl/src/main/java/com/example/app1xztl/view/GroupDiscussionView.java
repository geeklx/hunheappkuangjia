package com.example.app1xztl.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.SubjectVo;

/**
 * View  GroupDiscussionFragment
 *
 * @author zhaosen
 */
public interface GroupDiscussionView extends BaseView {

    void getSubjectSuccess(SubjectVo subjectVo);

    void networkError(String msg);
}
