package com.example.app1xztl.activity;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.adapter.EllGridViewAdapter;
import com.example.app1xztl.fragment.MyChatRoomMessageFragment;
import com.example.app1xztl.listener.OnDiscussListener;
import com.example.app1xztl.presenter.DiscussPresenter;
import com.example.app1xztl.view.DiscussView;
import com.just.agentweb.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomStatusChangeData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.ai.pop.CustomDrawerPopupView;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.listener.AlbumOrCameraListener;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libpublic.utils.GlideImageLoader;
import com.sdzn.fzx.student.libpublic.utils.chatroom.ExpandableLinearLayout;
import com.sdzn.fzx.student.libpublic.utils.chatroom.SubjectUtils;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.views.NoScrollGridView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.SoftKeyBoardListener;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.chatroom.SpacesItemVeiticalDecoration;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.sdzn.fzx.student.vo.chatroombean.DiscussionPicList;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;
import com.sdzn.fzx.student.vo.chatroombean.ImageSlideBean;
import com.sdzn.fzx.student.vo.chatroombean.SubjectBean;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;


/**
 * 小组讨论界面
 * <p>
 * 需要从h5获取的数据  id  groupChatTaskId  groupId groupName
 */
public class DiscussActivity extends MBaseActivity<DiscussPresenter> implements DiscussView, View.OnClickListener, OnDiscussListener, OnRefreshListener {
    private RelativeLayout relContent;
    private RelativeLayout rlTop;
    private TextView mTvBack;
    private LinearLayout llPackBack;
    private ImageView imMember;
    private TextView tvTitle;
    private View line;
    private RelativeLayout rlResult;
    private RelativeLayout llDiscussResult;
    private LinearLayout llResult;
    private TextView tvResult;
    private TextView tvResultSend;
    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView rv;
    private LinearLayout llEmpty;
    private TextView tvEmpty;
    private TextView tvChapterName;
    private View viewLine;

    private ExpandableLinearLayout ellTop;
    private TextView tvArrow;
    private TextView tvSubject;
    private FrameLayout chatRoomMessageFragment;


    private boolean isResultSend = false;//右下角发送
    private boolean isResult = false;//false  能发送   true 跳转到其他小组
    //右侧 列表刷新
    private Y_MultiRecyclerAdapter adapter;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();
    private static int DISCUSS_OLD = 1;
    private static int DISCUSS_NOW = 0;
    public static final int REQ_CODE_CAMERA = 1003;
    /**
     * 子页面
     */
    private MyChatRoomMessageFragment messageFragment;
    private AbortableFuture<EnterChatRoomResultData> enterRequest;

    private boolean hasEnterSuccess = false; // 是否已经成功登录聊天室

    private EllGridViewAdapter ellGridViewAdapter;

    @Override
    public void initPresenter() {
        mPresenter = new DiscussPresenter();
        mPresenter.attachView(this, this);
    }

    private Bundle bundle;
    private String title, content, endTime, createTime = "2019-09-11 13:50:09";
    private int state, isRead;
    private String groupChatTaskId = "277", id = "614", groupId = "43", groupName;
    private String roomId = "117795375";
    private String picJsonStr = "";
    private String score;
    private ArrayList<GroupListBean.DataBean.TeachGroupChatResultPicsBean> pics = new ArrayList<>();
    private static final int REQUECT_CODE_CAMERA = 1100;
    private static final int IS_READ = 0;
    private TextView tvMyTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_discuss);
        relContent = (RelativeLayout) findViewById(R.id.rel_content);
        rlTop = (RelativeLayout) findViewById(R.id.rl_top);
        mTvBack = (TextView) findViewById(R.id.tvBack);
        llPackBack = (LinearLayout) findViewById(R.id.ll_pack_Back);
        imMember = (ImageView) findViewById(R.id.imMember);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        line = (View) findViewById(R.id.line);
        rlResult = (RelativeLayout) findViewById(R.id.rl_result);
        llDiscussResult = (RelativeLayout) findViewById(R.id.ll_discuss_result);
        llResult = (LinearLayout) findViewById(R.id.llResult);
        tvResult = (TextView) findViewById(R.id.tvResult);
        tvResultSend = (TextView) findViewById(R.id.tv_result_send);
        refreshLayout = (SmartRefreshLayout) findViewById(R.id.refreshLayout);
        rv = (EmptyRecyclerView) findViewById(R.id.rv);
        llEmpty = (LinearLayout) findViewById(R.id.llEmpty);
        tvEmpty = (TextView) findViewById(R.id.tvEmpty);
        tvChapterName = (TextView) findViewById(R.id.tv_chapter_name);
        viewLine = (View) findViewById(R.id.view_line);
        ellTop = (ExpandableLinearLayout) findViewById(R.id.ell_product);
        chatRoomMessageFragment = (FrameLayout) findViewById(R.id.chat_room_message_fragment);

        tvTitleEll = findViewById(R.id.tv_title);
        tvDateEll = findViewById(R.id.tv_date);
        tvStatus = findViewById(R.id.tvStatus);
        tvArrow = (TextView) findViewById(R.id.tv_discuss_arrow);
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);//个人中心按钮
        tvSubject = (TextView) findViewById(R.id.tv_subject);

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            Log.e("eeeeeappLinkIntent", "" + appLinkIntent);
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    id = appLinkData.getQueryParameter("id");
                    groupChatTaskId = appLinkData.getQueryParameter("groupChatTaskId");
                    groupId = appLinkData.getQueryParameter("groupId");
                    groupName = appLinkData.getQueryParameter("groupName");
                    roomId = appLinkData.getQueryParameter("roomId");
//                    if (appLinkData.getQueryParameter("query1")!=null) {
//                        defId = Integer.valueOf(appLinkData.getQueryParameter("query1"));
//                    }else {
//                        defId=0;
//                    }
                }
            }
        }
        Log.e("eeeeee", "aaaaaaa--" + "id--" + id +
                "\ngroupChatTaskId--" + groupChatTaskId + "\ngroupId--" + groupId + "\nroomId--" + roomId);


        view = View.inflate(this, R.layout.view_discuss_ell_top, null);

        tvContentEll = view.findViewById(R.id.tv_content);
        gridView = view.findViewById(R.id.gridView);
//        // ATTENTION: This was auto-generated to handle app links.
//        Intent appLinkIntent = getIntent();
//        if (appLinkIntent != null) {
//            String appLinkAction = appLinkIntent.getAction();
//            if (appLinkAction != null) {
//                Uri appLinkData = appLinkIntent.getData();
//                if (appLinkData != null) {
//                    String aaaa = appLinkData.getQueryParameter("query1");
//                    String bbbb = appLinkData.getQueryParameter("query2");
////                    final boolean isGuidance = (boolean) StudentSPUtils.get(DiscussActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
////                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
//                    if (!SlbLoginUtil2.get().isUserLogin()) {
//                        //isGuidance=true失败回退到activity
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
//                        startActivity(intent);
//                    } else {
//                        //isGuidance=false成功进入ctivity
//                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
//                    }
//
//                }
//            }
//        }
        mPresenter.getGroupChatTask(groupChatTaskId, groupId);
        setListener();

    }

    TextView tvTitleEll;
    TextView tvContentEll;
    TextView tvDateEll;
    TextView tvStatus;
    NoScrollGridView gridView;
    View view;

    @Override
    protected void onResume() {
        super.onResume();
        getChatPicData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (customDrawerPopupView != null && customDrawerPopupView.isShow()) {
            customDrawerPopupView.dismiss();
        }
    }

    CustomDrawerPopupView customDrawerPopupView;
    private void setListener(){
        mTvBack.setOnClickListener(this);
        llPackBack.setOnClickListener(this);
        imMember.setOnClickListener(this);

        tvMyTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 customDrawerPopupView = new CustomDrawerPopupView(DiscussActivity.this, new CustomDrawerPopupView.GrzxNextCallBack() {
                    @Override
                    public void toGrzxNextClick() {
                        ToastUtils.showShort("个人");

                    }
                });
                new XPopup.Builder(DiscussActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(customDrawerPopupView)
                        .show();
            }
        });
    }

    @Override
    protected void initView() {
        tvResultSend.setOnClickListener(this);
        //右侧 列表刷新
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        rv.setEmptyView(llEmpty);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(App2.get());
        rv.setLayoutManager(linearLayoutManager);
        adapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        rv.setAdapter(adapter);
        rv.addItemDecoration(new SpacesItemVeiticalDecoration(10));
        rv.addOnItemTouchListener(new OnItemTouchListener(rv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
            }
        });

        adapter.notifyDataSetChanged();


        SoftKeyBoardListener.setListener(DiscussActivity.this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {

            @Override
            public void onKeyBoardShow(int height) {
//                Toast.makeText(DiscussActivity.this, "键盘显示 高度" + height, Toast.LENGTH_SHORT).show();
                isBoardShow = true;
                if (false == isEllTop) {

                } else {
                    ellTop.toggle();
                }
            }

            @Override
            public void onKeyBoardHide(int height) {
                isBoardShow = false;
            }
        });

    }


    /**
     * 隐藏键盘
     */
    protected void hideInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View v = getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

    }

    private boolean isBoardShow = false;

    @Override
    protected void initData() {

    }

    /**
     * 头部展示view  可收起
     */

    private void initEllView() {
        ellTop.removeAllViews();//清除所有的子View（避免重新刷新数据时重复添加）

        if (pics.size() > 0) {
            ellGridViewAdapter = new EllGridViewAdapter(DiscussActivity.this, pics);
            gridView.setAdapter(ellGridViewAdapter);

        }
        if (isResult) {
            tvStatus.setVisibility(View.GONE);
        } else {
            tvStatus.setVisibility(View.VISIBLE);
        }
        tvTitleEll.setText(title);
        tvContentEll.setText(content);
        tvDateEll.setText("" + endTime);

        ellTop.addItem(view);
//        ellTop.toggle();//初始 展开
        tvArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBoardShow == true && isEllTop == false) {
                    hideInput();
                }
                ellTop.toggle();
            }
        });
        ellTop.setOnStateChangeListener(new ExpandableLinearLayout.OnStateChangeListener() {
            @Override
            public void onStateChanged(boolean isExpanded) {
                isEllTop = isExpanded;
                if (isExpanded == false) {
//                if (isExpanded == false && isBoardShow == true) {
                    tvArrow.setText("查看详情");
                } else {
                    tvArrow.setText("收起详情");
                }
            }
        });

    }

    private boolean isEllTop = false;

    private void initMessageFragment() {
//        messageFragment = (ChatRoomMessageFragment) getSupportFragmentManager().findFragmentById(R.id.chat_room_message_fragment);
        messageFragment = new MyChatRoomMessageFragment();
        if (messageFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.chat_room_message_fragment, messageFragment);
            transaction.commit();
            messageFragment.init(roomId, DateUtil.getTimeMillisByStr(createTime, "yyyy-MM-dd HH:mm:ss"));
        } else {
            // 如果Fragment还未Create完成，延迟初始化
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    initMessageFragment();
                }
            }, 50);
        }
    }

    private void enterRoom() {
        hasEnterSuccess = false;
        EnterChatRoomData data = new EnterChatRoomData(roomId);
        enterRequest = NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 4);
        enterRequest.setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData result) {
//                NimUIKit.enterChatRoomSuccess(result, false);
                initMessageFragment();
                ToastUtil.showShortlToast("进入聊天室");
                Log.e("eeeeee", "aaaaaaaaaaaaaa进入聊天室");
            }

            @Override
            public void onFailed(int code) {
                Log.e("eeeeee", "aaaaaaaaaaaaaa聊天室onFailed" + code);
                if (code == ResponseCode.RES_CHATROOM_BLACKLIST) {
                    ToastUtil.showShortlToast("你已被拉入黑名单，不能再进入");
                } else if (code == ResponseCode.RES_ENONEXIST) {
                    ToastUtil.showShortlToast("聊天室不存在");
                } else if (code == ResponseCode.RES_EXCEPTION) {
                    if (hasEnterSuccess) {
                        hasEnterSuccess = false;
                        enterRoom();
                    } else {
                        ToastUtil.showShortlToast("聊天室登录失败");
                    }
                } else {
                    ToastUtil.showShortlToast("聊天室登录失败, " + code);
                }
                initMessageFragment();

            }

            @Override
            public void onException(Throwable exception) {
                Log.e("eeeeee", "aaaaaaaaaaaaaa聊天室onException" + exception.getMessage());
                ToastUtil.showShortlToast("聊天室登录失败, e=" + exception.getMessage());
                initMessageFragment();
            }
        });
    }


    private void registerObservers(boolean register) {
        NIMClient.getService(ChatRoomServiceObserver.class).observeOnlineStatus(onlineStatus, register);
        NIMClient.getService(ChatRoomServiceObserver.class).observeKickOutEvent(kickOutObserver, register);
    }

    private void logoutChatRoom() {
        NIMClient.getService(ChatRoomService.class).exitChatRoom(roomId);
        onExitedChatRoom();
    }

    public void onExitedChatRoom() {
        NIMClient.getService(AuthService.class).logout();
    }

    Observer<ChatRoomStatusChangeData> onlineStatus = new Observer<ChatRoomStatusChangeData>() {
        @Override
        public void onEvent(ChatRoomStatusChangeData chatRoomStatusChangeData) {
            if (!chatRoomStatusChangeData.roomId.equals(roomId)) {
                return;
            }

            if (chatRoomStatusChangeData.status == StatusCode.UNLOGIN) {
                int errorCode = NIMClient.getService(ChatRoomService.class).getEnterErrorCode(roomId);
                // 如果遇到错误码13001，13002，13003，403，404，414，表示无法进入聊天室，此时应该调用离开聊天室接口。
                Log.e("eeeeee", "aaaaaaonlineStatus--errorCode" + errorCode);
                if (errorCode != 13001 || errorCode != 13002 || errorCode != 403 || errorCode != 404 || errorCode != 414) {
                    enterRoom();
                }
            }


        }
    };

    Observer<ChatRoomKickOutEvent> kickOutObserver = new Observer<ChatRoomKickOutEvent>() {
        @Override
        public void onEvent(ChatRoomKickOutEvent chatRoomKickOutEvent) {

            onExitedChatRoom();
        }
    };

    /**
     * 获取成员列表 成员
     */
    private void getMember() {
        // 以游客为例，从最新时间开始，查询30条
        NIMClient.getService(ChatRoomService.class).fetchRoomMembers(roomId, MemberQueryType.ONLINE_NORMAL, 0, 30).setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
            @Override
            public void onResult(int code, List<ChatRoomMember> result, Throwable exception) {

            }
        });
    }


    private void onLoginDone() {
        enterRequest = null;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        logoutChatRoom();
    }

    private void login() {
        Log.e("eeeeee", "账号--" + UserController.getLoginBean().getData().getUser().getAccountId());
        LoginInfo info = new LoginInfo(UserController.getLoginBean().getData().getUser().getAccountId()
                , UserController.getLoginBean().getData().getUser().getAccountId());//MD5.md5("");

        RequestCallback<LoginInfo> callback =
                new RequestCallback<LoginInfo>() {
                    @Override
                    public void onSuccess(LoginInfo loginInfo) {
                        Log.e("eeeeee", "aaaaaaaaaaaaaaonSuccess---");
                        enterRoom();

                        hasEnterSuccess = true;
                    }

                    @Override
                    public void onFailed(int i) {
                        Log.e("eeeeee", "aaaaaaaaaaaaaonFailed---" + i);
                        if (i == 302 || i == 404) {
                            ToastUtil.showShortlToast(getResources().getString(R.string.login_failed));
                        }
                        if (i == ResponseCode.RES_EXCEPTION) {
                            if (!hasEnterSuccess) {
                                login();
                                hasEnterSuccess = true;
                            }
                        } else if (i == ResponseCode.RES_EEXIST) {
                            onExitedChatRoom();
                            if (!hasEnterSuccess) {
                                login();
                                hasEnterSuccess = true;
                            }
                        }
                        ToastUtil.showShortlToast("登录聊天室失败");
                        initMessageFragment();
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        Log.e("eeeeee", "aaaaaaaaaaaathrowable---" + throwable.toString());
                        ToastUtil.showShortlToast(getResources().getString(R.string.login_exception));
                        initMessageFragment();
                    }
                    // 可以在此保存LoginInfo到本地，下次启动APP做自动登录用
                };
        NIMClient.getService(AuthService.class).login(info)
                .setCallback(callback);
    }

    @Override
    public void onBackPressed() {
        if (messageFragment == null) {
            super.onBackPressed();
        }
        logoutChatRoom();
        DiscussActivity.this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        DiscussActivity.this.finish();
        Intent msgIntent = new Intent();
        msgIntent.setAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.ll_pack_Back) {
            DiscussActivity.this.finish();
            Intent msgIntent = new Intent();
            msgIntent.setAction("activityRefresh");
            LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
            logoutChatRoom();

        } else if (i == R.id.tv_result_send) {
            if (isResult) {//跳转
                Intent intent = new Intent(DiscussActivity.this, DiscussOtherActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("groupChatTaskId", groupChatTaskId);
                intent.putExtras(bundle);
                startActivity(intent);

            } else {//send
                isResultSend = true;
                if (isIntentPop) {
                    mPresenter.showSelectImgDialog(new AlbumOrCameraListener() {
                        @Override
                        public void selectAlbum() {
                            new PhotoPickConfig.Builder(mPresenter.mActivity)
                                    .imageLoader(new GlideImageLoader())
                                    .showCamera(false)
                                    .maxPickSize(selectAlbumNum)
                                    .spanCount(8)
                                    .clipPhoto(false)
                                    .mustClip(false)
                                    .build();
                        }

                        @Override
                        public void selectCamera() {
                            requestPermissiontest();
                        }
                    });
                }
            }

        } else if (i == R.id.imMember) {//                getMember();
            Intent intent = new Intent(this, MemberActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("groupId", String.valueOf(groupId));
            bundle.putString("groupName", String.valueOf(groupName));
            intent.putExtras(bundle);
            startActivity(intent);

        } else {
        }
    }

    private void resultSend(boolean isResult, List<Photo> paths) {
        if (isResult) {//true
            isResultSend = false;
            mPresenter.uploadSubjectivePhoto(paths);
        }
    }

    @Override
    protected void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (isResultSend) {
            if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {//相册返回图片  图片选择器
                if (resultCode == Activity.RESULT_OK) {
                    if (data.getBooleanExtra("isClip", false)) {
                        String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                        List<Photo> paths = new ArrayList<>();
                        Photo photo = new Photo();
                        photo.setPath(path);
                        paths.add(photo);
                        resultSend(isResultSend, paths);
                    } else {
                        ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                        if (photoLists != null && !photoLists.isEmpty()) {
                            resultSend(isResultSend, photoLists);
                        }
                    }

                }
            } else if (requestCode == REQ_CODE_CAMERA) {//拍照返回
                if (resultCode == Activity.RESULT_OK) {
                    mPresenter.startClipPic(false);
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                if (resultCode == Activity.RESULT_OK) {

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(UCrop.getOutput(data));
                    paths.add(photo);
                    resultSend(isResultSend, paths);
                }
            }
        } else {
            if (messageFragment != null) {
                messageFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
/* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (isResultSend) {
            if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {//相册返回图片  图片选择器
                if (resultCode == Activity.RESULT_OK) {
                    if (data.getBooleanExtra("isClip", false)) {
                        String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                        List<Photo> paths = new ArrayList<>();
                        Photo photo = new Photo();
                        photo.setPath(path);
                        paths.add(photo);
                        resultSend(isResultSend, paths);
                    } else {
                        ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                        if (photoLists != null && !photoLists.isEmpty()) {
                            resultSend(isResultSend, photoLists);
                        }
                    }

                }
            } else if (requestCode == REQ_CODE_CAMERA) {//拍照返回
                if (resultCode == Activity.RESULT_OK) {
                    mPresenter.startClipPic(false);
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                if (resultCode == Activity.RESULT_OK) {

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(UCrop.getOutput(data));
                    paths.add(photo);
                    resultSend(isResultSend, paths);
                }
            }
        } else {
            if (messageFragment != null) {
                messageFragment.onActivityResult(requestCode, resultCode, data);
            }
        }

    }*/

    /**
     * myChatFragment--->发过来
     * 涉及图片地址处理
     */
    @Override
    public void setResultSendStatus() {
        isResultSend = false;

    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        getChatPicData();
        cancilLoadState();
    }


    /**
     * 讨论结果上传图片 返回结果
     */
    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);

    }

    /**
     * 此处刷新 或 重新加载接口
     */
    @Override
    public void onUploadPicSuccess(UploadPicVo uploadVos) { //pic
        if (uploadVos.getData().size() > 0) {
            List<String> insertList = new ArrayList<>();
            for (int i = 0; i < uploadVos.getData().size(); i++) {
                insertList.add(uploadVos.getData().get(i).getOriginalPath());
            }
            picJsonStr = buildChatPic(insertList).toString();
            insertPic();
        }
    }

    @Override
    public void onGroupSuccess(GroupListBean.DataBean groupChatTask) {
        isRead = groupChatTask.getIsRead();
        state = groupChatTask.getState();
        title = groupChatTask.getChatTitle();
        content = groupChatTask.getChatContent();
        endTime = groupChatTask.getEndTime();
        createTime = groupChatTask.getCreateTime();
        score = groupChatTask.getScore();
        pics.addAll(groupChatTask.getTeachGroupChatResultPics());
        if (groupChatTask.getSubjectName()!=null&& !TextUtils.isEmpty(groupChatTask.getSubjectName())){
            if (groupChatTask.getSubjectName().length()>1){
                setSubject(groupChatTask.getSubjectName());
            }
        }
        if (state == DISCUSS_NOW) {
            isResult = false;
        } else if (state == DISCUSS_OLD) {
            isResult = true;
            llResult.setVisibility(View.VISIBLE);
            tvResult.setText("" + score);

            tvResultSend.setText("其他小组讨论结果");
//            tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send_no));
            tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send));
        }
        if (isRead == IS_READ) {
            mPresenter.getChatIsRead(String.valueOf(groupChatTaskId));
        }

        // 注册监听
        registerObservers(true);
        initView();
        Log.e("eeeeee", "aaaaaaaa---在外面");
        if (!isResult) {
            Log.e("eeeeee", "aaaaaaaa---进入");
            login();
        }
        tvChapterName.setText(groupChatTask.getChapterName()+"");
        initEllView();

    }

    private void setSubject(String name){
        String subjectColor="";
        String subjectStr="";
        List<SubjectBean> subjectBeanList = SubjectUtils.getSubject();
        for (int i=0;i<subjectBeanList.size();i++){
            if (name.trim().equals(subjectBeanList.get(i).getId())){
                subjectStr = subjectBeanList.get(i).getDesc();
                subjectColor = subjectBeanList.get(i).getColor();
            }
        }

        if (!TextUtils.isEmpty(subjectStr)&&!TextUtils.isEmpty(subjectColor)) {
            Drawable normal = ContextCompat.getDrawable(App2.get(), R.drawable.bg_discuss_top_subject);
            GradientDrawable drawable = (GradientDrawable) normal;
            drawable.setStroke(1, Color.parseColor(subjectColor));
            tvSubject.setBackground(drawable);
            tvSubject.setText(subjectStr+"");
            tvSubject.setTextColor(Color.parseColor(subjectColor));
        }else {
            tvSubject.setVisibility(View.GONE);
        }


    }

    private void insertPic() {
        Log.e("eeeeee", "aaaaaaaaa---insertPic" + "\ngroupChatTaskPicJson---" + picJsonStr +
                "\ngroupChatTaskId---" + groupChatTaskId +
                "\ngroupChatResultId---" + id +
                "\ngroupId---" + groupId +
                "\ngroupName---" + groupName);
        Map<String, Object> parm = new HashMap<>();
        parm.put("groupChatTaskPicJson", picJsonStr);
        parm.put("groupChatTaskId", groupChatTaskId);
        parm.put("groupChatResultId", id);
        parm.put("groupId", groupId);
//        parm.put("groupName", UserController.getLoginBean().getData().getUser().getClassGroupName());
        parm.put("groupName", groupName);
        mPresenter.insertChatPic(parm);
    }

    private JSONArray buildChatPic(List<String> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (String url : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pic", url);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    /**
     * 插入图片  成功
     */
    @Override
    public void returnSuccess() {
        refreshLayout.autoRefresh();
    }

    private void getChatPicData() {
        Map<String, Object> parm = new HashMap<>();
        parm.put("groupChatTaskId", groupChatTaskId);
        parm.put("groupChatResultId", id);
        parm.put("groupId", groupId);
        mPresenter.getChatPic(parm);
    }

    private int selectAlbumNum = 2;
    private boolean isIntentPop = true;

    @Override
    public void getChatPic(final DiscussionPicList picList) {
        itemEntityList.clear();
        if (picList.getData().size() > 0) {
            int myCount = 0;
            final ArrayList<ImageSlideBean> picsBeanList = new ArrayList<>();
            for (int i = 0; i < picList.getData().size(); i++) {
                picsBeanList.add(new ImageSlideBean(picList.getData().get(i).getId(),
                        picList.getData().get(i).getPicUrl(), picList.getData().get(i).getGroupId(), String.valueOf(picList.getData().get(i).getUserStudentId())));
                if (picList.getData().get(i).getGroupId().equals(UserController.getLoginBean().getData().getUser().getClassGroupId())
                        && picList.getData().get(i).getUserStudentId() == UserController.getLoginBean().getData().getUser().getId()) {
                    myCount++;
                }
            }
            if (state == DISCUSS_NOW) {
                if (myCount > 1) {//当多余两张时不能在发送
                    isIntentPop = false;
//                    tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send_no));
                    tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send));
                } else {
                    isIntentPop = true;
                    if (myCount > 0) {
                        selectAlbumNum = 1;
                    } else {
                        selectAlbumNum = 2;
                    }
                    tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send));
                }
            }
            itemEntityList.addItems(R.layout.item_discuss_result_recycler, picList.getData())
                    .addOnBind(R.layout.item_discuss_result_recycler, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindHolder(rv.getHeight(), holder, (DiscussionPicList.DataBean) itemData, picsBeanList, isResult, groupId);
                        }
                    });
//        refreshLayout.setEnableLoadMore(false);
            adapter.notifyDataSetChanged();
            cancilLoadState();

        } else {
            rv.setEmptyView(llEmpty);
            isIntentPop = true;
            selectAlbumNum = 2;

        }
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    /**
     * 相机权限
     */
    public void requestPermissiontest() {
//        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
//        EasyPermissions.requestPermissions(this, "请设置相机权限",
//                REQUECT_CODE_CAMERA, perms);
        methodRequiresTwoPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @AfterPermissionGranted(REQUECT_CODE_CAMERA)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            mPresenter.toSystemCamera();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置相机权限",
                    REQUECT_CODE_CAMERA, perms);
        }
    }
//
//    @PermissionGrant(REQUECT_CODE_CAMERA)
//    public void requestSdcardSuccess() {
//        mPresenter.toSystemCamera();
//    }
//
//    @PermissionDenied(REQUECT_CODE_CAMERA)
//    public void requestSdcardFailed() {
//        ToastUtil.showShortlToast("请设置相机权限");
//    }


}
