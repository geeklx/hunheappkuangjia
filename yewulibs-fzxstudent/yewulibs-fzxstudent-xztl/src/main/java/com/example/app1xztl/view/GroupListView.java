package com.example.app1xztl.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

/**
 * Created by Administrator on 2019/8/11 0011.
 */

public interface GroupListView extends BaseView {
    void getDiscussSuccess(GroupListBean discussBeanList);
}
