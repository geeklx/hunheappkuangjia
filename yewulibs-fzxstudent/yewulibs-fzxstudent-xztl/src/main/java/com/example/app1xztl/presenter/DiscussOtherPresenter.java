package com.example.app1xztl.presenter;

import com.blankj.utilcode.util.SPUtils;
import com.example.app1xztl.view.DiscussOtherView;
import com.google.gson.Gson;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.vo.chatroombean.ChatOther;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Administrator on 2019/8/16 0012.
 */

public class DiscussOtherPresenter extends BasePresenter<DiscussOtherView, BaseActivity> {


    public void getDiscussResult(String groupChatTaskId) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("groupChatTaskId", groupChatTaskId);
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Network.createService(NetWorkService.TalkService.class, BuildConfig2.SERVER_ISERVICE_NEW2)
                .getOtherGroupChatTask("Bearer " + (String) SPUtils.getInstance().getString("token", ""), requestBody)
                .map(new StatusFunc<ChatOther>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ChatOther>(new SubscriberListener<ChatOther>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable != null) {
                            mView.netError(throwable.toString());
                        }
                    }

                    @Override
                    public void onNext(ChatOther chatOtherBean) {
                        mView.getDiscussResult(chatOtherBean);
                    }
                }, mActivity, true, false, false, ""));

    }


}
