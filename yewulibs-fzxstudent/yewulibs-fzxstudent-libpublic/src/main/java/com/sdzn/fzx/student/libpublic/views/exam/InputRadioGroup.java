package com.sdzn.fzx.student.libpublic.views.exam;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;

import com.sdzn.fzx.student.vo.ClozeAnswerBean;

/**
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-11
 */
public class InputRadioGroup extends RadioGroup {
    private FillBlankEditText et;//对应的填空控件
    private ClozeAnswerBean mBean;//填空的结果bean
    private int examPosition = -1;//第几个试题

    public InputRadioGroup(Context context) {
        super(context);
    }

    public InputRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FillBlankEditText getEditText() {
        return et;
    }

    public void setEditText(FillBlankEditText et) {
        this.et = et;
    }

    public ClozeAnswerBean getBean() {
        return mBean;
    }

    public void setBean(ClozeAnswerBean bean) {
        mBean = bean;
    }

    public int getExamPosition() {
        return examPosition;
    }

    public void setExamPosition(int examPosition) {
        this.examPosition = examPosition;
    }
}