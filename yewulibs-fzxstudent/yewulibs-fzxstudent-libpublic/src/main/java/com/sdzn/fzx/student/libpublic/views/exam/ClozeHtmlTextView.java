package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import androidx.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.SparseArray;

import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/14
 * 修改单号：
 * 修改内容:
 */
public class ClozeHtmlTextView extends BaseHtmlTextView {
    private SparseArray<ClozeAnswerBean> mAnswerMap = new SparseArray<>();
    private boolean showIndex;//空位默认不显示索引

    private ArrayList<CharSequence> mTextBodyList = new ArrayList<>();

    public ClozeHtmlTextView(Context context) {
        super(context);
    }

    public ClozeHtmlTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ClozeHtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public ClozeHtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ClozeHtmlTextView);
        showIndex = a.getBoolean(R.styleable.ClozeHtmlTextView_showIndex,false);
        a.recycle();
    }

    public boolean isShowIndex() {
        return showIndex;
    }

    public void setShowIndex(boolean showIndex) {
        this.showIndex = showIndex;
        buildText();
    }

    @Override
    protected void glideLoadImageSuccess() {
        setHtmlText(html,null);
    }

    public void setHtmlText(String htmlText, List<AnswerListBean.ExamOptionBean> examOptionList) {
        getImageSourceAndAnswer(htmlText);
        Spanned spanned = HtmlTagHandler.fromHtml(htmlText, this, this);
        SparseArray<AnswerListBean.ExamOptionBean> arr = new SparseArray<>();
        if (examOptionList != null) {
            for (AnswerListBean.ExamOptionBean optionBean : examOptionList) {
                arr.append(optionBean.getSeq(), optionBean);
            }
        }
//        setText(spanned);
        setTextBody(spanned, arr);
    }

    private void getImageSourceAndAnswer(String source) {
        html = source;
        mMap.clear();
        mAnswerMap.clear();
        Document doc = Jsoup.parse(source);
        Elements img = doc.getElementsByTag("img");
        for (Element element : img) {
            String width = element.attr("width");
            String height = element.attr("height");
            String src = element.attr("src");
            if (width == null || width.isEmpty()) {
                width = "0";
            }
            if (height == null || height.isEmpty()) {
                height = "0";
            }
            mMap.put(src, new Point(Integer.valueOf(width), Integer.valueOf(height)));
        }
        //解析input标签
        Elements input = doc.getElementsByTag("input");
        for (Element element : input) {
            int index = Integer.valueOf(element.attr("index").trim());
            String value = element.attr("value");
            mAnswerMap.put(index, new ClozeAnswerBean(index, value));
        }
    }

    private void setTextBody(CharSequence mask, SparseArray<AnswerListBean.ExamOptionBean> blankTextList) {
        ArrayList<CharSequence> arr = new ArrayList<>();//用input标记切割后的试题文本
        ArrayList<CharSequence> list = new ArrayList<>();//用imp替换input标记后的试题文本
        Matcher matcher = Pattern.compile(DEFAULT_TAG).matcher(mask);
        int index = 0;
        int blankCount = 0;
        while (matcher.find()) {
            blankCount++;
            arr.add(mask.subSequence(index, matcher.start()));
            index = matcher.end();
        }
        arr.add(mask.subSequence(index, mask.length()));

        for (int i = 0; i < arr.size(); i++) {
            list.add(arr.get(i));
            if (i < blankCount) {
                list.add(new CharSequenceImp());
            }
        }

        //当填空位不足时补足填空位长度
        int count = blankCount - blankTextList.size();
        if (count > 0) {
            for (int i = 1; i <= blankCount; i++) {
                AnswerListBean.ExamOptionBean optionBean = blankTextList.get(i);
                if (optionBean == null) {
                    AnswerListBean.ExamOptionBean bean = new AnswerListBean.ExamOptionBean();
                    bean.setSeq(i);
                    bean.setMyAnswer("");
                    blankTextList.append(i, bean);
                }
            }
        }
        //填空内容添加到作答map中
        for (int i = 0; i < blankTextList.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            String str = blankTextList.valueAt(i).getMyAnswer();
            bean.setAnswer(str == null ? "" : str);
            bean.setSelected(blankTextList.valueAt(i).getIsRight() == 1);
        }
        setTextList(list);
    }

    private void setTextList(ArrayList<CharSequence> list) {
        if (list != null) {
            mTextBodyList.clear();
            mTextBodyList.addAll(list);
        }
        buildText();
    }

    /**
     * 设置文本后将原始文本与填充内容整合显示
     */
    private void buildText() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int count = 0;
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {
                int length = builder.length();
                builder.append(BLANK_CELL_STR);
                ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                if (showIndex) {
                    builder.append(bean.getValue());
                }
                builder.append(bean.getAnswer());
                builder.append(BLANK_CELL_STR);
                builder.setSpan(new UnderlineSpan(), length, builder.length() - 1,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (bean.isSelected()) {
                    builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.fragment_main_item_pigai)),
                            length, builder.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.fragment_main_text1)),
                            length, builder.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                count++;
            } else {//文本
                builder.append(sequence);
            }
        }
        setText(builder);
    }
}
