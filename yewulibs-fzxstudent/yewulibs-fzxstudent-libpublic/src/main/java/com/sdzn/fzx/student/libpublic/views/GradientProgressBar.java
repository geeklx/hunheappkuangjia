package com.sdzn.fzx.student.libpublic.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;

/**
 * 描述：仿iphone带进度的进度条，线程安全的View，可直接在线程中更新进度
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/15
 */
public class GradientProgressBar extends View {
    private Context mContext;

    private int mArcColor;
    private int mArcWidth;
    private Paint arcPaint;
    private Paint arcCirclePaint;
    private RectF arcRectF;
    private float mCurData = 0;
    private int arcStartColor;
    private int arcEndColor;
    private Paint startCirclePaint;

    public GradientProgressBar(Context context) {
        this(context, null);
    }

    public GradientProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GradientProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.GradientProgressBar, defStyleAttr, 0);
        mCurData = typedArray.getInt(R.styleable.GradientProgressBar_gradientProgress, 0);
        mArcColor = typedArray.getColor(R.styleable.GradientProgressBar_gradientColor, 0xFFFF0000);
        arcStartColor = typedArray.getColor(R.styleable.GradientProgressBar_gradientStartColor, 0xFFFF0000);
        arcEndColor = typedArray.getColor(R.styleable.GradientProgressBar_gradientEndColor, 0xFFFF0000);
        mArcWidth = typedArray.getDimensionPixelSize(R.styleable.GradientProgressBar_gradientWidth, UiUtils.dp2px(context, 20));

        typedArray.recycle();

        initPaint();
    }

    private void initPaint() {

        startCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        startCirclePaint.setStyle(Paint.Style.FILL);
        //startCirclePaint.setStrokeWidth(mArcWidth);
        startCirclePaint.setColor(arcStartColor);

        arcCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arcCirclePaint.setStyle(Paint.Style.STROKE);
        arcCirclePaint.setStrokeWidth(mArcWidth);
        arcCirclePaint.setColor(0xFFEDEDED);
        arcCirclePaint.setStrokeCap(Paint.Cap.ROUND);

        arcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arcPaint.setStyle(Paint.Style.STROKE);
        arcPaint.setStrokeWidth(mArcWidth);
        arcPaint.setColor(mArcColor);
        arcPaint.setStrokeCap(Paint.Cap.ROUND);

        //圓弧的外接矩形
        arcRectF = new RectF();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, measureDimensionHeight(heightMeasureSpec));
    }

    private int measureDimensionHeight(int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = mArcWidth;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        canvas.rotate(180, getWidth() / 2, getHeight() / 2);
//        canvas.translate(0, (mArcWidth / 2 - mCircleRadius) / 2);
//        arcRectF.set(getWidth() / 2 - mCircleRadius + mArcWidth / 2, getHeight() / 2 - mCircleRadius + mArcWidth / 2
//                , getWidth() / 2 + mCircleRadius - mArcWidth / 2, getHeight() / 2 + mCircleRadius - mArcWidth / 2);
//        canvas.drawArc(arcRectF, 0, 180, false, arcCirclePaint);
//        arcPaint.setShader(new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{arcStartColor1, arcEndColor1}, new float[]{0, mCurData1 / 200}));
//        canvas.drawArc(arcRectF, 0, 180 * mCurData1 / 100, false, arcPaint);
//        canvas.rotate(90, getWidth() / 2, getHeight() / 2);
//        canvas.drawCircle(getWidth() / 2, getHeight() / 2 - mCircleRadius + mArcWidth / 2, mArcWidth / 2, startCirclePaint);
//
//        int circleRadius2 = mCircleRadius * 8 / 9;
//        canvas.rotate(-90, getWidth() / 2, getHeight() / 2);
//        arcRectF.set(getWidth() / 2 - circleRadius2 + mArcWidth / 2, getHeight() / 2 - circleRadius2 + mArcWidth / 2
//                , getWidth() / 2 + circleRadius2 - mArcWidth / 2, getHeight() / 2 + circleRadius2 - mArcWidth / 2);
//        canvas.drawArc(arcRectF, 0, 180, false, arcCirclePaint);
//        arcPaint.setShader(new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{arcStartColor2, arcEndColor2}, new float[]{0, mCurData2 / 200}));
//        canvas.drawArc(arcRectF, 0, 180 * mCurData2 / 100, false, arcPaint);
//        canvas.rotate(90, getWidth() / 2, getHeight() / 2);
//        startCirclePaint.setColor(arcStartColor2);
//        canvas.drawCircle(getWidth() / 2, getHeight() / 2 - circleRadius2 + mArcWidth / 2, mArcWidth / 2, startCirclePaint);
//
//        int circleRadius3 = mCircleRadius * 7 / 9;
//        canvas.rotate(-90, getWidth() / 2, getHeight() / 2);
//        arcRectF.set(getWidth() / 2 - circleRadius3 + mArcWidth / 2, getHeight() / 2 - circleRadius3 + mArcWidth / 2
//                , getWidth() / 2 + circleRadius3 - mArcWidth / 2, getHeight() / 2 + circleRadius3 - mArcWidth / 2);
//        canvas.drawArc(arcRectF, 0, 180, false, arcCirclePaint);
//        arcPaint.setShader(new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{arcStartColor3, arcEndColor3}, new float[]{0, mCurData3 / 200}));
//        canvas.drawArc(arcRectF, 0, 180 * mCurData3 / 100, false, arcPaint);
//        canvas.rotate(90, getWidth() / 2, getHeight() / 2);
//        startCirclePaint.setColor(arcStartColor3);
//        canvas.drawCircle(getWidth() / 2, getHeight() / 2 - circleRadius3 + mArcWidth / 2, mArcWidth / 2, startCirclePaint);
        arcRectF.set(0, 0, getWidth() * mCurData / 100, mArcWidth);
        arcPaint.setShader(new LinearGradient(0f, 0f, (float) (getWidth() * mCurData / 100), 0f, new int[]{arcStartColor, arcEndColor}, new float[]{0, mCurData / 100}, Shader.TileMode.CLAMP));
        canvas.drawRoundRect(arcRectF, mArcWidth / 2, mArcWidth / 2, arcPaint);
    }

    public void setPercentData(float data) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(mCurData, data);
        valueAnimator.setDuration((long) (Math.abs(mCurData - data) * 30));
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                mCurData = (float) (Math.round(value * 10)) / 10;
                invalidate();
            }
        });
        valueAnimator.start();
    }
}