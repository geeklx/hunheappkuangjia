package com.sdzn.fzx.student.libpublic.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * @author Reisen at 2018-10-25
 */
public class ScrollCallBackView extends ScrollView {
    public ScrollCallBackView(Context context) {
        super(context);
    }

    public ScrollCallBackView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollCallBackView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ScrollCallBackView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mCallBack != null) {
            mCallBack.onScroll(l, t, oldl, oldt);
        }
    }

    public interface OnScrollCallBack {
        void onScroll(int l, int t, int oldl, int oldt);
    }

    private OnScrollCallBack mCallBack;

    public void setScrollCallBack(OnScrollCallBack callBack) {
        mCallBack = callBack;
    }
}
