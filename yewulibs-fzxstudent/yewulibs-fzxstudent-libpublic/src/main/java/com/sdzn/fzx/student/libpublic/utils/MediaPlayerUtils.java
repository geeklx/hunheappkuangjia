package com.sdzn.fzx.student.libpublic.utils;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.text.TextUtils;

import java.io.IOException;

/**
 * MediaPlayerUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class MediaPlayerUtils implements MediaPlayer.OnPreparedListener {

    private MediaPlayer mediaPlayer;

    private static MediaPlayerUtils mInstance;

    public synchronized static MediaPlayerUtils getInstance() {
        if (mInstance == null) {
            mInstance = new MediaPlayerUtils();
        }
        return mInstance;
    }

    private MediaPlayerUtils() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_SYSTEM);
        mediaPlayer.setOnPreparedListener(this);
    }

    public void play(String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        } else {
            try {
                mediaPlayer.stop();
            } catch (Exception e) {
                mediaPlayer.reset();
            }

        }

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }
}
