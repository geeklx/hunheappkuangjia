package com.sdzn.fzx.student.libpublic.event;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/2
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class UpdataPhoto  extends Event {

    String type;

    public UpdataPhoto() {
    }

    public UpdataPhoto(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
