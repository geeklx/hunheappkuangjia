package com.sdzn.fzx.student.libpublic.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;


/**
 * 描述：仿iphone带进度的进度条，线程安全的View，可直接在线程中更新进度
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/15
 */
public class SemicircleProgressBar extends View {
    private Context mContext;

    private int mArcWidth;
    private int mCircleRadius;
    private Paint arcPaint;
    private Paint arcCirclePaint;
    private RectF arcRectF;
    private float max = 100;
    private float mCurData1 = 0;
    private float mCurData2 = 0;
    private float mCurData3 = 0;
    private int arcStartColor1;
    private int arcEndColor1;
    private int arcStartColor2;
    private int arcEndColor2;
    private int arcStartColor3;
    private int arcEndColor3;
    private Paint startCirclePaint;

    public SemicircleProgressBar(Context context) {
        this(context, null);
    }

    public SemicircleProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SemicircleProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SemicircleProgressBar, defStyleAttr, 0);
        mCurData1 = typedArray.getInt(R.styleable.SemicircleProgressBar_arcProgress1, 0);
        mCurData2 = typedArray.getInt(R.styleable.SemicircleProgressBar_arcProgress2, 0);
        mCurData3 = typedArray.getInt(R.styleable.SemicircleProgressBar_arcProgress3, 0);
        mArcWidth = typedArray.getDimensionPixelSize(R.styleable.SemicircleProgressBar_arcWidth, UiUtils.dp2px(context, 20));
        mCircleRadius = typedArray.getDimensionPixelSize(R.styleable.SemicircleProgressBar_circleRadius, UiUtils.dp2px(context, 100));
        arcStartColor1 = 0xFF3D7CD0;
        arcEndColor1 = 0xFF3EC7B2;
        arcStartColor2 = 0xFF16A085;
        arcEndColor2 = 0xFFEBCE42;
        arcStartColor3 = 0xFFF46993;
        arcEndColor3 = 0xFFFEA85A;

        typedArray.recycle();

        initPaint();
    }

    private void initPaint() {

        startCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        startCirclePaint.setStyle(Paint.Style.FILL);
        //startCirclePaint.setStrokeWidth(mArcWidth);

        arcCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arcCirclePaint.setStyle(Paint.Style.STROKE);
        arcCirclePaint.setStrokeWidth(mArcWidth);
        arcCirclePaint.setColor(0xFFEDEDED);
        arcCirclePaint.setStrokeCap(Paint.Cap.ROUND);

        arcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arcPaint.setStyle(Paint.Style.STROKE);
        arcPaint.setStrokeWidth(mArcWidth);
        arcPaint.setStrokeCap(Paint.Cap.ROUND);

        //圓弧的外接矩形
        arcRectF = new RectF();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(measureDimensionWidth(widthMeasureSpec), measureDimensionHeight(heightMeasureSpec));
    }

    private int measureDimensionWidth(int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = mCircleRadius * 2;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private int measureDimensionHeight(int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = mCircleRadius * 2;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize) / 2 + mArcWidth;
            }
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.rotate(180, getWidth() / 2, getHeight() / 2);
        canvas.translate(0, (mArcWidth / 2 - mCircleRadius) / 2);
        arcRectF.set(getWidth() / 2 - mCircleRadius + mArcWidth / 2, getHeight() / 2 - mCircleRadius + mArcWidth / 2
                , getWidth() / 2 + mCircleRadius - mArcWidth / 2, getHeight() / 2 + mCircleRadius - mArcWidth / 2);
        canvas.drawArc(arcRectF, 0, 180, false, arcCirclePaint);
        arcPaint.setShader(new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{arcStartColor1, arcEndColor1}, new float[]{0, mCurData1 / 200}));
        canvas.drawArc(arcRectF, 0, 180 * mCurData1 / 100, false, arcPaint);
        canvas.rotate(90, getWidth() / 2, getHeight() / 2);
        startCirclePaint.setColor(arcStartColor1);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2 - mCircleRadius + mArcWidth / 2, mArcWidth / 2, startCirclePaint);

        int circleRadius2 = mCircleRadius * 8 / 9;
        canvas.rotate(-90, getWidth() / 2, getHeight() / 2);
        arcRectF.set(getWidth() / 2 - circleRadius2 + mArcWidth / 2, getHeight() / 2 - circleRadius2 + mArcWidth / 2
                , getWidth() / 2 + circleRadius2 - mArcWidth / 2, getHeight() / 2 + circleRadius2 - mArcWidth / 2);
        canvas.drawArc(arcRectF, 0, 180, false, arcCirclePaint);
        arcPaint.setShader(new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{arcStartColor2, arcEndColor2}, new float[]{0, mCurData2 / 200}));
        canvas.drawArc(arcRectF, 0, 180 * mCurData2 / 100, false, arcPaint);
        canvas.rotate(90, getWidth() / 2, getHeight() / 2);
        startCirclePaint.setColor(arcStartColor2);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2 - circleRadius2 + mArcWidth / 2, mArcWidth / 2, startCirclePaint);

        int circleRadius3 = mCircleRadius * 7 / 9;
        canvas.rotate(-90, getWidth() / 2, getHeight() / 2);
        arcRectF.set(getWidth() / 2 - circleRadius3 + mArcWidth / 2, getHeight() / 2 - circleRadius3 + mArcWidth / 2
                , getWidth() / 2 + circleRadius3 - mArcWidth / 2, getHeight() / 2 + circleRadius3 - mArcWidth / 2);
        canvas.drawArc(arcRectF, 0, 180, false, arcCirclePaint);
        arcPaint.setShader(new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{arcStartColor3, arcEndColor3}, new float[]{0, mCurData3 / 200}));
        canvas.drawArc(arcRectF, 0, 180 * mCurData3 / 100, false, arcPaint);
        canvas.rotate(90, getWidth() / 2, getHeight() / 2);
        startCirclePaint.setColor(arcStartColor3);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2 - circleRadius3 + mArcWidth / 2, mArcWidth / 2, startCirclePaint);
    }

    public void setMax(float max) {
        this.max = max;
    }

    public void setPercentData(float data1, float data2, float data3) {
        ValueAnimator valueAnimator1 = ValueAnimator.ofFloat(mCurData1, data1);
        valueAnimator1.setDuration((long) (Math.abs(mCurData1 - data1) * 100));
        valueAnimator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                mCurData1 = (float) (Math.round(value * 10f)) / 10f * 100f / max;
                invalidate();
            }
        });
        valueAnimator1.start();

        ValueAnimator valueAnimator2 = ValueAnimator.ofFloat(mCurData2, data2);
        valueAnimator2.setDuration((long) (Math.abs(mCurData2 - data2) * 30));
        valueAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                mCurData2 = (float) (Math.round(value * 10)) / 10 * 100f / max;
                invalidate();
            }
        });
        valueAnimator2.start();

        ValueAnimator valueAnimator3 = ValueAnimator.ofFloat(mCurData3, data3);
        valueAnimator3.setDuration((long) (Math.abs(mCurData3 - data3) * 30));
        valueAnimator3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                mCurData3 = (float) (Math.round(value * 10)) / 10 * 100f / max;
                invalidate();
            }
        });
        valueAnimator3.start();
    }
}