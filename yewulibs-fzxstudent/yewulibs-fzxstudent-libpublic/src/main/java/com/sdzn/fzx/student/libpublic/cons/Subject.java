package com.sdzn.fzx.student.libpublic.cons;

import android.util.SparseArray;

import com.sdzn.fzx.student.libpublic.R;


/**
 * 学科
 *
 * @author wangchunxiao
 * @date 2018/1/9
 */
public enum Subject {
    other("其他", 0, R.mipmap.quanbu_nor, R.mipmap.quanbu_nor, R.mipmap.quanbu_sel),
    yuwen("语文", 1, R.mipmap.yuwen, R.mipmap.yuwen_nor, R.mipmap.yuwen_sel),
    shuxue("数学", 2, R.mipmap.shuxue, R.mipmap.shuxue_nor, R.mipmap.shuxue_sel),
    yingyu("英语", 3, R.mipmap.yingyu, R.mipmap.yingyu_nor, R.mipmap.yingyu_sel),
    wuli("物理", 4, R.mipmap.wuli, R.mipmap.wuli_nor, R.mipmap.wuli_sel),
    huaxue("化学", 5, R.mipmap.huaxue, R.mipmap.huaxue_nor, R.mipmap.huaxue_sel),
    shengwu("生物", 6, R.mipmap.shengwu, R.mipmap.shengwu_nor, R.mipmap.shengwu_sel),
    lishi("历史", 7, R.mipmap.lishi, R.mipmap.lishi_nor, R.mipmap.lishi_sel),
    dili("地理", 8, R.mipmap.dili, R.mipmap.dili_nor, R.mipmap.dili_sel),
    //    sixiangpinde("思想品德", 9, R.mipmap.zhengzhi, R.mipmap.zhengzhi_nor, R.mipmap.zhengzhi_sel),
    zhengzhi("思想政治", 10, R.mipmap.zhengzhi, R.mipmap.zhengzhi_nor, R.mipmap.zhengzhi_sel),
    yinyue("音乐", 11, R.mipmap.yinyue, R.mipmap.music_nor, R.mipmap.music_sel),
    meishu("美术", 12, R.mipmap.meishu, R.mipmap.meishu_nor, R.mipmap.meishu_sel),
    xinxijishu("信息技术", 13, R.mipmap.jisuanji, R.mipmap.jisuanji_nor, R.mipmap.jisuanji_sel),
    tiyuyujiankang("体育与健康", 14, R.mipmap.tiyu, R.mipmap.tiyu_nor, R.mipmap.tiyu_sel),
    kexue("科学", 15, R.mipmap.kexue, R.mipmap.kexue_nor, R.mipmap.kexue_sel),
    tongyongjishu("通用技术", 16, R.mipmap.tongyongjishu, R.mipmap.tongyong_nor, R.mipmap.tongyong_sel),
    daodeyufazhi("道德与法制", 18, R.mipmap.daode, R.mipmap.zhengzhi_nor, R.mipmap.zhengzhi_sel),
    zongheshijian("综合实践", 19, R.mipmap.zonghe_img, R.mipmap.shijian_nor, R.mipmap.shijian_sel),

    //===================== 软云部分参数 =====================
    zhinenglianxi("智能练习", 101, R.mipmap.intelligent_test, R.mipmap.intelligent_test, R.mipmap.intelligent_test),
    zhishidianlianxi("知识点练习", 102, R.mipmap.knowledge_point_test, R.mipmap.knowledge_point_test, R.mipmap.knowledge_point_test),
    tongbulianxi("同步练习", 103, R.mipmap.sync_test, R.mipmap.sync_test, R.mipmap.sync_test),
    tishenglianxi("提升练习", 104, R.mipmap.up_test, R.mipmap.up_test, R.mipmap.up_test);

    private String name;
    private int value;
    private int drawableId;
    private int drawableId_nor;
    private int drawableId_sel;

    Subject(String name, int value, int drawableId, int drawableId_nor, int drawableId_sel) {
        this.name = name;
        this.value = value;
        this.drawableId = drawableId;
        this.drawableId_nor = drawableId_nor;
        this.drawableId_sel = drawableId_sel;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public int getDrawableId_nor() {
        return drawableId_nor;
    }

    public int getDrawableId_sel() {
        return drawableId_sel;
    }

    public static SparseArray<Subject> subjects = new SparseArray<>();

    static {
        subjects.put(0, other);
        subjects.put(1, yuwen);
        subjects.put(2, shuxue);
        subjects.put(3, yingyu);
        subjects.put(4, wuli);
        subjects.put(5, huaxue);
        subjects.put(6, shengwu);
        subjects.put(7, lishi);
        subjects.put(8, dili);
//        subjects.put(9, sixiangpinde);
        subjects.put(10, zhengzhi);
        subjects.put(11, yinyue);
        subjects.put(12, meishu);
        subjects.put(13, xinxijishu);
        subjects.put(14, tiyuyujiankang);
        subjects.put(15, kexue);
        subjects.put(16, tongyongjishu);
        subjects.put(18, daodeyufazhi);
        subjects.put(19, zongheshijian);

        //===================== 软云部分参数 =====================
        subjects.put(101,zhinenglianxi);
        subjects.put(102,zhishidianlianxi);
        subjects.put(103,tongbulianxi);
        subjects.put(104,tishenglianxi);
    }
}
