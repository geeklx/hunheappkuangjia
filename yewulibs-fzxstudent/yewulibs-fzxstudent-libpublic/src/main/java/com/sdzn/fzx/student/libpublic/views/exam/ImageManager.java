package com.sdzn.fzx.student.libpublic.views.exam;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * @author Reisen at 2018-09-04
 */
public class ImageManager {
    private static ImageManager sManager;

    public static ImageManager getInstance() {
        if (sManager == null) {
            synchronized (ImageManager.class) {
                if (sManager == null) {
                    sManager = new ImageManager();
                }
            }
        }
        return sManager;
    }

    private ImageManager() {
        mCache = new LruCache<String, Bitmap>((int) (Runtime.getRuntime().maxMemory() / 8)){
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };
    }

    private LruCache<String, Bitmap> mCache;

    public void addCache(String url, Bitmap bitmap) {
        if (getCache(url) == null) {
            mCache.put(url,bitmap);
        }
    }

    public Bitmap getCache(String url) {
        return mCache.get(url);
    }

    public void remove(String url) {
        mCache.remove(url);
    }

}
