package com.sdzn.fzx.student.libpublic.views.statistic_chat;

import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseStaticChatView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public abstract class BaseStaticChatView extends View {

    GestureDetector mGestureDetector;

    int mItemWidth;

    /**
     * 显示图表的区域，不包括周边文字区域
     */
    RectF chatRectF;

    int mMaxHorizontalScrollDis;

    List<BaseItemInfoVo> mDatas = new ArrayList<>();

    public BaseStaticChatView(Context context) {
        this(context, null);
    }

    public BaseStaticChatView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseStaticChatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public abstract void setShowItem(int item);

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            float x = e.getX() + getScrollX();
            int item = (int) (x / mItemWidth);
            if (item < mDatas.size())
                setShowItem(item);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {

            if (Math.abs(distanceX) < Math.abs(distanceY)) {
                return false;
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);//屏蔽父控件拦截onTouch事件
            }
            int dx = (int) distanceX;
            int x = getScrollX();

            if (x + dx < 0) {
                dx = 0 - x;
            }
            if (mMaxHorizontalScrollDis < 0) {
                dx = 0;
            } else if (x + dx > mMaxHorizontalScrollDis) {
                dx = mMaxHorizontalScrollDis - x;
            }

            scrollBy(dx, 0);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float vX, float vY) {

            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    }

    public static class BaseItemInfoVo {

    }
}
