package com.sdzn.fzx.student.libpublic.baseadapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * 学科内容页面
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList;

    public TaskPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList == null ? 0 : fragmentList.size();
    }
}
