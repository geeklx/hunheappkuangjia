package com.sdzn.fzx.student.libpublic.utils.chatroom;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.vo.chatroombean.SubjectBean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SubjectUtils {

    public static List<SubjectBean> getSubject() {
        try {
            InputStream open = App2.get().getResources().getAssets().open("subject.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<SubjectBean>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }
}
