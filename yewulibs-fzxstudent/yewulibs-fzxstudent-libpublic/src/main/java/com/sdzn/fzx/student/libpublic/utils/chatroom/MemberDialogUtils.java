package com.sdzn.fzx.student.libpublic.utils.chatroom;

import android.app.Dialog;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.sdzn.fzx.student.libpublic.R;


/**
 * Created by admin on 2019/8/16.
 */

public class MemberDialogUtils {
    public Dialog createTipDialog(Context context, String contextTxt) {
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_member_layout, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(context).setView(view).create();


        alertDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView confirm = (TextView) view.findViewById(R.id.sure_btn);
        final TextView content = (TextView) view.findViewById(R.id.tip_content_txt);
        content.setText(contextTxt);
        alertDialog.setCancelable(false);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        return alertDialog;
    }
}
