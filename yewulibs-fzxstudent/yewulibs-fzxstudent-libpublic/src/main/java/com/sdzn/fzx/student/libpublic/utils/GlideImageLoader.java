package com.sdzn.fzx.student.libpublic.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.sdzn.fzx.student.libpublic.R;

import rain.coder.photopicker.loader.ImageLoader;
import rain.coder.photopicker.weidget.GalleryImageView;

/**
 * Describe :GlideImageLoader
 * Email:baossrain99@163.com
 * Created by Rain on 17-5-3.
 */

public class GlideImageLoader implements ImageLoader {

    private final static String TAG = "GlideImageLoader";

    @Override
    public void displayImage(Context context, String path, GalleryImageView galleryImageView, boolean resize) {
        RequestBuilder<Drawable> builder;

        builder = Glide.with(context)
                .load(path);
        RequestOptions options = new RequestOptions();
        if (resize) {
            options = options.centerCrop();
//            builder = builder.centerCrop();
        }
        options = options.error(context.getResources().getDrawable(R.mipmap.error_image)).diskCacheStrategy(DiskCacheStrategy.NONE);
        builder.apply(options).into(galleryImageView);
    }

    @Override
    public void clearMemoryCache() {

    }
}
/*
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *     ┃　　　┃
 *     ┃　　　┃
 *     ┃　　　┗━━━┓
 *     ┃　　　　　　　┣┓
 *     ┃　　　　　　　┏┛
 *     ┗┓┓┏━┳┓┏┛
 *       ┃┫┫　┃┫┫
 *       ┗┻┛　┗┻┛
 *        神兽保佑
 *        代码无BUG!
 */