package com.example.app1grzx.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.example.app1grzx.R;
import com.sdzn.fzx.student.libbase.utils.YanzhengUtil;
import com.sdzn.fzx.student.libpublic.event.UpdataPhoto;
import com.sdzn.fzx.student.presenter.ChangePhonePresenter1;
import com.sdzn.fzx.student.view.ChangePhoneViews;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libpublic.views.ClearableEditText;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;

public class ChangePhoneFragment extends BaseFragment implements ChangePhoneViews, View.OnClickListener {
    ChangePhonePresenter1 mPresenter;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView titleTv;
    private LinearLayout confirmLy;
    private TextView phoneTx;
    private ClearableEditText userNumEdit;
    private TextView codeTx;
    private ClearableEditText userCodeEdit;
    private EditText valicodeEdit;
    private ImageView Effectiveness;
    private TextView countDownBtn;


    private String userPhone;
    private String title;
    private int flag;

    public ChangePhoneFragment() {
        // Required empty public constructor
    }

    public static ChangePhoneFragment newInstance(Bundle bundle) {
        ChangePhoneFragment fragment = new ChangePhoneFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_phone, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        titleTv = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        phoneTx = (TextView) view.findViewById(R.id.phone_tx);
        userNumEdit = (ClearableEditText) view.findViewById(R.id.user_num_edit);
        codeTx = (TextView) view.findViewById(R.id.code_tx);
        userCodeEdit = (ClearableEditText) view.findViewById(R.id.user_code_edit);
        countDownBtn = (TextView) view.findViewById(R.id.count_down_btn);
        valicodeEdit = (EditText) view.findViewById(R.id.valicode_edit);
        Effectiveness = (ImageView) view.findViewById(R.id.iv_effectiveness);
        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        countDownBtn.setOnClickListener(this);
        Effectiveness.setOnClickListener(this);
        initPresenter();
        initData();
        return view;
    }

    private void initData() {
        if (getArguments() != null) {
            title = getArguments().getString("title");
            flag = getArguments().getInt("flag");
            titleTv.setText(title);
        }
        mPresenter.QueryImgToken();
    }


    public void initPresenter() {
        mPresenter = new ChangePhonePresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (i == R.id.confirm_ly) {
            confirm();
        } else if (i == R.id.count_down_btn) {
            getCode();
        }else if(i==R.id.iv_effectiveness){
            mPresenter.QueryImgToken();
        }
    }
    private String editcode;
    private String valicode;

    private void confirm() {
        userPhone = userNumEdit.getText().toString();
        editcode = userCodeEdit.getText().toString().trim();
        valicode = valicodeEdit.getText().toString().trim();
        if (userPhone.equals("")) {
            ToastUtil.showCenterBottomToast("手机号不能为空");
            return;
        }else if (!StringUtils.isMobile(userNumEdit.getText().toString().trim()) && userNumEdit.getText().toString().trim().length() != 11) {
            ToastUtils.showShort("请输入正确的账号或手机号");
            return;
        } else if (editcode.equals("")) {
            ToastUtil.showCenterBottomToast("验证码不能为空");
            return;
        } else if (valicode.equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
            return;
        }
        mPresenter.checkVerityNum(userPhone, userCodeEdit.getText().toString());
    }

    private void getCode() {
        userPhone = userNumEdit.getText().toString();
        valicode = valicodeEdit.getText().toString().trim();
        if (!StringUtils.isMobile(userPhone)) {
            ToastUtils.showShort("请输入正确的手机号格式");
        } else if (valicode.equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
//            mPresenter.sendVerityCode("Bearer "+(String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
//                    String.valueOf(UserController.getLoginBean().getData().getUser().getId()),userPhone);
            mPresenter.sendVerityCode("Bearer "+(String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
                    String.valueOf(UserController.getLoginBean().getData().getUser().getId()),
                    userPhone, valicode, imgToken);

        }
    }


    @Override
    public void verifySuccess(String phone, String code) {
        //刷新  DataSafetyFragment的数据
        EventBus.getDefault().post(new UpdataPhoto("update_safety"));
        getFragmentManager().popBackStack();
    }

    @Override
    public void savePhone() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void OnVerityCodeSuccess(Object code) {
        ToastUtils.showShort(String.valueOf(code));
        YanzhengUtil.startTime(60 * 1000, countDownBtn);//倒计时bufen
    }

    @Override
    public void OnVerityCodeFailed(String msg) {
        valicodeEdit.setText("");
        ToastUtils.showShort("获取验证码失败");
        mPresenter.QueryImgToken();
    }

    private String imgToken;


    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgTokenFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        Bitmap bitimg = BitmapFactory.decodeStream(inputStream);
        Glide.with(getActivity()).load(bitimg).into(Effectiveness);
    }

    @Override
    public void OnImgFailed(String msg) {
        ToastUtils.showShort(msg);
    }


    @Override
    public String getIdentifier() {
        return null;
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestory();
        YanzhengUtil.timer_des();
        super.onDestroy();
    }
}
