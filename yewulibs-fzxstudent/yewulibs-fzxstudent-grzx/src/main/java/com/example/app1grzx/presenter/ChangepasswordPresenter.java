package com.example.app1grzx.presenter;


import com.example.app1grzx.view.ChangepasswordView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ChangepasswordPresenter extends BasePresenter<ChangepasswordView, BaseActivity> {
    public void changePassword(String oldPassword, String newPassword, String confirmPassword) {
        Network.createTokenService(NetWorkService.ChangePasswordService.class)
                .ChangePassword(oldPassword, newPassword, confirmPassword)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        mView.changePassswordSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getStatus().getCode() == 21009) {
                                ToastUtil.showLonglToast("旧密码输入错误，请重新输入");
                            }
                            if (((ApiException) e).getStatus().getCode() == 21014) {
                                ToastUtil.showLonglToast("新密码和原密码相同");
                            }
                        }

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }
}
