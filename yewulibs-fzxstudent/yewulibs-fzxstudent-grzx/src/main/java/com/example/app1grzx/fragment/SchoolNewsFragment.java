package com.example.app1grzx.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app1grzx.R;
import com.example.app1grzx.presenter.SchoolNewsPresenter;
import com.example.app1grzx.view.SchoolNewsView;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.vo.LoginBean;

/**
 * A simple {@link Fragment} subclass.
 */
public class SchoolNewsFragment extends MBaseFragment<SchoolNewsPresenter> implements SchoolNewsView {


   /* @BindView(R.id.school_tx)
    TextView schoolTx;
    @BindView(R.id.school_rl)
    RelativeLayout schoolRl;
    @BindView(R.id.level_tx)
    TextView levelTx;
    @BindView(R.id.class_tx)
    TextView classTx;
    @BindView(R.id.teacher_tx)
    TextView teacherTx;*/

    private RelativeLayout schoolRl;
    private TextView schoolTx;
    private TextView levelTx;
    private TextView classTx;
    private TextView teacherTx;
    private LoginBean loginBean;

    public SchoolNewsFragment() {
        // Required empty public constructor
    }

    public static SchoolNewsFragment newInstance(Bundle bundle) {
        SchoolNewsFragment fragment = new SchoolNewsFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school_news, container, false);
        schoolRl = (RelativeLayout) view.findViewById(R.id.school_rl);
        schoolTx = (TextView) view.findViewById(R.id.school_tx);
        levelTx = (TextView) view.findViewById(R.id.level_tx);
        classTx = (TextView) view.findViewById(R.id.class_tx);
        teacherTx = (TextView) view.findViewById(R.id.teacher_tx);

        initData();
        return view;
    }

    private void initData() {
        loginBean = UserController.getLoginBean();
        if (loginBean == null && loginBean.getData().getUser() == null) {
            return;
        }
        if (!TextUtils.isEmpty(loginBean.getData().getUser().getCustomerSchoolName())) {
            schoolTx.setText(loginBean.getData().getUser().getCustomerSchoolName());
        }
        if (!TextUtils.isEmpty(loginBean.getData().getUser().getBaseLevelName())) {
            levelTx.setText(loginBean.getData().getUser().getBaseLevelName());
        }
        if (!TextUtils.isEmpty(loginBean.getData().getUser().getClassName())) {
            classTx.setText(loginBean.getData().getUser().getClassName());
        }
//        if (!TextUtils.isEmpty(loginBean.getData().getUser().getClassManagerName())) {
//            teacherTx.setText(loginBean.getData().getUser().getClassManagerName());
//        }
    }

    @Override
    public void initPresenter() {
        mPresenter = new SchoolNewsPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }
}
