package com.sdzn.fzx.student.api.network;

import android.text.TextUtils;

import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.app.Config;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.RefreshTokenBean;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;

/**
 * RefreshTokenHandler〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class RefreshTokenHandler {

    public static final RefreshTokenHandler handler = new RefreshTokenHandler();

    public static RefreshTokenHandler getInstance() {
        return handler;
    }

    private RefreshTokenHandler() {

    }

    //刷新token
    public Response redirectNewToken(String token, Request request, Interceptor.Chain chain) throws IOException {

        final String access_token = refreshToken(token);

        if (TextUtils.isEmpty(access_token)) {//获取token 失败
            throw new ApiException(new StatusVo(10000,"刷新token，取不到accessToken，导致的"));


        } else {
            HttpUrl.Builder newUrlBuilder = request.url()
                    .newBuilder()
                    .scheme(request.url().scheme())
                    .host(request.url().host())
                    .removeAllQueryParameters("access_token")
                    .addQueryParameter("access_token", access_token);

            Request newRequest = request.newBuilder()
                    .method(request.method(), request.body())
                    .url(newUrlBuilder.build())
                    .build();
            return chain.proceed(newRequest);
        }


    }

    public String refreshToken(String token) throws IOException {
        synchronized (this) {
            Log.e("refresh:start");

            final LoginBean loginBean = StudentSPUtils.getLoginBean();
            if (loginBean == null) {
                return null;
            }
            //对比是否需要刷新
            if (TextUtils.equals(token, loginBean.getData().getOldAccessToken())) {
                Log.e("refresh:end");
                return loginBean.getData().getAccessToken();
            }


            final String refreToken = loginBean.getData().getRefreshToken();
            final String accountId = loginBean.getData().getUser().getAccountId() + "";
            final String userTypeId = "4";//user类型 id 学生端为4
            final String deviceId = AndroidUtil.getDeviceID(App2.get());
            String deviceToken = (String) StudentSPUtils.get(App2.get(), StudentSPUtils.SAVE_UMENG_DEVICE_TOKEN, "");
            if (TextUtils.isEmpty(deviceToken)) {
                deviceToken = Config.DEVICES_DEFULT_TOKEN;
            }
            Log.e("刷新token接口---token:" + loginBean.getData().getAccessToken() + "，refreToken:" + loginBean.getData().getRefreshToken() + "，请求接口:");

            //请求刷新token 接口获取新token
            Call<RefreshTokenBean> objectObservable = Network.createService(NetWorkService.RefreshToken.class)
                    .refresh(refreToken, accountId, userTypeId, deviceId, deviceToken);
            RefreshTokenBean refreshTokenBean = objectObservable.execute().body();

            if (refreshTokenBean == null || refreshTokenBean.getCode() != 0) {
                Log.e("刷新token失败:---token：" + loginBean.getData().getAccessToken() + "，refreToken:" + loginBean.getData().getRefreshToken() + "，请求接口:");
                throw new ApiException(new StatusVo(10000,"刷新token 返回值错误"));
            }


            loginBean.getData().setOldAccessToken(loginBean.getData().getAccessToken());
            loginBean.getData().setRefreshToken(refreshTokenBean.getResult().getData().getRefresh_token());
            loginBean.getData().setAccessToken(refreshTokenBean.getResult().getData().getAccess_token());
            StudentSPUtils.saveLoginBean(loginBean);
            final String access_token = refreshTokenBean.getResult().getData().getAccess_token();

            Log.e("刷新token成功:---token：" + loginBean.getData().getAccessToken() + "，refreToken:" + loginBean.getData().getRefreshToken() + "，请求接口:");
            Log.e("refresh:end");
            return access_token;
        }
    }
}
