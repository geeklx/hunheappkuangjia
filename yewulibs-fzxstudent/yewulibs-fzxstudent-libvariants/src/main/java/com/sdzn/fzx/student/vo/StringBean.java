package com.sdzn.fzx.student.vo;


/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/13.
 */
public class StringBean {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
