package com.sdzn.fzx.student.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.bean.SubjectBean;
import com.sdzn.fzx.student.view.SubjtctViews;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @Title: 学科列表
 * @author: houjie
 * @date： 2021-02-05 9:04
 */
public class SubjtctPresenter extends Presenter<SubjtctViews> {

    public void getSubjtct() {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .getSubject(BuildConfig2.SERVER_ISERVICE_NEW2 + "/knowledgecenter/baseSubject/listAll", "2")
                .enqueue(new Callback<ResponseSlbBean1<List<SubjectBean>>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<List<SubjectBean>>> call, Response<ResponseSlbBean1<List<SubjectBean>>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onSubjtctNodata(response.body().getMessage());
                            return;
                        }
                        getView().onSubjtctSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<List<SubjectBean>>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onSubjtctFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
