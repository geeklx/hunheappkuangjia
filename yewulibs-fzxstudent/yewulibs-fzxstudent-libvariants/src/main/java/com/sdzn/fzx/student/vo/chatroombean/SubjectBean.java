package com.sdzn.fzx.student.vo.chatroombean;

public class SubjectBean {


    /**
     * id : 语文
     * color : #FF587E
     * desc : 语
     */

    private String id;
    private String color;
    private String desc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
