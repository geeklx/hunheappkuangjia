package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/14.
 */
public class BookVersionBean {
    private Book bookVersion;
    private List<Course> courseMapping;

    public Book getBookVersion() {
        return bookVersion;
    }

    public void setBookVersion(Book bookVersion) {
        this.bookVersion = bookVersion;
    }

    public List<Course> getCourseMapping() {
        return courseMapping;
    }

    public void setCourseMapping(List<Course> courseMapping) {
        this.courseMapping = courseMapping;
    }

    public static class Book {
        private int seq;
        private int id;
        private int local_version_id;
        private int partner_id;
        private int sub_id;
        private int version_id;
        private String version_name;

        private boolean selected;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLocal_version_id() {
            return local_version_id;
        }

        public void setLocal_version_id(int local_version_id) {
            this.local_version_id = local_version_id;
        }

        public int getPartner_id() {
            return partner_id;
        }

        public void setPartner_id(int partner_id) {
            this.partner_id = partner_id;
        }

        public int getSub_id() {
            return sub_id;
        }

        public void setSub_id(int sub_id) {
            this.sub_id = sub_id;
        }

        public int getVersion_id() {
            return version_id;
        }

        public void setVersion_id(int version_id) {
            this.version_id = version_id;
        }

        public String getVersion_name() {
            return version_name;
        }

        public void setVersion_name(String version_name) {
            this.version_name = version_name;
        }
    }

    public static class Course {
        private long bookId;
        private int editionID;
        private String editionName;
        private String name;
        private int oid;
        private int termID;
        private int typeID;

        private boolean selected;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public long getBookId() {
            return bookId;
        }

        public void setBookId(long bookId) {
            this.bookId = bookId;
        }

        public int getEditionID() {
            return editionID;
        }

        public void setEditionID(int editionID) {
            this.editionID = editionID;
        }

        public String getEditionName() {
            return editionName;
        }

        public void setEditionName(String editionName) {
            this.editionName = editionName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getOid() {
            return oid;
        }

        public void setOid(int oid) {
            this.oid = oid;
        }

        public int getTermID() {
            return termID;
        }

        public void setTermID(int termID) {
            this.termID = termID;
        }

        public int getTypeID() {
            return typeID;
        }

        public void setTypeID(int typeID) {
            this.typeID = typeID;
        }
    }
}
