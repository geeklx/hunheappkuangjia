package com.sdzn.fzx.student.vo.chatroombean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2019/8/16.
 */

public class GroupListBean {

    public List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * id : 25
         * classId : 596
         * groupId : 1
         * lessonId : null
         * state : -1
         * score : 0
         * groupChatTaskId : 17
         * roomId : 17
         * createTime : 2019-08-14T09:46:51.000+0000
         * endTime : 2019-08-14T09:46:50.000+0000
         * chatTitle : 小组讨论标题
         * chatContent : 小组讨论内容
         * teachGroupChatResultPics : [{"id":2,"picUrl":"http://pic25.nipic.com/20121112/9252150_150552938000_2.jpg","groupChatResultId":25,"userStudentId":null,"createTime":null,"groupId":null,"groupName":null,"groupChatTaskId":null,"userStudentName":null}]
         * isRead
         */

        private String id;
        private int isRead;
        private int classId;
        private String groupId;
        private int lessonId;
        private int state;
        private String socre;
        private String groupChatTaskId;
        private String roomId;
        private String createTime;
        private String chapterName;
        private String endTime;
        private String chatTitle;
        private String chatContent;
        private String subjectId;
        private String subjectName;
//        private List<TeachGroupChatResultPicsBean> teachGroupChatResultPics;
        private List<TeachGroupChatResultPicsBean> groupChatContentPics;

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }



        public String getGroupChatTaskId() {
            return groupChatTaskId;
        }

        public void setGroupChatTaskId(String groupChatTaskId) {
            this.groupChatTaskId = groupChatTaskId;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getScore() {
            return socre;
        }

        public void setScore(String score) {
            this.socre = score;
        }

        public String getRoomId() {
            return roomId;
        }

        public void setRoomId(String roomId) {
            this.roomId = roomId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getChatTitle() {
            return chatTitle;
        }

        public void setChatTitle(String chatTitle) {
            this.chatTitle = chatTitle;
        }

        public String getChatContent() {
            return chatContent;
        }

        public void setChatContent(String chatContent) {
            this.chatContent = chatContent;
        }

        public List<TeachGroupChatResultPicsBean> getTeachGroupChatResultPics() {
            return groupChatContentPics;
        }

        public void setTeachGroupChatResultPics(List<TeachGroupChatResultPicsBean> teachGroupChatResultPics) {
            this.groupChatContentPics = teachGroupChatResultPics;
        }



        public static class TeachGroupChatResultPicsBean implements Parcelable {
            /**
             * id : 2
             * picUrl : http://pic25.nipic.com/20121112/9252150_150552938000_2.jpg
             * groupChatResultId : 25
             * userStudentId : null
             * createTime : null
             * groupId : null
             * groupName : null
             * groupChatTaskId : null
             * userStudentName : null
             */


            private int id;
            private String picUrl;
            private int groupChatResultId;
            private String userStudentId;
            private String createTime;
            private String groupId;
            private String groupName;
            private String groupChatTaskId;
            private String userStudentName;



            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public int getGroupChatResultId() {
                return groupChatResultId;
            }

            public void setGroupChatResultId(int groupChatResultId) {
                this.groupChatResultId = groupChatResultId;
            }

            public Object getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(String userStudentId) {
                this.userStudentId = userStudentId;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getGroupId() {
                return groupId;
            }

            public void setGroupId(String groupId) {
                this.groupId = groupId;
            }

            public String getGroupName() {
                return groupName;
            }

            public void setGroupName(String groupName) {
                this.groupName = groupName;
            }

            public String getGroupChatTaskId() {
                return groupChatTaskId;
            }

            public void setGroupChatTaskId(String groupChatTaskId) {
                this.groupChatTaskId = groupChatTaskId;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.id);
                dest.writeString(this.picUrl);
                dest.writeInt(this.groupChatResultId);
                dest.writeString(this.userStudentId);
                dest.writeString(this.createTime);
                dest.writeString(this.groupId);
                dest.writeString(this.groupName);
                dest.writeString(this.groupChatTaskId);
                dest.writeString(this.userStudentName);
            }

            public TeachGroupChatResultPicsBean() {
            }

            protected TeachGroupChatResultPicsBean(Parcel in) {
                this.id = in.readInt();
                this.picUrl = in.readString();
                this.groupChatResultId = in.readInt();
                this.userStudentId = in.readString();
                this.createTime = in.readString();
                this.groupId = in.readString();
                this.groupName = in.readString();
                this.groupChatTaskId = in.readString();
                this.userStudentName = in.readString();
            }

            public static final Creator<TeachGroupChatResultPicsBean> CREATOR = new Creator<TeachGroupChatResultPicsBean>() {
                @Override
                public TeachGroupChatResultPicsBean createFromParcel(Parcel source) {
                    return new TeachGroupChatResultPicsBean(source);
                }

                @Override
                public TeachGroupChatResultPicsBean[] newArray(int size) {
                    return new TeachGroupChatResultPicsBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeInt(this.isRead);
            dest.writeInt(this.classId);
            dest.writeString(this.groupId);
            dest.writeInt(this.lessonId);
            dest.writeInt(this.state);
            dest.writeString(this.socre);
            dest.writeString(this.groupChatTaskId);
            dest.writeString(this.roomId);
            dest.writeString(this.createTime);
            dest.writeString(this.endTime);
            dest.writeString(this.chatTitle);
            dest.writeString(this.chatContent);
            dest.writeString(this.subjectId);
            dest.writeList(this.groupChatContentPics);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.id = in.readString();
            this.isRead = in.readInt();
            this.classId = in.readInt();
            this.groupId = in.readString();
            this.lessonId = in.readInt();
            this.state = in.readInt();
            this.socre = in.readString();
            this.groupChatTaskId = in.readString();
            this.roomId = in.readString();
            this.createTime = in.readString();
            this.endTime = in.readString();
            this.chatTitle = in.readString();
            this.chatContent = in.readString();
            this.subjectId = in.readString();
            this.groupChatContentPics = new ArrayList<TeachGroupChatResultPicsBean>();
            in.readList(this.groupChatContentPics, TeachGroupChatResultPicsBean.class.getClassLoader());
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
