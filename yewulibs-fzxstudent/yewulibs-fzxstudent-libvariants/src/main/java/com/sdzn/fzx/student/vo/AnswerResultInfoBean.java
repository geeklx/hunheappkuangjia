package com.sdzn.fzx.student.vo;

import java.util.ArrayList;

/**
 * @author Reisen at 2018-12-11
 */
public class AnswerResultInfoBean {
    private AnswerResultDataBean data;

    public AnswerResultDataBean getData() {
        return data;
    }

    public void setData(AnswerResultDataBean data) {
        this.data = data;
    }

    public static class AnswerResultDataBean {
        private int answerViewOpen;//试题作答列表
        private StudentTaskVo studentTaskVo;//时间 分数等信息
        private ArrayList<AnswerListBean.AnswerDataBean> examAnswerList;

        public int getAnswerViewOpen() {
            return answerViewOpen;
        }

        public void setAnswerViewOpen(int answerViewOpen) {
            this.answerViewOpen = answerViewOpen;
        }

        public StudentTaskVo getStudentTaskVo() {
            return studentTaskVo;
        }

        public void setStudentTaskVo(StudentTaskVo studentTaskVo) {
            this.studentTaskVo = studentTaskVo;
        }

        public ArrayList<AnswerListBean.AnswerDataBean> getExamAnswerList() {
            return examAnswerList;
        }

        public void setExamAnswerList(ArrayList<AnswerListBean.AnswerDataBean> examAnswerList) {
            this.examAnswerList = examAnswerList;
        }
    }
}
