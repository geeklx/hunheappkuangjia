package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/3/8 0008.
 */

public class UploadVo {
    /**
     * data : http://192.168.0.215:8886/exam/2018/03/573509d5de0041af8cba117b051b6603.jpg
     */

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
