package com.sdzn.fzx.student.dao.controller;

import com.sdzn.fzx.student.dao.WhiteBoardHistory;
import com.sdzn.fzx.student.dao.WhiteBoardHistoryDao;
import com.sdzn.fzx.student.utils.CommonDaoUtils;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/14
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class WhiteBoardHistoryController {

    public static WhiteBoardHistory queryByExamAndPage(Integer exam, Integer page, Integer examId) {
        return CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().queryBuilder().where(WhiteBoardHistoryDao.Properties.Exam.eq(exam), WhiteBoardHistoryDao.Properties.Page.eq(page), WhiteBoardHistoryDao.Properties.ExamId.eq(examId)).unique();
    }


    public static List<WhiteBoardHistory> queryAllExam(String exam, String examId) {
        return CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().queryBuilder().where(WhiteBoardHistoryDao.Properties.Exam.eq(exam), WhiteBoardHistoryDao.Properties.ExamId.eq(examId)).list();
    }

    public static Long insertWhiteBoardHistory(WhiteBoardHistory whiteBoardHistory) {
        return CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().insert(whiteBoardHistory);
    }

    public static void updateWhiteBoardHistory(WhiteBoardHistory whiteBoardHistory) {
        CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().update(whiteBoardHistory);
    }

    public static void deleteAllHistory(String exam, String examId) {
        final List<WhiteBoardHistory> whiteBoardHistories = queryAllExam(exam, examId);
        if (whiteBoardHistories != null) {
            CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().deleteInTx(whiteBoardHistories);
        }
    }

    public static void insertAllHistory(Iterable<WhiteBoardHistory> histories) {
        CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().insertInTx(histories);
    }


    public static void deleteWhiteBoardHistory(WhiteBoardHistory whiteBoardHistory) {
        CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().deleteByKey(whiteBoardHistory.getId());
    }

    public static WhiteBoardHistory queryByExamAndUrl(String url) {
        return CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().queryBuilder().where(WhiteBoardHistoryDao.Properties.Url.eq(url)).unique();
    }

    public static List<WhiteBoardHistory> queryByExamList(String exam, String examId) {
        return CommonDaoUtils.getInstance().getDaoSession().getWhiteBoardHistoryDao().queryBuilder().where(WhiteBoardHistoryDao.Properties.Exam.eq(exam), WhiteBoardHistoryDao.Properties.ExamId.eq(examId)).list();
    }
}
