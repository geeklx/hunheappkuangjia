package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * RightRateBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class RightRateBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * highRate : 0
         * classRate : 0
         * myRate : 0
         * day : 2017-09-30
         */

        private float highRate;
        private float classRate;
        private float myRate;
        private long day;

        public float getHighRate() {
            return highRate / 100;
        }

        public void setHighRate(float highRate) {
            this.highRate = highRate;
        }

        public float getClassRate() {
            return classRate / 100;
        }

        public void setClassRate(float classRate) {
            this.classRate = classRate;
        }

        public float getMyRate() {
            return myRate / 100;
        }

        public void setMyRate(float myRate) {
            this.myRate = myRate;
        }

        public long getDay() {
            return day;
        }

        public void setDay(long day) {
            this.day = day;
        }
    }
}
