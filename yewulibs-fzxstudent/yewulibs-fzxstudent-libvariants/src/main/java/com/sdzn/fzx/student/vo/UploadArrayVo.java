package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class UploadArrayVo {
    /**
     * data : http://192.168.0.215:8886/exam/2018/03/573509d5de0041af8cba117b051b6603.jpg
     */

    private List<String> data;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
