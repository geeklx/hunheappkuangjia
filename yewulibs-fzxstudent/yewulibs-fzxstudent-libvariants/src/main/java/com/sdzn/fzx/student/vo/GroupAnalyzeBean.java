package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * GroupAnalyzeBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class GroupAnalyzeBean {

    /**
     * id : 1
     * classId : 1
     * className : 一班
     * classGroupId : 1
     * classGroupName : 创新组
     * createAccountId : 1
     * createTime : 1515999265000
     * createUserName : 1
     * points : 200
     * type : 1
     * subjectId : 1
     * subjectName : 语文
     * pointsCounts : 4
     * totalPoints : 200
     * totalCount : 4
     * totalSort : 1
     * taskPoints : 100
     * taskCount : 2
     * taskSort : 1
     * classPoints : 0
     * classCount : 0
     * classSort : -1
     */

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String id;
        private String classId;
        private String className;
        private String classGroupId;
        private String classGroupName;
        private String createAccountId;
        private long createTime;
        private String createUserName;
        private int points;
        private int type;
        private int subjectId;
        private String subjectName;
        private int pointsCounts;
        private int totalPoints;
        private int totalCount;
        private int totalSort;
        private int taskPoints;
        private int taskCount;
        private int taskSort;
        private int classPoints;
        private int classCount;
        private int classSort;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public String getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(String createAccountId) {
            this.createAccountId = createAccountId;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public int getPointsCounts() {
            return pointsCounts;
        }

        public void setPointsCounts(int pointsCounts) {
            this.pointsCounts = pointsCounts;
        }

        public int getTotalPoints() {
            return totalPoints;
        }

        public void setTotalPoints(int totalPoints) {
            this.totalPoints = totalPoints;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getTotalSort() {
            return totalSort;
        }

        public void setTotalSort(int totalSort) {
            this.totalSort = totalSort;
        }

        public int getTaskPoints() {
            return taskPoints;
        }

        public void setTaskPoints(int taskPoints) {
            this.taskPoints = taskPoints;
        }

        public int getTaskCount() {
            return taskCount;
        }

        public void setTaskCount(int taskCount) {
            this.taskCount = taskCount;
        }

        public int getTaskSort() {
            return taskSort;
        }

        public void setTaskSort(int taskSort) {
            this.taskSort = taskSort;
        }

        public int getClassPoints() {
            return classPoints;
        }

        public void setClassPoints(int classPoints) {
            this.classPoints = classPoints;
        }

        public int getClassCount() {
            return classCount;
        }

        public void setClassCount(int classCount) {
            this.classCount = classCount;
        }

        public int getClassSort() {
            return classSort;
        }

        public void setClassSort(int classSort) {
            this.classSort = classSort;
        }
    }

    public static class MyGroupInfo {
        private int totalRank;
        private int taskRank;
        private int classRank;

        public MyGroupInfo() {
        }

        public MyGroupInfo(int totalRank, int taskRank, int classRank) {
            this.totalRank = totalRank;
            this.taskRank = taskRank;
            this.classRank = classRank;
        }

        public int getTotalRank() {
            return totalRank;
        }

        public void setTotalRank(int totalRank) {
            this.totalRank = totalRank;
        }

        public int getTaskRank() {
            return taskRank;
        }

        public void setTaskRank(int taskRank) {
            this.taskRank = taskRank;
        }

        public int getClassRank() {
            return classRank;
        }

        public void setClassRank(int classRank) {
            this.classRank = classRank;
        }
    }
}
