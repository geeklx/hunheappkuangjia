package com.sdzn.fzx.student.presenter;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.view.ShouyeViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class ShouyePresenter extends Presenter<ShouyeViews> {
    public void queryShouye() {
        Log.e("queryShouye", "queryShouye: " + SPUtils.getInstance().getString("token", ""));
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryShouye(BuildConfig2.SERVER_ISERVICE_NEW2 + "/course/home/student/data", "Bearer " + (String) SPUtils.getInstance().getString("token", ""))
                .enqueue(new Callback<ResponseSlbBean1<ShouyeRecActBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ShouyeRecActBean>> call, Response<ResponseSlbBean1<ShouyeRecActBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            getView().onCehuaNodata("");
                            return;
                        }
                        if (response.body().getCode() == 2000) {
                            ToastUtils.showShort("用户登录信息已失效，请重新登录");
                            isToken();
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCehuaFail(response.body().getMessage());
                            return;
                        }
                        getView().onCehuaSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ShouyeRecActBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCehuaFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    public void isToken() {
        if (!TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.sdzn.fzx.student.libbase.login.activity.LoginActivity")) {
            ActivityUtils.finishAllActivities();
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.loginactivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            App2.get().startActivity(intent);
        }
    }
}
