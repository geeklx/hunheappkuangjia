package com.sdzn.fzx.student.view;


import com.haier.cellarette.libmvp.mvp.IView;

import java.io.InputStream;

/**
 *
 */

public interface ChangePhoneViews extends IView {

    void verifySuccess(final String phone, final String code);

    void savePhone();

    void onFailed(String msg);

    void OnVerityCodeSuccess(Object code);

    void OnVerityCodeFailed(String msg);

    void OnImgTokenSuccess(Object imgtoken);

    void OnImgTokenFailed(String msg);

    void OnImgSuccess(InputStream img);

    void OnImgFailed(String msg);
}
