package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * 任务列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskFileVo {

    /**
     * total : 2
     * data : [{"classId":1,"className":"一班","lessonId":2,"lessonName":"课程","baseSubjectId":1,"baseSubjectName":"语文","chapterId":2,"chapterName":"我们的民族小学2","chapterNodeIdPath":"0.1.2.1","chapterNodeNamePath":"第一组>我们的民族小学","updateTime":"2018-01-29 17:31:09.0","count":1},{"classId":1,"className":"一班","lessonId":1,"lessonName":"课程","baseSubjectId":1,"baseSubjectName":"语文","chapterId":2,"chapterName":"我们的民族小学","chapterNodeIdPath":"0.1.2","chapterNodeNamePath":"第一组>我们的民族小学","updateTime":"2018-01-29 16:45:03.0","count":3}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * classId : 1
         * className : 一班
         * lessonId : 2
         * lessonName : 课程
         * baseSubjectId : 1
         * baseSubjectName : 语文
         * chapterId : 2
         * chapterName : 我们的民族小学2
         * chapterNodeIdPath : 0.1.2.1
         * chapterNodeNamePath : 第一组>我们的民族小学
         * updateTime : 2018-01-29 17:31:09.0
         * count : 1
         */

        private int classId;
        private String className;
        private int lessonId;
        private String lessonName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private long updateTime;
        private int count;

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
