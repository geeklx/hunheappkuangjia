package com.sdzn.fzx.student.vo;

public class ChatInfoBean {
    private String id;
    private String groupChatTaskId;
    private String groupId;
    private String groupName;
    private String roomId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupChatTaskId() {
        return groupChatTaskId;
    }

    public void setGroupChatTaskId(String groupChatTaskId) {
        this.groupChatTaskId = groupChatTaskId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}
