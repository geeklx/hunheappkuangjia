package com.sdzn.fzx.student.vo;

/**
 * RefreshTokenBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class RefreshTokenBean {

    /**
     * code : 0
     * msg : success
     * timestamp : 1495676918774
     * result : {"data":{"access_token":"d75bcfc49c2f4f93b773b307601aeb73","refresh_token":"63588c7c58a24946b15c05e0853b8d24","expires_in":7200}}
     */

    private int code;
    private String msg;
    private String timestamp;
    /**
     * data : {"access_token":"d75bcfc49c2f4f93b773b307601aeb73","refresh_token":"63588c7c58a24946b15c05e0853b8d24","expires_in":7200}
     */

    private ResultBean result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * access_token : d75bcfc49c2f4f93b773b307601aeb73
         * refresh_token : 63588c7c58a24946b15c05e0853b8d24
         * expires_in : 7200
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            private String access_token;
            private String refresh_token;
            private int expires_in;

            public String getAccess_token() {
                return access_token;
            }

            public void setAccess_token(String access_token) {
                this.access_token = access_token;
            }

            public String getRefresh_token() {
                return refresh_token;
            }

            public void setRefresh_token(String refresh_token) {
                this.refresh_token = refresh_token;
            }

            public int getExpires_in() {
                return expires_in;
            }

            public void setExpires_in(int expires_in) {
                this.expires_in = expires_in;
            }

            @Override
            public String toString() {
                return "DataBean{" +
                        "access_token='" + access_token + '\'' +
                        ", refresh_token='" + refresh_token + '\'' +
                        ", expires_in=" + expires_in +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "ResultBean{" +
                    "data=" + data +
                    '}';
        }
    }
}
