package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * @author Reisen at 2018-10-08
 */
public class ExamSubmitBean {
    private int type;
    private long templateId;
    private int score;
    private long lessonTaskDetailsId;
    private long lessonAnswerExamParentId;
    private long lessonAnswerExamId;
    private int emptyCount;
    private int isRead;
    private long useTime;
    private List<ExamSubmitListBean> answerList;

    /**
     * 文字bean
     */
    public static ExamSubmitBean initTextBean() {
        ExamSubmitBean bean = new ExamSubmitBean();
        bean.type = 3;
        return bean;
    }

    /**
     * 资源bean
     * @param isRead 是否查看
     * @param useTime 打开时间
     */
    public static ExamSubmitBean initResBean(long lessonTaskDetailsId, long lessonAnswerExamParentId,
                                             long lessonAnswerExamId, int isRead, long useTime) {
        ExamSubmitBean bean = new ExamSubmitBean();
        bean.type = 2;
        bean.score = -1;
        bean.lessonTaskDetailsId = lessonTaskDetailsId;
        bean.lessonAnswerExamParentId = lessonAnswerExamParentId;
        bean.lessonAnswerExamId = lessonAnswerExamId;
        bean.isRead = isRead;
        bean.useTime = useTime;
        return bean;
    }

    /**
     * 试题bean
     */
    public static ExamSubmitBean init(int type, long templateId, float score, long lessonTaskDetailsId,
                                      long lessonAnswerExamParentId, long lessonAnswerExamId, int emptyCount) {
        ExamSubmitBean bean = new ExamSubmitBean();
        bean.type = type;
        bean.templateId = templateId;
        bean.score = (int) score;
        bean.lessonTaskDetailsId = lessonTaskDetailsId;
        bean.lessonAnswerExamParentId = lessonAnswerExamParentId;
        bean.lessonAnswerExamId = lessonAnswerExamId;
        bean.emptyCount = emptyCount;
        return bean;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(long templateId) {
        this.templateId = templateId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getLessonTaskDetailsId() {
        return lessonTaskDetailsId;
    }

    public void setLessonTaskDetailsId(long lessonTaskDetailsId) {
        this.lessonTaskDetailsId = lessonTaskDetailsId;
    }

    public long getLessonAnswerExamParentId() {
        return lessonAnswerExamParentId;
    }

    public void setLessonAnswerExamParentId(long lessonAnswerExamParentId) {
        this.lessonAnswerExamParentId = lessonAnswerExamParentId;
    }

    public long getLessonAnswerExamId() {
        return lessonAnswerExamId;
    }

    public void setLessonAnswerExamId(long lessonAnswerExamId) {
        this.lessonAnswerExamId = lessonAnswerExamId;
    }

    public int getEmptyCount() {
        return emptyCount;
    }

    public void setEmptyCount(int emptyCount) {
        this.emptyCount = emptyCount;
    }

    public List<ExamSubmitListBean> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<ExamSubmitListBean> answerList) {
        this.answerList = answerList;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public long getUseTime() {
        return useTime;
    }

    public void setUseTime(long useTime) {
        this.useTime = useTime;
    }

    public static class ExamSubmitListBean {
        private boolean isAnswer;
        private String myAnswer;
        private String myAnswerHtml;
        private String rightAnswer;
        private int seq;
        private int answerType;//填空题作答方式

        public static ExamSubmitListBean create(boolean isAnswer, String myAnswer, String myAnswerHtml, String rightAnswer, int seq) {
            return create(isAnswer, myAnswer, myAnswerHtml, rightAnswer, seq, 0);
        }

        public static ExamSubmitListBean create(boolean isAnswer, String myAnswer, String myAnswerHtml, String rightAnswer, int seq, int answerType) {
            ExamSubmitListBean bean = new ExamSubmitListBean();
            bean.isAnswer = isAnswer;
            bean.myAnswer = myAnswer;
            bean.myAnswerHtml = myAnswerHtml;
            bean.rightAnswer = rightAnswer;
            bean.seq = seq;
            bean.answerType = answerType;
            return bean;
        }

        public boolean isAnswer() {
            return isAnswer;
        }

        public void setAnswer(boolean answer) {
            isAnswer = answer;
        }

        public String getMyAnswer() {
            return myAnswer;
        }

        public void setMyAnswer(String myAnswer) {
            this.myAnswer = myAnswer;
        }

        public String getMyAnswerHtml() {
            return myAnswerHtml;
        }

        public void setMyAnswerHtml(String myAnswerHtml) {
            this.myAnswerHtml = myAnswerHtml;
        }

        public String getRightAnswer() {
            return rightAnswer;
        }

        public void setRightAnswer(String rightAnswer) {
            this.rightAnswer = rightAnswer;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getAnswerType() {
            return answerType;
        }

        public void setAnswerType(int answerType) {
            this.answerType = answerType;
        }
    }
}
