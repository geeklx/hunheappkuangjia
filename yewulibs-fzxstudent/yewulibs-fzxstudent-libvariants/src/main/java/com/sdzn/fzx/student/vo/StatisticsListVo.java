package com.sdzn.fzx.student.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangc on 2018/3/20 0020.
 */

public class StatisticsListVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * score : 1
         * examTemplateId : 1
         * isRight : 1
         * parentSeq : 1
         * examTemplateStyleId : 168
         * lessonTaskDetailsId : 656
         * scoreTotal : 1
         * examTemplateStyleName : 任务型阅读
         * id : 734
         * seq : 1
         * scoreAvg : 0.2
         */

        private float score;
        private int examTemplateId;
        private int isRight;
        private int parentSeq;
        private int examTemplateStyleId;
        private int lessonTaskDetailsId;
        private float scoreTotal;
        private String examTemplateStyleName;
        private int id;
        private int seq;
        private float scoreAvg;
        private ArrayList<ClozeOption> clozeOption;

        public ArrayList<ClozeOption> getClozeOption() {
            return clozeOption;
        }

        public void setClozeOption(ArrayList<ClozeOption> clozeOption) {
            this.clozeOption = clozeOption;
        }

        public float getScore() {
            return score;
        }

        public void setScore(float score) {
            this.score = score;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public int getParentSeq() {
            return parentSeq;
        }

        public void setParentSeq(int parentSeq) {
            this.parentSeq = parentSeq;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public int getLessonTaskDetailsId() {
            return lessonTaskDetailsId;
        }

        public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
            this.lessonTaskDetailsId = lessonTaskDetailsId;
        }

        public float getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(float scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public String getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public float getScoreAvg() {
            return scoreAvg;
        }

        public void setScoreAvg(float scoreAvg) {
            this.scoreAvg = scoreAvg;
        }
    }

    public static class ClozeOption {
        private float avgScore;
        private float fullScore;
        private float score;
        private long id;
        private int right;
        private int seq;

        public float getAvgScore() {
            return avgScore;
        }

        public void setAvgScore(float avgScore) {
            this.avgScore = avgScore;
        }

        public float getFullScore() {
            return fullScore;
        }

        public void setFullScore(float fullScore) {
            this.fullScore = fullScore;
        }

        public float getScore() {
            return score;
        }

        public void setScore(float score) {
            this.score = score;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public int getRight() {
            return right;
        }

        public void setRight(int right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }
    }
}
