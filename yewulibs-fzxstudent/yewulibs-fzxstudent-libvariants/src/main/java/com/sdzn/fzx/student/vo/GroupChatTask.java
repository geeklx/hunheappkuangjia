package com.sdzn.fzx.student.vo;

import java.util.List;

public class GroupChatTask {


    /**
     * id : 613
     * classId : 88
     * groupId : 43
     * lessonLibId : 603
     * state : 1
     * score : 0
     * groupChatTaskId : 271
     * roomId : 250522057
     * createTime : 2020-11-11 18:01:39
     * endTime : 2020-11-28 00:00:00
     * chatTitle : 11111111111
     * chatContent : 11111111111
     * groupChatLibId : 0
     * isRead : 0
     * groupChatResultPics : []
     * groupChatContentPics : [{"id":176,"picUrl":"http://file.fuzhuxian.com/1605088894618timg.jpg","groupChatLibId":0,"groupChatTaskId":271,"createTime":"2020-11-11 18:01:38"}]
     */

    private int id;
    private int classId;
    private int groupId;
    private int lessonLibId;
    private int state;
    private int score;
    private int groupChatTaskId;
    private int roomId;
    private String createTime;
    private String endTime;
    private String chatTitle;
    private String chatContent;
    private int groupChatLibId;
    private int isRead;
    private List<?> groupChatResultPics;
    private List<GroupChatContentPicsBean> groupChatContentPics;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getLessonLibId() {
        return lessonLibId;
    }

    public void setLessonLibId(int lessonLibId) {
        this.lessonLibId = lessonLibId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getGroupChatTaskId() {
        return groupChatTaskId;
    }

    public void setGroupChatTaskId(int groupChatTaskId) {
        this.groupChatTaskId = groupChatTaskId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChatTitle() {
        return chatTitle;
    }

    public void setChatTitle(String chatTitle) {
        this.chatTitle = chatTitle;
    }

    public String getChatContent() {
        return chatContent;
    }

    public void setChatContent(String chatContent) {
        this.chatContent = chatContent;
    }

    public int getGroupChatLibId() {
        return groupChatLibId;
    }

    public void setGroupChatLibId(int groupChatLibId) {
        this.groupChatLibId = groupChatLibId;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public List<?> getGroupChatResultPics() {
        return groupChatResultPics;
    }

    public void setGroupChatResultPics(List<?> groupChatResultPics) {
        this.groupChatResultPics = groupChatResultPics;
    }

    public List<GroupChatContentPicsBean> getGroupChatContentPics() {
        return groupChatContentPics;
    }

    public void setGroupChatContentPics(List<GroupChatContentPicsBean> groupChatContentPics) {
        this.groupChatContentPics = groupChatContentPics;
    }

    public static class GroupChatContentPicsBean {
        /**
         * id : 176
         * picUrl : http://file.fuzhuxian.com/1605088894618timg.jpg
         * groupChatLibId : 0
         * groupChatTaskId : 271
         * createTime : 2020-11-11 18:01:38
         */

        private int id;
        private String picUrl;
        private int groupChatLibId;
        private int groupChatTaskId;
        private String createTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public int getGroupChatLibId() {
            return groupChatLibId;
        }

        public void setGroupChatLibId(int groupChatLibId) {
            this.groupChatLibId = groupChatLibId;
        }

        public int getGroupChatTaskId() {
            return groupChatTaskId;
        }

        public void setGroupChatTaskId(int groupChatTaskId) {
            this.groupChatTaskId = groupChatTaskId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
