package com.sdzn.fzx.student.vo;

/**
 * @author Created by Reisen on 2019/3/1.
 */
public class QiNiuConfigBean {

    public ConfigBean data;

    public ConfigBean getData() {
        return data;
    }

    public void setData(ConfigBean data) {
        this.data = data;
    }

    public static class ConfigBean {
        public String domian;
        public String imageStyle;

        public String getDomian() {
            return domian;
        }

        public void setDomian(String domian) {
            this.domian = domian;
        }

        public String getImageStyle() {
            return imageStyle;
        }

        public void setImageStyle(String imageStyle) {
            this.imageStyle = imageStyle;
        }
    }
}
