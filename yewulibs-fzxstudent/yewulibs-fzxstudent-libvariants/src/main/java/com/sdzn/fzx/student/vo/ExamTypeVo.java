package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * ExamTypeVo〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ExamTypeVo {


    /**
     * total : 57
     * data : [{"id":175,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"短文选词填空","updateUserId":2,"updateUserName":"付燕","updateTime":1520510605000},{"id":174,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"多句选词填空","updateUserId":2,"updateUserName":"付燕","updateTime":1520510598000},{"id":173,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"补全对话（有选项）","updateUserId":2,"updateUserName":"付燕","updateTime":1520510591000},{"id":172,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"信息匹配","updateUserId":2,"updateUserName":"付燕","updateTime":1520510583000},{"id":171,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"听力匹配","updateUserId":2,"updateUserName":"付燕","updateTime":1520510574000},{"id":170,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"听力排序","updateUserId":2,"updateUserName":"付燕","updateTime":1520510564000},{"id":169,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":14,"templateName":"完形填空","templateStyleName":"完形填空","updateUserId":2,"updateUserName":"付燕","updateTime":1520510551000},{"id":168,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":16,"templateName":"综合题","templateStyleName":"任务型阅读","updateUserId":3,"updateUserName":"张梦节","updateTime":1520786590000},{"id":167,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":16,"templateName":"综合题","templateStyleName":"阅读回答问题","updateUserId":3,"updateUserName":"张梦节","updateTime":1520786594000},{"id":166,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":16,"templateName":"综合题","templateStyleName":"阅读理解","updateUserId":3,"updateUserName":"张梦节","updateTime":1520786604000},{"id":165,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":11,"templateName":"阅读填空","templateStyleName":"阅读填表","updateUserId":2,"updateUserName":"付燕","updateTime":1520510506000},{"id":164,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":15,"templateName":"信息还原","templateStyleName":"七选五阅读填空","updateUserId":3,"updateUserName":"张梦节","updateTime":1520774578000},{"id":163,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":10,"templateName":"听力综合","templateStyleName":"听录音补全句子/对话/短文（综合）","updateUserId":2,"updateUserName":"付燕","updateTime":1520510485000},{"id":162,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":10,"templateName":"听力综合","templateStyleName":"听录音完成表格（选择）","updateUserId":2,"updateUserName":"付燕","updateTime":1520510477000},{"id":161,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":10,"templateName":"听力综合","templateStyleName":"听短文及问题选答案","updateUserId":2,"updateUserName":"付燕","updateTime":1520510468000},{"id":160,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":10,"templateName":"听力综合","templateStyleName":"听短文选答案","updateUserId":2,"updateUserName":"付燕","updateTime":1520510459000},{"id":159,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":10,"templateName":"听力综合","templateStyleName":"听长对话选答案","updateUserId":2,"updateUserName":"付燕","updateTime":1520510449000},{"id":158,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":9,"templateName":"听力判断","templateStyleName":"听力判断","updateUserId":2,"updateUserName":"付燕","updateTime":1520510433000},{"id":157,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":8,"templateName":"听力填空","templateStyleName":"听录音补全句子/对话/短文（单题填空）","updateUserId":2,"updateUserName":"付燕","updateTime":1520510414000},{"id":156,"baseLevelId":2,"baseLevelName":"初中","baseSubjectId":3,"baseSubjectName":"英语","templateId":8,"templateName":"听力填空","templateStyleName":"听录音完成表格（填空）","updateUserId":2,"updateUserName":"付燕","updateTime":1520510404000}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 175
         * baseLevelId : 2
         * baseLevelName : 初中
         * baseSubjectId : 3
         * baseSubjectName : 英语
         * templateId : 15
         * templateName : 信息还原
         * templateStyleName : 短文选词填空
         * updateUserId : 2
         * updateUserName : 付燕
         * updateTime : 1520510605000
         */

        private String id;
        private int baseLevelId;
        private String baseLevelName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int templateId;
        private String templateName;
        private String templateStyleName;
        private int updateUserId;
        private String updateUserName;
        private long updateTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getBaseLevelId() {
            return baseLevelId;
        }

        public void setBaseLevelId(int baseLevelId) {
            this.baseLevelId = baseLevelId;
        }

        public String getBaseLevelName() {
            return baseLevelName;
        }

        public void setBaseLevelName(String baseLevelName) {
            this.baseLevelName = baseLevelName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getTemplateId() {
            return templateId;
        }

        public void setTemplateId(int templateId) {
            this.templateId = templateId;
        }

        public String getTemplateName() {
            return templateName;
        }

        public void setTemplateName(String templateName) {
            this.templateName = templateName;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getUpdateUserId() {
            return updateUserId;
        }

        public void setUpdateUserId(int updateUserId) {
            this.updateUserId = updateUserId;
        }

        public String getUpdateUserName() {
            return updateUserName;
        }

        public void setUpdateUserName(String updateUserName) {
            this.updateUserName = updateUserName;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }
    }
}
