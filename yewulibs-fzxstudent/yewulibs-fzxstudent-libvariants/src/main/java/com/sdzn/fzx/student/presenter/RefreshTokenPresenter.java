package com.sdzn.fzx.student.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.bean.RefreshTokenRecActBean;
import com.sdzn.fzx.student.view.RefreshTokenViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class RefreshTokenPresenter extends Presenter<RefreshTokenViews> {

    public void queryRefreshToken(String refreshToken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryRefreshToken(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/login/refresh/token", refreshToken)
                .enqueue(new Callback<ResponseSlbBean1<RefreshTokenRecActBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<RefreshTokenRecActBean>> call, Response<ResponseSlbBean1<RefreshTokenRecActBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            getView().onRefreshTokenNodata("");
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onRefreshTokenNodata(response.body().getMessage());
                            return;
                        }
                        getView().onRefreshTokenSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<RefreshTokenRecActBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onRefreshTokenFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
