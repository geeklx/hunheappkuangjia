package com.sdzn.fzx.student.vo;


import com.sdzn.fzx.student.libutils.annotations.CheckState;
import com.sdzn.fzx.student.libutils.annotations.InputMethod;

/**
 * 填空 完型 的空位bean
 *
 * @author Reisen at 2018-12-26
 */
public class ClozeAnswerBean {
    private int index;
    private String value;//序号
    private String answer;//作答内容
    private boolean selected;//是否被选中 作答结果中true表示作答正确
    @InputMethod
    private int inputMethod;//作答内容是否为图片
    @CheckState
    private int checkState;//批改状态

    public ClozeAnswerBean(int index, String value) {
        this(index, value, "");
    }

    public ClozeAnswerBean(int index, String value, String answer) {
        this(index, value, answer, false, InputMethod.KeyBoard);
    }

    public ClozeAnswerBean(int index, String value, String answer, boolean selected, @InputMethod int inputMethod) {
        this(index, value, answer, selected, inputMethod, CheckState.UN_CHECK);
    }

    public ClozeAnswerBean(int index, String value, String answer, boolean selected, @InputMethod int inputMethod, @CheckState int checkState) {
        this.index = index;
        this.value = value;
        this.answer = answer;
        this.selected = selected;
        this.inputMethod = inputMethod;
        this.checkState = checkState;
    }

    @InputMethod
    public int getInputMethod() {
        return inputMethod;
    }

    public void setInputMethod(@InputMethod int inputMethod) {
        this.inputMethod = inputMethod;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @CheckState
    public int getCheckState() {
        return checkState;
    }

    public void setCheckState(@CheckState int checkState) {
        this.checkState = checkState;
    }

    public void setData(ClozeAnswerBean bean) {
        this.index = bean.index;
        if (this.value == null || this.value.isEmpty()) {
            this.value = bean.value;
        }
        this.answer = bean.answer;
        this.selected = bean.selected;
        this.inputMethod = bean.inputMethod;
        this.checkState = bean.checkState;
    }
}
