package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * 正确率
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class RightRateVo {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * scoreTotal : 0.833333
         * submit_time : 1515661262000
         */

        private double scoreTotal;
        private long submitTime;

        public double getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(double scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public long getSubmitTime() {
            return submitTime;
        }

        public void setSubmitTime(long submitTime) {
            this.submitTime = submitTime;
        }
    }
}
