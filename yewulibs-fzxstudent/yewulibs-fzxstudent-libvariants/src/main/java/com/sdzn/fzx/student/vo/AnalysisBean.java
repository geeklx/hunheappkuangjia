package com.sdzn.fzx.student.vo;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * 统计分析bean
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/11.
 */
public class AnalysisBean {

    private List<Ability> ability;
    private float avgScore;
    private float avgUseTime;
    private float maxScore;
    private long maxUseTime;
    private List<Method> method;
    private float myScore;
    private long myUseTime;
    private List<Point> point;
    private String quesTotal;//总题数
    private double errorRate;//正确率
    private String errorTotal;//错题数
    private double rightRate;

    public double getRightRate() {
        return rightRate;
    }

    public void setRightRate(double rightRate) {
        this.rightRate = rightRate;
    }

    public String getQuesTotal() {
        return quesTotal;
    }

    public void setQuesTotal(String quesTotal) {
        this.quesTotal = quesTotal;
    }

    public double getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(double errorRate) {
        this.errorRate = errorRate;
    }

    public String getErrorTotal() {
        return errorTotal;
    }

    public void setErrorTotal(String errorTotal) {
        this.errorTotal = errorTotal;
    }

    public void setAbility(List<Ability> ability) {
        this.ability = ability;
    }

    public List<Ability> getAbility() {
        return ability;
    }

    public void setAvgScore(float avgScore) {
        this.avgScore = avgScore;
    }

    public float getAvgScore() {
        return avgScore;
    }

    public void setAvgUseTime(float avgUseTime) {
        this.avgUseTime = avgUseTime;
    }

    public float getAvgUseTime() {
        return avgUseTime;
    }

    public void setMaxScore(float maxScore) {
        this.maxScore = maxScore;
    }

    public float getMaxScore() {
        return maxScore;
    }

    public void setMaxUseTime(long maxUseTime) {
        this.maxUseTime = maxUseTime;
    }

    public long getMaxUseTime() {
        return maxUseTime;
    }

    public void setMethod(List<Method> method) {
        this.method = method;
    }

    public List<Method> getMethod() {
        return method;
    }

    public void setMyScore(float myScore) {
        this.myScore = myScore;
    }

    public float getMyScore() {
        return myScore;
    }

    public void setMyUseTime(long myUseTime) {
        this.myUseTime = myUseTime;
    }

    public long getMyUseTime() {
        return myUseTime;
    }

    public void setPoint(List<Point> point) {
        this.point = point;
    }

    public List<Point> getPoint() {
        return point;
    }

    /**
     * 能力
     */
    public static class Ability {
        private double abilityAccuracy;
        private long abilityId;
        private String abilityName;

        public void setAbilityAccuracy(double abilityAccuracy) {
            this.abilityAccuracy = abilityAccuracy;
        }

        public double getAbilityAccuracy() {
            return abilityAccuracy;
        }

        public void setAbilityId(long abilityId) {
            this.abilityId = abilityId;
        }

        public long getAbilityId() {
            return abilityId;
        }

        public void setAbilityName(String abilityName) {
            this.abilityName = abilityName;
        }

        public String getAbilityName() {
            return abilityName;
        }
    }

    /**
     * 方法
     */
    public static class Method {
        private double methodAccuracy;
        private long methodId;
        private String methodName;

        public void setMethodAccuracy(double methodAccuracy) {
            this.methodAccuracy = methodAccuracy;
        }

        public double getMethodAccuracy() {
            return methodAccuracy;
        }

        public void setMethodId(long methodId) {
            this.methodId = methodId;
        }

        public long getMethodId() {
            return methodId;
        }

        public void setMethodName(String methodName) {
            this.methodName = methodName;
        }

        public String getMethodName() {
            return methodName;
        }
    }

    /**
     * 知识点
     */
    public static class Point implements Comparable<Point> {
        private List<Point> children;
        private double pointAccuracy;
        private long pointId;
        private String pointName;
        private long pointParentId;
        private String pointParentName;
        private int pointQuestionCount;
        private double pointScore;
        private int pointWrongCount;


        private boolean last;

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public List<Point> getChildren() {
            return children;
        }

        public void setChildren(List<Point> children) {
            this.children = children;
        }

        public void setPointAccuracy(double pointAccuracy) {
            this.pointAccuracy = pointAccuracy;
        }

        public double getPointAccuracy() {
            return pointAccuracy;
        }

        public void setPointId(long pointId) {
            this.pointId = pointId;
        }

        public long getPointId() {
            return pointId;
        }

        public void setPointName(String pointName) {
            this.pointName = pointName;
        }

        public String getPointName() {
            return pointName;
        }

        public void setPointParentId(long pointParentId) {
            this.pointParentId = pointParentId;
        }

        public long getPointParentId() {
            return pointParentId;
        }

        public void setPointParentName(String pointParentName) {
            this.pointParentName = pointParentName;
        }

        public String getPointParentName() {
            return pointParentName;
        }

        public void setPointQuestionCount(int pointQuestionCount) {
            this.pointQuestionCount = pointQuestionCount;
        }

        public int getPointQuestionCount() {
            return pointQuestionCount;
        }

        public void setPointScore(double pointScore) {
            this.pointScore = pointScore;
        }

        public double getPointScore() {
            return pointScore;
        }

        public void setPointWrongCount(int pointWrongCount) {
            this.pointWrongCount = pointWrongCount;
        }

        public int getPointWrongCount() {
            return pointWrongCount;
        }

        @Override
        public int compareTo(@NonNull Point point) {
            return Long.compare(pointParentId, point.pointParentId);
        }
    }

    public static class NameAccuracy {
        private double accuracy;
        private long id;
        private String name;

        public NameAccuracy(Ability ability) {
            accuracy = ability.abilityAccuracy;
            id = ability.abilityId;
            name = ability.abilityName;
        }

        public NameAccuracy(Method method) {
            accuracy = method.methodAccuracy;
            id = method.methodId;
            name = method.methodName;
        }

        public NameAccuracy(String name) {
            this.id = -1;
            this.name = name;
        }

        public double getAccuracy() {
            return accuracy;
        }

        public void setAccuracy(double accuracy) {
            this.accuracy = accuracy;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
