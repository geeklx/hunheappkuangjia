package com.sdzn.fzx.student.presenter;


import com.blankj.utilcode.util.ToastUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.view.ChangePhoneViews;

import java.io.InputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class ChangePhonePresenter1 extends Presenter<ChangePhoneViews> {


    /*获取图形验证码Token*/
    public void QueryImgToken() {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImgToken(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/valicode/imgToken")
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnImgTokenFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnImgTokenSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnImgTokenFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    /*获取图形验证码图片*/
    public void QueryImg(String imgToken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImg(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/valicode/img", imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        InputStream io = response.body().byteStream();
                        getView().OnImgSuccess(io);
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnImgFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    private String tokenStr, idStr;

    public void sendVerityCode(String token, String id, String tel,String valicode,String imgToken) {
        tokenStr = token;
        idStr = id;
        RetrofitNetNew.build(Api.class, getIdentifier())
                .sendYZM1(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/login/sendVerifyCode", tel,valicode,imgToken)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnVerityCodeFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnVerityCodeSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnVerityCodeFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    public void checkVerityNum(final String phoneNum, final String code) {
        savePhone(phoneNum, code);
    }

    public void savePhone(final String phoneNum, final String code) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .updatePhone(BuildConfig2.SERVER_ISERVICE_NEW2 + "/usercenter/student/update/mobile", tokenStr, phoneNum, code, idStr)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        getView().verifySuccess(phoneNum, code);

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onFailed("绑定手机号码失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
//        Network.createTokenService(NetWorkService.SavePhoneService.class)
//                .SavePhone(phoneNum)
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
//                    @Override
//                    public void onNext(Object o) {
//                        ToastUtil.showShortlToast("成功绑定手机号");
//                        mView.savePhone();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            if (((ApiException) e).getCode() == 21206) {
//                                ToastUtil.showShortlToast("该手机号已被绑定！");
//                            }
//                        }
//
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity));

    }
}
