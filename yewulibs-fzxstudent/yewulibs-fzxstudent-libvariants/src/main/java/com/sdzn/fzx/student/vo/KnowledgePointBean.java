package com.sdzn.fzx.student.vo;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * 知识点练习bean
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/13.
 */
public class KnowledgePointBean implements Comparable<KnowledgePointBean> {
    private List<KnowledgePointBean> children;
    private int node_id;

    private String node_name;
    private int order_index;
    private int parent_node_id;
    private int subject_id;
    private double score;

    private int id;
    private String no;
    private int parentId;
    private double quesCount;
    private String name;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<KnowledgePointBean> getChildren() {
        return children;
    }

    public void setChildren(List<KnowledgePointBean> children) {
        this.children = children;
    }

    public int getNode_id() {
        return node_id;
    }

    public void setNode_id(int node_id) {
        this.node_id = node_id;
    }

    public String getNode_name() {
        return node_name;
    }

    public void setNode_name(String node_name) {
        this.node_name = node_name;
    }

    public int getOrder_index() {
        return order_index;
    }

    public void setOrder_index(int order_index) {
        this.order_index = order_index;
    }

    public int getParent_node_id() {
        return parent_node_id;
    }

    public void setParent_node_id(int parent_node_id) {
        this.parent_node_id = parent_node_id;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public int compareTo(@NonNull KnowledgePointBean o) {
        return Long.compare(node_id, o.node_id);
    }
}
