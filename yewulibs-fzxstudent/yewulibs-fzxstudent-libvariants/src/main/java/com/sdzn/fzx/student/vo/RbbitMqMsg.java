package com.sdzn.fzx.student.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/9/19
 * 修改单号：
 * 修改内容:
 */
public class RbbitMqMsg {
    /**
     * name : 解锁
     * id : 4
     */

    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
