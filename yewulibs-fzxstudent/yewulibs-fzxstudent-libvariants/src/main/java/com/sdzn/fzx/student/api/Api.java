package com.sdzn.fzx.student.api;

import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.sdzn.fzx.student.bean.GrzxRecActBean;
import com.sdzn.fzx.student.bean.RecyclerTabBean;
import com.sdzn.fzx.student.bean.RefreshTokenRecActBean;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;
import com.sdzn.fzx.student.bean.SubjectBean;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.UserBeanVo;
import com.sdzn.fzx.student.vo.VerifyUserMobile;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {

    // 课堂学习学科
    @GET
    Call<ResponseSlbBean1<List<SubjectBean>>> getSubject(@Url String path, @Query("levelId") String levelId);

    // 检查更新
    @FormUrlEncoded
    @POST
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Url String path, @Field("programId") String programId, @Field("type") String type);

    // 侧滑pop
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST(ApiInterface.QUERY_CEHUA_INFO)
    Call<ResponseSlbBean1<GrzxRecActBean>> queryCehua(@Url String path);

    // 动态获取tab数据
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST(ApiInterface.QUERY_TAB_INFO)
    Call<ResponseSlbBean1<RecyclerTabBean>> queryTab(@Url String path, @Body RequestBody body);

    // 首页数据
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST(ApiInterface.QUERY_SHOUYE_INFO)
    Call<ResponseSlbBean1<ShouyeRecActBean>> queryShouye(@Url String path, @Header("Authorization") String token);

    // 个人中心获取资料
//    @POST(ApiInterface.STUDENT_INFO)
    @POST
    Call<ResponseSlbBean1<UserBeanVo>> studentInfo(@Url String path, @Header("Authorization") String token);

    // 登录
//    @FormUrlEncoded
//    @POST
////    @POST(ApiInterface.LOGIN_INFO)
//    Call<ResponseSlbBean1<LoginBean.DataBean>> login(@Url String path, @Field("username") String username, @Field("password") String password);
//
    // 登录
    @FormUrlEncoded
    @POST
    Call<ResponseSlbBean1<LoginBean.DataBean>> login(@Url String path, @Field("username") String username, @Field("password") String password, @Field("deviceId") String deviceId, @Field("type") String type);

    // 发送验证码
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.SEND_VERIFYCODE)
    Call<ResponseSlbBean1<Object>> sendYZM(@Url String path, @Field("telephone") String telephone);

    // new 发送验证码
    @FormUrlEncoded
    @POST
    Call<ResponseSlbBean1<Object>> sendYZM1(@Url String path, @Field("telephone") String telephone, @Field("imgCode") String imgCode, @Field("imgToken") String imgToken);


    // 验证验证码
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.CHECK_VERIFYCODE)
    Call<ResponseSlbBean1<Object>> checkYZM(@Url String path, @Field("mobile") String mobile, @Field("code") String code);

    // 学生更新手机号
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.STUDENT_MOBILE_UPDATE)
    Call<ResponseSlbBean1<Object>> updatePhone(@Url String path, @Header("Authorization") String token, @Field("newTel") String mobile, @Field("code") String code, @Field("userId") String userId);

    // 学生更新密码
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.STUDENT_PASSWORD_UPDATE)
    Call<ResponseSlbBean1<Object>> updatePassword(@Url String path, @Header("Authorization") String token, @Field("id") String id,
                                                  @Field("oldPassword") String oldPassword, @Field("password") String password);

    // 忘记密码--获取手机号
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST
//    @POST(ApiInterface.STUDENT_LOGIN_MOBILE)
    Call<ResponseSlbBean1<VerifyUserMobile>> studentFindMobile(@Url String path, @Body RequestBody body);

    // 忘记密码--更新密码
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.STUDENT_LOGIN_PASSWORD_UPDATE)
    Call<ResponseSlbBean1<Object>> updateForgetPas(@Url String path, @Field("mobile") String mobile, @Field("password") String password, @Field("code") String code);

    //刷新token
    @FormUrlEncoded
    @POST
//    @POST(ApiInterface.QUERY_TOKEN)
    Call<ResponseSlbBean1<RefreshTokenRecActBean>> queryRefreshToken(@Url String path, @Field("refreshToken") String refreshToken);

    // 获取图形token
    @POST
//    @POST("/student/login/sendVerifyCode")
    Call<ResponseSlbBean1<Object>> QueryImgToken(@Url String path);

    // 获取Img图片
    @FormUrlEncoded
    @POST
//    @POST("/student/login/sendVerifyCode")
    Call<ResponseBody> QueryImg(@Url String path, @Field("imgToken") String imgToken);

}
