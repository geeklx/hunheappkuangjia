package com.sdzn.fzx.student.vo;

import androidx.annotation.NonNull;

import java.util.List;

public class ExamText {

    /**
     * baseEducationId : 1
     * baseEducationName : 六三制
     * baseGradeId : 7
     * baseGradeName : 七年级
     * baseLevelId : 2
     * baseLevelName : 初中
     * baseSubjectId : 1
     * baseSubjectName : 语文
     * baseVersionId : 1
     * baseVersionName : 人教版（部编版）
     * baseVolumeId : 1
     * baseVolumeName : 上册
     * chapterId : 2932
     * chapterName : 3* 雨的四季/刘湛秋
     * chapterNodeIdPath : 0.2888.2895.2932
     * chapterNodeNamePath : 第一单元>阅读>3* 雨的四季/刘湛秋
     * createTime : 1543195280404
     * createUserId : 233
     * createUserName : 王银勇
     * customerSchoolId : 60
     * customerSchoolName : 时代智囊测试
     * difficulty : 2
     * downCount : 5
     * examAnalysis : 解答这道题要熟知常见的修辞手法有：比喻、比拟、夸张、排比、对偶、反问、设问、互文等。其中比喻包括暗喻、明喻、借喻，比拟包括拟人和拟物。学生要明确其各自特点，<br />答案：<br />（1）比喻（2）比喻（3）排比（4）拟人
     * examAnswer : 比喻 比喻 排比 拟人
     * examOptions : [{"content":"比喻","right":true,"seq":1},{"content":"比喻","right":true,"seq":2},{"content":"排比","right":true,"seq":3},{"content":"拟人","right":true,"seq":4}]
     * examStem : 指出下列句子所运用的修辞手法。<br />（1）半空中似乎总挂着透明的水雾的丝帘，牵动着阳光的彩棱镜。<input index=1 class='cus-com' readonly='readonly' type='text' value=(1)><br />（2）而那萌发的叶子，简直就像起伏着一层绿茵茵的波浪。<input index=2 class='cus-com' readonly='readonly' type='text' value=(2)><br />（3）那是雨，是使人静谧、使人怀想、使人动情的秋雨啊！<input index=3 class='cus-com' readonly='readonly' type='text' value=(3)><br />（4）但这时候，雨已经化了妆，它经常变成美丽的雪花，飘然莅临人间。<input index=4 class='cus-com' readonly='readonly' type='text' value=(4)>。
     * examTypeId : 6
     * favTime : 0
     * flagShare : 0
     * holdUserId : 233
     * holdUserName : 王银勇
     * hots : 0
     * id : 5bfb4a901cb0766fcc843bdf
     * label :
     * optionNumber : 4
     * paperCount : 126
     * partnerExamId : fffbdc05-e80d-435f-8eaa-e6e7f9f8ff3b
     * points : [{"eName":"chinese","id":3664,"levelId":2,"name":"修辞手法及运用","no":"61","nodeIdPath":"0.3627.3663.3664","parentId":"3663","subjectId":1}]
     * score : 3
     * sourceId : 3
     * sourceName : 菁优网
     * templateStyleId : 63
     * templateStyleName : 填空题
     * type : 1
     * updateTime : 1543195280404
     * viewCount : 100
     * zoneIdPath : 0.15.223.5025
     * zoneName : 高新区
     */

    private int baseEducationId;
    private String baseEducationName;
    private int baseGradeId;
    private String baseGradeName;
    private int baseLevelId;
    private String baseLevelName;
    private int baseSubjectId;
    private String baseSubjectName;
    private int baseVersionId;
    private String baseVersionName;
    private int baseVolumeId;
    private String baseVolumeName;
    private int chapterId;
    private String chapterName;
    private String chapterNodeIdPath;
    private String chapterNodeNamePath;
    private long createTime;
    private int createUserId;
    private String createUserName;
    private int customerSchoolId;
    private String customerSchoolName;
    private int difficulty;
    private int downCount;
    private String examAnalysis;
    private String examAnswer;
    private String examStem;
    private int examTypeId;
    private int favTime;
    private int flagShare;
    private int holdUserId;
    private String holdUserName;
    private int hots;
    private String id;
    private String label;
    private int optionNumber;
    private int paperCount;
    private String partnerExamId;
    private int score;
    private int sourceId;
    private String sourceName;
    private int templateStyleId;
    private String templateStyleName;
    private int type;
    private long updateTime;
    private int viewCount;
    private String zoneIdPath;
    private String zoneName;
    private List<ExamOptionsBean> examOptions;
    private List<PointsBean> points;

    public int getBaseEducationId() {
        return baseEducationId;
    }

    public void setBaseEducationId(int baseEducationId) {
        this.baseEducationId = baseEducationId;
    }

    public String getBaseEducationName() {
        return baseEducationName;
    }

    public void setBaseEducationName(String baseEducationName) {
        this.baseEducationName = baseEducationName;
    }

    public int getBaseGradeId() {
        return baseGradeId;
    }

    public void setBaseGradeId(int baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseGradeName() {
        return baseGradeName;
    }

    public void setBaseGradeName(String baseGradeName) {
        this.baseGradeName = baseGradeName;
    }

    public int getBaseLevelId() {
        return baseLevelId;
    }

    public void setBaseLevelId(int baseLevelId) {
        this.baseLevelId = baseLevelId;
    }

    public String getBaseLevelName() {
        return baseLevelName;
    }

    public void setBaseLevelName(String baseLevelName) {
        this.baseLevelName = baseLevelName;
    }

    public int getBaseSubjectId() {
        return baseSubjectId;
    }

    public void setBaseSubjectId(int baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public String getBaseSubjectName() {
        return baseSubjectName;
    }

    public void setBaseSubjectName(String baseSubjectName) {
        this.baseSubjectName = baseSubjectName;
    }

    public int getBaseVersionId() {
        return baseVersionId;
    }

    public void setBaseVersionId(int baseVersionId) {
        this.baseVersionId = baseVersionId;
    }

    public String getBaseVersionName() {
        return baseVersionName;
    }

    public void setBaseVersionName(String baseVersionName) {
        this.baseVersionName = baseVersionName;
    }

    public int getBaseVolumeId() {
        return baseVolumeId;
    }

    public void setBaseVolumeId(int baseVolumeId) {
        this.baseVolumeId = baseVolumeId;
    }

    public String getBaseVolumeName() {
        return baseVolumeName;
    }

    public void setBaseVolumeName(String baseVolumeName) {
        this.baseVolumeName = baseVolumeName;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterNodeIdPath() {
        return chapterNodeIdPath;
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getChapterNodeNamePath() {
        return chapterNodeNamePath;
    }

    public void setChapterNodeNamePath(String chapterNodeNamePath) {
        this.chapterNodeNamePath = chapterNodeNamePath;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public int getCustomerSchoolId() {
        return customerSchoolId;
    }

    public void setCustomerSchoolId(int customerSchoolId) {
        this.customerSchoolId = customerSchoolId;
    }

    public String getCustomerSchoolName() {
        return customerSchoolName;
    }

    public void setCustomerSchoolName(String customerSchoolName) {
        this.customerSchoolName = customerSchoolName;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getDownCount() {
        return downCount;
    }

    public void setDownCount(int downCount) {
        this.downCount = downCount;
    }

    public String getExamAnalysis() {
        return examAnalysis;
    }

    public void setExamAnalysis(String examAnalysis) {
        this.examAnalysis = examAnalysis;
    }

    public String getExamAnswer() {
        return examAnswer;
    }

    public void setExamAnswer(String examAnswer) {
        this.examAnswer = examAnswer;
    }

    public String getExamStem() {
        return examStem;
    }

    public void setExamStem(String examStem) {
        this.examStem = examStem;
    }

    public int getExamTypeId() {
        return examTypeId;
    }

    public void setExamTypeId(int examTypeId) {
        this.examTypeId = examTypeId;
    }

    public int getFavTime() {
        return favTime;
    }

    public void setFavTime(int favTime) {
        this.favTime = favTime;
    }

    public int getFlagShare() {
        return flagShare;
    }

    public void setFlagShare(int flagShare) {
        this.flagShare = flagShare;
    }

    public int getHoldUserId() {
        return holdUserId;
    }

    public void setHoldUserId(int holdUserId) {
        this.holdUserId = holdUserId;
    }

    public String getHoldUserName() {
        return holdUserName;
    }

    public void setHoldUserName(String holdUserName) {
        this.holdUserName = holdUserName;
    }

    public int getHots() {
        return hots;
    }

    public void setHots(int hots) {
        this.hots = hots;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }

    public int getPaperCount() {
        return paperCount;
    }

    public void setPaperCount(int paperCount) {
        this.paperCount = paperCount;
    }

    public String getPartnerExamId() {
        return partnerExamId;
    }

    public void setPartnerExamId(String partnerExamId) {
        this.partnerExamId = partnerExamId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public int getTemplateStyleId() {
        return templateStyleId;
    }

    public void setTemplateStyleId(int templateStyleId) {
        this.templateStyleId = templateStyleId;
    }

    public String getTemplateStyleName() {
        return templateStyleName;
    }

    public void setTemplateStyleName(String templateStyleName) {
        this.templateStyleName = templateStyleName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getZoneIdPath() {
        return zoneIdPath;
    }

    public void setZoneIdPath(String zoneIdPath) {
        this.zoneIdPath = zoneIdPath;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public List<ExamOptionsBean> getExamOptions() {
        return examOptions;
    }

    public void setExamOptions(List<ExamOptionsBean> examOptions) {
        this.examOptions = examOptions;
    }

    public List<PointsBean> getPoints() {
        return points;
    }

    public void setPoints(List<PointsBean> points) {
        this.points = points;
    }

    public static class ExamOptionsBean implements Comparable<ExamOptionsBean>{
        /**
         * content : 比喻
         * right : true
         * seq : 1
         */

        private String content;
        private boolean right;
        private int seq;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamText.ExamOptionsBean o) {
            return Integer.compare(seq,o.seq);
        }
    }

    public static class PointsBean {
        /**
         * eName : chinese
         * id : 3664
         * levelId : 2
         * name : 修辞手法及运用
         * no : 61
         * nodeIdPath : 0.3627.3663.3664
         * parentId : 3663
         * subjectId : 1
         */

        private String eName;
        private int id;
        private int levelId;
        private String name;
        private String no;
        private String nodeIdPath;
        private String parentId;
        private int subjectId;

        public String getEName() {
            return eName;
        }

        public void setEName(String eName) {
            this.eName = eName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevelId() {
            return levelId;
        }

        public void setLevelId(int levelId) {
            this.levelId = levelId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getNodeIdPath() {
            return nodeIdPath;
        }

        public void setNodeIdPath(String nodeIdPath) {
            this.nodeIdPath = nodeIdPath;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }
    }
}
