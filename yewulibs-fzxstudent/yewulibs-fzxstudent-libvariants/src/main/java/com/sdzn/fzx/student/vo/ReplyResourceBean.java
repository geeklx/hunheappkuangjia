package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/3/26 0026.
 */

public class ReplyResourceBean {
    /**
     * lessonAnswerResourceId : 25
     * isRead : 1
     * useTime : 100
     */

    private int lessonAnswerResourceId;
    private int isRead;
    private long useTime;

    public int getLessonAnswerResourceId() {
        return lessonAnswerResourceId;
    }

    public void setLessonAnswerResourceId(int lessonAnswerResourceId) {
        this.lessonAnswerResourceId = lessonAnswerResourceId;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public long getUseTime() {
        return useTime;
    }

    public void setUseTime(long useTime) {
        this.useTime = useTime;
    }
}
