package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * ErrorSubjectListBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ErrorSubjectListBean {

    /**
     * total : 0
     * data : [{"count":0,"baseSubjectId":1,"baseSubjectName":"语文"},{"count":0,"baseSubjectId":2,"baseSubjectName":"数学"},{"count":0,"baseSubjectId":3,"baseSubjectName":"英语"},{"count":0,"baseSubjectId":9,"baseSubjectName":"思想品德"},{"count":0,"baseSubjectId":11,"baseSubjectName":"音乐"},{"count":0,"baseSubjectId":12,"baseSubjectName":"美术"},{"count":0,"baseSubjectId":13,"baseSubjectName":"信息技术"},{"count":0,"baseSubjectId":14,"baseSubjectName":"体育与健康"},{"count":0,"baseSubjectId":15,"baseSubjectName":"科学"}]
     */

    private int total;
    /**
     * count : 0
     * baseSubjectId : 1
     * baseSubjectName : 语文
     */

    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Comparable<DataBean> {
        private int count;
        private int baseSubjectId;
        private String baseSubjectName;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        @Override
        public int compareTo(DataBean dataBean) {
            return dataBean.getCount() - this.getCount();
        }
    }
}
