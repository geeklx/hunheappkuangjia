package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/3/8 0008.
 */

public class ServerTimeVo {
    /**
     * data : 1520488981540
     */

    private long data;

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }
}
