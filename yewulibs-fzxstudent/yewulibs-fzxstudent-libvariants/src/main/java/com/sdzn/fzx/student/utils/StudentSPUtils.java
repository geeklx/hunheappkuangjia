package com.sdzn.fzx.student.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;

import com.blankj.utilcode.util.CloseUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.vo.ChapterBean;
import com.sdzn.fzx.student.vo.LoginBean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.sdzn.fzx.student.libutils.util.MyLogUtil.TAG;


/**
 * SP的工具类
 * Created by Tony on 16/6/6.
 */
public class StudentSPUtils {
    /**
     * 保存在手机里面的文件名
     */
    public static final String FILE_NAME = "share_data";
    public static final String GUIDANCE_TAG = "guidance_tag";

    /**
     * 用于实现账号互顶，用一个单一的uuid，组成唯一mqtt的topic
     * 当有新设备登陆的时候，服务端会发一个mqtt消息到该话题上，
     * 收到消息后可以提示用户有其他的设备上登陆了该账号
     */
    public static final String SINGLE_UUID = "mqtt_uuid";

    public static final String LOGIN_USER_NUM = "login_user_name";

    public static final String AUTO_LOGIN = "autoLogin";

    /**
     * 记住密码
     */
    public static final String LOGIN_USER_PWD = "login_user_pwd";

    public static final String LOGIN_CHECK_STATUS= "login_check_status";

    /**
     * 保存／获取本地  登陆成功返回的信息
     */
    public static final String SAVE_LOGIN_INFO = "login_info";
    public static final String WRITE_IS_OPEN = "write_is_open";

    public static final String SAVE_UMENG_DEVICE_TOKEN = "umeng_device_token";
    /**
     * 上次配对信息
     */
    public static final String SP_LAST_PAIRED = "last_paired_device";
    /**
     * 记录配对信息
     */
    public static final String SP_PAIRED_DEVICE = "sp_paird";
    /**
     * 关键字
     */
    public static final String SP_PAIRED_KEY = "address";

    public static final String SP_ANSWER_TIME = "answerTime";

    public static final String SP_KEY_BOARD_HEIGHT = "spKeyBoardHeight";

    public static final String SP_STUDENT_COURSE = "spStudentCourse";

    public static final String SP_TOKEN= "AccessToken";
    /**
     *科目 学科
     */
    public static final String SP_XUEKE= "sp_xueke";


    /**
     * 存储历史搜索记录
     *
     * @param keywords
     */
    public static void saveSearchStr(List<String> keywords) {
        StudentSPUtils.putList(App2.get(), "123lishi", "searchHistory", keywords);
    }
    /**
     * 获取历史搜索记录
     *
     * @return
     */
    public static List getSearchStr() {
        return StudentSPUtils.getList(App2.get(), "123lishi", "searchHistory");
    }
    /**
     * 存储 学情分析 科目
     */
    public static void saveXueKeStr(String name) {
        StudentSPUtils.put(App2.get(), StudentSPUtils.SP_XUEKE, name);
    }

    public static String getXueKeStr() {
        return (String) StudentSPUtils.get(App2.get(), StudentSPUtils.SP_XUEKE, "");
    }



    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     *
     * @param context
     * @param key
     * @param object
     */
    public static void put(Context context, String key, Object object) {

        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }

        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param context
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object get(Context context, String key, Object defaultObject) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);

        if (defaultObject instanceof String) {
            return sp.getString(key, (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sp.getLong(key, (Long) defaultObject);
        }

        return null;
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param context
     * @param key
     */
    public static void remove(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param context
     * @param key
     */
    public static void removeWrite(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(SP_LAST_PAIRED, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 清除所有数据
     *
     * @param context
     */
    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean contains(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.contains(key);
    }

    /**
     * 返回所有的键值对
     *
     * @param context
     * @return
     */
    public static Map<String, ?> getAll(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.getAll();
    }

    /**
     * 创建一个解决SharedPreferencesCompat.apply方法的一个兼容类
     *
     * @author zhy
     */
    private static class SharedPreferencesCompat {
        private static final Method sApplyMethod = findApplyMethod();

        /**
         * 反射查找apply的方法
         *
         * @return
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        private static Method findApplyMethod() {
            try {
                Class clz = SharedPreferences.Editor.class;
                return clz.getMethod("apply");
            } catch (NoSuchMethodException e) {
            }

            return null;
        }

        /**
         * 如果找到则使用apply执行，否则使用commit
         *
         * @param editor
         */
        public static void apply(SharedPreferences.Editor editor) {
            try {
                if (sApplyMethod != null) {
                    sApplyMethod.invoke(editor);
                    return;
                }
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
            editor.commit();
        }
    }


    private static LoginBean mLoginBean;


    public static LoginBean getLoginBean() {
        if (mLoginBean != null) {
            return mLoginBean;
        }

        String loginInfo = (String) StudentSPUtils.get(App2.get(), StudentSPUtils.SAVE_LOGIN_INFO, "");
        if (!TextUtils.isEmpty(loginInfo)) {
            try {
                mLoginBean = new Gson().fromJson(loginInfo, LoginBean.class);
            } catch (Exception e) {
                mLoginBean = null;
            }
        }
        return mLoginBean;
    }

    public static void saveLoginBean(LoginBean loginBean) {
        mLoginBean = loginBean;
        final String loginJsonStr = new Gson().toJson(loginBean);
        put(App2.get(), SAVE_LOGIN_INFO, loginJsonStr);
    }

    public static void saveLastPenAddr(String addr) {
        put(App2.get(), SP_PAIRED_KEY, addr);
    }

    public static void saveAnswerTime(long time) {
        put(App2.get(), SP_ANSWER_TIME, time);
    }

    public static long getAnswerTime() {
        return (long) get(App2.get(), SP_ANSWER_TIME, 0L);
    }

    public static String getLastPenAddr() {
        return (String) get(App2.get(), SP_PAIRED_KEY, "");
    }


    public static void savetoken(String token) {
        put(App2.get(), SP_TOKEN, token);
    }

    public static String gettoken() {
        return (String) get(App2.get(), SP_TOKEN, "");
    }

    /*================ 管控登录对接 ================*/

    public static void saveLoginUserNum(String name) {
        StudentSPUtils.put(App2.get(), StudentSPUtils.LOGIN_USER_NUM, name);
    }

    public static String getLoginUserNum() {
        return (String) StudentSPUtils.get(App2.get(), StudentSPUtils.LOGIN_USER_NUM, "");
    }

    public static void saveLoginUserPwd(String pwd) {
        StudentSPUtils.put(App2.get(), StudentSPUtils.LOGIN_USER_PWD, pwd);
    }

    public static String getLoginUserPwd() {
        return (String) StudentSPUtils.get(App2.get(), StudentSPUtils.LOGIN_USER_PWD, "");
    }

    public static void setAutoLogin(boolean b) {
        StudentSPUtils.put(App2.get(), StudentSPUtils.AUTO_LOGIN, b);
    }

    public static boolean isAutoLogin() {
        return (boolean) StudentSPUtils.get(App2.get(), StudentSPUtils.AUTO_LOGIN, false);
    }


    //===================== 键盘高度 =====================

    public static void saveKeyBoardHeight(int height) {
        StudentSPUtils.put(App2.get(), SP_KEY_BOARD_HEIGHT, height);
    }

    public static int getKeyBoardHeight() {
        return (int) StudentSPUtils.get(App2.get(), SP_KEY_BOARD_HEIGHT, 0);
    }


    private static void saveStudentCourse(List<ChapterBean> list) {
        StudentSPUtils.put(App2.get(), SP_STUDENT_COURSE, GsonUtil.toJson(list));
    }

    public static void saveStudentCourse(int subjectId, long courseId) {
        List<ChapterBean> list = getStudentCourse();
        if (list == null) {
            list = new ArrayList<>();
        }
        for (ChapterBean bean : list) {
            if (bean.getSubjectId() == subjectId) {
                if (bean.getChapterId() == courseId) {
                    return;
                }
                bean.setChapterId(courseId);
                saveStudentCourse(list);
                return;
            }
        }
        list.add(new ChapterBean(subjectId, courseId));
        saveStudentCourse(list);
    }

    private static List<ChapterBean> getStudentCourse() {
        String str = (String) StudentSPUtils.get(App2.get(), SP_STUDENT_COURSE, "");
        if (str == null || str.isEmpty()) {
            return null;
        }
        return GsonUtil.gsonToList(str, ChapterBean.class);
    }

    public static long getStudentCourse(int subjectId) {
        List<ChapterBean> list = getStudentCourse();
        if (list == null || list.isEmpty()) {
            return -1;
        }
        for (ChapterBean bean : list) {
            if (bean.getSubjectId() == subjectId) {
                return bean.getChapterId();
            }
        }
        return -1;
    }


    /**
     * @param context
     * @param fileName
     * @param key
     * @param list
     */
    public static void putList(Context context, String fileName, String key, List list) {
        SharedPreferences sp = context.getSharedPreferences(fileName,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        // 实例化一个ByteArrayOutputStream对象，用来装载压缩后的字节文件。
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // 然后将得到的字符数据装载到ObjectOutputStream
        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            // writeObject 方法负责写入特定类的对象的状态，以便相应的 readObject 方法可以还原它
            objectOutputStream.writeObject(list);
            // 最后，用Base64.encode将字节文件转换成Base64编码保存在String中
            String sceneListString = new String(Base64.encode(
                    byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
            editor.putString(key, sceneListString);
        } catch (IOException e) {
            LogUtils.e(TAG, e);
        } finally {
            CloseUtils.closeIOQuietly(objectOutputStream);
        }
        SharedPreferencesCompat.apply(editor);

    }
    public static List getList(Context context, String fileName, String key) {
        List list;
        SharedPreferences sp = context.getSharedPreferences(fileName,
                Context.MODE_PRIVATE);
        String sceneListString = sp.getString(key, "");
        byte[] mobileBytes = Base64.decode(sceneListString.getBytes(), Base64.DEFAULT);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                mobileBytes);
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(
                    byteArrayInputStream);
            list = (List) objectInputStream.readObject();

        } catch (IOException | ClassNotFoundException e) {
            LogUtils.e(TAG, e);
            list = new ArrayList();
        } finally {
            CloseUtils.closeIOQuietly(objectInputStream);
        }
        return list;

    }
}
