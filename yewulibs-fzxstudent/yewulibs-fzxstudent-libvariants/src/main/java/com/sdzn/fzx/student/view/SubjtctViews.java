package com.sdzn.fzx.student.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.student.bean.GrzxRecActBean;
import com.sdzn.fzx.student.bean.SubjectBean;

import java.util.List;

/**
 * 首页 view
 */
public interface SubjtctViews extends IView {
    void onSubjtctSuccess(List<SubjectBean> subjectBeans);

    void onSubjtctNodata(String msg);

    void onSubjtctFail(String msg);
}
