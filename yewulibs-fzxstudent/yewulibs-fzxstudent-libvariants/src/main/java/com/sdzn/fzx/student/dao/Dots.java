package com.sdzn.fzx.student.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @author Reisen at 2018-08-06
 */
@Entity
public class Dots {
    public Dots(String appId, String appNum, int BookID, int PageID, float x, float y, int f, int t, int width, int color, int counter, int angle) {
        this(appId,appNum,0,BookID,PageID,x,y,f,t,width,color,counter,angle);
    }

    public Dots(String appId, String appNum,int position, int BookID, int PageID, float x, float y, int f, int t, int width, int color, int counter, int angle) {
        bookID = BookID;
        pageID = PageID;
        pointX = x;
        pointY = y;
        force = f;
        ntype = t;
        penWidth = width;
        ncolor = color;
        ncounter = counter;
        nangle = angle;
        this.appId = appId;
        this.appNum = appNum;
        this.position = position;
    }

    @Generated(hash = 2113066965)
    public Dots(int bookID, int pageID, int ncounter, float pointX, float pointY, int force, int ntype, int penWidth, int ncolor, int nangle, String appId,
            String appNum, int position) {
        this.bookID = bookID;
        this.pageID = pageID;
        this.ncounter = ncounter;
        this.pointX = pointX;
        this.pointY = pointY;
        this.force = force;
        this.ntype = ntype;
        this.penWidth = penWidth;
        this.ncolor = ncolor;
        this.nangle = nangle;
        this.appId = appId;
        this.appNum = appNum;
        this.position = position;
    }

    @Generated(hash = 362553106)
    public Dots() {
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppNum() {
        return appNum;
    }

    public void setAppNum(String appNum) {
        this.appNum = appNum;
    }

    public int bookID;
    public int pageID;
    public int ncounter;
    public float pointX;
    public float pointY;
    public int force;
    public int ntype;  //0-down;1-move;2-up;
    public int penWidth;
    public int ncolor;
    public int nangle;
    public String appId;
    public String appNum;
    public int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public int getPageID() {
        return pageID;
    }

    public void setPageID(int pageID) {
        this.pageID = pageID;
    }

    public int getNcounter() {
        return ncounter;
    }

    public void setNcounter(int ncounter) {
        this.ncounter = ncounter;
    }

    public float getPointX() {
        return pointX;
    }

    public void setPointX(float pointX) {
        this.pointX = pointX;
    }

    public float getPointY() {
        return pointY;
    }

    public void setPointY(float pointY) {
        this.pointY = pointY;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getNtype() {
        return ntype;
    }

    public void setNtype(int ntype) {
        this.ntype = ntype;
    }

    public int getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(int penWidth) {
        this.penWidth = penWidth;
    }

    public int getNcolor() {
        return ncolor;
    }

    public void setNcolor(int ncolor) {
        this.ncolor = ncolor;
    }

    public int getNangle() {
        return nangle;
    }

    public void setNangle(int nangle) {
        this.nangle = nangle;
    }
}
