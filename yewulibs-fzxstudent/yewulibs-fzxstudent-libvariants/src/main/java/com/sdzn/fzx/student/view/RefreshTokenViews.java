package com.sdzn.fzx.student.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.student.bean.RefreshTokenRecActBean;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;

/**
 * 首页 view
 */
public interface RefreshTokenViews extends IView {
    void onRefreshTokenSuccess(RefreshTokenRecActBean refreshTokenRecActBean);

    void onRefreshTokenNodata(String msg);

    void onRefreshTokenFail(String msg);
}
