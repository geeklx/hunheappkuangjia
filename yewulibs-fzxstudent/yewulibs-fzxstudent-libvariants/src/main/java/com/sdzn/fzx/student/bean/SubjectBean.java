package com.sdzn.fzx.student.bean;

public class SubjectBean {

    /**
     * id : 2
     * name : 语文
     * subjectPic : {"select":"https://file.fuzhuxian.com/chineseActive.png","notSelect":"https://file.fuzhuxian.com/chinese.png"}
     * sort : 1
     * levelId : 2
     * isValid : 1
     * isDelete : 0
     * isUpdate : 0
     * createTime : 2020-07-01 13:52:03
     * updateTime :
     * deleteTime :
     * isDefault : 1
     * levelName : 初中
     */

    private String id;
    private String name;
    private String subjectPic;
    private int sort;
    private int levelId;
    private int isValid;
    private int isDelete;
    private int isUpdate;
    private String createTime;
    private String updateTime;
    private String deleteTime;
    private int isDefault;
    private String levelName;
    private boolean enable;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public SubjectBean(String id, String name, String subjectPic, boolean enable) {
        this.id = id;
        this.name = name;
        this.subjectPic = subjectPic;
        this.enable = enable;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubjectPic() {
        return subjectPic;
    }

    public void setSubjectPic(String subjectPic) {
        this.subjectPic = subjectPic;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public int getIsValid() {
        return isValid;
    }

    public void setIsValid(int isValid) {
        this.isValid = isValid;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public int getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(int isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

}
