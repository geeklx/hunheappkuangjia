package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class WrongVo {

    /**
     * total : 0
     * data : [{"count":0,"baseSubjectId":1,"baseSubjectName":"语文"},{"count":0,"baseSubjectId":2,"baseSubjectName":"数学"},{"count":0,"baseSubjectId":3,"baseSubjectName":"英语"},{"count":0,"baseSubjectId":9,"baseSubjectName":"思想品德"},{"count":0,"baseSubjectId":11,"baseSubjectName":"音乐"},{"count":0,"baseSubjectId":12,"baseSubjectName":"美术"},{"count":0,"baseSubjectId":13,"baseSubjectName":"信息技术"},{"count":0,"baseSubjectId":14,"baseSubjectName":"体育与健康"},{"count":0,"baseSubjectId":15,"baseSubjectName":"科学"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * count : 0
         * baseSubjectId : 1
         * baseSubjectName : 语文
         */

        private int count;
        private int baseSubjectId;
        private String baseSubjectName;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }
    }
}
