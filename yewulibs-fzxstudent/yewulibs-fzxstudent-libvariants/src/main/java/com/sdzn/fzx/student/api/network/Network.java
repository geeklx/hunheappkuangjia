package com.sdzn.fzx.student.api.network;

import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.interceptor.LoggingInterceptor2;
import com.sdzn.fzx.student.api.interceptor.TokenInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 自定义Subscriber
 * 描述：
 * -
 * 创建人：zhangchao
 * 创建时间2017/3/20
 */
public class Network {

    public final static boolean IS_TEST = false;

    //    public static String BASE_ADDRESS = IS_TEST ? "http://192.168.0.204" : "http://115.28.91.19";//开发测试环境:正式环境
//    public static String BASE_ADDRESS = IS_TEST ? "http://118.190.68.12" : "http://115.28.91.19";//测试环境
//    public static final String BASE_ADDRESS = IS_TEST ? "http://192.168.0.115" : "http://115.28.91.19";//测试环境
//    public static String BASE_MQTT_URL = IS_TEST ? "tcp://192.168.0.206:1883" : "tcp://115.28.84.106:1883";//mqttc测试地址
    public static String BASE_MQTT_URL = BuildConfig2.MQTT;
//    public static String BASE_SHARE_URL = IS_TEST ? "http://192.168.0.202:9096" : "http://share.runningtank.com";//分享地址

    //    public static String BASE_DOWN_POST = IS_TEST ? ":8030/app.info" : ":8020/app.info";
//    public static String BASE_DOWN_URL = IS_TEST ? "http://192.168.0.207:8030/app.info" : "http://115.28.91.19:8020/app.info";//下载
    public static String BASE_DOWN_URL = BuildConfig2.DOWN;
//    public static String BASE_DOWN_URL = "http://115.28.91.19:8020/app.in
// fo";//下载

    //    public static final String POST = IS_TEST ? ":9000" : ":8000";
//    public static final String POST = IS_TEST ? ":8000" : ":8000";
    public final static String BASE_URL = BuildConfig2.BASE_ADDRESS + BuildConfig2.PORT;

    public final static String BASE_MQTT_USENAME = "sdzn_tank";//mqtt正式用户名
    public final static String BASE_MQTT_PASSWORD = "sdzn_tank";//mqtt正式密码
    public final static String BASE_GET_UPTOKEN = BuildConfig2.BASE_ADDRESS + BuildConfig2.PORT + "/teach/QiniuController/getUptoken";

    public final static String QINIU_KEY="3699249853abafbbf99b778ac0b9c3b90b8b49ba";

    public final static String QINIU_HOST="http://csfile.fuzhuxian.com";


    private static TokenInterceptor tokenInterceptor = new TokenInterceptor();
    private static LoggingInterceptor2 loggingInterceptor = new LoggingInterceptor2();

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

    private static OkHttpClient tokenOkHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(tokenInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();

    private static Converter.Factory gsonConverterFactory = GsonConverterFactory.create();
    private static CallAdapter.Factory rxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create();


    /**
     * 一般登陆和获取token的时候使用
     *
     * @param service
     * @param <S>
     * @return
     */
    public static <S> S createService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);
    }

    public static <S> S createService(Class<S> service, String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);
    }

    /**
     * 检查版本更新
     *
     * @param service
     * @param <S>
     * @return
     */
    public static <S> S createUpdateService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Network.BASE_DOWN_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);
    }

    /**
     * 带token验证的services
     *
     * @param service
     * @param <S>
     * @return
     */
    public static <S> S createTokenService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(tokenOkHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);

    }

    /**
     * 带token验证的services
     *
     * @param service
     * @param <S>
     * @return
     */
   /* public static <S> S createLocalService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(tokenOkHttpClient)
//                .baseUrl("http://114.115.155.179:8000")
                .baseUrl("http://192.168.100.247:8880")
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);

    }*/
    /**
     * 带token验证的services
     * 和后台接口调试
     *
     * @param service
     * @param <S>
     * @return
     */
    public static <S> S createLocalService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(tokenOkHttpClient)
//                .baseUrl("http://114.115.155.179:8000")
                .baseUrl("http://192.168.0.103:8089")
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);

    }

}