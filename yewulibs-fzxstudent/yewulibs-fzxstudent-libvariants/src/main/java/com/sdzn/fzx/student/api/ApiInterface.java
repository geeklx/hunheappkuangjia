/*
package com.sdzn.fzx.student.api;

*/
/**
 * 描述：API请求地址
 * -
 *//*

public class ApiInterface {
    public static final String H5_SECOND_LEVEL_JUMP = "http://49.4.7.45:8090/#";//H5二级测试地址
    public static final String INTERFACE_ADDRESS = "http://49.4.7.45:7799";//测试环境接口地址

//    public static final String H5_ADDRESS = "http://192.168.0.131:8080/#/";//H5地址
//    public static final String H5_SECOND_LEVEL_JUMP = "http://192.168.0.131:8080/#";//H5地址

//    public static final String H5_ADDRESS = "http://49.4.1.81:8090/#/";//H5地址
//    public static final String H5_SECOND_LEVEL_JUMP = "http://49.4.1.81:8090/#";//H5地址
//    public static final String INTERFACE_ADDRESS = "http://49.4.1.81:7799";//开发环境接口地址


    public static final String QUERY_VERSION_INFO = "http://doc.znclass.com/usercenter/sysVersionAppInfo/selectOne";//查询版本信息
    public static final String QUERY_CEHUA_INFO = INTERFACE_ADDRESS + "/course/home/student/broadside/list";//侧边栏数据
    public static final String QUERY_TAB_INFO = INTERFACE_ADDRESS + "/course/home/tabJump/list";//动态获取tab数据
    public static final String QUERY_SHOUYE_INFO = INTERFACE_ADDRESS + "/course/home/student/data";//首页数据
    public static final String QUERY_TOKEN = INTERFACE_ADDRESS + "/student/login/refresh/token";//刷新token
    public static final String LOGIN_INFO = INTERFACE_ADDRESS + "/student/login/token";//学生端登录
    public static final String SEND_VERIFYCODE = INTERFACE_ADDRESS + "/student/login/sendVerifyCode";//发送短信验证码
    public static final String CHECK_VERIFYCODE = INTERFACE_ADDRESS + "/teacher/check/smsToken";//验证验证码
    public static final String STUDENT_INFO = INTERFACE_ADDRESS + "/usercenter/student/find/StudentUserCenter";//个人中心获取学生资料
    public static final String STUDENT_MOBILE_UPDATE = INTERFACE_ADDRESS + "/usercenter/student/update/mobile";//个人中心更新-手机号
    public static final String STUDENT_PASSWORD_UPDATE = INTERFACE_ADDRESS + "/usercenter/student/update/password";//个人中心更新-密码
    public static final String STUDENT_LOGIN_MOBILE = INTERFACE_ADDRESS + "/usercenter/student/find/studentMobile";//忘记密码--通过学生账号/手机号 获取手机号
    public static final String STUDENT_LOGIN_PASSWORD_UPDATE = INTERFACE_ADDRESS + "/usercenter/student/student/forget/password";//忘记密码--更新密码

    public static final String QINIU_TOKEN = INTERFACE_ADDRESS + "/knowledgecenter/resource/getUploadToken";//获取七牛token
    public static final String QINIU_URL = "/knowledgecenter/resource/getUrlByFileName";//七牛获取
}
*/
