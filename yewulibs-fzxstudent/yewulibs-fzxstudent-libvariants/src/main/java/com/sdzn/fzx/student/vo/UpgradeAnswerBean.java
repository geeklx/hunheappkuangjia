package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/14.
 */
public class UpgradeAnswerBean {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private List<UpgradeAnswerListBean.AnswerDataBean> examList;
        private String teacherExamId;

        public List<UpgradeAnswerListBean.AnswerDataBean> getExamList() {
            return examList;
        }

        public void setExamList(List<UpgradeAnswerListBean.AnswerDataBean> examList) {
            this.examList = examList;
        }

        public String getTeacherExamId() {
            return teacherExamId;
        }

        public void setTeacherExamId(String teacherExamId) {
            this.teacherExamId = teacherExamId;
        }
    }

    public static class SubmitBean {
        private String studentAnswer;
        private String questionId;
        private List<SubmitOptionAnswer> optionAnswer;

        public String getStudentAnswer() {
            return studentAnswer;
        }

        public void setStudentAnswer(String studentAnswer) {
            this.studentAnswer = studentAnswer;
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public List<SubmitOptionAnswer> getOptionAnswer() {
            return optionAnswer;
        }

        public void setOptionAnswer(List<SubmitOptionAnswer> optionAnswer) {
            this.optionAnswer = optionAnswer;
        }
    }

    public static class SubmitOptionAnswer {
        public static SubmitOptionAnswer init(boolean select, int seq) {
            SubmitOptionAnswer answer = new SubmitOptionAnswer();
            answer.setSelect(select);
            answer.setSeq(seq);
            return answer;
        }

        private boolean select;
        private int seq;

        public boolean isSelect() {
            return select;
        }

        public void setSelect(boolean select) {
            this.select = select;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }
    }
}
