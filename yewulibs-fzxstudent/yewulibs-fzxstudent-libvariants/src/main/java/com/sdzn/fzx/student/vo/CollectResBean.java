package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * CollectResBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CollectResBean {

    /**
     * id : 1
     * resourceId : 1
     * resourceType : null
     * resourceName : null
     * subjectStructureId : null
     * subjectStructureName : null
     * subjectStructureIdPath : null
     * subjectStructureNamePath : null
     * userStudentId : 1
     * userStudentName : null
     * createTime : null
     * chapterId : null
     * chapterName : null
     * chapterNodeIdPath : null
     * chapterNodeNamePath : null
     * baseVolumeId : null
     * baseVolumeName : null
     * baseSubjectId : null
     * baseSubjectName : null
     * baseGradeId : null
     * baseGradeName : null
     * type : null
     * typeReview : null
     * typeReviewName : null
     * lessonId : null
     * lessonName : null
     * resourceText : null
     */

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private int id;
        private int resourceId;
        private int resourceType;
        private String resourceName;
        private String subjectStructureId;
        private String subjectStructureName;
        private String subjectStructureIdPath;
        private String subjectStructureNamePath;
        private int userStudentId;
        private String userStudentName;
        private long createTime;
        private String chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private String baseVolumeId;
        private String baseVolumeName;
        private String baseSubjectId;
        private String baseSubjectName;
        private String baseGradeId;
        private String baseGradeName;
        private String type;
        private String typeReview;
        private String typeReviewName;
        private String lessonId;
        private String lessonName;
        private String resourceText;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getResourceId() {
            return resourceId;
        }

        public void setResourceId(int resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public String getSubjectStructureId() {
            return subjectStructureId;
        }

        public void setSubjectStructureId(String subjectStructureId) {
            this.subjectStructureId = subjectStructureId;
        }

        public String getSubjectStructureName() {
            return subjectStructureName;
        }

        public void setSubjectStructureName(String subjectStructureName) {
            this.subjectStructureName = subjectStructureName;
        }

        public String getSubjectStructureIdPath() {
            return subjectStructureIdPath;
        }

        public void setSubjectStructureIdPath(String subjectStructureIdPath) {
            this.subjectStructureIdPath = subjectStructureIdPath;
        }

        public String getSubjectStructureNamePath() {
            return subjectStructureNamePath;
        }

        public void setSubjectStructureNamePath(String subjectStructureNamePath) {
            this.subjectStructureNamePath = subjectStructureNamePath;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getChapterId() {
            return chapterId;
        }

        public void setChapterId(String chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public String getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(String baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public String getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(String baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public String getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(String baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTypeReview() {
            return typeReview;
        }

        public void setTypeReview(String typeReview) {
            this.typeReview = typeReview;
        }

        public String getTypeReviewName() {
            return typeReviewName;
        }

        public void setTypeReviewName(String typeReviewName) {
            this.typeReviewName = typeReviewName;
        }

        public String getLessonId() {
            return lessonId;
        }

        public void setLessonId(String lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public String getResourceText() {
            return resourceText;
        }

        public void setResourceText(String resourceText) {
            this.resourceText = resourceText;
        }
    }
}
