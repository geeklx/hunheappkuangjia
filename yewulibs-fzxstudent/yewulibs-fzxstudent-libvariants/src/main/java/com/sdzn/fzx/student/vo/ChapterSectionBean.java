package com.sdzn.fzx.student.vo;


import androidx.annotation.NonNull;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/14.
 */
public class ChapterSectionBean implements Comparable<ChapterSectionBean>{
    private List<ChapterSectionBean> children;
    private long nodeId;
    private String nodeName;
    private int orderIndex;
    private long parentNodeId;
    private double score;

    public List<ChapterSectionBean> getChildren() {
        return children;
    }

    public void setChildren(List<ChapterSectionBean> children) {
        this.children = children;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public long getParentNodeId() {
        return parentNodeId;
    }

    public void setParentNodeId(long parentNodeId) {
        this.parentNodeId = parentNodeId;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public int compareTo(@NonNull ChapterSectionBean o) {
        return Long.compare(nodeId,o.nodeId);
    }
}
