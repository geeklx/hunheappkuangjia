package com.sdzn.fzx.student.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/15
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class WhiteVo {
    private String name;
    private int page;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
