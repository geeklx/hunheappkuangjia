/**
 * Created by wjh on 2018/3/1.
 */
var TodayError = function () {
  this.view = $('.error');
  this.getData();

};

TodayError.prototype.getData = function () {
  var param = {};
  param.userStudentId = userStudentId;
  param.model = model;
  ErrorHandler.getTodayError(param, this, this.getDataCallBack);

};

TodayError.prototype.getDataCallBack = function (flag, data) {
  if (flag) {
    this.data = data.result.data;
    this.initData();
  }
};

TodayError.prototype.initData = function () {
  if (this.data.length != 0) {
    this.view.empty();
    for (var i = 0; i < this.data.length; i++) {
      this.subjectAllView = $(TodayError.subjectAllTempalte).clone();
      this.subjectNameView = $(TodayError.subjectNameTempalte).clone();
      this.subjectAllView.append(this.subjectNameView);
      this.view.append(this.subjectAllView);

      // 学科logo
      this.subjectNameView.find('[sid=t-subject-icon]').attr('sid', 'subjectIcon' + this.data[i].baseSubjectId);
      if (this.data[i].baseSubjectId === 1) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/yuwen.png" width="32px" height="32px" style="vertical-align: middle">');
      } else if (this.data[i].baseSubjectId === 2) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/shuxue.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 3) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/yingyu.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 4) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/wuli.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 5) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/huaxue.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 6) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/shengwu.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 7) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/lishi.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 8) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/dili.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 9) {
        // this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/zhengzhi.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 10) {
        this.subjectNameView.find('[sid=subjectIcon'+this.data[i].baseSubjectId +']').html('<img src="../img/error/zhengzhi.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 11) {
        // this.subjectNameView.find('[sid=subjectIcon'+this.data[i].baseSubjectId +']').html('<img src="../img/error/zhengzhi.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 12) {
        // this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/tongyong.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 13) {
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/kexue.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 14) {
        // this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/lishi.png" width="32px" height="32px" style="vertical-align: middle">');

      } else if (this.data[i].baseSubjectId === 15) {
        // this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/lishi.png" width="32px" height="32px" style="vertical-align: middle">');

      }else if (this.data[i].baseSubjectId === 16) { //通用技术
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/tongyong.png" width="32px" height="32px" style="vertical-align: middle">');

      }
      else if (this.data[i].baseSubjectId === 18) {//政治
        this.subjectNameView.find('[sid=subjectIcon' + this.data[i].baseSubjectId + ']').html('<img src="../img/error/zhengzhi.png" width="32px" height="32px" style="vertical-align: middle">');

      }

      // 学科名称
      this.subjectNameView.find('[sid=t-subject-name]').attr('sid', 'subject' + this.data[i].baseSubjectId);
      this.subjectNameView.find('[sid=subject' + this.data[i].baseSubjectId + ']').html("&nbsp;&nbsp;" + this.data[i].baseSubjectName);

      //对应试题数量
      this.subjectNameView.find('[sid=t-subject-totalNum]').attr('sid', 'subjectTotal' + this.data[i].baseSubjectId);
      this.subjectNameView.find('[sid=subjectTotal' + this.data[i].baseSubjectId + ']').text(this.data[i].examList.length);

      var subTest = this.data[i].examList;
      for (var j = 0; j < subTest.length; j++) {
        this.testInsAllView = $(TodayError.testInsAllTempalte).clone();
        this.testInsView = $(TodayError.testInsTempalte).clone();
        this.testInsAllView.append(this.testInsView);
        this.subjectAllView.append(this.testInsAllView);

        // 序号
        this.testInsView.find('[sid=testNum]').attr('class', 'testNum');
        this.testInsView.find('[sid=testNum]').attr('sid', 'testNum' + subTest[j].id);
        this.testInsView.find('[sid=testNum' + subTest[j].id + ']').text(j + 1 + ".");

        // 试题类型
        this.testInsView.find('[sid=testType]').attr('sid', 'testType' + subTest[j].id);
        this.testInsView.find('[sid=testType' + subTest[j].id + ']').html(subTest[j].examTemplateStyleName + "&nbsp;&nbsp;&nbsp;&nbsp;");

        // 错误次数
        this.testInsView.find('[sid=errorcount]').attr('sid', 'errorcount' + subTest[j].id);
        this.testInsView.find('[sid=errorcount' + subTest[j].id + ']').html(subTest[j].count + "次&nbsp;&nbsp;&nbsp;&nbsp;");

        // 难易程度
        this.testInsView.find('[sid=testDifficulty]').attr('sid', 'testDifficulty' + subTest[j].id);
        if (subTest[j].examDifficulty === 1) {
          this.testInsView.find('[sid=testDifficulty' + subTest[j].id + ']').html("简单" + "&nbsp;&nbsp;&nbsp;&nbsp;");
        } else if (subTest[j].examDifficulty === 2) {
          this.testInsView.find('[sid=testDifficulty' + subTest[j].id + ']').html("中等" + "&nbsp;&nbsp;&nbsp;&nbsp;");
        } else if (subTest[j].examDifficulty === 3) {
          this.testInsView.find('[sid=testDifficulty' + subTest[j].id + ']').html("较难" + "&nbsp;&nbsp;&nbsp;&nbsp;");
        } else {
          this.testInsView.find('[sid=testDifficulty' + subTest[j].id + ']').html("难" + "&nbsp;&nbsp;&nbsp;&nbsp;");
        }

        // 章节
        this.testInsView.find('[sid=chapter]').attr('sid', 'chapter' + subTest[j].id);
        if(subTest[j].chapterNodeNamePath != null){
          this.testInsView.find('[sid=chapter' + subTest[j].id + ']').html(subTest[j].chapterNodeNamePath + "&nbsp;&nbsp;");
        }


        // 修改时间
        var updateTime = formatDate(subTest[j].timeUpdate, 'yyyy-MM-dd');
        var weekDay = this.getWeeks(updateTime);
        this.testInsView.find('[sid=testUpdate]').attr('sid', 'testUpdate' + subTest[j].id);
        this.testInsView.find('[sid=testUpdate' + subTest[j].id + ']').html(updateTime + "&nbsp;&nbsp;" + weekDay + "&nbsp;&nbsp;");
        // 删除错题
        this.testInsView.find('[sid=test-delete]').attr('sid', 'test-delete' + subTest[j].id);
        this.testInsView.find('[sid=test-delete' + subTest[j].id + ']').on("click", {
          'obj': this,
          'id': subTest[j].id
        }, this.deleteClick);

        //加载题型
        var temp = new ExamPreview(JSON.parse(subTest[j].examText), 1);
        this.testInsAllView.append(temp.view);


        // 答案显示隐藏
        this.view.find('[sid=t-test]').attr('sid', 't-test' + subTest[j].id);
        this.testInsView.find('[sid=rough-took]').attr('sid', 'rough-took' + subTest[j].id);
        this.showAnswerView = this.testInsView.find('[sid=rough-took' + subTest[j].id + ']');
        this.answerSelectObj = new SwitchSelect(this.showAnswerView);
        this.answerSelectObj.addListener(this, this.changeAnswerStatus);

        this.testInsAllView.find('.exam-scan-h').addClass('dn');


        // 展开 收起
        this.imgSwitchView = $(TodayError.imgSwitchTempalte).clone();
        this.testInsAllView.append(this.imgSwitchView);
        if (this.testInsAllView.height() > 201) {
          this.imgSwitchView.append('<img src="../img/error/zhankia_icon@2x.png" style="width: 42px;height: 16px">');
          var aa = this.testInsAllView.find('.exam-scan').html();
          this.aa = aa;
          this.testInsAllView.find('[sid=imgSwith]').attr('sid', 'imgSwith' + subTest[j].id);
          this.contentSelectView = this.testInsAllView.find('[sid=imgSwith' + subTest[j].id + ']');
          this.contentSelectObj = new SwitchContent(this, subTest[j].id, this.aa, this.contentSelectView);
        }

      }

    }
  }

};

TodayError.prototype.changeAnswerStatus = function (status, obj) {
  if (status == 1) {
    $(obj).parent().parent().parent().parent().find('.exam-scan-h').removeClass('dn');
  } else {
    $(obj).parent().parent().parent().parent().find('.exam-scan-h').addClass('dn');
  }

};


TodayError.prototype.getWeeks = function (days) {
  var m = new Date(days);
  var weeks;
  if (m.getDay() === 0) {
    weeks = "星期天";
  }
  if (m.getDay() === 1) {
    weeks = "星期一";
  }
  if (m.getDay() === 2) {
    weeks = "星期二";
  }
  if (m.getDay() === 3) {
    weeks = "星期三";
  }
  if (m.getDay() === 4) {
    weeks = "星期四";
  }
  if (m.getDay() === 5) {
    weeks = "星期五";
  }
  if (m.getDay() === 6) {
    weeks = "星期六";
  }
  return weeks;
};

TodayError.subjectAllTempalte = '<div class="subjectAll" sid="t-subjectAll">' +
  '</div>';

// 学科名 模板
TodayError.subjectNameTempalte = '<div class="t-subject clearfix" sid="t-subject">' +
  '<div class="l">' +
  '<i class="t-subject-icon" sid="t-subject-icon"></i>' +
  '<span class="font-black" sid="t-subject-name"></span>' +
  '</div>' +
  '<div class="r">' +
  '<span class="font-grey">共</span>' +
  '<span class="font-black2" sid="t-subject-totalNum">14</span>' +
  '<span  class="font-grey">道</span>' +
  '</div>' +
  '</div>';

//题目介绍
TodayError.testInsAllTempalte = '<div class="t-test clearfix" sid="t-test">' +
  '</div>';

TodayError.testInsTempalte = '<div class="test-ins font-grey bac-grey clearfix" sid="test-ins">' +
  '<div class="l" style="line-height: 38px">' +
  '<span class="" sid="testNum"></span>' +
  '<span class="" sid="testType"></span>' +
  '<span class="font-red" sid="errorcount"></span>' +
  '<span class="" sid="testDifficulty"></span>' +
  '<span class="" sid="chapter"></span>' +
  '</div>' +
  '<div class="r">' +

  '<div class="test-delete" sid="test-delete" style="float: right;margin-top: 4px"></div>' +

  '<div class="ans-r-info-btn pi-jp-item">' +
  '<div class="pi-jp-switch" sid="rough-took">' +
  '<span class="switch-y">显</span> ' +
  '<span class="switch-n">隐</span>' +
  '<i class="switch-icon"></i>' +
  '</div>' +
  '<span class="pi-jp-label">答案</span>' +
  '</div>' +

  '<div class="" sid="testUpdate" style="float: right;line-height: 38px"></div>' +
  '</div>' +
  '</div>';
// 展开 收起
TodayError.imgSwitchTempalte = '<div class="imgSwith" sid="imgSwith" style="text-align: center;border-top: 1px solid #EDEDED;margin-bottom: 10px;">' +

  '</div>';
/**
 * 删除今日错题事件
 * @param e
 */
// TodayError.prototype.deleteClick = function (e) {
//     // console.log(e.data);
//     var self = e.data.obj;
//     var studentBookId = e.data.id;
//     if (model === 1) {
//         ErrorHandler.errorDelete({'studentBookId': studentBookId}, self, self.deleteCallBack);
//     } else {
//         ErrorHandler.collectDelete({'studentBookId': studentBookId}, self, self.deleteCallBack);
//     }
//
//
// };


TodayError.prototype.deleteClick = function (e) {

  var self = e.data.obj;
  var studentBookId = e.data.id;
  delCollExam(model, studentBookId);
};


// TodayError.prototype.deleteCallBack = function (flag, data) {
//     if (flag) {
//         this.getData();
//         console.log("删除成功");
//         deleInfo(data.msg);
//     } else {
//         console.log("删除失败");
//         deleInfo(data.msg);
//     }
//
// };




