/**
 * 文件名：answer-exam-handler
 * 描述：接口
 * 修改人： zhangxia
 * 修改时间：2018/2/28
 */
/**试题收藏**/
var EXAM_COLLECTION_ADD = 'teach/student/exam/book/collect/save';
var EXAM_COLLECTION_DELETE = 'teach/student/exam/book/collect/delete';

/**资料收藏**/
var RES_COLLECTION_ADD = 'teach/teachStudentResource/resource/collect/insert';
var RES_COLLECTION_DELETE = 'teach/teachStudentResource/resource/collect/delete';

/**获取任务详情**/
var GET_LESSON_GUIDE_DETAIL = 'teach/student/exam/list';

/**获取试题详情**/
var GET_EXAM_DETAILS = 'teach/exam/info';

/**保存学生答题详情**/
var SUBMIT_ANSWER_DETAIL = 'teach/student/exam/submit';
var SAVE_ANSWER_DETAIL = 'teach/student/exam/save';

/**获取作答结果**/
var GET_STUDENT_ANSWER_DETAIL = 'teach/student/exam/info';
var GET_STUDENT_UPTOKEN = 'teach/QiniuController/getUptoken';

//获取七牛图片后缀
var GET_QINIU_RESULT = 'teach/QiniuController/getQiniuConfig';

var LearningGuideHandler = function () {
};
LearningGuideHandler.resGetQiniuConfig = function (data, listener, listenerFun) {

    doAjax({
        url: GET_QINIU_RESULT,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};
LearningGuideHandler.readExam = function (data, listener, listenerFun) {
    doAjax({
        url: GET_EXAM_DETAILS,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("EXAM_COLLECTION_DELETE");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }

        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
}
LearningGuideHandler.examCollectAdd = function (data, listener, listenerFun) {

    doAjax({
        url: EXAM_COLLECTION_ADD,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("EXAM_COLLECTION_ADD");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};

LearningGuideHandler.examCollectDelete = function (data, listener, listenerFun) {

    doAjax({
        url: EXAM_COLLECTION_DELETE,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("EXAM_COLLECTION_DELETE");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }

        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};

LearningGuideHandler.resCollectAdd = function (data, listener, listenerFun) {

    doAjax({
        url: RES_COLLECTION_ADD,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};

LearningGuideHandler.resCollectDelete = function (data, listener, listenerFun) {
    doAjax({
        url: RES_COLLECTION_DELETE,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }

        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};

LearningGuideHandler.getLMissionDetail = function (data, listener, listenerFun) {

    doAjax({
        url: GET_LESSON_GUIDE_DETAIL,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("GET_LESSON_GUIDE_DETAIL");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};

//保存学生答题详情 提交
LearningGuideHandler.submitLessonGuideDetail = function (data, listener, listenerFun) {
    console.log(JSON.stringify(data));
    doAjax({
        url: SUBMIT_ANSWER_DETAIL,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            alert(JSON.stringify(data));
            listenerFun.call(listener, false, data);
        }
    });
};

//保存学生答题详情  每分钟保存一个
LearningGuideHandler.getStudentLessonGuideDetail = function (data, listener, listenerFun) {
  doAjax({
    url: SAVE_ANSWER_DETAIL,
    data: data,
    type: "post",
    dataType: "json",
    success: function (data) {
      if (data.code === 0) {
        listenerFun.call(listener, true, data);
      } else {
        listenerFun.call(listener, false, data);
      }

        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};

//获取作答详情
LearningGuideHandler.getStudentAnswerDetail = function (data, listener, listenerFun) {

    doAjax({
        url: GET_STUDENT_ANSWER_DETAIL,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("GET_STUDENT_LESSON_GUIDE_DETAIL");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            console.log(data);
            listenerFun.call(listener, false, data);
        }
    });
};


LearningGuideHandler.getStudentUptoken = function (data, listener, listenerFun) {

    doAjax({
        url: GET_STUDENT_UPTOKEN,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};
