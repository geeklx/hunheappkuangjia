/**
 * Created by zhangxia on 2018/3/6.
 */
var AnswerResult = function () {
    this.view = $('.answer-result');
    $('.empty-view').removeClass('dn');
    this.view.addClass('dn');
    this.mode = 0; //只看错题
    this.examArr = []; //题目对象数组
    this.showFalse = 0; //0不启用  1启用
    this.showAnswer = 0;//0不启用  1启用
    this.init();
};

AnswerResult.prototype.init = function () {
    this.infoView = this.view.find('[sid=ans-r-info]');
    this.paperTypeView = this.infoView.find('[sid=paper]');
    this.myScoreView = this.infoView.find('[sid=my-score]');
    this.allScoreView = this.infoView.find('[sid=all-score]');
    this.averageScoreView = this.infoView.find('[sid=average-score]');
    this.timeView = this.infoView.find('[sid=time]');

    this.examCon = this.view.find('[sid=ans-r-con]');
    this.showAnswerView = this.view.find('[sid=rough-took]');
    this.showFalseExamView = this.view.find('[sid=error-model]');

    this.answerSelectObj = new SwitchSelect(this.showAnswerView);   //答案
    this.falseSelectObj = new SwitchSelect(this.showFalseExamView);   //错误试题
    this.falseSelectObj.addListener(this, this.onlyShowFalseExam);

    this.getExamList();
};

AnswerResult.prototype.setAnswerStatus = function () {
    this.answerSelectObj.setStatus(this.answerViewOpen);
    for (var i = 0; i < this.examArr.length; i++) {
        if (this.answerViewOpen == 1) {
            this.showAnswer = 1;
            this.examArr[i].examObj.showAnswer();
        } else {
            this.showAnswer = 0;
            this.examArr[i].examObj.hideAnswer();
        }
    }
    if (this.answerViewOpen) {
        this.answerSelectObj.addListener(this, this.changeAnswerStatus);
    } else {
        this.answerSelectObj.view.off('click');
    }
};

AnswerResult.prototype.changeAnswerStatus = function (status) {
    if (status == 1) {
        for (var i = 0; i < this.examArr.length; i++) {
            this.examArr[i].examObj.showAnswer();
        }
    } else {
        for (var i = 0; i < this.examArr.length; i++) {
            this.examArr[i].examObj.hideAnswer();
        }
    }
    this.showAnswer = status;
};

AnswerResult.prototype.getExamList = function (status) {
    var param = {};
    // param.mode = status;
    param.lessonTaskId = lessonTaskId;
    param.lessonTaskStudentId = lessonTaskStudentId;
    LearningGuideHandler.getStudentAnswerDetail(param, this, this.getDataCallBack);
};

AnswerResult.prototype.getDataCallBack = function (flag, data) {
    if (flag) {
        this.view.removeClass('dn');
        $('.empty-view').addClass('dn');
        this.data = data.result.data.examAnswerList;
        this.studentTaskVo = data.result.data.studentTaskVo;
        this.answerViewOpen = data.result.data.answerViewOpen;
        // this.answerViewOpen = 0;
        this.initExamView();
        this.initInfoView();
    } else {
        this.view.addClass('dn');
        $('.empty-view').removeClass('dn');
    }
};

AnswerResult.prototype.onlyShowFalseExam = function (status) {
    this.getExamList();
    if (status == 1) {
        this.initFalseExam();
    } else {
        this.initExamView(1);
    }
};

AnswerResult.prototype.initExamView = function (type) {
    this.examArr = [];
    this.examCon.empty();
    var examCount = 1;
    for (var i = 0; i < this.data.length; i++) {
        switch (this.data[i].type) {
            case 1:
                var temp = new ExamItem(this, this.data[i], examCount);
                this.examArr.push(temp);
                this.examCon.append(temp.view);
                examCount++;
                break;
            case 2:
                var tempRes = new ResReusltItem(this, this.data[i]);
                this.examCon.append(tempRes.view);
                break;
            case 3:
                var tempStr = this.checkStr(this.data[i].resourceText);
                this.examCon.append(tempStr);
                break;
            default:
                console.log('不能判断数据类型');
        }
    }
    if (this.showAnswer) {
        for (var i = 0; i < this.examArr.length; i++) {
            this.examArr[i].examObj.showAnswer();
        }
    } else {
    }
    if (type != 1) {
        this.setAnswerStatus();
    }
};

AnswerResult.prototype.checkStr = function (str) {
    if (str == '<p></p>' || str == '<p><br/></p>' || str == '<br/>' || str == '<p><br/></p><p><br/></p>') {
        return '';
    } else {
        return str;
    }
};

AnswerResult.prototype.initFalseExam = function () {
    this.examArr = [];
    this.examCon.empty();
    var falseExamCount = 1;
    for (var i = 0; i < this.data.length; i++) {
        if ((this.data[i].type == 1 && this.data[i].isRight == 2) || this.data[i].type == 1 && this.data[i].isRight == 3 || this.data[i].type == 1 && this.data[i].isAnswer == 0) {
            var temp = new ExamItem(this, this.data[i], falseExamCount);
            this.examArr.push(temp);
            this.examCon.append(temp.view);
            falseExamCount++;
            if (this.showAnswer) {
                temp.examObj.showAnswer();
            } else {
            }
        } else {
        }
    }
    // this.changeAnswerStatus();
};

AnswerResult.prototype.initInfoView = function () {
    this.myScoreView.text(this.studentTaskVo.scoreTotal < 0 ? "--" : this.studentTaskVo.scoreTotal + '分');
    this.allScoreView.text(this.studentTaskVo.score < 0 ? "--" : this.studentTaskVo.score + '分');
    this.averageScoreView.text(this.studentTaskVo.scoreAvg < 0 ? "--" : this.studentTaskVo.scoreAvg + '分');
    // this.timeView.text((this.studentTaskVo.useTime?this.studentTaskVo.useTime:'获取失败'));
    this.timeView.text(formatSeconds(this.studentTaskVo.useTime));
    if (this.studentTaskVo.correctType == 2) {
        this.paperTypeView.removeClass('dn');
        this.paperTypeView.find('[sid=paper-pic]').addClass('bad');
        this.paperTypeView.find('[sid=paper-text]').text('粗糙卷面');
    } else if (this.studentTaskVo.correctType == 1) {
        this.paperTypeView.removeClass('dn');
        this.paperTypeView.find('[sid=paper-pic]').removeClass('bad');
        this.paperTypeView.find('[sid=paper-text]').text('优秀卷面');
    } else {
        this.paperTypeView.addClass('dn');
    }
};
