/**
 * Created by zhangxia on 2018/3/6.
 */
var ExamItem = function (parentObj,data,index) {
  this.parentObj = parentObj;
  this.data = data;
  this.index = index;
  this.collFlag = this.data.isCollect;
  this.init();
};

ExamItem.prototype.init = function () {
  this.view = $(ExamItem.tempalte).clone();
  this.collView =this.view.find('[sid=ans-e-a-coll]');
  this.collView.off('click').on('click',this,this.collectFun);
  this.examView = this.view.find('[sid=exam-i-exam]');
  this.initExam();
  this.updateCollect();
};

ExamItem.prototype.initExam = function () {
  this.examObj = new ExamAnswerPreview(this.data,this.data.examSeq);
  this.examView.append(this.examObj.view);
};

ExamItem.prototype.collectFun = function (e) {
  var self = e.data;
  if(!self.collFlag){
    LearningGuideHandler.examCollectAdd({'id':self.data.id},self,self.collectCallBack);
  }else{
    LearningGuideHandler.examCollectDelete({'studentBookId':self.data.studentBookId},self,self.collectCallBack);
  }
};

ExamItem.prototype.collectCallBack = function (flag,data) {
  if(flag){
    this.collFlag = !this.collFlag;
    this.updateCollect();
    if(this.collFlag){
      this.data.studentBookId = data.result.data;
      addCollectSuccess('收藏成功！');
    }else{
      deleteCollectSuccess('取消收藏成功！');
    }
  }else{
    collectError(data.msg);
  }
};

ExamItem.prototype.updateCollect = function () {
  if(this.collFlag){
    this.collView.addClass('active');
  }else{
    this.collView.removeClass('active');
  }
};

ExamItem.tempalte = $('<div class="ans-e-exam box-s"><div class="ans-e-a-coll" sid="ans-e-a-coll" ></div><div class="ans-e-a-r" sid="exam-i-exam"></div></div>');

