var host,
  access_token,
  refresh_token,
  accountId,
  answerTime,
  teacherId,
  studentId,
  levelId,
  chapterName,//章节名称
  chapterId,//章节ID
  baseSubjectId,//学科ID
  status,//场景 全部不传值 只看待完成传 2
  lessonName,
  lessonId,
  model,
  page,
  endTime,
  writeBook,
  rows;
var requestArr = [];
var editIdArray = [];


function doAjax(options) {

  if (!options.dataType) {
    options.dataType = 'json';
  }
  if (!options.data) {
    options.data = {};
  }

  if (!options.timeout) {
    options.timeout = 10000;
  }

  options.url = host + options.url;
  options.data.access_token = access_token;

  var successFun = options.success;
  options.success = function (data) {
    if (data.code == 20005) {
      console.log('token失效  去刷新token,token='+access_token);
      var tempOption = {};
      tempOption.key = JSON.stringify(checkArray(new Date().getTime()));
      tempOption.value = options;
      requestArr.push(tempOption);
      mobileRefreshTokenFun(access_token,tempOption.key);
      return;
    }
    successFun(data);
  };

  var errorFun = options.error;
  options.error = function (data) {
    errorFun(data);
  };
  if (!options.beforeSend) {
    options.beforeSend = function () {
//    console.log("11111111111111111111111111");
//      showLoading();
    };
  }
  if (!options.complete) {
       options.complete = function (XMLHttpRequest,status) {
         hideLoading();
         if(status=='timeout'){//超时,status还有success,error等值的情况
           html5SendSystemInfoToNative("网络情况不佳，提交超时，请重新操作！");
         }
       };
     }
  options.async = true;
  options.crossDomain = true;
  $.ajax(options);
}

function getUrlParam(name) {

  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
  var r = window.location.search.substr(1).match(reg); // 匹配目标参数
  if (r != null) {
    return unescape(r[2]);
  }
  return null; // 返回参数值
}

function formatDate (dateTime, fmt) {
  const date = new Date(dateTime)
  const o = {
    'M+': date.getMonth() + 1, // 月份
    'd+': date.getDate(), // 日
    'h+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
  }
  return fmt
}

var learningGuide,
  collectionExam,
  errorExam;

//初始化调用获取所有需要参数
function getAllParams (options) {
  options = $.extend({
    token: ""
  }, options);
  host = options.host ? options.host:'';
  access_token =  options.access_token ? options.access_token:'';
  refresh_token =  options.refresh_token ? options.refresh_token:'';
  accountId = options.accountId ? options.accountId:'';
  lessonId = options.lessonId ? options.lessonId:'';
  lessonName = options.lessonName ? options.lessonName:'';
  answerTime = options.answerTime ? options.answerTime:'';
  lessonTaskId = options.lessonTaskId ? options.lessonTaskId:'';
  lessonTaskStudentId = options.lessonTaskStudentId ? options.lessonTaskStudentId:'';
  userStudentId = options.userStudentId ? options.userStudentId:'';
  userStudentName = options.userStudentName ? options.userStudentName:'';
  chapterId =  options.chapterId ? options.chapterId:'';
  baseSubjectId = options.baseSubjectId ? options.baseSubjectId:'';
  status = options.status ? options.status:'';
  model = options.model ? options.model:'';
  page =  options.page ? options.page: 1;
  rows = options.rows ? options.rows: 30;
  examTemplateStyleId = options.examTemplateStyleId ? options.examTemplateStyleId:'';
  timeType = options.timeType ? options.timeType:'';
  count = options.count ? options.count:'';
  endTime = options.endTime ? options.endTime:'';
  chapterName = options.chapterName ? options.chapterName:'';
  writeBook = options.writeBook ? options.writeBook:'';
}

function set_focus(el) {
  var range = document.createRange();
  range.selectNodeContents(el);
  range.collapse(false);
  var sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
}

//更新综合题答题进度（前端）
function updateComStatus () {
  for(var i=0;i<learningGuide.examArr.length;i++){
    if(JSON.parse(learningGuide.examArr[i].allData.examText).examTypeId == 16){
      learningGuide.examArr[i].answerCallBack();
    }
  }
}

function formatSeconds(value) {
  var theTime = parseInt(value);// 秒
  var theTime1 = 0;// 分
  var theTime2 = 0;// 小时
// alert(theTime);
  if(theTime > 59) {
    theTime1 = parseInt(theTime/60);
    theTime = parseInt(theTime%60);
// alert(theTime1+"-"+theTime);
    if(theTime1 > 59) {
      theTime2 = parseInt(theTime1/60);
      theTime1 = parseInt(theTime1%60);
    }
  }
  var result = "";
  if(10> theTime > 0) {
    result = "0"+parseInt(theTime);
  }else if(theTime >= 10){
    result = ""+parseInt(theTime);
  }else{
    result = "00";
  }
  if(10> theTime1 > 0) {
    result = "0"+parseInt(theTime1)+":"+result;
  }else if(theTime1 >= 10){
    result = ""+parseInt(theTime1)+":"+result;
  }else{
    result = "00:"+result;
  }
  if(10>theTime2 > 0) {
    result = "0"+parseInt(theTime2)+":"+result;
  }else if(theTime2 >= 10){
    result = ""+parseInt(theTime2)+":"+result;
  }else{
    result = "00:"+result;
  }
  return result;
}

function checkArray (id) {

  if(editIdArray.indexOf(id) > -1){
    //找到（不可用）
    return  checkArray(new Date().getTime());
  }else{
    //可用
    editIdArray.push(id);
    return id;
  }
};

function trimStr(str) {
  if(str){
    return $.trim(str);
  }else {
    return str;
  }
}