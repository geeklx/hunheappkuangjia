/**
 * Created by wjh on 2018/3/7.
 */
var SwitchSelect = function (view) {

    this.view = view;
    this.status = 0; //0:关闭  1：开启
    this.init();
};


SwitchSelect.prototype.init = function () {

    this.view.on('click', this,this.handleClick);
};

SwitchSelect.prototype.handleClick = function (e) {
    var self = e.data;
    if (self.status === 1) {
        self.setStatus(0);
    } else if (self.status === 0) {
        self.setStatus(1);
    }
    if (self.listener) {
        self.listenerFun.call(self.listener, self.status ,this);
    }
};

SwitchSelect.prototype.setStatus = function (status) {
    this.status = parseInt(status);
    if (parseInt(status) === 1) {
        this.view.addClass('active')
    } else if (parseInt(status) === 0) {
        this.view.removeClass('active')
    }
};

SwitchSelect.prototype.addListener = function (listener, listenerFun) {
    this.listener = listener;
    this.listenerFun = listenerFun;
};