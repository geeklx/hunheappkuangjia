/**
 * Created by zhangxia on 2018/2/27.
 */
var SelectOption = function (parentObj, data, index) {
  this.data = data;
  this.parentObj = parentObj;
  this.index = index;
  this.clickFlag = false;
  this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  this.serialNum3 = ["正确", "错误"];
  this.initType();
};

SelectOption.prototype.initType = function () {
  switch (this.data.examTypeId) {
    case 1:
      this.initSingleOption();
      break;
    case 2:
      this.initSingleOption();
      break;
    case 3:
      this.initJudgeOption();
      break;
    default :
      console.log('未找到匹配的选项类型，无法加载该选项');
  }
};

SelectOption.prototype.initSingleOption = function () {
  this.indexFlag = this.serialNum1[this.index];
  this.view = $(ExamPreViewTem.optionTemplate).clone();
  this.view.find('[sid=index]').text(this.indexFlag);
  this.view.find('[sid=stem]').html(this.data.content);
  this.view.find('[sid=index]').off('click').on('click', this, this.clickItem);
  this.setDefaultAnswer();
};

SelectOption.prototype.initJudgeOption = function () {
  this.indexFlag = this.serialNum3[this.index];
  this.view = $(ExamPreViewTem.optionJudgeTemplate).clone();
  this.view.find('[sid=stem]').text(this.indexFlag);
  this.view.find('[sid=index]').off('click').on('click', this, this.clickItem);
  this.setDefaultAnswer();
};

SelectOption.prototype.setDefaultAnswer = function () {
  // if (this.data) {
  //   if(this.type != 4){
  //     this.setContent();
  //   }
  //   if(this.optionData.right){
  //     this.template.find('[sid=cus-checkbox-exam]').trigger('click');
  //   }
  // }
};

SelectOption.prototype.clickItem = function (e) {
  var self = e.data;
  self.clickFlag = !self.clickFlag;
  if (self.listener) {
    self.listenerFun.call(self.listener, self, self.clickFlag);
  }
};

SelectOption.prototype.addListener = function (listener, listenerFun) {
  this.listener = listener;
  this.listenerFun = listenerFun;
};
