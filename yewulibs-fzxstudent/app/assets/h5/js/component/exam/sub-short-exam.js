/**
 * Created by zhangxia on 2018/3/1
 * 简答
 */
var SubShortExam = function (answerObj, data, index) {
  this.answerObj = answerObj;
  this.allData = data;
  this.answerData = data.examOptionList;
  this.data = JSON.parse(data.examText);
  this.index = index;
  this.answered = false;
  this.score = this.allData.score;
  this.view = $(ExamPreViewTem.boxTemplate).clone();
  this.bigStem = this.answerObj.data.examStem;
  this.smallStem =  JSON.parse(this.allData.examText).examStem;
  this.scanStem = this.bigStem + this.smallStem;
  this.imageStyle = localStorage.getItem("upToken");
  this.init();

};

SubShortExam.prototype.init = function () {
  this.introView = $(ExamPreViewTem.introTemplate).clone();
  this.stemView = $(ExamPreViewTem.stemTemplate).clone();
  this.answerView = $(SubShortExam.answerTemplate);
  this.stemView.html(this.data.examStem);
  this.introView.find('[sid=type]').text(this.allData.tiXingName);
  this.introView.find('[sid=index]').text("(" + this.index + ")");
  this.view.append(this.introView);
  this.view.append(this.stemView);
  this.view.append(this.answerView);
  if (!this.score || this.score <= 0) {
    this.introView.find('[cid=exam_score]').hide();
  } else {
    this.introView.find('[cid=exam_score]').show();
    this.introView.find('[cid=score_text]').text(this.score);
  }
  this.writeBtn = this.view.find('[sid=write-btn]');
  this.photoBtn = this.view.find('[sid=photo-btn]');
  this.menu = this.view.find('[sid=menu]');
  this.writeView = this.view.find('[sid=write]');
  this.photoView = this.view.find('[sid=photo]');
  this.addBtn = this.photoView.find('[sid=add-pic]');
  this.wDelBtn = this.writeView.find('[sid=del]');
  this.pDelBtn = this.photoView.find('[sid=del]');
  this.addBtn.on('click', this, this.addPicFun);
  this.writeBtn.on('click', this, this.writeFun);
  this.photoBtn.on('click', this, this.photoBtnFun);
  this.wDelBtn.on('click', this, this.delWFun);
  this.pDelBtn.on('click', this, this.delPFun);
  this.photoView.find('[sid=s-e-con]').attr('id', 'p' + this.allData.id);
  this.writeView.find('[sid=s-e-con]').attr('id', 'w' + this.allData.id);
  if (writeBook == 'hide') {
    this.view.find('.s-e-menu').addClass('no-write-book');
  }

};


SubShortExam.prototype.addPicFun = function (e) {
  var self = e.data;
  if (self.photoView.find('[sid=s-e-con]').find('img').length >= 4) {
    html5MorePic();
  } else {
    html5GetNativePic('p' + self.allData.id, '', self.allData.id, self.photoView.find('[sid=s-e-con]').find('img').length);
  }
};

SubShortExam.prototype.photoBtnFun = function (e) {
  var self = e.data;
  self.menu.addClass('dn');
  self.photoView.removeClass('dn');
  self.writeView.addClass('dn');
  if (self.photoView.find('[sid=s-e-con]').find('img').length >= 4) {
    html5MorePic();
  } else {
    html5GetNativePic('p' + self.allData.id, '', self.allData.id, self.photoView.find('[sid=s-e-con]').find('img').length);
  }
};

SubShortExam.prototype.writeFun = function (e) {
  var self = e.data;
  self.menu.addClass('dn');
  self.photoView.addClass('dn');
  self.writeView.removeClass('dn');
  self.answerObj.answerObj.updateNavData('w' + self.allData.id, '', lessonTaskId + self.answerObj.allData.id, self.answerObj.index + '(' + self.index + ')', self.answerObj.index + '' + self.index);
  html5GetNativeWrite('w' + self.allData.id, 'num' + self.allData.id, lessonTaskId + self.answerObj.allData.id + '', self.answerObj.index + '(' + self.index + ')', self.answerObj.index + self.index, self.scanStem);
};

SubShortExam.prototype.photoDel = function () {
  this.menu.removeClass('dn');
  this.photoView.addClass('dn');
  this.photoView.find('.imgUP').remove();
  this.answered = false;
  this.answerObj.answerCallBack();

};

SubShortExam.prototype.writeDel = function () {
  this.menu.removeClass('dn');
  this.writeView.addClass('dn');
  this.writeView.find('[sid=s-e-con]').empty().append($(SubShortExam.writeIngTemplate));
  this.answered = false;
  this.answerObj.answerCallBack();
  if (this.answerObj.answerObj.navData.data.id && (this.allData.id == this.answerObj.answerObj.navData.data.id.substring(1))) {
    this.answerObj.answerObj.updateNavData('', '', '', '', '');
  }
};

SubShortExam.prototype.delWFun = function (e) {
  var self = e.data;
  if (self.writeView.find('[sid=s-e-con]').find('img').length > 0) {
    delExamPop(self.allData.id, 2,lessonTaskId + self.allData.id,self.index);
  } else {
    self.menu.removeClass('dn');
    self.writeView.addClass('dn');
    self.writeView.find('[sid=s-e-con]').empty().append($(SubShortExam.writeIngTemplate));
    self.answered = false;
    self.answerObj.answerCallBack();
    self.answerObj.answerObj.updateNavData('', '', '', '', '');
    if (!learningGuide.navCon.hasClass('dn')) {
      learningGuide.navCon.addClass('dn');
      learningGuide.updateNavData('', '', '', '', '');
    }
  }
};

SubShortExam.prototype.delPFun = function (e) {
  var self = e.data;
  if (self.photoView.find('[sid=s-e-con]').find('img').length > 0) {
    delExamPop(self.allData.id, 1,lessonTaskId + self.allData.id,self.index);
  } else {
    self.menu.removeClass('dn');
    self.photoView.addClass('dn');
    self.photoView.find('.imgUP').remove();
    self.answered = false;
    self.answerObj.answerCallBack();
  }
};

// SubShortExam.prototype.initAnswerView = function () {
//   var that = this;
//   if (this.answerData && this.answerData.length > 0) {
//     if (this.answerData[0].seq == 1) {
//       this.writeView.find('[sid=s-e-con]').empty();
//       // var srcArr = this.answerData[0].myAnswer.split(',');
//         var srcArr1 = this.answerData[0].myAnswer.split(';')[0].split(',');
//         var srcArr = this.answerData[0].myAnswer.split(';')[1].split(',');
//     } else {
//       var srcArr1 = this.answerData[0].myAnswer.split(';')[0].split(',');
//       var srcArr = this.answerData[0].myAnswer.split(';')[1].split(',');
//     }
//     for (var i = 0; i < srcArr.length; i++) {
//         if (this.answerData[0].seq == 2) {
//             var img = $('<img class="imgUP1" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '" style="display: none">');
//             var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '"></div>');
//             imgContain.on("click", function () {
//                 html5SendPicToNative($(this).attr('sid'), 1, that.allData.id, $(this).index());
//             });
//             imgContain.css("background-image","url("+srcArr[i]+")");
//             imgContain.css("background-position","center");
//             imgContain.css("background-repeat","no-repeat");
//             imgContain.css("background-size","contain");
//             this.menu.addClass('dn');
//             this.photoView.removeClass('dn');
//             imgContain.append(img);
//             this.photoView.find('[sid=s-e-con]').append(imgContain);
//         } else {
//             var img = $('<img class="imgUP1" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '" style="display: none">');
//             var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '"></div>');
//             imgContain.on("click", function () {
//                 // steamAll ="";
//                 // steamAll = JSON.parse(that.allData.examText).examStem;
//                 html5EditWrite($(this).attr('src'), 'w' + that.allData.id, 'num' + that.allData.id, lessonTaskId + that.allData.id, that.index, that.index, JSON.parse(that.allData.examText).examStem);
//             });
//             imgContain.css("background-image","url("+srcArr[i]+")");
//             imgContain.css("background-position","center");
//             imgContain.css("background-repeat","no-repeat");
//             imgContain.css("background-size","contain");
//             this.menu.addClass('dn');
//             this.writeView.removeClass('dn');
//             imgContain.append(img);
//             this.writeView.find('[sid=s-e-con]').append(imgContain);
//
//         }
//     }
//     this.answered = true;
//     this.answerObj.answerCallBack();
//   }
// };
SubShortExam.prototype.initAnswerView = function () {
    var that = this;
    if (this.answerData && this.answerData.length > 0) {
        if (this.answerData[0].seq == 1) {
            this.writeView.find('[sid=s-e-con]').empty();
            var srcArr = this.answerData[0].myAnswer.split(',');
        } else {
            var srcArr = this.answerData[0].myAnswer.split(',');
        }
        for (var i = 0; i < srcArr.length; i++) {
            if (this.answerData[0].seq == 2) {
                var srcThn = srcArr[i] + "?" + that.imageStyle;
                var img = $('<img class="imgUP1" sid="' + srcArr[i] + '" src="' + srcThn +'">');
                var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr[i] + '" ></div>');
                imgContain.on("click", function () {
                    html5SendPicToNative($(this).attr('sid'), 1, that.allData.id, $(this).index());
                });
                //imgContain.css("background-image","url("+srcArr[i]+")");
                //imgContain.css("background-position","center");
                //imgContain.css("background-repeat","no-repeat");
                //imgContain.css("background-size","contain");
                this.menu.addClass('dn');
                this.photoView.removeClass('dn');
                imgContain.append(img);
                this.photoView.find('[sid=s-e-con]').append(imgContain);
            } else {
                var srcThn = srcArr[i] + "?" + that.imageStyle;
                var img = $('<img class="imgUP1" sid="' + srcArr[i] + '" src="' + srcThn +'">');
                var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr[i] + '" ></div>');
                imgContain.on("click", function () {
                    html5EditWrite($(this).attr('src'), 'w' + that.allData.id, 'num' + that.allData.id, lessonTaskId + that.allData.id, that.index, that.index, JSON.parse(that.allData.examText).examStem);
                });
                //imgContain.css("background-image","url("+srcArr[i]+")");
                //imgContain.css("background-position","center");
                //imgContain.css("background-repeat","no-repeat");
                //imgContain.css("background-size","contain");
                this.menu.addClass('dn');
                this.writeView.removeClass('dn');
                imgContain.append(img);
                this.writeView.find('[sid=s-e-con]').append(imgContain);

            }
        }
        this.answered = true;
        this.answerObj.answerCallBack();
    }
};

// SubShortExam.prototype.getValue = function () {
//   var param = {};
//   param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
//   param.lessonTaskDetailsId = this.allData.id;
//   param.lessonAnswerExamParentId = this.allData.lessonAnswerExamParentId;
//   param.templateId = this.allData.examTemplateId;
//   param.emptyCount = this.allData.examEmptyCount;
//   param.score = this.allData.score;
//   if (!this.answered) {
//     param.answerList = null;
//     return param;
//   }
//   param.answerList = [];
//   var t = {};
//   if (!this.writeView.hasClass('dn')) {
//     var imgArr = this.writeView.find('.imgUP');
//     t.seq = 1;//手写板
//   } else {
//     var imgArr = this.photoView.find('.imgUP');
//     t.seq = 2;//相册
//   }
//   t.myAnswerHtml = '';
//   t.isAnswer = true;
//   t.rightAnswer = '';
//   var srcArr = [];
//   var srcArr1 = [];
//   for (var i = 0; i < imgArr.length; i++) {
//     srcArr.push($(imgArr[i]).attr('src'));
//     if ($(imgArr[i]).attr('sid')) {
//       srcArr1.push($(imgArr[i]).attr('sid'));
//     } else {
//     }
//   }
//   if (srcArr1.length > 0) {
//     t.myAnswer = (srcArr1.join(',')) + ';' + (srcArr.join(','));
//   } else {
//     t.myAnswer = srcArr.join(',');
//   }
//   param.answerList.push(t);
//   return param;
// };
SubShortExam.prototype.getValue = function () {
    var param = {};
    param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
    param.lessonTaskDetailsId = this.allData.id;
    param.lessonAnswerExamParentId = this.allData.lessonAnswerExamParentId;
    param.templateId = this.allData.examTemplateId;
    param.emptyCount = this.allData.examEmptyCount;
    param.score = this.allData.score;
    if (!this.answered) {
        param.answerList = null;
        return param;
    }
    param.answerList = [];
    var t = {};
    if (!this.writeView.hasClass('dn')) {
        var imgArr = this.writeView.find('.imgUP');
        t.seq = 1;//手写板
    } else {
        var imgArr = this.photoView.find('.imgUP');
        t.seq = 2;//相册
    }
    t.myAnswerHtml = '';
    t.isAnswer = true;
    t.rightAnswer = '';
    var srcArr = [];
    for (var i = 0; i < imgArr.length; i++) {
        srcArr.push($(imgArr[i]).attr('sid'));
    }
    if (srcArr.length > 0) {
        t.myAnswer = srcArr.join(',');
    }
    param.answerList.push(t);
    return param;
};

SubShortExam.template = '<span class="black_filling" contenteditable></span>';

SubShortExam.answerTemplate = '<div class="short-exam-a box-s">' +
  '<div class="s-e-menu box-s" sid="menu">' +
  '<span class="s-e-text">在此作答</span>' +
  '<span class="write-btn" sid="write-btn"></span>' +
  '<span class="photo-btn" sid="photo-btn"></span>' +
  '</div>' +
  '<div class="s-e-type s-e-write box-s dn" sid="write">' +
  '<div class="s-e-type-t">' +
  '<span class="s-e-text">当前为手写笔作答</span>' +
  '<span class="s-e-del" sid="del"></span>' +
  '</div>' +
  '<div class="s-e-con clearfix" sid="s-e-con"><div class="short-writing"></div></div>' +
  '</div>' +
  '<div class="s-e-type box-s s-e-photo dn" sid="photo">' +
  '<div class="s-e-type-t">' +
  '<span class="s-e-text">当前为拍照作答</span>' +
  '<span class="s-e-del" sid="del"></span>' +
  '</div>' +
  '<div class="s-e-con clearfix" sid="s-e-con"><div class="add-photo box-s" sid="add-photo"><div class="add-pic" sid="add-pic"></div></div></div>' + '</div>' +
  '</div>' +
  '</div>';

SubShortExam.prototype.addListener = function (listener, listenerFun) {
  this.listener = listener;
  this.listenerFun = listenerFun;
};

SubShortExam.writeIngTemplate = '<div class="short-writing"></div>'