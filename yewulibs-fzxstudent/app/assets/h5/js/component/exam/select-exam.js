/**
 * Created by zhangxia on 2018/2/27.
 * 单选 多选 判断
 */

var SelectExam = function (answerObj, data) {
  this.answerObj = answerObj;
  this.allData = data;
  this.data = JSON.parse(data.examText);
  this.answerData = data.examOptionList;
  this.index = data.examSeq;
  this.collFlag = this.allData.isCollect;
  this.score = this.allData.score;
  this.finishFlag = false;
  this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-coll" sid="ans-e-a-coll"></div><div class="ans-e-a-r" sid="exam"></div></div>');
  this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-r" sid="exam"></div></div>');

  this.numView = $('<span class="ans-e-num-item" id="#' + this.allData.id + '">' + this.index + '</span>');
  this.init();
};

SelectExam.prototype.jumpExam = function () {
  var hr = $(this).attr("id");
  // console.log($(hr).offset().top+$(".ans-e-con").scrollTop());
  var anh = $(hr).offset().top + $(".ans-e-con").scrollTop() - 50;
  $(".ans-e-con").stop().animate({scrollTop: anh}, 200);
};

SelectExam.prototype.init = function () {
  // this.collView = this.view.find('[sid=ans-e-a-coll]');
  // this.collView.off('click').on('click', this, this.collectFun);
  var viewArr = this.initData();
  this.boxView = viewArr.boxView;
  this.boxView.append(viewArr.introView);
  this.boxView.append(viewArr.stemView);
  this.boxView.append(viewArr.optionConView);
  this.optionArr = [];
  for (var i = 0; i < this.data.examOptions.length; i++) {
    var tempData = this.data.examOptions[i];
    tempData.examTypeId = this.data.examTypeId;
    var option = new SelectOption(this, this.data.examOptions[i], i);
    if (this.data.examTypeId == 2) {
      //多选
      option.addListener(this, this.clickOptionCallBack2);
    } else {
      //单选，判断
      option.addListener(this, this.clickOptionCallBack1);
    }
    this.optionArr.push(option);
    viewArr.optionConView.append(option.view);
  }
  this.view.find('[sid=exam]').append(this.boxView);
  this.numView.on('click', this, this.jumpExam);
  if (!this.score || this.score <= 0) {
    viewArr.introView.find('[cid=exam_score]').hide();
  }
  else {
    viewArr.introView.find('[cid=exam_score]').show();
  }
  viewArr.introView.find('[cid=score_text]').text(this.score);
  // this.updateCollect();
  this.initAnswerView();
};

SelectExam.prototype.initData = function () {
  var viewArr = {};
  viewArr.boxView = $(ExamPreViewTem.boxTemplate).clone();
  viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
  viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
  viewArr.introView.find('[sid=index]').text(this.index + '.');
  viewArr.introView.find('[sid=type]').text(this.data.templateStyleName);
  viewArr.stemView.html(this.data.examStem);

  viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
  return viewArr;
};

//单选
SelectExam.prototype.clickOptionCallBack1 = function (obj, type) {
  for (var i = 0; i < this.optionArr.length; i++) {
    this.optionArr[i].clickFlag = false;
    this.optionArr[i].view.find('[sid=index]').removeClass('active');
  }
  if (type) {
    obj.clickFlag = true;
    obj.view.find('[sid=index]').addClass('active');
    this.numView.addClass('all');
    this.finishFlag = true;
  } else {
    this.numView.removeClass('all');
    this.finishFlag = false;
  }
};

//多选
SelectExam.prototype.clickOptionCallBack2 = function (obj, type) {
  var len = 0;
  if (type) {
    obj.clickFlag = true;
    obj.view.find('[sid=index]').addClass('active');
  } else {
    obj.clickFlag = false;
    obj.view.find('[sid=index]').removeClass('active');
  }
  for (var i = 0; i < this.optionArr.length; i++) {
    if (this.optionArr[i].clickFlag) {
      len++;
    }
  }
  if (len > 0) {
    this.numView.addClass('all');
  } else {
    this.numView.removeClass('all');
  }
  this.finishFlag = true;
};

SelectExam.prototype.collectFun = function (e) {
  var self = e.data;
  showLoading();
  if (!self.collFlag) {
    LearningGuideHandler.examCollectAdd({'id': self.allData.id}, self, self.collectCallBack);
  } else {
    LearningGuideHandler.examCollectDelete({'studentBookId': self.allData.studentBookId}, self, self.collectCallBack);
  }
};

SelectExam.prototype.collectCallBack = function (flag, data) {
  hideLoading();
  if (flag) {
    this.collFlag = !this.collFlag;
    this.updateCollect();
    if (this.collFlag) {
      this.allData.studentBookId = data.result.data;
      addCollectSuccess();
    } else {
      deleteCollectSuccess();
    }
  } else {
    collectError(data.msg);
  }
};

SelectExam.prototype.updateCollect = function () {
  if (this.collFlag) {
    this.collView.addClass('active');
  } else {
    this.collView.removeClass('active');
  }
};

SelectExam.prototype.addListener = function (listener, listenerFun) {
  this.listener = listener;
  this.listenerFun = listenerFun;
};

SelectExam.prototype.initAnswerView = function () {
  if (this.answerData && this.answerData.length > 0) {
    for (var i = 0; i < this.answerData.length; i++) {
      this.optionArr[this.answerData[i].seq - 1].view.find('[sid=index]').trigger('click');
    }
  }
};

SelectExam.prototype.getValue = function () {
  var param = {};
  param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
  param.lessonTaskDetailsId = this.allData.id;
  param.lessonAnswerExamParentId = this.allData.parentId;
  param.templateId = this.allData.examTemplateId;
  param.emptyCount = this.allData.examEmptyCount;
  param.score = this.allData.score;
  if ((!this.numView.hasClass('half')) && (!this.numView.hasClass('all'))) {
    param.answerList = null;
    return param;
  }
  param.answerList = [];
  for (var i = 0; i < this.optionArr.length; i++) {
    if (this.optionArr[i].clickFlag) {
      var t = {};
      t.seq = parseInt(i + 1);
      t.myAnswer = this.optionArr[i].indexFlag;
      t.myAnswerHtml = '';
      t.isAnswer = this.optionArr[i].data.right;
      t.rightAnswer = '';
      param.answerList.push(t);
    }
  }
  return param;
};

