/**
 * Created by zhangxia on 2018/2/28.
 * 简答
 */

var ShortExam = function (answerObj, data) {
    // alert("简答");
    this.answerObj = answerObj;
    this.allData = data;
    this.data = JSON.parse(data.examText);
    this.answerData = data.examOptionList;
    this.index = data.examSeq;
    this.collFlag = this.allData.isCollect;
    this.finishFlag = false;
    this.score = this.allData.score;
    // this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-coll" sid="ans-e-a-coll"></div><div class="ans-e-a-r" sid="exam"></div></div>');
    this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-r" sid="exam"></div></div>');
    this.numView = $('<span class="ans-e-num-item"><span class="jump-box" sid="jump-box" id="#' + this.allData.id + '">' + this.index + '</span></span>');
    this.imageStyle = localStorage.getItem("upToken");
    this.init();

};

ShortExam.prototype.jumpExam = function () {
    var hr = $(this).attr("id");
    // console.log($(hr).offset().top+$(".ans-e-con").scrollTop());
    var anh = $(hr).offset().top + $(".ans-e-con").scrollTop() - 50;
    $(".ans-e-con").stop().animate({scrollTop: anh}, 200);
};

ShortExam.prototype.init = function () {
    // this.collView = this.view.find('[sid=ans-e-a-coll]');
    // this.collView.off('click').on('click', this, this.collectFun);
    this.boxView = $(ExamPreViewTem.boxTemplate).clone();
    this.introView = $(ExamPreViewTem.introTemplate).clone();
    this.stemView = $(ExamPreViewTem.stemTemplate).clone();
    this.answerView = $(ShortExam.answerTemplate);
    this.stemView.html(this.data.examStem);
    this.introView.find('[sid=type]').text(this.data.templateStyleName);
    if (this.index) {
        this.introView.find('[sid=index]').text(this.index + '.');
    }

    if (!this.score || this.score <= 0) {
        this.introView.find('[cid=exam_score]').hide();
    }
    else {
        this.introView.find('[cid=exam_score]').show();
        this.introView.find('[cid=score_text]').text(this.score);
    }
    this.boxView.append(this.introView);
    this.boxView.append(this.stemView);
    this.boxView.append(this.answerView);
    this.view.find('[sid=exam]').append(this.boxView);
    this.writeBtn = this.view.find('[sid=write-btn]');
    this.photoBtn = this.view.find('[sid=photo-btn]');
    this.menu = this.view.find('[sid=menu]');
    this.writeView = this.view.find('[sid=write]');
    this.photoView = this.view.find('[sid=photo]');
    this.wDelBtn = this.writeView.find('[sid=del]');
    this.pDelBtn = this.photoView.find('[sid=del]');
    this.addBtn = this.photoView.find('[sid=add-pic]');
    this.writeBtn.on('click', this, this.writeFun);
    this.photoBtn.on('click', this, this.photoBtnFun);
    this.wDelBtn.on('click', this, this.delWFun);
    this.pDelBtn.on('click', this, this.delPFun);
    this.addBtn.on('click', this, this.addPicFun);
    this.photoView.find('[sid=s-e-con]').attr('id', 'p' + this.allData.id);
    this.writeView.find('[sid=s-e-con]').attr('id', 'w' + this.allData.id);
    this.numView.attr('id', 'num' + this.allData.id);
    this.numView.find('[sid=jump-box]').on('click', this, this.jumpExam);
    // this.updateCollect();
    this.initAnswerView();
    if (writeBook == 'hide') {
        this.view.find('.s-e-menu').addClass('no-write-book');
    }

};

ShortExam.prototype.addPicFun = function (e) {
    var self = e.data;
    if (self.photoView.find('[sid=s-e-con]').find('img').length >= 4) {
        html5MorePic();
    } else {
        html5GetNativePic('p' + self.allData.id, 'num' + self.allData.id, self.allData.id, self.photoView.find('[sid=s-e-con]').find('img').length);
    }
};

ShortExam.prototype.writeFun = function (e) {
    var self = e.data;
    self.menu.addClass('dn');
    self.photoView.addClass('dn');
    self.writeView.removeClass('dn');
    self.answerObj.updateNavData('w' + self.allData.id, 'num' + self.allData.id, lessonTaskId + self.allData.id, self.index, self.index);
    var s = JSON.parse(self.allData.examText).examStem;
    // steamAll = s;
    html5GetNativeWrite('w' + self.allData.id, 'num' + self.allData.id, lessonTaskId + self.allData.id, self.index, self.index, s);
};

ShortExam.prototype.photoBtnFun = function (e) {
    var self = e.data;
    self.menu.addClass('dn');
    self.photoView.removeClass('dn');
    self.writeView.addClass('dn');
    if (self.photoView.find('[sid=s-e-con]').find('img').length >= 4) {
        html5MorePic();
    } else {
        html5GetNativePic('p' + self.allData.id, 'num' + self.allData.id, self.allData.id, self.photoView.find('[sid=s-e-con]').find('img').length);
    }
};

ShortExam.prototype.writeDel = function () {
    this.menu.removeClass('dn');
    this.writeView.addClass('dn');
    this.writeView.find('[sid=s-e-con]').empty().append($(ShortExam.writeIngTemplate));
    this.finishFlag = false;
    this.numView.removeClass('all');
    if (this.answerObj.navData.data.id && (this.allData.id == this.answerObj.navData.data.id.substring(1))) {
        this.answerObj.updateNavData('', '', '', '', '');
    }
};

ShortExam.prototype.delWFun = function (e) {
    var self = e.data;
    if (self.writeView.find('[sid=s-e-con]').find('img').length > 0) {
        delExamPop(self.allData.id, 2, lessonTaskId + self.allData.id, self.index);
    } else {
        self.menu.removeClass('dn');
        self.writeView.addClass('dn');
        self.writeView.find('.imgUP').remove();
        self.finishFlag = false;
        self.numView.removeClass('all');
        if (!learningGuide.navCon.hasClass('dn')) {
            learningGuide.navCon.addClass('dn');
            learningGuide.updateNavData('', '', '', '', '');
        }
    }
};

ShortExam.prototype.delPFun = function (e) {
    var self = e.data;
    if (self.photoView.find('[sid=s-e-con]').find('img').length > 0) {
        delExamPop(self.allData.id, 1, lessonTaskId + self.allData.id, self.index);
    } else {
        self.menu.removeClass('dn');
        self.photoView.addClass('dn');
        self.photoView.find('.imgUP').remove();
        self.finishFlag = false;
        self.numView.removeClass('all');
    }
};

ShortExam.prototype.photoDel = function () {
    this.menu.removeClass('dn');
    this.photoView.addClass('dn');
    this.photoView.find('.imgUP').remove();
    this.finishFlag = false;
    this.numView.removeClass('all');
};

ShortExam.prototype.collectFun = function (e) {
    var self = e.data;
    if (!self.collFlag) {
        LearningGuideHandler.examCollectAdd({'id': self.allData.id}, self, self.collectCallBack);
    } else {
        LearningGuideHandler.examCollectDelete({'studentBookId': self.allData.studentBookId}, self, self.collectCallBack);
    }
};

ShortExam.prototype.collectCallBack = function (flag, data) {
    hideLoading();
    if (flag) {
        this.collFlag = !this.collFlag;
        this.updateCollect();
        if (this.collFlag) {
            this.allData.studentBookId = data.result.data;
            addCollectSuccess();
        } else {
            a
            deleteCollectSuccess();
        }
    } else {
        collectError(data.msg);
    }
};

ShortExam.prototype.updateCollect = function () {
    if (this.collFlag) {
        this.collView.addClass('active');
    } else {
        this.collView.removeClass('active');
    }
};

ShortExam.prototype.initAnswerView = function () {
    var that = this;
    if (this.answerData && this.answerData.length > 0) {

        if (this.answerData[0].seq == 1) {

            this.writeView.find('[sid=s-e-con]').empty();
            // var srcArr = this.answerData[0].myAnswer.split(',');
            //   var srcArr1 = this.answerData[0].myAnswer.split(';')[0].split(',');  //大图
            //   var srcArr = this.answerData[0].myAnswer.split(';')[1].split(',');   //小图
            var srcArr = this.answerData[0].myAnswer.split(',');
        } else {

            // var srcArr1 = this.answerData[0].myAnswer.split(';')[0].split(',');
            // var srcArr = this.answerData[0].myAnswer.split(';')[1].split(',');
            var srcArr = this.answerData[0].myAnswer.split(',');
        }
        for (var i = 0; i < srcArr.length; i++) {
            if (this.answerData[0].seq == 2) {
                // var img = $('<img class="imgUP1" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '" style="display: none">');
                // var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '"></div>');\

                var srcThn = srcArr[i] + "?" + that.imageStyle;
                var img = $('<img class="imgUP1" sid="' + srcArr[i] + '" src="' + srcThn +'">');
                var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr[i] + '" ></div>');
                img.on("click", function () {
                    html5SendPicToNative($(this).attr('sid'), 1, that.allData.id, $(this).index());
                });
                this.menu.addClass('dn');
                this.photoView.removeClass('dn');
                imgContain.append(img);
                this.photoView.find('[sid=s-e-con]').append(imgContain);
            } else {
                // var img = $('<img class="imgUP1" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '" style="display: none">');
                // var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '"></div>');

                var srcThn = srcArr[i] + "?" + that.imageStyle;
                var img = $('<img class="imgUP1" sid="' + srcArr[i] + '" src="' + srcThn +'">');
                var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr[i] + '" ></div>');
                img.on("click", function () {
                    html5EditWrite($(this).attr('src'), 'w' + that.allData.id, 'num' + that.allData.id, lessonTaskId + that.allData.id, that.index, that.index, JSON.parse(that.allData.examText).examStem);
                });
                //imgContain.css("background-image","url("+srcArr[i]+")");
                //imgContain.css("background-position","center");
                //imgContain.css("background-repeat","no-repeat");
                //imgContain.css("background-size","contain");
                this.menu.addClass('dn');
                this.writeView.removeClass('dn');
                imgContain.append(img);
                this.writeView.find('[sid=s-e-con]').append(imgContain);

            }
        }
        this.finishFlag = true;
        this.numView.addClass('all');


    }
};


// ShortExam.prototype.getValue = function () {
//   var param = {};
//   param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
//   param.lessonTaskDetailsId = this.allData.id;
//   param.lessonAnswerExamParentId = this.allData.parentId;
//   param.templateId = this.allData.examTemplateId;\
//   param.emptyCount = this.allData.examEmptyCount;
//   param.score = this.allData.score;
//   if ((!this.numView.hasClass('half')) && (!this.numView.hasClass('all'))) {
//     param.answerList = null;
//     return param;
//   }
//   param.answerList = [];
//   var t = {};
//   if (!this.writeView.hasClass('dn')) {
//     var imgArr = this.writeView.find('[sid=s-e-con]').find('img');
//     t.seq = 1;//手写板
//   } else {
//     var imgArr = this.photoView.find('[sid=s-e-con]').find('img');
//     t.seq = 2;//相册
//   }
//   t.myAnswerHtml = '';
//   t.isAnswer = true;
//   t.rightAnswer = '';
//   var srcArr = [];
//   var srcArr1 = [];
//   for (var i = 0; i < imgArr.length; i++) {
//     srcArr.push($(imgArr[i]).attr('src'));
//     if ($(imgArr[i]).attr('sid')) {
//       srcArr1.push($(imgArr[i]).attr('sid'));
//     } else {
//     }
//   }
//   if (srcArr1.length > 0) {
//     t.myAnswer = (srcArr1.join(',')) + ';' + (srcArr.join(','));
//   } else {
//     // t.myAnswer = srcArr.join(',');
//      console.log("出错了");
//   }
//
//   param.answerList.push(t);
//   return param;
// };
ShortExam.prototype.getValue = function () {
    var param = {};
    param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
    param.lessonTaskDetailsId = this.allData.id;
    param.lessonAnswerExamParentId = this.allData.parentId;
    param.templateId = this.allData.examTemplateId;
    param.emptyCount = this.allData.examEmptyCount;
    param.score = this.allData.score;
    if ((!this.numView.hasClass('half')) && (!this.numView.hasClass('all'))) {
        param.answerList = null;
        return param;
    }
    param.answerList = [];
    var t = {};
    if (!this.writeView.hasClass('dn')) {
        var imgArr = this.writeView.find('[sid=s-e-con]').find('img');
        t.seq = 1;//手写板
    } else {
        var imgArr = this.photoView.find('[sid=s-e-con]').find('img');
        t.seq = 2;//相册
    }
    t.myAnswerHtml = '';
    t.isAnswer = true;
    t.rightAnswer = '';
    var srcArr = [];
    for (var i = 0; i < imgArr.length; i++) {
        srcArr.push($(imgArr[i]).attr('sid'));
    }
    if (srcArr.length > 0) {
        t.myAnswer = srcArr.join(',');
    }
    param.answerList.push(t);
    return param;
};


ShortExam.template = '<span class="black_filling" contenteditable></span>';

ShortExam.answerTemplate = '<div class="short-exam-a box-s">' +
    '<div class="s-e-menu box-s" sid="menu">' +
    '<span class="s-e-text">在此作答</span>' +
    '<span class="write-btn" sid="write-btn"></span>' +
    '<span class="photo-btn" sid="photo-btn"></span>' +
    '</div>' +
    '<div class="s-e-type s-e-write box-s dn" sid="write">' +
    '<div class="s-e-type-t">' +
    '<span class="s-e-text">当前为手写笔作答</span>' +
    '<span class="s-e-del" sid="del"></span>' +
    '</div>' +
    '<div class="s-e-con clearfix" sid="s-e-con"><div class="short-writing"></div></div>' +
    '</div>' +
    '<div class="s-e-type box-s s-e-photo dn" sid="photo">' +
    '<div class="s-e-type-t">' +
    '<span class="s-e-text">当前为拍照作答</span>' +
    '<span class="s-e-del" sid="del"></span>' +
    '</div>' +
    '<div class="s-e-con clearfix" sid="s-e-con"><div class="add-photo box-s" sid="add-photo"><div class="add-pic" sid="add-pic"></div></div></div>' + '</div>' +
    '</div>' +
    '</div>';

ShortExam.prototype.addListener = function (listener, listenerFun) {
    this.listener = listener;
    this.listenerFun = listenerFun;
};

ShortExam.writeIngTemplate = '<div class="short-writing"></div>'