/**
 * Created by zhangxia on 2018/3/6.
 */
/**
 * Created by zhangxia on 2018/2/27.
 */
var ResReusltItem = function (parentObj, data) {
    console.log(JSON.stringify(data));
    this.parentObj = parentObj;
    this.resData = data;
    this.data = JSON.parse(this.resData.resourceText);
    this.collFlag = true;
    this.isRead = 0;
    this.useTime = 0;
    this.init();
};

ResReusltItem.prototype.init = function () {
    this.view = $(ResReusltItem.resBoxTempalte).clone();
    this.resView = $(ResReusltItem.resTempalte).clone();
     this.collView = this.view.find('[sid=ans-e-a-coll]');
     this.collView.off('click').on('click', this, this.collectFun);
    this.resView.find('[sid=name]').text(this.data.resourceName);
    this.resView.find('[sid=name]').attr('title', this.data.resourceName);
    this.picView = this.resView.find('[sid=pic]');
    this.initType(this.data, this.picView);
    this.errorView = this.resView.find('[sid=error]');
    if (this.data.convertStatus == 3) {
        this.errorView.addClass('error')
    } else if (this.data.convertStatus == 1) {
        this.errorView.addClass('ing')
    }
    this.view.find('[sid=res]').append(this.resView);
    this.view.find('[sid=res]').find('[sid=inner-res]').on('click', this.scanRes.BindView(this));
     this.updateCollect();
};

ResReusltItem.prototype.initType = function (data, view) {
    switch (data.resourceType) {
        case 5:
            view.addClass('music');
            break;
        case 4:
            view.addClass('pic');
            break;
        case 3:
            view.addClass('mp4');
            break;
        case 2:
            view.addClass('pdf');
            break;
        case 6:
            view.addClass('real-pdf');
            break;
    }
    if (data.resourceType == 1) {
        if (data.resourceSuffix == ".txt") {
            view.addClass("txt");
        } else {
            view.addClass('word');
        }
    }
};

ResReusltItem.prototype.collectFun = function (e) {
    var self = e.data;
    // showLoading();
    if (!self.collFlag) {
    console.log("1  "  +self.collFlag);
        LearningGuideHandler.resCollectAdd({
            'id': self.resData.id,
            'lessonId': lessonId,
            'lessonName': lessonName
        }, self, self.collectCallBack);
    } else {
     console.log("2  "  +self.collFlag);
       LearningGuideHandler.resCollectDelete({'resourceCollectId': self.resData.studentBookId}, self, self.collectCallBack);
    }
};

ResReusltItem.prototype.scanRes = function () {
    this.isRead = 1;
    html5ScanResource(this.resData);
};

ResReusltItem.prototype.collectCallBack = function (flag, data) {
    // hideLoading();
    if (flag) {
        this.collFlag = !this.collFlag;
        this.updateCollect();
        if (this.collFlag) {
            this.resData.studentBookId = data.result.data;
            addCollectSuccess('收藏成功！');
        } else {
            deleteCollectSuccess('取消收藏成功！');
        }
    } else {
        collectError(data.msg);
    }
};

ResReusltItem.prototype.updateCollect = function () {
    if (this.collFlag) {
        this.collView.addClass('active');
    } else {
        this.collView.removeClass('active');
    }
};

ResReusltItem.prototype.getValue = function () {
    if (this.isRead) {
        var param = {};
        param.lessonAnswerExamId = this.resData.lessonAnswerExamId;
        param.lessonTaskDetailsId = this.resData.id;
        param.lessonAnswerExamParentId = this.resData.parentId;
        param.templateId = null;
        param.emptyCount = null;
        param.answerList = null;
        param.type = 2;
        param.isRead = this.isRead;
        param.useTime = this.useTime;
        param.score = this.resData.score;
        return param;
    } else {
        return false;
    }
};

// ResReusltItem.resBoxTempalte = $('<div class="ans-e-exam ans-e-res box-s"><div class="ans-e-a-coll coll-res" sid="ans-e-a-coll" ></div><div class="ans-e-a-r" sid="res"></div></div>');
ResReusltItem.resBoxTempalte = $('<div class="ans-e-exam ans-e-res box-s"><div class="ans-e-a-coll coll-res" sid="ans-e-a-coll" ></div><div class="ans-e-a-r" sid="res"></div></div>');

ResReusltItem.resTempalte = '<div><div class="inner-res clearfix" sid="inner-res">' +
    '    <span class="back" sid="pic"></span>' +
    '    <span class="name" sid="name"></span>' +
    '    <span class="" sid="error"></span>' +
    '</div></div>';