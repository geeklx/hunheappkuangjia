/**
 * Created by zhangxia on 2018/1/31.
 */
var ExamPreViewTem = function () {
};

ExamPreViewTem.boxTemplate = '<div class="exam-scan"></div>';

ExamPreViewTem.introTemplate = '<div class="exam-scan-intro">' +
    '                             <span class="exam-flag" sid="index"></span>' +
    '                             <span class="exam-score" cid="exam_score">(本题<span cid="score_text">满</span>分)</span>' +
    '                             <span class="exam-i" sid="type"></span>' +
    '                         </div>';

ExamPreViewTem.stemTemplate = '<div class="exam-scan-stem clearfix" sid="exam-scan-stem" style="word-wrap:break-word;"></div>';

ExamPreViewTem.optionConTemplate = '<div class="exam-scan-option" sid="exam-scan-option"></div>';

ExamPreViewTem.optionConResultTemplate = '<div class="exam-scan-option" sid="exam-scan-option" style="padding-left: initial;padding-right: initial"></div>';

ExamPreViewTem.clozeAnswerTemplate = '<div class="scan-cloze-answer">' +
  '                                      <div class="scan-cloze-flag" sid="flag"></div>' +
  '                                      <div class="scan-cloze-option"></div>' +
  '                                 </div>';


ExamPreViewTem.clozeAnswerResultTemplate = '<div class="scan-cloze-answer" style="padding-left: initial">' +
  '                                      <div class="scan-cloze-flag" sid="flag"></div>' +
  '                                      <div class="scan-cloze-option"></div>' +
  '                                 </div>';


ExamPreViewTem.optionAllConTemp = '<div class="exam-scan-small-box" sid="exam-scan-small-box"></div>';

ExamPreViewTem.optionJudgeConTem = '<div class="exam-scan-option" sid="exam-scan-option">' +
    '                            <div class="exam-scan-o-li">' +
    '                                <span class="exam-tip judge-tip" sid="index"></span>' +
    '                                <span class="exam-text" sid="stem">正确</span>' +
    '                            </div>' +
    '                            <div class="exam-scan-o-li">' +
    '                                <span class="exam-tip judge-tip" sid="index"></span>' +
    '                                <span class="exam-text" sid="stem">错误</span>' +
    '                            </div>' +
    '                        </div>';

ExamPreViewTem.optionJudgeTemplate = '<div class="exam-scan-o-li">' +
    '                                <span class="exam-tip judge-tip" sid="index"></span>' +
    '                                <span class="exam-text" sid="stem">正确</span>' +
    '                            </div>';

ExamPreViewTem.optionTemplate = ' <div class="exam-scan-o-li">' +
    '                                 <span class="exam-tip" sid="index">A.</span>' +
    '                                 <span class="exam-text" sid="stem">若若方程组的解的解满足</span>' +
    '                             </div>';

ExamPreViewTem.amswerTemplate = '<div class="exam-scan-h">' +
    '                             <span class="exam-ana ans-left-has-p">[答案]</span>' +
    '                             <span class="exam-text clearfix" sid="exam-scan-answer"></span>' +
    '                         </div>';

ExamPreViewTem.amswerTemplate1 = '<div class="exam-scan-hh">' +
    '                             <span class="exam-ana">[作答]</span>' +
    '                           <span cid="xscore" class="txts" style="display: none;"></span>' +
    '                            <div class="ans-r-info-btn pi-jp-item" style="float:right;margin: 0px;" >' +
    '                                   <div class="pi-jp-switch" cid="radio">' +
    '                                        <i class="switch-icon"></i>' +
    '                                   </div>' +
    '                                   <span class="pi-jp-label"style="float: right;font-size: 14px;color: #808FA3;" >批改痕迹</span>' +
    '                             </div>' +
    '                             <div class="exam-text clearfix" sid="exam-scan-answer-s"><span class="flag" sid="flag"></span></div>' +
    '                             <div class="exam-text clearfix" sid="exam-scan-answer-h" style="display:none;"><span class="flag" sid="flag"></span></div>' +

    '                         </div>';

ExamPreViewTem.analyzeTemplate = '<div class="exam-scan-h">' +
    '                             <span class="exam-ana">[解析]</span>' +
    '                             <span class="exam-text clearfix" sid="exam-scan-analyse">有理数的加减混合运算</span>' +
    '                         </div>';

ExamPreViewTem.openAnswer = '<div class="openAnswer" cid="openAnswer"></div>';

ExamPreViewTem.clozeOptionItemTemplate = '<div class="scan-cloze-item"><span class="scan-cloze-item-index" sid="index"></span><span class="scan-cloze-content"></span></div>';

ExamPreViewTem.clozeOptionItemResultTemplate = '<div class="scan-cloze-item"><span class="scan-cloze-item-index" sid="index"></span><span class="scan-cloze-content"></span></div>';


