package com.example.app1home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app1home.R;
import com.example.app1home.presenter.MainPresenter;
import com.example.app1home.view.MainView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.baseui.activity.AnswerNativeActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.AnswerResultNativeActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.ErrorNativeActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.TodayNativeErrorActivity;
import com.sdzn.fzx.student.libbase.comparator.RightRateComparator;
import com.sdzn.fzx.student.libbase.comparator.WrongComparator;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libbase.listener.OnSubjectClickListener;
import com.sdzn.fzx.student.libbase.pop.SubjectPop;
import com.sdzn.fzx.student.libpublic.event.RefreshTaskEvent;
import com.sdzn.fzx.student.libpublic.event.ToTaskEvent;
import com.sdzn.fzx.student.libpublic.views.ChartLegendView;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.views.GraphView;
import com.sdzn.fzx.student.libpublic.views.MainFragmentTitleView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.RightRateVo;
import com.sdzn.fzx.student.vo.SubjectVo;
import com.sdzn.fzx.student.vo.TaskListVo;
import com.sdzn.fzx.student.vo.WrongVo;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.view.annotation.Event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 * 首页
 *
 * @author wangchunxiao
 * @date 2018/1/8
 */
public class MainFragment extends MBaseFragment<MainPresenter> implements MainView, OnRefreshListener, View.OnClickListener  {
    //CustomAdapt
    private SmartRefreshLayout refreshLayout;
    private LinearLayout swipeTarget;
    private MainFragmentTitleView mftvUnF;
    private EmptyRecyclerView rvUnF;
    private LinearLayout llUnfEmpty;
    private ImageView ivUnfEmpty;
    private TextView tvUnfEmpty;
    private MainFragmentTitleView mftvF;
    private EmptyRecyclerView rvF;
    private LinearLayout llFEmpty;
    private ImageView ivFEmpty;
    private TextView tvFEmpty;
    private MainFragmentTitleView mftvW;
    private PieChart mPieChart;
    private TextView tvWCount;
    private ChartLegendView clv1;
    private ChartLegendView clv3;
    private ChartLegendView clv2;
    private ChartLegendView clv4;
    private TextView tvSubject;
    private GraphView graphView;

    private Y_ItemEntityList itemEntityListUnf = new Y_ItemEntityList();
    private Y_ItemEntityList itemEntityListF = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter unfAdapter;
    private Y_MultiRecyclerAdapter fAdapter;

    private List<MainPresenter.MyCountDownTimer> downTimers = new ArrayList<>();

    private List<SubjectVo.DataBean> subjects = new ArrayList<>();

    private SubjectPop subjectPop;

    public static MainFragment newInstance(Bundle bundle) {
        MainFragment fragment = new MainFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new MainPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        swipeTarget = (LinearLayout) rootView.findViewById(R.id.swipe_target);
        mftvUnF = (MainFragmentTitleView) rootView.findViewById(R.id.mftvUnF);
        rvUnF = (EmptyRecyclerView) rootView.findViewById(R.id.rvUnF);
        llUnfEmpty = (LinearLayout) rootView.findViewById(R.id.llUnfEmpty);
        ivUnfEmpty = (ImageView) rootView.findViewById(R.id.ivUnfEmpty);
        tvUnfEmpty = (TextView) rootView.findViewById(R.id.tvUnfEmpty);
        mftvF = (MainFragmentTitleView) rootView.findViewById(R.id.mftvF);
        rvF = (EmptyRecyclerView) rootView.findViewById(R.id.rvF);
        llFEmpty = (LinearLayout) rootView.findViewById(R.id.llFEmpty);
        ivFEmpty = (ImageView) rootView.findViewById(R.id.ivFEmpty);
        tvFEmpty = (TextView) rootView.findViewById(R.id.tvFEmpty);
        mftvW = (MainFragmentTitleView) rootView.findViewById(R.id.mftvW);
        mPieChart = (PieChart) rootView.findViewById(R.id.piechart);
        tvWCount = (TextView) rootView.findViewById(R.id.tvWCount);
        clv1 = (ChartLegendView) rootView.findViewById(R.id.clv1);
        clv3 = (ChartLegendView) rootView.findViewById(R.id.clv3);
        clv2 = (ChartLegendView) rootView.findViewById(R.id.clv2);
        clv4 = (ChartLegendView) rootView.findViewById(R.id.clv4);
        tvSubject = (TextView) rootView.findViewById(R.id.tvSubject);
        graphView = (GraphView) rootView.findViewById(R.id.graphView);

        tvSubject.setOnClickListener(this);
        mftvW.setOnClickListener(this);
        mftvF.setOnClickListener(this);
        mftvUnF.setOnClickListener(this);
        //x.view().inject(this, rootView);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        initView();
        return rootView;
    }

    public Intent answerIntent;

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));

        ivUnfEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.unf_empty));
        tvUnfEmpty.setText(getResources().getString(R.string.fragment_main_text16));
        rvUnF.setEmptyView(llUnfEmpty);
        LinearLayoutManager linearLayoutManagerUnf = new LinearLayoutManager(App2.get());
        rvUnF.setLayoutManager(linearLayoutManagerUnf);
        unfAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityListUnf);
        rvUnF.setAdapter(unfAdapter);
        rvUnF.addOnItemTouchListener(new OnItemTouchListener(rvUnF) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                if (vh.getAdapterPosition() >= itemEntityListUnf.getItemCount() || vh.getAdapterPosition() < 0) {
                    return;
                }
                Object itemData = itemEntityListUnf.getItemData(vh.getAdapterPosition());
                if (itemData instanceof TaskListVo.DataBean) {
                    final TaskListVo.DataBean dataBean = (TaskListVo.DataBean) itemData;
                    mPresenter.updateStatus(String.valueOf(dataBean.getLessonTaskStudentId()));
                    if (dataBean.getStatus() == 2 || dataBean.getStatus() == 3) {
                        SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                answerIntent = new Intent(activity, AnswerResultNativeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("taskDataBean", dataBean);
                                answerIntent.putExtras(bundle);
                                startActivity(answerIntent);
                            }
                        });
//                        Intent answerIntent;
//                        if (AppLike.showH5) {
//                            answerIntent = new Intent(activity, AnswerResultActivity.class);
//                        }else {
//                        answerIntent = new Intent(activity, AnswerResultNativeActivity.class);
//                        }
//                        Intent answerIntent = new Intent(activity, AnswerResultActivity.class);
//                        Intent answerIntent = new Intent(activity, AnswerResultNativeActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable("taskDataBean", dataBean);
//                        answerIntent.putExtras(bundle);
//                        startActivity(answerIntent);
                    } else {
                        SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                answerIntent = new Intent(activity, AnswerNativeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("taskDataBean", dataBean);
                                answerIntent.putExtras(bundle);
                                startActivity(answerIntent);
                            }
                        });
//                        Intent answerIntent;
//                        if (AppLike.showH5) {
//                            answerIntent = new Intent(activity, AnswerActivity.class);
//                        } else {
//                        answerIntent = new Intent(activity, AnswerNativeActivity.class);
//                        }
//                        Intent answerIntent = new Intent(activity, AnswerActivity.class);
//                        Intent answerIntent = new Intent(activity, AnswerNativeActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable("taskDataBean", dataBean);
//                        answerIntent.putExtras(bundle);
//                        startActivity(answerIntent);
                    }
                }
            }
        });

        ivFEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.f_empty));
        tvFEmpty.setText(getResources().getString(R.string.fragment_main_text17));
        rvF.setEmptyView(llFEmpty);
        LinearLayoutManager linearLayoutManagerF = new LinearLayoutManager(App2.get());
        rvF.setLayoutManager(linearLayoutManagerF);
        fAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityListF);
        rvF.setAdapter(fAdapter);
        rvF.addOnItemTouchListener(new OnItemTouchListener(rvF) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityListF.getItemCount()) {
                    return;
                }
                Object itemData = itemEntityListF.getItemData(vh.getAdapterPosition());
                if (itemData instanceof TaskListVo.DataBean) {
                    final TaskListVo.DataBean dataBean = (TaskListVo.DataBean) itemData;
                    mPresenter.updateStatus(String.valueOf(dataBean.getLessonTaskStudentId()));
                    if (dataBean.getStatus() == 2 || dataBean.getStatus() == 3) {
//                        if (AppLike.showH5) {
//                            answerIntent = new Intent(activity, AnswerResultActivity.class);
//                        } else {
//                        }
//                        Intent answerIntent = new Intent(activity, AnswerResultActivity.class);
                        SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                answerIntent = new Intent(activity, AnswerResultNativeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("taskDataBean", dataBean);
                                answerIntent.putExtras(bundle);
                                startActivity(answerIntent);
                            }
                        });
                    } else {
                        SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                answerIntent = new Intent(activity, AnswerNativeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("taskDataBean", dataBean);
                                answerIntent.putExtras(bundle);
                                startActivity(answerIntent);
                            }
                        });
//                        Intent answerIntent;
//                        if (AppLike.showH5) {
//                            answerIntent = new Intent(activity, AnswerActivity.class);
//                        } else {
//                        answerIntent = new Intent(activity, AnswerNativeActivity.class);
//                        }
//                        Intent answerIntent = new Intent(activity, AnswerActivity.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable("taskDataBean", dataBean);
//                        answerIntent.putExtras(bundle);
//                        startActivity(answerIntent);
                    }
                }
            }
        });

        getUnFinishes();
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        getUnFinishes();
    }

    private void getUnFinishes() {
        mPresenter.getUnFinishes();
    }

    @Override
    public void getUnFinishSuccess(TaskListVo unFinishVo) {
        for (MainPresenter.MyCountDownTimer myCountDownTimer : downTimers) {
            myCountDownTimer.cancel();
        }
        itemEntityListUnf.clear();
        if (unFinishVo != null && unFinishVo.getData() != null && unFinishVo.getData().size() > 0) {
            itemEntityListUnf.addItems(R.layout.item_fragment_main_unf, unFinishVo.getData())
                    .addOnBind(R.layout.item_fragment_main_unf, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            MainPresenter.MyCountDownTimer myCountDownTimer = mPresenter.bindUnfHolder(rvUnF.getHeight(), holder, (TaskListVo.DataBean) itemData);
                            if (myCountDownTimer != null) {
                                downTimers.add(myCountDownTimer);
                            }
                        }
                    });
            unfAdapter.notifyDataSetChanged();
            mftvUnF.setNum(unFinishVo.getTotal());
        } else {
            ivUnfEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.unf_empty));
            tvUnfEmpty.setText(getResources().getString(R.string.fragment_main_text16));
            rvUnF.setEmptyView(llUnfEmpty);
            mftvUnF.setNum(0);
        }
    }

    @Override
    public void getFinishSuccess(TaskListVo finishVo) {
        itemEntityListF.clear();
        if (finishVo != null && finishVo.getData() != null && finishVo.getData().size() > 0) {
            itemEntityListF.addItems(R.layout.item_fragment_main_f, finishVo.getData())
                    .addOnBind(R.layout.item_fragment_main_f, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindFHolder(rvF.getHeight(), holder, (TaskListVo.DataBean) itemData);
                        }
                    });
            fAdapter.notifyDataSetChanged();
            mftvF.setNum(finishVo.getTotal());
        } else {
            ivFEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.f_empty));
            tvFEmpty.setText(getResources().getString(R.string.fragment_main_text17));
            rvF.setEmptyView(llFEmpty);
            mftvF.setNum(0);
        }
    }

    @Override
    public void getWrongSuccess(WrongVo wrongVo) {
        mPieChart.clearChart();
        List<WrongVo.DataBean> data = wrongVo.getData();
        Collections.sort(data, new WrongComparator());
        for (int i = 0; i < data.size(); i++) {
            WrongVo.DataBean dataBean = data.get(i);
            int count = dataBean.getCount();
            String baseSubjectName = dataBean.getBaseSubjectName();

            if (i == 0) {
                if (count > 0) {
                    mPieChart.addPieSlice(new PieModel("", count, getResources().getColor(R.color.fragment_main_chart_green)));
                }
                clv1.setSubject(baseSubjectName);
                clv1.setTextColor(getResources().getColor(R.color.fragment_main_chart_green));
                clv1.setCount(count);
            } else if (i == 1) {
                if (count > 0) {
                    mPieChart.addPieSlice(new PieModel("", count, getResources().getColor(R.color.fragment_main_chart_yellow)));
                }
                clv2.setSubject(baseSubjectName);
                clv2.setTextColor(getResources().getColor(R.color.fragment_main_chart_yellow));
                clv2.setCount(count);
            } else if (i == 2) {
                if (count > 0) {
                    mPieChart.addPieSlice(new PieModel("", count, getResources().getColor(R.color.fragment_main_chart_violet)));
                }
                clv3.setSubject(baseSubjectName);
                clv3.setTextColor(getResources().getColor(R.color.fragment_main_chart_violet));
                clv3.setCount(count);
            } else if (i == 3) {
                if (count > 0) {
                    mPieChart.addPieSlice(new PieModel("", count, getResources().getColor(R.color.fragment_main_chart_other)));
                }
                clv4.setSubject("其他");
                clv4.setTextColor(getResources().getColor(R.color.fragment_main_chart_other));
                clv4.setCount(count);
            } else {
                break;
            }
        }
        tvWCount.setText("共" + wrongVo.getTotal() + "道");
        mftvW.setNum(wrongVo.getTotal());
    }

    @Override
    public void getRightRateSuccess(RightRateVo rightRateVo) {
        if (rightRateVo != null && rightRateVo.getData() != null) {
            List<RightRateVo.DataBean> data = rightRateVo.getData();
            Collections.sort(data, new RightRateComparator());

            List<GraphView.LineViewBean> mDatas = new ArrayList<>();

            GraphView.LineViewBean lineViewBean = new GraphView.LineViewBean();
            ArrayList<GraphView.ItemInfoVo> list = new ArrayList<>();
            for (RightRateVo.DataBean dataBean : data) {
                GraphView.ItemInfoVo vo = new GraphView.ItemInfoVo();
                vo.setValue((float) dataBean.getScoreTotal());
//                    vo.setValue((float) Math.random());

                vo.setName(DateUtil.getTimeStrByTimemillis(dataBean.getSubmitTime(), "MM-dd"));
                vo.setUnit(DateUtil.formatToWeek(dataBean.getSubmitTime()));
                list.add(vo);
            }
            lineViewBean.setItemVo(list);
            lineViewBean.setColor(getResources().getColor(R.color.fragment_main_chart_line_text));
            lineViewBean.setShadowColor(new int[]{getResources().getColor(R.color.fragment_main_chart_line_top), 0x00FFFFFF});
            mDatas.add(lineViewBean);
            graphView.setDatas(mDatas, false);
        }
    }

    @Override
    public void getSubjectSuccess(SubjectVo subjectVo) {
        subjects.clear();
        if (subjectVo != null && subjectVo.getData() != null) {
            List<SubjectVo.DataBean> data = subjectVo.getData();
            subjects.addAll(data);
        }
        if (lastSubjectId != -1) {
            for (SubjectVo.DataBean bean : subjects) {
                if (bean.getId() == lastSubjectId) {
                    tvSubject.setText(bean.getName());
                    return;
                }
            }
        }
        if (subjects.size() > 0) {
            SubjectVo.DataBean dataBean = subjects.get(0);
            tvSubject.setText(dataBean.getName());
        }
    }

    private int lastSubjectId = -1;

    @Override
    public int getLastSubjectId() {
        return lastSubjectId;
    }

    @Override
    public void setLastSubjectId(SubjectVo subjectVo) {
        if (lastSubjectId != -1) {
            return;
        }
        if (subjectVo != null && subjectVo.getData() != null) {
            List<SubjectVo.DataBean> data = subjectVo.getData();
            if (data.size() != 0) {
                lastSubjectId = data.get(0).getId();
            }
        }
    }

    @Override
    public void updateStatusSuccess() {

    }

    @Override
    public void getUnFinishFailure() {
        ivUnfEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.no_data));
        tvUnfEmpty.setText(getResources().getString(R.string.fragment_main_text18));
        rvUnF.setEmptyView(llUnfEmpty);
    }

    @Override
    public void getFinishFailure() {
        ivFEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.no_data));
        tvFEmpty.setText(getResources().getString(R.string.fragment_main_text18));
        rvF.setEmptyView(llFEmpty);
    }

    @Override
    public void networkError(String msg) {
        cancilLoadState();
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void onCompleted() {
        cancilLoadState();
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvSubject) {
            showPop();
            //通过event 首页切换到学习任务
        } else if (i == R.id.mftvUnF) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new ToTaskEvent(true));
                }
            });
            //通过event 首页切换到学习任务
        } else if (i == R.id.mftvF) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new ToTaskEvent(false));
                }
            });
        } else if (i == R.id.mftvW) {
//           EventBus.getDefault().post(new ToWrongEvent());
//           Intent intent;
//                if (AppLike.showH5) {
//                    intent = new Intent(activity, TodayErrorActivity.class);
//                } else {
//            intent = new Intent(activity, TodayNativeErrorActivity.class);
//                }
//                Intent intent = new Intent(activity, TodayErrorActivity.class);
//            intent.putExtra(ErrorNativeActivity.IS_ERROR, "1");
//            startActivity(intent);
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(activity, TodayNativeErrorActivity.class);
                    intent.putExtra(ErrorNativeActivity.IS_ERROR, "1");
                    startActivity(intent);
                }
            });
        }
    }
   /* @Event(value = {R.id.tvSubject, R.id.mftvUnF, R.id.mftvF, R.id.mftvW}, type = View.OnClickListener.class)
    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSubject:
                showPop();
                break;
            case R.id.mftvUnF:
                EventBus.getDefault().post(new ToTaskEvent(true));
                break;
            case R.id.mftvF:
                EventBus.getDefault().post(new ToTaskEvent(false));
                break;
            case R.id.mftvW:
//                EventBus.getDefault().post(new ToWrongEvent());
                Intent intent;
//                if (AppLike.showH5) {
//                    intent = new Intent(activity, TodayErrorActivity.class);
//                } else {
                    intent = new Intent(activity, TodayNativeErrorActivity.class);
//                }
//                Intent intent = new Intent(activity, TodayErrorActivity.class);
                intent.putExtra(ErrorNativeActivity.IS_ERROR, "1");
                startActivity(intent);
                break;
            default:
                break;
        }
    }*/

    private void showPop() {
        if (subjectPop == null) {
            subjectPop = new SubjectPop(activity, new OnSubjectClickListener() {
                @Override
                public void onSubjectClick(SubjectVo.DataBean dataBean) {
                    lastSubjectId = dataBean.getId();
                    mPresenter.getRightRates(dataBean.getId());
                    tvSubject.setText(dataBean.getName());
                }
            });
        }
        subjectPop.showPopupWindow(tvSubject, subjects);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(Event event) {
        if (event instanceof RefreshTaskEvent) {
            getUnFinishes();
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

//    @Override
//    public boolean isBaseOnWidth() {
//        return false;
//    }
//
//    @Override
//    public float getSizeInDp() {
//        return 1280;
//    }
}