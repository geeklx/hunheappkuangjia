package com.example.app1home;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.ActivityUtils;
import com.bumptech.glide.Glide;
import com.example.baselibrary.base.BaseAppManager;


public class AdCommImgActivity extends AppCompatActivity {
    private String forname;
    private TextView tvCancel,tvConfirm;//稍后
    private ImageView iv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hios_adcomm_img_activity);
        BaseAppManager.getInstance().add(this);
        initView();
    }

    private void initView() {
        //虚拟键
//        if (NavigationBarUtil.hasNavigationBar(this)) {
//            NavigationBarUtil.hideBottomUIMenu(this);
//        }
//        getWindow().getDecorView().setSystemUiVisibility(View.INVISIBLE);// topbar

        forname = getIntent().getExtras().getString("forname");
        tvCancel = findViewById(R.id.tv_cancel);
        tvConfirm = findViewById(R.id.tv_confirm);
//        iv1 = findViewById(R.id.iv1);
//        RequestOptions options = new RequestOptions()
//                .placeholder(R.drawable.ic_def_loading)
//                .error(R.drawable.ic_def_loading)
//                .fallback(R.drawable.ic_def_loading); //url为空的时候,显示的图片;
//        Glide.with(this).load(R.drawable.ic_def_loading).into(iv1);
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishClass();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    Class<? extends Activity> klass = null;

    private void finishClass() {
        try {
            klass = (Class<? extends Activity>) Class.forName("com.example.app1home.MainActivityIndex");
            ActivityUtils.finishToActivity(klass, false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        BaseAppManager.getInstance().remove(this);
        super.onDestroy();
    }
}
