package com.example.app1home.aistudent.ktxx.fragment;


import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.app1home.R;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;


/**
 * A simple {@link } subclass.
 */
public class SpellingContentFragment extends BaseActFragment1 {


    public SpellingContentFragment() {
        // Required empty public constructor
    }

    public static SpellingContentFragment newInstance(String id, String courseName, String typeIn) {
        Bundle args = new Bundle();
//        args.putString(SUBJECT_TYPE, id);
//        args.putString(TYPE_SEARCH, courseName);
//        args.putString(IS_SEARCH_IN, typeIn);
        SpellingContentFragment fragment = new SpellingContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_spelling_content;
    }

    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }
}
