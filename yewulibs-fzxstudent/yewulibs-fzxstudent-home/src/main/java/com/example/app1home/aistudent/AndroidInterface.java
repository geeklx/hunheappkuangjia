package com.example.app1home.aistudent;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.google.gson.Gson;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.LocalBroadcastManagers;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.bean.JumpNewActBean;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.student.libutils.util.UrlEncodeUtils;

import java.io.UnsupportedEncodingException;

/**
 * Created by cenxiaozhong on 2017/5/14.
 * source code  https://github.com/Justson/AgentWeb
 */

public class AndroidInterface {
    private AgentWeb agent;
    private Activity context;

    public AndroidInterface(AgentWeb agent, Activity context) {
        this.agent = agent;
        this.context = context;
    }
    private JumpNewActBean jumpNewActBean;
    private String replaceurl;

    /**
     * 统一跳转二级界面方法
     */
    @JavascriptInterface
    public void JumpNewAndroidAct(final String url) {
        if (url != null) {
            jumpNewActBean = new Gson().fromJson(url, JumpNewActBean.class);
            Intent intent;
            if (jumpNewActBean.getPath().equals("/errorsBook/statistics")) {
                intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LevelLevelWebViewActivity");
                replaceurl = BuildConfig2.SERVER_ISERVICE_NEW1 + jumpNewActBean.getPath();
            } else if (jumpNewActBean.getPath().substring(0, jumpNewActBean.getPath().indexOf("?")).equals("/errorsBook/makeQuestion")) {
                intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ThreeLevelWebViewActivity");
                replaceurl = BuildConfig2.SERVER_ISERVICE_NEW1 + jumpNewActBean.getPath();
            } else {
                intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SecondLevelWebViewActivity");
                replaceurl = BuildConfig2.SERVER_ISERVICE_NEW1 + jumpNewActBean.getPath();//.replace('"', ' ');
            }
            try {
                intent.putExtra("url_key", UrlEncodeUtils.encodeUrl(replaceurl));//.replace(" ", "")));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            context.startActivity(intent);
        }
    }

    /**
     * 二级返回一级刷新
     */
    @JavascriptInterface
    public void activityRefresh() {
        Intent msgIntent = new Intent();
        msgIntent.setAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
    }


    /**
     * token失效跳转登录页
     * str取到的参数无用
     */
    @JavascriptInterface
    public void JumpLogin(String str) {
        MyLogUtil.e("ssssssssssssssssssssss", ActivityUtils.getTopActivity().getComponentName().getClassName().toString());
        if (!TextUtils.equals(ActivityUtils.getTopActivity().getComponentName().getClassName().toString(), "com.sdzn.fzx.student.libbase.login.activity.LoginActivity")) {
            ActivityUtils.finishAllActivities();
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.loginactivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
    /**
     * 提交关闭
     */
    @JavascriptInterface
    public void nativeBackClick(String str) {
        Activity activity = ActivityUtils.getActivityByContext(context);
        ActivityUtils.finishActivity(activity);
        Intent msgIntent = new Intent();
        msgIntent.setAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
    }

    /**
     * 统一方法
     */
//    @JavascriptInterface
//    public String activitySetting() {
//        String ClassName = SPUtils.getInstance().getString("shouye_class_name", "0");
//        return ClassName;
//    }

    /**
     * web端拿token方法
     */
//    @JavascriptInterface
//    public String activityToken() {
//        String JsrefreshToken = SPUtils.getInstance().getString("token", "");
//        ToastUtils.showLong(JsrefreshToken);
//        return JsrefreshToken;
//    }

    /**
     * web端拿用户实体方法
     */
//    @JavascriptInterface
//    public String activityUserBean() {
//        LoginBean data = StudentSPUtils.getLoginBean();
//        final String loginJsonStr = new Gson().toJson(data);
//        ToastUtils.showLong(loginJsonStr);
//        return loginJsonStr;
//    }
}
