package com.example.app1home.aistudent.ktxx;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.aistudent.ktxx.adapter.KtxxTablayoutdapter;
import com.example.app1home.aistudent.ktxx.fragment.SpellingContentFragment;
import com.sdzn.fzx.student.bean.SubjectBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.SubjtctPresenter;
import com.sdzn.fzx.student.view.SubjtctViews;

import java.util.ArrayList;
import java.util.List;

/**
 * 课堂学习
 */
public class KtxxActivity1 extends BaseActWebActivity1 implements BaseOnClickListener, SubjtctViews {
    private XRecyclerView recyclerView1Order11;
    private ViewPagerSlide viewpagerMy1Orderf2;

    private String current_id;
    private KtxxTablayoutdapter mAdapter11;
    private List<SubjectBean> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private SubjtctPresenter mSubjtctPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ktxx;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        TitleShowHideState(1);
        setBaseOnClickListener(this);
        recyclerView1Order11 = findViewById(R.id.recycler_view1_order11);
        viewpagerMy1Orderf2 = findViewById(R.id.viewpager_my1_orderf2);
        onclick();
        mSubjtctPresenter = new SubjtctPresenter();
        mSubjtctPresenter.onCreate(this);
        mSubjtctPresenter.getSubjtct();
    }

    private void onclick() {
        recyclerView1Order11.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new KtxxTablayoutdapter();
        recyclerView1Order11.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                SubjectBean bean1 = (SubjectBean) adapter.getData().get(position);
                current_id = bean1.getId();
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                viewpagerMy1Orderf2.setCurrentItem(position, true);
            }
        });
    }

    private void donetwork(List<SubjectBean> subjectBeans) {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < subjectBeans.size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new SubjectBean("0", "全部", subjectBeans.get(i).getSubjectPic(), true));
            } else {
                mDataTablayout.add(new SubjectBean(subjectBeans.get(i).getId(), subjectBeans.get(i).getName(), subjectBeans.get(i).getSubjectPic(), false));
            }
        }
        current_id = mDataTablayout.get(0).getId();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        Log.e("---geekyun----", current_id);
    }

    private void init_viewp(List<SubjectBean> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getId());
            if (i == 0) {
                SpellingContentFragment fragment1 = FragmentHelper.newFragment(SpellingContentFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else {
                SpellingContentFragment fragment1 = FragmentHelper.newFragment(SpellingContentFragment.class, bundle);
                mFragmentList.add(fragment1);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerMy1Orderf2.setAdapter(orderFragmentPagerAdapter);
        viewpagerMy1Orderf2.setOffscreenPageLimit(6);
        viewpagerMy1Orderf2.setScroll(true);//控制是否滑动
        viewpagerMy1Orderf2.setCurrentItem(0, false);
        viewpagerMy1Orderf2.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                SubjectBean bean1 = mAdapter11.getData().get(position);
                set_footer_change(bean1);
            }
        });
    }

    private void set_footer_change(SubjectBean model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            SubjectBean item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "课堂学习");
    }

    @Override
    public void onSubjtctSuccess(List<SubjectBean> subjectBeans) {
        if (subjectBeans == null && subjectBeans.size() <= 0) {
            return;
        }
        donetwork(subjectBeans);
    }

    @Override
    public void onSubjtctNodata(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onSubjtctFail(String msg) {
        ToastUtils.showShort(msg);
    }
}
