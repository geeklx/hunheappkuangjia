package com.example.app1home.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.bean.RefreshTokenRecActBean;
import com.sdzn.fzx.student.presenter.RefreshTokenPresenter;
import com.sdzn.fzx.student.view.RefreshTokenViews;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class UpdataCommonservices extends Service implements RefreshTokenViews {

    public static final String HUIBEN_READINGTIME_ACTION = "HUIBEN_READINGTIME_ACTION";
    RefreshTokenPresenter refreshTokenPresenter;
    // 通过静态方法创建ScheduledExecutorService的实例
    private ScheduledExecutorService mScheduledExecutorService = Executors.newScheduledThreadPool(2);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MsgBinder();
    }

    @Override
    public String getIdentifier() {
        return System.currentTimeMillis() + "";
    }


    public class MsgBinder extends Binder {
        public UpdataCommonservices getService() {
            return UpdataCommonservices.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        refreshTokenPresenter = new RefreshTokenPresenter();
        refreshTokenPresenter.onCreate(this);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    //启动service
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && !TextUtils.isEmpty(intent.getAction())) {
            String action = intent.getAction();
            if (action.equals(HUIBEN_READINGTIME_ACTION)) {
                //定时服务
                mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("onIndexSuccess", "定时刷新接口");
//                        refreshTokenPresenter.queryRefreshToken(SPUtils.getInstance().getString("refreshToken", ""));
                    }
                }, 1, 20, TimeUnit.SECONDS);//SECONDS--秒);//MILLISECONDS--毫秒
            }
        }
        return START_STICKY;
    }

    @Override
    public void onRefreshTokenSuccess(RefreshTokenRecActBean refreshTokenRecActBean) {
        SPUtils.getInstance().put("token", refreshTokenRecActBean.getAccess_token());
        SPUtils.getInstance().put("refreshToken", refreshTokenRecActBean.getRefresh_token());
        ToastUtils.showLong(SPUtils.getInstance().getString("token"));
    }

    @Override
    public void onRefreshTokenNodata(String msg) {

    }

    @Override
    public void onRefreshTokenFail(String msg) {

    }

    @Override
    public void onDestroy() {
        refreshTokenPresenter.onDestory();
        super.onDestroy();
    }
}
