package com.example.app1home;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.app1home.aistudent.AndroidInterface;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

/**
 * 我的课程
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class ThreeLevelWebViewActivity extends BaseActWebActivity1 implements BaseOnClickListener {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        TitleShowHideState(1);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
        }

    }

    @Nullable
    @Override
    protected String getUrl() {
        String target = getIntent().getStringExtra(URL_KEY);
        return target;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}