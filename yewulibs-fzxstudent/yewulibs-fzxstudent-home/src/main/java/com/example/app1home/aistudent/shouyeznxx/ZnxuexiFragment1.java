package com.example.app1home.aistudent.shouyeznxx;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.app1home.R;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;

import java.util.ArrayList;
import java.util.List;

/**
 * AI智囊学习首页
 *
 * @author houjie
 * @date 2020/09/16
 */
public class ZnxuexiFragment1 extends BaseActFragment1 implements View.OnClickListener {

    private TextView tvCtb, tvWdbj, tvStsc, tvZysc;
    private FrameLayout framelayout;
    private List<Fragment> fragments;
    private Fragment currFragment;
    private int containerId = R.id.framelayout;

    public static ZnxuexiFragment1 newInstance(Bundle bundle) {
        ZnxuexiFragment1 fragment = new ZnxuexiFragment1();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_znxuexi1;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        tvCtb = rootView.findViewById(R.id.tv_ctb);
        tvWdbj = rootView.findViewById(R.id.tv_wdbj);
        tvStsc = rootView.findViewById(R.id.tv_stsc);
        tvZysc = rootView.findViewById(R.id.tv_zysc);
        framelayout = rootView.findViewById(R.id.framelayout);
        setOnClick();

        initFragment();
    }

    private void initFragment() {
        Bundle bundle = new Bundle();
        Bundle bundle1 = new Bundle();
        Bundle bundle2 = new Bundle();
        Bundle bundle3 = new Bundle();
        bundle.putString("url_key", BuildConfig2.SERVER_ISERVICE_NEW1 + "/errorsBook/index?step=0");
        bundle1.putString("url_key", BuildConfig2.SERVER_ISERVICE_NEW1 + "/myNotes/index?step=1");
        bundle2.putString("url_key", BuildConfig2.SERVER_ISERVICE_NEW1 + "/collection?step=0");
        bundle3.putString("url_key", BuildConfig2.SERVER_ISERVICE_NEW1 + "/collection?step=1");
        fragments = new ArrayList<>();
        fragments.add(CtbFragment.newInstance(bundle));
        fragments.add(WdbjFragment.newInstance(bundle1));
        fragments.add(StscFragment.newInstance(bundle2));
        fragments.add(ZyscFragment.newInstance(bundle3));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.replace(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        currFragment = fragment;
        ft.replace(containerId, fragment);

        ft.commit();
    }

    private void setOnClick() {
        tvCtb.setOnClickListener(this);
        tvWdbj.setOnClickListener(this);
        tvStsc.setOnClickListener(this);
        tvZysc.setOnClickListener(this);
    }

    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tv_ctb) {
            showAssignedFragment(0);
            tvCtb.setTextColor(this.getResources().getColor(R.color.color_FA541C));
            tvWdbj.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvStsc.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvZysc.setTextColor(this.getResources().getColor(R.color.color_3F000000));
        } else if (i == R.id.tv_wdbj) {
            showAssignedFragment(1);
            tvWdbj.setTextColor(this.getResources().getColor(R.color.color_FA541C));
            tvCtb.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvStsc.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvZysc.setTextColor(this.getResources().getColor(R.color.color_3F000000));
        } else if (i == R.id.tv_stsc) {
            showAssignedFragment(2);
            tvStsc.setTextColor(this.getResources().getColor(R.color.color_FA541C));
            tvZysc.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvWdbj.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvCtb.setTextColor(this.getResources().getColor(R.color.color_3F000000));
        } else if (i == R.id.tv_zysc) {
            showAssignedFragment(3);
            tvZysc.setTextColor(this.getResources().getColor(R.color.color_FA541C));
            tvStsc.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvWdbj.setTextColor(this.getResources().getColor(R.color.color_3F000000));
            tvCtb.setTextColor(this.getResources().getColor(R.color.color_3F000000));
        }
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }
}