package com.example.app1home.aistudent.shouyeznxx;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author 我的笔记
 * @date
 */
public class WdbjFragment extends BaseActFragment1 {
    private String target;

    public WdbjFragment() {
    }

    public static WdbjFragment newInstance(Bundle bundle) {
        WdbjFragment fragment = new WdbjFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_wdbj;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            Bundle bundle = this.getArguments();
            target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(R.id.ll_base_container_wdbj);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
