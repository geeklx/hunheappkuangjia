package com.example.app1home.aistudent.wdpg;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * 我的批改
 */
public class WdpgActivity extends BaseActWebActivity1 implements BaseOnClickListener {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        TitleShowHideState(1);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            target = getIntent().getStringExtra(URL_KEY);
            loadWebSite(target);
        }
    }

    private String target;
//    @Nullable
//    @Override
//    protected String getUrl() {
//        target = getIntent().getStringExtra(URL_KEY);
//        return target;
//    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "我的批改");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            Nonetwok.setVisibility(View.GONE);
            loadWebSite(target);
        } else {
            Nonetwok.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
