package com.example.app1home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app1home.R;
import com.sdzn.fzx.student.bean.OneBean1;

public class Tablayoutznxuexidapter extends BaseQuickAdapter<OneBean1, BaseViewHolder> {

    public Tablayoutznxuexidapter() {
        super(R.layout.recycleview_znxx_tablayout_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, OneBean1 item) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
//        View view1 = helper.itemView.findViewById(R.id.view1);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        tv1.setText(item.getTab_name());
        if (item.isEnable()) {
            //选中
//            view1.setVisibility(View.VISIBLE);
            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color_FA541C));
            iv1.setImageResource(item.getTab_icon());
        } else {
            //未选中
//            view1.setVisibility(View.INVISIBLE);
            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color_3F000000));
            iv1.setImageResource(item.getTab_icon());
        }
    }
}
