package com.example.app1home.aistudent.ktxx.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app1home.R;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.bean.SubjectBean;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;

public class KtxxTablayoutdapter extends BaseQuickAdapter<SubjectBean, BaseViewHolder> {

    private int tabWidth;
    public KtxxTablayoutdapter() {
        super(R.layout.recycleview_hxkt_tablayout_item2);
    }

    public KtxxTablayoutdapter(int setTabWidth) {
        super(R.layout.recycleview_hxkt_tablayout_item2);
        tabWidth=setTabWidth;
    }
    private  String select,notSelect;

    @Override
    protected void convert(BaseViewHolder helper, SubjectBean item) {
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        JSONObject jsonObject = JSON.parseObject(item.getSubjectPic());
        select= jsonObject.getString("select");
        notSelect = jsonObject.getString("notSelect");
        if (item.isEnable()) {
            //选中
            Glide.with(mContext).load(select).into(iv1);
//            view1.setVisibility(View.VISIBLE);
//            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color_FA541C));
//            iv1.setImageResource(item.getTab_icon());
        } else {
            Glide.with(mContext).load(notSelect).into(iv1);
            //未选中
//            view1.setVisibility(View.INVISIBLE);
//            tv1.setTextColor(ContextCompat.getColor(mContext, R.color.color_3F000000));
//            iv1.setImageResource(item.getTab_icon());
        }
//        if (tabWidth!=0){
//            ViewGroup.LayoutParams lp = view1.getLayoutParams();
//            lp.width = tabWidth;
//            view1.setLayoutParams(lp);
//        }
    }
}
