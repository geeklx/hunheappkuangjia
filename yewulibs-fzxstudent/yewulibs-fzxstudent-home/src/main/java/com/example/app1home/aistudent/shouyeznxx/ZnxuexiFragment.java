package com.example.app1home.aistudent.shouyeznxx;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.adapter.Tablayoutznxuexidapter;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.bean.HCategoryBean;
import com.sdzn.fzx.student.bean.HCategoryBean1;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * AI智囊学习首页
 *
 * @author houjie
 * @date 2020/09/16
 */
public class ZnxuexiFragment extends BaseActFragment1 implements View.OnClickListener {
    private XRecyclerView recyclerViewZnxuexi;
    private ViewPagerSlide viewpagerViewZnxuexi;
    private ImageView iv_sousuo;

    private String current_id;
    private Tablayoutznxuexidapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_znxuexi;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        final LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(getActivity())
                .dismissOnBackPressed(false)
                .asLoading("加载中")
                .show();
        loadingPopup.delayDismissWith(2000, new Runnable() {
            @Override
            public void run() {
                loadingPopup.destroy();
            }
        });
        recyclerViewZnxuexi = rootView.findViewById(R.id.recycler_view_znxuexi);
        viewpagerViewZnxuexi = rootView.findViewById(R.id.viewpager_view_znxuexi);
        iv_sousuo = rootView.findViewById(R.id.iv_sousuo);
        iv_sousuo.setOnClickListener(this);
        onclick();
        donetwork();
    }

    public void donetwork() {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "错题本"));
        mDataTablayout1.add(new HCategoryBean1("2", "我的笔记"));
        mDataTablayout1.add(new HCategoryBean1("3", "试题收藏"));
        mDataTablayout1.add(new HCategoryBean1("4", "资源收藏"));
        hCategoryBean.setList(mDataTablayout1);
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), hCategoryBean.getList().get(i).getIcon(), true, BuildConfig2.SERVER_ISERVICE_NEW1 + "/errorsBook/index?step=0"));//BuildConfig2.SERVER_ISERVICE_NEW1 + "/errorsBook/index?step=0
            } else if (i == 1) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), hCategoryBean.getList().get(i).getIcon(), false, BuildConfig2.SERVER_ISERVICE_NEW1 + "/myNotes/index?step=1"));
            } else if (i == 2) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), hCategoryBean.getList().get(i).getIcon(), false, BuildConfig2.SERVER_ISERVICE_NEW1 + "/collection?step=0"));
            } else if (i == 3) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), hCategoryBean.getList().get(i).getIcon(), false, BuildConfig2.SERVER_ISERVICE_NEW1 + "/collection?step=1"));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
    }


    private void onclick() {
        recyclerViewZnxuexi.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));
        recyclerViewZnxuexi.setNestedScrollingEnabled(false);
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutznxuexidapter();
        recyclerViewZnxuexi.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
                current_id = bean1.getTab_id();
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                viewpagerViewZnxuexi.setCurrentItem(position, true);
            }
        });
    }

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            bundle.putString("url_key", mlist.get(i).getUrl());
            if (i == 0) {
//                bundle.putString("url_key", "https://netease.im/");
                CtbFragment fragment1 = FragmentHelper.newFragment(CtbFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
//                bundle.putString("url_key", "https://www.pgyer.com/");
                WdbjFragment fragment1 = FragmentHelper.newFragment(WdbjFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 2) {
//                bundle.putString("url_key", "file:///android_asset/js_aiznxx/stsc.html");
                StscFragment fragment1 = FragmentHelper.newFragment(StscFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else {
//                bundle.putString("url_key", "file:///android_asset/js_aiznxx/zysc.html");
                ZyscFragment fragment1 = FragmentHelper.newFragment(ZyscFragment.class, bundle);
                mFragmentList.add(fragment1);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(getChildFragmentManager(), getActivity(), mFragmentList);
        viewpagerViewZnxuexi.setAdapter(orderFragmentPagerAdapter);
        viewpagerViewZnxuexi.setOffscreenPageLimit(2);
        viewpagerViewZnxuexi.setScroll(true);//控制是否滑动
        viewpagerViewZnxuexi.setCurrentItem(0, false);
        viewpagerViewZnxuexi.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                OneBean1 bean1 = mAdapter11.getData().get(position);
                set_footer_change(bean1);
            }
        });
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_sousuo) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ZzxxSearchActivity");
            startActivity(intent);
        }
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }

    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return null;
    }
}
