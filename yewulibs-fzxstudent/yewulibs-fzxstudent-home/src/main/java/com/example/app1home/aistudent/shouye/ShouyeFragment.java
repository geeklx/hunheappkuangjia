package com.example.app1home.aistudent.shouye;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.example.app1home.aistudent.shouye.adapter.Ktxxdapter;
import com.example.app1home.aistudent.shouye.adapter.Wdzydapter;
import com.example.app1home.aistudent.shouye.adapter.Zzxxdapter;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.google.gson.Gson;
import com.just.agentweb.LocalBroadcastManagers;
import com.just.agentweb.WebViewClient;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.presenter.ShouyePresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.view.ShouyeViews;
import com.sdzn.fzx.student.vo.LoginBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class ShouyeFragment extends BaseActFragment1 implements View.OnClickListener, ShouyeViews, NetconListener2 {

    private TextView tvDynamicTitle;//头部标题
    private TextView tvShouyeTime;//头部时间
    private XRecyclerView recyclerViewKtxx;//课堂学习
    private XRecyclerView recyclerViewZzxx;//自主学习
    private XRecyclerView recyclerViewWdzy;//我的作业
    private RelativeLayout rlZnxtApp;//跳转学生端App
    private RelativeLayout rlWdks;//我的考试
    private RelativeLayout rlHztj;//合作探究
    private RelativeLayout rlXqfx;//学情分析
    private RelativeLayout rlWdpg;//我的批改
    /*我的考试*/
    private ImageView ivWdksPicture;
    private TextView tvWdksTitle;
    private TextView tvWdksContent;
    /*合作探究*/
    private ImageView ivHztjPicture;
    private TextView tvHztjTitle;
    private TextView tvHztjContent;
    /*学情分析*/
    private ImageView ivXqfxPicture;
    private TextView tvXqfxTitle;
    private TextView tvXqfxContent;
    /*我的批改*/
    private ImageView ivWdpgPictrue;
    private TextView tvWdpgTitle;
    private TextView tvWdpgContent;

    /*自主学习*/
    private TextView tvZzxxTitle;//标题
    private TextView tvZzxxComplete;//完成度
    private TextView tvZzxxTasks;//自主学习全部任务
    private LinearLayout llZzxxNodata;//自主学习暂无数据

    /*我的作业*/
    private TextView tvWdzyTitle;//我的作业标题
    private TextView tvWdzyComplete;//完成度
    private TextView tvWdzyTasks;//我的作业全部任务
    private LinearLayout llZuoyeNodata;//作业暂无数据

    /*课堂学习*/
    private TextView tvKtxxTitle;//我的课堂标题
    private TextView tvKtxxTasks;//课堂学习全部任务
    private LinearLayout llKtxxNodata;//课堂学习暂无数据
    private String userInfo;//用户信息

    private Ktxxdapter ktxxdapter;
    private Zzxxdapter zzxxdapter;
    private Wdzydapter wdzydapter;
    private List<OneKtxx> mDataTablayout;
    private List<OneKtxx> mDataTablayoutzzxx;
    private List<OneKtxx> mDataTablayout2wdzy;
    ShouyePresenter shouyePresenter;

    protected EmptyViewNew1 emptyview1;//网络监听
    protected NetState netState;
    protected SmartRefreshLayout refreshLayout1;//刷新
    LinearLayout llShouyebg;//首页bind内容
    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("activityRefresh".equals(intent.getAction())) {
                    RefreshLoad();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_student_shouye;
    }


    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);

        //网络监听
        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);
        netState = new NetState();
        netState.setNetStateListener(this, getActivity());
        RefreshLoad();
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);//刷新控件
        llShouyebg = rootView.findViewById(R.id.ll_shouye_outer_layer);
        if (refreshLayout1 != null && emptyview1 != null) {
            refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(final RefreshLayout refreshLayout) {
                    //刷新内容
                    RefreshLoad();
                }
            });

            /*绑定empty方法*/
            emptyview1.bind(llShouyebg).setRetryListener(new EmptyView.RetryListener() {
                @Override
                public void retry() {
                    // 分布局
                    emptyview1.loading();
                    RefreshLoad();
                }
            });
            emptyview1.notices("暂无数据", "没有网络了,检查一下吧", "正在加载....", "");
        }

        tvDynamicTitle = rootView.findViewById(R.id.tv_dynamic_title);//首页标题
        tvShouyeTime = rootView.findViewById(R.id.tv_shouye_time);//首页时间
        recyclerViewKtxx = rootView.findViewById(R.id.recycler_view_ktxx);//课堂学习列表
        recyclerViewZzxx = rootView.findViewById(R.id.recycler_view_zzxx);//自主学习列表
        recyclerViewWdzy = rootView.findViewById(R.id.recycler_view_wdzy);//我的作业列表
        rlWdks = rootView.findViewById(R.id.rl_wdks);//我的考试
        rlHztj = rootView.findViewById(R.id.rl_hztj);//合作探究
        rlXqfx = rootView.findViewById(R.id.rl_xqfx);//学情分析
        rlWdpg = rootView.findViewById(R.id.rl_wdpg);//我的批改

        //我的考试
        ivWdksPicture = (ImageView) rootView.findViewById(R.id.iv_wdks_picture);
        tvWdksTitle = (TextView) rootView.findViewById(R.id.tv_wdks_title);
        tvWdksContent = (TextView) rootView.findViewById(R.id.tv_wdks_content);
        //合作探究
        ivHztjPicture = (ImageView) rootView.findViewById(R.id.iv_hztj_picture);
        tvHztjTitle = (TextView) rootView.findViewById(R.id.tv_hztj_title);
        tvHztjContent = (TextView) rootView.findViewById(R.id.tv_hztj_content);
        //学情分析
        ivXqfxPicture = (ImageView) rootView.findViewById(R.id.iv_xqfx_picture);
        tvXqfxTitle = (TextView) rootView.findViewById(R.id.tv_xqfx_title);
        tvXqfxContent = (TextView) rootView.findViewById(R.id.tv_xqfx_content);
        //我的批改
        ivWdpgPictrue = (ImageView) rootView.findViewById(R.id.iv_wdpg_pictrue);
        tvWdpgTitle = (TextView) rootView.findViewById(R.id.tv_wdpg_title);
        tvWdpgContent = (TextView) rootView.findViewById(R.id.tv_wdpg_content);

        /*自主学习*/
        tvZzxxTitle = rootView.findViewById(R.id.tv_zzxx_title);//自主学习标题
        tvZzxxComplete = rootView.findViewById(R.id.tv_zzxx_complete);//完成度
        tvZzxxTasks = rootView.findViewById(R.id.tv_zzxx_tasks);//自主学习全部任务按钮
        llZzxxNodata = rootView.findViewById(R.id.ll_zzxx_nodata);

        /*我的作业*/
        tvWdzyTitle = rootView.findViewById(R.id.tv_wdzy_title);//我的作业标题
        tvWdzyComplete = rootView.findViewById(R.id.tv_wdzy_complete);//我的作业标题
        tvWdzyTasks = rootView.findViewById(R.id.tv_wdzy_tasks);//我的作业全部任务按钮
        llZuoyeNodata = rootView.findViewById(R.id.ll_zuoye_nodata);

        /*课堂学习*/
        tvKtxxTitle = rootView.findViewById(R.id.tv_ktxx_title);//课堂学习标题
        tvKtxxTasks = rootView.findViewById(R.id.tv_ktxx_tasks);//课堂学习全部任务按钮
        llKtxxNodata = rootView.findViewById(R.id.ll_ktxx_nodata);

        /*跳转app*/
        rlZnxtApp = rootView.findViewById(R.id.rl_znxt_app);//跳转拼课堂app按钮

        tvWdzyTasks.setOnClickListener(this);
        tvZzxxTasks.setOnClickListener(this);
        tvKtxxTasks.setOnClickListener(this);
        rlZnxtApp.setOnClickListener(this);
        rlWdks.setOnClickListener(this);
        rlHztj.setOnClickListener(this);
        rlXqfx.setOnClickListener(this);
        rlWdpg.setOnClickListener(this);
        onclick();
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            LoginBean.DataBean data = UserController.getLoginBean().getData();
            if (data == null) {
                return;
            } else {
                userInfo = jsUserBean(data);//js交互数据处理
            }
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
            loadWebSite(BuildConfig2.SERVER_ISERVICE_NEW1 + "/setToken"); // 刷新

            final String token = SPUtils.getInstance().getString("token", "");
            mAgentWeb.getWebCreator().getWebView().setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    view.loadUrl("javascript:getTokenAndUserInfo('" + token + "','" + userInfo + "')");
                }
            });
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(R.id.ll_base_container_shouye);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    private String jsUserBean(LoginBean.DataBean dataBean) {
        final String loginJsonStr = new Gson().toJson(dataBean.getUser());
        return loginJsonStr;
    }


    public void RefreshLoad() {
        shouyePresenter.queryShouye();
    }

    private void donetworkketangzizhuxuexi(List<ShouyeRecActBean.Studentlist1Bean.StudentitemlistBean> studentitemlistBeanList) {
        mDataTablayoutzzxx = new ArrayList<>();
        if (studentitemlistBeanList == null || studentitemlistBeanList.size() == 0) {
            llZzxxNodata.setVisibility(View.VISIBLE);
        } else {
            llZzxxNodata.setVisibility(View.GONE);
            for (int i = 0; i < studentitemlistBeanList.size(); i++) {
                mDataTablayoutzzxx.add(new OneKtxx(studentitemlistBeanList.get(i).getSubjectid(), studentitemlistBeanList.get(i).getCurriculumname(), 0, false, studentitemlistBeanList.get(i).getSubjectname(), studentitemlistBeanList.get(i).getTime(), 0, studentitemlistBeanList.get(i).getSubjectpicture(), studentitemlistBeanList.get(i).getAct()));
            }
            zzxxdapter.setNewData(mDataTablayoutzzxx);
        }
    }

    private void donetworkketangwodezuoye(List<ShouyeRecActBean.Studentlist2Bean.StudentitemlistBeanX> studentitemlistBeanXList) {
        mDataTablayout2wdzy = new ArrayList<>();
        if (studentitemlistBeanXList == null || studentitemlistBeanXList.size() == 0) {
            llZuoyeNodata.setVisibility(View.VISIBLE);
        } else {
            llZuoyeNodata.setVisibility(View.GONE);
            for (int i = 0; i < studentitemlistBeanXList.size(); i++) {
                mDataTablayout2wdzy.add(new OneKtxx(studentitemlistBeanXList.get(i).getSubjectid(), studentitemlistBeanXList.get(i).getCurriculumname(), 0, false, studentitemlistBeanXList.get(i).getSubjectname(), studentitemlistBeanXList.get(i).getTime(), 0, studentitemlistBeanXList.get(i).getSubjectpicture(), studentitemlistBeanXList.get(i).getAct()));
            }
            wdzydapter.setNewData(mDataTablayout2wdzy);
        }
    }


    private void donetworkketangxuexi(List<ShouyeRecActBean.Studentlist3Bean.StudentitemlistBeanXX> studentitemlistBeanXXList) {
        mDataTablayout = new ArrayList<>();
        if (studentitemlistBeanXXList == null || studentitemlistBeanXXList.size() == 0) {
            llKtxxNodata.setVisibility(View.VISIBLE);
        } else {
            llKtxxNodata.setVisibility(View.GONE);
            for (int i = 0; i < studentitemlistBeanXXList.size(); i++) {//
                mDataTablayout.add(new OneKtxx(studentitemlistBeanXXList.get(i).getSubjectid(), studentitemlistBeanXXList.get(i).getCurriculumname(), 0, false, studentitemlistBeanXXList.get(i).getSubjectname(), studentitemlistBeanXXList.get(i).getTime(), 0, studentitemlistBeanXXList.get(i).getSubjectpicture(), studentitemlistBeanXXList.get(i).getAct()));
            }
            ktxxdapter.setNewData(mDataTablayout);
        }
    }

    private void onclick() {
        /*自主学习*/
        recyclerViewZzxx.setLayoutManager(new LinearLayoutManager(getActivity()));
        zzxxdapter = new Zzxxdapter();
        recyclerViewZzxx.setAdapter(zzxxdapter);
        zzxxdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SecondLevelWebViewActivity");
                String replaceurl = BuildConfig2.SERVER_ISERVICE_NEW1 + "/autonomyLearn/learnTarget?" + "unit=1" + "&complete=false" + "&id=" + shouyeRecActBean.getStudentlist1().getStudentitemlist().get(position).getLessonTaskId() + "&pageName=" + shouyeRecActBean.getStudentlist1().getStudentitemlist().get(position).getCurriculumname();
                intent.putExtra("url_key", replaceurl);
//                intent.putExtra("titlename", shouyeRecActBean.getStudentlist1().getStudentitemlist().get(position).getCurriculumname());
                startActivity(intent);
//                IntentShow("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.ZzxxActivity", ApiInterface.H5_ADDRESS + "autonomyLearn/index");
            }
        });

        /*我的作业*/
        recyclerViewWdzy.setLayoutManager(new LinearLayoutManager(getActivity()));
        wdzydapter = new Wdzydapter();
        recyclerViewWdzy.setAdapter(wdzydapter);
        wdzydapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SecondLevelWebViewActivity");
                String replaceurl = BuildConfig2.SERVER_ISERVICE_NEW1 + "homework/answer?" + "unit=1" + "&id=" + shouyeRecActBean.getStudentlist2().getStudentitemlist().get(position).getTaskHomeworkId() + "&complete=false" + "&name=" + shouyeRecActBean.getStudentlist2().getStudentitemlist().get(position).getCurriculumname();
                LogUtils.e(replaceurl);
                intent.putExtra("url_key", replaceurl);
                startActivity(intent);
//                IntentShow("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdzyActivity", BuildConfig2.SERVER_ISERVICE_NEW1 + "/homework/index");
            }
        });

        /*课堂学习*/
        recyclerViewKtxx.setLayoutManager(new LinearLayoutManager(getActivity()));
        ktxxdapter = new Ktxxdapter();
        recyclerViewKtxx.setAdapter(ktxxdapter);
        ktxxdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (shouyeRecActBean.getStudentlist3().getStudentitemlist().size() == 0) {
                    return;
                } else {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SecondLevelWebViewActivity");
                    String replaceurl = BuildConfig2.SERVER_ISERVICE_NEW1 + "/classroomLearning/learnTarget?" + "&id=" + shouyeRecActBean.getStudentlist3().getStudentitemlist().get(position).getLessonTaskId() + "&pageName=" + shouyeRecActBean.getStudentlist3().getStudentitemlist().get(position).getCurriculumname();
                    intent.putExtra("url_key", replaceurl);
//                intent.putExtra("titlename", shouyeRecActBean.getStudentlist3().getStudentitemlist().get(position).getCurriculumname());
                    startActivity(intent);
//                IntentShow("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.KtxxActivity", ApiInterface.H5_ADDRESS + "classroomLearning");
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rl_wdks) {//我的考试
            ToastUtils.showShort("正在开发中，请耐心等待");
//            if (shouyeRecActBean == null) {
//                return;
//            } else {
//                if (shouyeRecActBean.getWdks() != null) {
//                    IntentShow(shouyeRecActBean.getWdks().getAct(), BuildConfig2.SERVER_ISERVICE_NEW1 + "/examination/index");
//                }
//            }
        } else if (id == R.id.rl_hztj) {//合作探究
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getHztj() != null) {
                    IntentShow(shouyeRecActBean.getHztj().getAct(), BuildConfig2.SERVER_ISERVICE_NEW1 + "/cooperation");
                }
            }
        } else if (id == R.id.rl_xqfx) {//学情分析
            ToastUtils.showShort("正在开发中，请耐心等待");
//            if (shouyeRecActBean == null) {
//                return;
//            } else {
//                if (shouyeRecActBean.getXqfx() != null) {
//                    IntentShowExtra(shouyeRecActBean.getXqfx().getAct());
//                }
//            }
        } else if (id == R.id.rl_wdpg) {//我的批改
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getWdpg() != null) {
                    IntentShow(shouyeRecActBean.getWdpg().getAct(), BuildConfig2.SERVER_ISERVICE_NEW1 + "/taskCorrecting/index");
                }
            }
        } else if (id == R.id.tv_zzxx_tasks) {//自主学习
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getStudentlist1() != null) {
                    IntentShow(shouyeRecActBean.getStudentlist1().getAct(), BuildConfig2.SERVER_ISERVICE_NEW1 + "/autonomyLearn/index");
                }
            }
        } else if (id == R.id.tv_wdzy_tasks) {//我的作业
//            ToastUtils.showLong("正在测试中，请耐心等待");
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getStudentlist2() != null) {
                    IntentShow(shouyeRecActBean.getStudentlist2().getAct(), BuildConfig2.SERVER_ISERVICE_NEW1 + "/homework/index");
                }
            }
        } else if (id == R.id.tv_ktxx_tasks) {//课堂学习
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.KtxxActivity");
            startActivity(intent);
//            if (shouyeRecActBean == null) {
//                return;
//            } else {
//                if (shouyeRecActBean.getStudentlist3() != null) {
//                    IntentShow(shouyeRecActBean.getStudentlist3().getAct(), BuildConfig2.SERVER_ISERVICE_NEW1 + "/classroomLearning");
//                }
//            }
        } else if (id == R.id.rl_znxt_app) {//跳转app
            if (AppUtils.isAppInstalled("com.sdzn.pkt.student.hd")) {
//                Intent intent = new Intent();
//                intent.setAction("com.sdzn.pkt.student.hd.hs.act.SplashActivity");
//                startActivity(intent);
                String name = StudentSPUtils.getLoginUserNum();
                String pwd = StudentSPUtils.getLoginUserPwd();
                String state = "1";
                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd + "&state=" + state));
                startActivity(intent);
            } else {
                ToastUtils.showLong("请先安装智囊学堂学生端APP");
            }
        }
    }

    public static final String URL_KEY = "url_key";

    /*带参跳转*/
    private void IntentShow(String act, String url) {
        if (act != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(act));
            intent.putExtra(URL_KEY, url);
            startActivity(intent);
        }
    }

    /*无参跳转*/
    private void IntentShowExtra(String act) {
        if (act != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(act));
            startActivity(intent);
        }
    }

    private ShouyeRecActBean shouyeRecActBean;

    @Override
    public void onCehuaSuccess(ShouyeRecActBean grzxRecActBean) {
//        ToastUtils.showLong("成功....");
        emptyview1.success();
        refreshLayout1.finishRefresh(true);
        shouyeRecActBean = new ShouyeRecActBean();
        shouyeRecActBean = grzxRecActBean;
//        LogUtils.e(new Gson().toJson(shouyeRecActBean));
        if (shouyeRecActBean == null) {
            return;
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(100);
                }
            }).start();
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 100) {
                shouyeXxgj();//学习工具
                donetworkketangzizhuxuexi(shouyeRecActBean.getStudentlist1().getStudentitemlist());//获取自主学习列表数据
                donetworkketangwodezuoye(shouyeRecActBean.getStudentlist2().getStudentitemlist());//获取我的作业列表数据
                donetworkketangxuexi(shouyeRecActBean.getStudentlist3().getStudentitemlist());//获取课堂学习学习列表数据
            }
        }
    };

    @Override
    public void onCehuaNodata(String msg) {
//        emptyview1.nodata();
        ToastUtils.showLong(msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void onCehuaFail(String msg) {
        ToastUtils.showLong(msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);

    }

    /*学习工具*/
    private void shouyeXxgj() {
        if (shouyeRecActBean == null) {
            return;
        }
        /*头部头部内容*/
        tvDynamicTitle.setText(Html.fromHtml(shouyeRecActBean.getStudentcount().getStudenttitle()));
        tvShouyeTime.setText(shouyeRecActBean.getStudentcount().getStudenttime());
        /*我的考试*/
//            Glide.with(getActivity()).load(shouyeRecActBean.getWdks().getPicture()).skipMemoryCache(false).dontAnimate().centerCrop().into(ivWdksPicture);
//            Glide.with(getActivity()).load(shouyeRecActBean.getWdks().getPicture()).into(ivWdksPicture);
        tvWdksTitle.setText(shouyeRecActBean.getWdks().getName());
        tvWdksContent.setText(shouyeRecActBean.getWdks().getDetails());

        /*合作探究*/
//            Glide.with(getActivity()).load(shouyeRecActBean.getHztj().getPicture()).into(ivHztjPicture);
        tvHztjTitle.setText(shouyeRecActBean.getHztj().getName());
        tvHztjContent.setText(shouyeRecActBean.getHztj().getDetails());

        /*学情分析*/
//            Glide.with(getActivity()).load(shouyeRecActBean.getXqfx().getPicture()).into(ivXqfxPicture);
        tvXqfxTitle.setText(shouyeRecActBean.getXqfx().getName());
        tvXqfxContent.setText(shouyeRecActBean.getXqfx().getDetails());

        /*我的批改*/
//            Glide.with(getActivity()).load(shouyeRecActBean.getWdpg().getPicture()).into(ivWdpgPictrue);
        tvWdpgTitle.setText(shouyeRecActBean.getWdpg().getName());
        tvWdpgContent.setText(shouyeRecActBean.getWdpg().getDetails());

        /*自主学习*/
        tvZzxxTitle.setText(shouyeRecActBean.getStudentlist1().getName());
        tvZzxxComplete.setText(Html.fromHtml(shouyeRecActBean.getStudentlist1().getDwctitle()));

        /*我的作业*/
        tvWdzyTitle.setText(shouyeRecActBean.getStudentlist2().getName());
        tvWdzyComplete.setText(Html.fromHtml(shouyeRecActBean.getStudentlist2().getDwctitle()));

        /*课堂学习*/
        tvKtxxTitle.setText(shouyeRecActBean.getStudentlist3().getName());
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
        shouyePresenter.onDestory();
        super.onDestroy();
    }


    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }


    @Override
    public void net_con_none() {
        ToastUtils.showShort("网络异常，请检查网络连接");
    }

    @Override
    public void net_con_success() {
    }

    @Override
    public void showNetPopup() {
        netState.setUpdate(true);
        ToastUtils.showShort("当前是移动网络");
    }
}
