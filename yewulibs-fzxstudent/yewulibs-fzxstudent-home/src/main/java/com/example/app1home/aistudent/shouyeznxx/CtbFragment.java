package com.example.app1home.aistudent.shouyeznxx;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.NetworkUtils;
import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;

/**
 * @author 错题本
 * @date
 */
public class CtbFragment extends BaseActFragment1 {
    View basecontainer;//首页bind内容

    public CtbFragment() {
    }

    public static CtbFragment newInstance(Bundle bundle) {
        CtbFragment fragment = new CtbFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_ctb;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        basecontainer = rootView.findViewById(R.id.ll_base_container_ctb);
        final boolean netConnectOk = NetworkUtils.isAvailableByPing();
//        if (netConnectOk){
//            Nonetwok.setVisibility(View.GONE);
//        }else{
//            Nonetwok.setVisibility(View.VISIBLE);
//        }
//        Nonetwok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (NetworkUtils.isAvailableByPing()){
////                    mAgentWeb.getUrlLoader().reload();
//                    loadWebSite(target); // 刷新
//                    Nonetwok.setVisibility(View.GONE);
//                }else{
//                    ToastUtils.showShort("请设置网络");
//                    Nonetwok.setVisibility(View.VISIBLE);
//                }
//            }
//        });
    }

    private String target;

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
//          new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdzyActivity"));
//                    intent.putExtra("url_key", BuildConfig2.SERVER_ISERVICE_NEW1 + "/homework/index");
//                    startActivity(intent);
//                }
//            }, 8000);
            Bundle bundle = this.getArguments();
            target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
