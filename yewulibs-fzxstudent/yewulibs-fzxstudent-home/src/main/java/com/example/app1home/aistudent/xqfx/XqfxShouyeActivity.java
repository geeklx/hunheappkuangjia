package com.example.app1home.aistudent.xqfx;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.adapter.Tablayoutdapter;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.bean.RecyclerTabBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.TabPresenter;
import com.sdzn.fzx.student.view.TabViews;

import java.util.ArrayList;
import java.util.List;

/*
 * 学情分析 首页
 * */
public class XqfxShouyeActivity extends BaseActWebActivity1 implements BaseOnClickListener, TabViews {
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private int defCurrentItem = 0;//设置默认见面
    TabPresenter tabPresenter;
    private String type = "10";//查询


    @Override
    protected int getLayoutId() {
        return R.layout.activity_xqfxhouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        donetwork();


    }

    /* 重载业务部分*/
    public void donetwork() {
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        onclick();
        tabPresenter = new TabPresenter();
        tabPresenter.onCreate(this);
        tvTitleName.setText("学情分析");
        tabPresenter.querytab(type);
    }

    private void setData(List<RecyclerTabBean.ListBean> recyclerTabBeans) {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < recyclerTabBeans.size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), 0, true, recyclerTabBeans.get(i).getUrl()));
            } else if (i == 1) {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), 0, false, recyclerTabBeans.get(i).getUrl()));
            } else {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), 0, false, recyclerTabBeans.get(i).getUrl()));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
    }


    private void onclick() {
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter(50);
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
                current_id = bean1.getTab_id();
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }

    public static final String URL_KEY = "url_key";

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            initFragment(mFragmentList, mlist, i);
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(4);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                changeEnable(position);
            }
        });
    }

    /**
     * 首次初始化 fragment
     */
    private void initFragment(List<Fragment> mFragmentList, List<OneBean1> mlist, int i) {
        Bundle bundle = new Bundle();
        bundle.putString("id", mlist.get(i).getTab_id());
        if (i == 0) {
            bundle.putString(URL_KEY, mlist.get(i).getUrl());
            XqfxShouyeFragment fragment1 = FragmentHelper.newFragment(XqfxShouyeFragment.class, bundle);
            mFragmentList.add(fragment1);
        } else if (i == 1) {
            bundle.putString(URL_KEY, mlist.get(i).getUrl());
            XqfxBaogaoFragment fragment2 = FragmentHelper.newFragment(XqfxBaogaoFragment.class, bundle);
            mFragmentList.add(fragment2);
        } else {
            bundle.putString(URL_KEY, mlist.get(i).getUrl());
            XqfxJieduanFragment fragment3 = FragmentHelper.newFragment(XqfxJieduanFragment.class, bundle);
            mFragmentList.add(fragment3);
        }
    }

    private void changeEnable(int position) {
        OneBean1 bean1 = mAdapter11.getData().get(position);
        current_id = bean1.getTab_id();
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        tabPresenter.onDestory();
        super.onDestroy();
    }

    @Override
    public void onTabSuccess(RecyclerTabBean recyclerTabBean) {
        setData(recyclerTabBean.getList());
    }

    @Override
    public void onTabNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onTabFail(String msg) {
        ToastUtils.showLong(msg);
    }
}