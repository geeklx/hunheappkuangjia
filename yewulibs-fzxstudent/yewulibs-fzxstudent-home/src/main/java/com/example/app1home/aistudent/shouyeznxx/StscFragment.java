package com.example.app1home.aistudent.shouyeznxx;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;

/**
 * @author 试题收藏
 * @date
 */
public class StscFragment
        extends BaseActFragment1 {
    private String target;

    public StscFragment() {
    }

    public static StscFragment newInstance(Bundle bundle) {
        StscFragment fragment = new StscFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_stsc;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            Bundle bundle = this.getArguments();
            target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(R.id.ll_base_container_stsc);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

