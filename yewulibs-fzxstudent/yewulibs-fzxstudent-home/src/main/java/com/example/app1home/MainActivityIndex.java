package com.example.app1home;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSON;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app1home.adapter.ShouyeFooterAdapter;
import com.example.app1home.adapter.ShouyeFooterBean;
import com.example.app1home.aistudent.shouye.ShouyeFragment;
import com.example.app1home.aistudent.shouyeznxx.ZnxuexiFragment;
import com.example.app1home.aistudent.shouyeznxx.ZnxuexiFragment1;
import com.example.app1home.service.UpdataCommonservices;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.bean.GrzxRecActBean;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.MmkvUtils;
import com.sdzn.fzx.student.libutils.util.StartHiddenManager;
import com.sdzn.fzx.student.presenter.CehuaPresenter;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CehuaViews;
import com.sdzn.fzx.student.view.CheckverionViews;
import com.sdzn.fzx.student.vo.LoginBean;

import org.greenrobot.greendao.annotation.NotNull;

import java.util.ArrayList;
import java.util.List;

import constant.UiType;
import listener.UpdateDownloadListener;
import me.jessyan.autosize.AutoSizeCompat;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;


public class MainActivityIndex extends BaseActWebActivity1 implements View.OnClickListener , CehuaViews, CheckverionViews {
    private RecyclerView recyclerView;
    private ShouyeFooterAdapter mAdapter;
    private TextView tvSubject;//个人中心
    private TextView tvName;//首页名称
    private RoundAngleImageView shouyeHead;//学生头像
    private FragmentManager mFragmentManager;

    private int current_pos = 0;
    private String tag_ids;
    private List<ShouyeFooterBean> mList;
    public static final String id1 = "11";
    public static final String id2 = "22";
    private static final String LIST_TAG1 = "list11";
    private static final String LIST_TAG2 = "list22";
    private static boolean isExit;// 标示是否退出

    private ShouyeFragment mFragment1; //
    private ZnxuexiFragment1 mFragment2; //

    private StartHiddenManager startHiddenManager;

    CehuaPresenter cehuaPresenter;//侧滑
    CheckverionFzxPresenter checkverionFzxPresenter;//侧滑

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_shouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);

        findview();
        onclick();
        mFragmentManager = getSupportFragmentManager();
        // 解决fragment布局重叠错乱
        if (savedInstanceState != null) {
            mFragment1 = (ShouyeFragment) mFragmentManager.findFragmentByTag(LIST_TAG1);
            mFragment2 = (ZnxuexiFragment1) mFragmentManager.findFragmentByTag(LIST_TAG2);
        }
        doNetWork();
//        checkLatestMedal();//启动服务
        //
        startHiddenManager = new StartHiddenManager(findViewById(R.id.iv_left1), findViewById(R.id.tv_right1), null, new StartHiddenManager.OnClickFinish() {
            @Override
            public void onFinish() {
                new XPopup.Builder(MainActivityIndex.this)
                        //.dismissOnBackPressed(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .autoOpenSoftInput(true)
//                        .autoFocusEditText(false) //是否让弹窗内的EditText自动获取焦点，默认是true
                        .isRequestFocus(false)
                        //.moveUpToKeyboard(false)   //是否移动到软键盘上面，默认为true
                        .asInputConfirm("修改地址", SPUtils.getInstance().getString("版本地址2", "https://www.baidu.com"), null, "",
                                new OnInputConfirmListener() {
                                    @Override
                                    public void onConfirm(String text) {
//                                        toast("input text: " + text);
                                        if (text.contains("http")) {
                                            String[] content = text.split(",");
                                            SPUtils.getInstance().put("版本地址1", content[0]);
                                            SPUtils.getInstance().put("版本地址2", content[1]);
                                            BuildConfig2.SERVER_ISERVICE_NEW1 = content[0];
                                            BuildConfig2.SERVER_ISERVICE_NEW2 = content[1];
                                        }
//                                new XPopup.Builder(getContext()).asLoading().show();
                                    }
                                })
                        .show();
            }
        });

        /*侧滑数据存储*/
        cehuaPresenter = new CehuaPresenter();
        cehuaPresenter.onCreate(this);
        cehuaPresenter.queryCehua();
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        checkverionFzxPresenter.checkVerion("1", "8");
    }

    private void checkLatestMedal() {
        Intent intent = new Intent(this, UpdataCommonservices.class);
        intent.setAction(UpdataCommonservices.HUIBEN_READINGTIME_ACTION);
        startService(intent);
    }

    private void doNetWork() {
        mList = new ArrayList<>();
        addList();
        recyclerView.setLayoutManager(new GridLayoutManager(this, mList.size(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();
        current_pos = 0;
        footer_onclick();
    }

    //点击item
    private void footer_onclick() {
        final ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(current_pos);
        if (model.isEnselect()) {
            // 不切换当前的item点击 刷新当前页面
            showFragment(model.getText_id(), true);
        } else {
            // 切换到另一个item
            if (model.getText_id().equalsIgnoreCase(id2)) {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            } else {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            }
        }
    }

    private void set_footer_change(ShouyeFooterBean model) {
        //设置为选中
        initList();
        model.setEnselect(true);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();

    }

    private void findview() {
        recyclerView = findViewById(R.id.recycler_view1);
        tvSubject = (TextView) findViewById(R.id.tv_subject);
        tvName = findViewById(R.id.tv_name);
        shouyeHead = findViewById(R.id.shouye_head);
        mAdapter = new ShouyeFooterAdapter(this);

        LoginBean.DataBean data = UserController.getLoginBean().getData();
        if (data == null) {
            return;
        } else {
            if (!TextUtils.isEmpty(data.getUser().getRealName())) {
                tvName.setText(data.getUser().getRealName());
            }
            if (!TextUtils.isEmpty(data.getUser().getPhoto())) {
                RequestOptions options=new RequestOptions()
                        .error(R.mipmap.mrtx_img)
                        .transform(new CircleTransform(App2.get()));

                Glide.with(App2.get())
                        .load(data.getUser().getPhoto())
                        .apply(options)
                        .into(shouyeHead);
            }
        }
    }


    private void onclick() {
        mAdapter.setOnItemClickLitener(new ShouyeFooterAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                //点击item
                current_pos = position;
                footer_onclick();
            }
        });
        tvSubject.setOnClickListener(this);
        shouyeHead.setOnClickListener(this);
    }

    private void addList() {
        mList.add(new ShouyeFooterBean(id1, "我的首页", true));
        mList.add(new ShouyeFooterBean(id2, "AI智能学习", false));
    }

    private void initList() {
        for (int i = 0; i < mList.size(); i++) {
            ShouyeFooterBean item = mList.get(i);
            if (item.isEnselect()) {
                item.setEnselect(false);
            }
        }
    }

    private void showFragment(final String tag, final boolean isrefresh) {
        tag_ids = tag;
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        hideFragments(transaction);

        if (tag.equalsIgnoreCase("-1")) {
//            if (mFragment1 == null) {
//                mFragment1 = new FragmentContent1();
//                transaction.add(R.id.container, mFragment1, LIST_TAG0);
//            } else {
//                transaction.show(mFragment1);
//                mFragment1.initData();
//            }
        } else if (tag.equalsIgnoreCase(id1)) {
            if (mFragment1 == null) {
                mFragment1 = new ShouyeFragment();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment1.setArguments(args);
                transaction.add(R.id.container, mFragment1, LIST_TAG1);
            } else {
                transaction.show(mFragment1);
                mFragment1.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id2)) {
            if (mFragment2 == null) {
                mFragment2 = new ZnxuexiFragment1();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment2.setArguments(args);
                transaction.add(R.id.container, mFragment2, LIST_TAG2);
            } else {
                transaction.show(mFragment2);
                mFragment2.getCate(tag, isrefresh);
            }
        }
        transaction.commitAllowingStateLoss();
    }

    private void hideFragments(FragmentTransaction transaction) {
        if (mFragment1 != null) {
            transaction.hide(mFragment1);
            mFragment1.give_id(tag_ids);
        }
        if (mFragment2 != null) {
            transaction.hide(mFragment2);
            mFragment2.give_id(tag_ids);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private long exitTime;

    /**
     * 连按两次退出程序
     */
    private void exit() {
        ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(0);
        if (model != null && !tag_ids.equals(model.getText_id())) {
            set_footer_change(model);
            showFragment(model.getText_id(), false);
        } else {
            if ((System.currentTimeMillis() - exitTime) < 1500) {
//                RichText.recycle();
//                BaseAppManager.getInstance().closeApp();

//                ActivityManager.exit();
                //清楚堆栈
                ActivityUtils.finishAllActivities();
            } else {
                ToastUtils.showShort("再按一次退出程序 ~");
                exitTime = System.currentTimeMillis();
            }
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_subject) {//个人中心
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            startActivity(intent);
        } else if (id == R.id.shouye_head) {//头像
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            startActivity(intent);
        }
    }

    /**
     * 侧滑数据处理
     */

    @Override
    public void onCehuaSuccess(GrzxRecActBean grzxRecActBean) {
        MmkvUtils.getInstance().set_common_json("GrzxRecActBean", JSON.toJSONString(grzxRecActBean), GrzxRecActBean.class);
    }

    @Override
    public void onCehuaNodata(String msg) {

    }

    @Override
    public void onCehuaFail(String msg) {

    }
    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = AppUtils.getAppVersionCode();//App2Utils.getAppVersionCode(App2.get());//获取版本号
            if (!versionInfoBean.getVersionNum().equals("")) {
                int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
                if (updateVersion > currVersion) {
                    if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                        Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl(), versionInfoBean.getVersionInfo());
                    }
                }
            }
        }
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showShort(msg);
    }

    private void Updatemethod(String description, final String targetUrl, String versionInfo) {
        String updateTitle = "AI智囊学堂学生端" + versionInfo;
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(com.sdzn.fzx.student.libbase.R.layout.view_update_dialog_custom_student);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(com.sdzn.fzx.student.libbase.R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/fzxstudenthd");//apk下载位置
        forceconfig.setApkSaveName("AI智囊学堂学生端");//app名称
        String count = description.replace("|", "\n");
        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

}
