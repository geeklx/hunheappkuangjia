package com.example.app1home.aistudent.shouyeznxx;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.sdzn.fzx.student.libbase.newbase.BaseActFragment1;

/**
 * @author 张超
 * @date
 */
public class ZyscFragment extends BaseActFragment1 {

    public ZyscFragment() {
    }

    public static ZyscFragment newInstance(Bundle bundle) {
        ZyscFragment fragment = new ZyscFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_zysc;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
            Bundle bundle = this.getArguments();
            String target = bundle.getString("url_key");
            loadWebSite(target); // 刷新
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = getActivity().findViewById(R.id.ll_base_container_zysc);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }
}
