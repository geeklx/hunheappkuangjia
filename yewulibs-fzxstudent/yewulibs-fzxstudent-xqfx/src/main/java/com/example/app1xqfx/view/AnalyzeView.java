package com.example.app1xqfx.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.ColumStatisticsChat;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.DoubleColumView;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.GraphView;
import com.sdzn.fzx.student.vo.GroupAnalyzeBean;

import java.util.List;

/**
 * AnalyzeView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface AnalyzeView extends BaseView {

    public void onSubjectSuccess(List<ColumStatisticsChat.ItemInfoVo> retList);

    public void onSubjectFailed();

    public void onGroupSuccess(List<DoubleColumView.ItemInfoVo> list, GroupAnalyzeBean.MyGroupInfo myGroupInfo);

    public void onGroupFail();

    public void onRightRateSuccess(List<GraphView.LineViewBean> list);

    public void onRightRateFailed();
}
