package com.example.app1xqfx.presenter;

import android.text.TextUtils;

import com.example.app1xqfx.fragment.AnalyzeFragment;
import com.example.app1xqfx.view.AnalyzeView;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.ColumStatisticsChat;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.DoubleColumView;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.GraphView;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.AnalyzeSubjectBean;
import com.sdzn.fzx.student.vo.GroupAnalyzeBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.RightRateBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * AnalyzePresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class AnalyzePresenter extends BasePresenter<AnalyzeView, BaseActivity> {

    private String timeId;

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }

    public void getSubjectStatictis() {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        final String userStudentId = loginBean.getData().getUser().getId() + "";
        final String classId = loginBean.getData().getUser().getClassId() + "";

        Network.createTokenService(NetWorkService.StudyStatstic.class)
                .accuracyStatistic(userStudentId, classId + "", timeId)
                .map(new StatusFunc<AnalyzeSubjectBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<AnalyzeSubjectBean>(new SubscriberListener<AnalyzeSubjectBean>() {
                    @Override
                    public void onNext(AnalyzeSubjectBean o) {
                        handleSubjectData(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.onSubjectFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    public void getRightRateStatictis(final String subjectId) {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        final String userStudentId = loginBean.getData().getUser().getId() + "";
        final String classId = loginBean.getData().getUser().getClassId() + "";

        Map<String, String> map = new HashMap<>();
        map.put("userStudentId", userStudentId);
        map.put("classId", classId);
        map.put("baseSubjectId", subjectId);
        map.put("timeType", timeId);

        Network.createTokenService(NetWorkService.StudyStatstic.class)
                .rightRateStatistic(map)
                .map(new StatusFunc<RightRateBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<RightRateBean>(new SubscriberListener<RightRateBean>() {
                    @Override
                    public void onNext(RightRateBean o) {
                        final List<GraphView.LineViewBean> list = dealRightRateList(o);
                        mView.onRightRateSuccess(list);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onRightRateFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity, false));
    }


    public void getGroupAnalyzeData(String subjectId) {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        final String classId = loginBean.getData().getUser().getClassId() + "";
        final String classGroupId = loginBean.getData().getUser().getClassGroupId();

        Network.createTokenService(NetWorkService.StudyStatstic.class)
                .groupAnalyze(subjectId, classId, timeId)
                .map(new StatusFunc<GroupAnalyzeBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GroupAnalyzeBean>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.onGroupFail();
                    }

                    @Override
                    public void onNext(GroupAnalyzeBean o) {
                        handleGroupData(o, classGroupId);
                    }
                });

    }

    private void handleSubjectData(AnalyzeSubjectBean bean) {
        List<ColumStatisticsChat.ItemInfoVo> retList = new ArrayList<>();
        if (bean == null) {
            mView.onSubjectSuccess(retList);
            return;
        }

        for (int i = 0; i < bean.getData().size(); i++) {
            final AnalyzeSubjectBean.DataBean dataBean = bean.getData().get(i);
            ColumStatisticsChat.ItemInfoVo infoVo = new ColumStatisticsChat.ItemInfoVo(dataBean.getBaseSubjectId(), dataBean.getBaseSubjectName(), (float) dataBean.getScoreTotal());
            retList.add(infoVo);
        }

        mView.onSubjectSuccess(retList);

        if (bean != null && bean.getData() != null && bean.getData().size() > 0) {
            getGroupAnalyzeData(bean.getData().get(0).getBaseSubjectId());
            getRightRateStatictis(bean.getData().get(0).getBaseSubjectId());
        } else {
            mView.onGroupFail();
            mView.onRightRateFailed();
        }


    }

    private void handleGroupData(GroupAnalyzeBean bean, String groupId) {
        List<DoubleColumView.ItemInfoVo> retList = new ArrayList<>();
        GroupAnalyzeBean.MyGroupInfo myGroupInfo = null;

        if (bean == null) {
            mView.onGroupSuccess(retList, myGroupInfo);
            return;
        }
        for (int i = 0; i < bean.getData().size(); i++) {
            final GroupAnalyzeBean.DataBean dataBean = bean.getData().get(i);
            DoubleColumView.ItemInfoVo infoVo = new DoubleColumView.ItemInfoVo();
            infoVo.setId(dataBean.getClassGroupId());
            infoVo.setName(dataBean.getClassGroupName());
            infoVo.setValue(dataBean.getTotalPoints());
            infoVo.setTopValue(dataBean.getClassPoints());
            infoVo.setBottomValue(dataBean.getTaskPoints());
            retList.add(infoVo);
            if (groupId != null && TextUtils.equals(bean.getData().get(i).getClassGroupId(), groupId)) {
                myGroupInfo = new GroupAnalyzeBean.MyGroupInfo(dataBean.getTotalSort(), dataBean.getTaskSort(), dataBean.getClassSort());
            }

        }
        mView.onGroupSuccess(retList, myGroupInfo);
        return;

    }


    private List<GraphView.LineViewBean> dealRightRateList(RightRateBean bean) {
        if (bean == null)
            return null;

        final List<RightRateBean.DataBean> rightDataList = bean.getData();

        List<GraphView.LineViewBean> retList = new ArrayList<>();

        ArrayList<GraphView.ItemInfoVo> mylist = new ArrayList<>();
        ArrayList<GraphView.ItemInfoVo> averageList = new ArrayList<>();
        ArrayList<GraphView.ItemInfoVo> toplist = new ArrayList<>();


        final GraphView.LineViewBean myLineBean = new GraphView.LineViewBean();
        final GraphView.LineViewBean averageLineBean = new GraphView.LineViewBean();
        final GraphView.LineViewBean topLinBean = new GraphView.LineViewBean();

        for (int j = 0; j < rightDataList.size(); j++) {
            GraphView.ItemInfoVo myVo = new GraphView.ItemInfoVo();
            GraphView.ItemInfoVo averageVo = new GraphView.ItemInfoVo();
            GraphView.ItemInfoVo topVo = new GraphView.ItemInfoVo();
            String day = "";
            if (timeId.equals(AnalyzeFragment.timeIds[0])) {
                day = StringUtils.transTime(rightDataList.get(j).getDay(), "yyyy-MM");
            } else if (timeId.equals(AnalyzeFragment.timeIds[1])) {
                day = StringUtils.transTime(rightDataList.get(j).getDay(), "E");
            } else {
                day = StringUtils.transTime(rightDataList.get(j).getDay(), "MM-dd");
            }

            myVo.setValue(rightDataList.get(j).getMyRate());
            myVo.setName(day);
            averageVo.setValue(rightDataList.get(j).getClassRate());
            averageVo.setName(day);
            topVo.setValue(rightDataList.get(j).getHighRate());
            topVo.setName(day);
            mylist.add(myVo);
            averageList.add(averageVo);
            toplist.add(topVo);
        }
        myLineBean.setItemVo(mylist);
        myLineBean.setColor(GraphView.COLOR_COLUM_COLORS[0]);
        myLineBean.setName("我的");
        averageLineBean.setItemVo(averageList);
        averageLineBean.setColor(GraphView.COLOR_COLUM_COLORS[1]);
        averageLineBean.setName("平均");
        topLinBean.setItemVo(toplist);
        topLinBean.setColor(GraphView.COLOR_COLUM_COLORS[2]);
        topLinBean.setName("最高");

        retList.add(myLineBean);
        retList.add(averageLineBean);
        retList.add(topLinBean);

        return retList;
    }

}
