package com.example.app1xqfx.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.StatisticsGraphView;
import com.sdzn.fzx.student.vo.TeachStatisticsVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/8
 * 修改单号：
 * 修改内容:
 */
public interface AnalysisStatisticsView extends BaseView {
    void setTeachStatisticsVo(TeachStatisticsVo teachStatisticsVo);
    void setLineViewBean(List<StatisticsGraphView.LineViewBean> lineViewBean);
    void networkError(String msg);
}
