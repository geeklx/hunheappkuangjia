package com.example.app1xqfx.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1xqfx.R;
import com.example.app1xqfx.adapter.TeachStatisticsAdapter;
import com.example.app1xqfx.presenter.AnalysisStatisticsPresenter;
import com.example.app1xqfx.view.AnalysisStatisticsView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libpublic.views.NoScrollGridView;
import com.sdzn.fzx.student.libpublic.views.SpinerPopWindow;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.StatisticsGraphView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.TeachStatisticsVo;

import java.util.ArrayList;
import java.util.List;


/**
 * 教学统计
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class StatisticsFragment extends MBaseFragment<AnalysisStatisticsPresenter>
        implements AnalysisStatisticsView, OnChartValueSelectedListener, View.OnClickListener {
    public static final String SUBJECTID = "subjectId";

    protected Typeface tfRegular;
    protected Typeface tfLight;
    public static final String[] timeIds = {"3", "1", "2"};

    /* @BindView(R.id.tv_number)
     TextView tvNumber;
     @BindView(R.id.class_spinner_txt_two)
     TextView classSpinnerTxtTwo;
     @BindView(R.id.time_analyze_view)
     StatisticsGraphView timeAnalyzeView;
     @BindView(R.id.chart1)
     PieChart chart;
     @BindView(R.id.select_tv_two)
     TextView selectTvTwo;
     @BindView(R.id.gridView)
     NoScrollGridView gridView;
     //本界面GridVIew
     @BindView(R.id.ll_gridView)
     LinearLayout ll_gridView;
     @BindView(R.id.ll_pienodata)
     LinearLayout ll_pienodata;*/
    private TextView tv;
    private TextView tvNumber;
    private TextView tv2;
    private TextView classSpinnerTxtTwo;
    private StatisticsGraphView timeAnalyzeView;
    private TextView tv3;
    private LinearLayout llPienodata;
    private LinearLayout llLeftPir;
    private ImageView ivNodata;
    private PieChart chart;
    private LinearLayout llGridView;
    private LinearLayout llTitle;
    private TextView selectTvTwo;
    private NoScrollGridView gridView;

    private List<String> parties = new ArrayList<>();
    private List<Float> percent = new ArrayList<>();
    private SpinerPopWindow timeChoosePop;
    private TeachStatisticsAdapter teachStatisticsAdapter;
    private TeachStatisticsVo teachStatistics;

    private int subjectId;

    public static StatisticsFragment newInstance(Bundle bundle) {
        StatisticsFragment fragment = new StatisticsFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new AnalysisStatisticsPresenter();
        mPresenter.attachView(this, activity);
        //parties.put
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View rootView = inflater.inflate(R.layout.fragment_statistics, container, false);
        tv = (TextView) rootView.findViewById(R.id.tv);
        tvNumber = (TextView) rootView.findViewById(R.id.tv_number);
        tv2 = (TextView) rootView.findViewById(R.id.tv2);
        classSpinnerTxtTwo = (TextView) rootView.findViewById(R.id.class_spinner_txt_two);
        timeAnalyzeView = (StatisticsGraphView) rootView.findViewById(R.id.time_analyze_view);
        tv3 = (TextView) rootView.findViewById(R.id.tv3);
        llPienodata = (LinearLayout) rootView.findViewById(R.id.ll_pienodata);
        llLeftPir = (LinearLayout) rootView.findViewById(R.id.ll_left_pir);
        ivNodata = (ImageView) rootView.findViewById(R.id.iv_nodata);
        chart = (PieChart) rootView.findViewById(R.id.chart1);
        llGridView = (LinearLayout) rootView.findViewById(R.id.ll_gridView);
        llTitle = (LinearLayout) rootView.findViewById(R.id.ll_title);
        selectTvTwo = (TextView) rootView.findViewById(R.id.select_tv_two);
        gridView = (NoScrollGridView) rootView.findViewById(R.id.gridView);

        classSpinnerTxtTwo.setOnClickListener(this);
        //x.view().inject(this, rootView);

        tfRegular = Typeface.createFromAsset(activity.getAssets(), "OpenSans-Regular.ttf");
        tfLight = Typeface.createFromAsset(activity.getAssets(), "OpenSans-Light.ttf");
        initView();
        initChartView();
        initData();
        return rootView;
    }

    private void initData() {
        subjectId = getArguments().getInt(SUBJECTID);
        mPresenter.showStudentAssessementHistory(subjectId, 1);
//        mPresenter.getStatistics("1", "1", "1");

//        mPresenter.showStudentAssessementHistory(subjectId,1);
    }

    private void initView() {
        tvNumber.setText("");
        tvNumber.setSelected(true);
        tvNumber.setBackground(getResources().getDrawable(R.drawable.bg_select_selector_2));
        selectTvTwo.setSelected(true);
        selectTvTwo.setText("");
        selectTvTwo.setBackground(getResources().getDrawable(R.drawable.bg_select_selector_2));

        timeChoosePop = new SpinerPopWindow(getActivity());
        timeChoosePop.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                classSpinnerTxtTwo.setText(pos == 0 ? "近一周" : "近一月");
                onTime(pos);
            }
        });
        List<String> timeList = new ArrayList<>();
        timeList.add("近一周");
        timeList.add("近一月");
        timeChoosePop.setPopDatas(timeList);
    }

    private void initChartView() {
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setCenterTextTypeface(tfLight);
        chart.setCenterText("");


        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(0);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        chart.animateY(1400, Easing.EaseInOutQuad);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(7f);
        l.setYOffset(0f);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);

        //new 1
        chart.setDrawEntryLabels(false);
    }

    private void setData(int count, float range) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry(percent.get(i),
                    parties.get(i),
                    getResources().getDrawable(R.drawable.star)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");//未做知识点

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(12f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(tfLight);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
        chart.setCenterTextSize(12f);
        chart.setCenterTextColor(0xff323C47);
        chart.setCenterTextTypeface(Typeface.DEFAULT_BOLD);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // ButterKnife.unbind(this);
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void onValueSelected(Entry entry, Highlight highlight) {
        PieEntry pieEntry = (PieEntry) entry;

        if (pieEntry.getLabel().length() > 3) {
            StringBuilder sb = new StringBuilder(pieEntry.getLabel());
            sb.insert(3, "\n");
            chart.setCenterText(sb.toString());
        } else {
            return;
        }

        if (pieEntry.getLabel().substring(0, 2).contains("A+")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getAplus().getPointNamesForAplus(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("A")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getA().getPointNamesForA(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("B")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getB().getPointNamesForB(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("C")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getC().getPointNamesForC(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("D")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getD().getPointNamesForD(), getActivity());
        } else if (pieEntry.getLabel().substring(0, 1).contains("E")) {
            teachStatisticsAdapter = new TeachStatisticsAdapter(
                    teachStatistics.getPieChart().getE().getPointNamesForE(), getActivity());
        }
        String str = (String) pieEntry.getLabel().subSequence(0, 1);
        String str1 = (String) pieEntry.getLabel().subSequence(0, 2);
        if (str.length() == 1) {
            selectTvTwo.setText(str);
        } else if (str.length() == 2) {
            selectTvTwo.setText(str1);
        }
        //selectTvTwo.setText(pieEntry.getLabel().substring(0, 2));
        gridView.setAdapter(teachStatisticsAdapter);
    }

    @Override
    public void onNothingSelected() {
    }

    @Override
    public void setTeachStatisticsVo(TeachStatisticsVo teachStatisticsVo) {
        teachStatistics = teachStatisticsVo;
        chart.clear();
        tvNumber.setText(teachStatisticsVo.getWholeLevel());
        parties.clear();
        parties.add("A+  " + teachStatisticsVo.getPieChart().getAplus().getAccuracyForAplus());
        parties.add("A  " + teachStatisticsVo.getPieChart().getA().getAccuracyForA());
        parties.add("B  " + teachStatisticsVo.getPieChart().getB().getAccuracyForB());
        parties.add("C  " + teachStatisticsVo.getPieChart().getC().getAccuracyForC());
        parties.add("D  " + teachStatisticsVo.getPieChart().getD().getAccuracyForD());
        parties.add("E  " + teachStatisticsVo.getPieChart().getE().getAccuracyForE());

        percent.clear();
        percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getAplus().getAccuracyForAplus().split("%")[0]));
        percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getA().getAccuracyForA().split("%")[0]));
        percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getB().getAccuracyForB().split("%")[0]));
        percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getC().getAccuracyForC().split("%")[0]));
        percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getD().getAccuracyForD().split("%")[0]));
        percent.add(Float.parseFloat(teachStatisticsVo.getPieChart().getE().getAccuracyForE().split("%")[0]));
        setData(6, 10);

        for (IDataSet<?> set : chart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
        chart.invalidate();
        teachStatisticsAdapter = new TeachStatisticsAdapter(teachStatisticsVo.getPieChart().getAplus().getPointNamesForAplus(), getActivity());

        gridView.setAdapter(teachStatisticsAdapter);
        //new
        if (percent.get(0) > 0) {
            setCenterStr(parties.get(0));
        } else if (percent.get(1) > 0) {
            setCenterStr(parties.get(1));
        } else if (percent.get(2) > 0) {
            setCenterStr(parties.get(2));
        } else if (percent.get(3) > 0) {
            setCenterStr(parties.get(3));
        } else if (percent.get(4) > 0) {
            setCenterStr(parties.get(4));
        } else if (percent.get(5) > 0) {
            setCenterStr(parties.get(5));
        }
        selectTvTwo.setText("A+");
    }

    private void setCenterStr(String str) {
        if (str.length() > 3) {
            StringBuilder sb = new StringBuilder(parties.get(0));
            sb.insert(3, "\n");
            chart.setCenterText(sb.toString());
        }
    }

    private List<StatisticsGraphView.ItemInfoVo> showitemVo = new ArrayList<>();

    @Override
    public void setLineViewBean(List<StatisticsGraphView.LineViewBean> lineViewBean) {
        for (int i = 0; i < lineViewBean.size(); i++) {
            showitemVo = lineViewBean.get(i).getItemVo();
        }
        if (lineViewBean != null) {
            timeAnalyzeView.setData(lineViewBean);
            chart.setVisibility(View.VISIBLE);
            llPienodata.setVisibility(View.GONE);
            llGridView.setVisibility(View.VISIBLE);
        } else {
            llGridView.setVisibility(View.GONE);
            llPienodata.setVisibility(View.VISIBLE);
            chart.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.class_spinner_txt_two) {
            timeChoosePop.showAsDropDown(classSpinnerTxtTwo);
        }
    }
 /*   @OnClick({R.id.class_spinner_txt_two})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.class_spinner_txt_two:
                timeChoosePop.showAsDropDown(classSpinnerTxtTwo);
                break;
        }
    }*/

    private void onTime(int pos) {
        if (pos == 0) {
            //mPresenter.getStatistics(classVo.getData().get(0).getClassId() + "", SPUtils.getCurrentSubject().getSubjectId() + "", "1");
//            mPresenter.getStatistics("1", "1", "1");
            mPresenter.showStudentAssessementHistory(subjectId, 1);
        } else {
            //mPresenter.getStudentStatistics(classGrouping.getData().get((id - 1)).getStudentId() + "", SPUtils.getCurrentSubject().getSubjectId() + "", "1");
//            mPresenter.getStatistics("1", "1", "2");
            mPresenter.showStudentAssessementHistory(subjectId, 2);
        }
    }
}
