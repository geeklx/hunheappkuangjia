package com.example.app1xqfx.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

public interface AnalyzeContentView extends BaseView {

     void onSubjectSuccess(List<SubjectVo.DataBean> retList);

     void networkError(String str);

}
