package com.example.app1xqfx.listener;


import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

/**
 *
 */
public interface OnAnalyzeListener {
    void onSubjectItemClick(int position);

    void onSearch(String searchStr);

    void onStatusChanged(int status);

    void onSubjectGet(List<SubjectVo.DataBean> subjects);

    void setOnPageChangeListener(OnPageChangeListener onPageChangeListener);
}
