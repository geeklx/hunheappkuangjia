package com.sdzn.fzx.student.libbase.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.sdzn.fzx.student.libbase.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 *
 */
public class UpgradeTreeItemHolder extends TreeNode.BaseNodeViewHolder<UpgradeTreeItemHolder.TreeItem> {
    private PrintView arrowView;

    public UpgradeTreeItemHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, TreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_upgrade_node, null, false);
        View content = view.findViewById(R.id.content);
        content.setBackgroundColor(value.parentId == 0 ? Color.WHITE : Color.parseColor("#EDF5FF"));

        TextView tvName = view.findViewById(R.id.tv_name);
        tvName.setText(value.name);
        ImageView star1 = view.findViewById(R.id.iv_star1);
        ImageView star2 = view.findViewById(R.id.iv_star2);
        ImageView star3 = view.findViewById(R.id.iv_star3);
        ImageView star4 = view.findViewById(R.id.iv_star4);
        ImageView star5 = view.findViewById(R.id.iv_star5);
        if (value.degree <16.6d) {
            star1.setImageResource(R.mipmap.gray_star);
            star2.setImageResource(R.mipmap.gray_star);
            star3.setImageResource(R.mipmap.gray_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 33.3d&&value.degree>=16.6d) {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.gray_star);
            star3.setImageResource(R.mipmap.gray_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 50d&&value.degree>=33.3d) {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.gray_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 66.6d&&value.degree>=50d) {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.orange_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 83.3d&&value.degree>=66.6d){
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.orange_star);
            star4.setImageResource(R.mipmap.orange_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree>=83.3d){
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.orange_star);
            star4.setImageResource(R.mipmap.orange_star);
            star5.setImageResource(R.mipmap.orange_star);
        }

        arrowView = view.findViewById(R.id.arrow_icon);

        return view;
    }

    @Override
    public void toggle(boolean active) {
        arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
    }

    public static class TreeItem {
        public long id;
        public long parentId;
        public String name;
        public double degree;

        public TreeItem(long id, long parentId, String name, double degree) {
            this.id = id;
            this.name = name;
            this.degree = degree;
            this.parentId = parentId;
        }
    }
}
