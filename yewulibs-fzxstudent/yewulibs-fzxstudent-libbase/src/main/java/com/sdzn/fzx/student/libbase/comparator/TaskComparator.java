package com.sdzn.fzx.student.libbase.comparator;


import com.sdzn.fzx.student.vo.TaskListVo;

import java.util.Comparator;

/**
 * 任务排序器
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class TaskComparator implements Comparator<Object> {
    @Override
    public int compare(Object t1, Object t2) {
        long startTime1 = ((TaskListVo.DataBean)t1).getStartTime();
        long startTime2 = ((TaskListVo.DataBean)t2).getStartTime();
        if (startTime1 > startTime2) {
            return -1;
        } else if (startTime1 < startTime2) {
            return 1;
        } else {
            return 0;
        }
    }
}
