package com.sdzn.fzx.student.libbase.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.tqltech.tqlpencomm.Dot;
import com.tqltech.tqlpencomm.PenCommAgent;
import com.tqltech.tqlpencomm.PenStatus;
import com.tqltech.tqlpencomm.listener.TQLPenSignal;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * QTL手写笔蓝牙服务
 */
public class BluetoothLEService extends Service {
    private String mBluetoothDeviceAddress;
    /*================  ================*/
    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "ACTION_DATA_AVAILABLE";
    public final static String ACTION_PEN_STATUS_CHANGE = "ACTION_PEN_STATUS_CHANGE";
    public final static String RECEVICE_DOT = "RECEVICE_DOT";

    public final static String DEVICE_DOES_NOT_SUPPORT_UART = "DEVICE_DOES_NOT_SUPPORT_UART";
    public static final int CONNECT_BLE_PEN = 2233;
    public static final int GET_ALL_STATE = 3322;
    /*================  ================*/


    private PenCommAgent bleManager;
    private boolean isPenConnected = false;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @SuppressLint("MissingPermission")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == CONNECT_BLE_PEN) {
                boolean isOpen = (boolean) StudentSPUtils.get(App2.get(), "TQLPen", false);
                if (isOpen) {
                    if (!isPenConnected) {//若未连接, 就尝试连接
                        String addr = StudentSPUtils.getLastPenAddr();
                        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                        if (!addr.isEmpty() && adapter != null && adapter.isEnabled()) {
                            initialize();
                            connect(addr);
                        }
                        mHandler.sendEmptyMessageDelayed(CONNECT_BLE_PEN, 3000);
                    } else {//若已连接
                        if (bleManager == null) {
                            initialize();
                        }
                        try {
                            bleManager.getPenMac();
                            Thread.sleep(100);
                            bleManager.getPenPowerStatus();
//                            bleManager.getPenAllStatus();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mHandler.sendEmptyMessageDelayed(GET_ALL_STATE, 60000);//每1min进行一次轮询
                    }
                } else {//未开启手写笔连接, 3秒轮询
                    mHandler.sendEmptyMessageDelayed(CONNECT_BLE_PEN, 3000);
                }
            } else if (msg.what == GET_ALL_STATE) {
                if (!isPenConnected) {
                    mHandler.sendEmptyMessageDelayed(CONNECT_BLE_PEN, 3000);
                    return;
                }
                if (bleManager == null) {
                    initialize();
                }
                try {
                    bleManager.getPenMac();
                    Thread.sleep(100);
                    bleManager.getPenPowerStatus();
//                        bleManager.getPenAllStatus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mHandler.sendEmptyMessageDelayed(GET_ALL_STATE, 60000);
            }

        }
    };

    private void penConnect() {
        synchronized (BluetoothLEService.class) {
            CommonUtils.flag = 1;
            isPenConnected = true;
            Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
            while (iterator.hasNext()) {
                iterator.next().onConnected();
            }
        }
    }

    private void penDisconnect() {
        synchronized (BluetoothLEService.class) {
            CommonUtils.flag = 0;
//                    AppLike.mPenName = "";
            CommonUtils.mBTMac = "";
            CommonUtils.mBattery = 0;
            isPenConnected = false;
            mHandler.removeMessages(GET_ALL_STATE);
            mHandler.sendEmptyMessageDelayed(CONNECT_BLE_PEN, 3000);
            Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDisconnected();
            }
        }
    }

//    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            final String action = intent.getAction();
//            if (action == null) {
//                return;
//            }
//            switch (action) {
//                case ACTION_GATT_CONNECTED:
//                    penConnect();
//                    break;
//                case ACTION_GATT_DISCONNECTED:
//                    penDisconnect();
//                    break;
//            }
//        }
//    };

    /**
     * 获取手写笔连接状态
     */
    public boolean isPenConnected() {
        return isPenConnected;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mHandler.sendEmptyMessage(CONNECT_BLE_PEN);
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 发送广播
     */
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        public BluetoothLEService getService() {
            return BluetoothLEService.this;
        }
    }

    private final LocalBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        regReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        close();
//        unregisterReceiver(mGattUpdateReceiver);
    }

//    private void regReceiver() {
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(BluetoothLEService.ACTION_GATT_CONNECTED);
//        intentFilter.addAction(BluetoothLEService.ACTION_GATT_DISCONNECTED);
//        intentFilter.addAction(BluetoothLEService.ACTION_GATT_SERVICES_DISCOVERED);
//        intentFilter.addAction(BluetoothLEService.ACTION_DATA_AVAILABLE);
//        intentFilter.addAction(BluetoothLEService.RECEVICE_DOT);
//        registerReceiver(mGattUpdateReceiver, intentFilter);
//    }

    /**
     * 初始化蓝牙adapter
     *
     * @return true=初始化成功, false=初始化失败.
     */
    public boolean initialize() {
        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(getApplication());
        }
        bleManager.setTQLPenSignalListener(mPenSignalCallback);

        if (!bleManager.isSupportBluetooth()) {
            Log.e("该设备不支持蓝牙");
            return false;
        }

        if (!bleManager.isSupportBLE()) {
            Log.e("该设备不支持BLE");
            return false;
        }
        return true;
    }

    /**
     * 连接到指定蓝牙地址上
     *
     * @return
     */
    public boolean connect(final String address) {
        if (address == null) {
            Log.w("蓝牙适配器未初始化或未指定地址");
            return false;
        }
        if (bleManager == null) {
            initialize();
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && bleManager.isConnect(address)) {
//            Log.i("尝试使用现有手写笔进行连接");
            StudentSPUtils.saveLastPenAddr(address);
            return true;
        }

        Log.i("尝试创建新连接...");
        mBluetoothDeviceAddress = address;
        boolean flag = bleManager.connect(address);
        if (!flag) {
            Log.i("bleManager.connect(address)-----false");
            return false;
        }
//        isPenConnected = true;
//        penConnect();
        Log.i("bleManager.connect(address)-----true");
        StudentSPUtils.saveLastPenAddr(address);
        return true;
    }

    /**
     * 断开连接
     */
    public void disconnect() {
        if (bleManager != null) {
            if (mBluetoothDeviceAddress != null) {
                bleManager.disconnect(mBluetoothDeviceAddress);
            }
            isPenConnected = false;
            mHandler.removeMessages(GET_ALL_STATE);
            mHandler.sendEmptyMessageDelayed(CONNECT_BLE_PEN, 3000);
            CommonUtils.flag = 0;
            penDisconnect();
        }
    }

    /**
     * 关闭蓝牙适配器
     */
    public void close() {
        if (bleManager == null) {
            return;
        }

        Log.e("mBluetoothGatt closed");
        mHandler.removeCallbacksAndMessages(null);
        if (mBluetoothDeviceAddress != null) {
            bleManager.disconnect(mBluetoothDeviceAddress);
        } else {
            bleManager.disconnect(StudentSPUtils.getLastPenAddr());
        }
        mBluetoothDeviceAddress = null;
        bleManager = null;
//        AppLike.mAppLike.mBLEServiceManger = null;
    }

    /// ===========================================================
    private static final Set<OnDataReceiveListener> mListenerList = Collections.synchronizedSet(new HashSet<OnDataReceiveListener>());

    /**
     * 数据回调接口
     */
    public interface OnDataReceiveListener {

        void onConnected();

        void onDisconnected();

        void onDataReceive(Dot dot);

        void onOfflineDataReceive(Dot dot);

        void onFinishedOfflineDown(boolean success);

        void onOfflineDataNum(int num);

        void onReceiveOIDSize(int OIDSize);

        void onReceiveOfflineProgress(int i);

        void onDownloadOfflineProgress(int i);

        void onReceivePenLED(byte color);

        void onOfflineDataNumCmdResult(boolean success);

        void onDownOfflineDataCmdResult(boolean success);

        void onWriteCmdResult(int code);

        void onReceivePenAllStatus(PenStatus status);
    }

    public static abstract class BaseOnDataReceiveListener implements OnDataReceiveListener {

        @Override
        public void onConnected() {
        }

        @Override
        public void onDisconnected() {
        }

        @Override
        public void onDataReceive(Dot dot) {

        }

        @Override
        public void onOfflineDataReceive(Dot dot) {

        }

        @Override
        public void onFinishedOfflineDown(boolean success) {

        }

        @Override
        public void onOfflineDataNum(int num) {

        }

        @Override
        public void onReceiveOIDSize(int OIDSize) {

        }

        @Override
        public void onReceiveOfflineProgress(int i) {

        }

        @Override
        public void onDownloadOfflineProgress(int i) {

        }

        @Override
        public void onReceivePenLED(byte color) {

        }

        @Override
        public void onOfflineDataNumCmdResult(boolean success) {
        }

        @Override
        public void onDownOfflineDataCmdResult(boolean success) {
        }

        @Override
        public void onWriteCmdResult(int code) {
        }

        @Override
        public void onReceivePenAllStatus(PenStatus status) {
        }
    }

    public void addOnDataReceiveListener(OnDataReceiveListener dataReceiveListener) {
        if (dataReceiveListener == null) {
            return;
        }
        synchronized (BluetoothLEService.class) {
            mListenerList.add(dataReceiveListener);
        }
    }

    public void removeOnDataReceiveListener(OnDataReceiveListener dataReceiveListener) {
        if (dataReceiveListener == null) {
            return;
        }
        synchronized (BluetoothLEService.class) {
            mListenerList.remove(dataReceiveListener);
        }
    }

    public void removeAllDataReceiverListener() {
        mListenerList.clear();
    }

    private TQLPenSignal mPenSignalCallback = new TQLPenSignal() {

        //设备 连接成功
        @Override
        public void onConnected() {//连接
            Log.i("TQLPenSignal had Connected");
//            broadcastUpdate(ACTION_GATT_CONNECTED);
            if (isPenConnected) {
                return;
            }
            penConnect();
            Log.i("Connected to GATT server.");
        }

        //设备 断开 后。
        @Override
        public void onDisconnected() {//断开连接
            String intentAction;
            Log.i("TQLPenSignal had onDisconnected");
//            intentAction = ACTION_GATT_DISCONNECTED;
//            Log.i("C.");
            if (isPenConnected) {
                penDisconnect();
//                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onConnectFailed() {
            Log.i("TQLPenSignal had onDisconnected");
            if (isPenConnected) {
                penDisconnect();
            }
        }

        @Override
        public void onWriteCmdResult(int code) {
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onWriteCmdResult(code);
                }
            }
        }

        @Override
        public void onDownOfflineDataCmdResult(boolean isSuccess) {
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onDownOfflineDataCmdResult(isSuccess);
                }
            }
        }

        @Override
        public void onOfflineDataListCmdResult(boolean isSuccess) {
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onOfflineDataNumCmdResult(isSuccess);
                }
            }
        }

        @Override
        public void onOfflineDataList(int offlineNotes) {
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onOfflineDataNum(offlineNotes);
                }
            }
        }

        @Override
        public void onStartOfflineDownload(boolean isSuccess) {
        }

        @Override
        public void onFinishedOfflineDownload(boolean isSuccess) {
            Log.i("-------offline download success-------");
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onFinishedOfflineDown(isSuccess);
                }
            }
        }

        @Override
        public void onReceiveOfflineStrokes(Dot dot) {
            Log.i(dot.toString());
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onOfflineDataReceive(dot);
                }
            }
        }

        @Override
        public void onDownloadOfflineProgress(int i) {
            Log.e("DownloadOfflineProgress----" + i);
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onDownloadOfflineProgress(i);
                }
            }
        }

        @Override
        public void onReceiveOfflineProgress(int i) {
            Log.e("onReceiveOfflineProgress----" + i);
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceiveOfflineProgress(i);
                }
            }
        }

        @Override
        public void onPenConfirmRecOfflineDataResponse(boolean b) {

        }

        @Override
        public void onPenDeleteOfflineDataResponse(boolean b) {

        }

        @Override
        public void onReceivePresssureValue(int i, int i1) {

        }

        /**
         * 获取在线笔记数据
         */
        @Override
        public void onReceiveDot(Dot dot) {
//            Log.e("bluetooth service recivice=====" + dot.toString());
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onDataReceive(dot);
                }
            }
        }


        /**
         * @param isUp true-落笔 false 抬笔
         */
        @Override
        public void onUpDown(boolean isUp) {

        }

        /**
         * 设置笔名回调
         * @param bIsSuccess true-成功 false-失败
         */
        @Override
        public void onPenNameSetupResponse(boolean bIsSuccess) {
//            if (bIsSuccess) {
//                AppLike.mPenName = AppLike.tmp_mPenName;
//            }
//            String intentAction = ACTION_PEN_STATUS_CHANGE;
//            Log.i("Disconnected from GATT server.");
//            broadcastUpdate(intentAction);
        }

        /**
         * 校正rtc时间回调
         */
        @Override
        public void onPenTimetickSetupResponse(boolean bIsSuccess) {
//            if (bIsSuccess) {
//                AppLike.mTimer = AppLike.tmp_mTimer;
//            }
//            String intentAction = ACTION_PEN_STATUS_CHANGE;
//            Log.i("Disconnected from GATT server.");
//            broadcastUpdate(intentAction);
        }

        /**
         * 设置自动关机时间
         */
        @Override
        public void onPenAutoShutdownSetUpResponse(boolean bIsSuccess) {
//            if (bIsSuccess) {
//                AppLike.mPowerOffTime = AppLike.tmp_mPowerOffTime;
//            }
//            Log.i("Disconnected from GATT server.");
//            broadcastUpdate(ACTION_PEN_STATUS_CHANGE);
        }

        /**
         * 恢复出厂设置
         */
        @Override
        public void onPenFactoryResetSetUpResponse(boolean bIsSuccess) {
        }

        /**
         * 设置自动关机模式回调
         */
        @Override
        public void onPenAutoPowerOnSetUpResponse(boolean bIsSuccess) {
//            if (bIsSuccess) {
//                AppLike.mPowerOnMode = AppLike.tmp_mPowerOnMode;
//            }
//            Log.i("Disconnected from GATT server.");
//            broadcastUpdate(ACTION_PEN_STATUS_CHANGE);
        }

        /**
         * 设置蜂鸣器状态回调
         */
        @Override
        public void onPenBeepSetUpResponse(boolean bIsSuccess) {
//            if (bIsSuccess) {
//                AppLike.mBeep = AppLike.tmp_mBeep;
//            }
//            Log.i("Disconnected from GATT server.");
//            broadcastUpdate(ACTION_PEN_STATUS_CHANGE);
        }

        /**
         * 设置笔灵敏度回调
         */
        @Override
        public void onPenSensitivitySetUpResponse(boolean bIsSuccess) {
//            if (bIsSuccess) {
//                AppLike.mPenSens = AppLike.tmp_mPenSens;
//            }
//            Log.i("Disconnected from GATT server.");
//            broadcastUpdate(ACTION_PEN_STATUS_CHANGE);
        }

        /**
         * 设置笔LED颜色回调
         */
        @Override
        public void onPenLedConfigResponse(boolean bIsSuccess) {
        }

        /**
         * 设置笔点码类型
         */
        @Override
        public void onPenDotTypeResponse(boolean bIsSuccess) {
        }

        /**
         * 设置切换呼吸灯功能是否开启回调
         */
        @Override
        public void onPenChangeLedColorResponse(boolean bIsSuccess) {
        }

        @Override
        public void onPenOTAMode(boolean bIsSuccess) {
        }

        @Override
        public void onReceivePenAllStatus(PenStatus status) {
            CommonUtils.mBattery = status.mPenBattery;
//            AppLike.mUsedMem = status.mPenMemory;
//            AppLike.mTimer = status.mPenTime;
//            Log.e("ApplicationResources.mTimer is " + AppLike.mTimer + ", status is " + status.toString());
//            AppLike.mPowerOnMode = status.mPenPowerOnMode;
//            AppLike.mPowerOffTime = status.mPenAutoOffTime;
//            AppLike.mBeep = status.mPenBeep;
//            AppLike.mPenSens = status.mPenSensitivity;
            //ApplicationResources.tmp_mEnableLED = status.mPenEnableLed;

//            AppLike.mPenName = status.mPenName;
            CommonUtils.mBTMac = status.mPenMac;
//            AppLike.mFirmWare = status.mBtFirmware;
//            AppLike.mMCUFirmWare = status.mPenMcuVersion;
//            AppLike.mCustomerID = status.mPenCustomer;

            broadcastUpdate(ACTION_PEN_STATUS_CHANGE);

            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceivePenAllStatus(status);
                }
            }
        }

        @Override
        public void onReceivePenMac(String penMac) {
            CommonUtils.mBTMac = penMac;
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceivePenAllStatus(null);
                }
            }
        }

        @Override
        public void onReceivePenName(String penName) {
        }

        @Override
        public void onReceivePenBtFirmware(String penBtFirmware) {
        }

        @Override
        public void onReceivePenTime(long penTime) {
        }

        @Override
        public void onReceivePenBattery(byte penBattery, Boolean bIsCharging) {
            CommonUtils.mBattery = penBattery;
            CommonUtils.mPenIsCharging = bIsCharging;
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceivePenAllStatus(null);
                }
            }
        }

        @Override
        public void onReceivePenMemory(byte penMemory) {
        }

        @Override
        public void onReceivePenAutoPowerOnModel(Boolean bIsOn) {
        }

        @Override
        public void onReceivePenBeepModel(Boolean bIsOn) {
        }

        @Override
        public void onReceivePenAutoOffTime(byte autoOffTime) {
        }

        @Override
        public void onReceivePenMcuVersion(String penMcuVersion) {
        }

        @Override
        public void onReceivePenCustomer(String penCustomerID) {
        }

        @Override
        public void onReceivePenSensitivity(byte penSensitivity) {
        }

        @Override
        public void onReceivePenType(byte penType) {
        }

        @Override
        public void onReceivePenDotType(byte penDotType) {
        }

        @Override
        public void onReceivePenDataType(byte penDataType) {
        }

        @Override
        public void onReceivePenLedConfig(byte penLedConfig) {
            Log.e("receive hand write color is " + penLedConfig);
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceivePenLED(penLedConfig);
                }
            }
        }

        @Override
        public void onReceivePenEnableLed(Boolean bEnableFlag) {
        }

        @Override
        public void onReceiveOIDFormat(long penOIDSize) {
            Log.e("onReceiveOIDFormat---> " + penOIDSize);
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceiveOIDSize((int) penOIDSize);
                }
            }
        }

        /**
         * 切换笔迹颜色
         * @param color
         */
        @Override
        public void onReceivePenHandwritingColor(byte color) {
            Log.e("receive hand write color is " + color);
            synchronized (BluetoothLEService.class) {
                Iterator<OnDataReceiveListener> iterator = mListenerList.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onReceivePenLED(color);
                }
            }
        }
    };
}
