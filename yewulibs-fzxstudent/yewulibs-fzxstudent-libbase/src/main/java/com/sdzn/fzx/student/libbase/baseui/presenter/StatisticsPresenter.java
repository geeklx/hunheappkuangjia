package com.sdzn.fzx.student.libbase.baseui.presenter;

import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.baseui.view.StatisticsView;
import com.sdzn.fzx.student.vo.StatisticsListVo;
import com.sdzn.fzx.student.vo.StatisticsScoreVo;
import com.sdzn.fzx.student.vo.StatisticsTimeVo;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 作答页面
 *
 * @author wangchunxiao
 * @date 2018/3/7
 */
public class StatisticsPresenter extends BasePresenter<StatisticsView, BaseActivity> {

    private Subscription subscribe;

    public void getStatisticScore(String taskId, String lessonTaskStudentId) {
        if (subscribe != null && subscribe.isUnsubscribed()) {
            subscribe.unsubscribe();
            subscriptions.remove(subscribe);
        }
        Map<String, String> params = new HashMap<>();
        params.put("taskId", taskId);
        params.put("studentId", lessonTaskStudentId);
        subscribe = Network.createTokenService(NetWorkService.AnswerService.class)
                .getStatisticScore(params)
                .map(new StatusFunc<StatisticsScoreVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StatisticsScoreVo>(new SubscriberListener<StatisticsScoreVo>() {
                    @Override
                    public void onNext(StatisticsScoreVo statisticsScoreVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getStatisticsScoreSuccess(statisticsScoreVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void getStatisticsTime(String taskId, String lessonTaskStudentId) {
        if (subscribe != null && subscribe.isUnsubscribed()) {
            subscribe.unsubscribe();
            subscriptions.remove(subscribe);
        }
        Map<String, String> params = new HashMap<>();
        params.put("taskId", taskId);
        params.put("studentId", lessonTaskStudentId);
        subscribe = Network.createTokenService(NetWorkService.AnswerService.class)
                .getStatisticTime(params)
                .map(new StatusFunc<StatisticsTimeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StatisticsTimeVo>(new SubscriberListener<StatisticsTimeVo>() {
                    @Override
                    public void onNext(StatisticsTimeVo statisticsTimeVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getStatisticsTimeSuccess(statisticsTimeVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void getStatisticsList(String taskId, String lessonTaskStudentId) {
        if (subscribe != null && subscribe.isUnsubscribed()) {
            subscribe.unsubscribe();
            subscriptions.remove(subscribe);
        }
        Map<String, String> params = new HashMap<>();
        params.put("lessonTaskId", taskId);
        params.put("lessonTaskStudentId", lessonTaskStudentId);
        subscribe = Network.createTokenService(NetWorkService.AnswerService.class)
                .getStatisticList(params)
                .map(new StatusFunc<StatisticsListVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StatisticsListVo>(new SubscriberListener<StatisticsListVo>() {
                    @Override
                    public void onNext(StatisticsListVo statisticsListVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getStatisticsListSuccess(statisticsListVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }
}
