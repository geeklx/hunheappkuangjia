package com.sdzn.fzx.student.libbase.app;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.sdzn.fzx.student.libutils.util.FileUtil;

import java.util.HashSet;

/**
 * @author Reisen at 2018-11-27
 */
public class FileDownloadManager {
    public static FileDownloadManager getInstance() {
        if (sManager == null) {
            synchronized (FileDownloadManager.class) {
                if (sManager == null) {
                    sManager = new FileDownloadManager();
                }
            }
        }
        return sManager;
    }

    private static FileDownloadManager sManager;
    private static HashSet<DownloadCallBack> mList;

    private FileDownloadManager() {
        mList = new HashSet<>();
    }

    public void download(String url) {
        String[] split = url.split("/");
        String fileName = split[split.length - 1];
        download(url, FileUtil.getAppFilesDir() + fileName);
    }

    public void download(String url, String path) {
        FileDownloader.getImpl().create(url).setPath(path)
                .setListener(new FileDownloadListener() {
                    @Override
                    protected void pending(BaseDownloadTask baseDownloadTask, int i, int i1) {
                    }

                    @Override
                    protected void progress(BaseDownloadTask baseDownloadTask, int i, int i1) {
                        FileDownloadManager.this.progress(baseDownloadTask.getUrl(), i, i1);
                    }

                    @Override
                    protected void completed(BaseDownloadTask baseDownloadTask) {
                        FileDownloadManager.this.completed(baseDownloadTask.getUrl(), baseDownloadTask.getPath());
                    }

                    @Override
                    protected void paused(BaseDownloadTask baseDownloadTask, int i, int i1) {
                    }

                    @Override
                    protected void error(BaseDownloadTask baseDownloadTask, Throwable throwable) {
                        FileDownloadManager.this.error(baseDownloadTask.getUrl(),throwable);
                    }

                    @Override
                    protected void warn(BaseDownloadTask baseDownloadTask) {
                    }
                }).start();
    }

    private synchronized void progress(String url, int soFar, int total) {
        for (DownloadCallBack back : mList) {
            back.progress(url, soFar, total);
        }
    }

    private synchronized void completed(String url, String path) {
        for (DownloadCallBack back : mList) {
            back.completed(url, path);
        }
    }

    private synchronized void error(String url, Throwable throwable) {
        for (DownloadCallBack back : mList) {
            back.error(url, throwable);
        }
    }

    public void reg(DownloadCallBack callBack) {
        mList.add(callBack);
    }

    public void unReg(DownloadCallBack callBack) {
        mList.remove(callBack);
    }

    public interface DownloadCallBack {
        void progress(String url, int soFar, int total);

        void completed(String url, String path);

        void error(String url, Throwable throwable);
    }
}
