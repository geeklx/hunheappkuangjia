package com.sdzn.fzx.student.libbase.listener;


import com.sdzn.fzx.student.vo.AnswerListBean;

/**
 * 试题题目点击事件接口
 * @author Reisen at 2018-11-16
 */
public interface OnExamResultClickListener {

    /**
     * 打开图片
     */
    void openImg(int index, String url, boolean showDel, AnswerListBean.AnswerDataBean bean);

    /**
     * 收藏/取消收藏
     *
     //* @param isCollect     true=要收藏,false=要取消收藏
     //* @param id            bean.id, 收藏时传入该参数, 结果返回studentBookId
     //* @param studentBookId bean.studentBookId 取消收藏时要上传的参数
     */
    void changeCollect(AnswerListBean.AnswerDataBean bean);

    /**
     * 打开资源文件
     */
    void showResource(AnswerListBean.AnswerDataBean bean);
}
