package com.sdzn.fzx.student.libbase.baseui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.RadioServiceManager;
import com.sdzn.fzx.student.libbase.baseui.activity.PlayerActivity;
import com.sdzn.fzx.student.libbase.event.RadioServiceEvent;
import com.sdzn.fzx.student.libbase.service.RadioService;
import com.sdzn.fzx.student.libpublic.views.VerticalSeekBar;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 描述：视频播放
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/17
 */
public class RadioViewFragment extends PlayerFragment implements RadioService.MediaPlayerCallBack, View.OnClickListener {

   /* @ViewInject(R.id.rl_title)
    RelativeLayout rlVideoTitle;
    @ViewInject(R.id.rlRadioArea)
    RelativeLayout rlRadioArea;
    @ViewInject(R.id.rlController)
    RelativeLayout rlController;
    @ViewInject(R.id.ivPlay)
    ImageView ivPlay;
    @ViewInject(R.id.tvPlayTime)
    TextView tvPlayTime;
    @ViewInject(R.id.seekbar)
    SeekBar seekbar;
    @ViewInject(R.id.volumeSeekBar)
    VerticalSeekBar volumeSeekBar;
    @ViewInject(R.id.llVolumeSeekBar)
    LinearLayout llVolumeSeekBar;
    @ViewInject(R.id.tvTotalTime)
    TextView tvTotalTime;
    @ViewInject(R.id.ivVolume)
    ImageView ivVolume;
    @ViewInject(R.id.iv_minimize)
    ImageView ivMiniMize;
    @ViewInject(R.id.tv_video_title)
    TextView tvVideoTitle;
    @ViewInject(R.id.ImRadioArea)
    ImageView imRadioArea;*/

    private RelativeLayout rlVideo;
    private RelativeLayout rlRadioArea;
    private ImageView imRadioArea;
    private RelativeLayout rlVideoTitle;
    private TextView tvVideoTitle;
    private TextView tvClose;
    private RelativeLayout rlController;
    private ImageView ivMiniMize;
    private ImageView ivVolume;
    private LinearLayout llVolumeSeekBar;
    private VerticalSeekBar volumeSeekBar;
    private LinearLayout llProgress;
    private ImageView ivPlay;
    private TextView tvPlayTime;
    private SeekBar seekbar;
    private TextView tvTotalTime;
    private String path;
    private boolean showMiniButton;

    private Timer timer = new Timer();
    private TimerTask timerTask;

    private boolean isDisplay = false;

    private static final int MSG_CONTROLLER = 0;
    private static final int HIDE_TIME = 3000;

    private int width;
    private int height;
    private String title;

    private AudioManager mAudioManager;

    private MyVolumeReceiver mVolumeReceiver;

    private boolean isPlaying;

    private boolean isTracking = false;

    private Animation rotateAnimation;

    public static RadioViewFragment newInstance(Bundle bundle) {
        RadioViewFragment fragment = new RadioViewFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radio_view, null);
        rlVideo = (RelativeLayout) view.findViewById(R.id.rl_video);
        rlRadioArea = (RelativeLayout) view.findViewById(R.id.rlRadioArea);
        imRadioArea = (ImageView) view.findViewById(R.id.ImRadioArea);
        rlVideoTitle = (RelativeLayout) view.findViewById(R.id.rl_title);
        tvVideoTitle = (TextView) view.findViewById(R.id.tv_video_title);
        tvClose = (TextView) view.findViewById(R.id.tv_close);
        rlController = (RelativeLayout) view.findViewById(R.id.rlController);
        ivMiniMize = (ImageView) view.findViewById(R.id.iv_minimize);
        ivVolume = (ImageView) view.findViewById(R.id.ivVolume);
        llVolumeSeekBar = (LinearLayout) view.findViewById(R.id.llVolumeSeekBar);
        volumeSeekBar = (VerticalSeekBar) view.findViewById(R.id.volumeSeekBar);
        llProgress = (LinearLayout) view.findViewById(R.id.llProgress);
        ivPlay = (ImageView) view.findViewById(R.id.ivPlay);
        tvPlayTime = (TextView) view.findViewById(R.id.tvPlayTime);
        seekbar = (SeekBar) view.findViewById(R.id.seekbar);
        tvTotalTime = (TextView) view.findViewById(R.id.tvTotalTime);

        ivPlay.setOnClickListener(this);
        ivVolume.setOnClickListener(this);
        tvClose.setOnClickListener(this);
        ivMiniMize.setOnClickListener(this);
        // x.view().inject(this, view);
        initView();
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null && (service.getCurrentState() == RadioService.PAUSE ||
                service.getCurrentState() == RadioService.PLAYBACK_COMPLETED)) {
            service.start();
            long duration = service.getDuration();
            ivPlay.setImageResource(R.mipmap.zanting_icon);
            tvTotalTime.setText(DateUtil.millsecondsToStr(duration));
            imRadioArea.startAnimation(rotateAnimation);
            hideControllerDelayed();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    playerHandler.sendEmptyMessage(0);
                }
            };
            timer.schedule(timerTask, 0, 1000);
            isPlaying = true;
        }
        playFunction(view);
        return view;
    }

    private void initView() {
        rotate();
        if (!showMiniButton) {
            ViewGroup.LayoutParams params = ivMiniMize.getLayoutParams();
            params.width = 0;
            ivMiniMize.setLayoutParams(params);
        }
        tvVideoTitle.setText(title);

        // 解决thumb透明问题
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekbar.setSplitTrack(false);
            volumeSeekBar.setSplitTrack(false);
        }

        rlRadioArea.setOnTouchListener(touchListener);
        rlController.setVisibility(View.VISIBLE);
//        if (setScreenSize) {
//            ivFullScreen.setVisibility(View.VISIBLE);
//        } else {
//            ivFullScreen.setVisibility(View.GONE);
//        }
        seekbar.setMax(100);

        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeSeekBar.setMax(max);
        volumeSeekBar.setProgress(current);

        setVolumeImage(max, current);

        mVolumeReceiver = new MyVolumeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.VOLUME_CHANGED_ACTION");
        getActivity().registerReceiver(mVolumeReceiver, filter);

        setListener();
    }

    private void setListener() {
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        volumeSeekBar.setOnSeekBarChangeListener(onVolumeSeekBarChangeListener);
    }

    /**
     * 处理音量变化时的界面显示
     *
     * @author long
     */
    private class MyVolumeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //如果音量发生变化则更改seekbar的位置
            int currVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);// 当前的媒体音量
            volumeSeekBar.setProgress(currVolume);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initData();
    }

    @Override
    public void onDestroy() {
        if (playerHandler != null) {
            playerHandler.removeCallbacksAndMessages(null);
            playerHandler = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null) {
            service.removeMediaPalyCallBack(this);
        }
        if (reStartService) {
            RadioServiceManager.getInstance().unbindService(getActivity());
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (mVolumeReceiver != null) {
            getActivity().unregisterReceiver(mVolumeReceiver);
        }
        super.onDestroy();
    }

    protected void initData() {
        showMiniButton = getArguments().getBoolean(PlayerActivity.SHOW_MINI_BUTTON);
        path = getArguments().getString("path");
        title = getArguments().getString("title");
        width = getArguments().getInt("width");
        height = getArguments().getInt("height");
    }

    private void playFunction(View view) {

        if (TextUtils.isEmpty(path)) {
            ToastUtil.showShortlToast(getString(R.string.fragment_task_content_text12));
        } else {
            RadioService service = RadioServiceManager.getInstance().getService();
            if (service == null) {
                reStartService = true;
                RadioServiceManager.getInstance().bindService(getActivity());
            } else {
                onServiceConnected(new RadioServiceEvent(service));
            }
        }
    }

    private boolean reStartService;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceConnected(RadioServiceEvent event) {
        if (event == null) {
            return;
        }
        RadioService service = event.getService();
        if (service == null) {
            return;
        }
        service.addMediaPlayCallBack(this);
        //打开后处理播放初始化事件
        if (TextUtils.equals(service.getPlayPath(), path)) {//路径相同, 判断状态
            switch (service.getCurrentState()) {
                case RadioService.IDLE:
                    try {
                        service.setDataSource(path, title);
                        service.prepareAsync();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case RadioService.INITLAIZED:
                    service.prepareAsync();
                    break;
                case RadioService.PREPARING:
                    break;
                case RadioService.PREPARED:
                    service.start();
                    break;
                case RadioService.STARTED:
                    break;
                case RadioService.PAUSE:
                    break;
                case RadioService.STOPED:
                    service.prepareAsync();
                    break;
                case RadioService.PLAYBACK_COMPLETED:
                    break;
                case RadioService.END:
                    service.reInitMediaPlayerAndPlay(path, title);
                    break;
                case RadioService.ERROR:
                    service.reInitMediaPlayerAndPlay(path, title);
                    break;
            }
        } else {//两路径不同, 重新加载
            service.reset();
            try {
                service.setDataSource(path, title);
                service.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBufferingUpdate(RadioService service, int percent) {
        seekbar.setSecondaryProgress((int) (((float) percent / (float) service.getDuration()) * seekbar.getMax()));
    }

    @Override
    public void onPrepared(RadioService service) {
        setScreenSizeParams(width, height);
        long duration = service.getDuration();
        ivPlay.setImageResource(R.mipmap.zanting_icon);
        tvTotalTime.setText(DateUtil.millsecondsToStr(duration));

        service.start();
        imRadioArea.startAnimation(rotateAnimation);
        hideControllerDelayed();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                playerHandler.sendEmptyMessage(0);
            }
        };
        timer.schedule(timerTask, 0, 1000);
        isPlaying = true;
    }

    @Override
    public void onCompletion(RadioService service) {
//        service.reset();
        isPlaying = false;
        ivPlay.setImageResource(R.mipmap.bofang_icon);
        imRadioArea.clearAnimation();
        seekbar.setProgress(0);
    }

    @Override
    public void onError(RadioService service, int what, int extra) {
    }

    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            RadioService service = RadioServiceManager.getInstance().getService();
            if (service != null) {
                service.seekTo(seekBar.getProgress() * service.getDuration() / seekBar.getMax());
            }
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
            isTracking = false;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
            isTracking = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            RadioService service = RadioServiceManager.getInstance().getService();
            if (service != null) {
                tvPlayTime.setText(DateUtil.millsecondsToStr(progress * service.getDuration() / seekBar.getMax()));
            }
        }
    };

    SeekBar.OnSeekBarChangeListener onVolumeSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, AudioManager.FLAG_VIBRATE);
            setVolumeImage(seekBar.getMax(), progress);
        }
    };

    public void setVolumeImage(int max, int progress) {
        if (progress > max / 2) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangda_icon));
        } else if (progress == 0) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangwu_icon));
        } else {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangxiao_icon));
        }
    }

    @Override
    public void onClick(View view) {
        RadioService service = RadioServiceManager.getInstance().getService();
        int i = view.getId();
        if (i == R.id.ivPlay) {
            hideControllerDelayed();
            if (service != null) {
                if (service.isPlaying()) {
                    isPlaying = false;
                    service.pause();
                    imRadioArea.clearAnimation();
                    ivPlay.setImageResource(R.mipmap.bofang_icon);
                    updateProgress();
                } else {
                    imRadioArea.startAnimation(rotateAnimation);
                    isPlaying = true;
                    service.start();
                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    updateProgress();
                }
            }
        } else if (i == R.id.ivVolume) {
            int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            volumeSeekBar.setProgress(current);
            llVolumeSeekBar.setVisibility(View.VISIBLE);

        } else if (i == R.id.tv_close) {
            ((PlayerActivity) getActivity()).back();

        } else if (i == R.id.iv_minimize) {
            ((PlayerActivity) getActivity()).miniMize();

        } else {
        }
    }
   /* @Event(value = {R.id.ivPlay, R.id.ivVolume, R.id.iv_minimize, R.id.tv_close, R.id.llProgress, R.id.rl_title, R.id.llVolumeSeekBar}, type = View.OnClickListener.class)
    private void onClick(View view) {
        RadioService service = RadioServiceManager.getInstance().getService();
        switch (view.getId()) {
            case R.id.ivPlay:
                hideControllerDelayed();
                if (service != null) {
                    if (service.isPlaying()) {
                        isPlaying = false;
                        service.pause();
                        imRadioArea.clearAnimation();
                        ivPlay.setImageResource(R.mipmap.bofang_icon);
                        updateProgress();
                    } else {
                        imRadioArea.startAnimation(rotateAnimation);
                        isPlaying = true;
                        service.start();
                        ivPlay.setImageResource(R.mipmap.zanting_icon);
                        updateProgress();
                    }
                }
                break;
            case R.id.ivVolume:
                int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                volumeSeekBar.setProgress(current);
                llVolumeSeekBar.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_close:
                ((PlayerActivity) activity).back();
                break;
            case R.id.iv_minimize:
                ((PlayerActivity) activity).miniMize();
                break;
            default:
                break;
        }
    }*/

    Handler playerHandler = new Handler() {
        public void handleMessage(Message msg) {
            // 更新播放进度
            if (isPlaying) {
                updateProgress();
            }
        }
    };

    private void updateProgress() {
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service == null) {
            return;
        }
        long position = service.getCurrentPosition();
        long duration = service.getDuration();

        if (duration > 0) {
            long pos = seekbar.getMax() * position / duration;
            tvPlayTime.setText(DateUtil.millsecondsToStr(position));
            if (!isTracking) {
                seekbar.setProgress((int) pos);
            }
        }
    }

    /**
     * 控制播放器面板显示
     */
    private View.OnTouchListener touchListener = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                controllerHandler.removeMessages(MSG_CONTROLLER);
                if (isDisplay) {
                    setLayoutVisibility(View.GONE, false);
                } else {
                    setLayoutVisibility(View.VISIBLE, true);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                hideControllerDelayed();
            }
            return true;
        }
    };

    private void hideControllerDelayed() {
        controllerHandler.removeMessages(MSG_CONTROLLER);
        controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
    }

    private void setLayoutVisibility(int visibility, boolean isDisplay) {
        this.isDisplay = isDisplay;
        rlController.setVisibility(visibility);
        rlVideoTitle.setVisibility(visibility);
        llVolumeSeekBar.setVisibility(View.GONE);
    }

    Handler controllerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setLayoutVisibility(View.GONE, false);
        }
    };

    @Override
    public void setScreenSizeParams(int width, int height) {
    }

    @Override
    public void onResume() {
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null && service.getCurrentState() == RadioService.PAUSE) {
            ivPlay.setImageResource(R.mipmap.zanting_icon);
            service.start();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null && service.getCurrentState() == RadioService.STARTED) {
            service.pause();
            ivPlay.setImageResource(R.mipmap.bofang_icon);
        }
        super.onPause();
    }

    //音乐旋转动画
    public void rotate() {
        rotateAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.im_rotate);
        LinearInterpolator lin = new LinearInterpolator();//设置动画匀速运动
        rotateAnimation.setInterpolator(lin);
    }
}