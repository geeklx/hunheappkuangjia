package com.sdzn.fzx.student.libbase.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.sdzn.fzx.student.libbase.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Bogdan Melnychuk on 2/15/15.
 */
public class UpgradeHeaderHolder extends TreeNode.BaseNodeViewHolder<UpgradeTreeItemHolder.TreeItem> {
    private PrintView arrowView;

    public UpgradeHeaderHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, UpgradeTreeItemHolder.TreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_upgrade_header, null, false);

        View content = view.findViewById(R.id.content);
        content.setBackgroundColor(value.parentId == 0 ? Color.WHITE : Color.parseColor("#EDF5FF"));


        TextView tvName = view.findViewById(R.id.tv_name);
        tvName.setText(value.name);
        ImageView star1 = view.findViewById(R.id.iv_star1);
        ImageView star2 = view.findViewById(R.id.iv_star2);
        ImageView star3 = view.findViewById(R.id.iv_star3);
        ImageView star4 = view.findViewById(R.id.iv_star4);
        ImageView star5 = view.findViewById(R.id.iv_star5);
        if (value.degree < 40) {
            star1.setImageResource(R.mipmap.gray_star);
            star2.setImageResource(R.mipmap.gray_star);
            star3.setImageResource(R.mipmap.gray_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 60) {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.gray_star);
            star3.setImageResource(R.mipmap.gray_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 70) {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.gray_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 85) {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.orange_star);
            star4.setImageResource(R.mipmap.gray_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else if (value.degree < 95){
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.orange_star);
            star4.setImageResource(R.mipmap.orange_star);
            star5.setImageResource(R.mipmap.gray_star);
        }else {
            star1.setImageResource(R.mipmap.orange_star);
            star2.setImageResource(R.mipmap.orange_star);
            star3.setImageResource(R.mipmap.orange_star);
            star4.setImageResource(R.mipmap.orange_star);
            star5.setImageResource(R.mipmap.orange_star);
        }

        arrowView = view.findViewById(R.id.arrow_icon);
        if (node.isLeaf()) {
            arrowView.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void toggle(boolean active) {
        arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
    }
}
