package com.sdzn.fzx.student.libbase.event;

import androidx.annotation.Nullable;

import com.sdzn.fzx.student.libbase.service.BluetoothLEService;


/**
 * @author Reisen at 2018-08-04
 */
public class BLEServiceEvent {
    private BluetoothLEService mService;

    public BLEServiceEvent(BluetoothLEService service) {
        mService = service;
    }

    @Nullable
    public BluetoothLEService getService() {
        return mService;
    }
}
