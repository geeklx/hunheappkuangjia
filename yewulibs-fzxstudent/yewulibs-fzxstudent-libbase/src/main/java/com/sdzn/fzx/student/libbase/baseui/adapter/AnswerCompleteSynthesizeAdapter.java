package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.listener.OnExamResultClickListener;
import com.sdzn.fzx.student.libpublic.utils.InputTools;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankCheckTextView;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.libutils.annotations.CheckState;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import java.util.Collections;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/20
 * 修改单号：
 * 修改内容:
 */
public class AnswerCompleteSynthesizeAdapter extends BaseAdapter {
    private OnExamResultClickListener mListener;
    private List<AnswerListBean.AnswerDataBean> mList;
    private Context mContext;
    private boolean isCorrect;
    private boolean showAnswer;

    public AnswerCompleteSynthesizeAdapter(List<AnswerListBean.AnswerDataBean> list, Context context, boolean isCorrect) {
        this.mList = list;
        this.mContext = context;
        this.isCorrect = isCorrect;
    }

    public void setListener(OnExamResultClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getExamTemplateId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = createItemView(getItemViewType(position));
        }
        if (convertView == null) {
            return null;
        }
        Object tag = convertView.getTag();
        if (tag instanceof SynSelectorHolder) {//选择判断
            bindSelectorData(position, convertView, (SynSelectorHolder) tag);
        } else if (tag instanceof SynShortAnswerHolder) {//简答
            bindShortAnswerData(position, convertView, (SynShortAnswerHolder) tag);
        } else if (tag instanceof SynFillBlankHolder) {//填空
            bindFillBlank(position, convertView, (SynFillBlankHolder) tag);
        }
        return convertView;
    }

    private View createItemView(int itemViewType) {
        View view;
        SynHolder holder;
        switch (itemViewType) {
            case 1://单选
            case 2://多选
            case 3://判断
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 4://简答
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_short_answer, null);
                holder = new SynShortAnswerHolder(view);
                view.setTag(holder);
                return view;
            case 6://填空
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_text, null);
                holder = new SynFillBlankHolder(view);
                view.setTag(holder);
                return view;
            default://综合题不嵌套综合题
                return LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_text, null);
        }
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
    }


    private static class SynHolder {
        TextView tvCount;
        TextView tvType;
        TextView tvScore;
        LinearLayout parsLy;
        HtmlTextView tvAnswer;
        HtmlTextView tvPars;

        SynHolder(View view) {
            tvCount = view.findViewById(R.id.tv_count);
            tvType = view.findViewById(R.id.tv_type);
            tvScore = view.findViewById(R.id.tv_score);
            parsLy = view.findViewById(R.id.pars_ly);
            tvAnswer = view.findViewById(R.id.tvAnswer);
            tvPars = view.findViewById(R.id.tv_pars);
        }
    }

    //单选多选判断
    private static class SynSelectorHolder extends SynHolder {
        HtmlTextView tvExam;//题干
        RadioGroup rg;//选项

        SynSelectorHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            rg = view.findViewById(R.id.rg_answer);
        }
    }

    //简答
    private static class SynShortAnswerHolder extends SynHolder {
        HtmlTextView tvExam;//题干
        CheckBox mCheckBox;//批改痕迹开关
        TextView tvUnAnswer;//未作答文本
        RecyclerView rvAddPic;//图片列表

        SynShortAnswerHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            mCheckBox = view.findViewById(R.id.switcher);
            tvUnAnswer = view.findViewById(R.id.tv_un_answer);
            rvAddPic = view.findViewById(R.id.rv_add_pic);
        }
    }

    //填空
    private static class SynFillBlankHolder extends SynHolder {
        FillBlankCheckTextView tvExam;

        SynFillBlankHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv);
        }
    }

    //统一设置标题
    private void bindItemTitle(int position, SynHolder holder) {
        AnswerListBean.AnswerDataBean bean = mList.get(position);
        holder.tvType.setText(getExamType(bean.getExamTemplateId()));
        holder.tvCount.setText("(" + bean.getExamSeq() + ")");
        int score = (int) bean.getScore();
        if (score <= 0 || bean.getExamBean() == null) {
            holder.tvScore.setVisibility(View.GONE);
        }else {
            holder.tvScore.setVisibility(View.VISIBLE);
            if (bean.getExamTemplateId() == 6) {
                score *= bean.getExamBean().getExamOptions().size();
            }
            holder.tvScore.setText("(本题" + score + "分)");
        }
        //===================== 答案 =====================
        holder.parsLy.setVisibility(showAnswer ? View.VISIBLE :View.GONE);
        holder.tvAnswer.setHtmlText(bean.getExamBean() == null ? "" : bean.getExamBean().getExamAnswer());
        holder.tvPars.setHtmlText(bean.getExamBean() == null ? "" : bean.getExamBean().getExamAnalysis());
    }

    //作答结果页面题型要通过id匹配
    private String getExamType(int type) {
        switch (type) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知题型";
        }
    }

    //选择判断
    private void bindSelectorData(int position, View convertView, SynSelectorHolder holder) {
        bindItemTitle(position, holder);
        AnswerListBean.AnswerDataBean bean = mList.get(position);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        if (examTextBean == null) {
            holder.tvExam.setHtmlText("");
        }else {
            holder.tvExam.setHtmlText(examTextBean.getExamStem());
        }

        RadioGroup rg = holder.rg;
        if (examTextBean == null) {
            rg.removeAllViews();
            return;
        }
        List<AnswerListBean.ExamOptions> options = examTextBean.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(mContext).inflate(R.layout.item_result_child_select, rg, true);
                childCount++;
            }
        }

        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            String str = String.valueOf((char) (65 + i));
            tvNumber.setText(str);
            tvText.setHtmlText(options.get(i).getContent());
            final List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextBean.getExamTypeId() == 1) {//单选/判断
                tvNumber.setBackground(mContext.getResources().getDrawable(
                        bean.getExamBean().getExamOptions().get(i).isRight()?
                        R.drawable.bg_result_select_selector_y :
                        R.drawable.bg_result_select_selector_n));
                tvNumber.setSelected(TextUtils.equals(str, examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (AnswerListBean.ExamOptionBean optionBean : examList) {
                    if (TextUtils.equals(str, optionBean.getMyAnswer())) {
                        tvNumber.setBackground(mContext.getResources().getDrawable(
                                bean.getExamBean().getExamOptions().get(i).isRight()?
                                R.drawable.bg_result_select_selector_y :
                                R.drawable.bg_result_select_selector_n));
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }
    }

    //填空
    private void bindFillBlank(int position, View convertView, SynFillBlankHolder holder) {
        bindItemTitle(position, holder);
        AnswerListBean.AnswerDataBean answerDataBean = mList.get(position);
        AnswerListBean.ExamTextBean examBean = answerDataBean.getExamBean();
        if (examBean == null) {
            holder.tvExam.setHtmlBody("");
            return;
        }
        List<AnswerListBean.ExamOptionBean> list = answerDataBean.getExamOptionList();
        SparseArray<ClozeAnswerBean> array = new SparseArray<>();
        array.clear();
        for (int i = 0; i < list.size(); i++) {
            AnswerListBean.ExamOptionBean optionBean = list.get(i);
            int checkState;
            if (isCorrect || InputTools.isKeyboard(optionBean.getAnswerType())) {
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                }else {
                    checkState = CheckState.FALSE;
                }
            }else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(), optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(),checkState));
        }
        holder.tvExam.setHtmlBody(examBean.getExamStem(),array);
        holder.tvExam.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                mListener.openImg(0,imageSrc,false,null);
            }
        });
    }

    //简答
    private void bindShortAnswerData(int position, View convertView, final SynShortAnswerHolder holder) {
        bindItemTitle(position,holder);

        final AnswerListBean.AnswerDataBean bean = mList.get(position);
        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        holder.tvExam.setVisibility(View.VISIBLE);
        HtmlTextView tv = holder.tvExam;
        tv.setHtmlText(examBean == null ? "" : examBean.getExamStem());

        //该题作答状态, true=未作答
        final boolean unAnswer = bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty() ||
                TextUtils.isEmpty(bean.getExamOptionList().get(0).getMyAnswer());
        if (unAnswer) {//未作答
            holder.tvUnAnswer.setVisibility(View.VISIBLE);
            holder.rvAddPic.setVisibility(View.GONE);
        } else {//已做达
            holder.tvUnAnswer.setVisibility(View.GONE);
            holder.rvAddPic.setVisibility(View.VISIBLE);
        }

        CheckBox s = holder.mCheckBox;
        if (unAnswer) {
            s.setOnCheckedChangeListener(null);
            return;
        }

        RecyclerView rv = holder.rvAddPic;
        rv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(mContext);
            rv.setAdapter(adapter);
            adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
                @Override
                public void clickImage(int index, String url) {
                    if (mListener != null) {
                        mListener.openImg(index, url, true, bean);
                    }
                }
            });
        }
        List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
        for (AnswerListBean.ExamOptionBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }

        s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String answer = isChecked ? bean.getExamOptionList().get(0).getMyAnswer() :
                        bean.getExamOptionList().get(0).getMyOldAnswer();
                ShortAnswerResultAdapter shortAdapter = (ShortAnswerResultAdapter) holder.rvAddPic.getAdapter();
                if (answer == null) {
                    shortAdapter.setList(new String[]{}, null);
                    return;
                }
                String[] arr = answer.split(";");
                if (arr.length == 1) {
                    shortAdapter.setList(arr[0].split(","), null);
                } else {
                    shortAdapter.setList(arr[1].split(","), arr[0].split(","));
                }
            }
        });
    }
}
