package com.sdzn.fzx.student.libbase.baseui.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.ExamTypeVo;
import com.sdzn.fzx.student.vo.SubjectErrorBean;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/12/11
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */
public interface ErrorNativeView extends BaseView {
    void setSubjectListSuccess(SubjectErrorBean subjectErrorBean);

    public void onExamTypeSuccess(List<ExamTypeVo.DataBean> list, List<String> strings);
}
