package com.sdzn.fzx.student.libbase.base;

import android.app.Activity;

import java.util.ArrayList;

/**
 * 描述：维护Activity栈
 * <p>
 * 创建人：zhangchao
 * 创建时间：16/7/28
 */
public class ActivityManager {

    private static final ArrayList<Activity> liveActivityList = new ArrayList<Activity>();
    private static final ArrayList<Activity> visibleActivityList = new ArrayList<Activity>();
    private static final ArrayList<Activity> foregroundActivityList = new ArrayList<Activity>();

    public static void addLiveActivity(Activity Activity) {
        if (!liveActivityList.contains(Activity)) {
            liveActivityList.add(Activity);
        }
    }

    public static void addVisibleActivity(Activity Activity) {
        if (!visibleActivityList.contains(Activity)) {
            visibleActivityList.add(Activity);
        }
    }

    public static void removeLiveActivity(Activity activity) {


        liveActivityList.remove(activity);
        visibleActivityList.remove(activity);
        foregroundActivityList.remove(activity);
    }

    public static void removeVisibleActivity(Activity Activity) {
        visibleActivityList.remove(Activity);
    }

    public static void addForegroundActivity(Activity Activity) {
        if (!foregroundActivityList.contains(Activity)) {
            foregroundActivityList.add(Activity);
        }
    }

    public static void removeForegroundActivity(Activity Activity) {
        foregroundActivityList.remove(Activity);
    }

    public static void finishAll() {
        for (Activity activity : liveActivityList) {
            activity.finish();
        }
        for (Activity activity : visibleActivityList) {
            activity.finish();
        }
        for (Activity activity : foregroundActivityList) {
            activity.finish();
        }
    }

    public static Activity getLiveActivity(Class<? extends BaseActivity> clazz) {
        for (Activity activity : liveActivityList) {
            if (activity.getClass().equals(clazz)) {
                return activity;
            }
        }
        return null;
    }

    public static void exit() {
        finishAll();
    }
}
