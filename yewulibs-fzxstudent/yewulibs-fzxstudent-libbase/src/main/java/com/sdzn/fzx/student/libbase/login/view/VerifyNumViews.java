package com.sdzn.fzx.student.libbase.login.view;


import com.haier.cellarette.libmvp.mvp.IView;

import java.io.InputStream;

/**
 * VerifyNumView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface VerifyNumViews extends IView {

    void OnImgTokenSuccess(Object imgtoken);

    void OnImgTokenFailed(String msg);

    void OnImgSuccess(InputStream img);

    void OnImgFailed(String msg);


    void OnVerifyMobilePhoneSuccess(String tel);//效验手机号成功

    void OnVerifyMobilePhoneFailed(String msg);//效验手机号失败

    void onVerityCodeSuccess(Object sueccess);//获取验证码成功
    void onVerityCodeFailed(String msg);//获取验证码失败

    void onCheckCodeSuccess(final String phone, final String code);//效验验证码成功
    void onCheckCodeFailed(String msg);//效验验证码失败
}
