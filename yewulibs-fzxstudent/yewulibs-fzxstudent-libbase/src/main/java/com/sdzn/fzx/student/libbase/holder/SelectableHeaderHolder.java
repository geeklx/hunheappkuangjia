package com.sdzn.fzx.student.libbase.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.sdzn.fzx.student.libbase.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Bogdan Melnychuk on 2/15/15.
 */
public class SelectableHeaderHolder extends TreeNode.BaseNodeViewHolder<IconTreeItemHolder.IconTreeItem> {
    private PrintView arrowView;

    public SelectableHeaderHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, IconTreeItemHolder.IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_selectable_header, null, false);
        TextView tvName = view.findViewById(R.id.tv_name);
        tvName.setText(value.name);
        TextView tvDegree = view.findViewById(R.id.tv_degree);
        TextView tvAccuracy = view.findViewById(R.id.tv_accuracy);
        TextView tvErrors = view.findViewById(R.id.tv_errors);
        TextView tvAmount = view.findViewById(R.id.tv_amount);
        if (value.last) {
            tvDegree.setText(value.degree);
            tvAccuracy.setText(value.accuracy);
            tvErrors.setText(value.errors);
            tvAmount.setText(value.amount);
        } else {
            tvDegree.setVisibility(View.GONE);
            tvAccuracy.setVisibility(View.GONE);
            tvErrors.setVisibility(View.GONE);
            tvAmount.setVisibility(View.GONE);
        }

        arrowView = view.findViewById(R.id.arrow_icon);
        if (node.isLeaf()) {
            arrowView.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    @Override
    public void toggle(boolean active) {
        arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
    }
}
