package com.sdzn.fzx.student.libbase.event;


import com.sdzn.fzx.student.libbase.service.RadioService;

/**
 * @author Reisen at 2018-12-06
 */
public class RadioServiceEvent {
    private RadioService mService;

    public RadioServiceEvent(RadioService service) {
        mService = service;
    }

    public RadioService getService() {
        return mService;
    }

    public void setService(RadioService service) {
        mService = service;
    }
}
