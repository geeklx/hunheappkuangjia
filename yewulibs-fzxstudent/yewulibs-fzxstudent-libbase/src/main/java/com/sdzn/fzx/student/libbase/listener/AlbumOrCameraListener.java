package com.sdzn.fzx.student.libbase.listener;

/**
 * 选择照片
 *
 * @author wangchunxiao
 * @date 2018/3/7
 */
public interface AlbumOrCameraListener {
    void selectAlbum();

    void selectCamera();
}
