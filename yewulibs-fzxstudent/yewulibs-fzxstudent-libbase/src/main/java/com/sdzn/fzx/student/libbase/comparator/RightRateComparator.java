package com.sdzn.fzx.student.libbase.comparator;


import com.sdzn.fzx.student.vo.RightRateVo;

import java.util.Comparator;

/**
 * 正确率排序器
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class RightRateComparator implements Comparator<RightRateVo.DataBean> {
    @Override
    public int compare(RightRateVo.DataBean r1, RightRateVo.DataBean r2) {
        long submitTime1 = r1.getSubmitTime();
        long submitTime2 = r2.getSubmitTime();
        if (submitTime1 > submitTime2) {
            return 1;
        } else if (submitTime1 < submitTime2) {
            return -1;
        } else {
            return 0;
        }
    }
}
