package com.sdzn.fzx.student.libbase.baseui.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libpublic.utils.OpenFileUtil;
import com.sdzn.fzx.student.libpublic.views.wps.Wps;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class DocViewActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout activityPptView;
    private ProgressBar pbLoading;


    private String path;
    private String target;
    private boolean isLoading = true;
    private Callback.Cancelable cancelable;
    private String type;

    private static final int REQUECT_CODE_SDCARD = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppt_view);
        activityPptView = (RelativeLayout) findViewById(R.id.activity_ppt_view);
        pbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        activityPptView.setOnClickListener(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(DocViewActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        if (target == null) {
            ToastUtil.showShortlToast("未找到目标文件");
            finish();
            return;
        }
        if (target.startsWith("http")) {
            String[] split = target.split(TextUtils.isEmpty(CommonUtils.qiniuDomain) ? "http://file.fuzhuxian.com/" : CommonUtils.qiniuDomain);
            String fileName = split[split.length - 1];
            String path;
            try {
                path = FileUtil.getResPath();
            } catch (Exception e) {
                e.printStackTrace();
                path = FileUtil.getAppFilesDir();
            }
            File file = new File(path, fileName);
            target = file.getAbsolutePath();
        }
        File file = new File(target);
        if (!file.exists()) {
            // 下载文件
            isLoading = true;
            pbLoading.setVisibility(View.VISIBLE);
            requestPermissiontest();
        } else {
            showDoc(file, type);
        }
    }

    @Override
    protected void initData() {
        path = getIntent().getStringExtra("path");
        type = getIntent().getStringExtra("type");
        try {
            target = path;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDoc(File file, String type) {
        Wps wps = new Wps(activity);
        String wpsVersion = wps.checkVersion();
        if (TextUtils.isEmpty(wpsVersion)) {
            Intent pptIntent = OpenFileUtil.getAttachmentIntent(file, type);
            if (OpenFileUtil.openAppNum(pptIntent) > 0) {
                activity.startActivity(pptIntent);
            } else {
                ToastUtil.showShortlToast(activity.getString(R.string.uninstalled_hint));
            }
        } else {
            wps.open(file);
        }
        this.finish();
    }

    public void requestPermissiontest() {
//        MPermissions.requestPermissions(this, REQUECT_CODE_SDCARD,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                Manifest.permission.READ_EXTERNAL_STORAGE);
//        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
//        EasyPermissions.requestPermissions(this, "请设置权限",
//                REQUECT_CODE_SDCARD, perms);
        methodRequiresTwoPermission();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @AfterPermissionGranted(REQUECT_CODE_SDCARD)
    private void methodRequiresTwoPermission() {
        String[] perms = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            download();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置权限",
                    REQUECT_CODE_SDCARD, perms);
        }
    }

//    @PermissionGrant(REQUECT_CODE_SDCARD)
//    public void requestSdcardSuccess() {
//        download();
//    }
//
//    @PermissionDenied(REQUECT_CODE_SDCARD)
//    public void requestSdcardFailed() {
//        pbLoading.setVisibility(View.GONE);
//        ToastUtil.showShortlToast("访问SD卡权限被拒绝");
//    }

    private void download() {
        RequestParams params = new RequestParams(path);
        //设置断点续传
        params.setAutoResume(true);
        params.setAutoRename(true);
        params.setSaveFilePath(target);
        cancelable = x.http().get(params, new Callback.ProgressCallback<File>() {

            @Override
            public void onSuccess(File result) {
                pbLoading.setVisibility(View.GONE);
                isLoading = false;
                showDoc(new File(target), type);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onFinished() {
            }

            @Override
            public void onWaiting() {
            }

            @Override
            public void onStarted() {
            }

            @Override
            public void onLoading(long total, long current, boolean isDownloading) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.activity_ppt_view) {
            if (cancelable != null && !cancelable.isCancelled() && isLoading) {
                cancelable.cancel();
            }
            this.finish();

        }
    }
/*    @Event(value = {R.id.activity_ppt_view}, type = View.OnClickListener.class)
    private void onClick(View view) {
        if (cancelable != null && !cancelable.isCancelled() && isLoading) {
            cancelable.cancel();
        }
        this.finish();
    }*/


    @Override
    protected void onDestroy() {
        if (cancelable != null && !cancelable.isCancelled() && isLoading) {
            cancelable.cancel();
        }
        super.onDestroy();
    }
}
