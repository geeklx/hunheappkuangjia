package com.sdzn.fzx.student.libbase.show;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.event.BLEServiceEvent;
import com.sdzn.fzx.student.libbase.service.BluetoothLEService;
import com.sdzn.fzx.student.libpublic.utils.ImageUtils;
import com.sdzn.fzx.student.libpublic.views.CustomDialog;
import com.sdzn.fzx.student.libpublic.views.DrawImageView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.VDRelativeLayout;
import com.tqltech.tqlpencomm.Dot;
import com.tqltech.tqlpencomm.PenCommAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * @author Reisen at 2018-11-28
 */

public class WhiteBoardWithMethodFragment extends MBaseFragment<WhiteBoardPresenter> implements WhiteBoardView {
    private VDRelativeLayout mVDRelativeLayout;
    private ImageView ivDragView;
    private WebView mXWalkView;
    private ImageView ivShape;
    private ScrollView mScrollView;
    private RelativeLayout viewWindow;
    private DrawImageView mImageView;


    private String appNum = "";
    private String appId = "";
    private String examId = "";
    //    private String index = "";
    private boolean unTouch = true;//是否书写

    /*================  ================*/

    // A5
    private static double A5_WIDTH = 182.86;//横坐标相对坐标原点的偏移量
    private static double A5_HEIGHT = 256.0;//纵坐标相对坐标原点的偏移量
    private static int BG_REAL_WIDTH = 1075;
    private static int BG_REAL_HEIGHT = 1512;
    private int BG_WIDTH;//控件宽
    private int BG_HEIGHT;//控件高
    private int A5_X_OFFSET;
    private int A5_Y_OFFSET;
    private float mov_x;//声明起点坐标
    private float mov_y;//声明起点坐标
    public static float mWidth;//屏幕宽
    public static float mHeight;//屏幕高
    public int gCurPageID = -1;
    public int gCurBookID = -1;

    private static final double XDIST_PERUNIT = 1.524;
    private static final double YDIST_PERUNIT = 1.524;

    ArrayList<Dots> dot_number = new ArrayList<>();
    ArrayList<Dots> dot_number1 = new ArrayList<>();
    ArrayList<Dots> dot_number2 = new ArrayList<>();
    ArrayList<Dots> dot_number4 = new ArrayList<>();

    public static float fromX, toX;
    public static float fromY, toY;

    private boolean gbSetNormal = false;


    private byte[] bookID = new byte[]{0, 1, 100};
    private Boolean bBookIDIsValide;
    // the actual data
    public int mN;
    private PenCommAgent bleManager;

    Handler mHandler = new Handler(Looper.getMainLooper());

    /*================  ================*/


    public static WhiteBoardWithMethodFragment newInstance(Bundle args) {
        WhiteBoardWithMethodFragment fragment = new WhiteBoardWithMethodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new WhiteBoardPresenter();
        mPresenter.attachView(this, (WhiteBoardWithMethodActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_white_board_with_method, container, false);
        mVDRelativeLayout = (VDRelativeLayout) view.findViewById(R.id.vdrl);
        ivDragView = (ImageView) view.findViewById(R.id.iv_drag);
        mXWalkView = (WebView) view.findViewById(R.id.xWalkView);
        ivShape = (ImageView) view.findViewById(R.id.iv_shape);
        mScrollView = (ScrollView) view.findViewById(R.id.bottom);
        viewWindow = (RelativeLayout) view.findViewById(R.id.viewWindow);
        mImageView = (DrawImageView) view.findViewById(R.id.iv_draw);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            appNum = bundle.getString("appNum");
            appId = bundle.getString("appId");
            examId = bundle.getString("examStem");
//            index = bundle.getString("index");
        }
        initView();
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BLEServiceManger.getInstance().getService() == null) {
            BLEServiceManger.getInstance().bindService(getActivity());
        }
    }

    private void initView() {
//        mVDRelativeLayout.setTopView(mTopView);
        mVDRelativeLayout.setTopView(mXWalkView);
        mVDRelativeLayout.setShapeView(ivShape);
        mVDRelativeLayout.setDragView(ivDragView);
        mVDRelativeLayout.setBottomView(mScrollView);
        mVDRelativeLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mVDRelativeLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        mTopView.postInvalidate();
                        ((WhiteBoardWithMethodActivity) getActivity()).layoutView();
                    }
                }, 100);
                return false;
            }
        });
        mVDRelativeLayout.setCallBack(new VDRelativeLayout.VDCallBack() {
            @Override
            public void layoutView() {
                mXWalkView.setClipBounds(new Rect(0, 0, 1920, ivDragView.getBottom()));
                ((WhiteBoardWithMethodActivity) getActivity()).layoutView();
            }
        });
        ivDragView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {//屏蔽长按事件, 防止长按事件穿透到webview上
                return true;
            }
        });
//        mTopView.setScrollCallBack(new ScrollCallBackView.OnScrollCallBack() {
//            @Override
//            public void onScroll(int l, int t, int oldl, int oldt) {
//                ((WhiteBoardWithMethodActivity)activity).layoutView();
//            }
//        });
        initXWalkView();

        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(App2.get());
        }
        //   bleManager.setXYDataFormat(1);
        // bleManager.setPenLedConfig(0x07);//黑色
        if (BLEServiceManger.getInstance().getService() == null) {
            BLEServiceManger.getInstance().bindService(getActivity());
        } else {
            addListener();
        }

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        mWidth = dm.widthPixels;
        mHeight = dm.heightPixels;

        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        param.width = (int) mWidth;
        param.height = (int) mHeight;
        param.rightMargin = 1;
        param.bottomMargin = 1;

        drawInit();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mImageView.getLayoutParams();
        float ratio;
        if (mWidth == 1536 && mHeight == 2048) {  // 适应小米PAD,4:3
            ratio = (mWidth * 63 / 72) / BG_REAL_WIDTH;
        } else if (mWidth == 1536 && mHeight == 1952) { // 非标准4：3，适用iFive
            ratio = (mWidth * 60 / 72) / BG_REAL_WIDTH;
        } else {
            ratio = (mWidth * 70 / 72) / BG_REAL_WIDTH;
        }
        BG_WIDTH = (int) (BG_REAL_WIDTH * ratio);
        BG_HEIGHT = (int) (BG_REAL_HEIGHT * ratio);

        int gcontentLeft = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT).getLeft();
        //布局左边缘和顶部的位置
        int gcontentTop = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        A5_X_OFFSET = (int) (mWidth - gcontentLeft - BG_WIDTH) / 2;
        A5_Y_OFFSET = (int) (mHeight - gcontentTop - BG_HEIGHT) / 2;

        mImageView.setLayoutParams(params);
        changeUI();
    }

    private void initXWalkView() {
        mXWalkView.setDrawingCacheEnabled(true);
        mXWalkView.getSettings().setJavaScriptEnabled(true);
//        mXWalkView.setClipBounds(new Rect(0,0,1920,1104));
        mXWalkView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loadH5res();
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return true;
            }
        });
        mXWalkView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                result.confirm();
                return true;
            }
        });
        mXWalkView.loadUrl("file:///android_asset/h5/html/read-exam.html", null);
        mVDRelativeLayout.requestLayout();
    }

    private void loadH5res() {
        Map<String, String> params = new HashMap<>();
//        params.put("host", Network.BASE_URL + "/");
//        params.put("access_token", UserController.getAccessToken());
//        params.put("refresh_token", UserController.getRefreshToken());
        params.put("examStem", examId);
//        params.put("index", index);
        String json = GsonUtil.toJson(params);
        if (mXWalkView != null) {
            mXWalkView.evaluateJavascript("javascript:getReadExam(" + json + ")", null);
        }
    }

    private int flag;

    private void initData() {
        if ("1".equals(getActivity().getIntent().getStringExtra("flag"))) {
            flag = 1;
            mPresenter.loadLocalData(appNum, appId);
        }
    }

    /**
     * 添加回调监听
     */

    private void addListener() {
        BLEServiceManger.getInstance().getService().addOnDataReceiveListener(getDataListener());
        if (BLEServiceManger.getInstance().getService().isPenConnected()) {
            mImageView.setVisibility(View.VISIBLE);
        } else {
            ToastUtil.showShortlToast("手写笔未连接");
            mImageView.setVisibility(View.VISIBLE);
//            finish();
        }
    }

    private void changeUI() {
        if (dot_number.isEmpty() && dot_number1.isEmpty() && dot_number2.isEmpty()) {
            ((WhiteBoardWithMethodActivity) getActivity()).setButtonEnable(false);
        } else {
            ((WhiteBoardWithMethodActivity) getActivity()).setButtonEnable(true);
        }
    }

    private BluetoothLEService.BaseOnDataReceiveListener mListener;

    private BluetoothLEService.BaseOnDataReceiveListener getDataListener() {
        if (mListener == null) {
            mListener = new BluetoothLEService.BaseOnDataReceiveListener() {
                @Override
                public void onConnected() {
                    mImageView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onDisconnected() {
                    CustomDialog.Builder builder = new CustomDialog.Builder(getActivity());
                    builder.setMessage("手写笔连接中断，是否保存当前作答？");
                    builder.setPositive("保存", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            savePhoto(1);
                        }
                    });
                    builder.setNegative("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            savePhoto(0);
                        }
                    });
                    CustomDialog dialog = builder.create();
                    dialog.show();
                }

                @Override
                public void onDataReceive(final Dot dot) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            unTouch = false;
                            processDots(dot);
                        }
                    });
                }

                @Override
                public void onReceiveOIDSize(int OIDSize) {
                    gCurPageID = -1;
                }
            };
        }
        return mListener;
    }


    /**
     * 绑定成功后添加监听
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceConnected(BLEServiceEvent event) {
        if (event.getService() != null) {
            addListener();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dot_number.clear();
        dot_number = null;

        dot_number1.clear();
        dot_number1 = null;

        dot_number2.clear();
        dot_number2 = null;

        dot_number4.clear();
        dot_number4 = null;

        BLEServiceManger manger = BLEServiceManger.getInstance();
        if (manger.getService() != null) {
            manger.getService().removeOnDataReceiveListener(getDataListener());
        }
        manger.unbindService(getActivity());
        EventBus.getDefault().unregister(this);
    }

    public void cleanScreen() {
        CustomDialog.Builder builder = new CustomDialog.Builder(getActivity());
        builder.setTitle("提示");
        builder.setMessage("是否重新作答此题（此操作不可撤销）");
        builder.setPositive("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                drawInit();
                SetBackgroundImage(gCurBookID, gCurPageID);
                dot_number.clear();
                dot_number1.clear();
                dot_number2.clear();
                dot_number4.clear();
                changeUI();
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    public void confirm() {
        savePhoto(1);
    }

    public void titleBack() {
        back();
    }

    public void back() {
        if (unTouch) {
            getActivity().finish();
            return;
        }
        if (getDotsList().isEmpty()) {
            savePhoto(0);
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(getActivity());
        builder.setMessage("是否保存当前书写内容? ");
        builder.setPositive("保存", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                savePhoto(1);
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (flag == 1) {
                    getActivity().finish();
                    return;
                }
                savePhoto(0);
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    private void savePhoto(int flag) {
        mPresenter.deleteDots(appNum, appId);
        //保存数据库
        Collection<Dots> list = getDotsList();
        if (list.isEmpty()) {
            confirmData("", 0);
        } else {
            int size = 8;
            RectF rectF = new RectF(-1, -1, -1, -1);
            for (Dots dots : list) {
                if (rectF.top == -1) {
                    rectF.top = dots.pointY;
                }
                if (rectF.bottom == -1) {
                    rectF.bottom = dots.pointY;
                }
                if (rectF.left == -1) {
                    rectF.left = dots.pointX;
                }
                if (rectF.right == -1) {
                    rectF.right = dots.pointX;
                }
                if (dots.pointX > rectF.right) {
                    rectF.right = dots.pointX;
                } else if (dots.pointX < rectF.left) {
                    rectF.left = dots.pointX;
                }
                if (dots.pointY > rectF.bottom) {
                    rectF.bottom = dots.pointY;
                } else if (dots.pointY < rectF.top) {
                    rectF.top = dots.pointY;
                }
            }
            //x(0~992
            //y(0~1403
            rectF.left -= size;
            rectF.right += size;
            rectF.top -= size;
            rectF.bottom += size;
            if (rectF.left < 0) {
                rectF.left = 0;
            }
            if (rectF.top < 0) {
                rectF.top = 0;
            }
            if (rectF.right > 992) {
                rectF.right = 992;
            }
            if (rectF.bottom > 1403) {
                rectF.bottom = 1403;
            }
//            ToastUtil.showShortlToast(rectF.toString());
            mPresenter.saveDots(list, flag, rectF);
        }
    }

    public Collection<Dots> getDotsList() {
        switch (gCurBookID) {
            case 0:
                return dot_number1;
            case 1:
                return dot_number2;
            default://默认book100
                return dot_number;
        }
    }

    private void confirmData(String realFilePath, int flag) {

        Intent intentData = new Intent();
        Bundle b = new Bundle();
        b.putInt("flag", flag);
        b.putString("webUrl", realFilePath);
        b.putString("examStr", examId);
        intentData.putExtras(b);
        getActivity().setResult(RESULT_OK, intentData);
        getActivity().finish();
    }

    @Override
    public void onDestroy() {
        BLEServiceManger.getInstance().getService().removeOnDataReceiveListener(getDataListener());
        super.onDestroy();
    }

    //重新加载本地数据成功
    @Override
    public void loadLocalDataSuccess(List<Dots> list) {
        for (Dots dots : list) {
            switch (dots.bookID) {
                case 100:
                    gCurBookID = 100;
                    gCurPageID = dots.pageID;
                    dot_number.add(dots);
                    break;
                case 0:
                    gCurBookID = 0;
                    gCurPageID = dots.pageID;
                    dot_number1.add(dots);
                    break;
                case 1:
                    gCurBookID = 1;
                    gCurPageID = dots.pageID;
                    dot_number2.add(dots);
                    break;
            }
        }
        replay();
    }

    /**
     * 储绘制点成功
     */
    @Override
    public void saveDotsSuccess(int flag, RectF rectF) {
        if (flag == 0) {
            confirmData("", 0);
            return;
        }
        Bitmap bitmap;
        if (rectF != null) {
            bitmap = Bitmap.createBitmap(mImageView.backBitmap, ((int) rectF.left), ((int) rectF.top), ((int) (rectF.right - rectF.left)), ((int) (rectF.bottom - rectF.top)));
        } else {
            bitmap = mImageView.backBitmap;
        }
        String imagePath = ImageUtils.savePhoto(bitmap, Environment.getExternalStorageDirectory().getAbsolutePath(), String.valueOf(System.currentTimeMillis()));
        if (imagePath != null) {
            // 拿着imagePath上传了
            confirmData(imagePath, flag);
        } else {
            ToastUtil.showLonglToast("图片截取失败");
        }
    }


    /**
     * 保存失败
     */

    @Override
    public void saveDotsFailed(String msg, int flag) {
        ToastUtil.showShortlToast(msg);
    }

    private void processDots(Dot dot) {
        int counter = 0;
        int pointZ = dot.force;

        //点压力值<0, 未接触纸面
        if (pointZ < 0) {
            return;
        }

        //计算接触点横坐标
        int tmpx = dot.x;
        float pointX = dot.fx;
        pointX /= 100.0;
        pointX += tmpx;

        //计算接触点纵坐标
        int tmpy = dot.y;
        float pointY = dot.fy;
        pointY /= 100.0;
        pointY += tmpy;

        //通过书号判断横纵坐标相对坐标原点的偏移量和实际尺寸
        if (dot.BookID == 100) {//这里用的是book100
            A5_WIDTH = 182.86;
            A5_HEIGHT = 256.0;
            BG_REAL_WIDTH = 1075;
            BG_REAL_HEIGHT = 1512;
        } else if (dot.BookID == 0) {
            A5_WIDTH = 210.29;
            A5_HEIGHT = 297.15;
            BG_REAL_WIDTH = 992;
            BG_REAL_HEIGHT = 1403;
        } else if (dot.BookID == 1) {
            A5_WIDTH = 210.68;
            A5_HEIGHT = 285.67;
            BG_REAL_WIDTH = 2489;
            BG_REAL_HEIGHT = 3375;
        }

        getRedio(BG_REAL_WIDTH);
        pointX *= (BG_WIDTH);
        float ax = (float) (A5_WIDTH / XDIST_PERUNIT) * 1.9f;//--
        pointX /= ax;

        pointY *= (BG_HEIGHT);
        float ay = (float) (A5_HEIGHT / YDIST_PERUNIT) * 1.88f;//--
        pointY /= ay;

        pointX += A5_X_OFFSET;//--
        pointY += A5_Y_OFFSET + 50;

        int gWidth = 1;
        if (pointZ > 0) {
            if (dot.type == Dot.DotType.PEN_DOWN) {
                int PageID, BookID;
                PageID = dot.PageID;
                BookID = dot.BookID;
                if (PageID < 0 || BookID < 0 || PageID > 64) {
                    // 谨防笔连接不切页的情况
                    return;
                }

                for (byte aBookID : bookID) {
                    if (BookID == aBookID) {
                        bBookIDIsValide = true;
                    }
                }

                if (!bBookIDIsValide) {
                    return;
                }

                if (PageID != gCurPageID || BookID != gCurBookID) {
                    gbSetNormal = false;
                    bBookIDIsValide = false;
                    SetBackgroundImage(dot.BookID, PageID);
                    mImageView.setVisibility(View.VISIBLE);
                    gCurPageID = PageID;
                    gCurBookID = BookID;
                    drawInit();
                    DrawExistingStroke(gCurBookID, gCurPageID);
                }

                drawSubFountainPen2(gWidth, pointX, pointY, 0);

                // 保存屏幕坐标，原始坐标会使比例缩小
                saveData(gCurBookID, gCurPageID, pointX, pointY, pointZ, 0, gWidth, Color.BLACK, dot.Counter, dot.angle);
                mov_x = pointX;
                mov_y = pointY;
                return;
            }

            if (dot.type == Dot.DotType.PEN_MOVE) {
                // Pen Move
                mN += 1;
                mov_x = pointX;
                mov_y = pointY;

                drawSubFountainPen2(gWidth, pointX, pointY, 1);
                // 保存屏幕坐标，原始坐标会使比例缩小
                saveData(gCurBookID, gCurPageID, pointX, pointY, pointZ, 1, gWidth, Color.BLACK, dot.Counter, dot.angle);
            }
        } else if (dot.type == Dot.DotType.PEN_UP) {
            // Pen Up
            if (dot.x == 0 || dot.y == 0) {
                pointX = mov_x;
                pointY = mov_y;
            }

            drawSubFountainPen2(gWidth, pointX, pointY, 2);

            // 保存屏幕坐标，原始坐标会使比例缩小
            saveData(gCurBookID, gCurPageID, pointX, pointY, pointZ, 2, gWidth, Color.BLACK, dot.Counter, dot.angle);
            mN = 0;
        }
    }

    private void getRedio(int width) {//计算缩放比例
        float ratio;
        if (mWidth == 1536 && mHeight == 2048) { // 适应小米PAD,4:3
            ratio = (mWidth * 63 / 72) / width;
        } else if (mWidth == 1536 && mHeight == 1952) { // 非标准4：3，适用iFive
            ratio = (mWidth * 60 / 72) / width;
        } else if (mWidth == 1600 && mHeight == 2452) {
            ratio = (mWidth * 67 / 72) / width;
        } else if (mWidth == 1920 && mHeight == 1128) {//联想pad
            ratio = (mWidth * 80 / 72) / width;
        } else {
            ratio = (mWidth * 70 / 72) / width;
        }

        BG_WIDTH = (int) (BG_REAL_WIDTH * ratio);
        BG_HEIGHT = (int) (BG_REAL_HEIGHT * ratio);

//        A5_X_OFFSET = (int) (mWidth - gcontentLeft - BG_WIDTH) / 2;//-106
//        A5_Y_OFFSET = (int) (mHeight - gcontentTop - BG_HEIGHT) / 4;//-936
        A5_X_OFFSET = 0;
        A5_Y_OFFSET = -50;
    }

    private void SetBackgroundImage(int BookID, int PageID) {
        if (!gbSetNormal) {
            RelativeLayout.LayoutParams para = (RelativeLayout.LayoutParams) mImageView.getLayoutParams();
//            para.width = BG_WIDTH;
//            para.height = BG_HEIGHT;
            mImageView.setLayoutParams(para);
            gbSetNormal = true;
        }


        if (BookID == 100) {
            if (getResources().getIdentifier("p" + PageID, "drawable", getActivity().getPackageName()) == 0) {
                return;
            }
            mImageView.setImageResource(getResources().getIdentifier("p" + PageID, "drawable", getActivity().getPackageName()));
        } else if (BookID == 0) {
            if (getResources().getIdentifier("blank" + PageID, "drawable", getActivity().getPackageName()) == 0) {
                return;
            }
            mImageView.setImageResource(getResources().getIdentifier("blank" + PageID, "drawable", getActivity().getPackageName()));
        } else if (BookID == 1) {
            if (getResources().getIdentifier("zhen" + PageID, "drawable", getActivity().getPackageName()) == 0) {
                return;
            }
            mImageView.setImageResource(getResources().getIdentifier("zhen" + PageID, "drawable", getActivity().getPackageName()));
        }
    }

    private void drawInit() {
        if (gCurPageID == -1 && gCurBookID == -1) {
            mImageView.initDraw(mImageView.getContext());
        }
        mImageView.paint.setStrokeCap(Paint.Cap.ROUND);
        mImageView.paint.setStyle(Paint.Style.FILL);
        mImageView.paint.setAntiAlias(true);
        mImageView.invalidate();
    }

    public void DrawExistingStroke(int BookID, int PageID) {
        if (BookID == 100) {
            dot_number4 = dot_number;
        } else if (BookID == 0) {
            dot_number4 = dot_number1;
        } else if (BookID == 1) {
            dot_number4 = dot_number2;
        }

        if (dot_number4.isEmpty()) {
            return;
        }

        for (Dots dot : dot_number4) {
            if (dot.getPageID() == PageID) {
                drawSubFountainPen1(dot.penWidth, dot.pointX, dot.pointY, dot.ntype);
            }
        }
//        Set<Integer> keys = dot_number4.keySet();
//        for (int key : keys) {
//            if (key == PageID) {
//                List<Dots> dots = dot_number4.get(key);
//                for (Dots dot : dots) {
//                    Log.i("=========pageID=======" + dot.pointX + "====" + dot.pointY + "===" + dot.ntype);
//
//                    drawSubFountainPen1(dot.penWidth, dot.pointX, dot.pointY, dot.ntype);
//                }
//            }
//        }
    }

    private void drawSubFountainPen1(int penWidth, float x, float y, int ntype) {
        if (ntype == 0) {
            fromX = x;
            fromY = y;
            toX = x;
            toY = y;
        }

        if (ntype == 2) {
            toX = x;
            toY = y;
        } else {
            toX = x;
            toY = y;
        }

        mImageView.paint.setStrokeWidth(penWidth);
//        mImageView.canvas.drawLine(fromX, fromY, toX, toY, mImageView.paint);
        mImageView.drawLine(fromX, fromY, toX, toY);
//        mImageView.postInvalidate();
        fromX = toX;
        fromY = toY;
    }

    private void drawSubFountainPen2(int penWidth, float x, float y, int ntype) {
        switch (ntype) {
            case 0:
                fromX = x;
                fromY = y;
                toX = x;
                toY = y;
                break;
            case 1:
                toX = x;
                toY = y;
                break;
            case 2:
                toX = x;
                toY = y;
                break;
        }
        mImageView.paint.setStrokeWidth(penWidth);
//        mImageView.canvas.drawLine(fromX, fromY, toX, toY, mImageView.paint);
        mImageView.drawLine(fromX, fromY, toX, toY);
//        mImageView.postInvalidate();
        fromX = toX;
        fromY = toY;
    }

    private void saveData(Integer bookID, Integer pageID, float pointX, float pointY, int force, int ntype, int penWidth, int color, int counter, int angle) {
        Dots dot = new Dots(appId, appNum, bookID, pageID, pointX, pointY, force, ntype, penWidth, color, counter, angle);

        if (bookID == 100) {
            dot_number.add(dot);
        } else if (bookID == 0) {
            dot_number1.add(dot);
        } else if (bookID == 1) {
            dot_number2.add(dot);
        }
        changeUI();
    }

    private boolean bIsReply;

    /**
     * 重绘内容
     */
    private void replay() {
        if (bIsReply) {
            return;
        }
        bIsReply = true;
        if (gCurPageID < 0) {
            return;
        }
        drawInit();

        if (gCurBookID == 100) {//这里用book100
            dot_number4 = dot_number;
        } else if (gCurBookID == 0) {
            dot_number4 = dot_number1;
        } else if (gCurBookID == 1) {
            dot_number4 = dot_number2;
        }

        if (dot_number4.isEmpty()) {
            bIsReply = false;
            return;
        }

        Dots lastDot = dot_number4.get(dot_number4.size() - 1);
        int lastPage = lastDot.getPageID();
        SetBackgroundImage(lastDot.getBookID(), lastDot.getPageID());
        for (Dots dot : dot_number4) {
            if (dot.getPageID() == lastPage) {
//            if (dot.getPageID() != lastPage) {
//                lastPage = dot.getPageID();
//                SetBackgroundImage(dot.getBookID(), dot.getPageID());
//            }
                gbSetNormal = false;
                bBookIDIsValide = false;
                mImageView.setVisibility(View.VISIBLE);
                if (gCurBookID != dot.bookID || gCurPageID != dot.pageID) {
                    gCurPageID = dot.pageID;
                    gCurBookID = dot.bookID;
                }
                drawInit();
                drawSubFountainPen1(dot.penWidth, dot.pointX, dot.pointY, dot.ntype);
            }
        }
        bIsReply = false;
//        Set<Integer> keys = dot_number4.keySet();
//        for (int key : keys) {
//            bIsReply = true;
//            List<Dots> dots = dot_number4.get(key);
//            if (!dots.isEmpty()) {
//                SetBackgroundImage(dots.get(0).bookID, dots.get(0).pageID);
//            }
//            for (Dots dot : dots) {
//                gbSetNormal = false;
//                bBookIDIsValide = false;
//                mImageView.setVisibility(View.VISIBLE);
//                if (gCurBookID != dot.bookID || gCurPageID != dot.pageID) {
//                    gCurPageID = dot.pageID;
//                    gCurBookID = dot.bookID;
//                }
//                drawInit();
//                drawSubFountainPen1(dot.penWidth, dot.pointX, dot.pointY, dot.ntype);
//            }
//        }
//        bIsReply = false;
        changeUI();
    }
}
