package com.sdzn.fzx.student.libbase.listener;


import com.sdzn.fzx.student.vo.SubjectVo;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/19
 */
public interface OnSubjectClickListener {
    void onSubjectClick(SubjectVo.DataBean dataBean);
}
