package com.sdzn.fzx.student.libbase.show;

import android.content.Intent;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;

import java.util.List;

public class WhiteBoardWithMethodActivity extends MBaseActivity<WhiteBoardPresenter> implements WhiteBoardView, View.OnClickListener {
    private LinearLayout titleBackLy;
    private TextView title;
    private TextView cleanScreenBut;
    private LinearLayout confirmLy;
    private FrameLayout fl;

    private String appNum = "";
    private String appId = "";
    private String examId = "";
    private String index = "";

    private WhiteBoardWithMethodFragment mFragment;

    @Override
    public void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white_board_with_method);
        titleBackLy = (LinearLayout) findViewById(R.id.title_back_ly);
        title = (TextView) findViewById(R.id.title);
        cleanScreenBut = (TextView) findViewById(R.id.cleanScreenBut);
        confirmLy = (LinearLayout) findViewById(R.id.confirm_ly);
        fl = (FrameLayout) findViewById(R.id.fl);
        cleanScreenBut.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        titleBackLy.setOnClickListener(this);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(WhiteBoardWithMethodActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        appNum = getIntent().getStringExtra("appNum");
        if (appNum == null) {
            appNum = String.valueOf(getIntent().getIntExtra("appNum", 1));
        }
        appId = getIntent().getStringExtra("appId");
        examId = getIntent().getStringExtra("examStem");
        index = getIntent().getStringExtra("index");
        title.setText("第" + appNum + "题");

        final Bundle bundle = new Bundle();
        bundle.putString("appNum", appNum);
        bundle.putString("appId", appId);
        bundle.putString("examStem", examId);
//        bundle.putString("index",index);
        mFragment = WhiteBoardWithMethodFragment.newInstance(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(fl.getId(), mFragment, mFragment.getClass().getName());
        transaction.commit();
        fl.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                left = fl.getLeft();
                right = fl.getRight();
                top = fl.getTop();
                bottom = fl.getBottom();
                fl.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });
    }

    private int left;
    private int top;
    private int bottom;
    private int right;

    @Override
    protected void initData() {
    }

    @Override
    public void onBackPressed() {
        mFragment.back();
    }

    public void setButtonEnable(boolean b) {
        confirmLy.setEnabled(b);
        cleanScreenBut.setEnabled(b);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.cleanScreenBut) {//清屏
            mFragment.cleanScreen();
        } else if (i == R.id.confirm_ly) {//保存
            mFragment.confirm();
        } else if (i == R.id.title_back_ly) {//返回
            mFragment.titleBack();
        }
    }

    public void layoutView() {
        Log.e(left + ", " + top + ", " + right + ", " + bottom);
        fl.invalidate(left, top, right, bottom);
    }

    @Override
    public void loadLocalDataSuccess(List<Dots> list) {

    }

    @Override
    public void saveDotsSuccess(int flag, RectF rectF) {

    }

    @Override
    public void saveDotsFailed(String msg, int flag) {

    }
}
