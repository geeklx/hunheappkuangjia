package com.sdzn.fzx.student.libbase.ai.pop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.lxj.xpopup.core.CenterPopupView;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.utils.YanzhengUtil;
import com.sdzn.fzx.student.libpublic.event.UpdataPhoto;
import com.sdzn.fzx.student.libpublic.views.ClearableEditText;
import com.sdzn.fzx.student.libpublic.views.search.KeyboardUtils;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.presenter.ChangePhonePresenter1;
import com.sdzn.fzx.student.view.ChangePhoneViews;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;

/**
 * 修改手机号，添加手机号
 */
public class ChangePhonePopupView extends CenterPopupView implements ChangePhoneViews {
    private TextView tvTitle;
    private TextView tvCancel;
    private TextView tvConfirm;
    private Context mContext;
    private String flag;
    private ClearableEditText userNumEdit;
    private EditText valicodeEdit;
    private ImageView Effectiveness;
    private ClearableEditText userCodeEdit;
    private TextView countDownBtn;
    ChangePhonePresenter1 mPresenter;

    public ChangePhonePopupView(@NonNull Context context, String flag) {
        super(context);
        this.mContext = context;
        this.flag = flag;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_center_yewu_changephone;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Log.e("tag", "CustomDrawerPopupView onCreate");
        initview();
        initclick();
    }

    private void initview() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        userNumEdit = (ClearableEditText) findViewById(R.id.user_num_edit);
        valicodeEdit = findViewById(R.id.valicode_edit);
        Effectiveness = (ImageView) findViewById(R.id.iv_effectiveness);
        userCodeEdit = (ClearableEditText) findViewById(R.id.user_code_edit);
        countDownBtn = (TextView) findViewById(R.id.count_down_btn);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);

        if (flag.equals("0")) {
            tvTitle.setText("绑定手机号");
        } else {
            tvTitle.setText("修改手机号");
        }
        mPresenter = new ChangePhonePresenter1();
        mPresenter.onCreate(this);
        mPresenter.QueryImgToken();
    }

    private void initclick() {
        /*获取验证码*/
        countDownBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getCode();
            }
        });
        /*更新图形验证码*/
        Effectiveness.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.QueryImgToken();
            }
        });
        /*取消*/
        tvCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        /*确定*/
        tvConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm();
            }
        });
    }

    private String userPhone;
    private String valicode;
    private String editcode;

    private void confirm() {
        userPhone = userNumEdit.getText().toString();
        editcode = userCodeEdit.getText().toString().trim();
        valicode = valicodeEdit.getText().toString().trim();
        if (userPhone.equals("")) {
            ToastUtil.showCenterBottomToast("手机号不能为空");
            return;
        } else if (!StringUtils.isMobile(userNumEdit.getText().toString().trim()) && userNumEdit.getText().toString().trim().length() != 11) {
            ToastUtils.showShort("请输入正确的账号或手机号");
            return;
        } else if (editcode.equals("")) {
            ToastUtil.showCenterBottomToast("验证码不能为空");
            return;
        } else if (valicode.equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
            return;
        }
        mPresenter.checkVerityNum(userPhone, userCodeEdit.getText().toString());
    }

    private void getCode() {
        userPhone = userNumEdit.getText().toString();
        valicode = valicodeEdit.getText().toString().trim();
        if (!StringUtils.isMobile(userPhone)) {
            ToastUtils.showShort("请输入正确的手机号格式");
        } else if (valicode.equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.sendVerityCode("Bearer " + (String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
                    String.valueOf(UserController.getLoginBean().getData().getUser().getId()),
                    userPhone, valicode, imgToken);

        }
    }

    @Override
    public void verifySuccess(String phone, String code) {
        //刷新  DataSafetyFragment的数据
        EventBus.getDefault().post(new UpdataPhoto("update_safety"));
        dismiss();
    }

    @Override
    public void savePhone() {
        dismiss();
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void OnVerityCodeSuccess(Object success) {
        ToastUtils.showShort(String.valueOf(success));
        YanzhengUtil.startTime(60 * 1000, countDownBtn);//倒计时bufen
    }

    @Override
    public void OnVerityCodeFailed(String msg) {
        valicodeEdit.setText("");
        ToastUtils.showShort("获取验证码失败");
        mPresenter.QueryImgToken();
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgTokenFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        Bitmap bitimg = BitmapFactory.decodeStream(inputStream);
        Glide.with(this).load(bitimg).into(Effectiveness);
    }

    @Override
    public void OnImgFailed(String msg) {

    }

    @Override
    public String getIdentifier() {
        return null;
    }

    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestory();
        YanzhengUtil.timer_des();
        super.onDestroy();
    }
}
