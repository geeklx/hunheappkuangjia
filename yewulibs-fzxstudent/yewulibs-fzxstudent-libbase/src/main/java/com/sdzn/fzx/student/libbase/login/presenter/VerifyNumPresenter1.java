package com.sdzn.fzx.student.libbase.login.presenter;

import com.blankj.utilcode.util.ToastUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.libbase.login.view.VerifyNumViews;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.vo.VerifyUserMobile;

import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * VerifyNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VerifyNumPresenter1 extends Presenter<VerifyNumViews> {

    /*获取图形验证码Token*/
    public void QueryImgToken() {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImgToken(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/valicode/imgToken")
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnImgTokenFailed(response.body().getMessage());
                            return;
                        }
                        getView().OnImgTokenSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnImgTokenFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    /*获取图形验证码图片*/
    public void QueryImg(String imgToken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .QueryImg(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/valicode/img", imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        InputStream io = response.body().byteStream();
                        getView().OnImgSuccess(io);
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnImgFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }

    /*验证手机号*/
    public void VerifyMobilePhoneNumber(final String cardId) {
        if (!StringUtils.isMobile(cardId) && cardId.length() != 8) {
            ToastUtils.showShort("请输入正确的账号或手机号");
            return;
        }

        com.alibaba.fastjson.JSONObject requestData = new com.alibaba.fastjson.JSONObject();
        requestData.put("account", cardId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .studentFindMobile(BuildConfig2.SERVER_ISERVICE_NEW2 + "/usercenter/student/find/studentMobile", requestBody)
                .enqueue(new Callback<ResponseSlbBean1<VerifyUserMobile>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<VerifyUserMobile>> call, Response<ResponseSlbBean1<VerifyUserMobile>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            ToastUtils.showShort(response.body().getMessage());
                            return;
                        }
                        getView().OnVerifyMobilePhoneSuccess(response.body().getResult().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<VerifyUserMobile>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnVerifyMobilePhoneFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    /*获取验证码*/
    public void sendVerityCode(String phone,String imgcode,String imgtoken) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .sendYZM1(BuildConfig2.SERVER_ISERVICE_NEW2 + "/student/login/sendVerifyCode", phone,imgcode,imgtoken)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onVerityCodeFailed(response.body().getMessage());
                            return;
                        }
                        getView().onVerityCodeSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onVerityCodeFailed(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    public void checkVerityNum(final String phoneNum, final String code) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .checkYZM(BuildConfig2.SERVER_ISERVICE_NEW2 + "/teacher/check/smsToken", phoneNum, code)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCheckCodeFailed(response.body().getMessage());
                            return;
                        }
                        if (String.valueOf(response.body().getResult()).equals("true")) {
                            getView().onCheckCodeSuccess(phoneNum, code);
                        } else {
                            getView().onCheckCodeFailed("验证码错误，请重新输入");
                        }

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onCheckCodeFailed("验证码错误，请重新输入");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
