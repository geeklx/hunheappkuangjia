package com.sdzn.fzx.student.libbase.baseui.presenter;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.AppUtils;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.dao.DotsDao;
import com.sdzn.fzx.student.utils.CommonDaoUtils;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.baseui.view.AnswerNativeView;
import com.sdzn.fzx.student.libbase.listener.AlbumOrCameraListener;
import com.sdzn.fzx.student.libpublic.views.CustomDialog;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ExamSubmitBean;
import com.sdzn.fzx.student.vo.ImageUploadInfoBean;
import com.sdzn.fzx.student.vo.QiniuUptoken;
import com.sdzn.fzx.student.vo.ResourceVo;
import com.sdzn.fzx.student.vo.ServerTimeVo;
import com.sdzn.fzx.student.vo.TaskListVo;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.utils.ImageUtils;
import rain.coder.photopicker.utils.UCropUtils;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * 作答页面
 *
 * @author Reisen at 2018-08-27
 */
public class AnswerNativePresenter extends BasePresenter<AnswerNativeView, BaseActivity> {

    private Subscription subscribe2;
    private File takeImageFile;
    public static final int REQ_CODE_CAMERA = 1003;
    /**
     * 更新试题作答状态
     */
    public void updateStatus(final String lessonTaskStudentId) {
        if (subscribe2 != null && subscribe2.isUnsubscribed()) {
            subscribe2.unsubscribe();
            subscriptions.remove(subscribe2);
        }
        Map<String, String> params = new HashMap<>();
        params.put("lessonTaskStudentId", lessonTaskStudentId);
        subscribe2 = Network.createTokenService(NetWorkService.TaskService.class)
                .updateStatus(params)
                .map(new StatusFunc<>())
                .flatMap(new Func1<Object, Observable<StatusVo<ServerTimeVo>>>() {
                    @Override
                    public Observable<StatusVo<ServerTimeVo>> call(Object o) {
//                        Map<String, String> params = new HashMap<>();
                        return Network.createTokenService(NetWorkService.AnswerService.class)
                                .getServerTime2(Integer.valueOf(lessonTaskStudentId));
                    }
                })
                .map(new StatusFunc<ServerTimeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ServerTimeVo>(new SubscriberListener<ServerTimeVo>() {
                    @Override
                    public void onNext(ServerTimeVo serverTimeVo) {
                        if (mView == null) {
                            return;
                        }
                        mView.getServerTimeSuccess(serverTimeVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe2);
    }

    /**
     * 加载试题
     */
    public void loadAnswer(int id, int lessonTaskStudentId) {
        Map<String, String> params = new HashMap<>();
        params.put("lessonTaskId", String.valueOf(id));
        params.put("lessonTaskStudentId", String.valueOf(lessonTaskStudentId));
        Network.createTokenService(NetWorkService.AnswerService.class)
                .getAnswers(params)
                .map(new StatusFunc<AnswerListBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<AnswerListBean>(new SubscriberListener<AnswerListBean>() {
                    @Override
                    public void onNext(AnswerListBean bean) {
                        initData(bean.getData());
                        mView.getAnswerListSuccess(bean.getData());
                    }

                    private void initData( List<AnswerListBean.AnswerDataBean> list) {
                        if (list == null) {
                            return;
                        }
                        for (int i = 0; i < list.size(); i++) {
                            AnswerListBean.AnswerDataBean bean = list.get(i);
                            if (bean.getType() == 3) {//纯文本
                                continue;
                            }
                            if (bean.getType() == 2) {//资源
                                String resourceText = bean.getResourceText();
                                ResourceVo.ResourceTextVo resourceBean = GsonUtil.fromJson(resourceText, ResourceVo.ResourceTextVo.class);
                                if (resourceBean == null) {
                                    throw new RuntimeException("加载数据失败, 没有资源信息");
                                }
                                bean.setResourceVoBean(resourceBean);
                                continue;
                            }
                            //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = GsonUtil.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            if (examTextBean == null) {
                                throw new RuntimeException("加载数据失败, 没有试题信息");
                            }
                            bean.setExamBean(examTextBean);
                            //综合题额外进行一轮解析
                            if (bean.getExamTemplateId() != 16) {
                                continue;
                            }
                            for (AnswerListBean.AnswerDataBean dataBean : bean.getExamList()) {
                                AnswerListBean.ExamTextBean json = GsonUtil.fromJson(dataBean.getExamText(), AnswerListBean.ExamTextBean.class);
                                if (json == null) {
                                    throw new RuntimeException("加载数据失败, 没有试题信息");
                                }
                                dataBean.setExamBean(json);
                            }
                            //综合题试题加到list里
                            i++;
                            list.addAll(i,bean.getExamList());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                        String msg = "请求失败";
                        if (e instanceof ApiException) {
                            msg = ((ApiException) e).getMsg();
                        }
                        mView.networkError(msg);
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, false, ""));
    }

    /**
     * 提交作答
     */
    public void submitAnswer(List<AnswerListBean.AnswerDataBean> mList,
                             TaskListVo.DataBean taskDataBean,
                             long serverTime) {
        if (mList == null || mList.isEmpty()) {
            mView.submitAnswerFailure("提交失败");
            return;
        }
        String replyExamJSON = createSaveJson(mList);
        HashMap<String, Object> map = new HashMap<>();
        map.put("lessonTaskId", taskDataBean.getId());
        map.put("lessonTaskStudentId", taskDataBean.getLessonTaskStudentId());
        map.put("userStudentId", UserController.getUserId());
        map.put("userStudentName", UserController.getUserName());
        map.put("answerTime", serverTime);
        map.put("endTime", taskDataBean.getEndTime());
        map.put("replyExamJSON", replyExamJSON);
        map.put("access_token", UserController.getAccessToken());

        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .submitAnswer(map)
                .map(new StatusFunc<>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        if (mView == null) {
                            return;
                        }
                        mView.submitAnswerOneMinSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            e.printStackTrace();
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.submitAnswerFailure(status.getMsg());
                            } else {
                                mView.submitAnswerFailure("提交失败");
                            }
                        } else {
                            mView.submitAnswerFailure("提交失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
    }

    /**
     * 检查试题是否已全部作答
     */
    public boolean examIsAllDo(List<AnswerListBean.AnswerDataBean> mList) {
        for (AnswerListBean.AnswerDataBean bean : mList) {
            if (bean.getExamSeq() == 0) {//非试题
                continue;
            }
            switch (bean.getExamTemplateId()) {
                case 1://单选
                case 2://多选
                case 3://判断
                    if (!convertSelect(bean)) {
                        return false;
                    }
                    break;
                case 4://简答
                    if (!convertAnswer(bean)) {
                        return false;
                    }
                    break;
                case 6://填空
                case 14://完型
                    if (!convertBlank(bean)) {
                        return false;
                    }
                    break;
                case 16://综合
                    if (!convertSyn(bean)) {
                        return false;
                    }
                    break;
            }
        }
        return true;
    }

    /**
     * 填空 完型要看是否每个都作答
     */
    private boolean convertBlank(AnswerListBean.AnswerDataBean bean) {
        if (bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty()) {
            return false;
        }
        //判断作答数量
        List<AnswerListBean.ExamOptions> options = bean.getExamBean().getExamOptions();
        if (options == null || options.isEmpty()) {
            return true;
        }
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list == null || list.size() < options.size()) {
            return false;
        }
        //判断是否每个空都作答
        for (AnswerListBean.ExamOptionBean optionBean : list) {
            if (optionBean.getMyAnswer() == null || optionBean.getMyAnswer().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 简答 看有没有图片
     */
    private boolean convertAnswer(AnswerListBean.AnswerDataBean bean) {
        return bean.getExamOptionList() != null && !bean.getExamOptionList().isEmpty();
    }

    /**
     * 选择判断直接看isAnswer
     */
    private boolean convertSelect(AnswerListBean.AnswerDataBean bean) {
        return !(bean.getIsAnswer() == 0);
    }

    /**
     * 判断综合题是否作答
     */
    private boolean convertSyn(AnswerListBean.AnswerDataBean bean) {
        List<AnswerListBean.AnswerDataBean> list = bean.getExamList();
        if (list == null || list.isEmpty()) {//没有试题
            return true;
        }
        //有试题, 遍历试题, 确认作答状态
        int examCount = 0;
        for (AnswerListBean.AnswerDataBean dataBean : list) {
            if (dataBean.getIsAnswer() == 1) {
                examCount++;
            } else {
                List<AnswerListBean.ExamOptionBean> optionList = dataBean.getExamOptionList();
                if (optionList == null || optionList.isEmpty()) {
                    continue;
                }
                //综合填空
                int stateCount = 0;
                for (AnswerListBean.ExamOptionBean optionBean : optionList) {
                    if (optionBean.getMyAnswer() != null) {
                        stateCount++;
                    }
                }
                if (stateCount == optionList.size()) {//全部作答
                    examCount++;
                    continue;
                }
                if (stateCount != 0) {//有填空, 未全部作答
                    return false;
                }
            }
        }
        return examCount == list.size();
    }

    /**
     * 显示选择插入图片方式
     */
    public void showSelectImgDialog(final AlbumOrCameraListener listener) {
        final Dialog dialog = new Dialog(mActivity, R.style.Dialog);
        @SuppressWarnings("inflateParams")
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_album_or_camera, null);
        view.findViewById(R.id.tv_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlbum();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectCamera();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
    }

    /**
     * 调用相机
     */
    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            if (FileUtil.existsSdcard())
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            Uri uri;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                uri = Uri.fromFile(takeImageFile);
            } else {
                uri = FileProvider.getUriForFile(mActivity, AppUtils.getAppPackageName() + ".fileProvider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities
                        (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    mActivity.grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        mActivity.startActivityForResult(takePictureIntent, REQ_CODE_CAMERA);
    }

    /**
     * 裁剪图片
     */
    public void startClipPic(boolean isFillBlank) {
        if (isFillBlank) {
            GalleryFinal.init(mActivity);
            GalleryFinal.openCrop(takeImageFile.getAbsolutePath(), new GalleryFinal.OnHanlderResultCallback() {
                @Override
                public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                    if (resultList == null || resultList.isEmpty()) {
                        onHanlderFailure(reqeustCode, "裁剪失败");
                        return;
                    }
                    mView.clipSuccess(resultList.get(0).getPhotoPath());
                }

                @Override
                public void onHanlderFailure(int requestCode, String errorMsg) {
                    mView.clipFailed(errorMsg);
                }
            });
        }else {
            String imagePath = ImageUtils.getImagePath(mActivity, "/Crop/");
            File corpFile = new File(imagePath + ImageUtils.createFile());
            UCropUtils.start(mActivity, new File(takeImageFile.getAbsolutePath()), corpFile, false);
        }
    }

    public void uploadSubjectivePhoto(final List<Photo> photoLists) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
                RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);
                String key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
                    QiniuUptoken uptoken = GsonUtil.fromJson(result, QiniuUptoken.class);
                    if (uptoken == null || uptoken.getResult() == null || TextUtils.isEmpty(uptoken.getResult().getUpToken())) {
                        throw new RuntimeException("上传失败: 获取upToken失败");
                    }
                    QiniuUptoken.ResultBean bean = uptoken.getResult();
                    return Observable.just(new ImageUploadInfoBean(key, bean.getUpToken(), bean.getDomain(), bean.getImageStyle(), bitmap));
                } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        mView.networkError(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                            .create(bean.getDomain(), key, bean.getImageStyle());

                                    uploadPicVoList.add(dataBean);

                                    if (uploadPicVoList.size() == photoLists.size()) {
                                        pdm.cancelWaiteDialog();
                                        UploadPicVo mUploadPicVo = new UploadPicVo();
                                        mUploadPicVo.setData(uploadPicVoList);
                                        mView.onUploadPicSuccess(mUploadPicVo);
                                    }
                                } else {
                                    pdm.cancelWaiteDialog();
                                    mView.networkError(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }

    /**
     * 填空手写上传
     */
    public void whiteUploadFillBlank(String path) {
        whiteUpload(path, true);
    }

    /**
     * 简答手写上传
     */
    public void WhiteUploadSubjectivePhoto(String filePath) {
        whiteUpload(filePath, false);
    }

    /**
     * 手写上传
     *
     * @param isFillBlank 是否为填空
     */
    private void whiteUpload(final String filePath, final boolean isFillBlank) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");

        final String key = getKey();
        Network.createTokenService(NetWorkService.AnswerService.class)
                .getQiNiuUptoken(key)
                .map(new Func1<QiniuUptoken, QiniuUptoken.ResultBean>() {
                    @Override
                    public QiniuUptoken.ResultBean call(QiniuUptoken qiniuUptoken) {
                        if (qiniuUptoken == null || qiniuUptoken.getResult() == null || TextUtils.isEmpty(qiniuUptoken.getResult().getUpToken())) {
                            throw new RuntimeException("上传失败: 获取upToken失败");
                        }
                        return qiniuUptoken.getResult();
                    }
                })
                .map(new Func1<QiniuUptoken.ResultBean, Object[]>() {
                    @Override
                    public Object[] call(QiniuUptoken.ResultBean bean) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
                        return new Object[]{bean, qiNiuCompress(bitmap)};
                    }
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Object[]>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        pdm.cancelWaiteDialog();
                        mView.networkError(throwable.getMessage());
                        CrashReport.postCatchedException(throwable);
                    }

                    @Override
                    public void onNext(Object[] tokenBitmapArray) {
                        if (tokenBitmapArray == null || tokenBitmapArray.length != 2) {
                            throw new RuntimeException("上传失败");
                        }
                        final QiniuUptoken.ResultBean bean = (QiniuUptoken.ResultBean) tokenBitmapArray[0];
                        String token = bean.getUpToken();
                        byte[] bytes = bitmap2Bytes((Bitmap) tokenBitmapArray[1]);
                        new UploadManager().put(bytes, key, token, new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                pdm.cancelWaiteDialog();
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                            .create(bean.getDomain(), key, bean.getImageStyle());
//                                    if (isFillBlank) {
//                                        mView.onFillBlankWhiteUploadSuccess(dataBean);
//                                        return;
//                                    }

                                    List<UploadPicVo.DataBean> list = new ArrayList<>();
                                    list.add(dataBean);

                                    UploadPicVo uploadPicVo = new UploadPicVo();
                                    uploadPicVo.setData(list);

                                    mView.onWhiteUploadPicSuccess(uploadPicVo);
                                } else {
                                    mView.networkError(info.error);
                                }
                            }
                        }, null);
                    }
                });
    }

    /**
     * 每分钟调用该接口自动保存, 退出时也调用该接口进行保存
     */
    public void submitAnswerOneMin(List<AnswerListBean.AnswerDataBean> mList,
                                   TaskListVo.DataBean taskDataBean,
                                   long serverTime, final boolean showProgress) {
        if (mList == null || mList.isEmpty()) {
            mView.submitAnswerOneMinFailure("", showProgress);
            return;
        }
        String json = createSaveJson(mList);
        HashMap<String, Object> map = new HashMap<>();
        map.put("lessonTaskId", taskDataBean.getId());
        map.put("lessonTaskStudentId", taskDataBean.getLessonTaskStudentId());
        map.put("userStudentId", UserController.getUserId());
        map.put("userStudentName", UserController.getUserName());
        map.put("answerTime", serverTime);
        map.put("endTime", taskDataBean.getEndTime());
        map.put("replyExamJSON", json);
        map.put("access_token", UserController.getAccessToken());

        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .saveExam(map)
                .map(new StatusFunc<>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        if (mView == null) {
                            return;
                        }
                        mView.submitAnswerOneMinSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.submitAnswerOneMinFailure(status.getMsg(), showProgress);
                            } else {
                                mView.submitAnswerOneMinFailure("保存失败", showProgress);
                            }
                        } else {
                            mView.submitAnswerOneMinFailure("保存失败", showProgress);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, showProgress, false, false, "保存中..."));
    }

    /**
     * 创建保存/提交的作答结果json
     */
    private String createSaveJson(List<AnswerListBean.AnswerDataBean> mList) {
        ArrayList<ExamSubmitBean> list = new ArrayList<>();
        for (AnswerListBean.AnswerDataBean bean : mList) {
            if (bean.getType() == 3) {//纯文字
                list.add(ExamSubmitBean.initTextBean());
                continue;
            }
            if (bean.getType() == 2) {//资源
                list.add(ExamSubmitBean.initResBean(bean.getId(), bean.getLessonAnswerExamParentId(),
                        bean.getLessonAnswerExamId(), bean.getIsRead(), bean.getUseTime()));
                continue;
            }
            if (bean.getParentId() != 0) {//综合题忽略
                continue;
            }
            //按照题型取数据
            switch (bean.getExamTemplateId()) {
                case 1://单选
                    createSubmitBean(bean, list, true);
                    break;
                case 2://多选
                    createSubmitBean(bean, list, true);
                    break;
                case 3://判断
                    createSubmitBean(bean, list, true);
                    break;
                case 4://简答
                    createSubmitBean(bean, list, true);
                    break;
                case 6://填空
                    createSubmitBean(bean, list, false);
                    break;
                case 14://完型
                    createSubmitBean(bean, list, true);
                    break;
                case 16://综合
                    if (bean.getExamList() == null) {
                        continue;
                    }
                    for (AnswerListBean.AnswerDataBean dataBean : bean.getExamList()) {
                        switch (dataBean.getExamTemplateId()) {
                            case 1://单选
                                createSubmitBean(dataBean, list, true);
                                break;
                            case 2://多选
                                createSubmitBean(dataBean, list, true);
                                break;
                            case 3://判断
                                createSubmitBean(dataBean, list, true);
                                break;
                            case 4://简答
                                createSubmitBean(dataBean, list, true);
                                break;
                            case 6://填空
                                createSubmitBean(dataBean, list, false);
                                break;
                            //综合题没有完型和综合
                        }
                    }
                    break;
            }

        }
        return GsonUtil.toJson(list);
    }

    private void createSubmitBean(AnswerListBean.AnswerDataBean bean, ArrayList<ExamSubmitBean> list, boolean hideRight) {
        ExamSubmitBean examSubmitBean = ExamSubmitBean.init(bean.getType(), bean.getExamTemplateId(),
                bean.getScore(), bean.getId(), bean.getLessonAnswerExamParentId(),
                bean.getLessonAnswerExamId(), bean.getExamEmptyCount());
        ArrayList<ExamSubmitBean.ExamSubmitListBean> listBeans = null;
        List<AnswerListBean.ExamOptionBean> optionList = bean.getExamOptionList();
        if (optionList != null) {
            for (int i = 0; i < optionList.size(); i++) {
                AnswerListBean.ExamOptionBean optionBean = optionList.get(i);
                if (optionBean.getMyAnswer() == null || optionBean.getMyAnswer().isEmpty()) {
                    continue;
                }
                //空的个数
                List<AnswerListBean.ExamOptions> textOptions = bean.getExamBean().getExamOptions();
                ExamSubmitBean.ExamSubmitListBean listBean = ExamSubmitBean.ExamSubmitListBean.
                        create(bean.getIsAnswer() == 1,
                                optionBean.getMyAnswer(),
                                optionBean.getMyAnswerHtml(),
                                hideRight || textOptions == null || textOptions.isEmpty() || textOptions.size() <= i ?
                                        "" : textOptions.get(i).getContent(),
                                optionBean.getSeq(), bean.getExamTemplateId() == 6 ? optionBean.getAnswerType() : 0);
                if (listBeans == null) {
                    listBeans = new ArrayList<>();
                }
                listBeans.add(listBean);
            }
        }
        examSubmitBean.setAnswerList(listBeans);
        list.add(examSubmitBean);
    }

    /**
     * 显示确定取消提示弹窗
     */
    public void showDialog(String msg, final DialogInterface.OnClickListener listener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setMessage(msg);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.onClick(dialog, which);
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 简答题删除指定图片
     */
    public void removeImage(AnswerListBean.AnswerDataBean bean, int index) {
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list == null || list.isEmpty()) {
            return;
        }
        String myAnswer = list.get(0).getMyAnswer();
        if (TextUtils.isEmpty(myAnswer)) {
            bean.setExamOptionList(null);
            bean.setIsAnswer(0);
        } else {
//            if (split.length == 1) {
            String[] split1 = myAnswer.split(",");
            split1[index] = "";
            StringBuilder sb = new StringBuilder();
            for (String s : split1) {
                if (!s.isEmpty()) {
                    sb.append(s).append(",");
                }
            }
            if (sb.length() != 0) {
                sb.deleteCharAt(sb.length() - 1);
                list.get(0).setMyAnswer(sb.toString());
            } else {
                bean.setExamOptionList(null);
                bean.setIsAnswer(0);
            }
        }
    }

    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * 填空手写-读数据库加载本地数据
     */
    public void loadLocalData(final String appId, final String appNum, final int position) {
        CommonDaoUtils.getInstance()
                .getDaoSession()
                .getDotsDao()
                .rx()
                .loadAll()
                .compose(new Observable.Transformer<List<Dots>, List<Dots>>() {
                    @Override
                    public Observable<List<Dots>> call(Observable<List<Dots>> listObservable) {
                        return listObservable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).map(new Func1<List<Dots>, List<Dots>>() {
            @Override
            public List<Dots> call(List<Dots> list) {
                if (appNum == null) {
                    throw new RuntimeException("appNum is NULL");
                }
                if (appId == null) {
                    throw new RuntimeException("appId is NULL");
                }
                if (position == -1) {
                    throw new RuntimeException("position is -1");
                }
                if (list == null) {
                    return new ArrayList<>();
                }
                ArrayList<Dots> dots1 = new ArrayList<>();
                for (Dots dots : list) {
                    if (appNum.equals(dots.appNum) && appId.equals(dots.appId) && position == dots.position) {
                        dots1.add(dots);
                    }
                }
                return dots1;
            }
        }).subscribe(new ProgressSubscriber<>(new SubscriberListener<List<Dots>>() {
            @Override
            public void onNext(List<Dots> list) {
                mView.loadLocalDataSuccess(list);
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.showShortlToast(e.getMessage());
            }

            @Override
            public void onCompleted() {

            }
        }, mActivity, true, false, false, ""));
    }

    /**
     * 填空手写-删除手写的点
     */
    public void deleteDots(String appId, String appNum, int position) {
        CommonDaoUtils.getInstance()
                .getDaoSession()
                .getDotsDao()
                .queryBuilder()
                .where(DotsDao.Properties.AppNum.eq(appNum), DotsDao.Properties.AppId.eq(appId), DotsDao.Properties.Position.eq(position))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }

    /**
     * 填空手写-保存手写数据
     */
    public void saveDots(Collection<Dots> list, final RectF rectF) {
        if (list == null) {
            return;
        }
        CommonDaoUtils.getInstance()
                .getDaoSession()
                .getDotsDao()
                .rx()
                .saveInTx(list)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        mView.saveDotsSuccess(rectF);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.saveDotsFailed(e.getMessage());
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity, true, false, false, ""));
    }
}
