package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankCheckTextView;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.vo.ExamText;
import com.sdzn.fzx.student.vo.SubjectErrorBean;

import java.util.Collections;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：综合题小题的适配器
 * 修改内容：
 * 修改时间：on 2018/12/14
 * 修改单号：
 * 修改内容:
 */
public class AllAnserAdapter extends BaseAdapter {
    private List<SubjectErrorBean.DataBean> mList = null;
    private Context mContext;
    private boolean isOpen = true;

    public AllAnserAdapter(List<SubjectErrorBean.DataBean> List, Context mContext) {
        this.mList = List;
        this.mContext = mContext;
    }

    void setGoneParse(boolean isChecked) {
        Log.e("点击了关闭隐藏按钮", "点击了关闭隐藏按钮" + isChecked);
        isOpen = isChecked;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getExamTemplateId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = createItemView(getItemViewType(position));
        }
        if (convertView == null) {
            return null;
        }
        Object tag = convertView.getTag();
        if (tag instanceof SynSelectorHolder) {
            bindSelectorData(position, convertView, (SynSelectorHolder) tag);
        } else if (tag instanceof SynShortAnswerHolder) {
            bindShortAnswerData(position, convertView, (SynShortAnswerHolder) tag);
        } else if (tag instanceof SynFillBlankHolder) {
            bindFillBlank(position, convertView, (SynFillBlankHolder) tag);
        }
        return convertView;
    }

    private View createItemView(int itemViewType) {
        View view;
        SynHolder holder;
        switch (itemViewType) {
            case 1://单选
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 2://多选
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 3://判断
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 4://简答
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_short, null);
                holder = new SynShortAnswerHolder(view);
                view.setTag(holder);
                return view;
            case 6://填空
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_fill_blank_result, null);
                holder = new SynFillBlankHolder(view);
                view.setTag(holder);
                return view;
            case 14://完型
                return null;
            default://综合题不嵌套综合题
                return null;
        }
    }

    private static class SynHolder {
        TextView tvCount;
        TextView tvType;
        LinearLayout parsLy;

        SynHolder(View view) {
            tvCount = view.findViewById(R.id.tv_count);
            tvType = view.findViewById(R.id.tv_type);
            parsLy = view.findViewById(R.id.pars_ly);
        }
    }

    //单选多选判断
    private static class SynSelectorHolder extends SynHolder {
        HtmlTextView tvExam;//题干
        HtmlTextView tvPars;//解析
        HtmlTextView tvAnswer;//解析
        RadioGroup rg;//选项


        SynSelectorHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            rg = view.findViewById(R.id.rg_answer);
            tvPars = view.findViewById(R.id.tv_pars);
            tvAnswer = view.findViewById(R.id.tvAnswer);

        }
    }

    //简答
    private static class SynShortAnswerHolder extends SynHolder {
        HtmlTextView tvExam;
        HtmlTextView tvPars;//解析
        HtmlTextView tvAnswer;//解析

        SynShortAnswerHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv);
            tvPars = view.findViewById(R.id.tv_pars);
            tvAnswer = view.findViewById(R.id.tvAnswer);
        }
    }

    //填空
    private static class SynFillBlankHolder extends SynHolder {
        FillBlankCheckTextView tvExam;
        HtmlTextView tvPars;//解析
        HtmlTextView tvAnswer;//解析

        SynFillBlankHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv);
            tvPars = view.findViewById(R.id.tv_pars);
            tvAnswer = view.findViewById(R.id.tvAnswer);
        }
    }

    //统一设置标题
    private void bindItemTitle(int position, SynHolder holder) {
        int i = position + 1;
        holder.tvCount.setText("(" + i + ")");
        holder.tvType.setText(getExamType(mList.get(position).getExamTemplateId()));
        if (isOpen) {
            holder.parsLy.setVisibility(View.VISIBLE);
        } else {
            holder.parsLy.setVisibility(View.GONE);
        }
    }

    private String getExamType(int type) {
        switch (type) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知题型";
        }
    }

    //选择判断
    private void bindSelectorData(int position, View convertView, SynSelectorHolder holder) {

        bindItemTitle(position, holder);

        SubjectErrorBean.DataBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlText(examTextVo.getExamStem());
        holder.tvAnswer.setHtmlText(examTextVo.getExamAnswer());
        holder.tvPars.setHtmlText(examTextVo.getExamAnalysis());


        //无选项情况
        List<ExamText.ExamOptionsBean> examOptions = examTextVo.getExamOptions();
        if (examOptions == null || examOptions.isEmpty()) {
            holder.rg.removeAllViews();
            return;
        }
        //有选项
        Collections.sort(examOptions);
        int size = examOptions.size();
        int childCount = holder.rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            holder.rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(mContext).inflate(R.layout.item_child_select, holder.rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = holder.rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(examOptions.get(i).getContent());
            List<ExamText.ExamOptionsBean> examOptionss = dataBean.getExamTextVo().getExamOptions();
            if (examOptionss == null || examOptionss.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examOptionss);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examOptionss.get(0).getContent()));
            } else {//多选
                Collections.sort(examOptionss);
                for (ExamText.ExamOptionsBean optionBean : examOptionss) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getContent())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }

        }
    }

    /**
     * 填空
     */
    private void bindFillBlank(int position, View convertView, SynFillBlankHolder holder) {
        bindItemTitle(position, holder);
        SubjectErrorBean.DataBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlBody(examTextVo.getExamStem());
        holder.tvAnswer.setHtmlText(examTextVo.getExamAnswer());
        holder.tvPars.setHtmlText(examTextVo.getExamAnalysis());

    }

    /**
     * 简单题
     */
    private void bindShortAnswerData(int position, View convertView, SynShortAnswerHolder holder) {
        bindItemTitle(position, holder);
        SubjectErrorBean.DataBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlText(examTextVo.getExamStem());
        holder.tvAnswer.setHtmlText(examTextVo.getExamAnswer());
        holder.tvPars.setHtmlText(examTextVo.getExamAnalysis());

    }
}
