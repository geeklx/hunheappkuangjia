package com.sdzn.fzx.student.libbase.newbase;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.blankj.utilcode.util.ToastUtils;
import com.example.baselibrary.base.BaseAppManager;
import com.just.agentweb.LocalBroadcastManagers;
import com.just.agentweb.base.BaseStudentCurrencyAgentWebActivity;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.ai.pop.CustomDrawerPopupView;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.view.ExpandViewRect;

import me.jessyan.autosize.AutoSizeCompat;

/*加载WebView的BaseActivity*/
public abstract class BaseActWebActivity1 extends BaseStudentCurrencyAgentWebActivity {
    public TextView tvBack;//返回
    public TextView tvTitleName;//标题名称
    public XRecyclerView recyclerViewTitle;//RecyclerView滑动选择
    public TextView tvMyTitle;//个人中心按钮
    public LinearLayout Nonetwok;//无网络
    private long mCurrentMs = System.currentTimeMillis();
    public Activity activity;
    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("activityRefresh".equals(intent.getAction())) {
//                    final LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(activity)
//                            .dismissOnBackPressed(false)
//                            .asLoading("加载中")
//                            .show();
//                    loadingPopup.delayDismissWith(500, new Runnable() {
//                        @Override
//                        public void run() {
//                            loadingPopup.destroy();
//                        }
//                    });
                    if (mAgentWeb != null) {
                        mAgentWeb.getUrlLoader().reload();
//                        loadWebSite(mAgentWeb.getWebCreator().getWebView().getUrl());
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal(super.getResources());//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity(super.getResources(), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        BaseAppManager.getInstance().add(this);
        final LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(this)
                .dismissOnBackPressed(false)
                .asLoading("加载中")
                .show();
        loadingPopup.delayDismissWith(500, new Runnable() {
            @Override
            public void run() {
                loadingPopup.destroy();
            }
        });
        setup(savedInstanceState);
        activity = this;
        findiview();
    }


    /*加载布局*/
    protected abstract int getLayoutId();


    /**/
    protected void setup(@Nullable Bundle savedInstanceState) {
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);

        tvBack = (TextView) findViewById(R.id.tv_back);//返回
        Nonetwok = (LinearLayout) findViewById(R.id.ll_nonetwok);//无网络
        tvTitleName = (TextView) findViewById(R.id.tv_title_name);//标题名称
        recyclerViewTitle = (XRecyclerView) findViewById(R.id.recycler_view_title);//recyclerVie滑动
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);//个人中心按钮
        clickListener();
    }

    protected void findiview() {

    }

    /*绑定控件*/
    @Override
    protected ViewGroup getAgentWebParent() {
        View basecontainer = this.findViewById(R.id.ll_base_container);
        if (basecontainer != null) {
            return (ViewGroup) basecontainer;
        } else {
            return null;
        }
    }

    /*设置标题*/
    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "");
    }

    protected void setTitleContent(String title, String content) {
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 20) {
                title = title.substring(0, 20).concat("...");
            }
        }
        if (TextUtils.isEmpty(content)) {
            if (tvTitleName != null) {
                tvTitleName.setText(title);
            }
            return;
        }
        if (TextUtils.equals(title, content)) {
            if (tvTitleName != null) {
                tvTitleName.setText(title);
            }
        } else {
            if (tvTitleName != null) {
                tvTitleName.setText(content);
            }
        }
    }


    private BaseOnClickListener mListener;

    public void setBaseOnClickListener(BaseOnClickListener listener) {
        mListener = listener;
    }

    CustomDrawerPopupView customDrawerPopupView;

    //个人中心
    public void Titlegrzx() {
        customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("个人-----");
            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(customDrawerPopupView)
                .show();
    }

    /*返回*/
    public void TitleBack() {
        this.finish();
        Intent msgIntent = new Intent();
        msgIntent.setAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        finish();
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();//恢复
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause(); //暂停应用内所有WebView ， 调用mWebView.resumeTimers();/mAgentWeb.getWebLifeCycle().onResume(); 恢复。
        }
        super.onPause();
        if (customDrawerPopupView != null && customDrawerPopupView.isShow()) {
            customDrawerPopupView.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
        BaseAppManager.getInstance().remove(this);
        super.onDestroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }


    /*点击事件添加回调*/
    protected void clickListener() {
        /*返回点击*/
        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleBack();
                    }
                }
            });
            ExpandViewRect.expandViewTouchDelegate(tvBack, 5, 5, 20, 20);
        }
        if (tvMyTitle != null) {
            tvMyTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlegrzx();
                    }
                }
            });
        }
    }

    /**
     * @param LayoutStyle 返回判断
     */
    protected void TitleShowHideState(int LayoutStyle) {
        if (LayoutStyle == 1) { /*返回，title，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 2) {/*返回，title，中间RecyclerView,个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        }
    }
}
