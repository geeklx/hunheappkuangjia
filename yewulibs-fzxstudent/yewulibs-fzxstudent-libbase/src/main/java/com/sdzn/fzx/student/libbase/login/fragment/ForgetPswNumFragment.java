package com.sdzn.fzx.student.libbase.login.fragment;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.login.presenter.ForgetPswNumPresenter1;
import com.sdzn.fzx.student.libbase.login.view.ForgetPswNumViews;
import com.sdzn.fzx.student.libpublic.utils.DialogUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPswNumFragment extends BaseFragment implements ForgetPswNumViews, View.OnClickListener {

    ForgetPswNumPresenter1 mPresenter;
    private LinearLayout containerLy;
    private RelativeLayout titleContainerRy;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView titleNameTxt;
    private Button userNumIcon;
    private EditText userNumEdit;
    private ImageView editDelImg;
    private Button nextBtn;

    public ForgetPswNumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forget_psw_num, container, false);
        containerLy = (LinearLayout) view.findViewById(R.id.container_ly);
        titleContainerRy = (RelativeLayout) view.findViewById(R.id.title_container_ry);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        titleNameTxt = (TextView) view.findViewById(R.id.title_name_txt);
        userNumIcon = (Button) view.findViewById(R.id.user_num_icon);
        userNumEdit = (EditText) view.findViewById(R.id.user_num_edit);
        editDelImg = (ImageView) view.findViewById(R.id.edit_del_img);
        nextBtn = (Button) view.findViewById(R.id.next_btn);

        titleBackLy.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        editDelImg.setOnClickListener(this);
        initPresenter();
        initView();
        return view;
    }

    public void initPresenter() {
        mPresenter = new ForgetPswNumPresenter1();
        mPresenter.onCreate(this);
    }


    private void initView() {
        titleNameTxt.setText("找回密码");
        userNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                userNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.title_back_ly) {
            getActivity().finish();
        } else if (i == R.id.next_btn) {
            final String userName = userNumEdit.getText().toString();
            vertifyNum(userName);
            mPresenter.verityUserName(userName);
        } else if (i == R.id.edit_del_img) {
            userNumEdit.setText("");
        }
    }

    private boolean vertifyNum(final String userName) {
        if (userName.length() == 8 || userName.length() == 18) {
            return true;
        } else {
            ToastUtils.showShort("只能输入8位学生号或18位身份证号");
            return false;
        }

    }

    @Override
    public void verifySuccess(final String tel) {
        if (TextUtils.isEmpty(tel)) {
            new DialogUtils().createTipDialog(getActivity(), "该账号未绑定手机号，\n" +
                    "请联系学校管理员找回密码。").show();
            return;
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        VerifyNumFragment fragment = new VerifyNumFragment();
        Bundle bundle = new Bundle();
        bundle.putString(VerifyNumFragment.USER_PHONE, tel);
        fragment.setArguments(bundle);

        transaction.replace(R.id.forget_psw_fragment, fragment);
        transaction.commit();
        transaction.addToBackStack("tag");
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
