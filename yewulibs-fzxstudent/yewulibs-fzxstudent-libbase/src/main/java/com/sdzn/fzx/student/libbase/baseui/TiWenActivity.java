package com.sdzn.fzx.student.libbase.baseui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libpublic.event.CloseActivityEvent;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.ResponderData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TiWenActivity extends BaseActivity {
    public static final String TiWenStatus = "TiWenStatus";

    private ImageView iv;
    private ImageView ivClose;
    private int type, studentId, teacherId;
    private String id;
    private boolean isFirst = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ti_wen);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(TiWenActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        initView();
    }

    @Override
    protected void initView() {
        iv = (ImageView) findViewById(R.id.iv);
        ivClose = (ImageView) findViewById(R.id.iv_close);

        type = getIntent().getIntExtra("type", 0);
        id = getIntent().getStringExtra("Id");
        //1 抢答  2 随机
        if (type == 1) {
            teacherId = getIntent().getIntExtra("teacherId", 0);
            iv.setImageResource(R.mipmap.qiangda);
            ivClose.setVisibility(View.GONE);
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isFirst) {
                        getResponder(id, teacherId);
                    }
                }
            });
        } else {
            iv.setImageResource(R.mipmap.xuanzhongsuiji);
        }


        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TiWenActivity.this.finish();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCloseActivity(CloseActivityEvent event) {
        TiWenActivity.this.finish();
    }

    @Override
    protected void initData() {

    }

//    public enum TiWenState {
//        QiangDa(R.mipmap.qiangda,false,true),
//        ChengGong(R.mipmap.chenggong,true,false),
//        ShiBai(R.mipmap.shibai,true,false),
//        XuanZhong(R.mipmap.xuanzhongsuiji,true,false);
//
//        @DrawableRes
//        int imageRes;
//        boolean showClose;
//        boolean canTouchImage;
//
//        TiWenState(@DrawableRes int imageRes, boolean showClose, boolean canTouchImage) {
//            this.imageRes = imageRes;
//            this.showClose = showClose;
//            this.canTouchImage = canTouchImage;
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void getResponder(String questionId, int teacherId) {
        Map<String, Object> map = new HashMap<>();
        map.put("questionId", questionId);
        map.put("studentId", String.valueOf(UserController.getLoginBean().getData().getUser().getId()));
        map.put("classId", UserController.getClassId());
        map.put("exchangeStr", UserController.getLoginBean().getData().getUser().getCustomerSchoolName());
        map.put("teacherId", teacherId);
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getResponder(map)
                .map(new StatusFunc<ResponderData>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponderData>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showShortlToast(status.getMsg());
                            } else {

                            }
                        }
                    }

                    @Override
                    public void onNext(ResponderData o) {
                        ivClose.setVisibility(View.VISIBLE);
                        if (o.getData().isEmpty() || "".equals(o.getData())) {
                            iv.setImageResource(R.mipmap.shibai);
                        } else {
                            iv.setImageResource(R.mipmap.chenggong);
                        }
                        isFirst = true;

                    }
                });
    }

}
