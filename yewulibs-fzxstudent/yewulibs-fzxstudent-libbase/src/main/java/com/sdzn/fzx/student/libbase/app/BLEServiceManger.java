package com.sdzn.fzx.student.libbase.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.sdzn.fzx.student.libbase.event.BLEServiceEvent;
import com.sdzn.fzx.student.libbase.service.BluetoothLEService;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;

/**
 * @author Reisen at 2018-08-04
 */
public class BLEServiceManger {
    private Intent bleServiceIntent;
    private ServiceConnection mServiceConnection;
    private BluetoothLEService mService;
    private static BLEServiceManger mManger;

    private BLEServiceManger() {
        bleServiceIntent = new Intent(App2.get(), BluetoothLEService.class);
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = ((BluetoothLEService.LocalBinder) service).getService();
                EventBus.getDefault().post(new BLEServiceEvent(mService));
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                EventBus.getDefault().post(new BLEServiceEvent(null));
            }
        };
//        AppLike.mAppLike.mBLEServiceManger = this;
    }

    public static BLEServiceManger getInstance() {
        if (mManger == null) {
            synchronized (BLEServiceManger.class) {
                if (mManger == null) {
                    mManger = new BLEServiceManger();
                }
            }
        }
        return mManger;
    }

    public void bindService(Activity activity) {
        if (mService != null) {
            return;
        }
        try {
            activity.bindService(bleServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unbindService(Activity activity) {
        if (mService == null) {
            return;
        }
//        mService = null;
        try {
            activity.unbindService(mServiceConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startService() {
        App2.get().startService(bleServiceIntent);
    }

    public void stopService() {
        App2.get().stopService(bleServiceIntent);
    }

    public BluetoothLEService getService() {
        return mService;
    }

    public void destroyManger(Activity activity) {
        synchronized (BLEServiceManger.class) {
            unbindService(activity);
            stopService();
            bleServiceIntent = null;
            mServiceConnection = null;
            mManger = null;
//            AppLike.mAppLike.mBLEServiceManger = null;
        }
    }
}
