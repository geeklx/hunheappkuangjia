package com.sdzn.fzx.student.libbase.baseui.activity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.baseui.presenter.StatisticsPresenter;
import com.sdzn.fzx.student.libbase.baseui.view.StatisticsView;
import com.sdzn.fzx.student.libpublic.views.ChartLegendView;
import com.sdzn.fzx.student.libpublic.views.SemicircleProgressBar;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.StatisticsListVo;
import com.sdzn.fzx.student.vo.StatisticsScoreVo;
import com.sdzn.fzx.student.vo.StatisticsTimeVo;
import com.sdzn.fzx.student.vo.TaskListVo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 作答结果页面
 *
 * @author wangchunxiao
 * @date 2018/3/7
 */
public class StatisticsActivity extends MBaseActivity<StatisticsPresenter> implements StatisticsView, View.OnClickListener {

    private RelativeLayout netErrorRy;
    private TextView tvBack;
    private TextView tvTitle;
    private TextView tvScore;
    private SemicircleProgressBar spb;
    private TextView tvRank;
    private LinearLayout llLegend;
    private ChartLegendView clv1;
    private ChartLegendView clv3;
    private ChartLegendView clv2;
    private ChartLegendView clv4;
    private TextView tvTime;
    private LinearLayout llMyTime;
    private TextView tvMyTime;
    private ProgressBar mypb;
    private LinearLayout llClassTime;
    private TextView tvClassTime;
    private ProgressBar classpb;
    private RecyclerView rvStatistics;


    private Y_MultiRecyclerAdapter statisticsAdapter;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();
    private TaskListVo.DataBean taskDataBean;

    @Override
    public void initPresenter() {
        mPresenter = new StatisticsPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvScore = (TextView) findViewById(R.id.tvScore);
        spb = (SemicircleProgressBar) findViewById(R.id.spb);
        tvRank = (TextView) findViewById(R.id.tvRank);
        llLegend = (LinearLayout) findViewById(R.id.llLegend);
        clv1 = (ChartLegendView) findViewById(R.id.clv1);
        clv3 = (ChartLegendView) findViewById(R.id.clv3);
        clv2 = (ChartLegendView) findViewById(R.id.clv2);
        clv4 = (ChartLegendView) findViewById(R.id.clv4);
        tvTime = (TextView) findViewById(R.id.tvTime);
        llMyTime = (LinearLayout) findViewById(R.id.llMyTime);
        tvMyTime = (TextView) findViewById(R.id.tvMyTime);
        mypb = (ProgressBar) findViewById(R.id.mypb);
        llClassTime = (LinearLayout) findViewById(R.id.llClassTime);
        tvClassTime = (TextView) findViewById(R.id.tvClassTime);
        classpb = (ProgressBar) findViewById(R.id.classpb);
        rvStatistics = (RecyclerView) findViewById(R.id.rvStatistics);

        tvBack.setOnClickListener(this);
        initData();
        initView();
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(StatisticsActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(App2.get());
        rvStatistics.setLayoutManager(linearLayoutManager);
        statisticsAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        rvStatistics.setAdapter(statisticsAdapter);
    }

    @Override
    protected void initData() {
        taskDataBean = getIntent().getParcelableExtra("taskDataBean");
        if (taskDataBean.getScore() > 0) {//有分数
            tvScore.setVisibility(View.VISIBLE);
            spb.setVisibility(View.VISIBLE);
            tvRank.setVisibility(View.VISIBLE);
            llLegend.setVisibility(View.VISIBLE);
            mPresenter.getStatisticScore(String.valueOf(taskDataBean.getId()), String.valueOf(taskDataBean.getLessonTaskStudentId()));
        }
        mPresenter.getStatisticsTime(String.valueOf(taskDataBean.getId()), String.valueOf(taskDataBean.getLessonTaskStudentId()));
        mPresenter.getStatisticsList(String.valueOf(taskDataBean.getId()), String.valueOf(taskDataBean.getLessonTaskStudentId()));
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvBack) {
            finish();
        }
    }

    @Override
    public void getStatisticsScoreSuccess(StatisticsScoreVo statisticsScoreVo) {
        if (statisticsScoreVo != null && statisticsScoreVo.getData() != null) {
            StatisticsScoreVo.DataBean data = statisticsScoreVo.getData();
            spb.setMax(data.getSumScore());
            spb.setPercentData(data.getScoreTotal(), data.getScoreAvg(), data.getHighestScore());
            clv1.setCount(data.getScoreTotal());
            clv2.setCount(data.getScoreAvg());
            clv3.setCount(data.getSumScore());
            clv4.setCount(data.getHighestScore());
            clv1.setNameColor(getResources().getColor(R.color.fragment_main_item_num));
            clv2.setNameColor(getResources().getColor(R.color.fragment_main_item_num));
            clv3.setNameColor(getResources().getColor(R.color.fragment_main_item_num));
            clv4.setNameColor(getResources().getColor(R.color.fragment_main_item_num));
            clv1.setTextColor(getResources().getColor(R.color.activity_main_text1));
            clv2.setTextColor(getResources().getColor(R.color.activity_main_text1));
            clv3.setTextColor(getResources().getColor(R.color.activity_main_text1));
            clv4.setTextColor(getResources().getColor(R.color.activity_main_text1));
            tvRank.setText("第" + (int) data.getRanking() + "名");
        }
    }

    @Override
    public void getStatisticsTimeSuccess(StatisticsTimeVo statisticsTimeVo) {
        if (statisticsTimeVo != null && statisticsTimeVo.getData() != null) {
            StatisticsTimeVo.DataBean data = statisticsTimeVo.getData();
            int maxTime = 0;
            if (data.getUseTime() > data.getUseTimeAvg()) {
                maxTime = data.getUseTime();
            } else {
                maxTime = (int) data.getUseTimeAvg();
            }
            mypb.setMax(maxTime);
            classpb.setMax(maxTime);
            mypb.setProgress(data.getUseTime());
            classpb.setProgress((int) data.getUseTimeAvg());
            tvMyTime.setText(DateUtil.formatTimeDuration(data.getUseTime() * 1000));
            tvClassTime.setText(DateUtil.formatTimeDuration((int) (data.getUseTimeAvg() * 1000)));
        }
    }

    @Override
    public void getStatisticsListSuccess(StatisticsListVo statisticsTimeVo) {
        if (statisticsTimeVo != null && statisticsTimeVo.getData() != null && statisticsTimeVo.getData().size() > 0) {
            final List<StatisticsListVo.DataBean> data = statisticsTimeVo.getData();
            for (int i = 0; i < data.size(); i++) {
                StatisticsListVo.DataBean bean = data.get(i);//父级
                if (bean.getExamTemplateId() == 14 && bean.getClozeOption() != null && !bean.getClozeOption().isEmpty()) {//完形填空
                    data.remove(i);
                    ArrayList<StatisticsListVo.ClozeOption> list = bean.getClozeOption();
                    ArrayList<StatisticsListVo.DataBean> childList = new ArrayList<>();
                    for (StatisticsListVo.ClozeOption option : list) {
                        StatisticsListVo.DataBean bean1 = new StatisticsListVo.DataBean();
                        bean1.setParentSeq(bean.getSeq());
                        bean1.setSeq(option.getSeq());
                        bean1.setIsRight(option.getRight());
                        bean1.setExamTemplateStyleName(bean.getExamTemplateStyleName());
                        bean1.setScore(option.getScore());
                        bean1.setScoreAvg(option.getAvgScore());
                        bean1.setScoreTotal(option.getFullScore());
                        childList.add(bean1);
                    }
                    data.addAll(i, childList);
                }
            }
            itemEntityList.addItems(R.layout.item_statistics, statisticsTimeVo.getData())
                    .addOnBind(R.layout.item_statistics, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            if (position % 2 == 0) {
                                holder.getChildView(R.id.llbg).setBackgroundColor(getResources().getColor(R.color.white));
                            } else {
                                holder.getChildView(R.id.llbg).setBackgroundColor(getResources().getColor(R.color.fragment_main_bg));
                            }
                            StatisticsListVo.DataBean dataBean = (StatisticsListVo.DataBean) itemData;
                            if (dataBean.getParentSeq() == 0) {
                                holder.setText(R.id.tvNum, String.valueOf(dataBean.getSeq()));
                            } else {
                                if (dataBean.getSeq() == 1) {
                                    holder.setText(R.id.tvNum, String.valueOf(dataBean.getParentSeq()) + "(" + String.valueOf(dataBean.getSeq()) + ")");
                                } else {
                                    holder.setText(R.id.tvNum, " (" + String.valueOf(dataBean.getSeq()) + ")");
                                }
                            }
                            holder.setText(R.id.tvType, dataBean.getExamTemplateStyleName());
                            if ((dataBean.getExamTemplateId() == 6 || dataBean.getExamTemplateId() == 4)
                                    && (taskDataBean == null || taskDataBean.getIsCorrect() != 1)) {
                                holder.getChildView(R.id.tvCorrect).setVisibility(View.INVISIBLE);//填空未批改隐藏按状态
                            } else if (dataBean.getExamTemplateId() == 4 || dataBean.getExamTemplateId() == 6) {
                                if (dataBean.getScoreTotal() > 0 && dataBean.getIsRight() != 0) {
                                    holder.getChildView(R.id.tvCorrect).setVisibility(View.VISIBLE);
                                    if (dataBean.getScore() == dataBean.getScoreTotal()) {
                                        ((ImageView) holder.getChildView(R.id.tvCorrect)).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.quandui_img));
                                    } else if (dataBean.getScore() == 0) {
                                        ((ImageView) holder.getChildView(R.id.tvCorrect)).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.quancuo_img));
                                    } else {
                                        ((ImageView) holder.getChildView(R.id.tvCorrect)).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.bandui_img));
                                    }
                                } else {
                                    holder.getChildView(R.id.tvCorrect).setVisibility(View.INVISIBLE);
                                }
                            } else {
                                holder.getChildView(R.id.tvCorrect).setVisibility(View.VISIBLE);
                                if (dataBean.getIsRight() == 1) {
                                    ((ImageView) holder.getChildView(R.id.tvCorrect)).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.quandui_img));
                                } else if (dataBean.getIsRight() == 2 || dataBean.getIsRight() == 0) {
                                    ((ImageView) holder.getChildView(R.id.tvCorrect)).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.quancuo_img));
                                } else if (dataBean.getIsRight() == 3) {
                                    ((ImageView) holder.getChildView(R.id.tvCorrect)).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.bandui_img));
                                } else {
                                    holder.getChildView(R.id.tvCorrect).setVisibility(View.INVISIBLE);
                                }
                            }
                            if (taskDataBean.getScore() <= 0) {
                                holder.setText(R.id.tvScore, "─");
                                holder.setText(R.id.tvTotal, "─");
                                holder.setText(R.id.tvAverage, "─");
                            } else {
                                holder.setText(R.id.tvScore, String.valueOf(dataBean.getScore()));
                                holder.setText(R.id.tvTotal, String.valueOf(dataBean.getScoreTotal()));
                                holder.setText(R.id.tvAverage, String.valueOf(dataBean.getScoreAvg()));
                            }
                        }
                    });
        }
        statisticsAdapter.notifyDataSetChanged();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
