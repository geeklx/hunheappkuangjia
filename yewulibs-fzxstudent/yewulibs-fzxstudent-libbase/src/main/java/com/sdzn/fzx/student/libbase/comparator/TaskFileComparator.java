package com.sdzn.fzx.student.libbase.comparator;


import com.sdzn.fzx.student.vo.TaskFileVo;

import java.util.Comparator;

/**
 * 任务排序器
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class TaskFileComparator implements Comparator<Object> {
    @Override
    public int compare(Object t1, Object t2) {
        long time1 = ((TaskFileVo.DataBean) t1).getUpdateTime();
        long time2 = ((TaskFileVo.DataBean) t2).getUpdateTime();
        if (time1 > time2) {
            return -1;
        } else if (time1 < time2) {
            return 1;
        } else {
            return 0;
        }
    }
}
