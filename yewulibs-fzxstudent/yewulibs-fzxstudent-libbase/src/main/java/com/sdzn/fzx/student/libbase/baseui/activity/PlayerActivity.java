package com.sdzn.fzx.student.libbase.baseui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.RadioServiceManager;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.baseui.fragment.PlayerFragment;
import com.sdzn.fzx.student.libbase.baseui.fragment.RadioViewFragment;
import com.sdzn.fzx.student.libbase.baseui.fragment.VideoViewFragment;
import com.sdzn.fzx.student.libbase.service.RadioService;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;

import org.eazegraph.lib.utils.Utils;

public class PlayerActivity extends BaseActivity {
    public static final String SHOW_MINI_BUTTON = "showMiniButton";

    private FrameLayout flContent;

    private PlayerFragment playerFragment;
    private int screenHeight;
    private int screenWidth;
    private int FL_HEIGHT;
    public static final int FULL_SCREEN = 0;
    public static final int NORMAL_SCREEN = 1;
    private String videoUrl;
    private String radioUrl;
    private boolean showMiniButton;
    private String title;
    private long startTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        flContent = findViewById(R.id.flContent);
        initData();
        initView();
        startTime = System.currentTimeMillis();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(PlayerActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        initVideo(title);
    }

    @Override
    protected void initData() {
        FL_HEIGHT = (int) Utils.dpToPx(400);
        videoUrl = getIntent().getStringExtra("videoUrl");
        radioUrl = getIntent().getStringExtra("radioUrl");
        showMiniButton = getIntent().getBooleanExtra(SHOW_MINI_BUTTON, false);
        title = getIntent().getStringExtra("title");
    }


    private void initVideo(String title) {
        getScreenSize();
        setVideoLayoutParams();

        Bundle videoBundle = new Bundle();

        if (!TextUtils.isEmpty(videoUrl)) {
            playerFragment = new VideoViewFragment();
            videoBundle.putString("path", videoUrl);
        } else {
            playerFragment = new RadioViewFragment();
            videoBundle.putString("path", radioUrl);
            videoBundle.putBoolean(SHOW_MINI_BUTTON, showMiniButton);
        }

        videoBundle.putString("title", title);
        videoBundle.putBoolean("setScreenSize", true);
        videoBundle.putInt("width", screenWidth);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            videoBundle.putInt("height", screenHeight);
        } else {
            videoBundle.putInt("height", FL_HEIGHT);
        }
        playerFragment.setArguments(videoBundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, playerFragment).commit();
    }

    private void setVideoLayoutParams(int width, int height) {
        setFlContent(width, height);
        if (playerFragment != null) {
            playerFragment.setScreenSizeParams(width, height);
        }
    }

    private void setFlContent(int width, int height) {
        flContent.getLayoutParams().height = height;
        flContent.getLayoutParams().width = width;
    }

    private void setVideoLayoutParams() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setVideoLayoutParams(screenWidth, screenHeight);
        } else {
            setVideoLayoutParams(screenWidth, FL_HEIGHT);
        }
    }

    /**
     * 获取屏幕宽高
     */
    private void getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        screenHeight = display.getHeight();
        screenWidth = display.getWidth();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getScreenSize();
        setVideoLayoutParams();
    }

    /**
     * 切换全屏并横屏
     *
     * @param screen
     */
    public void setScreen(int screen) {
        if (screen == FULL_SCREEN) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else if (screen == NORMAL_SCREEN) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            back();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void back() {
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null) {
//            service.stop();
            service.cleanAndReset();
        }
        Intent intent = new Intent();
        intent.putExtra("isMini", false);
        intent.putExtra("time", (System.currentTimeMillis() - startTime) / 1000);
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }

    public void miniMize() {
        Intent intent = new Intent();
//        intent.putExtra("time", (System.currentTimeMillis() - startTime) / 1000);
        intent.putExtra("isMini", true);
        intent.putExtra("time", (System.currentTimeMillis() - startTime) / 1000);
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }
}
