package com.sdzn.fzx.student.libbase.ai.pop;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.core.DrawerPopupView;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.sdzn.fzx.student.libbase.ai.pop.bean.GRZXBaseRecActBean;

import java.util.List;

/**
 * Description: 自定义抽屉弹窗
 * <p>
 * //通过设置topMargin，可以让Drawer弹窗进行局部阴影展示
 * //        ViewGroup.MarginLayoutParams params = (MarginLayoutParams) getPopupContentView().getLayoutParams();
 * //        params.topMargin = 450;
 */
public class CustomDrawerPopupView1 extends DrawerPopupView {
    private RecyclerView mRecyclerView;
    private List<GRZXBaseRecActBean> mList;
    private GRZXBaseRecActAdapter mAdapter;

    GrzxNextCallBack grzxNextCallBack;

    public CustomDrawerPopupView1(@NonNull Context context) {
        super(context);
    }

    public CustomDrawerPopupView1(@NonNull Context context, GrzxNextCallBack mGrzxNextCallBack) {
        super(context);
        this.grzxNextCallBack = mGrzxNextCallBack;
    }


    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_right_drawer;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
//        Log.e("tag", "CustomDrawerPopupView onCreate");
        findview();
//        donetwork();
//        onclicklistener();
    }


    private void findview() {
        mRecyclerView = findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false));

    }

//    private void donetwork() {
//        mList = new ArrayList<>();
//        mList = getMultipleItemData(2);
//        mAdapter = new GRZXBaseRecActAdapter(mList);
//        mAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
//        mAdapter.setNotDoAnimationCount(3);// mFirstPageItemCount
//
//        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
//                int type = mList.get(position).type;
//                if (type == GRZXBaseRecActBean.style1) {
//                    return 2;
//                } else {
//                    return 1;
//                }
//            }
//        });
//
//        mRecyclerView.setAdapter(mAdapter);
//
//    }

//    private void onclicklistener() {
//        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                GRZXBaseRecActBean addressBean = mList.get(position);
//                int i = view.getId();
//                if (i == R.id.iv_item) {
//
//                    if (!addressBean.getmBean().getCreatedAt().isEmpty()) {
//                        if ("com.sdzn.pkt.student.hd".equals(addressBean.getmBean().getCreatedAt())) {
//                            if (AppUtils.isAppInstalled("com.sdzn.pkt.student.hd")) {
//                                String name = StudentSPUtils.getLoginUserNum();
//                                String pwd = StudentSPUtils.getLoginUserPwd();
//                                String state = "1";
//                                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
//                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd + "&state=" + state));
//                                getContext().startActivity(intent);
//                            } else {
////                ToastUtils.showLong("跳转异常，请检查跳转配置、包名及Activity访问权限");
//                                ToastUtils.showLong("请先安装智囊学堂学生端APP");
//                            }
//                        }else if (".hs.act.WdpgActivity".equals(addressBean.getmBean().getCreatedAt())){
//                            Intent intent = new Intent(AppUtils.getAppPackageName() + addressBean.getmBean().getCreatedAt());
//                            if (!addressBean.getmBean().getAddress().isEmpty()) {
//                                intent.putExtra("url_key", "http://49.4.7.45:8080/#/taskCorrecting/index");
//                            }
//                            getContext().startActivity(intent);
//
//                        } else {
//                            Intent intent = new Intent(AppUtils.getAppPackageName() + addressBean.getmBean().getCreatedAt());
//                            if (!addressBean.getmBean().getAddress().isEmpty()) {
//                                intent.putExtra("url_key", com.blankj.utilcode.util.SPUtils.getInstance().getString("url", "https://www.baidu.com/") + addressBean.getmBean().getAddress());
//                            }
//                            intent.putExtra("show", "1");
//                            getContext().startActivity(intent);
//                        }
//                    }
//                    dismiss();
//                }
//            }
//        });
//
//        findViewById(R.id.btn).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
//        findViewById(R.id.tv_grzx).setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
//                getContext().startActivity(intent);
//                dismiss();
////                if (grzxNextCallBack != null) {
////                    grzxNextCallBack.toGrzxNextClick();
////                }
//            }
//        });
//    }

    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

//    public static List<GRZXBaseRecActBean> getMultipleItemData(int lenth) {
//        List<GRZXBaseRecActBean> list = new ArrayList<>();
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, new GRZXBaseRecActChildBean("课程任务")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_1,".hs.act.ZzxxActivity","autonomyLearn/index")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_2,".hs.act.KtxxActivity","classroom")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_3,".hs.act.WdzyActivity","homework/index")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, new GRZXBaseRecActChildBean("学习工具")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_4,".hs.act.KSActivity","examination/index")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_5,".hs.act.HZTJActivity","cooperation")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_6,".hs.act.XqfxShouyeActivity","")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_7,".hs.act.WdpgActivity","http://49.4.7.45:8080/#/taskCorrecting/index")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, new GRZXBaseRecActChildBean("学习提升")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_8,"com.sdzn.pkt.student.hd","")));
//        return list;
//    }

    public interface GrzxNextCallBack {
        void toGrzxNextClick();
    }
}