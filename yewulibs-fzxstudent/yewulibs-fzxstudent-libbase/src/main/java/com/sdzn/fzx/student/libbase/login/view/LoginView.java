package com.sdzn.fzx.student.libbase.login.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.LoginBean;

/**
 * LoginView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface LoginView extends BaseView {

    void loginSuccess(LoginBean vo);
}
