package com.sdzn.fzx.student.libbase.baseui.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.TodayNativeVo;

public interface TodayNativeErrorView extends BaseView {
    void setTodayListSuccess(TodayNativeVo todayNativeVo);
}
