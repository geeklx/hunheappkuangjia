package com.sdzn.fzx.student.libbase.login.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.login.presenter.VerifyNumPresenter1;
import com.sdzn.fzx.student.libbase.login.view.VerifyNumViews;
import com.sdzn.fzx.student.libbase.utils.YanzhengUtil;
import com.sdzn.fzx.student.libpublic.utils.DialogUtils;
import com.sdzn.fzx.student.libutils.util.StringUtils;

import java.io.InputStream;


/**
 * 忘记密码
 * A simple {@link Fragment} subclass.
 */
public class VerifyNumFragment extends BaseFragment implements VerifyNumViews, View.OnClickListener {
    VerifyNumPresenter1 mPresenter;
    public static final String USER_PHONE = "user_phone";
    private LinearLayout titleBackLy;//返回
    private TextView titleNameTxt;//title标题
    private Button verityNumIcon;//验证码标识图片
    private Button userNumIcon;//手机号标识图片
    private EditText verityNumEdit;//验证码输入框
    private EditText userNumEdit;//手机输入框
    private EditText valicodeEdit;//图形验证码输入框
    private TextView countDownBtn;//获取验证码按钮
    private ImageView Effectiveness;//图形验证码按钮
    private ImageView editDelImg;//手机号清除
    private Button nextBtn;//下一步


    public VerifyNumFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verify_num1, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleNameTxt = (TextView) view.findViewById(R.id.title_name_txt);
        verityNumIcon = (Button) view.findViewById(R.id.verity_num_icon);
        userNumIcon = view.findViewById(R.id.user_num_icon);
        verityNumEdit = (EditText) view.findViewById(R.id.verity_num_edit);
        userNumEdit = (EditText) view.findViewById(R.id.user_num_edit);
        valicodeEdit = (EditText) view.findViewById(R.id.valicode_edit);
        editDelImg = view.findViewById(R.id.edit_del_img);
        Effectiveness = view.findViewById(R.id.iv_effectiveness);
        countDownBtn = (TextView) view.findViewById(R.id.count_down_btn);
        nextBtn = (Button) view.findViewById(R.id.next_btn);
        initPresenter();
        initView();
        return view;
    }


    public void initPresenter() {
        mPresenter = new VerifyNumPresenter1();
        mPresenter.onCreate(this);
    }

    private void initView() {
        titleBackLy.setOnClickListener(this);
        countDownBtn.setOnClickListener(this);
        editDelImg.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        Effectiveness.setOnClickListener(this);
        titleNameTxt.setText("找回密码");
        mPresenter.QueryImgToken();
        userNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                userNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        verityNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                verityNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestory();
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.title_back_ly) {//返回
            getActivity().onBackPressed();
        } else if (i == R.id.edit_del_img) {
            userNumEdit.setText("");
        } else if (i == R.id.count_down_btn) {//获取验证码
            final String userName = userNumEdit.getText().toString();
            if (userName.equals("")) {
                ToastUtils.showShort("手机号不能为空");
                return;
            }
            mPresenter.VerifyMobilePhoneNumber(userName);
        } else if (i == R.id.next_btn) {//下一步
            if (userNumEdit.getText().toString().trim() == null) {
                ToastUtils.showShort("手机号不能为空");
                return;
            }else if (!StringUtils.isMobile(userNumEdit.getText().toString().trim()) && userNumEdit.getText().toString().trim().length() != 11) {
                ToastUtils.showShort("请输入正确的账号或手机号");
                return;
            } else if (valicodeEdit.getText().toString().trim() == null) {
                ToastUtils.showShort("图形验证码不能为空");
                return;
            } else if (verityNumEdit.getText().toString().trim() == null) {
                ToastUtils.showShort("验证码不能为空");
                return;
            }
            mPresenter.checkVerityNum(userNumEdit.getText().toString().trim(), verityNumEdit.getText().toString());
        } else if (i == R.id.iv_effectiveness) {//获取图形验证码
            mPresenter.QueryImgToken();
        }
    }

    @Override
    public void OnVerifyMobilePhoneSuccess(String tel) {
        if (tel == null){
            new DialogUtils().createTipDialog(getActivity(), "该账号未绑定手机号，\n" +
                    "请联系学校管理员找回密码。").show();
            return;
        }
        if (userNumEdit.getText().toString().trim().equals("")) {
            ToastUtils.showShort("手机号不能为空");
        } else if (valicodeEdit.getText().toString().trim().equals("")) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.sendVerityCode(tel, valicodeEdit.getText().toString().trim(), imgToken);
        }
    }

    @Override
    public void OnVerifyMobilePhoneFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onVerityCodeSuccess(Object sueccess) {
        ToastUtils.showShort(String.valueOf(sueccess));
        YanzhengUtil.startTime(60 * 1000, countDownBtn);//倒计时bufen
    }

    @Override
    public void onVerityCodeFailed(String msg) {
        valicodeEdit.setText("");
        ToastUtils.showShort("获取验证码失败");
        mPresenter.QueryImgToken();
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgTokenFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        Bitmap bitimg = BitmapFactory.decodeStream(inputStream);
        Glide.with(getActivity()).load(bitimg).into(Effectiveness);
    }

    @Override
    public void OnImgFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onCheckCodeSuccess(final String phone, final String code) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        ResetPswFragment fragment = new ResetPswFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ResetPswFragment.USER_PHONE, phone);
        bundle.putString(ResetPswFragment.VERIFT_CODE, code);

        fragment.setArguments(bundle);


        transaction.replace(R.id.forget_psw_fragment, fragment);
        transaction.commit();
        transaction.addToBackStack("tag");
    }

    @Override
    public void onCheckCodeFailed(String msg) {
        valicodeEdit.setText("");
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    @Override
    public String getIdentifier() {
        return null;
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestory();
        YanzhengUtil.timer_des();
        super.onDestroy();
    }
}
