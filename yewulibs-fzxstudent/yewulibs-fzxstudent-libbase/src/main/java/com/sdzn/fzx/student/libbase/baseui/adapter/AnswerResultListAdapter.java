package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.libbase.listener.OnExamResultClickListener;
import com.sdzn.fzx.student.libpublic.utils.CustomClickListener;
import com.sdzn.fzx.student.libpublic.utils.InputTools;
import com.sdzn.fzx.student.libpublic.views.exam.ClozeHtmlTextView;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankCheckTextView;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.libutils.annotations.CheckState;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 试题列表adapter
 *
 * @author Reisen at 2018-08-29
 */
public class AnswerResultListAdapter extends BaseRcvAdapter<AnswerListBean.AnswerDataBean> {
    private OnExamResultClickListener mListener;
    private boolean showErrorExam;//只看错题
    private boolean showAnswer;//答案
    private boolean isCorrect;//是否已批改

    public AnswerResultListAdapter(Context context, List<AnswerListBean.AnswerDataBean> mList) {
        super(context, mList);
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    public void setIsCorrect(boolean correct) {
        isCorrect = correct;
        notifyDataSetChanged();
    }

    public void setListener(OnExamResultClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        AnswerListBean.AnswerDataBean bean = mList.get(position);
        return bean.getType() * 10000 + bean.getExamTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 20000://资源
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_resource);
            case 30000://文本
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_text);
            case 10001://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_select);
            case 10002://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_select);
            case 10003://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_select);
            case 10004://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_short_answer);
            case 10006://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_fill_blank);
            case 10014://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_other_type);
            case 10016://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_result_exam_synthesize);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        if (bean.getType() == 2) {//资源
            bindResource(holder, position, bean);
            return;
        }
        if (bean.getType() == 3) {//文字
            bindText(holder, position, bean);
            return;
        }
        switch (bean.getExamTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindClozeTest(holder, position, bean);//完型填空
                break;
            case 16:
                bindSynthesize(holder, position, bean);//综合
                break;
            default:
//                bindOtherType(holder, position, bean);//其他类型
                break;
        }
    }

    /**
     * 综合
     */
    private void bindSynthesize(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        //标题部分
        ImageView iv_collect = holder.getView(R.id.iv_collect);
        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        holder.setText(R.id.tv_count, bean.getExamSeq() + ". ");
        int score = (int) bean.getScore();
        if (score <= 0) {
            holder.setVisible(R.id.tv_score, false);
        } else {
            holder.setVisible(R.id.tv_score, true);
            holder.setText(R.id.tv_score, "(本题" + score + "分)");
        }
        holder.setText(R.id.tv_type, bean.getExamTemplateStyleName());
        iv_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.changeCollect(bean);
                }
            }
        });
        //综合题题干
        HtmlTextView exam = holder.getView(R.id.tv_exam);
        exam.setHtmlText(bean.getExamBean().getExamStem());
        //试题列表
        ListView examListView = holder.getView(R.id.lv);
        AnswerCompleteSynthesizeAdapter synAdapter = new AnswerCompleteSynthesizeAdapter(bean.getExamList(), context, isCorrect);
        synAdapter.setShowAnswer(showAnswer);
        synAdapter.setListener(mListener);
        examListView.setAdapter(synAdapter);
        setListHeight(examListView);
    }


    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(examTextBean == null ? "" : examTextBean.getExamStem());

        RadioGroup rg = holder.getView(R.id.rg_answer);
        if (examTextBean == null) {
            rg.removeAllViews();
            return;
        }
        List<AnswerListBean.ExamOptions> options = examTextBean.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_result_child_select, rg, true);
                childCount++;
            }
        }

        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            String str = String.valueOf((char) (65 + i));
            tvNumber.setText(str);

            tvText.setHtmlText(options.get(i).getContent());
            final List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextBean.getExamTypeId() == 1) {//单选/判断
                tvNumber.setBackground(context.getResources().getDrawable(
                        bean.getExamBean().getExamOptions().get(i).isRight() ?
                                R.drawable.bg_result_select_selector_y :
                                R.drawable.bg_result_select_selector_n));
                tvNumber.setSelected(TextUtils.equals(str, examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (AnswerListBean.ExamOptionBean optionBean : examList) {
                    if (TextUtils.equals(str, optionBean.getMyAnswer())) {
                        tvNumber.setBackground(context.getResources().getDrawable(
                                bean.getExamBean().getExamOptions().get(i).isRight() ?
                                        R.drawable.bg_result_select_selector_y :
                                        R.drawable.bg_result_select_selector_n));
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }
    }


    /**
     * 文本
     */
    private void bindText(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getResourceText());
    }

    /**
     * 资源
     */
    private void bindResource(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        ImageView iv_collect = holder.getView(R.id.iv_collect);
        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        holder.setText(R.id.tv_name, bean.getResourceName());
        ImageView iv_type = holder.getView(R.id.iv_type);
//        final ResourceVo.ResourceTextVo resourceBean = bean.getResourceVoBean();
        //1文档类2演示稿3视频类4图片类5音频类
        switch (bean.getResourceType()) {
            case 1:
                if (bean.getResourceVoBean() == null) {
                    iv_type.setImageResource(R.drawable.word);
                }else if (".txt".equalsIgnoreCase(bean.getResourceVoBean().getResourceSuffix())) {
                    iv_type.setImageResource(R.drawable.txt);
                } else {
                    iv_type.setImageResource(R.drawable.word);
                }
                break;
            case 2:
                iv_type.setImageResource(R.drawable.ppt);
                break;
            case 3:
                iv_type.setImageResource(R.drawable.video);
                break;
            case 4:
                iv_type.setImageResource(R.drawable.image);
                break;
            case 5:
                iv_type.setImageResource(R.drawable.music);
                break;
            default:
                iv_type.setImageResource(R.drawable.pdf);
                break;
        }
   /*     holder.getView(R.id.ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.showResource(bean);
                }
            }
        });*/


        holder.getView(R.id.ll).setOnClickListener(new CustomClickListener() {
            @Override
            protected void onSingleClick() {
                if (mListener != null) {
                    mListener.showResource(bean);
                }
            }

            @Override
            protected void onFastClick() {
                ToastUtil.showLonglToast("请勿重复点击");
            }
        });
        iv_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.changeCollect(bean);
                }

            }
        });
    }

    /**
     * 简答
     */
    private void bindShortAnswer(final BaseViewHolder holder, final int position, final AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);

        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        holder.getView(R.id.tv_exam).setVisibility(View.VISIBLE);
        HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(examBean == null ? "" : examBean.getExamStem());

        //该题作答状态, true=未作答
        final boolean unAnswer = bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty() ||
                TextUtils.isEmpty(bean.getExamOptionList().get(0).getMyAnswer());
        if (unAnswer) {//未作答
            holder.setVisible(R.id.tv_un_answer, true);
            holder.setVisible(R.id.rv_add_pic, false);
        } else {//已做达
            holder.setVisible(R.id.tv_un_answer, false);
            holder.setVisible(R.id.rv_add_pic, true);
        }

        CheckBox s = holder.getView(R.id.switcher);
        if (unAnswer) {
            s.setOnCheckedChangeListener(null);
            return;
        }

        RecyclerView rv = holder.getView(R.id.rv_add_pic);
        rv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(context);
            rv.setAdapter(adapter);
            adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
                @Override
                public void clickImage(int index, String url) {
                    if (mListener != null) {
                        mListener.openImg(index, url, true, bean);
                    }
                }
            });
        }
        List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
        for (AnswerListBean.ExamOptionBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }

        s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String answer = isChecked ? bean.getExamOptionList().get(0).getMyAnswer() :
                        bean.getExamOptionList().get(0).getMyOldAnswer();
                ShortAnswerResultAdapter shortAdapter = (ShortAnswerResultAdapter) ((RecyclerView) holder.getView(R.id.rv_add_pic)).getAdapter();
                if (answer == null) {
                    shortAdapter.setList(new String[]{}, null);
                    return;
                }
                String[] arr = answer.split(";");
                if (arr.length == 1) {
                    shortAdapter.setList(arr[0].split(","), null);
                } else {
                    shortAdapter.setList(arr[1].split(","), arr[0].split(","));
                }
            }
        });
    }

    /**
     * 完型填空
     */
    private void bindClozeTest(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        //标题部分
        ImageView iv_collect = holder.getView(R.id.iv_collect);
        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        holder.setText(R.id.tv_count, bean.getExamSeq() + ". ");
        int score = (int) bean.getScore();
        if (score <= 0 || bean.getExamBean() == null) {
            holder.setVisible(R.id.tv_score, false);
        } else {
            holder.setVisible(R.id.tv_score, true);
            if (bean.getExamTemplateId() == 14 || bean.getExamTemplateId() == 6) {
                score *= bean.getExamBean().getExamOptions().size();
            }
            holder.setText(R.id.tv_score, "(本题" + score + "分)");

        }
        holder.setText(R.id.tv_type, bean.getExamTemplateStyleName());
        iv_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.changeCollect(bean);
                }
            }
        });
        //答案部分
        holder.getView(R.id.rl_result).setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        if (examTextBean == null) {
            analysis.setHtmlText("");
            ClozeHtmlTextView clozeHtmlTextView = holder.getView(R.id.tv);
            clozeHtmlTextView.setHtmlText("", new ArrayList<AnswerListBean.ExamOptionBean>());
            return;
        }
        analysis.setHtmlText(examTextBean.getExamAnalysis());
        ClozeHtmlTextView clozeHtmlTextView = holder.getView(R.id.tv);
        clozeHtmlTextView.setHtmlText(bean.getExamBean().getExamStem(), bean.getExamOptionList());
        setOptionContent((LinearLayout) holder.getView(R.id.ll_result), bean.getExamBean().getExamOptions());
    }

    /**
     * 填空
     */
    private void bindFillBlank(BaseViewHolder holder, final int position, AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);
        FillBlankCheckTextView tv = holder.getView(R.id.tv);
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        SparseArray<ClozeAnswerBean> array = new SparseArray<>();
        array.clear();
        for (int i = 0; i < list.size(); i++) {
            AnswerListBean.ExamOptionBean optionBean = list.get(i);
            int checkState;
            if (isCorrect || InputTools.isKeyboard(optionBean.getAnswerType())) {
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                }else {
                    checkState = CheckState.FALSE;
                }
            }else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(), optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(),checkState));
        }
        tv.setHtmlBody(bean.getExamBean().getExamStem(), array);
        tv.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                mListener.openImg(0,imageSrc,false,null);
            }
        });
    }

    /**
     * 试题标题
     */
    private void bindExamTitle(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        //标题部分
        ImageView iv_collect = holder.getView(R.id.iv_collect);
        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        holder.setText(R.id.tv_count, bean.getExamSeq() + ". ");
        int score = (int) bean.getScore();
        if (score <= 0 || bean.getExamBean() == null) {
            holder.setVisible(R.id.tv_score, false);
        } else {
            holder.setVisible(R.id.tv_score, true);
            if (bean.getExamTemplateId() == 6) {
                score *= bean.getExamBean().getExamOptions().size();
            }
            holder.setText(R.id.tv_score, "(本题" + score + "分)");
        }
        holder.setText(R.id.tv_type, bean.getExamTemplateStyleName());
        iv_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.changeCollect(bean);
                }
            }
        });
        //答案部分
        holder.getView(R.id.rl_result).setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        if (examTextBean == null) {
            result.setHtmlText("");
            analysis.setHtmlText("");
            return;
        }
        result.setHtmlText(examTextBean.getExamAnswer());
        analysis.setHtmlText(examTextBean.getExamAnalysis());
    }

    public void changeState(boolean showErrorExam, boolean showAnswer) {
        this.showErrorExam = showErrorExam;
        this.showAnswer = showAnswer;
    }

    private static abstract class SelectorOnClick implements View.OnClickListener {
        private AnswerListBean.AnswerDataBean bean;
        private int examType;

        public SelectorOnClick(AnswerListBean.AnswerDataBean bean, int examType) {
            this.bean = bean;
            this.examType = examType;
        }

        @Override
        public void onClick(View v) {
            onClick(v, bean, examType);
        }

        public abstract void onClick(View view, AnswerListBean.AnswerDataBean bean, int examType);
    }

    /**
     * 设置选项 以后优化
     *
     * @param linearLayout 显示答案界面
     * @param list         答案list
     */
    private void setOptionContent(LinearLayout linearLayout, List<AnswerListBean.ExamOptions> list) {
        for (AnswerListBean.ExamOptions examOptions : list) {
            if (examOptions.getList() == null) {
                continue;
            }
            switch (examOptions.getList().size()) {
                case 3:
                    inflateOption(linearLayout, examOptions, 3, list.size());
                    break;
                case 4:
                    inflateOption(linearLayout, examOptions, 4, list.size());
                    break;
                default:
                    break;
            }
        }
    }

    private void inflateOption(LinearLayout linearLayout, AnswerListBean.ExamOptions examOptions, int itemCount, int size) {

        if (linearLayout.getChildCount() == size) {
            return;
        }
        LinearLayout layout = inflateView(itemCount);
        linearLayout.addView(layout);

        TextView tvNum = layout.findViewById(R.id.tv_number);

        TextView tvContentA = layout.findViewById(R.id.tv_content_A);
        TextView tvContentB = layout.findViewById(R.id.tv_content_B);
        TextView tvContentC = layout.findViewById(R.id.tv_content_C);
        TextView tvContentD = itemCount == 3 ? null : (TextView) layout.findViewById(R.id.tv_content_D);


        TextView tvNumA = layout.findViewById(R.id.tv_num_A);
        TextView tvNumB = layout.findViewById(R.id.tv_num_B);
        TextView tvNumC = layout.findViewById(R.id.tv_num_C);
        TextView tvNumD = itemCount == 3 ? null : (TextView) layout.findViewById(R.id.tv_num_D);

        List<AnswerListBean.ExamOptions> list = examOptions.getList();


        tvNum.setText("(" + examOptions.getSeq() + ")");

        tvContentA.setText(list.get(0).getContent());
        if (list.get(0).isRight()) {
            tvNumA.setText("A");
            tvNumA.setTextColor(Color.WHITE);
            tvNumA.setBackgroundResource(R.drawable.shap_oval_green);
        } else {
            tvNumA.setText("A.");
            tvNumA.setTextColor(Color.BLACK);
            tvNumA.setBackgroundResource(R.drawable.shap_oval_white);
        }

        tvContentB.setText(list.get(1).getContent());
        if (list.get(1).isRight()) {
            tvNumB.setText("B");
            tvNumB.setTextColor(Color.WHITE);
            tvNumB.setBackgroundResource(R.drawable.shap_oval_green);
        } else {
            tvNumB.setText("B.");
            tvNumB.setTextColor(Color.BLACK);
            tvNumB.setBackgroundResource(R.drawable.shap_oval_white);
        }

        tvContentC.setText(list.get(2).getContent());
        if (list.get(2).isRight()) {
            tvNumC.setText("C");
            tvNumC.setTextColor(Color.WHITE);
            tvNumC.setBackgroundResource(R.drawable.shap_oval_green);
        } else {
            tvNumC.setText("C.");
            tvNumC.setTextColor(Color.BLACK);
            tvNumC.setBackgroundResource(R.drawable.shap_oval_white);
        }

        if (itemCount == 3) {
            return;
        }

        tvContentD.setText(list.get(3).getContent());
        if (list.get(3).isRight()) {
            tvNumD.setText("D");
            tvNumD.setTextColor(Color.WHITE);
            tvNumD.setBackgroundResource(R.drawable.shap_oval_green);
        } else {
            tvNumD.setText("D.");
            tvNumD.setTextColor(Color.BLACK);
            tvNumD.setBackgroundResource(R.drawable.shap_oval_white);
        }

    }

    private LinearLayout inflateView(int count) {
        if (count == 3) {
            return (LinearLayout) LayoutInflater.from(context).inflate(R.layout.item_fill_result_child_select_3, null);
        } else {
            return (LinearLayout) LayoutInflater.from(context).inflate(R.layout.item_fill_result_child_select_4, null);
        }
    }

    //设置listview高度
    private void setListHeight(ListView lv) {
        ListAdapter la = lv.getAdapter();
        if (null == la) {
            return;
        }
        // calculate height of all items.
        int h = 0;
        final int cnt = la.getCount();
        for (int i = 0; i < cnt; i++) {
            View item = la.getView(i, null, lv);
            item.measure(0, 0);
            h += item.getMeasuredHeight();
        }
        // reset ListView height
        ViewGroup.LayoutParams lp = lv.getLayoutParams();
        lp.height = h + (lv.getDividerHeight() * (cnt - 1));
        lv.setLayoutParams(lp);
    }
}
