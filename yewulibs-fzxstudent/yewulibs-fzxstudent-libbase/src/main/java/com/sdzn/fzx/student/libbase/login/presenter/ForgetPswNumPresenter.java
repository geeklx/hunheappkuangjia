package com.sdzn.fzx.student.libbase.login.presenter;

import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.login.activity.ForgetPswActivity;
import com.sdzn.fzx.student.libbase.login.view.ForgetPswNumView;
import com.sdzn.fzx.student.vo.VerifyUserNameBean;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.sdzn.fzx.student.libutils.util.ToastUtil.showShortlToast;


/**
 * ForgetPswNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ForgetPswNumPresenter extends BasePresenter<ForgetPswNumView, ForgetPswActivity> {

    public void verityUserName(final String cardId) {
        if (vertifyNum(cardId)) {
        }
        Network.createService(NetWorkService.ForgetPswService.class)
                .verityUserName(cardId)
                .map(new StatusFunc<VerifyUserNameBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<VerifyUserNameBean>(new SubscriberListener<VerifyUserNameBean>() {
                    @Override
                    public void onNext(VerifyUserNameBean o) {
//                        mView.verifySuccess(o.getData().getTelephone());
//                        mView.verifySuccess(o.getData().getTelephone());
//                        mView.verifySuccess(o.getData().getTelephone());
//                        mView.verifySuccess(o.getData().getTelephone());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException)
                            if (((ApiException) e).getStatus().getCode() == 21100) {
                                showShortlToast("您输入的账号不存在，请重新输入");
                                return;
                            }

                        showShortlToast("验证失败");
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }


    private boolean vertifyNum(final String userName) {
        if (userName.length() == 8 || userName.length() == 18) {
            return true;
        } else {
            showShortlToast("只能输入8位学生号或18位身份证号");
            return false;
        }

    }

}
