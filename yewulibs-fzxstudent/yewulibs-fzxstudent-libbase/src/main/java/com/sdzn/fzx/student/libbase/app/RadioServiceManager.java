package com.sdzn.fzx.student.libbase.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.sdzn.fzx.student.libbase.event.RadioServiceEvent;
import com.sdzn.fzx.student.libbase.service.RadioService;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;

/**
 * @author Reisen at 2018-12-05
 */
public class RadioServiceManager {
    private static RadioServiceManager sManager;
    private Intent mIntent;
    private ServiceConnection mConnection;
    private RadioService mService;

    public RadioServiceManager() {
        mIntent = new Intent(App2.get(), RadioService.class);
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = ((RadioService.RadioServiceBinder) service).getService();
                EventBus.getDefault().post(new RadioServiceEvent(mService));
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                EventBus.getDefault().post(new RadioServiceEvent(null));
            }
        };
    }

    public static RadioServiceManager getInstance() {
        if (sManager == null) {
            synchronized (RadioServiceManager.class) {
                if (sManager == null) {
                    sManager = new RadioServiceManager();
                }
            }
        }
        return sManager;
    }

    public void startService(Activity activity) {
        activity.startService(mIntent);
    }

    public void stopService(Activity activity) {
        activity.stopService(mIntent);
    }

    public void bindService(Activity activity) {
        if (mService != null) {
            return;
        }
        try {
            activity.bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unbindService(Activity activity) {
        if (mService == null) {
            return;
        }
        try {
            activity.unbindService(mConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public RadioService getService() {
        return mService;
    }

    public void destroyManager(Activity activity) {
        synchronized (RadioServiceManager.class) {
            unbindService(activity);
            stopService(activity);
            mIntent = null;
            mConnection = null;
            sManager = null;
        }
    }
}
