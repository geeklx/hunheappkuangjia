package com.sdzn.fzx.student.libbase.utils;

import com.sdzn.fzx.student.api.network.Network;

import org.xutils.common.util.MD5;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/14
 * 修改单号：
 * 修改内容:
 */
public class QiNieUtils {
    private static String key = "3699249853abafbbf99b778ac0b9c3b90b8b49ba";

    private static String S = "";


    public static HashMap<String, String> getSign(String url) {
        // http://csfile.fuzhuxian.com/93b2109e-ea34-41c2-b9ba-71a3449a4b3f.jpg?imageMogr2/format/jpg/rotate/0
//teach/QiniuController/getQiniuConfig

        String host = "http://csfile.fuzhuxian.com";
        String split = url.split("\\?")[0];
        String substring = split.substring(host.length());
        HashMap<String, String> result = new HashMap<>();
        String T = Long.toHexString(new Date().getTime() / 1000);
        S = Network.QINIU_KEY + substring + T;
        String s = MD5.md5(S).toLowerCase();
        result.put("sign", s);
        result.put("T", T);
        return result;
    }
}
