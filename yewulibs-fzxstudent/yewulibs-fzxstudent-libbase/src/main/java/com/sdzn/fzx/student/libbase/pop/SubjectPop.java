package com.sdzn.fzx.student.libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libbase.listener.OnSubjectClickListener;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 学科弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class SubjectPop extends PopupWindow {
    private RecyclerView rvSubject;

    private Activity activity;

    private List<SubjectVo.DataBean> subjectVos = new ArrayList<>();

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter subjectAdapter;

    private OnSubjectClickListener onSubjectClickListener;

    public SubjectPop(Activity activity, OnSubjectClickListener onSubjectClickListener) {
        this.onSubjectClickListener = onSubjectClickListener;
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_subject, null);
        rvSubject=contentView.findViewById(R.id.rvSubject);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        setSubjects();
    }

    private void setSubjects() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvSubject.setLayoutManager(linearLayoutManager);
        subjectAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvSubject.setAdapter(subjectAdapter);
        rvSubject.addOnItemTouchListener(new OnItemTouchListener(rvSubject) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                if (position < 0 || position >= subjectVos.size()) {
                    return;
                }
                SubjectVo.DataBean dataBean = subjectVos.get(position);
                SubjectPop.this.onSubjectClickListener.onSubjectClick(dataBean);
                dismiss();
            }
        });
    }

    private void addItems() {
        itemEntityList.clear();
        itemEntityList.addItems(R.layout.item_main_subject, subjectVos)
                .addOnBind(R.layout.item_main_subject, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        SubjectVo.DataBean dataBean = (SubjectVo.DataBean) itemData;
                        holder.setText(R.id.tvSubject, dataBean.getName());
                    }
                });
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent, List<SubjectVo.DataBean> subjectVos) {
        if (subjectVos != null && subjectVos.size() > 0) {
            this.subjectVos.clear();
            this.subjectVos.addAll(subjectVos);
            addItems();
            subjectAdapter.notifyDataSetChanged();
        }
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }
}
