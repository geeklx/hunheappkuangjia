package com.sdzn.fzx.student.libbase.base;

import org.xutils.common.Callback;

/**
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-28
 */
public abstract class BaseProgressCallBack<T> implements Callback.ProgressCallback<T> {
    @Override
    public void onWaiting() {

    }

    @Override
    public void onStarted() {

    }

    @Override
    public void onLoading(long total, long current, boolean isDownloading) {

    }

    @Override
    public void onSuccess(T result) {

    }

    @Override
    public void onError(Throwable ex, boolean isOnCallback) {

    }

    @Override
    public void onCancelled(CancelledException cex) {

    }

    @Override
    public void onFinished() {

    }
}
