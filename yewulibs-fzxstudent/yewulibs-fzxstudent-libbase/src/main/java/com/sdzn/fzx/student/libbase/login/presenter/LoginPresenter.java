package com.sdzn.fzx.student.libbase.login.presenter;

import android.content.Intent;
import android.util.Base64;
import android.util.Log;

import com.blankj.utilcode.util.SPUtils;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.login.activity.ForgetPswActivity;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libbase.login.view.LoginView;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.CommonAppUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;
import com.tencent.bugly.crashreport.CrashReport;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * LoginPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class LoginPresenter extends BasePresenter<LoginView, LoginActivity> {

    public void login(final String name, String psw) {
        if (!vertifyNum(name, psw)) {
            if (CommonAppUtils.isLingChuangPad()) {
                CommonAppUtils.logout();
                App2.get().stopService(new Intent(App2.get(), MqttService.class));
                ActivityManager.exit();
            }
            return;
        }
        final String deviceId = AndroidUtil.getDeviceID(App2.get());
        if ("/auth".equals(BuildConfig2.AUTH)) {
        }

        //这里上传敏感数据会导致无法通过安全测试
        CrashReport.putUserData(App2.get(), "studentName", Base64.encodeToString(name.getBytes(), Base64.NO_WRAP));
        psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
        CrashReport.putUserData(App2.get(), "studentPwd", psw);
        Network.createService(NetWorkService.LoginService.class)
                .login(name, psw, 1, deviceId, "Android")
                .map(new StatusFunc<LoginBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<LoginBean>(new SubscriberListener<LoginBean>() {
                    @Override
                    public void onNext(LoginBean o) {
                        mView.loginSuccess(o);
                        CommonUtils.isReceiveRbMq = true;
//                        StudentSPUtils.savetoken(o.getData().getAccessToken());
                        SPUtils.getInstance().put(StudentSPUtils.SP_TOKEN,o.getData().getAccessToken());
                        Log.e("aaatest", "token" + o.getData().getAccessToken());
                        startMqttService();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
                            ApiException exception = (ApiException) e;
                            int code = exception.getStatus().getCode();
                            if (code == 21001 && CommonAppUtils.isLingChuangPad()) {
                                StudentSPUtils.setAutoLogin(false);
                                StudentSPUtils.saveLoginUserNum("");
                                StudentSPUtils.saveLastPenAddr("");
                                CommonAppUtils.logout();
                                App2.get().stopService(new Intent(App2.get(), MqttService.class));
                                ActivityManager.exit();
                            }
                        } else {
                            ToastUtil.showShortlToast("登录失败");
                        }
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity, true, false, true, null));
    }

    public void forgetPsw() {
        Intent intent = new Intent(mActivity, ForgetPswActivity.class);
        mActivity.startActivity(intent);
    }


    private boolean vertifyNum(final String userName, final String psw) {
        if (userName.length() == 8 || userName.length() == 18) {
            if (StringUtils.vertifyPsw(psw)) {
                return true;
            } else {
                ToastUtil.showShortlToast("账号或密码错误");
            }
        } else {
            ToastUtil.showShortlToast("账号或密码错误");
        }
        return false;
    }

    /**
     * 启动mqtt
     */
    public void startMqttService() {
        Intent intent = new Intent(mActivity, MqttService.class);
        mActivity.startService(intent);

    }
}
