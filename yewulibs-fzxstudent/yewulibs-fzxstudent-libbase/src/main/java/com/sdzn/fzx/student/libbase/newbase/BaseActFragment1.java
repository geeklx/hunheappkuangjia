package com.sdzn.fzx.student.libbase.newbase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.just.agentweb.LocalBroadcastManagers;
import com.just.agentweb.base.BaseCurrencyAgentWebFragment;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libutils.app.App2;

/**
 * 描述：加载webView的Fragment基类
 * 创建时间：2020/10/20
 */
public abstract class BaseActFragment1 extends BaseCurrencyAgentWebFragment {
    private long mCurrentMs = System.currentTimeMillis();
    private MessageReceiverIndex mMessageReceiver;
    public LinearLayout Nonetwok;//无网络

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("activityRefresh".equals(intent.getAction())) {
                    if (mAgentWeb != null && !mAgentWeb.getWebCreator().getWebView().getUrl().equals(BuildConfig2.SERVER_ISERVICE_NEW1 + "/setToken")) {
                        mAgentWeb.getUrlLoader().reload();
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        setup(rootView, savedInstanceState);
        return rootView;
    }

    protected abstract int getLayoutId();

    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("activityRefresh");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);
        Nonetwok = (LinearLayout) rootView.findViewById(R.id.ll_nonetwok);//无网络
    }


    public String ids;

    public void call(Object value) {
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}