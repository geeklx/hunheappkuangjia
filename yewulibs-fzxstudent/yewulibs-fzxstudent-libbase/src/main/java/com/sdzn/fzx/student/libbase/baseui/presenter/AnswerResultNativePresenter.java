package com.sdzn.fzx.student.libbase.baseui.presenter;

import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.baseui.view.AnswerResultNativeView;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.AnswerResultInfoBean;
import com.sdzn.fzx.student.vo.ResourceVo;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Reisen at 2018-12-11
 */
public class AnswerResultNativePresenter extends BasePresenter<AnswerResultNativeView, BaseActivity> {
    private Subscription subscribe1;
    private Subscription subscribe2;

    public void loadExamInfo(Map<String, String> params) {
        if (subscribe2 != null && subscribe2.isUnsubscribed()) {
            subscribe2.unsubscribe();
            subscriptions.remove(subscribe2);
        }
        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .loadExamInfo(params)
                .map(new StatusFunc<AnswerResultInfoBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<AnswerResultInfoBean>(new SubscriberListener<AnswerResultInfoBean>() {
                    @Override
                    public void onNext(AnswerResultInfoBean bean) {
                        if (bean.getData() == null) {
                            onError(new NullPointerException("bean.data is null"));
                            return;
                        }
                        initData(bean.getData());
                        mView.loadExamInfoSuccess(bean.getData());
                    }

                    private void initData(AnswerResultInfoBean.AnswerResultDataBean beans) {
                        List<AnswerListBean.AnswerDataBean> list = beans.getExamAnswerList();
                        if (list == null) {
                            return;
                        }
                        for (AnswerListBean.AnswerDataBean bean : list) {
                            if (bean.getType() == 3) {//纯文本
                                continue;
                            }
                            if (bean.getType() == 2) {//资源
                                String resourceText = bean.getResourceText();
                                ResourceVo.ResourceTextVo resourceBean = GsonUtil.fromJson(resourceText, ResourceVo.ResourceTextVo.class);
                                if (resourceBean == null) {
                                    throw new RuntimeException("加载数据失败, 没有资源信息");
                                }
                                bean.setResourceVoBean(resourceBean);
                                continue;
                            }
                            //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = GsonUtil.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            if (examTextBean == null) {
                                throw new RuntimeException("加载数据失败, 没有试题信息");
                            }
                            bean.setExamBean(examTextBean);
                            //综合题额外进行一轮解析
                            if (bean.getExamTemplateId() != 16) {
                                continue;
                            }
                            for (AnswerListBean.AnswerDataBean dataBean : bean.getExamList()) {
                                AnswerListBean.ExamTextBean json = GsonUtil.fromJson(dataBean.getExamText(), AnswerListBean.ExamTextBean.class);
                                if (json == null) {
                                    throw new RuntimeException("加载数据失败, 没有试题信息");
                                }
                                dataBean.setExamBean(json);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.loadExamInfoFailed(status.getMsg());
                            } else {
                                mView.loadExamInfoFailed("请求失败");
                            }
                        } else {
                            mView.loadExamInfoFailed("请求失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, false, ""));

    }

    public void saveCollect(final AnswerListBean.AnswerDataBean bean) {
        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .saveCollect(bean.getId())
                .map(new StatusFunc<Map<String, Long>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Map<String, Long>>(
                        new SubscriberListener<Map<String, Long>>() {
                            @Override
                            public void onNext(Map<String, Long> stringLongMap) {
                                bean.setCollect(true);
                                bean.setStudentBookId(stringLongMap.get("data"));
                                mView.saveCollectSuccess();
                            }


                            @Override
                            public void onError(Throwable e) {
                                if (mView == null) {
                                    return;
                                }
                                if (e instanceof ApiException) {
                                    ApiException apiException = (ApiException) e;
                                    StatusVo status = apiException.getStatus();
                                    if (status != null && status.getMsg() != null) {
                                        mView.collectError(status.getMsg());
                                    } else {
                                        mView.collectError("收藏失败");
                                    }
                                } else {
                                    mView.collectError("收藏失败");
                                }
                            }

                            @Override
                            public void onCompleted() {
                            }
                        }, mActivity));
    }

    public void deleteCollect(final AnswerListBean.AnswerDataBean bean) {
        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .deleteCollect(bean.getStudentBookId())
                .map(new StatusFunc<>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        bean.setCollect(false);
                        mView.deleteCollectSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.collectError(status.getMsg());
                            } else {
                                mView.collectError("取消收藏失败");
                            }
                        } else {
                            mView.collectError("取消收藏失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void saveResCollect(final AnswerListBean.AnswerDataBean bean, long lessonId, String lessonName) {
        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .saveResCollect(bean.getId(),lessonId,lessonName)
                .map(new StatusFunc<Map<String, Long>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Map<String, Long>>(
                        new SubscriberListener<Map<String, Long>>() {
                            @Override
                            public void onNext(Map<String, Long> stringLongMap) {
                                bean.setCollect(true);
                                bean.setStudentBookId(stringLongMap.get("data"));
                                mView.saveCollectSuccess();
                            }


                            @Override
                            public void onError(Throwable e) {
                                if (mView == null) {
                                    return;
                                }
                                if (e instanceof ApiException) {
                                    ApiException apiException = (ApiException) e;
                                    StatusVo status = apiException.getStatus();
                                    if (status != null && status.getMsg() != null) {
                                        mView.collectError(status.getMsg());
                                    } else {
                                        mView.collectError("收藏失败");
                                    }
                                } else {
                                    mView.collectError("收藏失败");
                                }
                            }

                            @Override
                            public void onCompleted() {
                            }
                        }, mActivity));
    }

    public void deleteResCollect(final AnswerListBean.AnswerDataBean bean) {
        Network.createTokenService(NetWorkService.AnswerNativeService.class)
                .deleteResCollect(bean.getStudentBookId())
                .map(new StatusFunc<>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        bean.setCollect(false);
                        mView.deleteCollectSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.collectError(status.getMsg());
                            } else {
                                mView.collectError("取消收藏失败");
                            }
                        } else {
                            mView.collectError("取消收藏失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }
}
