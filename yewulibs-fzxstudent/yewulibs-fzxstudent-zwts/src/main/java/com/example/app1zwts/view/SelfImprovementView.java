package com.example.app1zwts.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.BookVersionBean;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

/**
 * 自我提升
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/6.
 */
public interface SelfImprovementView extends BaseView {
    void networkError(String msg);

    void getSubjectSuccess(SubjectVo subjectVo);

    void queryBookVersionSuccess(List<BookVersionBean> list);
}
