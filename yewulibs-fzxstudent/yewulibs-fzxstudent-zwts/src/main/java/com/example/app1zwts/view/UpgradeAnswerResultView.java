package com.example.app1zwts.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.UpgradeAnswerResultBean;

import java.util.List;

/**
 * 提升练习结果
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public interface UpgradeAnswerResultView extends BaseView {

    void loadDataSuccess(List<UpgradeAnswerResultBean> list);

    void loadDataFailed(String msg);
}
