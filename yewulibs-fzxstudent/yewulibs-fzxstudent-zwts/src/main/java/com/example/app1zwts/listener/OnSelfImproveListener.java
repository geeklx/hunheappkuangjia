package com.example.app1zwts.listener;


import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/6.
 */
public interface OnSelfImproveListener {
    /**
     * 点击上方课程回调到下方fragment中
     */
    void onSubjectItemClick(int position);

    /**
     * 获取到课程信息回调  默认第几页
     */
    void onSubjectGet(List<SubjectVo.DataBean> subjects, int postion);

    /**
     * 下方viewpager翻页监听
     */
    void setOnPageChangeListener(OnPageChangeListener onPageChangeListener);
}
