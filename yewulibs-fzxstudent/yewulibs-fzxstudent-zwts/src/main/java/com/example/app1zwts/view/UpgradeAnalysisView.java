package com.example.app1zwts.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.AnalysisBean;

/**
 * 提升练习结果
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public interface UpgradeAnalysisView extends BaseView {

    void loadDataSuccess(AnalysisBean bean);

    void loadDataFailed(String msg);
}
