package com.example.app1zwts.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.example.app1zwts.R;
import com.sdzn.fzx.student.libbase.listener.OnExamRefreshListener;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.vo.UpgradeAnswerListBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class UpgradeAnswerPagerAdapter extends PagerAdapter {
    private List<UpgradeAnswerListBean.AnswerDataBean> mList;

    private LinkedList<View> mViewCache = new LinkedList<>();
    private LayoutInflater inflater;
    private OnExamRefreshListener mRefreshListener;

    public UpgradeAnswerPagerAdapter(Context context, List<UpgradeAnswerListBean.AnswerDataBean> list, OnExamRefreshListener listener) {
        mList = list;
        inflater = LayoutInflater.from(context);
        mRefreshListener = listener;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewHolder holder;
        View convertView;
        if (mViewCache.size() == 0) {
            convertView = this.inflater.inflate(R.layout.item_answer_pager, container, false);
            holder = new ViewHolder();
            holder.tvCount = convertView.findViewById(R.id.tv_count);
            holder.tvScore = convertView.findViewById(R.id.tv_score);
            holder.tvType = convertView.findViewById(R.id.tv_type);
            holder.tvExam = convertView.findViewById(R.id.tv_exam);
            holder.mRadioGroup = convertView.findViewById(R.id.rg_answer);
            convertView.setTag(holder);
        } else {
            convertView = mViewCache.removeFirst();
            holder = (ViewHolder) convertView.getTag();
        }
        UpgradeAnswerListBean.AnswerDataBean bean = mList.get(position);
        holder.tvCount.setText(bean.getExamSeq() + ". ");
        int score = (int) bean.getScore();
        if (score <= 0) {
            holder.tvScore.setVisibility(View.GONE);
//        } else {
//            holder.tvScore.setVisibility(View.VISIBLE);
//            if (bean.getExamTemplateId() == 14 || bean.getExamTemplateId() == 6) {
//                score *= bean.getExamBean().getExamOptions().size();
//            }
//            holder.tvScore.setText("(本题" + score + "分)");
        }
        holder.tvType.setText(bean.getExamTemplateStyleName());
        //=====================  =====================
        holder.tvExam.setHtmlText(bean.getExamStem());

        List<UpgradeAnswerListBean.ExamOptions> options = bean.getExamOptions();
        if (options == null || options.isEmpty()) {
            holder.mRadioGroup.removeAllViews();
            container.addView(convertView);
            return convertView;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = holder.mRadioGroup.getChildCount();
        if (childCount > size) {//控件多, 选项少
            holder.mRadioGroup.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                inflater.inflate(R.layout.item_child_select, holder.mRadioGroup, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = holder.mRadioGroup.getChildAt(i);//选项A,B,C,D，对，错
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            List<UpgradeAnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (bean.getExamTypeId() == 1) {//单选/判断getExamTemplateId
                Collections.sort(examList);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (UpgradeAnswerListBean.ExamOptionBean optionBean : examList) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getMyAnswer())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
            child.setOnClickListener(new SelectorOnClick(bean, bean.getExamTypeId()) {// bean.getExamTemplateId()
                @Override
                public void onClick(View view, UpgradeAnswerListBean.AnswerDataBean bean, int examType) {
                    radioGroupClick(view, bean, examType);
                    if (mRefreshListener != null) {
                        //mRefreshListener.requestRefresh();
                        mRefreshListener.requestRefreshExam(examType);
                    }
                }
            });
        }
        container.addView(convertView);
        return convertView;
    }

    /**
     * 选择选项点击事件处理
     */
    private void radioGroupClick(View view, UpgradeAnswerListBean.AnswerDataBean bean, int examType) {
        String trim = ((TextView) ((ViewGroup) view).getChildAt(0)).getText().toString().trim();
        List<UpgradeAnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list == null) {
            list = new ArrayList<>();
            bean.setExamOptionList(list);
        }
        if (examType == 1 || examType == 3) {//单选
            if (list.isEmpty()) {
                UpgradeAnswerListBean.ExamOptionBean optionBean = new UpgradeAnswerListBean.ExamOptionBean();
                optionBean.setMyAnswer(trim);
                optionBean.setSeq(trim.charAt(0) - 64);
                list.add(optionBean);
                bean.setIsAnswer(1);
            } else if (TextUtils.equals(list.get(0).getMyAnswer(), trim)) {
                list.remove(0);
                bean.setIsAnswer(0);
            } else {
                list.get(0).setMyAnswer(trim);
                list.get(0).setSeq(trim.charAt(0) - 64);
                bean.setIsAnswer(1);
            }
            return;
        }
        Iterator<UpgradeAnswerListBean.ExamOptionBean> iterator = list.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            UpgradeAnswerListBean.ExamOptionBean next = iterator.next();
            if (TextUtils.equals(trim, next.getMyAnswer())) {
                iterator.remove();
                b = true;
                break;
            }
        }
        if (!b) {
            UpgradeAnswerListBean.ExamOptionBean optionBean = new UpgradeAnswerListBean.ExamOptionBean();
            optionBean.setMyAnswer(trim);
            optionBean.setSeq(trim.charAt(0) - 64);
            list.add(optionBean);
        }
        bean.setIsAnswer(list.isEmpty() ? 0 : 1);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View contentView = (View) object;
        container.removeView(contentView);
        mViewCache.add(contentView);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public static class ViewHolder {
        TextView tvCount;
        TextView tvScore;
        TextView tvType;
        HtmlTextView tvExam;
        RadioGroup mRadioGroup;
    }

    public static abstract class SelectorOnClick implements View.OnClickListener {
        private UpgradeAnswerListBean.AnswerDataBean bean;
        private int examType;

        SelectorOnClick(UpgradeAnswerListBean.AnswerDataBean bean, int examType) {
            this.bean = bean;
            this.examType = examType;
        }

        @Override
        public void onClick(View v) {
            onClick(v, bean, examType);
        }

        public abstract void onClick(View view, UpgradeAnswerListBean.AnswerDataBean bean, int examType);
    }
}
