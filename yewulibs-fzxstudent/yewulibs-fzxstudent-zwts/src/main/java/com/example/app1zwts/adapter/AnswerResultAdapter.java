package com.example.app1zwts.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.app1zwts.fragment.AnswerResultFragment;
import com.sdzn.fzx.student.libbase.base.BaseViewpagerFragment;
import com.sdzn.fzx.student.vo.UpgradeAnswerResultBean;

import java.util.ArrayList;
import java.util.List;

public class AnswerResultAdapter extends FragmentStatePagerAdapter {
    private List<AnswerResultFragment> fragmentList = new ArrayList<>();

    public AnswerResultAdapter(FragmentManager fm) {
        super(fm);
    }

    public void init(List<UpgradeAnswerResultBean> list) {
        fragmentList.clear();
        for (UpgradeAnswerResultBean info : list) {
            //添加题目
            fragmentList.add(AnswerResultFragment.newInstance(info));
        }
    }

    public void refreshAllFragment(List<UpgradeAnswerResultBean> list, boolean isCheck) {
        for (UpgradeAnswerResultBean info : list) {
            for (AnswerResultFragment fragment : fragmentList) {
                //最好使用唯一标示来判定是否刷了正确的Fragment 比如id
                String pageTitle = fragment.getTitle();
                if (pageTitle != null && pageTitle.equals(info.getExamSeq()+"")) {
                    fragment.refreshData(info,isCheck);
                }
            }
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragmentList != null && position < fragmentList.size()) {
            return fragmentList.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (getItem(position) instanceof BaseViewpagerFragment) {
            return ((BaseViewpagerFragment) getItem(position)).getTitle();
        }
        return super.getPageTitle(position);
    }
}