package com.example.app1zwts.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.app1zwts.R;
import com.sdzn.fzx.student.vo.BookVersionBean;

import java.util.List;


/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/7.
 */
public class SetTextBookAdapter extends BaseAdapter {
    public static final int JIAO_CAI = 1;
    public static final int NIAN_JI = 2;

    private List<BookVersionBean> mList;
    private Context mContext;
    private int selectedColor;
    private int normalColor;

    private int state = JIAO_CAI;

    public int getState() {
        return state;
    }

    public boolean setState(int state) {
        if (state == NIAN_JI) {
            for (BookVersionBean bean : mList) {
                if (bean.getBookVersion().isSelected()) {
                    this.state = state;
                    return true;
                }
            }
            return false;
        }
        this.state = state;
        for (BookVersionBean bean : mList) {
            bean.getBookVersion().setSelected(false);
            for (BookVersionBean.Course course : bean.getCourseMapping()) {
                course.setSelected(false);
            }
        }
        return true;
    }

    public SetTextBookAdapter(List<BookVersionBean> list, Context context) {
        this.mList = list;
        this.mContext = context;
        selectedColor = mContext.getResources().getColor(R.color.blue);
        normalColor = Color.BLACK;
    }

    public void selectBook(int position) {
        for (int i = 0; i < mList.size(); i++) {
            BookVersionBean bean = mList.get(i);
            bean.getBookVersion().setSelected(i == position);
            for (BookVersionBean.Course course : bean.getCourseMapping()) {
                course.setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    public void selectCourse(int position) {
        for (BookVersionBean bean : mList) {
            if (!bean.getBookVersion().isSelected()) {
                continue;
            }
            for (int i = 0; i < bean.getCourseMapping().size(); i++) {
                bean.getCourseMapping().get(i).setSelected(i == position);
            }
        }
        notifyDataSetChanged();
    }

    public void setList(List<BookVersionBean> list) {
        if (list == null) {
            return;
        }
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (state == JIAO_CAI) {
            return mList.size();
        } else {
            for (BookVersionBean bean : mList) {
                if (bean.getBookVersion().isSelected()) {
                    return bean.getCourseMapping().size();
                }
            }
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (state == JIAO_CAI) {
            return mList.get(position).getBookVersion();
        } else {
            for (BookVersionBean bean : mList) {
                if (bean.getBookVersion().isSelected()) {
                    return bean.getCourseMapping().get(position);
                }
            }
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_set_textbook, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (state == JIAO_CAI) {
            holder.tv.setText(mList.get(position).getBookVersion().getVersion_name());
            holder.tv.setTextColor(mList.get(position).getBookVersion().isSelected() ? selectedColor : normalColor);
        } else {
            for (BookVersionBean bean : mList) {
                if (!bean.getBookVersion().isSelected()) {
                    continue;
                }
                holder.tv.setText(bean.getCourseMapping().get(position).getName());
                holder.tv.setTextColor(bean.getCourseMapping().get(position).isSelected() ? selectedColor : normalColor);

            }
        }
        return convertView;
    }

    static class ViewHolder {
        /*  @BindView(R.id.tv)
          TextView tv;*/
        private TextView tv;


        ViewHolder(View view) {
            tv = (TextView) view.findViewById(R.id.tv);
        }
    }
}
