package com.example.app1zwts.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.ChapterSectionBean;
import com.sdzn.fzx.student.vo.KnowledgePointBean;

import java.util.List;

/**
 * 任务内容列表PresenterView
 *
 * @author wangchunxiao
 */
public interface UpgradeListView extends BaseView {
    void getDataSuccess(List<KnowledgePointBean> list);//知识点练习

    void networkError(String msg);

    void noSubject();

    void getChapterSectionSuccess(List<ChapterSectionBean> list);//同步练习
}
