package com.example.app1zwts.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1zwts.ContextManager;
import com.example.app1zwts.R;
import com.example.app1zwts.adapter.AnswerResultAdapter;
import com.example.app1zwts.adapter.UpgradeAnswerTopBarAdapter;
import com.example.app1zwts.presenter.UpgradeAnswerResultPresenter;
import com.example.app1zwts.view.UpgradeAnswerResultView;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.UpgradeAnswerResultBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * 提升练习结果页
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class UpgradeAnswerResultActivity extends MBaseActivity<UpgradeAnswerResultPresenter>
        implements UpgradeAnswerResultView, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    public static final String ID = "id";
    public static final String TIME = "time";
    public static final String RIGHT_COUNT = "rightCount";
    public static final String EXAM_COUNT = "examCount";
    public static final String EXAMRESULT_ID = "examResultId";
    public static final String SUBJECT_ID = "subjectId";

    private TextView mTvBack;
    private TextView mTvStatistics;
    private TextView mTvScore;
    private TextView mTvTotalScore;
    private TextView mTvTime;
    private CheckBox mCbShowAnswer;
    private TextView mTvShowAnswer;
    private RecyclerView rvNumber;
    private ViewPager mViewPager;


    private List<UpgradeAnswerResultBean> mList = new ArrayList<>();
    private UpgradeAnswerTopBarAdapter mTopAdapter;
    //    private AnswerResultPagerAdapter mPagerAdapter;
    private long id;
    private String examResultId;
    AnswerResultAdapter resultAdapter;
    private int subjectId;

    @Override
    public void initPresenter() {
        mPresenter = new UpgradeAnswerResultPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_answer_native);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);final boolean isGuidance = (boolean) StudentSPUtils.get(UpgradeAnswerResultActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        mTvBack = (TextView) findViewById(R.id.tvBack);
        mTvStatistics = (TextView) findViewById(R.id.tvStatistics);
        mTvScore = (TextView) findViewById(R.id.tv_score);
        mTvTotalScore = (TextView) findViewById(R.id.tv_total_score);
        mTvTime = (TextView) findViewById(R.id.tv_time);
        mCbShowAnswer = (CheckBox) findViewById(R.id.iv_show_answer);
        mTvShowAnswer = (TextView) findViewById(R.id.tv_show_answer);
        rvNumber = (RecyclerView) findViewById(R.id.rv_number);
        mViewPager = (ViewPager) findViewById(R.id.vp);
        mTvBack.setOnClickListener(this);
        mTvStatistics.setOnClickListener(this);
        mTvShowAnswer.setOnClickListener(this);
        mCbShowAnswer.setOnCheckedChangeListener(this);

        mTopAdapter = new UpgradeAnswerTopBarAdapter(App2.get());
        rvNumber.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvNumber.setAdapter(mTopAdapter);
        mTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mViewPager.setCurrentItem(position, false);
                if (position < mList.size()) {
                    mTopAdapter.changeBg(position);
                }
            }
        });
//        mPagerAdapter = new AnswerResultPagerAdapter(this, mList);
//        mViewPager.setAdapter(mPagerAdapter);

        resultAdapter = new AnswerResultAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(resultAdapter);
        //若设置了该属性 则viewpager会缓存指定数量的Fragment
        mViewPager.setOffscreenPageLimit(15);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTopAdapter.changeBg(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    protected void initData() {
        id = getIntent().getLongExtra(ID, 0);
        long time = getIntent().getLongExtra(TIME, 0);
        int rightCount = getIntent().getIntExtra(RIGHT_COUNT, 0);
        int totalCount = getIntent().getIntExtra(EXAM_COUNT, 0);
        subjectId = getIntent().getIntExtra(SUBJECT_ID, 0);
        examResultId = getIntent().getStringExtra(EXAMRESULT_ID);
        mTvTime.setText(DateUtil.millsecondsToStr(time));
        mTvScore.setText(rightCount + "");
        mTvTotalScore.setText("/" + totalCount);
        mPresenter.loadData(id);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tvBack) {
            finish();
        } else if (i == R.id.tvStatistics) {
            SlbLoginUtil2.get().loginTowhere(this, new Runnable() {
                @Override
                public void run() {
                    ContextManager.startUpgradeAnalysis(activity, examResultId, subjectId);
                }
            });

        } else if (i == R.id.tv_show_answer) {
            mCbShowAnswer.setChecked(mCbShowAnswer.isChecked());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        mPagerAdapter.setShowAnswer(isChecked);
//        mPagerAdapter.notifyDataSetChanged();
        resultAdapter.refreshAllFragment(mList, isChecked);
    }


    @Override
    public void loadDataSuccess(List<UpgradeAnswerResultBean> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        Collections.sort(list);
        mList.clear();
        mList.addAll(list);
        for (int i = 0; i < mList.size(); i++) {//软云题没有题号
            UpgradeAnswerResultBean bean = mList.get(i);
            bean.setExamSeq(i + 1);
            bean.setExamTypeId(1);
        }
        mTopAdapter.clear();
        mTopAdapter.add(mList);
        mTopAdapter.notifyDataSetChanged();
        resultAdapter.init(mList);
        resultAdapter.notifyDataSetChanged();

        if (mList.size() > 0) {//默认
            mTopAdapter.changeBg(0);
        }
    }

    @Override
    public void loadDataFailed(String msg) {
        ToastUtil.showShortlToast(msg);
    }
}
