package com.example.app1zwts.presenter;

import com.example.app1zwts.view.UpgradeListView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.ArrayBean;
import com.sdzn.fzx.student.vo.ChapterSectionBean;
import com.sdzn.fzx.student.vo.KnowledgePointBean;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 任务内容列表
 *
 * @author wangchunxiao
 */
public class UpgradeListPresenter extends BasePresenter<UpgradeListView, BaseActivity> {

    /**
     * 知识点练习
     */
    public void queryKnowledgePointScoreList(int subjectId) {
        Network.createTokenService(NetWorkService.RuanYun.class)
                .queryKnowledgePointScoreList(subjectId)
                .map(new StatusFunc<ArrayBean<KnowledgePointBean>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ArrayBean<KnowledgePointBean>>(new SubscriberListener<ArrayBean<KnowledgePointBean>>() {
                    @Override
                    public void onNext(ArrayBean<KnowledgePointBean> bean) {
                        mView.getDataSuccess(bean.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("暂无数据");//数据获取失败
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    /**
     * 同步练习
     */
    public void queryChapterSection(int subjectId) {
        long courseId = StudentSPUtils.getStudentCourse(subjectId);
        if (courseId == -1) {
            mView.noSubject();
            return;
        }

        Network.createTokenService(NetWorkService.RuanYun.class)
                .queryChapterSection(courseId)
                .map(new StatusFunc<ArrayBean<ChapterSectionBean>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ArrayBean<ChapterSectionBean>>(new SubscriberListener<ArrayBean<ChapterSectionBean>>() {
                    @Override
                    public void onNext(ArrayBean<ChapterSectionBean> bean) {
                        mView.getChapterSectionSuccess(bean.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("暂无数据");//数据获取失败
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }
}
