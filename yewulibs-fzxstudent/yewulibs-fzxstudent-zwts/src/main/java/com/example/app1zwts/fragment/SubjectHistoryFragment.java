package com.example.app1zwts.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1zwts.ContextManager;
import com.example.app1zwts.R;
import com.example.app1zwts.presenter.SubjectHistoryPresenter;
import com.example.app1zwts.view.SubjectHistoryView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.SubjectVo;
import com.sdzn.fzx.student.vo.TeachIntelTask;

import java.util.List;


/**
 * 课程练习记录
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class SubjectHistoryFragment extends MBaseFragment<SubjectHistoryPresenter> implements SubjectHistoryView, OnRefreshListener, OnLoadMoreListener {
    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView rvTask;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private SubjectVo.DataBean bean;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter taskAdapter;
    private int currPage = 0;
    private static final int pageSize = 12;

    public static SubjectHistoryFragment newInstance(SubjectVo.DataBean bean) {
        SubjectHistoryFragment fragment = new SubjectHistoryFragment();
        fragment.bean = bean;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_list, container, false);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        rvTask = (EmptyRecyclerView) rootView.findViewById(R.id.swipe_target);
        llTaskEmpty = (LinearLayout) rootView.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) rootView.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) rootView.findViewById(R.id.tvTaskEmpty);

        initView();
        initData();
        return rootView;
    }

    private void initData() {
        loadData1();
    }

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));

        rvTask.setEmptyView(llTaskEmpty);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
        rvTask.setLayoutManager(gridLayoutManager);
        taskAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvTask.setAdapter(taskAdapter);
        rvTask.addOnItemTouchListener(new OnItemTouchListener(rvTask) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
                final TeachIntelTask task = (TeachIntelTask) itemEntityList.getItemData(vh.getAdapterPosition());
                final long time = task.getCommitTime() - task.getCreateTime();
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        ContextManager.startUpgradeAnswerResult(activity, task.getId(), time, task.getRightCount(),
                                task.getExamCount(), task.getExamResultId(), bean.getId());
                    }
                });
            }
        });
    }

    @Override
    public void initPresenter() {
        mPresenter = new SubjectHistoryPresenter();
        mPresenter.attachView(this, activity);
    }


    @Override
    public void getHistoryListSuccess(List<TeachIntelTask> list) {
        // 第一页，需要先清空
        if (currPage == 0) {
            itemEntityList.clear();
        }
        if (!list.isEmpty()) {
            for (Object bean : list) {
                itemEntityList.addItem(R.layout.item_fragment_history_list, bean)
                        .addOnBind(R.layout.item_fragment_history_list, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                mPresenter.bindTaskListHolder(holder, (TeachIntelTask) itemData);
                            }
                        });
            }
            taskAdapter.notifyDataSetChanged();
            if (list.size() < pageSize) {
                refreshLayout.setEnableLoadMore(false);
            } else {
                refreshLayout.setEnableLoadMore(true);
            }
        } else {
            itemEntityList.clear();
            taskAdapter.notifyDataSetChanged();
            ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.no_subject_history));
            rvTask.setEmptyView(llTaskEmpty);
            refreshLayout.setEnableLoadMore(false);
        }
        cancelLoadState();
    }

    @Override
    public void cancelLoad() {
        cancelLoadState();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
        ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.no_data));
        tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text18));
        rvTask.setEmptyView(llTaskEmpty);
        cancelLoadState();
    }

    /**
     * 取消加载状态
     */
    private void cancelLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        } else if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadMore();
        }
    }

    private void loadData1() {
        if (bean != null) {

            mPresenter.getHistoryList(bean.getId(), currPage * pageSize + 1);
        }
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 0;
        loadData1();
    }

    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        loadData1();
    }
}
