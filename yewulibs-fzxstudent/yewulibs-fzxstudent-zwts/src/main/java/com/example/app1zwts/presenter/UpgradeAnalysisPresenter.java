package com.example.app1zwts.presenter;

import com.example.app1zwts.view.UpgradeAnalysisView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libutils.app.Config;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.AnalysisBean;
import com.sdzn.fzx.student.vo.StringBean;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 提升练习-统计分析
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/11.
 */
public class UpgradeAnalysisPresenter extends BasePresenter<UpgradeAnalysisView,BaseActivity> {

    public void loadData(String id) {
        Network.createTokenService(NetWorkService.RuanYun.class)
                .showStudentPromoteAnalysis(Config.ruanYunDebug ? "hR+Lvgw2SRk=" : id)
                .map(new StatusFunc<StringBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StringBean>(new SubscriberListener<StringBean>() {
                    @Override
                    public void onNext(StringBean stringBean) {
                        AnalysisBean bean = GsonUtil.fromJson(stringBean.getData(), AnalysisBean.class);
                        mView.loadDataSuccess(bean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.loadDataFailed(status.getMsg());
                            } else {
                                mView.loadDataFailed("数据获取失败");
                            }
                        } else {
                            mView.loadDataFailed("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, null));
    }
}
