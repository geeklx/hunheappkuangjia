package com.example.app1zwts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.app1zwts.activity.PracticeHistoryActivity;
import com.example.app1zwts.activity.UpgradeAnalysisActivity;
import com.example.app1zwts.activity.UpgradeAnswerActivity;
import com.example.app1zwts.activity.UpgradeAnswerResultActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.StatisticsActivity;
import com.sdzn.fzx.student.vo.TaskListVo;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public final class ContextManager {
    private ContextManager() {
    }

    /**
     * 练习记录界面
     */
    public static void startPracticeHistory(Context context) {

        context.startActivity(new Intent(context, PracticeHistoryActivity.class));
    }

    /**
     * 提升练习界面
     *
     * @param subjectId        课程id 必传
     * @param knowledgePointId 知识点id 同步练习时不传
     * @param chapterSectionId 章节id 知识点练习时不传
     * @param status           0=知识点练习, 1=同步练习
     */
    public static void startUpgradeAnswer(Context context, int subjectId, long knowledgePointId, long chapterSectionId, int status) {
        Intent intent = new Intent(context, UpgradeAnswerActivity.class);
        intent.putExtra(UpgradeAnswerActivity.SUBJECTID, subjectId);
        intent.putExtra(UpgradeAnswerActivity.KNOWLEDGEPOINTID, knowledgePointId);
        intent.putExtra(UpgradeAnswerActivity.CHAPTERSECTIONID, chapterSectionId);
        intent.putExtra(UpgradeAnswerActivity.STATUS, status);
        context.startActivity(intent);
    }

    /**
     * 提升练习结果页
     */
    public static void startUpgradeAnswerResult(Context context, long id, long time, int rightCount, int totalCount, String examResultId, int subjectId) {
        Intent intent = new Intent(context, UpgradeAnswerResultActivity.class);
        intent.putExtra(UpgradeAnswerResultActivity.ID, id);
        intent.putExtra(UpgradeAnswerResultActivity.TIME, time);
        intent.putExtra(UpgradeAnswerResultActivity.RIGHT_COUNT, rightCount);
        intent.putExtra(UpgradeAnswerResultActivity.EXAM_COUNT, totalCount);
        intent.putExtra(UpgradeAnswerResultActivity.EXAMRESULT_ID, examResultId);
        intent.putExtra(UpgradeAnswerResultActivity.SUBJECT_ID, subjectId);
        context.startActivity(intent);
    }

    /**
     * 提升练习-统计分析
     */
    public static void startUpgradeAnalysis(Context context, String examResultId, int subjectId) {
        Intent intent = new Intent(context, UpgradeAnalysisActivity.class);
        intent.putExtra(UpgradeAnalysisActivity.ID, examResultId);
        intent.putExtra(UpgradeAnalysisActivity.SUBJECT_ID, subjectId);
        context.startActivity(intent);
    }

    /**
     * 试题统计分析
     */
    public static void startStistics(Context context, TaskListVo.DataBean taskDataBean) {
        Intent statisticsIntent = new Intent(context, StatisticsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("taskDataBean", taskDataBean);
        statisticsIntent.putExtras(bundle);
        context.startActivity(statisticsIntent);
    }
}
