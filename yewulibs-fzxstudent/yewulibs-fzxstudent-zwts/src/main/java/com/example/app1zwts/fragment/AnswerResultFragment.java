package com.example.app1zwts.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.app1zwts.R;
import com.sdzn.fzx.student.libbase.base.BaseViewpagerFragment;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.vo.UpgradeAnswerResultBean;

import java.util.Collections;
import java.util.List;

public class AnswerResultFragment extends BaseViewpagerFragment {
    private static final String ARG_INFO_ENTITY = "arg_info_entity";
    private static final int DELAY_TIME = 1000;
    UpgradeAnswerResultBean bean;
    TextView tvCount;
    TextView tvScore;
    TextView tvType;
    HtmlTextView tvExam;
    RadioGroup mRadioGroup;

    LinearLayout llResult;
    HtmlTextView tvResult;
    HtmlTextView tvAnalysis;

    private boolean showAnswer = true;
    private String s1;

    public static AnswerResultFragment newInstance(UpgradeAnswerResultBean infoEntity) {
        AnswerResultFragment fragment = new AnswerResultFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_INFO_ENTITY, infoEntity);
        fragment.setArguments(args);
        if (infoEntity != null) {
            fragment.setTitle(infoEntity.getExamSeq() + "");
        }
        return fragment;
    }

    @Override
    public void initVariables(Bundle bundle) {
        bean = bundle.getParcelable(ARG_INFO_ENTITY);
    }

    @Override
    protected View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.item_answer_result_pager, container, false);
        tvCount = convertView.findViewById(R.id.tv_count);
        tvScore = convertView.findViewById(R.id.tv_score);
        tvType = convertView.findViewById(R.id.tv_type);
        tvExam = convertView.findViewById(R.id.tv_exam);
        //题目View
        mRadioGroup = convertView.findViewById(R.id.rg_answer);

        llResult = convertView.findViewById(R.id.rl_result);
        tvResult = convertView.findViewById(R.id.tv_result);
        tvAnalysis = convertView.findViewById(R.id.tv_analysis);
        initData();
        return convertView;
    }

    @Override
    protected void initData() {
        tvCount.setText(bean.getExamSeq() + ". ");
        tvScore.setVisibility(View.GONE);
        tvType.setText(bean.getRyExam().getTemplateStyleName() + "");
        //=====================  =====================
        //答案部分
        llResult.setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        UpgradeAnswerResultBean.RyExam ryExam = bean.getRyExam();
        tvResult.setHtmlText(ryExam.getExamAnswer());
        tvAnalysis.setHtmlText(ryExam.getExamAnalysis());
        //=====================  =====================
        tvExam.setHtmlText(ryExam.getExamStem());

        List<UpgradeAnswerResultBean.ExamOptions> options = ryExam.getExamOptions();
        if (options == null || options.isEmpty()) {
            mRadioGroup.removeAllViews();

        }
        Collections.sort(options);
        int size = options.size();
        int childCount = mRadioGroup.getChildCount();
        if (childCount > size) {//控件多, 选项少
            mRadioGroup.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(getActivity()).inflate(R.layout.item_child_select, mRadioGroup, true);
                childCount++;
            }
        }
        //添加选项View
        for (int i = 0; i < size; i++) {
            View child = mRadioGroup.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            List<UpgradeAnswerResultBean.ExamOptions> examList = ryExam.getExamOptions();
            if (examList == null || examList.isEmpty()) {
//                tvNumber.setSelected(false);
                setTextStyle(tvNumber, R.drawable.bg_selector_upgrade_answer_0);

            } else {
                Collections.sort(examList);
                if (!ryExam.getTemplateStyleName().equals("多选题")) {//单选/判断 1或者3执行
                    if (bean.getIsRight() == 1) {//正确
                        String examAnswer = ryExam.getExamAnswer();
                        String v = ryExam.getExamAnswer();
                        if (examAnswer.equals("对") || examAnswer.equals("错")) {//判断题
                            //判断题
                            if (examAnswer.equals("对")) {
                                v = "A";
                            } else {
                                v = "B";
                            }
                        }
                        if (tvNumber.getText().toString().trim().equals(v)) {
                            //正确
                            setTextStyle(tvNumber, R.drawable.bg_selector_upgrade_answer_2);//设置背景为正确
                        }
                    } else {//错误
                        String myanswer = bean.getMyAnswer();
                        String m = bean.getMyAnswer();
                        if (myanswer.equals("对") || myanswer.equals("错")) {//判断题
                            //判断题
                            if (myanswer.equals("对")) {
                                m = "A";
                            } else {
                                m = "B";
                            }
                        }
                        if (getAnswer(tvNumber, bean.getMyAnswer())) {
                            //错误样式
                            setTextStyle(tvNumber, R.drawable.bg_selector_upgrade_answer_1);
                        } else if (tvNumber.getText().toString().trim().equals(m)) {
                            setTextStyle(tvNumber, R.drawable.bg_selector_upgrade_answer_1);
                        }
                    }
//                    tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), ryExam.getExamAnswer()));
                } else {
                    if (bean.getIsRight() == 1) {
                        if (ryExam.getExamAnswer().length() > 2) {//多选，多选题
                            for (int j = 0; j < ryExam.getExamAnswer().length(); j++) {
                                s1 = ryExam.getExamAnswer().substring(j, j + 1);
                                if (tvNumber.getText().toString().trim().equals(s1)) {
                                    setTextStyle(tvNumber, R.drawable.bg_selector_upgrade_answer_2);//设置背景为正确
                                }
                            }
                        }
                    } else {
                        if (getAnswer(tvNumber, bean.getMyAnswer())) {
                            setTextStyle(tvNumber, R.drawable.bg_selector_upgrade_answer_1);//设置背景为正确
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void setDefaultFragmentTitle(String title) {

    }

    public void refreshData(UpgradeAnswerResultBean infoEntity, boolean isCheck) {
        if (infoEntity != null) {
            bean = infoEntity;
            //如果被回收的Fragment会重新从Bundle里获取数据,所以也要更新一下
            Bundle args = getArguments();
            if (args != null) {
                args.putParcelable(ARG_INFO_ENTITY, bean);
            }

            if (llResult != null) {
//                llResult.setVisibility(showAnswer ? View.VISIBLE : View.GONE);
                showAnswer = isCheck;
            }

            if (isFragmentVisible()) {
                initData();
            } else {
                setForceLoad(true);
            }
        }
    }

    /**
     * 文字颜色及背景
     */
    private void setTextStyle(final TextView tvNumbe, int res) {
        tvNumbe.setBackground(getActivity().getResources().getDrawable(res));
        tvNumbe.setTextColor(getActivity().getResources().getColor(R.color.white));
    }

    /**
     * 判断
     * 全部
     */
    private boolean getAnswer(final TextView tvNumbe, String myAnswer) {
        boolean isEquals = false;
        String answerStr = "";
        if (myAnswer.trim().length() > 2) {//多选，多选题
            String[] s = myAnswer.split(",");
            for (int i = 0; i < s.length; i++) {
                String s1 = s[i];
                switch (s1) {
                    case "1":
                        answerStr = "A";
                        break;
                    case "2":
                        answerStr = "B";
                        break;
                    case "3":
                        answerStr = "C";
                        break;
                    case "4":
                        answerStr = "D";
                        break;
                    case "5":
                        answerStr = "E";
                        break;
                    case "6":
                        answerStr = "F";
                        break;
                    default:
                        break;
                }

                if (TextUtils.equals(tvNumbe.getText().toString().trim(), answerStr)) {
                    //正确
                    isEquals = true;
                }
            }
        } else {//单选题
            myAnswer = myAnswer.replaceAll(",", "");
            switch (myAnswer) {
                case "1":
                    answerStr = "A";
                    break;
                case "2":
                    answerStr = "B";
                    break;
                case "3":
                    answerStr = "C";
                    break;
                case "4":
                    answerStr = "D";
                    break;
                case "5":
                    answerStr = "E";
                    break;
                case "6":
                    answerStr = "F";
                    break;
                default:
                    break;
            }

            if (TextUtils.equals(tvNumbe.getText().toString().trim(), answerStr)) {
                //正确
                isEquals = true;
            }
            //否则默认 false
        }
        return isEquals;
    }

}
