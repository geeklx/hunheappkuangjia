package com.example.app1zwts.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app1zwts.ContextManager;
import com.example.app1zwts.R;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;


/**
 * A simple {@link Fragment} subclass.
 */
public class IntelligentFragment extends BaseFragment implements View.OnClickListener {
    public static final String SUBJECTID = "subjectId";
    private TextView tvStart;
    private int subjectId=-1;

    public IntelligentFragment() {
        // Required empty public constructor
    }
    public static IntelligentFragment newInstance(Bundle bundle) {
        IntelligentFragment fragment = new IntelligentFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_intelligent, container, false);
        tvStart = (TextView) rootView.findViewById(R.id.tv_start);
        Bundle arg = getArguments();
        if (arg!=null){
            subjectId = arg.getInt(SUBJECTID);
        }
        initView();
        return rootView;
    }
    private void initView(){
        tvStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_start) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    ContextManager.startUpgradeAnswer(getActivity(), subjectId, 0, 0, 3);
                }
            });
        }
    }

    @Override
    public void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);

    }
}
