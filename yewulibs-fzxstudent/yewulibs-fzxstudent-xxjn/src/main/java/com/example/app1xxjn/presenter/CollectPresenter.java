package com.example.app1xxjn.presenter;


import com.example.app1xxjn.view.CollectView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.ErrorSubjectListBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.StatisticCountBean;

import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * CollectPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CollectPresenter extends BasePresenter<CollectView, BaseActivity> {
    public void subjectStatistic(final String model) {
        final String userStudentId = UserController.getUserId();
        final String baseLevelId = UserController.getBaseLevelId();

        Network.createTokenService(NetWorkService.StudyPackageService.class)
                .subjectStatistic(userStudentId, model, baseLevelId)
                .map(new StatusFunc<ErrorSubjectListBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ErrorSubjectListBean>() {
                    @Override
                    public void onNext(ErrorSubjectListBean o) {
                        dealData(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                });
    }

    public void countStatistic(final String model) {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        final String userStudentId = loginBean.getData().getUser().getId() + "";
        Network.createTokenService(NetWorkService.StudyPackageService.class)
                .countStatistic(userStudentId, model)
                .map(new StatusFunc<StatisticCountBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StatisticCountBean>() {
                    @Override
                    public void onNext(StatisticCountBean o) {
                        mView.updataAllCountData(o, model);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showShortlToast(status.getMsg());
                            } else {
                                ToastUtil.showShortlToast("数据获取失败");
                            }
                        } else {
                            ToastUtil.showShortlToast("数据获取失败");
                        }
                        mView.onFailed();

                    }

                    @Override
                    public void onCompleted() {

                    }
                });
    }

    private void dealData(ErrorSubjectListBean o) {

        ErrorSubjectListBean.DataBean[] ret = new ErrorSubjectListBean.DataBean[4];
        final List<ErrorSubjectListBean.DataBean> data = o.getData();
        Collections.sort(data);


        int tempTotal = 0;
        int otherTotal = 0;

        for (int i = 0; i < data.size(); i++) {
            tempTotal += data.get(i).getCount();
            if (i > 2) {
                otherTotal += data.get(i).getCount();
            } else {
                ret[i] = data.get(i);
            }
        }

        ErrorSubjectListBean.DataBean otherBean = new ErrorSubjectListBean.DataBean();
        otherBean.setCount(otherTotal);
        otherBean.setBaseSubjectName("其他");
        otherBean.setBaseSubjectId(-1);

        ret[3] = otherBean;

        mView.updataStatisticCount(ret);
        mView.updataTotal(tempTotal);

        mView.updataSbuject(data);

    }
}
