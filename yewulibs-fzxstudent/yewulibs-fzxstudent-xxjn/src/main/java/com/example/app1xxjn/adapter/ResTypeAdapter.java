package com.example.app1xxjn.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app1xxjn.R;
import com.sdzn.fzx.student.vo.CollectResTyepBean;

import java.util.List;

/**
 * ResTypeAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ResTypeAdapter extends RecyclerView.Adapter<ResTypeAdapter.ViewHolder> {

    private Context context;

    private List<CollectResTyepBean.DataBean> list;

    private int curItem = 0;

    private OnItemChoosedListener listener;

    public void setList(List<CollectResTyepBean.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setOnItemChoosedListener(OnItemChoosedListener listener) {
        this.listener = listener;
    }

    public ResTypeAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ResTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.item_res_type_layout, parent, false);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(ResTypeAdapter.ViewHolder holder, final int position) {
        final CollectResTyepBean.DataBean dataBean = list.get(position);
        holder.typeName.setText(dataBean.getName());
        if (position == curItem)
            holder.typeName.setEnabled(false);
        else
            holder.typeName.setEnabled(true);
        holder.typeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                curItem = position;
                notifyDataSetChanged();
                if (listener != null)
                    listener.itemChoosed(dataBean);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView typeName;

        public ViewHolder(View itemView) {
            super(itemView);
            typeName = (TextView) itemView.findViewById(R.id.type_txt);
        }
    }

    public static interface OnItemChoosedListener {
        public void itemChoosed(CollectResTyepBean.DataBean bean);
    }
}
