package com.example.app1xxjn.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app1xxjn.R;
import com.sdzn.fzx.student.vo.SubjectFilterBean;

import java.util.List;

/**
 * SubjectFilterRvAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SubjectFilterRvAdapter extends RecyclerView.Adapter<SubjectFilterRvAdapter.SubjectFilterRvViewHolder> {

    private Context mCxt;

    private List<SubjectFilterBean.DataBean> subjectList;

    private OnItemChangedListener listener;

    private int curItem = 0;

    public void setItemChangedListener(OnItemChangedListener listener) {
        this.listener = listener;
    }

    public void setSubjectList(List<SubjectFilterBean.DataBean> subjectList) {
        this.subjectList = subjectList;
        notifyDataSetChanged();
    }

    public void setCurItem(int curItem) {
        this.curItem = curItem;
        notifyDataSetChanged();
    }

    public SubjectFilterRvAdapter(Context cxt) {
        this.mCxt = cxt;
    }

    @Override
    public SubjectFilterRvViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCxt).inflate(R.layout.item_subject_filter_rv_layout, parent, false);
        SubjectFilterRvViewHolder holder = new SubjectFilterRvViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SubjectFilterRvViewHolder holder, final int position) {
        final SubjectFilterBean.DataBean dataBean = subjectList.get(position);

        holder.subjectLogo.getDrawable().setLevel(dataBean.getId());
        holder.subjectNameTxt.setText(dataBean.getName());

        if (curItem == position) {
            holder.subjectLogo.setEnabled(false);
            holder.subjectNameTxt.setEnabled(false);
        } else {
            holder.subjectLogo.setEnabled(true);
            holder.subjectNameTxt.setEnabled(true);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                curItem = position;
                notifyDataSetChanged();
                if (listener != null)
                    listener.onItemChangedByClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return subjectList == null ? 0 : subjectList.size();
    }

    public static class SubjectFilterRvViewHolder extends RecyclerView.ViewHolder {

        ImageView subjectLogo;
        TextView subjectNameTxt;
        View view;

        public SubjectFilterRvViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            subjectLogo = (ImageView) itemView.findViewById(R.id.subject_logo);
            subjectNameTxt = (TextView) itemView.findViewById(R.id.subject_name_txt);
        }
    }

    public interface OnItemChangedListener {

        void onItemChangedByClicked(int pos);
    }

}
