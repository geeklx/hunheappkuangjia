package com.example.app1xxjn.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.ErrorSubjectListBean;
import com.sdzn.fzx.student.vo.StatisticCountBean;

import java.util.List;

/**
 * CollectView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface CollectView extends BaseView {

    void updataAllCountData(StatisticCountBean bean, String model);

    void updataSbuject(List<ErrorSubjectListBean.DataBean> list);

    void updataStatisticCount(ErrorSubjectListBean.DataBean[] ret);

    void updataTotal(int total);

    void onFailed();
}
