package com.example.app1xxjn.fragment;


import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.app1xxjn.R;
import com.example.app1xxjn.adapter.CollectSubjectAdapter;
import com.example.app1xxjn.presenter.CollectPresenter;
import com.example.app1xxjn.view.CollectView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libpublic.views.ChartLegendView;
import com.sdzn.fzx.student.libpublic.views.LoadStatusView;
import com.sdzn.fzx.student.libpublic.views.UnscrollableGridView;
import com.sdzn.fzx.student.vo.ErrorSubjectListBean;
import com.sdzn.fzx.student.vo.StatisticCountBean;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CollectFragment extends MBaseFragment<CollectPresenter> implements CollectView {

    public static final String TAG = "category_tag";

    public static final int ERROR_FRAGMENT = 1;
    public static final int COLLECT_FRAGMENT = 2;

    public static final int[] CATEGORY = {ERROR_FRAGMENT, COLLECT_FRAGMENT};
    private SmartRefreshLayout refreshLayout;
    private PieChart piechart;
    private TextView tvWCount;
    private ChartLegendView clv1;
    private ChartLegendView clv3;
    private ChartLegendView clv2;
    private ChartLegendView clv4;
    private TextView todayErrorTxt;
    private TextView weekErrorTxt;
    private TextView monthErrorTxt;
    private GridView subjectGv;
    private LoadStatusView errorRy;
    private CollectSubjectAdapter mAdapter;

    private ErrorSubjectListBean.DataBean todayBean = new ErrorSubjectListBean.DataBean();
//    private List<ErrorSubjectListBean.DataBean> mDatas = new ArrayList<>();

    public static CollectFragment newInstance(Bundle bundle) {
        CollectFragment fragment = new CollectFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collect, container, false);
        refreshLayout = (SmartRefreshLayout) view.findViewById(R.id.refreshLayout);
        piechart = (PieChart) view.findViewById(R.id.piechart);
        tvWCount = (TextView) view.findViewById(R.id.tvWCount);
        clv1 = (ChartLegendView) view.findViewById(R.id.clv1);
        clv3 = (ChartLegendView) view.findViewById(R.id.clv3);
        clv2 = (ChartLegendView) view.findViewById(R.id.clv2);
        clv4 = (ChartLegendView) view.findViewById(R.id.clv4);
        todayErrorTxt = (TextView) view.findViewById(R.id.today_error_txt);
        weekErrorTxt = (TextView) view.findViewById(R.id.week_error_txt);
        monthErrorTxt = (TextView) view.findViewById(R.id.month_error_txt);
        subjectGv = view.findViewById(R.id.subject_gv);
        subjectGv.setFocusable(false);
        errorRy = (LoadStatusView) view.findViewById(R.id.error_ry);
        initView();
        initData();
        return view;
    }

    private void initView() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final View errorLayout = inflater.inflate(R.layout.error_layout, null);
        final View emptyLayout = inflater.inflate(R.layout.empty_layout, null);

        refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                mPresenter.subjectStatistic(anInt + "");
                mPresenter.countStatistic(anInt + "");
            }
        });
        errorRy.setEmptyView(emptyLayout);
        errorRy.setErrorView(errorLayout);
    }


    private int anInt;

    private void initData() {
        anInt = getArguments().getInt(CollectFragment.TAG);
        mPresenter.subjectStatistic(anInt + "");
        mPresenter.countStatistic(anInt + "");

        errorRy.onEmpty();

        if (anInt == CATEGORY[0]) {
            todayBean.setBaseSubjectName("今日错题");
            todayBean.setBaseSubjectId(100);
            mAdapter = new CollectSubjectAdapter(getActivity(), true);

        } else {
            todayBean.setBaseSubjectName("今日收藏试题");
            todayBean.setBaseSubjectId(100);
            mAdapter = new CollectSubjectAdapter(getActivity(), false);
        }
        subjectGv.setAdapter(mAdapter);
    }

    @Override
    public void initPresenter() {
        mPresenter = new CollectPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void updataAllCountData(StatisticCountBean bean, String model) {
        String tip = "";

        if (model.equals(CATEGORY[0] + "")) {
            tip = "错题";
        } else {
            tip = "收藏";
        }

        final Spanned today = Html.fromHtml("今日" + tip + " <font color='#4291FF'><big>" + bean.getData().getTodayAdd() + "</big></font> 道");
        todayErrorTxt.setText(today);

        final Spanned week = Html.fromHtml("近一周" + tip + " <font color='#4291FF'><big>" + bean.getData().getWeekAdd() + "</big></font> 道");
        weekErrorTxt.setText(week);

        final Spanned mouth = Html.fromHtml("近一月" + tip + " <font color='#4291FF'><big>" + bean.getData().getMonthAdd() + "</big></font> 道");
        monthErrorTxt.setText(mouth);

        todayBean.setCount(bean.getData().getTodayAdd());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updataSbuject(List<ErrorSubjectListBean.DataBean> list) {
        list.add(0, todayBean);
        mAdapter.setList(list);
        errorRy.setVisibility(View.GONE);
        refreshLayout.finishRefresh(true);
    }

    @Override
    public void updataStatisticCount(ErrorSubjectListBean.DataBean[] ret) {
        piechart.clearChart();
        if (ret[0] != null) {
            clv1.setCount(ret[0].getCount());
            clv1.setSubject(ret[0].getBaseSubjectName());
            clv1.setTextColor(getResources().getColor(R.color.fragment_main_chart_green));
            piechart.addPieSlice(new PieModel(ret[0].getCount(), getResources().getColor(R.color.fragment_main_chart_green)));
        }
        if (ret[1] != null) {
            clv2.setCount(ret[1].getCount());
            clv2.setSubject(ret[1].getBaseSubjectName());
            clv2.setTextColor(getResources().getColor(R.color.fragment_main_chart_yellow));
            piechart.addPieSlice(new PieModel(ret[1].getCount(), getResources().getColor(R.color.fragment_main_chart_yellow)));
        }
        if (ret[2] != null) {
            clv3.setCount(ret[2].getCount());
            clv3.setSubject(ret[2].getBaseSubjectName());
            clv3.setTextColor(getResources().getColor(R.color.fragment_main_chart_violet));
            piechart.addPieSlice(new PieModel(ret[2].getCount(), getResources().getColor(R.color.fragment_main_chart_violet)));
        }
        if (ret[3] != null) {
            clv4.setCount(ret[3].getCount());
            clv4.setSubject(ret[3].getBaseSubjectName());
            clv4.setTextColor(getResources().getColor(R.color.fragment_main_chart_other));
            piechart.addPieSlice(new PieModel(ret[3].getCount(), getResources().getColor(R.color.fragment_main_chart_other)));
        }


    }

    @Override
    public void updataTotal(int total) {
//        piechart.setInnerValueString("共" + total + "道");
        tvWCount.setText("共" + total + "道");
    }

    @Override
    public void onFailed() {
        refreshLayout.finishRefresh(false);
    }
}
