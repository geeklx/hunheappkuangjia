package com.example.app1xxjn.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.app1xxjn.R;
import com.example.app1xxjn.presenter.StudyPagePresenter;
import com.example.app1xxjn.view.StudyPageView;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Administrator on 2018/1/8.
 * 学习锦囊
 */

public class StudyPageFragment extends MBaseFragment<StudyPagePresenter> implements StudyPageView {
    private RadioGroup collectRg;
    private RadioButton errorRb;
    private RadioButton collectRb;
    private RadioButton resRb;
    private FrameLayout studyPageMain;
    private List<Fragment> fragments;

    public static StudyPageFragment newInstance(Bundle bundle) {
        StudyPageFragment fragment = new StudyPageFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new StudyPagePresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pkg, container, false);
        collectRg = (RadioGroup) rootView.findViewById(R.id.collect_rg);
        errorRb = (RadioButton) rootView.findViewById(R.id.error_rb);
        collectRb = (RadioButton) rootView.findViewById(R.id.collect_rb);
        resRb = (RadioButton) rootView.findViewById(R.id.res_rb);
        studyPageMain = (FrameLayout) rootView.findViewById(R.id.study_page_main);

        initView();
        return rootView;
    }

    private void initView() {
        fragments = new ArrayList<>();
        for (int i = 0; i < CollectFragment.CATEGORY.length; i++) {
            Bundle bundle = new Bundle();
            bundle.putInt(CollectFragment.TAG, CollectFragment.CATEGORY[i]);
            CollectFragment fragment = CollectFragment.newInstance(bundle);
            fragments.add(fragment);
        }
        CollectResFragment fragment = CollectResFragment.newInstance(null);
        fragments.add(fragment);

        collectRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.error_rb) {
                    SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                        @Override
                        public void run() {
                            showAssignedFragment(0);
                        }
                    });
                } else if (i == R.id.collect_rb) {
                    SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                        @Override
                        public void run() {
                            showAssignedFragment(1);
                        }
                    });
                } else if (i == R.id.res_rb) {
                    SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                        @Override
                        public void run() {
                            showAssignedFragment(2);
                        }
                    });
                }
            }
        });
        showAssignedFragment(0);
    }

    private Fragment currFragment;

    private void showAssignedFragment(int fragmentIndex) {
        Fragment fragment = fragments.get(fragmentIndex);
        if (currFragment == fragment) {
            return;
        }
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        if (currFragment == null) {
            ft.add(R.id.study_page_main, fragment, fragment.getClass().getName());
        } else {
            if (!fragment.isAdded()) {
                ft.hide(currFragment).add(R.id.study_page_main, fragment, fragment.getClass().getName());
            } else {
                ft.hide(currFragment).show(fragment);
            }
        }
        currFragment = fragment;
        ft.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }


//    @Override
//    public boolean isBaseOnWidth() {
//        return false;
//    }
//
//    @Override
//    public float getSizeInDp() {
//        return 1280;
//    }
}
