package com.example.app1xxjn.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.app1xxjn.R;
import com.example.app1xxjn.adapter.CollectResAdapter;
import com.example.app1xxjn.adapter.ResTypeAdapter;
import com.example.app1xxjn.presenter.ResListPresenter;
import com.example.app1xxjn.view.ResListView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnSearchClickListener;
import com.sdzn.fzx.student.libbase.pop.TaskSearchPop;
import com.sdzn.fzx.student.libpublic.views.ImageHintEditText;
import com.sdzn.fzx.student.libpublic.views.LoadStatusView;
import com.sdzn.fzx.student.libpublic.views.SpinerPopWindow;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.CollectResBean;
import com.sdzn.fzx.student.vo.CollectResTyepBean;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * 收藏资源
 */
public class ResListFragment extends MBaseFragment<ResListPresenter> implements ResListView, View.OnClickListener {

    public static final String SUBJECT_ID_TAG = "subject_id";
    public static final String SAVE_CATEGORY = "save_category";


    private static final String[] time_filter = {"全部时间", "近一周", "近一个月"};
    private static final String[] timeIds = {"3", "1", "2"};
    private ImageHintEditText btnSearch;
    private TextView typeTxt;
    private RecyclerView resTypeRv;
    private TextView spinnerTxt;
    private SmartRefreshLayout refreshLayout;
    private GridView collectResGv;
    private LoadStatusView errorRy;
    private SpinerPopWindow window;
    private CollectResAdapter mResAdapter;
    private ResTypeAdapter mTypeAdapter;
    private TaskSearchPop taskSearchPop;
    private int subjectId;

    public ResListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_res_list, container, false);
        btnSearch = (ImageHintEditText) view.findViewById(R.id.btnSearch);
        typeTxt = (TextView) view.findViewById(R.id.type_txt);
        resTypeRv = (RecyclerView) view.findViewById(R.id.res_type_rv);
        spinnerTxt = (TextView) view.findViewById(R.id.spinner_txt);
        refreshLayout = (SmartRefreshLayout) view.findViewById(R.id.refreshLayout);
        collectResGv = (GridView) view.findViewById(R.id.collect_res_gv);
        errorRy = (LoadStatusView) view.findViewById(R.id.error_ry);
        btnSearch.setOnClickListener(this);
        spinnerTxt.setOnClickListener(this);
        initView();
        initData();
        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new ResListPresenter();
        mPresenter.attachView(this, activity);
    }

    private void initView() {
        LinearLayoutManager ms = new LinearLayoutManager(getActivity());
        ms.setOrientation(LinearLayoutManager.HORIZONTAL);
        resTypeRv.setLayoutManager(ms);
        mTypeAdapter = new ResTypeAdapter(getContext());
        resTypeRv.setAdapter(mTypeAdapter);


        window = new SpinerPopWindow(getActivity());
        window.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                spinnerTxt.setText(time_filter[pos]);
                mPresenter.setTimeType(timeIds[0]);
                mPresenter.getCollectResList();
            }
        });
        window.setPopDatas(time_filter);
        mResAdapter = new CollectResAdapter(activity);
        collectResGv.setAdapter(mResAdapter);

        mResAdapter.setListener(new CollectResAdapter.RemoveResListener() {
            @Override
            public void onItemRemoved(String id) {
                mPresenter.removeCollectRes(id);
            }
        });

        mTypeAdapter.setOnItemChoosedListener(new ResTypeAdapter.OnItemChoosedListener() {
            @Override
            public void itemChoosed(CollectResTyepBean.DataBean bean) {
                mPresenter.setExamTemplateStyleId(bean.getId());
                mPresenter.getCollectResList();
            }
        });

        refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                mPresenter.getCollectResList();
            }
        });
        errorRy.setReloadListener(new LoadStatusView.ReloadListener() {
            @Override
            public void onReloadClicked() {
                errorRy.onLoadding();
                mPresenter.getResType();
                mPresenter.getCollectResList();
            }
        });
    }

    private void initData() {
        errorRy.onEmpty();
        subjectId = getArguments().getInt(SUBJECT_ID_TAG);
        mPresenter.setBaseSubjectId(subjectId + "");
        mPresenter.setTimeType(timeIds[0]);

        mPresenter.getResType();
        mPresenter.getCollectResList();

        ArrayList<CollectResTyepBean.DataBean> tempList = new ArrayList<>();
        CollectResTyepBean.DataBean dataBean = new CollectResTyepBean.DataBean();
        dataBean.setName("全部");
        tempList.add(dataBean);
        mTypeAdapter.setList(tempList);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void onUpdataResType(CollectResTyepBean bean) {
        CollectResTyepBean.DataBean dataBean = new CollectResTyepBean.DataBean();
        dataBean.setName("全部");

        if (bean.getData() == null) {
            bean.setData(new ArrayList<CollectResTyepBean.DataBean>());
            bean.getData().add(dataBean);
        } else {
            bean.getData().add(0, dataBean);
        }

        mTypeAdapter.setList(bean.getData());
    }

    @Override
    public void onUpdataResList(CollectResBean bean) {
        refreshLayout.finishRefresh(true);
        if (bean.getData().size() > 0) {
            errorRy.onSuccess();
        } else {
            errorRy.onEmpty();
        }

        mResAdapter.setResList(bean.getData());
    }

    @Override
    public void onResListError(String msg) {
        errorRy.onError();
        refreshLayout.finishRefresh(false);
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void removeCollectSuccess(String id) {
        mResAdapter.removeItem(id);
        ToastUtil.showShortlToast("取消收藏");
    }

    @Override
    public void removeCollectFailed() {
        ToastUtil.showShortlToast("取消收藏失败");
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btnSearch) {
            showPop();
        } else if (i == R.id.spinner_txt) {
            window.showAsDropDown(spinnerTxt);
        }
    }
   /* @OnClick({R.id.btnSearch, R.id.spinner_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                showPop();
                break;
            case R.id.spinner_txt:
                window.showAsDropDown(spinnerTxt);
                break;
        }
    }*/

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(activity, new OnSearchClickListener() {
                @Override
                public void onSearch(String searchStr) {

                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    mPresenter.setKeyword(searchStr);
                    mPresenter.getCollectResList();
                }
            });
        }

        taskSearchPop.showPopupWindow(SAVE_CATEGORY, btnSearch, subjectId);
    }
}
