package com.example.app1xxjn.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app1xxjn.R;
import com.sdzn.fzx.student.libbase.baseui.activity.ErrorNativeActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.TodayNativeErrorActivity;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.ErrorSubjectListBean;

import java.util.List;


/**
 * CollectSubjectAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CollectSubjectAdapter extends BaseAdapter {

    List<ErrorSubjectListBean.DataBean> list;
    private boolean isError = false;
    public Intent intent;
    private Context mCxt;

    public CollectSubjectAdapter(Context mCxt, boolean flag) {
        this.mCxt = mCxt;
        this.isError = flag;
    }

    public void setList(List<ErrorSubjectListBean.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<ErrorSubjectListBean.DataBean> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ErrorSubjectListBean.DataBean dataBean = list.get(i);
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(mCxt).inflate(R.layout.item_collect_subject_layout, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.subjectImg.getDrawable().setLevel(dataBean.getBaseSubjectId());
        holder.subjectBgImg.getDrawable().setLevel(dataBean.getBaseSubjectId());
        holder.subjectTxt.setText(dataBean.getBaseSubjectName());
        holder.subjectCountTxt.setText("共 " + dataBean.getCount() + " 道");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i == 0) {
//                    if (AppLike.showH5) {
//                        intent = new Intent(mCxt, TodayErrorActivity.class);
//                    } else {
                    SlbLoginUtil2.get().loginTowhere((Activity) mCxt, new Runnable() {
                        @Override
                        public void run() {
                            intent = new Intent(mCxt, TodayNativeErrorActivity.class);
                            isErrorshow();
                        }
                    });
//                    }
                } else {
//                    intent = new Intent(mCxt, ErrorCollectActivity.class);
//                    intent.putExtra(ErrorCollectActivity.BASESUBJECT_ID_TAG, dataBean.getBaseSubjectId() + "");
//                    intent.putExtra(ErrorCollectActivity.BASESUBJECT_NAME_TAG, dataBean.getBaseSubjectName());
//                    intent.putExtra(ErrorCollectActivity.BASESUBJECT_COUNT, dataBean.getCount() + "");
//                    if (AppLike.showH5) {
//                        intent = new Intent(mCxt, ErrorCollectActivity.class);
//                        intent.putExtra(ErrorCollectActivity.BASESUBJECT_ID_TAG, dataBean.getBaseSubjectId() + "");
//                        intent.putExtra(ErrorCollectActivity.BASESUBJECT_NAME_TAG, dataBean.getBaseSubjectName());
//                        intent.putExtra(ErrorCollectActivity.BASESUBJECT_COUNT, dataBean.getCount() + "");
//                    } else {
                    SlbLoginUtil2.get().loginTowhere((Activity) mCxt, new Runnable() {
                        @Override
                        public void run() {
                            intent = new Intent(mCxt, ErrorNativeActivity.class);
                            intent.putExtra(ErrorNativeActivity.BASESUBJECT_ID_TAG, dataBean.getBaseSubjectId() + "");
                            intent.putExtra(ErrorNativeActivity.BASESUBJECT_NAME_TAG, dataBean.getBaseSubjectName());
                            intent.putExtra(ErrorNativeActivity.BASESUBJECT_COUNT, dataBean.getCount() + "");
                            isErrorshow();
                        }
                    });

//                    }
                }
//                if (AppLike.showH5) {
//                    if (isError) {
//                        intent.putExtra(ErrorCollectActivity.IS_ERROR, "1");
//                    } else {
//                        intent.putExtra(ErrorCollectActivity.IS_ERROR, "2");
//                    }
//                } else {
//                if (isError) {
//                    intent.putExtra(ErrorNativeActivity.IS_ERROR, "1");
//                    mCxt.startActivity(intent);
//                } else {
//                    intent.putExtra(ErrorNativeActivity.IS_ERROR, "2");
//                    mCxt.startActivity(intent);
//                }
//                }
            }
        });

        return view;
    }
    public void isErrorshow(){
        if (isError) {
            intent.putExtra(ErrorNativeActivity.IS_ERROR, "1");
            mCxt.startActivity(intent);
        } else {
            intent.putExtra(ErrorNativeActivity.IS_ERROR, "2");
            mCxt.startActivity(intent);
        }
    }
    static class ViewHolder {
        /* @BindView(R.id.subject_img)
         ImageView subjectImg;
         @BindView(R.id.subject_bg_img)
         ImageView subjectBgImg;
         @BindView(R.id.subject_txt)
         TextView subjectTxt;
         @BindView(R.id.subject_count_txt)
         TextView subjectCountTxt;*/
        private ImageView subjectBgImg;
        private ImageView subjectImg;
        private TextView subjectTxt;
        private TextView subjectCountTxt;


        ViewHolder(View view) {
            subjectBgImg = (ImageView) view.findViewById(R.id.subject_bg_img);
            subjectImg = (ImageView) view.findViewById(R.id.subject_img);
            subjectTxt = (TextView) view.findViewById(R.id.subject_txt);
            subjectCountTxt = (TextView) view.findViewById(R.id.subject_count_txt);
        }
    }
}
