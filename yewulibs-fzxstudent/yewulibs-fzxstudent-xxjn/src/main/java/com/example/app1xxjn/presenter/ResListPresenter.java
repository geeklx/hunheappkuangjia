package com.example.app1xxjn.presenter;

import com.example.app1xxjn.view.ResListView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.CollectResBean;
import com.sdzn.fzx.student.vo.CollectResTyepBean;
import com.sdzn.fzx.student.vo.LoginBean;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * ResListPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ResListPresenter extends BasePresenter<ResListView, BaseActivity> {
    private String baseSubjectId;
    private String examTemplateStyleId;
    private String timeType;
    private String keyword;

    public void setBaseSubjectId(String baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public void setExamTemplateStyleId(String examTemplateStyleId) {
        this.examTemplateStyleId = examTemplateStyleId;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void getResType() {
        Network.createTokenService(NetWorkService.StudyPackageService.class)
                .getCollectTypeList(baseSubjectId)
                .map(new StatusFunc<CollectResTyepBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CollectResTyepBean>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CollectResTyepBean o) {
                        mView.onUpdataResType(o);
                    }
                });
    }

    public void getCollectResList() {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        final int userStudentId = loginBean.getData().getUser().getId();

        Network.createTokenService(NetWorkService.StudyPackageService.class)
                .getCollectResList(userStudentId + "", examTemplateStyleId, baseSubjectId, timeType, keyword)
                .map(new StatusFunc<CollectResBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<CollectResBean>(new SubscriberListener<CollectResBean>() {
                    @Override
                    public void onNext(CollectResBean o) {

                        mView.onUpdataResList(o);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.onResListError(status.getMsg());
                            } else {
                                mView.onResListError("数据获取失败");
                            }
                        } else {
                            mView.onResListError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    public void removeCollectRes(final String id) {
        Network.createTokenService(NetWorkService.StudyPackageService.class)
                .removeCollectRes(id)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.removeCollectFailed();
                    }

                    @Override
                    public void onNext(Object o) {
                        mView.removeCollectSuccess(id);

                    }
                });

    }
}
