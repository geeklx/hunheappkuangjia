package com.example.app1xxjn.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.CollectResBean;
import com.sdzn.fzx.student.vo.CollectResTyepBean;

/**
 * ResListView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface ResListView extends BaseView {

    public void onUpdataResType(CollectResTyepBean bean);

    public void onUpdataResList(CollectResBean bean);

    public void onResListError(String msg);

    public void removeCollectSuccess(String id);

    public void removeCollectFailed();

}
