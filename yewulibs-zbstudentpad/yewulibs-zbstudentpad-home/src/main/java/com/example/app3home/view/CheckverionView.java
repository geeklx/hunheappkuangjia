package com.example.app3home.view;

import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.bean.VersionInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.core.base.BaseView;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public interface CheckverionView extends IView {
    void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean);
    void OnUpdateVersionNodata(String bean);
    void OnUpdateVersionFail(String msg);
}
