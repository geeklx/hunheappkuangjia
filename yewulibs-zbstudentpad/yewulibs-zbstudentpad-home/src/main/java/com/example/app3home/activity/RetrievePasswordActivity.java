package com.example.app3home.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.example.app3home.R;
import com.example.app3home.persenter.RetrievePwdPresenter;
import com.example.app3home.view.RetrievePwdView;
import com.example.app3libpublic.utils.CountDownTimerUtils;
import com.example.app3libpublic.utils.YanzhengUtil;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * 描述：
 * - 找回密码
 * 创建人：baoshengxiang
 * 创建时间：2017/7/8
 */
public class RetrievePasswordActivity extends BaseMVPActivity<RetrievePwdView, RetrievePwdPresenter> implements View.OnClickListener, RetrievePwdView {

    EditText etPhone;
    EditText etCode;
    Button btnGetCode;
    ImageView ivBack;
    private ImageView Effectiveness;
    private EditText valicodeEdit;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_retrieve_password;
    }

    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,0,findViewById(R.id.iv_logo));
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    protected RetrievePwdPresenter createPresenter() {
        return new RetrievePwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {

        etPhone = findViewById(R.id.et_phone);
        etCode = findViewById(R.id.et_code);
        btnGetCode = findViewById(R.id.btn_get_code);
        ivBack = findViewById(R.id.iv_back);
        Effectiveness = findViewById(R.id.iv_effectiveness);

        valicodeEdit = findViewById(R.id.valicode_edit);
        Effectiveness.setOnClickListener(this);
        findViewById(R.id.btn_get_code).setOnClickListener(this);
        findViewById(R.id.btn_certain).setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_find).setOnClickListener(this);
        initData();
        initView();
    }

    private void initData() {
        mPresenter.QueryImgToken();
    }

    private void initView() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_code) {
            getVerifyCode();
        } else if (id == R.id.btn_certain) {
            confirmVerifyCode();
        } else if (id == R.id.iv_back) {
            onBackPressed();
        } else if (id == R.id.tv_find) {//                IntentController.toRegister(mContext);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.RegisterActivity");//RegisterActivity
            startActivity(intent);
        } else if (R.id.iv_effectiveness==id){
            mPresenter.QueryImgToken();
        }

    }

    private void confirmVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("验证码不能为空");
        } else {
            mPresenter.confirmVerifyCode(phoneNo, code);
        }
    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String valicode = valicodeEdit.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (phoneNo.length() != 11) {
            ToastUtils.showShort("请输入正确的手机号");
        }else if (TextUtils.isEmpty(valicode)) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.getVerifyCode(phoneNo, valicode, imgToken);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
        YanzhengUtil.startTime(60 * 1000, btnGetCode);//倒计时
    }

    @Override
    public void getCodeFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);


    }

    @Override
    public void confirmCodeSuccess() {
//        IntentController.toResetPassword(mContext);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ReSetPasswordActivity");//ReSetPasswordActivity
        startActivity(intent);
    }

    @Override
    public void confirmCodeFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        Glide.with(this).load(bytes).centerCrop().into(Effectiveness);

        Log.e("eee", inputStream.toString());
//        Bitmap bitimg = BitmapFactory.decodeStream(inputStream);
//        Glide.with(this).load(bitimg).asBitmap().into(Effectiveness);

    }

    @Override
    public void getFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        YanzhengUtil.timer_des();
    }


}
