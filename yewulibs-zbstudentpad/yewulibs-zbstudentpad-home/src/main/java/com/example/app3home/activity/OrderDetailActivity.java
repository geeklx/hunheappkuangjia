package com.example.app3home.activity;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.adapter.OrderDetailAdapter;
import com.example.app3home.persenter.OrderDetailPresenter;
import com.example.app3home.view.OrderDetailView;
import com.example.app3libpublic.event.OrderPayEvent;
import com.example.app3libpublic.listener.WechatOrAlipayListener;
import com.example.app3libpublic.manager.AlipayManager;
import com.example.app3libpublic.manager.WXPayManager;
import com.example.app3libpublic.manager.constant.PayType;
import com.example.app3libpublic.utils.DialogUtil;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.CourseList;
import com.example.app3libvariants.bean.OrderDetail;
import com.example.app3libvariants.bean.PayInfoBean;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.core.widget.SweetAlertDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * * CANCEL已取消，
 * * INIT未支付,
 * * SUCCESS支付成功，
 * * REFUND退款，
 * * CHECKING退款审核中
 * * REFUSE退款审核未通过
 */


public class OrderDetailActivity extends BaseMVPActivity<OrderDetailView, OrderDetailPresenter>
        implements OrderDetailView, View.OnClickListener {
    public static final String ORDER_ID = "to_in_order";   //

    TitleBar titleBar;
    RecyclerView rcvCourse;
    TextView tvAccount;//实付款
    TextView tvOrderNumber;//订单号
    TextView tvOrderTime;//下单时间
    TextView tvPayTime;//支付时间
    TextView tvPayType;//支付方式
    TextView tvPayState;//订单状态
    RelativeLayout rlPayTime;
    RelativeLayout rlPayType;
    TextView tvPayment;
    TextView tvCancel;
    TextView tvCourseApply;//发起退课
    RelativeLayout rlPayCancel;
    LinearLayout llTk;
    TextView tvTkReason;//退课原因
    TextView tvTkMoney;//退课金额
    TextView tvTkApplyTime;//申请时间
    TextView tvTkTime;//退款时间
    TextView tvTkReject;//驳回原因
    RelativeLayout rltvTkTime;
    RelativeLayout reTkReject;
    TextView tvCourseCancel;//取消退课
    EmptyLayout emptyLayout;
    TextView tv_number_copy;//复制

    private int orderId;
    String strPayType = "";
    private List<CourseList> orderInfoBeans;
    private OrderDetailAdapter orderInfoAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_detail;
    }

    @Override
    protected void onInit(Bundle bundle) {
        EventBus.getDefault().register(this);
        orderId = getIntent().getIntExtra(ORDER_ID, -1);
        orderInfoBeans = new ArrayList<>();
        titleBar = findViewById(R.id.title_bar);
        rcvCourse = findViewById(R.id.rcv_course);
        tvAccount = findViewById(R.id.tv_account);
        tvOrderNumber = findViewById(R.id.tv_order_number);
        tvOrderTime = findViewById(R.id.tv_order_time);
        tvPayTime = findViewById(R.id.tv_pay_time);
        tvPayType = findViewById(R.id.tv_pay_type);
        tvPayState = findViewById(R.id.tv_pay_state);
        rlPayTime = findViewById(R.id.rl_pay_time);
        rlPayType = findViewById(R.id.rl_pay_type);
        tvPayment = findViewById(R.id.tv_payment);
        tvCancel = findViewById(R.id.tv_cancel);
        tvCourseApply = findViewById(R.id.tv_course_apply);
        rlPayCancel = findViewById(R.id.rl_pay_cancel);
        llTk = findViewById(R.id.ll_tk);
        tvTkReason = findViewById(R.id.tv_tk_reason);
        tvTkMoney = findViewById(R.id.tv_tk_money);
        tvTkApplyTime = findViewById(R.id.tv_tk_apply_time);
        tvTkTime = findViewById(R.id.tv_tk_time);
        tvTkReject = findViewById(R.id.tv_tk_reject);
        rltvTkTime = findViewById(R.id.rl_tk_time);
        reTkReject = findViewById(R.id.rl_tk_reject);
        tvCourseCancel = findViewById(R.id.tv_course_cancel);
        emptyLayout = findViewById(R.id.empty_layout);
        tv_number_copy = findViewById(R.id.tv_number_copy);
        tv_number_copy.setOnClickListener(this);
        tvPayment.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvCourseApply.setOnClickListener(this);
        tvCourseCancel.setOnClickListener(this);
        initView();
    }

    private void initView() {
        orderInfoAdapter = new OrderDetailAdapter(mContext, orderInfoBeans);
        mPresenter.getDetail(orderId);
        rcvCourse.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 1));
        rcvCourse.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rcvCourse.setAdapter(orderInfoAdapter);

    }


    @Override
    protected OrderDetailPresenter createPresenter() {
        return new OrderDetailPresenter();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_number_copy) {//剪贴板
            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData mClipData = ClipData.newPlainText("Label", tvOrderNumber.getText().toString());
            cm.setPrimaryClip(mClipData);
            ToastUtils.showShort("保存至粘贴板");
        } else if (id == R.id.tv_payment) {//去付款
            DialogUtil.showSelectPaymentDialog(this, new WechatOrAlipayListener() {
                @Override
                public void selectWx() {
                    mPresenter.toBuyIndent(orderId, PayType.WXPAY);
                }

                @Override
                public void selectAlipay() {
                    mPresenter.toBuyIndent(orderId, PayType.ALIPAY);
                }
            });
        } else if (id == R.id.tv_cancel) {//取消订单
            mPresenter.toCancelIndent(orderId);
        } else if (id == R.id.tv_course_apply) {//发起退课
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ReimburseActivity");
            intent.putExtra("orderId", orderId);
            startActivity(intent);
        } else if (id == R.id.tv_course_cancel) {//取消退课
            mPresenter.toCancelCourse(orderId);
        }
    }

    @Override
    public void onOrderInfo(OrderDetail orderDetail) {
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        this.orderInfoBeans.clear();
        this.orderInfoBeans.addAll(orderDetail.getCourse_info());
        orderInfoAdapter.notifyDataSetChanged();

        switch (orderDetail.getOrder_info().getStates()) {
            case "INIT"://INIT未支付
                llTk.setVisibility(View.GONE);
                rlPayTime.setVisibility(View.GONE);
                rlPayType.setVisibility(View.GONE);
                strPayType = "待付款";
                tvPayment.setVisibility(View.VISIBLE);
                tvCancel.setVisibility(View.VISIBLE);
                tvCourseApply.setVisibility(View.GONE);
                rlPayCancel.setVisibility(View.VISIBLE);
                break;
            case "SUCCESS"://SUCCESS支付成功
                llTk.setVisibility(View.GONE);
                strPayType = "支付成功";
                tvPayment.setVisibility(View.GONE);
                tvCancel.setVisibility(View.GONE);
                tvCourseApply.setVisibility(View.VISIBLE);
                rlPayCancel.setVisibility(View.VISIBLE);
                break;
            case "CANCEL"://CANCEL已取消
                llTk.setVisibility(View.GONE);
                rlPayTime.setVisibility(View.GONE);
                rlPayType.setVisibility(View.GONE);
                strPayType = "已取消";
                rlPayCancel.setVisibility(View.GONE);
                break;
            case "CHECKING"://CHECKING退款审核中
                llTk.setVisibility(View.VISIBLE);
                strPayType = "退款审核中";
                rlPayCancel.setVisibility(View.GONE);
                upCourseView(orderDetail);
                rltvTkTime.setVisibility(View.GONE);
                reTkReject.setVisibility(View.GONE);
                break;
            case "WAITREFUND"://WAITREFUND退款审核通过等待退款中
                llTk.setVisibility(View.VISIBLE);
                strPayType = "退款审核通过等待退款中";
                rlPayCancel.setVisibility(View.GONE);
                upCourseView(orderDetail);
                rltvTkTime.setVisibility(View.GONE);
                reTkReject.setVisibility(View.GONE);
                tvCourseCancel.setVisibility(View.GONE);
                break;
            case "REFUSE"://REFUSE退款审核未通过
                llTk.setVisibility(View.VISIBLE);
                strPayType = "退款审核未通过";
                rlPayCancel.setVisibility(View.GONE);
                upCourseView(orderDetail);
                rltvTkTime.setVisibility(View.GONE);
                tvCourseCancel.setVisibility(View.GONE);
                break;
            case "REFUND"://REFUND退款
                llTk.setVisibility(View.VISIBLE);
                strPayType = "已退款";
                rlPayCancel.setVisibility(View.GONE);
                upCourseView(orderDetail);
                reTkReject.setVisibility(View.GONE);
                tvCourseCancel.setVisibility(View.GONE);
                break;
            default:
                llTk.setVisibility(View.GONE);
                rlPayCancel.setVisibility(View.GONE);
                break;
        }
        titleBar.setTitleText(strPayType);
        tvPayState.setText(strPayType);
        upView(orderDetail);
    }

    @Override
    public void onOrderError(String msg) {
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderEvent(OrderPayEvent orderPayEvent) {
        mPresenter.getDetail(orderId);
        if (orderPayEvent.isSuccess()) {
            new SweetAlertDialog.Builder(mContext).setMessage("支付成功")
                    .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                        @Override
                        public void onClick(Dialog dialog, int which) {

                        }
                    }).show();
        } else {

        }
    }

    @Override
    public void getPayInfoSuccess(PayInfoBean payInfoBean) {
        if (PayType.WXPAY.equals(payInfoBean.getPayType())) {
            WXPayManager wxPayManager = new WXPayManager(mContext);
            wxPayManager.doStartWXPayPlugin(payInfoBean.getWxpayParams());
        } else {
            AlipayManager alipayManager = new AlipayManager(mContext);
            alipayManager.doStartALiPayPlugin(payInfoBean.getAlipayOrderStr());
        }
    }

    @Override
    public void cancelSuccess() {
        mPresenter.getDetail(orderId);
    }



    private void upView(OrderDetail orderDetail) {
        //订单
        tvAccount.setText("¥" + orderDetail.getOrder_info().getSumMoney());
        tvOrderNumber.setText(orderDetail.getOrder_info().getOrderNo());
        tvOrderTime.setText(orderDetail.getOrder_info().getCreateTime());
        tvPayTime.setText(orderDetail.getOrder_info().getPayTime());
        if (PayType.WXPAY.equalsIgnoreCase(orderDetail.getOrder_info().getPayType())) {
            tvPayType.setText("微信");
        } else if (PayType.ALIPAY.equalsIgnoreCase(orderDetail.getOrder_info().getPayType())) {
            tvPayType.setText("支付宝");
        } else {
            tvPayType.setText("学币");
            rlPayCancel.setVisibility(View.GONE);
        }

    }

    private void upCourseView(OrderDetail orderDetail) {
        tvTkReason.setText("" + orderDetail.getRefund_info().getRefundEduReason());
        tvTkMoney.setText(orderDetail.getRefund_info().getRefundAmount());
        tvTkApplyTime.setText(orderDetail.getRefund_info().getSubmitTime());
        tvTkTime.setText(orderDetail.getRefund_info().getRefundTime());
        tvTkReject.setText(orderDetail.getRefund_info().getNotRefundReason());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}



