package com.example.app3home.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.example.app3home.R;
import com.example.app3home.persenter.RegisterPresenter;
import com.example.app3home.view.RegisterView;
import com.example.app3libpublic.utils.CountDownTimerUtils;
import com.example.app3libpublic.utils.VerifyUtil;
import com.example.app3libpublic.utils.YanzhengUtil;
import com.example.app3libpublic.widget.ClearEditText;
import com.example.app3libpublic.widget.PwdEditText;
import com.example.app3libvariants.network.SPManager;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * 找回密码  没有手机号的去情况下
 */
public class RegisterActivity extends BaseMVPActivity<RegisterView, RegisterPresenter> implements View.OnClickListener, RegisterView {

    ClearEditText etAccount;
    EditText etPhone;
    EditText etCode;
    PwdEditText etPassword;
    Button btnGetCode;
    ImageView ivBack;
    private ImageView Effectiveness;
    private EditText valicodeEdit;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected RegisterPresenter createPresenter() {
        return new RegisterPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        etAccount = findViewById(R.id.et_account);
        etPhone = findViewById(R.id.et_phone);
        etCode = findViewById(R.id.et_code);
        etPassword = findViewById(R.id.et_password);
        btnGetCode = findViewById(R.id.btn_get_code);
        ivBack = findViewById(R.id.iv_back);
        Effectiveness = findViewById(R.id.iv_effectiveness);
        valicodeEdit = findViewById(R.id.valicode_edit);

        Effectiveness.setOnClickListener(this);
        btnGetCode.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        findViewById(R.id.btn_certain).setOnClickListener(this);

//        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (VerifyUtil.isMobileNO(etPhone.getText().toString())) {
//                        etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                    } else {
//                        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
//                            etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.red));
//                            etPhone.setError("手机号格式不正确");
//                        }
//                    }
//                } else {
//                    etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                }
//            }
//        });
        mPresenter.QueryImgToken();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_code) {
            getVerifyCode();
        } else if (id == R.id.btn_certain) {
            register();
        } else if (id == R.id.iv_back) {
            onBackPressed();
        } else if (R.id.iv_effectiveness == id) {
            mPresenter.QueryImgToken();
        }

    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String valicode = valicodeEdit.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("请输入正确的手机号");
        }  else  if (TextUtils.isEmpty(valicode)) {
            ToastUtils.showShort("图形验证码不能为空");
        } else {
            mPresenter.getVerifyCode(phoneNo, valicode, imgToken);
        }
    }

    private void register() {
        String account = etAccount.getText().toString().trim();
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
//        String password = etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("账号不能为空");
        } else if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("验证码不能为空");
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("account", account);
            params.put("phone", phoneNo);
            params.put("code", code);
//            params.put("password", password);
            mPresenter.retrievePassword(account, phoneNo, code);
        }
    }


    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
        YanzhengUtil.startTime(60 * 1000, btnGetCode);//倒计时
    }

    @Override
    public void getCodeFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }


    @Override
    public void registerSuccess() {
        String phoneNo = etPhone.getText().toString().trim();
        SPManager.saveLastLoginAccount(phoneNo);
        ToastUtils.showShort("注册成功，请登录");
//        IntentController.toLogin(mContext);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
        startActivity(intent);
    }

    @Override
    public void registerFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        Glide.with(this).load(bytes).centerCrop().into(Effectiveness);

    }

    @Override
    public void getFailure(String msg) {
        ToastUtils.showShort(msg);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        YanzhengUtil.timer_des();
    }
}
