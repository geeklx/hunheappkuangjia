package com.example.app3home.persenter;

import android.text.TextUtils;

import com.example.app3home.R;
import com.example.app3home.view.SelectSubjectView;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import rx.Subscription;

public class SelectSubjectPresenter extends BasePresenter<SelectSubjectView> {
    public void setSubAndGrade(String eduId, String eduName, String levelId, String levelName, String gradeId, String gradeName) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .updateEducaLevel(eduId, eduName, levelId, levelName, gradeId, gradeName)
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        SPManager.saveUser(userBean);
                        SPManager.saveSection(userBean.getSubjectId(), userBean.getSubjectName(), userBean.getEducationId());
                        SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());
                        getView().setSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onFailed(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);

    }
}
