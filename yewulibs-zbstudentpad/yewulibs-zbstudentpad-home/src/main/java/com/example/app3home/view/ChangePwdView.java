package com.example.app3home.view;

import com.sdzn.core.base.BaseView;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public interface ChangePwdView extends BaseView {

    void changeSuccess();

    void changeFailure(String msg);
}
