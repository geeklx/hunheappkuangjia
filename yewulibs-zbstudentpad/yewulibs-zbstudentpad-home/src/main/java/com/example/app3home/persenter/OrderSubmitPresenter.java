package com.example.app3home.persenter;

import android.text.TextUtils;

import com.example.app3home.R;
import com.example.app3home.view.OrderSubmitView;
import com.example.app3libvariants.bean.OrderInfoBean;
import com.example.app3libvariants.bean.PayInfoBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.ShoppingCartBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.google.gson.Gson;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.exception.RetryWhenNetworkException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/19
 */
public class OrderSubmitPresenter extends BasePresenter<OrderSubmitView> {

    /**
     * 查询购物车             此处根据条件查询
     */
    public void queryShoppingCart(String shopCartId) {
        Map<String, String> map = new HashMap<>();
        map.put("id", shopCartId);
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .queryShoppingCart(requestBody)
                .compose(TransformUtils.<ResultBean<ShoppingCartBean>>defaultSchedulers())
                .map(new ResponseNewFunc<ShoppingCartBean>())
                .retryWhen(new RetryWhenNetworkException())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<ShoppingCartBean>() {


                    @Override
                    public void onNext(ShoppingCartBean shoppingCartBeen) {
                        if (shoppingCartBeen.getShopcartList() == null || shoppingCartBeen.getShopcartList().isEmpty()) {
//                            getView().queryCartEmpty();
                        } else {
                            getView().queryCartSuccess(shoppingCartBeen);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
//                        getView().queryCartFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }


    /*提交订单*/
    public void submitOrder(Map<String, String> params, final String payType) {
        String json = new Gson().toJson(params);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        RestApi.getInstance()
                .createNew(CourseService.class)
                .submitOrder(requestBody)
                .compose(TransformUtils.<ResultBean<OrderInfoBean>>allIo())
                .map(new ResponseNewFunc<OrderInfoBean>())
                .concatMap((new Func1<OrderInfoBean, Observable<ResultBean<PayInfoBean>>>() {
                    @Override
                    public Observable<ResultBean<PayInfoBean>> call(OrderInfoBean orderInfoBean) {

                        getView().submitOrderSuccess();

                        Map<String, String> map = new HashMap<>();
                        map.put("payType", payType);
                        map.put("orderId", orderInfoBean.getOrderId());
                        map.put("deviceType", "1");//0 安卓手机  1安卓pad
                        String jsonPay = new Gson().toJson(map);//要传递的json
                        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
                        return RestApi.getInstance()
                                .createNew(CourseService.class)
                                .getOrderPayInfo(requestBodyPay);
                    } // 嵌套请求

                }))
                .compose(TransformUtils.<ResultBean<PayInfoBean>>defaultSchedulers())
                .map(new ResponseNewFunc<PayInfoBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<PayInfoBean>() {
                    @Override
                    public void onNext(PayInfoBean payInfoBean) {
                        getView().getPayInfoSuccess(payInfoBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getPayInfoFailure(msg);
                    }
                }, mActivity, false));
    }
}
