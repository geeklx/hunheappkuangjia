package com.example.app3home.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.app3home.R;
import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.TimeUtils;
import com.example.app3libvariants.bean.NoteBean;
import com.example.app3libpublic.widget.TitleBar;

/**
 * 描述：我的笔记详情的
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class NoteDetailActivity extends BaseActivity {
    TitleBar titleBar;
    TextView tvCourseName;
    TextView tvDate;
    TextView tvNoteTitle;
    TextView tvNoteContent;
    NoteBean.CourseNoteAllListBean bean;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_notestatic;
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleBar = findViewById(R.id.title_bar);
        tvCourseName = findViewById(R.id.tv_course_name);
        tvDate = findViewById(R.id.tv_date);
        tvNoteTitle = findViewById(R.id.tv_note_title);
        tvNoteContent = findViewById(R.id.tv_note_content);
        initView();
        iniData();
    }

    private void iniData() {
    }

    private void initView() {
        bean = (NoteBean.CourseNoteAllListBean) getIntent().getSerializableExtra("bean");
        if (bean != null) {
            tvCourseName.setText(bean.getCourseName());
            String updateTime = TimeUtils.millis2String(bean.getUpdateTime(), "yyyy.MM.dd HH:mm");
            tvDate.setText(updateTime);
            tvNoteTitle.setText(bean.getKpointName());
            tvNoteContent.setText(bean.getContent());
        }

    }

}
