package com.example.app3home.view;

public interface SelectSubjectView {

    void setSuccess();

    void onFailed(String msg);
}
