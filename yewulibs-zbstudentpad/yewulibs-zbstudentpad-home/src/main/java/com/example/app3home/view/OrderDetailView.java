package com.example.app3home.view;

import com.example.app3libvariants.bean.OrderDetail;
import com.example.app3libvariants.bean.PayInfoBean;
import com.sdzn.core.base.BaseView;

public interface OrderDetailView extends BaseView {

    void onOrderInfo(OrderDetail orderDetail);

    void onOrderError(String msg);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void cancelSuccess();
}
