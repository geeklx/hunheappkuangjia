package com.example.app3xtsz.fragment;


import android.os.Bundle;

import com.example.app3xtsz.R;
import com.sdzn.core.base.BaseFragment;

/**
 * 注销账号
 */
public class SettingRemoveFragment extends BaseFragment {


    public SettingRemoveFragment() {
        // Required empty public constructor
    }

    public static SettingRemoveFragment newInstance() {
        return new SettingRemoveFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting_remove;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {

    }

}
