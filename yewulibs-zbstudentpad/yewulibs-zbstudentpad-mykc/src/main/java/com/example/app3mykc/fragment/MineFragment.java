package com.example.app3mykc.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.adapter.SpellingTitleAdapter;
import com.example.app3libpublic.event.UpdateAvatarEvent;
import com.example.app3libpublic.widget.EmptySchoolLayout;
import com.example.app3libpublic.widget.NoScrollViewPager;
import com.example.app3libpublic.widget.pager.PagerSlidingTabStrip;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3mykc.R;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


/**
 * 描述：
 * - 我的页面
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class MineFragment extends BaseFragment {

    EmptySchoolLayout emptySchoolLayout;
    ImageView ivBarShop;
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    NoScrollViewPager vpCourse;
    View views;
    CoursePageFragment todayCourseFragment = null;

    private SpellingTitleAdapter fragmentAdapter;
    private List<Fragment> listFragment;//定义要装fragment的列表
    private ArrayList<String> listTitle;
    private OnFragmentInteractionListener mListener;
    private UserBean userBean;

    public MineFragment() {
        // Required empty public constructor
    }

    public static MineFragment newInstance() {
        return new MineFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        emptySchoolLayout = rootView.findViewById(R.id.empty_layout_mine);
        ivBarShop = rootView.findViewById(R.id.iv_bar_right);
        mPagerSlidingTabStrip = rootView.findViewById(R.id.tabs);
        vpCourse = rootView.findViewById(R.id.vp_course);
        views = rootView.findViewById(R.id.view);
        initView();
        initData();
    }

    private void initData() {
//        userBean = SPManager.getUser();
//        GlideImgManager.loadImage(mContext, "" + userBean.getPicImg(), R.mipmap.ic_avatar, R.mipmap.ic_avatar, imgAvatar);

    }

    @Override
    public void onResume() {
        super.onResume();
        setInitOnResume();
    }

    private void initView() {
        ivBarShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!SPToken.autoLogin(mContext)) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
                    startActivity(intent);
//                    IntentController.toLogin(mContext);
                    return;
                }
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");//ShoppingCartActivity
                startActivity(intent);
//                IntentController.toShoppingCart(mContext);
            }
        });

        listFragment = new ArrayList<>();
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_TODY));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_RECENTLY));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_COURSE));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_COURSE_OVER));
        listTitle = new ArrayList<>();
        listTitle.add("今日直播");
        listTitle.add("近期学习");
        listTitle.add("全部课程");
        listTitle.add("过期课程");
        fragmentAdapter = new SpellingTitleAdapter(getChildFragmentManager());
        fragmentAdapter.setmDatas(listTitle, listFragment);
        vpCourse.setAdapter(fragmentAdapter);
        vpCourse.setNoScroll(false);
        vpCourse.setOffscreenPageLimit(3);
        vpCourse.setCurrentItem(0);
        mPagerSlidingTabStrip.setViewPager(vpCourse);
    }

//    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.onFragmentInteraction();
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setInitOnResume() {
        if (!SPToken.autoLogin(mContext)) {
            views.setVisibility(View.VISIBLE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
            emptySchoolLayout.setErrorMessage(getString(R.string.error_view_load_mine));
        } else {
            emptySchoolLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
            views.setVisibility(View.GONE);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            initData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            setInitOnResume();
        }
    }
}