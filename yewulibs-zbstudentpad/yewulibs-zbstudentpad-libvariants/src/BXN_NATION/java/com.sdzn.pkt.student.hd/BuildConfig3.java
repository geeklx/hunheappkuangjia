package com.sdzn.pkt.student.hd;

import com.example.app3libvariants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.OOO;

    public static final String FLAVOR = UrlManager.FLAVOR3;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE3;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME3;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS3;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL3;
}
