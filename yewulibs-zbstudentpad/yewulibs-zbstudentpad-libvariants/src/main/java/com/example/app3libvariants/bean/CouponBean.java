package com.example.app3libvariants.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 描述：我的优惠券
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/20
 */

public class CouponBean implements Parcelable {

    /**
     * amount : 100
     * couponCode : 20KO17M07191450484D6
     * couponId : 10
     * createTime : 1500447048000
     * endTime : 1501257599000
     * id : 48
     * limitAmount : 0
     * optuserName : 小因
     * remindStatus : INIT
     * requestId :
     * startTime : 1500393600000
     * status : 5
     * title : 大优惠
     * trxorderId : 0
     * type : 0
     * useType : 2
     * userId : 1402
     */

    private int amount;
    private String couponCode;
    private int couponId;
    private long createTime;
    private long endTime;
    private int id;
    private int limitAmount;
    private String optuserName;
    private String remindStatus;
    private String requestId;
    private long startTime;
    private int status;
    private String title;
    private int trxorderId;
    private int type;
    private int useType;
    private int userId;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(int limitAmount) {
        this.limitAmount = limitAmount;
    }

    public String getOptuserName() {
        return optuserName;
    }

    public void setOptuserName(String optuserName) {
        this.optuserName = optuserName;
    }

    public String getRemindStatus() {
        return remindStatus;
    }

    public void setRemindStatus(String remindStatus) {
        this.remindStatus = remindStatus;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTrxorderId() {
        return trxorderId;
    }

    public void setTrxorderId(int trxorderId) {
        this.trxorderId = trxorderId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUseType() {
        return useType;
    }

    public void setUseType(int useType) {
        this.useType = useType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.amount);
        dest.writeString(this.couponCode);
        dest.writeInt(this.couponId);
        dest.writeLong(this.createTime);
        dest.writeLong(this.endTime);
        dest.writeInt(this.id);
        dest.writeInt(this.limitAmount);
        dest.writeString(this.optuserName);
        dest.writeString(this.remindStatus);
        dest.writeString(this.requestId);
        dest.writeLong(this.startTime);
        dest.writeInt(this.status);
        dest.writeString(this.title);
        dest.writeInt(this.trxorderId);
        dest.writeInt(this.type);
        dest.writeInt(this.useType);
        dest.writeInt(this.userId);
    }

    public CouponBean() {
    }

    protected CouponBean(Parcel in) {
        this.amount = in.readInt();
        this.couponCode = in.readString();
        this.couponId = in.readInt();
        this.createTime = in.readLong();
        this.endTime = in.readLong();
        this.id = in.readInt();
        this.limitAmount = in.readInt();
        this.optuserName = in.readString();
        this.remindStatus = in.readString();
        this.requestId = in.readString();
        this.startTime = in.readLong();
        this.status = in.readInt();
        this.title = in.readString();
        this.trxorderId = in.readInt();
        this.type = in.readInt();
        this.useType = in.readInt();
        this.userId = in.readInt();
    }

    public static final Creator<CouponBean> CREATOR = new Creator<CouponBean>() {
        @Override
        public CouponBean createFromParcel(Parcel source) {
            return new CouponBean(source);
        }

        @Override
        public CouponBean[] newArray(int size) {
            return new CouponBean[size];
        }
    };


}
