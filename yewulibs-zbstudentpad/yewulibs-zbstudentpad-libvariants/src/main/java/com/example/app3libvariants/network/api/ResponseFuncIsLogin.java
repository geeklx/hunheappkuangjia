package com.example.app3libvariants.network.api;

import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;

import rx.functions.Func1;

/**
 * code=90001 时 登录失效
 * <p>
 * 进入时使用    有token但失效时起作用（）
 */
public class ResponseFuncIsLogin<T> implements Func1<ResultBean<T>, T> {
    private static final int CODE = 401;
    private static final int CODE_LOSE = 2000;

    //此处逻辑根据约定报文进行修改，实现逻辑即可
    @Override
    public T call(ResultBean<T> httpResult) {
        if (0 == httpResult.getCode()) {//httpResult.isSuccess()
            LogUtils.i("LoginResult: " + httpResult.getResult());
            return httpResult.getResult();
        } else {
            if (CODE == httpResult.getCode() || CODE_LOSE == httpResult.getCode()) {//登录失效
                SPManager.changeLogin(App2.get(), false);
                ToastUtils.showShort(httpResult.getMsg() + "");
                EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程
                throw new ApiException(new Throwable(httpResult.getMsg()), httpResult.getCode());
            } else {
                LogUtils.e("LoginError: " + httpResult.getCode() + ", " + httpResult.getMsg() + ", " + httpResult.getResult());
                throw new ApiException(new Throwable(httpResult.getMsg()), httpResult.getCode());
            }
        }
    }
}