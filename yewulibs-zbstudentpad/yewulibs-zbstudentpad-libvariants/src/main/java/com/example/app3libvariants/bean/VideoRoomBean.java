package com.example.app3libvariants.bean;

/**
 * 描述：回放的实体类
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/16
 */

public class VideoRoomBean {

    private String origUrl;

    public String getOrigUrl() {
        return origUrl;
    }

    public void setOrigUrl(String origUrl) {
        this.origUrl = origUrl;
    }
}
