package com.example.app3libvariants.bean;

/**
 * 描述：申请退款后返回的实体类
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/7
 */

public class ReimburseBean {


    /**
     * msg : 只有支付成功的订单可以退课
     * canRefund : false
     * states : INIT
     */

    private String msg;
    private String canRefund;
    private String states;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCanRefund() {
        return canRefund;
    }

    public void setCanRefund(String canRefund) {
        this.canRefund = canRefund;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }
}
