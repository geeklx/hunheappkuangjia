package com.example.app3libvariants.bean;

import java.util.List;

public class RecommendCourse {


    /**
     * rows : [{"addTime":"2019-11-14 14:35:22","appointSchoolId":"","assistantTeacherId":0,"auditionLessionCount":0,"classId":313,"context":"<p>33346543<\/p>","courseGrossIncome":0,"courseId":881,"courseKpointLits":[],"courseName":"测试课程001","courseTerm":0,"currentPrice":0,"customerId":0,"endTime":null,"grade":0,"isAvaliable":0,"isPublic":0,"kpointNum":6,"lessionNum":0,"levelId":0,"liveBeginTime":null,"liveCourseId":0,"liveEndTime":null,"logo":"http://file.znclass.com/0c21d23e-4836-42ba-9e63-0e325e254127bg.png","loseTime":"","losetype":0,"mainTeacherId":0,"orgId":0,"packageEduCourseList":[],"packageType":0,"pageBuycount":0,"pageViewcount":0,"refundEndTime":null,"sellType":"LIVE","sequence":0,"signEndTime":"2019-11-27 14:46:53","sourcePrice":0,"subjectId":333,"subjectName":"英语","teacherId":0,"teacherName":"寿大祖","title":"1111","updateTime":null}]
     * total : 1
     */

    private int total;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * addTime : 2019-11-14 14:35:22
         * appointSchoolId :
         * assistantTeacherId : 0
         * auditionLessionCount : 0
         * classId : 313
         * context : <p>33346543</p>
         * courseGrossIncome : 0
         * courseId : 881
         * courseKpointLits : []
         * courseName : 测试课程001
         * courseTerm : 0
         * currentPrice : 0
         * customerId : 0
         * endTime : null
         * grade : 0
         * isAvaliable : 0
         * isPublic : 0
         * kpointNum : 6
         * lessionNum : 0
         * levelId : 0
         * liveBeginTime : null
         * liveCourseId : 0
         * liveEndTime : null
         * logo : http://file.znclass.com/0c21d23e-4836-42ba-9e63-0e325e254127bg.png
         * loseTime :
         * losetype : 0
         * mainTeacherId : 0
         * orgId : 0
         * packageEduCourseList : []
         * packageType : 0
         * pageBuycount : 0
         * pageViewcount : 0
         * refundEndTime : null
         * sellType : LIVE
         * sequence : 0
         * signEndTime : 2019-11-27 14:46:53
         * sourcePrice : 0
         * subjectId : 333
         * subjectName : 英语
         * teacherId : 0
         * teacherName : 寿大祖
         * title : 1111
         * updateTime : null
         */

        private String addTime;
        private String appointSchoolId;
        private int assistantTeacherId;
        private int auditionLessionCount;
        private int classId;
        private String context;
        private int courseGrossIncome;
        private int courseId;
        private String courseName;
        private int courseTerm;
        private double currentPrice;
        private int customerId;
        private Object endTime;
        private int grade;
        private int isAvaliable;
        private int isPublic;
        private int kpointNum;
        private int lessionNum;
        private int levelId;
        private String liveBeginTime;
        private int liveCourseId;
        private String liveEndTime;
        private String logo;
        private String loseTime;
        private int losetype;
        private int mainTeacherId;
        private int orgId;
        private int packageType;
        private int pageBuycount;
        private int pageViewcount;
        private Object refundEndTime;
        private String sellType;
        private int sequence;
        private String signEndTime;
        private double sourcePrice;
        private int subjectId;
        private String subjectName;
        private int teacherId;
        private String teacherName;
        private String title;
        private Object updateTime;
        private List<?> courseKpointLits;
        private List<?> packageEduCourseList;

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getAppointSchoolId() {
            return appointSchoolId;
        }

        public void setAppointSchoolId(String appointSchoolId) {
            this.appointSchoolId = appointSchoolId;
        }

        public int getAssistantTeacherId() {
            return assistantTeacherId;
        }

        public void setAssistantTeacherId(int assistantTeacherId) {
            this.assistantTeacherId = assistantTeacherId;
        }

        public int getAuditionLessionCount() {
            return auditionLessionCount;
        }

        public void setAuditionLessionCount(int auditionLessionCount) {
            this.auditionLessionCount = auditionLessionCount;
        }

        public double getCurrentPrice() {
            return currentPrice;
        }

        public void setCurrentPrice(double currentPrice) {
            this.currentPrice = currentPrice;
        }

        public double getSourcePrice() {
            return sourcePrice;
        }

        public void setSourcePrice(double sourcePrice) {
            this.sourcePrice = sourcePrice;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getContext() {
            return context;
        }

        public void setContext(String context) {
            this.context = context;
        }

        public int getCourseGrossIncome() {
            return courseGrossIncome;
        }

        public void setCourseGrossIncome(int courseGrossIncome) {
            this.courseGrossIncome = courseGrossIncome;
        }

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public int getCourseTerm() {
            return courseTerm;
        }

        public void setCourseTerm(int courseTerm) {
            this.courseTerm = courseTerm;
        }



        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public Object getEndTime() {
            return endTime;
        }

        public void setEndTime(Object endTime) {
            this.endTime = endTime;
        }

        public int getGrade() {
            return grade;
        }

        public void setGrade(int grade) {
            this.grade = grade;
        }

        public int getIsAvaliable() {
            return isAvaliable;
        }

        public void setIsAvaliable(int isAvaliable) {
            this.isAvaliable = isAvaliable;
        }

        public int getIsPublic() {
            return isPublic;
        }

        public void setIsPublic(int isPublic) {
            this.isPublic = isPublic;
        }

        public int getKpointNum() {
            return kpointNum;
        }

        public void setKpointNum(int kpointNum) {
            this.kpointNum = kpointNum;
        }

        public int getLessionNum() {
            return lessionNum;
        }

        public void setLessionNum(int lessionNum) {
            this.lessionNum = lessionNum;
        }

        public int getLevelId() {
            return levelId;
        }

        public void setLevelId(int levelId) {
            this.levelId = levelId;
        }

        public String getLiveBeginTime() {
            return liveBeginTime;
        }

        public void setLiveBeginTime(String liveBeginTime) {
            this.liveBeginTime = liveBeginTime;
        }

        public String getLiveEndTime() {
            return liveEndTime;
        }

        public void setLiveEndTime(String liveEndTime) {
            this.liveEndTime = liveEndTime;
        }

        public int getLiveCourseId() {
            return liveCourseId;
        }

        public void setLiveCourseId(int liveCourseId) {
            this.liveCourseId = liveCourseId;
        }


        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getLoseTime() {
            return loseTime;
        }

        public void setLoseTime(String loseTime) {
            this.loseTime = loseTime;
        }

        public int getLosetype() {
            return losetype;
        }

        public void setLosetype(int losetype) {
            this.losetype = losetype;
        }

        public int getMainTeacherId() {
            return mainTeacherId;
        }

        public void setMainTeacherId(int mainTeacherId) {
            this.mainTeacherId = mainTeacherId;
        }

        public int getOrgId() {
            return orgId;
        }

        public void setOrgId(int orgId) {
            this.orgId = orgId;
        }

        public int getPackageType() {
            return packageType;
        }

        public void setPackageType(int packageType) {
            this.packageType = packageType;
        }

        public int getPageBuycount() {
            return pageBuycount;
        }

        public void setPageBuycount(int pageBuycount) {
            this.pageBuycount = pageBuycount;
        }

        public int getPageViewcount() {
            return pageViewcount;
        }

        public void setPageViewcount(int pageViewcount) {
            this.pageViewcount = pageViewcount;
        }

        public Object getRefundEndTime() {
            return refundEndTime;
        }

        public void setRefundEndTime(Object refundEndTime) {
            this.refundEndTime = refundEndTime;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }

        public int getSequence() {
            return sequence;
        }

        public void setSequence(int sequence) {
            this.sequence = sequence;
        }

        public String getSignEndTime() {
            return signEndTime;
        }

        public void setSignEndTime(String signEndTime) {
            this.signEndTime = signEndTime;
        }


        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public int getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(int teacherId) {
            this.teacherId = teacherId;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public List<?> getCourseKpointLits() {
            return courseKpointLits;
        }

        public void setCourseKpointLits(List<?> courseKpointLits) {
            this.courseKpointLits = courseKpointLits;
        }

        public List<?> getPackageEduCourseList() {
            return packageEduCourseList;
        }

        public void setPackageEduCourseList(List<?> packageEduCourseList) {
            this.packageEduCourseList = packageEduCourseList;
        }
    }
}
