package com.example.app3libvariants.bean;


/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/24
 */
public class OrderInfoBean {

    /**
     * order : {"couponAmount":0,"couponCodeId":0,"createTime":"2017-07-24 18:03:54","createTimeFormat":"0秒前","description":"无","limitNum":0,"orderAmount":0.01,"orderId":239,"orderNo":"146420170724180354290","orderType":"COURSE","payType":"WEIXIN","reqChannel":"APP","reqIp":"192.168.6.118","states":"INIT","sumMoney":0.01,"sysUserId":0,"userId":1464,"version":0}
     */

    private OrderBean order;

    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public static class OrderBean {
        /**
         * couponAmount : 0
         * couponCodeId : 0
         * createTime : 2017-07-24 18:03:54
         * createTimeFormat : 0秒前
         * description : 无
         * limitNum : 0
         * orderAmount : 0.01
         * orderId : 239
         * orderNo : 146420170724180354290
         * orderType : COURSE
         * payType : WEIXIN
         * reqChannel : APP
         * reqIp : 192.168.6.118
         * states : INIT
         * sumMoney : 0.01
         * sysUserId : 0
         * userId : 1464
         * version : 0
         */

        private int couponAmount;
        private int couponCodeId;
        private String createTime;
        private String createTimeFormat;
        private String description;
        private int limitNum;
        private double orderAmount;
        private int orderId;
        private String orderNo;
        private String orderType;
        private String payType;
        private String reqChannel;
        private String reqIp;
        private String states;
        private double sumMoney;
        private int sysUserId;
        private int userId;
        private int version;

        public int getCouponAmount() {
            return couponAmount;
        }

        public void setCouponAmount(int couponAmount) {
            this.couponAmount = couponAmount;
        }

        public int getCouponCodeId() {
            return couponCodeId;
        }

        public void setCouponCodeId(int couponCodeId) {
            this.couponCodeId = couponCodeId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateTimeFormat() {
            return createTimeFormat;
        }

        public void setCreateTimeFormat(String createTimeFormat) {
            this.createTimeFormat = createTimeFormat;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getLimitNum() {
            return limitNum;
        }

        public void setLimitNum(int limitNum) {
            this.limitNum = limitNum;
        }

        public double getOrderAmount() {
            return orderAmount;
        }

        public void setOrderAmount(double orderAmount) {
            this.orderAmount = orderAmount;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getPayType() {
            return payType;
        }

        public void setPayType(String payType) {
            this.payType = payType;
        }

        public String getReqChannel() {
            return reqChannel;
        }

        public void setReqChannel(String reqChannel) {
            this.reqChannel = reqChannel;
        }

        public String getReqIp() {
            return reqIp;
        }

        public void setReqIp(String reqIp) {
            this.reqIp = reqIp;
        }

        public String getStates() {
            return states;
        }

        public void setStates(String states) {
            this.states = states;
        }

        public double getSumMoney() {
            return sumMoney;
        }

        public void setSumMoney(double sumMoney) {
            this.sumMoney = sumMoney;
        }

        public int getSysUserId() {
            return sysUserId;
        }

        public void setSysUserId(int sysUserId) {
            this.sysUserId = sysUserId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }
    }
}
