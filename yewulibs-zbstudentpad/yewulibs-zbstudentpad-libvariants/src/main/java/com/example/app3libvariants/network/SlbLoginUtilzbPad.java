package com.example.app3libvariants.network;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.fzx.student.libutils.util.Log;


public class SlbLoginUtilzbPad {

    public static final int LOGIN_REQUEST_CODE = 301;
    public static final int LOGINOUT_REQUEST_CODE = 302;
    public static final int LOGIN_RESULT_OK = 101;
    public static final int LOGIN_RESULT_CANCELED = 102;
    public static final int LOGINOUT_RESULT_OK = 201;
    public static final int LOGINOUT_RESULT_CANCELED = 202;

    private static SlbLoginUtilzbPad sInstance;
    private static final Object lock = new Object();
    private Runnable mLastRunnnable;


    public SlbLoginUtilzbPad() {
    }

    public static SlbLoginUtilzbPad get() {
        if (sInstance == null) {
            synchronized (lock) {
                sInstance = new SlbLoginUtilzbPad();
            }
        }
        return sInstance;
    }

    /**
     * 用户是否登录
     *
     * @return
     */
    public boolean isUserLogin() {
        // step 1 判断内存中是否有user_id
//        if (!TextUtils.isEmpty(SPUtils.getInstance().getString(CommonUtils.USER_TOKEN))) {
//        return (boolean) StudentSPUtils.get(App2.get(), StudentSPUtils.GUIDANCE_TAG, false);
//        return !TextUtils.isEmpty(MmkvUtils.getInstance().get_common(CommonUtils.MMKV_TOKEN));
//        // step 2 如果内存中没有， 则去文件中找
//        String uid = (String) SpUtils.get(get()).get(ConstantUtil.USER_ID, null);
//        // step 3 如果文件中有， 则提到内存中
//        if (!TextUtils.isEmpty(uid)) {
//            DataProvider.setUser_id(uid);
//            return true;
//        }
        // 未登录
        return true;
    }

    /**
     * 用户是否登录
     *
     * @return
     */
    public boolean isUserLogins() {
        String token = SPToken.getToken();
        Log.d("aaatest" + token);
        Log.d("aaatest" + TextUtils.isEmpty(token));
        return !TextUtils.isEmpty(token);
    }


    public void loginTowhere(Activity activity, Runnable runnable) {
        if (isUserLogins()) {
            if (runnable != null) {
                runnable.run();
                return;
            }
        }
        mLastRunnnable = runnable;
        login(activity);
    }

    public void login(Activity activity) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.loginactivity");
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(intent, LOGIN_REQUEST_CODE);
        }
    }

    public void loginOutTowhere(Activity activity, Runnable runnable) {
//        if (!isUserLogin()) {
//            if (runnable == null) {
//                runnable.run();
//                return;
//            }
//        }
        mLastRunnnable = runnable;
        loginOut(activity);
    }

    public void loginOut(Activity activity) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.otherlogintipactivity");
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(intent, LOGINOUT_REQUEST_CODE);
        }
    }

    public boolean login_activity_result(int requestCode, int resultCode, Intent data) {
        Runnable runnable = mLastRunnnable;
        mLastRunnnable = null;
        //已登录
        if (requestCode == LOGIN_REQUEST_CODE) {
            if (resultCode == LOGIN_RESULT_OK && runnable != null) {
                runnable.run();
            }
            return true;
        }
        //未登录
        if (requestCode == LOGINOUT_REQUEST_CODE) {
            if (resultCode == LOGINOUT_RESULT_OK && runnable != null) {
                runnable.run();
            }
            return true;
        }
        return false;
    }

}
