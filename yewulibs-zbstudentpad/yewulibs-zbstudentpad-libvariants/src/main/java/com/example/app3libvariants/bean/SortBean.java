package com.example.app3libvariants.bean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class SortBean {
    private int sortId;
    private String sortName;

    public SortBean(int sortId, String sortName) {
        this.sortId = sortId;
        this.sortName = sortName;
    }

    public int getSortId() {
        return sortId;
    }

    public void setSortId(int sortId) {
        this.sortId = sortId;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }
}
