package com.example.app3libvariants.network;

import android.content.Context;

import com.example.app3libvariants.bean.SectionBean;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.EncodeUtils;
import com.sdzn.core.utils.SPUtils;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.List;

/**
 * 描述：
 * - sp文件管理类
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SPManager {
    private static final String SP_NAME = "AppConfig";

    /**
     * 是否是第一次加载应用
     *
     * @param context
     * @return
     */
    public static boolean isFirst(Context context) {
        String version = (String) SPUtils.get(context, SP_NAME, "version", App2Utils.getVersion());
        boolean isFirst = (boolean) SPUtils.get(context, SP_NAME, "isfirst", true);
        if (App2Utils.getVersion().equals(version) || isFirst) {
            SPUtils.put(context, SP_NAME, "version", App2Utils.getVersion());
            SPUtils.put(context, SP_NAME, "isfirst", false);
        }
        return isFirst;
    }

    public static void setFirstEnterApp(Context context) {
        SPUtils.put(context, SP_NAME, "isfirst", true);
        SPUtils.put(context, SP_NAME, "version", "");
    }

    /**
     * 是否第一弹出 选中年级
     */
    public static boolean isFirstSubjectId(Context context) {
        String version = (String) SPUtils.get(context, SP_NAME, "version_sub", App2Utils.getVersion());
        boolean isFirst = (boolean) SPUtils.get(context, SP_NAME, "firstSub", true);
        return isFirst;
    }

    public static void setFirstSubjectId(Context context) {
        SPUtils.put(context, SP_NAME, "version_sub", App2Utils.getVersion());
        SPUtils.put(context, SP_NAME, "firstSub", false);
    }


    /**
     * 改变用户登录状态
     *
     * @param context
     * @param loginStatus
     */
    public static void changeLogin(Context context, boolean loginStatus) {
        SPUtils.put(context, SP_NAME, "islogin", loginStatus);
        if (loginStatus) {
            SPUtils.put(context, SP_NAME, "lastLoginTime", System.currentTimeMillis());
        }
        if (!loginStatus) {
            SPToken.saveToken("");
        }
    }


    /**
     * 是否登录超时
     *
     * @param context
     * @return
     */
    public static boolean isLoginOverdue(Context context) {
        long overdueTime = 1000L * 60L * 60L * 24L * 7L;
        long currentTime = System.currentTimeMillis();
        long lastLoginTime = (long) SPUtils.get(context, SP_NAME, "lastLoginTime", currentTime);
        long subTime = currentTime - lastLoginTime;//20天的登录有效期
        return subTime < overdueTime;
    }

    /**
     * 处于登录  toc
     */

    public static boolean isToCLogin() {
//        return SPManager.getUser().getUserName().isEmpty();
        return 0 == SPManager.getUser().getFzxStatus();
    }


    /**
     * 存储登录账号
     *
     * @param account
     */
    public static void saveLastLoginAccount(String account) {
        SPUtils.put(App2.get(), SP_NAME, "lastLoginAccount", account);
    }

    /**
     * 获取上次登录账号
     */
    public static String getLastLoginAccount() {
        return (String) SPUtils.get(App2.get(), SP_NAME, "lastLoginAccount", "");
    }

    /**
     * 存储用户信息
     *
     * @param userBean
     */
    public static void saveUser(UserBean userBean) {
        SPUtils.putObject(App2.get(), SP_NAME, "userBean", userBean);
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public static UserBean getUser() {
        UserBean userBean = SPUtils.getObject(App2.get(),
                SP_NAME, "userBean", UserBean.class);
        return userBean != null ? userBean : new UserBean();
    }

    /**
     * 存储密码
     *
     * @param password
     */
    public static void savePwd(String password) {
        //md5加密无法还原, 暂用base64
        String b64 = new String(EncodeUtils.base64Encode(password));
        SPUtils.put(App2.get(), getSpName(), "pwd", b64);
//        SPUtils.put(App2.get(), getSpName(), "pwd", EncryptUtils.encryptMD5ToString(password));
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public static String getPwd() {
        String code = (String) SPUtils.get(App2.get(), getSpName(), "pwd", "");
        return new String(EncodeUtils.base64Decode(code));
//        return (String) SPUtils.get(App2.get(), getSpName(), "pwd", "");
    }


    /**
     * 存储历史搜索记录
     *
     * @param keywords
     */
    public static void saveSearchStr(List<String> keywords) {
        SPUtils.putList(App2.get(), getSpName(), "searchHistory", keywords);
    }

    /**
     * 保存学段信息
     */
    public static void saveSection(SectionBean bean) {
        SPUtils.put(App2.get(), getSpName(), "sectionId", bean.getSectionId());
        SPUtils.put(App2.get(), getSpName(), "sectionName", bean.getSectionName());
        SPUtils.put(App2.get(), getSpName(), "eduId", bean.getEducationId());
    }

    public static void saveSection(int id, String name, int eduId) {
        SPUtils.put(App2.get(), getSpName(), "sectionId", id);
        SPUtils.put(App2.get(), getSpName(), "sectionName", name);
        SPUtils.put(App2.get(), getSpName(), "eduId", eduId);
    }

    /**
     * 获取学段ID, 默认为-1
     */
    public static int getSectionId() {
        return (int) SPUtils.get(App2.get(), getSpName(), "sectionId", -1);
    }

    /**
     * 获得学制  默认为0
     */
    public static int getEduId() {
        if (SPUtils.get(App2.get(), getSpName(), "eduId", 0) != null) {
            return (int) SPUtils.get(App2.get(), getSpName(), "eduId", 0);
        } else {
            return 0;
        }
    }

    /**
     * 获取学段名称默认为""
     */
    public static String getSectionName() {
        return (String) SPUtils.get(App2.get(), getSpName(), "sectionName", "");
    }

    /**
     * 保存年级 年级名
     */
    public static void saveGrade(int gradeId, String gradeName) {
        SPUtils.put(App2.get(), getSpName(), "gradeId", gradeId);
        SPUtils.put(App2.get(), getSpName(), "gradeName", gradeName);
    }

    /**
     * 获取年级ID, 默认为-1
     */
    public static int getgradeId() {
        return (int) SPUtils.get(App2.get(), getSpName(), "gradeId", -1);
    }

    /**
     * 获取年级名称默认为""
     */
    public static String getgradeName() {
        return (String) SPUtils.get(App2.get(), getSpName(), "gradeName", "");
    }

    /**
     * 保存学段信息 *********学校课程********* 学制
     */
    public static void saveSchoolSection(int id, String name, int
            eduId) {
        SPUtils.put(App2.get(), getSpName(), "sectionSchoolId", id);
        SPUtils.put(App2.get(), getSpName(), "sectionSchoolName", name);
        SPUtils.put(App2.get(), getSpName(), "eduSchoolId", eduId);
    }

    /**
     * 获取学段ID, 默认为-1 *********学校课程*********
     */
    public static int getSchoolSectionId() {
        return (int) SPUtils.get(App2.get(), getSpName(), "sectionSchoolId", -1);
    }

    /**
     * 获取学制ID, 默认为-1 *********学校课程*********
     */
    public static int getSchoolEduId() {
        return (int) SPUtils.get(App2.get(), getSpName(), "eduSchoolId", -1);
    }

    //    /**
//     * 获取学段名称默认为""  *********学校课程*********
//     */
    public static String getSchoolSectionName() {
        return (String) SPUtils.get(App2.get(), getSpName(), "sectionSchoolName", "");
    }

    /**
     * 保存年级 年级名*********学校课程*********
     */
    public static void saveSchoolGrade(int gradeId, String gradeName) {
        SPUtils.put(App2.get(), getSpName(), "gradeSchoolId", gradeId);
        SPUtils.put(App2.get(), getSpName(), "gradeSchoolName", gradeName);
    }

    /**
     * 获取年级ID, 默认为-1 *********学校课程*********
     */
    public static int getSchoolgradeId() {
        return (int) SPUtils.get(App2.get(), getSpName(), "gradeSchoolId", -1);
    }

    /**
     * 获取年级名称默认为""*********学校课程*********
     */
    public static String getSchoolgradeName() {
        return (String) SPUtils.get(App2.get(), getSpName(), "gradeSchoolName", "");
    }

    /**
     * 学制
     */


    /**
     * 获取历史搜索记录
     *
     * @return
     */
    public static List getSearchStr() {
        return SPUtils.getList(App2.get(), getSpName(), "searchHistory");
    }

    /**
     * 获取教师搜索记录
     */
    public static List getSearchTeacherStr() {
        return SPUtils.getList(App2.get(), getSpName(), "searchTeacherHistory");
    }

    /**
     * 保存教师搜索记录
     */
    public static void saveSearchTeacherStr(List<String> keywords) {
        SPUtils.putList(App2.get(), getSpName(), "searchTeacherHistory", keywords);
    }

    /**
     * 移动网络是否可用
     */
    public static boolean getMobileNetSwitch() {
        return (boolean) SPUtils.get(App2.get(), getSpName(), "mobileNetSwitch", true);
    }

    /**
     * 移动网络是否可用
     */
    public static void changeMobileNetSwitch(boolean checked) {
        SPUtils.put(App2.get(), getSpName(), "mobileNetSwitch", checked);
    }


    /**
     * 获取当前用户的sp文件名
     *
     * @return
     */
    private static String getSpName() {
        UserBean userBean = getUser();
        if (userBean != null) {
            return SP_NAME + "_" + userBean.getUserId();
        } else {
            return SP_NAME;
        }
    }

    /**
     * 基本配置  其实就是年级学段
     */
    public static void setBaseConfig(String configStr) {
        SPUtils.put(App2.get(), getSpName(), "gradeJson", configStr);
    }

    /**
     * 基本配置
     */
    public static String getBaseConfig() {
        return (String) SPUtils.get(App2.get(), getSpName(), "gradeJson", "");
    }
}
