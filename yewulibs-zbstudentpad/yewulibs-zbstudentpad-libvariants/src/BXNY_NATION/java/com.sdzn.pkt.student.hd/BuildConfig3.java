package com.sdzn.pkt.student.hd;

import com.example.app3libvariants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.YYY;

    public static final String FLAVOR = UrlManager.FLAVOR2;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE2;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME2;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS2;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL2;
}
