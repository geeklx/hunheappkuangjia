package com.example.app3grzx.fragment;

import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3grzx.R;
import com.example.app3grzx.adapter.CourseCataloguAdapter;
import com.example.app3libvariants.bean.CourseKpointListBean;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * - 单一课程目录
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class CourseCatalogueFragment extends BaseFragment {

    RecyclerView rcvCourseCatalogue;

    private CourseCataloguAdapter courseGroupCatalogueAdapter;
    private List<CourseKpointListBean> courseCatalogueBeans;

    public CourseCatalogueFragment() {

    }

    public static CourseCatalogueFragment newInstance() {
        return new CourseCatalogueFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course_catalogue;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        rcvCourseCatalogue = rootView.findViewById(R.id.rcv_course_catalogue);
        initView();
    }

    private void initData() {
        courseCatalogueBeans = new ArrayList<>();
    }

    private void initView() {
        rcvCourseCatalogue.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL, ResourcesCompat.getColor(getResources(), R.color.gray_cc, null), 1));

        rcvCourseCatalogue.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
//        courseGroupCatalogueAdapter = new CourseCataloguAdapter(mContext, courseCatalogueBeans);
        rcvCourseCatalogue.setAdapter((RecyclerView.Adapter) courseGroupCatalogueAdapter);
    }

    public void setData(List<CourseKpointListBean> courseKpointList) {


        this.courseCatalogueBeans.clear();
        this.courseCatalogueBeans.addAll(courseKpointList);
        courseGroupCatalogueAdapter.notifyDataSetChanged();
    }
}
