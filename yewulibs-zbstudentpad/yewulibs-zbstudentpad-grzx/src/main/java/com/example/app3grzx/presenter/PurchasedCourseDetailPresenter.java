package com.example.app3grzx.presenter;

import android.content.Intent;
import android.text.TextUtils;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3grzx.R;
import com.example.app3grzx.view.PurchasedCourseDetailView;
import com.example.app3libpublic.event.MineCourseEvent;
import com.example.app3libvariants.bean.AddToCartBean;
import com.example.app3libvariants.bean.NewLiveInfo;
import com.example.app3libvariants.bean.NewVideoInfo;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.ShoppingCartBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewDetailFunc;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.google.gson.Gson;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public class PurchasedCourseDetailPresenter extends BasePresenter<PurchasedCourseDetailView> {

    public void getLivingInfo(int kpointId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("kpointId", String.valueOf(kpointId));
            map.put("channel", "2");
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getNewLiveInfo(requestBody)
                    .compose(TransformUtils.<ResultBean<NewLiveInfo>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<NewLiveInfo>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewLiveInfo>() {
                        @Override
                        public void onNext(NewLiveInfo courses) {
                            if (courses != null) {
                                getView().getLiveRoomInfoSuccrss(courses);
                            }
                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());
                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }
    }

    /**
     * 回放
     *
     * @param kpointId
     */
    public void getReplayInfo(int kpointId, int courseId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("kpointId", String.valueOf(kpointId));
            map.put("courseId", String.valueOf(courseId));
            map.put("channel", "2");
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getNewReplayInfo(requestBody)
                    .compose(TransformUtils.<ResultBean<NewVideoInfo>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<NewVideoInfo>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewVideoInfo>() {
                        @Override
                        public void onNext(NewVideoInfo courses) {
                            if (courses != null) {
                                getView().getReplayInfoSuccess(courses);
                            } else {
                                ToastUtils.showShort("回放教室不存在或已删除");
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());
                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }
    }


    /**
     * 点播   。。
     *
     * @param kpointId
     */
    public void getVideoInfo(int kpointId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("kpointId", String.valueOf(kpointId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getNewVideoInfo(requestBody)
                    .compose(TransformUtils.<ResultBean<NewVideoInfo>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<NewVideoInfo>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewVideoInfo>() {
                        @Override
                        public void onNext(NewVideoInfo courses) {
                            if (courses != null) {
                                getView().getVideoRoomInfoSuccrss(courses);
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());
                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }
    }


    /**
     * 加入购物车
     *
     * @param courseId
     * @param isToBuy  是否为购买操作
     */
    public void addShoppingCart(int courseId, final boolean isToBuy) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("goodsId", String.valueOf(courseId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .addShoppingCart(requestBody)
                    .compose(TransformUtils.<ResultBean<AddToCartBean>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<AddToCartBean>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<AddToCartBean>() {
                        @Override
                        public void onNext(AddToCartBean addToCartBean) {
                            int cartNum = addToCartBean.getShopcartList() == null ? 0 : addToCartBean.getShopcartList().size();
                            if (isToBuy) {
                                if (cartNum > 0) {
                                    getView().toSettlement(addToCartBean.isIsexsits(), addToCartBean.getId(), cartNum);
                                }
                            } else {
                                getView().addShoppingCartSuccess(addToCartBean.isIsexsits(), cartNum);
                            }
                        }

                        @Override
                        public void onFail(Throwable e) {
                            String msg = mActivity.getString(R.string.request_failure_try_again);
                            if (e != null) {
                                msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                            }
                            getView().addShoppingCartFailure(msg);
                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }
    }


    public void addFavorite(int courseId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("courseId", String.valueOf(courseId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .addFavorite(requestBody)
                    .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                        @Override
                        public void onNext(Object obj) {
                            getView().addFavoriteSuccess();
                        }

                        @Override
                        public void onFail(Throwable e) {
                            String msg = mActivity.getString(R.string.request_failure_try_again);
                            if (e != null) {
                                msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                            }
                            getView().addFavoriteFailure(msg);
                        }
                    }, mActivity, true));
            addSubscribe(subscribe);
        }
    }

    public void delFavorite(int courseId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("courseId", String.valueOf(courseId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .delCollection(requestBody)
                    .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                        @Override
                        public void onNext(Object obj) {
                            getView().delFavoriteSuccess();
                        }

                        @Override
                        public void onFail(Throwable e) {
                            String msg = mActivity.getString(R.string.request_failure_try_again);
                            if (e != null) {
                                msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                            }
                            getView().delFavoriteFailure(msg);
                        }
                    }, mActivity, true));
            addSubscribe(subscribe);
        }
    }

    public void queryShoppingCart() {
        if (!SPToken.autoLogin(mActivity)) {
            return;
        }
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .queryShoppingCart()
                .compose(TransformUtils.<ResultBean<List<ShoppingCartBean.ShopCartListBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<ShoppingCartBean.ShopCartListBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<ShoppingCartBean.ShopCartListBean>>() {


                    @Override
                    public void onNext(List<ShoppingCartBean.ShopCartListBean> shopCartList) {
                        if (shopCartList != null && shopCartList.size() > 0) {
                            getView().getCartNumSuccess(shopCartList.size());
                        } else {
                            getView().getCartNumSuccess(0);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        getView().getCartNumSuccess(0);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 报名接口
     */
    public void getIsPurchase(int courseId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("courseId", String.valueOf(courseId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getIsPurchase(requestBody)
                    .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                    .map(new ResponseNewDetailFunc<Object>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                        @Override
                        public void onNext(Object courses) {
                            if (courses != null) {
                                getView().applySuccess();
                                EventBus.getDefault().post(new MineCourseEvent(true));
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());

                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }
    }


    public static final String LOGIN_DETAIL = "LOGIN_DETAIL";

    /**
     * 点击判断是否登录   未登录 点击 即跳转
     */
    private boolean isToLogin() {
        if (!SPToken.autoLogin(mActivity)) {
//            IntentController.toLogin(mActivity,true);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
            intent.putExtra(LOGIN_DETAIL, true);
            App2.get().startActivity(intent);
            return false;
        }
        return true;
    }

}
