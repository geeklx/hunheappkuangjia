package com.example.app3grzx.view;

import com.example.app3libvariants.bean.MineList;
import com.example.app3libvariants.bean.MineTaskBean;
import com.example.app3libvariants.bean.NewLiveInfo;
import com.example.app3libvariants.bean.NewVideoInfo;
import com.sdzn.core.base.BaseView;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/24
 */

public interface MineTaskView extends BaseView {

    void listCourseSuccess(List<MineTaskBean.RecordsBean> list);

    void listCourseEmpty();

    void listCourseError(String msg);

    void validatetime(Object url,String type);
}
