package com.example.app3grzx.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

/**
 * 描述：
 * - viewpager 内容为带标题的fragment的adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class PageAdapterWithIndicator extends PageAdapterWithAdapter {
    private String[] titles;
    private int mChildCount = 0;

    public PageAdapterWithIndicator(FragmentManager fm, List<Fragment> fragments, String[] titles) {
        super(fm, fragments);
        this.titles = titles;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position % titles.length];
    }


    @Override
    public void notifyDataSetChanged() {
        mChildCount = getCount();
        super.notifyDataSetChanged();
    }


    @Override
    public int getItemPosition(Object object) {
        if (mChildCount > 0) {
            mChildCount--;
            return PagerAdapter.POSITION_NONE;
        }
        return super.getItemPosition(object);

    }
}