package com.example.app3grzx.presenter;

import android.text.TextUtils;

import com.example.app3grzx.view.CourseDetailView;
import com.example.app3libvariants.bean.CourseCatalogueBean;
import com.example.app3libvariants.bean.CourseDetailBean;
import com.example.app3libvariants.bean.CourseIncludeBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.google.gson.Gson;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public class CourseDetailPresenter extends BasePresenter<CourseDetailView> {


    /**
     * 获取组合课程详情
     **/
    public void getCoursePackageDetailData(int courseId) {//组合
        Map<String, String> map = new HashMap<>();
        map.put("courseId", String.valueOf(courseId));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCoursePackageDetail(requestBody)
                .compose(TransformUtils.<ResultBean<CourseDetailBean>>defaultSchedulers())
                .map(new ResponseNewFunc<CourseDetailBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseDetailBean>() {
                    @Override
                    public void onNext(CourseDetailBean packageDetail) {
                        StringBuilder courseDesc = new StringBuilder();
                        ArrayList<String> listCourse = new ArrayList<>();
                        if (packageDetail.getProjectId() != 0) {
                            courseDesc.append("专题名称：" + packageDetail.getProjectName());
                            packageDetail.setCourseDescString(courseDesc.toString());
                        } else {

                            courseDesc.append("科目：");
                            for (CourseCatalogueBean courseCatalogueBean : packageDetail.getCourseList()) {
                                if (courseCatalogueBean.getSubjectName() != null && !courseCatalogueBean.getSubjectName().isEmpty() &&
                                        !listCourse.contains(courseCatalogueBean.getSubjectName())) {
                                    listCourse.add(courseCatalogueBean.getSubjectName());
                                    courseDesc.append(courseCatalogueBean.getSubjectName()).append("、");
                                }
                            }
                            if (!courseDesc.toString().isEmpty() && courseDesc.toString().length() > 3) {
                                courseDesc.deleteCharAt(courseDesc.toString().lastIndexOf("、"));
                                packageDetail.setCourseDescString(courseDesc.toString());
                            }
                        }
                        getView().getCourseDetailSuccess(packageDetail);

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = "";
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCourseDetailFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 获取普通课程详情
     */
    public void getCourseDetaiNormal(int courseId) {
        Map<String, String> map = new HashMap<>();
        map.put("courseId", String.valueOf(courseId));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCourseDetailNormal(requestBody)
                .compose(TransformUtils.<ResultBean<CourseIncludeBean>>defaultSchedulers())
                .map(new ResponseNewFunc<CourseIncludeBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseIncludeBean>() {
                    @Override
                    public void onNext(CourseIncludeBean courseIncludeBean) {
                        CourseDetailBean courseDetail = courseIncludeBean.getEduCourse();
                        if (courseIncludeBean.getEduTeacherList() != null && courseIncludeBean.getEduTeacherList().size() > 0) {
                            StringBuilder courseDesc = new StringBuilder();
                            for (CourseIncludeBean.EduTeacherListBean teacherListBean : courseIncludeBean.getEduTeacherList()) {
                                if (teacherListBean.getName() != null && !teacherListBean.getName().isEmpty()) {
                                    courseDesc.append(teacherListBean.getName()).append("、");
                                }
                            }
                            if (!courseDesc.toString().isEmpty()) {
                                courseDesc.deleteCharAt(courseDesc.lastIndexOf("、"));
                                courseDetail.setCourseDescString(courseDesc.toString());
                            }
                        }
                        getView().getCourseDetailSuccess(courseDetail);

                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = "";
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCourseDetailFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
