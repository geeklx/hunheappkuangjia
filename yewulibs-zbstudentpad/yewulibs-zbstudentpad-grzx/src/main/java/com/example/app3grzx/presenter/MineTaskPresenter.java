package com.example.app3grzx.presenter;

import com.example.app3grzx.view.MineTaskView;
import com.example.app3libvariants.bean.MineTaskBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewSchoolFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import rx.Subscription;

/**
 * zs
 */

public class MineTaskPresenter extends BasePresenter<MineTaskView> {
    public void getCourse(final String studentId, final String type, final int limit, final int page) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getMineTask(studentId, type, limit, page)
                .compose(TransformUtils.<ResultBean<MineTaskBean>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<MineTaskBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<MineTaskBean>() {
                    @Override
                    public void onNext(MineTaskBean mineTaskBean) {
                        if (mineTaskBean.getRecords() == null) {
                            getView().listCourseEmpty();
                        } else {
                            getView().listCourseSuccess(mineTaskBean.getRecords());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    public void getValidate(String homeworkId, String studentId, String type, String terminal, final String AnsweringState) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getValidate(homeworkId, studentId, type, terminal)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<Object>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object mineTaskBean) {
                        getView().validatetime(mineTaskBean, AnsweringState);
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        ToastUtil.showLonglToast(e.getMessage());
//                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
