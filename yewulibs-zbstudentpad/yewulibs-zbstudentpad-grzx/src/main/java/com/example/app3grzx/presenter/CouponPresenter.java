package com.example.app3grzx.presenter;

import com.example.app3grzx.view.CouponView;
import com.sdzn.core.base.BasePresenter;

/**
 * 描述：获取优惠券
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/20
 */

public class CouponPresenter extends BasePresenter<CouponView> {

    public void getListCoupon() {
        /*Subscription subscription = RestApi.getInstance()
                .create(AccountService.class)
                .listCoupon()
                .compose(TransformUtils.<ResultBean<List<CouponBean>>>defaultSchedulers())
                .map(new ResponseFunc<List<CouponBean>>())
                .subscribe(new MProgressSubscriber<List<CouponBean>>(new SubscriberOnNextListener<List<CouponBean>>() {

                    @Override
                    public void onNext(List<CouponBean> couponBeanList) {
                        getView().listOnSuccess(couponBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onError(msg);

                    }
                }, mActivity, false));
        addSubscribe(subscription);*/
    }

    /**
     * 查询购物车
     */
    public void queryShoppingCart() {
/*
        Subscription subscribe = RestApi.getInstance()
                .create(CourseService.class)
                .queryShoppingCart()
                .compose(TransformUtils.<ResultBean<ShoppingCartBean>>defaultSchedulers())
                .map(new ResponseFunc<ShoppingCartBean>())
                .retryWhen(new RetryWhenNetworkException())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<ShoppingCartBean>() {
                    @Override
                    public void onNext(ShoppingCartBean shoppingCartBeen) {
                        getView().listOnSuccess(shoppingCartBeen.getCouponCodeList());
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onError(msg);
                    }
                }, mActivity, false));

        addSubscribe(subscribe);*/
    }
}
