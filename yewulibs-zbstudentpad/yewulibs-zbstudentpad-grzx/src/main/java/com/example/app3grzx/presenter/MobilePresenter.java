package com.example.app3grzx.presenter;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.example.app3grzx.R;
import com.example.app3grzx.view.MobileView;
import com.example.app3libpublic.utils.ChangePhoneCountDownTimerUtils;
import com.example.app3libpublic.utils.VerifyUtil;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class MobilePresenter extends BasePresenter<MobileView> {


    /**
     * 获取 图形验证码  token
     */
    public void QueryImgToken() {
        Map<String, String> requestParams = new HashMap<>();
        String jsonPay = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .QueryImgToken(requestBodyPay)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().OnImgTokenSuccess(obj);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);


    }
    /**
     * 获取 图形验证码  图形
     */
    public void QueryImg(String imgToken) {
        RestApi.getInstance()
                .createNew(CourseService.class)
                .QueryImg(imgToken)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        InputStream inputStream = response.body().byteStream();
                        getView().OnImgSuccess(inputStream);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        getView().getFailure("获取图形验证码失败");
                    }
                });

    }


    /**
     * 获取验证码
     *
     * @param mobile
     * @param
     */
//    public void gainCode(String mobile, View rootView) {
    public void gainCode(String mobile, String imgCode, String imgToken) {
        //先判断下手机号是否为空
        if (TextUtils.isEmpty(mobile.trim())) {
            ToastUtils.showShort("请输入手机号");
            return;
        }
        if (!VerifyUtil.isMobileNO(mobile.trim())) {
            ToastUtils.showShort("手机号格式错误");
            return;
        }
        if (TextUtils.isEmpty(imgCode)) {
            ToastUtils.showShort("图形验证码不能为空");
            return;
        }

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .sendYZM(mobile,imgCode,imgToken)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscribe);
    }


    /**
     * @param mobile
     * @param code
     */
    public void confirm(String mobile, String code) {

        //先判断下手机号是否为空
        if (TextUtils.isEmpty(mobile.trim())) {
            ToastUtils.showShort("请输入手机号");
            return;
        }
        if (!VerifyUtil.isMobileNO(mobile.trim())) {
            ToastUtils.showShort("手机号格式错误");
            return;
        }
        //判断验证码是否为空
        if (TextUtils.isEmpty(code.trim())) {
            ToastUtils.showShort("请输入验证码");
            return;
        }
        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .confirmPhoneVerifyCode(mobile, code, SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {

                    @Override
                    public void onNext(Object o) {
                        getView().onSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onError(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);

    }
}
