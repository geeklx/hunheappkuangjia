package com.example.app3grzx.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app3grzx.R;
import com.example.app3grzx.presenter.MobilePresenter;
import com.example.app3grzx.view.MobileView;
import com.example.app3libpublic.event.BindEvent;
import com.example.app3libpublic.utils.YanzhengUtil;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * 绑定手机
 *
 * @author Reisen at 2017-12-07
 */

public class BindingMobileFragment extends BaseMVPFragment<MobileView, MobilePresenter> implements MobileView, View.OnClickListener {

    EditText mMobile;
    EditText mCode;
    Button bt_confirm;
    TextView tv_getCode;
    TitleBar titleBar;

    private UserBean userBean;
    private TextWatcher mWatcher;
    private ImageView Effectiveness;
    private EditText valicodeEdit;

    public static BindingMobileFragment newInstance() {
        return new BindingMobileFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_bindmobile;
    }

    @Override
    protected void onInit(Bundle bundle) {
        mMobile = rootView.findViewById(R.id.tv_mobile);
        mCode = rootView.findViewById(R.id.tv_code);
        bt_confirm = rootView.findViewById(R.id.bt_confirm);
        tv_getCode = rootView.findViewById(R.id.tv_getcode);
        titleBar = rootView.findViewById(R.id.title);
        Effectiveness = rootView.findViewById(R.id.iv_effectiveness);
        valicodeEdit = rootView.findViewById(R.id.valicode_edit);
        Effectiveness.setOnClickListener(this);
        initData();
        intView();
    }

    private void initData() {
        userBean = SPManager.getUser();
        mPresenter.QueryImgToken();
    }

    private void intView() {
        tv_getCode.setOnClickListener(this);
        bt_confirm.setOnClickListener(this);
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String mobile = mMobile.getText().toString().trim();
                String code = mCode.getText().toString().trim();
                tv_getCode.setEnabled(!TextUtils.isEmpty(mobile));
                bt_confirm.setEnabled(!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(code));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        mMobile.addTextChangedListener(mWatcher);
        mCode.addTextChangedListener(mWatcher);
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        final String mobile = mMobile.getText().toString().trim();
        final String code = mCode.getText().toString().trim();
        String valicode = valicodeEdit.getText().toString().trim();
        int id = v.getId();
        if (id == R.id.bt_confirm) {
            KeyboardUtils.hideSoftInput(getActivity());
            if (!checkPhone(mobile)) {
                return;
            }
            mPresenter.confirm(mobile, code);
        } else if (id == R.id.tv_getcode) {
            if (!checkPhone(mobile)) {
                return;
            }
            mPresenter.gainCode(mobile, valicode, imgToken);
        }else if (R.id.iv_effectiveness == v.getId()) {
            mPresenter.QueryImgToken();
        }
    }

    private boolean checkPhone(String mobile) {
        if (mobile == null) {
            ToastUtils.showShort("请输入手机号");
            return false;
        }
        if (userBean == null) {
            userBean = SPManager.getUser();
        }
       /* if (userBean.isBundlingState() && TextUtils.equals(userBean.getMobile(), mobile)) {
            ToastUtils.showShort("新手机号不能与原手机号相同");
            return false;
        }*/
        return true;
    }

    @Override
    protected MobilePresenter createPresenter() {
        return new MobilePresenter();
    }

    @Override
    public void onSuccess() {
        String mobile = mMobile.getText().toString().trim();
        userBean.setMobile(mobile);
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        ToastUtils.showShort("绑定成功");
        onBackPressed();
    }

    @Override
    public void onError(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
        YanzhengUtil.startTime(60 * 1000, tv_getCode);//倒计时
    }

    @Override
    public void getCodeFailure(String msg) {
        mPresenter.QueryImgToken();
        ToastUtils.showShort(msg);
    }

    private String imgToken;

    @Override
    public void OnImgTokenSuccess(Object imgtoken) {
        if (imgtoken == null) {
            return;
        }
        imgToken = String.valueOf(imgtoken);
        mPresenter.QueryImg(imgToken);
    }

    @Override
    public void OnImgSuccess(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        Glide.with(getActivity()).load(bytes).centerCrop().into(Effectiveness);

    }

    @Override
    public void getFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        YanzhengUtil.timer_des();
    }
}
