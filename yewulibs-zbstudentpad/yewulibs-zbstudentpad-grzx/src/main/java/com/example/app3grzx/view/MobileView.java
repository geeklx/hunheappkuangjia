package com.example.app3grzx.view;

import com.sdzn.core.base.BaseView;

import java.io.InputStream;

/**
 * 描述：绑定手机
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public interface MobileView extends BaseView {
    void onSuccess();

    void onError(String msg);

    void getCodeSuccess();

    void getCodeFailure(String msg);

    void OnImgTokenSuccess(Object imgtoken);

    void OnImgSuccess(InputStream inputStream);

    void getFailure(String msg);
}
