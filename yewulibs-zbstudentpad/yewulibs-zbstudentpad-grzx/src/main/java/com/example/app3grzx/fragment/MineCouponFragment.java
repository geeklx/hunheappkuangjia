package com.example.app3grzx.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3grzx.R;
import com.example.app3grzx.presenter.CouponPresenter;
import com.example.app3grzx.view.CouponView;
import com.example.app3libpublic.adapter.CouponAdapter;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.CouponBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的优惠券
 *
 * @author Reisen at 2017-12-07
 */

public class MineCouponFragment extends BaseMVPFragment<CouponView, CouponPresenter>
        implements CouponView, com.scwang.smartrefresh.layout.listener.OnRefreshListener {
    TitleBar titleBar;
    RecyclerView recyclerCoupon;
    SmartRefreshLayout swipeToLoadLayout;
    EmptyLayout emptyLayout;

    private CouponAdapter couponAdapter;
    private List<CouponBean> mData = new ArrayList<>();

    public static MineCouponFragment newInstance() {
        return new MineCouponFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine_coupon;
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleBar = rootView.findViewById(R.id.title_bar);
        recyclerCoupon = rootView.findViewById(R.id.swipe_target);
        swipeToLoadLayout = rootView.findViewById(R.id.swipeToLoadLayout);
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        initView();
        initData();


    }

    private void initData() {
        if (mData.isEmpty()) {
            mPresenter.getListCoupon();
        }
    }

    private void initView() {
        couponAdapter = new CouponAdapter(mContext, mData);
        recyclerCoupon.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerCoupon.setAdapter((RecyclerView.Adapter) couponAdapter);
        recyclerCoupon.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_f6, null), 10));
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
        swipeToLoadLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
//        mPresenter.queryShoppingCart();
        mPresenter.getListCoupon();
    }

    @Override
    protected CouponPresenter createPresenter() {
        return new CouponPresenter();
    }

    @Override
    public void listOnSuccess(List<CouponBean> couponBeanList) {
        if (couponBeanList != null && !couponBeanList.isEmpty()) {
            mData.clear();
            mData.addAll(couponBeanList);
            couponAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        }

        swipeToLoadLayout.finishRefresh();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        swipeToLoadLayout.finishRefresh();
    }
}
