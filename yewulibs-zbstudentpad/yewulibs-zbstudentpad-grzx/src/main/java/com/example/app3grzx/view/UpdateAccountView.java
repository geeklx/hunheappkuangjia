package com.example.app3grzx.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.UserBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/16
 */
public interface UpdateAccountView extends BaseView {

    void updateAccountSuccess(UserBean userBean);

    void updateAccountFailure(String msg);
}
