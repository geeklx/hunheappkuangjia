package com.example.app3grzx.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.app3grzx.R;
import com.example.app3grzx.presenter.ChangePwdPresenter;
import com.example.app3grzx.view.ChangePwdView;
import com.example.app3libpublic.utils.DialogUtil;
import com.example.app3libpublic.widget.PwdEditText;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * 修改密码
 *
 * @author Reisen at 2017-12-08
 */

public class ChangePwdFragment extends BaseMVPFragment<ChangePwdView, ChangePwdPresenter> implements ChangePwdView {
    TitleBar titleBar;
    PwdEditText etOldpwd;
    PwdEditText etNewpwd;
    PwdEditText etAgainpwd;
    Button btAffirm;
    TextView tvOld;

    public static ChangePwdFragment newInstance() {
        return new ChangePwdFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_changepwd;
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleBar = rootView.findViewById(R.id.title_bar);
        etOldpwd = rootView.findViewById(R.id.et_oldpwd);
        etNewpwd = rootView.findViewById(R.id.et_newpwd);
        etAgainpwd = rootView.findViewById(R.id.et_againpwd);
        btAffirm = rootView.findViewById(R.id.bt_affirm);
        tvOld = rootView.findViewById(R.id.tv_oldpwd);
        rootView.findViewById(R.id.bt_affirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String old = etOldpwd.getText().toString().trim();
                String newP = etNewpwd.getText().toString().trim();
                String again = etAgainpwd.getText().toString().trim();
                if (SPManager.getUser().getPassword() == null || SPManager.getUser().getPassword().isEmpty()) {
                    mPresenter.confirm(newP, again);
                } else {
                    mPresenter.confirmPwd(old, newP, again);
                }
            }
        });
        initView();
        initData();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initData() {
    }

    private void initView() {
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (SPManager.getUser().getPassword() == null || SPManager.getUser().getPassword().isEmpty()) {//toc没设置密码
            tvOld.setVisibility(View.GONE);
            etOldpwd.setVisibility(View.GONE);
        }
    }

    @Override
    protected ChangePwdPresenter createPresenter() {
        return new ChangePwdPresenter();
    }

    @Override
    public void changeSuccess() {
        UserBean user = SPManager.getUser();
        user.setPassword(etNewpwd.getText().toString());
        SPManager.saveUser(user);
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_BIND_ACCOUNT));//去刷新密码
        DialogUtil.showReLoginDialog(getActivity(), "密码修改成功", false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //密码修改成功之后应该关掉所有界面并跳转到登录界面，
//                logout();
                onBackPressed();
            }
        });
    }

    @Override
    public void changeFailure(String msg) {
        ToastUtils.showShort(msg);

    }

    private void logout() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
    }
}
