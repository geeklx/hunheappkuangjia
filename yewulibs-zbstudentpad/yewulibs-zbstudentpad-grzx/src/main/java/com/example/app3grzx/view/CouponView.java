package com.example.app3grzx.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.CouponBean;

import java.util.List;

/**
 * 描述：我的优惠券
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/20
 */

public interface CouponView extends BaseView {
    void listOnSuccess(List<CouponBean> couponBeanList);

    void onError(String msg);
}
