package com.example.app3grzx.fragment;

import android.os.Bundle;
import android.text.Spanned;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.app3grzx.R;
import com.example.app3libvariants.bean.CourseDetailBean;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.utils.LogUtils;


/**
 * 描述：
 * - 课程介绍
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class CourseIntroduceFragment extends BaseFragment {

    WebView webView;
    //    @BindView(R.id.rcv_course_teacher)
//    RecyclerView rcvCourseTeacher;
//
//    private CourseTeacherAdapter courseTeacherAdapter;
//    private List<TeacherListBean> courseTeacherBeans;
    private CourseDetailBean courseDetailBean;

    public CourseIntroduceFragment() {

    }

    public static CourseIntroduceFragment newInstance() {
        return new CourseIntroduceFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course_introduce;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        webView = rootView.findViewById(R.id.web);
        initView();
    }

    private void initData() {

    }

    private void initView() {
        WebSettings settings = webView.getSettings();
        if (courseDetailBean != null && !TextUtils.isEmpty(courseDetailBean.getContext())) {
            Spanned result;
            LogUtils.w(courseDetailBean.getContext());

            //支持自动适配
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setSupportZoom(true);  //支持放大缩小
            settings.setBuiltInZoomControls(true); //显示缩放按钮
            settings.setBlockNetworkImage(true);// 把图片加载放在最后来加载渲染
            settings.setAllowFileAccess(false);
            settings.setSaveFormData(false);
            settings.setDomStorageEnabled(true);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            //设置不让其跳转浏览器
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }
            });

            webView.setWebChromeClient(new WebChromeClient());

//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                result = Html.fromHtml(courseDetailBean.getContext(), Html.FROM_HTML_MODE_LEGACY);
//            } else {
//                result = Html.fromHtml(courseDetailBean.getContext());
//            }
//            "<p><img src=\"http://file.znclass.com/c55c7aaf-77cf-461d-ab1c-8f4f82ccf86ca60cd365-a62f-4e84-a808-17b91bdeafc4微信图片_20200224095307.png\" title=\"\" alt=\"\"/>直播转点播</p>"


//            webView.loadData(courseDetailBean.getContext(), "text/html", "UTF-8");

            webView.loadDataWithBaseURL(null, courseDetailBean.getContext(), "text/html", "utf-8", null);

        }

    }

    public void setData(CourseDetailBean courseDetailBean) {
        this.courseDetailBean = courseDetailBean;
    }

}
