package com.example.app3pkt.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.R;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseFragment;

/**
 * @author Reisen at 2017-11-30
 */

public class SearchResultFragment extends BaseFragment implements View.OnClickListener {
    public static final String KEY_SEARCH_STR = "searchStr";
    public static final String KEY_COURSE_TYPE = "courseType";
    TextView tvSearch;
    private String searchStr;

    private int courseType;

    public static SearchResultFragment newInstance(String searchStr, @CourseCons.Type.CourseType int courseType) {

        Bundle args = new Bundle();
        args.putString(KEY_SEARCH_STR, searchStr);
        args.putInt(KEY_COURSE_TYPE, courseType);

        SearchResultFragment fragment = new SearchResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search_result;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        tvSearch = rootView.findViewById(R.id.tv_search);
        rootView.findViewById(R.id.iv_back).setOnClickListener(this);
        rootView.findViewById(R.id.tv_search).setOnClickListener(this);
        rootView.findViewById(R.id.iv_cart_shop).setOnClickListener(this);

        initView();
    }

    private void initData() {
        Bundle arg = getArguments();
        if (arg == null) {
            return;
        }
        searchStr = arg.getString(KEY_SEARCH_STR);
        courseType = arg.getInt(KEY_COURSE_TYPE, CourseCons.Type.ALL);
        //courseType  此处网listfragment传数据，是区分  所有学科还是单一学科
    }

    private void initView() {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_search_container, (Fragment) SpellingContentFragment.newInstance("", searchStr, SpellingContentFragment.IS_SEARCH_IN)).commit();
        tvSearch.setText(searchStr);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            onBackPressed();
        } else if (id == R.id.tv_search) {
            onBackPressed();
        } else if (id == R.id.iv_cart_shop) {
            if (!SPToken.autoLogin(mContext)) {
//                    IntentController.toLogin(mContext);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");//ShoppingCartActivity
            startActivity(intent);
//                IntentController.toShoppingCart(mContext);
        }
    }
}
