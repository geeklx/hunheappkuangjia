package com.example.app3pkt.presenter;

import android.text.TextUtils;

import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.SubjectBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3pkt.R;
import com.example.app3pkt.view.SpellingClassView;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import java.util.List;

import rx.Subscription;

public class SpellingClassPresenter extends BasePresenter<SpellingClassView> {

    public void getSubject() {
       /* Map<String, String> params = new HashMap<>();
        params.put("classId",String.valueOf(SPManager.getSectionId()));
        String json = new Gson().toJson(params);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);*/
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSubjectSpell(SPManager.getSectionId())
                .compose(TransformUtils.<ResultBean<List<SubjectBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<SubjectBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<SubjectBean>>() {
                    @Override
                    public void onNext(List<SubjectBean> subjectSpellBeanList) {
                        getView().onSubjectSuccess(subjectSpellBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onSubjectFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
