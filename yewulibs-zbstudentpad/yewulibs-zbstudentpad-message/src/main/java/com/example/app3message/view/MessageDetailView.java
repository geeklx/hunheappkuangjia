package com.example.app3message.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.MessageDetailBean;

/**
 * 描述：消息详情
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/21
 */

public interface MessageDetailView extends BaseView {
//    void getMessageDetailSuccess(MessageDetailBean messageDetailBean);
    void getMessageSuccess();

    void onError(String msg);
}
