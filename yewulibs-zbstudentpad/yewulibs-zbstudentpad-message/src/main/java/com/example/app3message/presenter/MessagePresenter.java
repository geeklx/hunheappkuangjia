package com.example.app3message.presenter;

import android.text.TextUtils;

import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.MessageBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3message.R;
import com.example.app3message.view.MessageView;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;

import java.util.List;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public class MessagePresenter extends BasePresenter<MessageView> {
    /**
     * 获取消息列表
     *
     * @param currentPage
     * @param pageSize
     */
    public void getMessageList(int currentPage, int pageSize) {
        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .listMessage(SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<List<MessageBean.LetterListBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<MessageBean.LetterListBean>>())
                .subscribe(new MProgressSubscriber<List<MessageBean.LetterListBean>>(new SubscriberOnNextListener<List<MessageBean.LetterListBean>>() {

                    @Override
                    public void onNext(List<MessageBean.LetterListBean> messageBean) {
                        getView().listMessage(messageBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onListError(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);

    }

    /**
     * 删除消息
     *
     * @param stringList
     */
    public void delMessage(List<String> stringList) {

        if (stringList.size() > 0) {
            StringBuilder buffer = new StringBuilder();
            for (String s : stringList) {
                buffer.append(s).append(",");

            }
            buffer.deleteCharAt(buffer.lastIndexOf(","));
            Subscription subscription = RestApi.getInstance()
                    .createNew(AccountService.class)
                    .delMessage(buffer.toString())
                    .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                    .map(new ResponseNewFunc<>())
                    .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {


                        @Override
                        public void onNext(Object o) {
                            getView().delMessage();
                        }

                        @Override
                        public void onFail(Throwable e) {
                            String msg = mActivity.getString(R.string.request_failure_try_again);
                            if (e != null) {
                                msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                            }
                            getView().delMessageOnError(msg);
                        }
                    }, mActivity, true, "请求中..."));
            addSubscribe(subscription);
        } else {
            ToastUtils.showShort("请选择你要删除的消息");
        }
    }

/*
    public void getUnReadMessageCount() {
        Subscription subscribe = RestApi.getInstance()
                .create(AccountService.class)
                .queryUnReadMsgCount()
                .compose(TransformUtils.<ResultBean<UnReadMsgCountBean>>defaultSchedulers())
                .map(new ResponseFunc<UnReadMsgCountBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UnReadMsgCountBean>() {
                    @Override
                    public void onNext(UnReadMsgCountBean resultBean) {
                        EventBus.getDefault().post(new MsgCountEvent((int) resultBean.getUnreadMsgCount()));
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
*/
}
