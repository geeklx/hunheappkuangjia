package com.example.app3message.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.MessageDetailBean;
import com.example.app3message.R;
import com.example.app3message.presenter.MessageDetailPresenter;
import com.example.app3message.view.MessageDetailView;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.core.utils.ToastUtils;

/**
 * 描述：消息详情
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class MessageDetailActivity extends BaseMVPActivity<MessageDetailView, MessageDetailPresenter> implements MessageDetailView {
    TitleBar titleBar;
    TextView tvDate;
    TextView tvMessageContent;
    public static final String ID_MESSAGE = "message_id";
    public static final String TYPE_MESSAGE = "message_type";
    public static final String CONTENT = "message_content";
    public static final String ADD_TIME = "message_add_time";
    EmptyLayout emptyLayout;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_messagestatic;
    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
        StatusBarUtil.statusBarLightMode(this);
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleBar = findViewById(R.id.title_bar);
        tvDate = findViewById(R.id.tv_date);
        tvMessageContent = findViewById(R.id.tv_message_content);
        emptyLayout = findViewById(R.id.empty_layout);
        String iii=getIntent().getStringExtra(ID_MESSAGE);
        if (!TextUtils.isEmpty(getIntent().getStringExtra(ID_MESSAGE))) {
            //更改状态
            mPresenter.getMessageDetail(getIntent().getStringExtra(ID_MESSAGE));
        }
        if (!TextUtils.isEmpty(getIntent().getStringExtra(TYPE_MESSAGE))) {
            titleBar.setTitleText(getIntent().getStringExtra(TYPE_MESSAGE));
        }
//        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mPresenter.getMessageDetail(getIntent().getStringExtra(ID_MESSAGE));
//            }
//        });

        if (!TextUtils.isEmpty(getIntent().getStringExtra(CONTENT))) {
            tvMessageContent.setText(getIntent().getStringExtra(CONTENT));

            String addTime = getIntent().getStringExtra(ADD_TIME);
            tvDate.setText(addTime);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }


    }

    @Override
    protected MessageDetailPresenter createPresenter() {
        return new MessageDetailPresenter();
    }

/*
    @Override
    public void getMessageDetailSuccess(MessageDetailBean messageDetailBean) {
        //获取未读消息数
        mPresenter.getUnReadMessageCount();
        if (messageDetailBean != null) {
            if (!TextUtils.isEmpty(messageDetailBean.getMsgReceive().getContent())) {
                tvMessageContent.setText(messageDetailBean.getMsgReceive().getContent());
                webView.loadData(messageDetailBean.getMsgReceive().getContent(), "text/html", "UTF-8");
            }
            String addTime = TimeUtils.millis2String(messageDetailBean.getMsgReceive().getAddTime(), "yyyy.MM.dd HH:mm");
            tvDate.setText(addTime);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        }

    }
*/

    @Override
    public void getMessageSuccess() {
//获取未读消息数
        mPresenter.getUnReadMessageCount();
//        if (messageDetailBean != null) {
//            if (!TextUtils.isEmpty(messageDetailBean.getMsgReceive().getContent())) {
//                tvMessageContent.setText(messageDetailBean.getMsgReceive().getContent());
//            }
//            String addTime = TimeUtils.millis2String(messageDetailBean.getMsgReceive().getAddTime(), "yyyy.MM.dd HH:mm");
//            tvDate.setText(addTime);
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
//        } else {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
//        }
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
