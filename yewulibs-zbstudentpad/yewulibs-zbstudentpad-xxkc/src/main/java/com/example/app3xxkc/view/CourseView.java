package com.example.app3xxkc.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.CourseList;
import com.example.app3libvariants.bean.CourseListBean;
import com.example.app3libvariants.bean.GradeJson;
import com.example.app3libvariants.bean.SectionBean;
import com.example.app3libvariants.bean.SubjectBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/11
 */
public interface CourseView extends BaseView {
    void getSubjectSuccess(List<SubjectBean> subjectBeens);

    void getSubjectEmpty();

    void onGradeSuccess(List<GradeJson> gradeJson);//section & grade

    void onGradeEmpty();

    void getCourseSuccess(List<CourseList> courseListBeen);

    void getTocFailedEmpty();

    void getTobFailedEmpty();

    void getToLoginEmpty();
}
