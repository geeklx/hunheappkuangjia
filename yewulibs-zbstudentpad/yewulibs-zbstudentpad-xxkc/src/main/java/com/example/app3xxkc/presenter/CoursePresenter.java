package com.example.app3xxkc.presenter;

import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.CourseListRows;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.SubjectBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.network.api.ResponseNewSchoolFunc;
import com.example.app3xxkc.view.CourseView;
import com.google.gson.Gson;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * zs
 */
public class CoursePresenter extends BasePresenter<CourseView> {
    /**
     * 获取学科列表
     */

    public void getSubject(int sectionId) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSubjectSpell(sectionId)
                .compose(TransformUtils.<ResultBean<List<SubjectBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<SubjectBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<SubjectBean>>() {
                    @Override
                    public void onNext(List<SubjectBean> subjectSpellBeanList) {
                        getView().getSubjectSuccess(subjectSpellBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {

                        getView().getSubjectEmpty();
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 获取年级
     */
/*    public List<GradeJson> getGrade() {
        try {
            InputStream open = mActivity.getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }*/

    /**
     * 获取 课程
     */

    public void getCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCourseSchool(requestBody)
                .compose(TransformUtils.<ResultBean<CourseListRows>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<CourseListRows>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseListRows>() {
                    @Override
                    public void onNext(CourseListRows courses) {
                        if (courses != null) {
                            getView().getCourseSuccess(courses.getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        if (!SPToken.autoLogin(mActivity) || "40002".equals(e.getMessage())) {
                            getView().getToLoginEmpty();
                        } else if (SPManager.isToCLogin()) {//java.lang.Throwable: 请登录学校下发的学生账号查看！
                            getView().getTocFailedEmpty();
                        } else {
                            getView().getTobFailedEmpty();
                        }
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }


}
