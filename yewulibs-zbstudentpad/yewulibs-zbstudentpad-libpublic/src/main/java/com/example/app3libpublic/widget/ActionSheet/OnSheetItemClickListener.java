package com.example.app3libpublic.widget.ActionSheet;

public interface OnSheetItemClickListener {
    void onClick(int which);
}