package com.example.app3libpublic.adtivity.presenter;

import android.text.TextUtils;

import com.example.app3libpublic.R;
import com.example.app3libpublic.adtivity.view.CourseTopicView;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.TopicBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import rx.Subscription;

/**
 * 专题课程列表
 */

public class CourseTopicPresenter extends BasePresenter<CourseTopicView> {

    public void getTopicTitle() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTopicSpell(1)
                .compose(TransformUtils.<ResultBean<TopicBean>>defaultSchedulers())
                .map(new ResponseNewFunc<TopicBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<TopicBean>() {
                    @Override
                    public void onNext(TopicBean topicBean) {
                        if (topicBean != null && topicBean.getRecords().size() > 0) {
                            getView().onTopicSuccess(topicBean.getRecords());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onTopicFailed(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
