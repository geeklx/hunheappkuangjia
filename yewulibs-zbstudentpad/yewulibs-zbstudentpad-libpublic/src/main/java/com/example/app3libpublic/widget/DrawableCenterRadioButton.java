package com.example.app3libpublic.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatRadioButton;

/**
 * 描述：
 * - 文字图片居中显示的radiobutton
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class DrawableCenterRadioButton extends AppCompatRadioButton {
    public DrawableCenterRadioButton(Context context) {
        super(context);
    }

    public DrawableCenterRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawableCenterRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable[] drawables = getCompoundDrawables();
        Drawable drawableLeft = drawables[0];
        final float textWidth = getPaint().measureText(getText().toString());
        final int drawablePadding = getCompoundDrawablePadding();
        if (drawableLeft != null) {
            int drawableWidth = drawableLeft.getIntrinsicWidth();
            float bodyWidth = textWidth + drawableWidth + drawablePadding;
            if (bodyWidth > getWidth()) {
                bodyWidth = getWidth();
            }
            canvas.translate((getWidth() - bodyWidth) / 2, 0);
        }
        Drawable drawableRight = drawables[2];
        if (drawableRight != null) {
            setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            int drawableWidth = drawableRight.getIntrinsicWidth();
            float bodyWidth = textWidth + drawableWidth + drawablePadding;
            if (bodyWidth > getWidth()) {
                bodyWidth = getWidth();
            }
            canvas.translate(-(getWidth() - bodyWidth) / 2, 0);
        }
        super.onDraw(canvas);
    }

}