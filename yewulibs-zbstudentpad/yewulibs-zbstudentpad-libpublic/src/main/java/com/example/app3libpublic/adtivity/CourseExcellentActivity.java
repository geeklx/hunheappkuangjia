package com.example.app3libpublic.adtivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.R;
import com.example.app3libpublic.adapter.RecommendAdapter;
import com.example.app3libpublic.adapter.WrapAdapter;
import com.example.app3libpublic.adtivity.presenter.CourseExcellentPresenter;
import com.example.app3libpublic.adtivity.view.CourseExcellentView;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.CourseList;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.base.BaseRcvAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 精品课程列表
 */

public class CourseExcellentActivity extends BaseMVPActivity<CourseExcellentView, CourseExcellentPresenter>
        implements CourseExcellentView, OnRefreshListener, OnRefreshLoadmoreListener {

    SmartRefreshLayout refreshLayout;
    EmptyLayout emptyLayout;
    RecyclerView rvCourseRec;
    TitleBar titleBar;

    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private int pageIndex = 1;//当前页
    private int pageSize = 12;//
    public static final String COURSE_ID = "courseId";
    public static final String SHOW_LIVE_BTN = "showLiveBtn";
    public static final String PACKAGE = "PACKAGE";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_excellent;
    }


    @Override
    protected CourseExcellentPresenter createPresenter() {
        return new CourseExcellentPresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        refreshLayout = findViewById(R.id.refresh_layout);
        emptyLayout = findViewById(R.id.empty_layout);
        rvCourseRec = findViewById(R.id.rv_course);
        titleBar = findViewById(R.id.title_bar);
        pageIndex = 1;
        initData();
        initView();
        loadNetData();
    }

    private void initData() {
        recommendCourses = new ArrayList<>();
    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPToken.autoLogin(mContext)) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                    startActivity(intent);
//                    IntentController.toLogin(mContext);
                    return;
                }
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(intent);
//                IntentController.toShoppingCart(mContext);
            }
        });
        refreshLayout.setOnRefreshLoadmoreListener(this);
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 4));

        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
//                    IntentController.toCourseDetail(mContext, recommendCourses.get(position).getSellType(), recommendCourses.get(position).getCourseId(), false);
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                    intent.putExtra(PACKAGE, recommendCourses.get(position).getSellType());
                    intent.putExtra(COURSE_ID, recommendCourses.get(position).getCourseId());
                    intent.putExtra(SHOW_LIVE_BTN, false);
                    startActivity(intent);
                }

            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                loadNetData();
            }
        });
    }

    private void loadNetData() {
        Map<String, String> map = new HashMap<>();
        map.put("size", String.valueOf(pageSize));
        map.put("index", String.valueOf(pageIndex));
        mPresenter.getCourse(map);

    }




    @Override
    public void getDataCourse(List<CourseList> courses) {//recommendCourses
        clearLoingState();
        if (pageIndex == 1) {
            this.recommendCourses.clear();
            if (courses.size() == 0) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                return;
            }
        }
        refreshLayout.setLoadmoreFinished(courses.size() < pageSize);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);


    }

    @Override
    public void getDataFailure(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        loadNetData();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        loadNetData();

    }

    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }
}
