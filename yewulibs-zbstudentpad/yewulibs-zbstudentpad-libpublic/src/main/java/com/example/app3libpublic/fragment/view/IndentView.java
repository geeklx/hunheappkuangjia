package com.example.app3libpublic.fragment.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.IndentBean;
import com.example.app3libvariants.bean.IndentResultBean;
import com.example.app3libvariants.bean.PayInfoBean;

/**
 * 描述：订单
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public interface IndentView extends BaseView {
    void listindents(IndentResultBean bean);

    void onError(String msg);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void getPayInfoFailure();

    void cancelSuccess(IndentBean indentBean);

    void cancelError(String msg);

}
