package com.example.app3libpublic.utils;

import android.os.CountDownTimer;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.app3libpublic.R;
import com.sdzn.fzx.student.libutils.app.App2;

public class YanzhengUtil {
    /**
     * 倒计时控件
     */
    private static CountDownTimer timer;

    /**
     * 从x开始倒计时
     *
     * @param x
     */
    public static void startTime(long x, final TextView btnHqyzm) {
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(x, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int remainTime = (int) (millisUntilFinished / 1000L);
                btnHqyzm.setEnabled(false);
//                btnHqyzm.setBackgroundResource(R.drawable.common_btn_bg2);
                btnHqyzm.setText(App2.get().getResources().getString(R.string.yhzc_tip502, remainTime));
                btnHqyzm.setTextColor(ContextCompat.getColor(App2.get(), R.color.white));
            }

            @Override
            public void onFinish() {
                btnHqyzm.setEnabled(true);
//                btnHqyzm.setBackgroundResource(R.drawable.common_btn_bg1);
                btnHqyzm.setText(App2.get().getResources().getString(R.string.yhzc_tip5));
                btnHqyzm.setTextColor(ContextCompat.getColor(App2.get(), R.color.white));
            }
        };
        timer.start();
    }

    public static void timer_des() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


}
