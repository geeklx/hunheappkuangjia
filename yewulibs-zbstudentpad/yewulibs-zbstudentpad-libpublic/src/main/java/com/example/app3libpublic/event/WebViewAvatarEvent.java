package com.example.app3libpublic.event;

/**
 * 描述：更新头像
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/12
 */

public class WebViewAvatarEvent {

    private String AnsweringState;

    public WebViewAvatarEvent(String AnsweringState) {
        this.AnsweringState = AnsweringState;
    }

    public String getAnsweringState() {
        return AnsweringState;
    }

    public void setAnsweringState(String answeringState) {
        AnsweringState = answeringState;
    }


}
