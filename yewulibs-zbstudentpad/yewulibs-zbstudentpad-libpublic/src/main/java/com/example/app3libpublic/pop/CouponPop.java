package com.example.app3libpublic.pop;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3libpublic.R;
import com.example.app3libpublic.adapter.CouponAdapter;
import com.example.app3libvariants.bean.CouponBean;
import com.example.app3libpublic.utils.PopUtils;
import com.sdzn.core.base.BaseRcvAdapter;

import java.util.List;

/**
 * 描述：
 * - 分享pop
 * 创建人：baoshengxiang
 * 创建时间：2017/7/6
 */
public class CouponPop extends PopupWindow implements PopupWindow.OnDismissListener, View.OnClickListener {
    RecyclerView mRecyclerView;

    private Context mContext;
    private List<CouponBean> mCoupons;
    private CouponAdapter mAdapter;
    private OnItemClickedListener mListener;

    public CouponPop(Context context, List<CouponBean> coupons) {
        this(context, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, coupons);
    }

    public CouponPop(final Context context, int height, int width, List<CouponBean> coupons) {
        mContext = context;
        mCoupons = coupons;
        View contentView = LayoutInflater.from(context).inflate(R.layout.pop_coupon, null);
        mRecyclerView = contentView.findViewById(R.id.rv_coupon);
        contentView.findViewById(R.id.tv_cancel).setOnClickListener(this);
        setContentView(contentView);
        setHeight(height);
        setWidth(width);
        setOnDismissListener(this);
        setAnimationStyle(R.style.PopFromBottomAnimation);
        setFocusable(true);
        setOutsideTouchable(true);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        //initAdapter
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new CouponAdapter(mContext, mCoupons);
        mRecyclerView.setAdapter((RecyclerView.Adapter) mAdapter);
        mAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (mListener != null) {
                    mListener.clickItem(mCoupons.get(position));
                }
                onDismiss();
            }
        });
    }


    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        PopUtils.backgroundAlpha(mContext, 0.8f);
        super.showAtLocation(parent, gravity, x, y);
    }

    public void showAtViewCenter(View view, OnItemClickedListener listener) {
        mListener = listener;
        showAtLocation(view, Gravity.CENTER, 0, 0);
    }

    @Override
    public void onDismiss() {
        PopUtils.backgroundAlpha(mContext, 1);
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_cancel) {
            CouponPop.this.dismiss();
        }
    }

    public interface OnItemClickedListener {
        void clickItem(CouponBean bean);
    }
}
