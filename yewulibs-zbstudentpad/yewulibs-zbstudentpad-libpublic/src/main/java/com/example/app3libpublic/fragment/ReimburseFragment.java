package com.example.app3libpublic.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.app3libpublic.R;
import com.example.app3libpublic.event.OrderPayEvent;
import com.example.app3libpublic.fragment.presenter.ReimbursePresenter;
import com.example.app3libpublic.fragment.view.ReimburseView;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.ReimburseBean;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * 申请退款
 *
 * @author Reisen at 2017-12-07
 */

public class ReimburseFragment extends BaseMVPFragment<ReimburseView, ReimbursePresenter> implements ReimburseView {
    public static final String KEY_ORDERID = "orderId";

    TitleBar titleBar;
    EditText etReason;
    Button btConfirm;
    private int orderId;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_reimburse;
    }

    public static ReimburseFragment newInstance(int orderId) {
        Bundle args = new Bundle();
        args.putInt(KEY_ORDERID, orderId);
        ReimburseFragment fragment = new ReimburseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void onInit(Bundle bundle) {
        orderId = getArguments().getInt("orderId", -1);
        titleBar = rootView.findViewById(R.id.title_bar);
        etReason = rootView.findViewById(R.id.et_reason);
        btConfirm = rootView.findViewById(R.id.bt_confirm);
        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onViewClicked();
            }
        });
        initView();
        initData();
    }

    private void initData() {
    }

    private void initView() {
        etReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                btConfirm.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
            }
        });
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void onViewClicked() {
        String reason = etReason.getText().toString().trim();
        if (TextUtils.isEmpty(reason)) {
            ToastUtils.showShort("请填写退款理由");
            return;
        }
        mPresenter.dropOut(orderId, reason);
    }

    @Override
    protected ReimbursePresenter createPresenter() {
        return new ReimbursePresenter();
    }

    @Override
    public void dropSuccess(Object o) {
        ToastUtils.showShort("已申请，请耐心等待");
        EventBus.getDefault().post(new OrderPayEvent(false));
        onBackPressed();
    }


    @Override
    public void dropError(String msg) {
        ToastUtils.showShort(msg);
    }
}
