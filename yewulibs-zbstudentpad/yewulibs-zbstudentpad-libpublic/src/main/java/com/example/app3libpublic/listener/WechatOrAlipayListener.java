package com.example.app3libpublic.listener;

/**
 * Created by wgk on 2017/5/3.
 */

public interface WechatOrAlipayListener {
    void selectWx();

    void selectAlipay();
}
